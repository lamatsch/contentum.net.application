﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using Contentum.eAdmin.Service;
using Newtonsoft.Json;
using System.Globalization;

public partial class eRecordComponent_UgyiratokList : System.Web.UI.UserControl
{

    private ASP.component_listheader_ascx ListHeader1;

    public ASP.component_listheader_ascx ListHeader
    {
        get { return this.ListHeader1; }
        set { this.ListHeader1 = value; }
    }

    private ScriptManager ScriptManager1;

    public ScriptManager ScriptManager
    {
        get { return this.ScriptManager1; }
        set { this.ScriptManager1 = value; }
    }

    public bool DossziePanelVisible
    {
        get { return PanelDossziekTD.Visible; }
        set { PanelDossziekTD.Visible = value; }
    }

    public ASP.erecordcomponent_dosszietreeview_ascx AttachedDosszieTreeView
    {
        get { return this.DosszieTreeView; }
        set { this.DosszieTreeView = value; }
    }

    private string _dosszieId = string.Empty;

    public string DosszieId
    {
        get { return this._dosszieId; }
        set { this._dosszieId = value; }
    }


    private string _dosszieId_HiddenField_ClientId;

    public string DosszieId_HiddenField_ClientId
    {
        get { return this._dosszieId_HiddenField_ClientId; }
        set { this._dosszieId_HiddenField_ClientId = value; }
    }


    public GridView UgyiratokGridView
    {
        get { return this.UgyUgyiratokGridView; }
    }


    UI ui = new UI();

    private String Startup = "";

    private bool isActive = true;

    public bool IsActive
    {
        get { return isActive; }
        set { isActive = value; }
    }


    bool TomegesLezarasEnabled
    {
        get; set;
    }
    private Constants.EnumUgyintezesiIdoFelfuggesztes RendszerParameterUgyIdoFelfuggesztesFancy;
    #region Base Page

    public void InitPage()
    {

        Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName);
        RendszerParameterUgyIdoFelfuggesztesFancy = Rendszerparameterek.GetUgyintezesiIdoFelfuggesztes(Page);
        if (Startup == Constants.Startup.Skontro
             || Startup == Constants.Startup.SkontroIrattarSearchForm
             || Startup == Constants.Startup.SkontroIrattarFastSearchForm)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "UgyiratokSkontroList");
        }
        else
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "UgyiratokList");
        }

        if (Session["UgyiratokListStartup"] == null)
        {
            if (Startup == "FromBejovoIktatas" || Startup == "FromBelsoIktatas" || Startup == Constants.Startup.FromEgyszerusitettIktatas
                 || Startup == Constants.Startup.SearchForm || Startup == Constants.Startup.FastSearchForm)
            {
                Session["UgyiratokListStartup"] = Startup;
                Response.Clear();
                Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath);
            }
            else if (Startup == Constants.Startup.SkontroIrattarSearchForm || Startup == Constants.Startup.SkontroIrattarFastSearchForm)
            {
                Session["UgyiratokListStartup"] = Startup;
                Response.Clear();
                Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath + "?" + QueryStringVars.Startup + "=" + Constants.Startup.Skontro);
            }
        }

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        // Jobb oldali dossziélista
        if (FunctionRights.GetFunkcioJog(Page, "MappakList"))
        {
            AttachedDosszieTreeView.FilterList += UgyUgyiratokGridViewBind;
            JavaScripts.RegisterSetDossziePositionScript(Page, false);
        }
        else
        {
            this.DossziePanelVisible = false;
        }

        HataridosFeladatok.SetFeladatJelzoColumnVisibility(Page, UgyUgyiratokGridView);

        // Skontró módban látszania kell az Ügyintézõ oszlopnak:
        // Meg kell keresni ezt az oszlopot, mert az oszlopsorrend változhat:

        int oszlopIndex_Ugyintezo = UI.GetGridViewColumnIndex(UgyUgyiratokGridView, "Ugyintezo_Nev");
        if (Startup == Constants.Startup.Skontro && oszlopIndex_Ugyintezo >= 0)
        {
            UgyUgyiratokGridView.Columns[oszlopIndex_Ugyintezo].Visible = true;
        }

        if (!FelhasznaloProfil.OrgIsBOPMH(Page))
        {
            UI.SetGridViewColumnVisiblity(UgyiratokGridView, "KezelesTipusNev", false);
        }

        SetGridTukColumns();

        // BLG_619
        if (Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
        {
            UI.SetGridViewColumnVisiblity(UgyiratokGridView, "UgyFelelos_SzervezetKod", true);
        }
        else UI.SetGridViewColumnVisiblity(UgyiratokGridView, "UgyFelelos_SzervezetKod", false);

        TomegesLezarasEnabled = Rendszerparameterek.GetBoolean(Page, "TOMEGES_LEZARAS_ENABLED", false);
    }

    private void SetGridTukColumns()
    {
        bool isTuk = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK);
        if (!isTuk)
            return;

        BoundField bfIrattariHely = new BoundField();
        bfIrattariHely.DataField = "IrattariHely";
        bfIrattariHely.HeaderText = "Fizikai hely";
        bfIrattariHely.SortExpression = "EREC_UgyUgyiratok.IrattariHely";
        bfIrattariHely.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfIrattariHely.HeaderStyle.Width = new Unit("80");
        bfIrattariHely.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfIrattariHely.Visible = true;
        UgyiratokGridView.Columns.Add(bfIrattariHely);
    }

    public void LoadPage()
    {
        //EREC_UgyUgyiratokService svc = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        //ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());


        //Result res = svc.TeljessegEllenorzes(xpm, "E8FF8729-D853-DD11-83E6-0019DB6DA442");

        // BUG_12682
        ScriptManager.AsyncPostBackTimeout = 400;

        ListHeader1.SetRightFunctionButtonsVisible(false);

        // Dossziés ikonok láthatóságának állítása
        if (!string.IsNullOrEmpty(this.AttachedDosszieTreeView.SelectedId) && this.AttachedDosszieTreeView.FilterAttachedList)
        {
            ListHeader1.DossziebaHelyezVisible = false;
            ListHeader1.DossziebolKiveszVisible = FunctionRights.GetFunkcioJog(Page, "MappakList") && FunctionRights.GetFunkcioJog(Page, "MappaTartalmakNew");
        }
        else
        {
            ListHeader1.DossziebaHelyezVisible = FunctionRights.GetFunkcioJog(Page, "MappakList") && FunctionRights.GetFunkcioJog(Page, "MappaTartalmakInvalidate");
            ListHeader1.DossziebolKiveszVisible = false;
        }

        if (Startup != Constants.Startup.Skontro)
        {
            ListHeader1.HeaderLabel = Resources.List.UgyUgyiratListHeaderTitle;

            ListHeader1.NewVisible = false;
            ListHeader1.InvalidateVisible = false;
            //ListHeader1.FTSearchVisible = true;

            //bernat.laszlo added : Excel Export (Grid)
            ListHeader1.ExportVisible = true;
            ScriptManager.RegisterPostBackControl(ListHeader1.ExportButton);
            //bernat.laszlo eddig

            // A baloldali, alapból invisible ikonok megjelenítése:
            ListHeader1.PrintVisible = true;
            ListHeader1.SSRSPrintVisible = true;
            ListHeader1.UgyiratTerkepVisible = true;

            ListHeader1.IktatasVisible = true;
            ListHeader1.BejovoIratIktatasVisible = true;
            ListHeader1.BelsoIratIktatasVisible = true;
            ListHeader1.BejovoIratIktatasAlszamraVisible = true;
            ListHeader1.BelsoIratIktatasAlszamraVisible = true;
            ListHeader1.SzignalasVisible = true;
            ListHeader1.TomegesSzignalasVisible = true;
            //ListHeader1.AtadasVisible = true;
            ListHeader1.AtadasraKijelolVisible = true;

            ListHeader1.AtvetelVisible = true;
            ListHeader1.AtvetelUgyintezesreVisible = true;
            ListHeader1.VisszakuldesVisible = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page), Rendszerparameterek.VISSZAKULDES_ENABLED, false); //true;
            ListHeader1.SkontrobaHelyezVisible = true;
            // (A Page_PreRender-ben állítva)
            //ListHeader1.SkontrobolKivetelVisible = true;
            ListHeader1.IrattarozasraVisible = true;
            ListHeader1.LezarasVisible = true;
            ListHeader1.LezarasVisszavonasVisible = true;
            ListHeader1.TeljessegEllenorzesVisible = true;
            ListHeader1.SztornoVisible = true;
            ListHeader1.SzerelesVisible = true;
            ListHeader1.CsatolasVisible = true;
            ListHeader1.ElintezetteNyilvanitasVisible = true;
            ListHeader1.ElintezetteNyilvanitasTomegesVisible = true;

            ListHeader1.LockVisible = true;
            ListHeader1.UnlockVisible = true;

            ListHeader1.EloadoiIvVisible = true;
            // BLG_587
            //ListHeader1.PeresEloadoiIvVisible = true;
            ListHeader1.PeresEloadoiIvVisible = FunctionRights.GetFunkcioJog(Page, "PeresEloadoiIvNyomtatas");

            /// TODO: ikonok, amik még kellenek:
            // - ügyirat elintézetté nyilvánítás visszavonása
            ListHeader1.TomegesOlvasasiJogVisible = true;
        }
        else
        {
            ListHeader1.HeaderLabel = Resources.List.UgyUgyiratSkontroListHeaderTitle;

            ListHeader1.NewVisible = false;
            ListHeader1.InvalidateVisible = false;
            //ListHeader1.FTSearchVisible = true;

            // A baloldali, alapból invisible ikonok megjelenítése:
            ListHeader1.PrintVisible = true;
            ListHeader1.SSRSPrintVisible = true;
            ListHeader1.UgyiratTerkepVisible = true;

            ListHeader1.AtadasraKijelolVisible = true;
            ListHeader1.AtvetelVisible = true;
            ListHeader1.VisszakuldesVisible = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page), Rendszerparameterek.VISSZAKULDES_ENABLED, false); //true;
            ListHeader1.SkontrobolKivetelVisible = true;

            ListHeader1.LockVisible = true;
            ListHeader1.UnlockVisible = true;

            //Sorrend fontos!!! 1. Custom Session Name 2. SerachObject Type (ez a template miatt kell)
            ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.SkontroSearch;

            ListHeader1.KolcsonzesVisible = true;
            ListHeader1.KolcsonzesOnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(UgyUgyiratokGridView.ClientID);
            ListHeader1.Kolcsonzes.ToolTip = Resources.Buttons.Kikeres;

            ListHeader1.KolcsonzesKiadasVisible = true;
            ListHeader1.KolcsonzesKiadasOnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(UgyUgyiratokGridView.ClientID);
            ListHeader1.KolcsonzesKiadas.ToolTip = Resources.Buttons.Kiadas;

            ListHeader1.KolcsonzesVisszaVisible = true;
            ListHeader1.KolcsonzesVisszaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            ListHeader1.KolcsonzesVissza.ToolTip = Resources.Buttons.KikeresVissza;

            #region LZS - Excel ikon a Skontróhoz
            if (Startup == Constants.Startup.Skontro)
            {
                ListHeader1.ExportVisible = true;
                ScriptManager.RegisterPostBackControl(ListHeader1.ExportButton);
                ListHeader1.ExportToolTip = Resources.Buttons.Export;
            }
            #endregion
        }

        // CR3289 Vezetõi panel kezelés
        if (Startup == Constants.Startup.FromVezetoiPanel_Ugyirat)
        {
            ListHeader1.HeaderLabel = Resources.List.UgyUgyiratListHeaderTitle;
            ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch;
        }


        ListHeader1.NumericSearchVisible = true;

        ListHeader1.SearchObjectType = typeof(EREC_UgyUgyiratokSearch);


        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes", "Javascripts/CheckBoxes.js");


        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        #region BLG 1131 visszavétel
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK))
        {
            ListHeader1.VisszavetelVisible = true;
            ListHeader1.VisszavetelEnabled = true;
            ListHeader1.VisszavetelOnClientClick = "var count = getSelectedCheckBoxesCount('"
            + UgyiratokGridView.ClientID + "','check'); "
            + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
        }
        #endregion

        // CR3289 Vezetõi Panel kezelés
        if (Startup == Constants.Startup.FromVezetoiPanel_Ugyirat)
        {
            ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokSearch.aspx", Constants.Startup.StartupName + "=" + Constants.Startup.FromVezetoiPanel_Ugyirat
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        }
        else
        if (Startup == Constants.Startup.Skontro)
            ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokSearch.aspx?Startup=Skontro", ""
        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        else
            ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokSearch.aspx", ""
                      , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        if (Startup == Constants.Startup.Skontro)
        {
            ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Constants.Startup.Skontro,
            "", Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        }
        // CR3289 Vezetõi Panel kezelés
        else if (Startup == Constants.Startup.FromVezetoiPanel_Ugyirat)
        {
            ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Constants.Startup.FromVezetoiPanel_Ugyirat,
         "", Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        }
        else
        {
            ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Constants.Startup.FromUgyirat,
                 "", Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        }
        //ListHeader1.FTSearchOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokFTSearch.aspx", ""
        //    , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        //ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.New
        //    , Defaults.PopupWidthMaxExtended, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(UgyUgyiratokGridView.ClientID);
        if (Startup == Constants.Startup.Skontro)
        {
            ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("UgyiratokListajaSkontroPrintForm.aspx");
        }
        else
        {
            ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("UgyiratokListajaPrintForm.aspx");
        }


        var visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState().LoadFromGrid(UgyiratokGridView, new Contentum.eRecord.BaseUtility.GridViewColumns.UgyiratokListColumnNames().GetType()).ToCustomString();
        Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.UgyUgyiratokSSRS] = visibilityState;


        if (Startup == Constants.Startup.Skontro)
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('UgyUgyiratokSSRSSkontro.aspx')";
        }
        else
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('UgyUgyiratokSSRS.aspx')";
        }

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(UgyUgyiratokGridView.ClientID);

        ListHeader1.IktatasOnClientClick = JavaScripts.SetOnClientClick("IktatasElokeszitesForm.aspx"
             , QueryStringVars.Command + "=" + CommandName.New
             , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);


        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(UgyUgyiratokGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(UgyUgyiratokGridView.ClientID);

        ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx",
             QueryStringVars.Command + "=" + CommandName.New
             , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.BelsoIratIktatasOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx",
             QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas
             , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        string jsGetUgyiratId = "var ugyiratId = getIdIfOneSelectedCheckbox('" + UgyUgyiratokGridView.ClientID + @"','check');
                                 var qsUgyiratId = ''; if(ugyiratId && ugyiratId!=''){
                                 qsUgyiratId = '&" + QueryStringVars.UgyiratId + @"=' + ugyiratId;}";

        ListHeader1.BejovoIratIktatasAlszamraOnClientClick = jsGetUgyiratId + JavaScripts.SetOnClientClick("IraIratokForm.aspx",
             QueryStringVars.Command + "=" + CommandName.New + "'+qsUgyiratId+'"
             , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.BelsoIratIktatasAlszamraOnClientClick = jsGetUgyiratId + JavaScripts.SetOnClientClick("IraIratokForm.aspx",
             QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas + "'+qsUgyiratId+'"
             , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        //ListHeader1.AtadasraKijelolOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.AtadasraKijelolOnClientClick = "var count = getSelectedCheckBoxesCount('"
                  + UgyUgyiratokGridView.ClientID + "','check'); "
                  + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        //ListHeader1.AtadasraKijelolOnClientClick += JavaScripts.SetOnClientClick("AtadasForm.aspx",
        //    QueryStringVars.UgyiratId + "=" + "'+getIdsBySelectedCheckboxes('" + UgyUgyiratokGridView.ClientID + "','check')+'"
        //    , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.IrattarozasraOnClientClick = "var count = getSelectedCheckBoxesCount('"
                  + UgyUgyiratokGridView.ClientID + "','check'); "
                  + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.SzignalasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.TomegesSzignalasOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(UgyUgyiratokGridView.ClientID);

        //ListHeader1.AtvetelOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.AtvetelOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                        + UgyUgyiratokGridView.ClientID + "','check'); "
                                        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.AtvetelUgyintezesreOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                        + UgyUgyiratokGridView.ClientID + "','check'); "
                                        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.VisszakuldesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.SkontrobaHelyezOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        //ListHeader1.SkontrobolKivetelOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.SkontrobolKivetelOnClientClick = " var count = getSelectedCheckBoxesCount('"
                  + UgyUgyiratokGridView.ClientID + "','check'); "
                  + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.TeljessegEllenorzesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.SztornoOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.SzerelesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.CsatolasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.ElintezetteNyilvanitasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.ElintezetteNyilvanitasTomegesOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                      + UgyUgyiratokGridView.ClientID + "','check'); "
                                      + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.LezarasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        if (!TomegesLezarasEnabled)
        {
            ListHeader1.LezarasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        }
        else
        {
            ListHeader1.LezarasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                      + UgyUgyiratokGridView.ClientID + "','check'); "
                                      + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
        }


        ListHeader1.LezarasVisszavonasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                        + UgyUgyiratokGridView.ClientID + "','check'); "
                                        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(UgyUgyiratokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
            QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                        , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, "", true);

        ListHeader1.DossziebaHelyezOnClientClick = "var count = getSelectedCheckBoxesCount('"
                             + UgyUgyiratokGridView.ClientID + "','check'); "
                             + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}"
                             + " if (!$get('" + this.AttachedDosszieTreeView.SelectedId_HiddenField + "').value) {alert('Nincs dosszié kijelölve' + $get('" + this.AttachedDosszieTreeView.SelectedId_HiddenField + "').value); return false;}";

        ListHeader1.DossziebolKiveszOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                        + UgyUgyiratokGridView.ClientID + "','check'); "
                                        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.TomegesOlvasasiJogOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                        + UgyUgyiratokGridView.ClientID + "','check'); "
                                        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = UgyUgyiratokGridView;

        Search.SetIdsToSearchObject(Page, "Id", new EREC_UgyUgyiratokSearch(true));

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);


        // Ha Menubol-bol nyitjak meg, akkor new popup-ot kell csinalni!
        bool popupNyitas = false;
        if (Session["UgyiratokListStartup"] != null)
        {
            if (Session["UgyiratokListStartup"].ToString() == "FromBejovoIktatas" || Session["UgyiratokListStartup"].ToString() == "FromBelsoIktatas" ||
                 Session["UgyiratokListStartup"].ToString() == Constants.Startup.FromEgyszerusitettIktatas || Session["UgyiratokListStartup"].ToString() == Constants.Startup.SearchForm
                 || Session["UgyiratokListStartup"].ToString() == Constants.Startup.FastSearchForm
                 || Session["UgyiratokListStartup"].ToString() == Constants.Startup.SkontroIrattarSearchForm
                 || Session["UgyiratokListStartup"].ToString() == Constants.Startup.SkontroIrattarFastSearchForm)
            {
                string script = "OpenNewWindow(); function OpenNewWindow() { ";
                if (Session["UgyiratokListStartup"].ToString() == "FromBejovoIktatas")
                {
                    script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("IraIratokForm.aspx",
                         QueryStringVars.Command + "=" + CommandName.New
                         , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                         , CustomUpdateProgress1.UpdateProgress.ClientID);
                }
                else if (Session["UgyiratokListStartup"].ToString() == "FromBelsoIktatas")
                {
                    script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("IraIratokForm.aspx",
                         QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas
                         , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                         , CustomUpdateProgress1.UpdateProgress.ClientID);
                }
                else if (Session["UgyiratokListStartup"].ToString() == Constants.Startup.FromEgyszerusitettIktatas)
                {
                    script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("EgyszerusitettIktatasForm.aspx",
                    QueryStringVars.Command + "=" + CommandName.New
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                    , CustomUpdateProgress1.UpdateProgress.ClientID);
                }
                else if (Session["UgyiratokListStartup"].ToString() == Constants.Startup.SearchForm)
                {
                    script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("UgyUgyiratokSearch.aspx", ""
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                    , CustomUpdateProgress1.UpdateProgress.ClientID);
                }
                else if (Session["UgyiratokListStartup"].ToString() == Constants.Startup.FastSearchForm)
                {
                    script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("UgyiratokNumericSearch.aspx"
                         , Constants.Startup.StartupName + "=" + Constants.Startup.FromUgyirat, Defaults.PopupWidth, Defaults.PopupHeight
                         , UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList, CustomUpdateProgress1.UpdateProgress.ClientID);
                }
                else if (Session["UgyiratokListStartup"].ToString() == Constants.Startup.SkontroIrattarSearchForm)
                {
                    script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("UgyUgyiratokSearch.aspx", Constants.Startup.StartupName + "=" + Constants.Startup.Skontro
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                    , CustomUpdateProgress1.UpdateProgress.ClientID);
                }
                else if (Session["UgyiratokListStartup"].ToString() == Constants.Startup.SkontroIrattarFastSearchForm)
                {
                    script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("UgyiratokNumericSearch.aspx"
                         , Constants.Startup.StartupName + "=" + Constants.Startup.Skontro, Defaults.PopupWidth, Defaults.PopupHeight
                         , UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList, CustomUpdateProgress1.UpdateProgress.ClientID);
                }
                script += "}";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Iktatas", script, true);
                Session.Remove("UgyiratokListStartup");

                popupNyitas = true;
            }
        }

        //LZS - BUG_11055 - NMHH-nál nem kell ez a funkció
        if (!FelhasznaloProfil.OrgIsNMHH(Page))
        {
            // BLG_7742
            bool ugyintezesOszlopokLathatoak = !Rendszerparameterek.GetBoolean(Page, "UGYINTEZES_OSZLOPOK_ELREJTESE", false);
            UI.SetGridViewColumnVisiblity(UgyiratokGridView, "Hatarido", ugyintezesOszlopokLathatoak);
            UI.SetGridViewColumnVisiblity(UgyiratokGridView, "ElteltUgyintezesiIdo", ugyintezesOszlopokLathatoak);
            UI.SetGridViewColumnVisiblity(UgyiratokGridView, "SakkoraAllapotNev", ugyintezesOszlopokLathatoak);
            UI.SetGridViewColumnVisiblity(UgyiratokGridView, "EljarasiSzakaszFokNev", ugyintezesOszlopokLathatoak);

            UI.SetGridViewTemplateColumnVisiblity(UgyiratokGridView, "IH", ugyintezesOszlopokLathatoak); // SakkoraAllapotTipus
            UI.SetGridViewTemplateColumnVisiblity(UgyiratokGridView, "Hátralévõ napok", ugyintezesOszlopokLathatoak); // HatralevoNapok
            if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.UGYINTEZESI_HATARIDO_MASOLAS_UGYROL) == 1)
            {
                UI.SetGridViewTemplateColumnVisiblity(UgyiratokGridView, "Hátralévõ munkanapok", false);
            }
            else
            {
                UI.SetGridViewTemplateColumnVisiblity(UgyiratokGridView, "Hátralévõ munkanapok", ugyintezesOszlopokLathatoak);
            }
        }


        // Ha iktató vagy keresõ képernyõt is kell nyitni, nem töltjük fel a listát:
        if (popupNyitas)
        {
            ListHeader1.NincsFeltoltes = true;
        }
        else if (!IsPostBack)
        {
            if (Request.QueryString["SkipLoad"] == null)
                UgyUgyiratokGridViewBind();
        }

        ListHeader1.UgyintIdoUjraSzamolasOnClientClick = JavaScripts.SetOnClientClickConfirm(Resources.Form.Confirm, Resources.Form.UgyintezesiIdoUjraSzamolasMegerosites);
    }



    public void PreRenderPage()
    {

        ListHeader1.NewEnabled = false;
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Modify);
        ListHeader1.UgyiratTerkepEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.View);
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.ViewHistory);
        //bernat.laszlo added
        ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.ExcelExport);
        //bernat.laszlo eddig

        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        ListHeader1.IktatasEnabled = FunctionRights.GetFunkcioJog(Page, "IktatasElokeszites");

        ListHeader1.BejovoIratIktatasEnabled = FunctionRights.GetFunkcioJog(Page, "BejovoIratIktatas");
        ListHeader1.BelsoIratIktatasEnabled = FunctionRights.GetFunkcioJog(Page, "BelsoIratIktatas");

        ListHeader1.BejovoIratIktatasAlszamraEnabled = FunctionRights.GetFunkcioJog(Page, "BejovoIratIktatas");
        ListHeader1.BelsoIratIktatasAlszamraEnabled = FunctionRights.GetFunkcioJog(Page, "BelsoIratIktatas");

        ListHeader1.AtadasraKijelolEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratAtadas");

        ListHeader1.SzignalasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSzignalas");
        ListHeader1.TomegesSzignalasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSzignalas");

        ListHeader1.AtvetelUgyintezesreEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratAtvetelUgyintezesre");

        ListHeader1.AtvetelEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratAtvetel");

        ListHeader1.VisszakuldesEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratAtvetel"); // egyelõre az átvétel jogához kötjük

        ListHeader1.IrattarozasraEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratAtadasIrattarba");

        ListHeader1.SkontrobaHelyezEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSkontroKezeles")
             || FunctionRights.GetFunkcioJog(Page, "UgyiratSkontrobaHelyezes");

        ListHeader1.SkontrobolKivetelEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSkontroKezeles")
             || FunctionRights.GetFunkcioJog(Page, "UgyiratSkontrobolKivetel");

        ListHeader1.SzerelesEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSzereles");

        ListHeader1.CsatolasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratCsatolasNew");

        ListHeader1.LezarasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratLezaras");

        ListHeader1.LezarasVisszavonasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratLezarasVisszavonas");

        ListHeader1.TeljessegEllenorzesEnabled = FunctionRights.GetFunkcioJog(Page, "TeljessegEllenorzes");

        ListHeader1.SztornoEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSztorno");

        ListHeader1.ElintezetteNyilvanitasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratElintezes");

        ListHeader1.ElintezetteNyilvanitasTomegesEnabled = ListHeader1.ElintezetteNyilvanitasEnabled;

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Lock);

        ListHeader1.FelfuggesztesEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratFelfuggesztes");
        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            //if (String.IsNullOrEmpty(MasterListSelectedRowId))
            //{
            //    ActiveTabClear();
            //}
            //ActiveTabRefreshOnClientClicks();
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(UgyUgyiratokGridView);

        var visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState().LoadFromGrid(UgyiratokGridView, new Contentum.eRecord.BaseUtility.GridViewColumns.UgyiratokListColumnNames().GetType()).ToCustomString();
        Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.UgyUgyiratokSSRS] = visibilityState;

        if (Startup == Constants.Startup.Skontro)
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('UgyUgyiratokSSRSSkontro.aspx')";
        }
        else
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('UgyUgyiratokSSRS.aspx')";
        }

        if (Startup == Constants.Startup.Skontro)
        {
            ListHeader1.KolcsonzesEnabled = ListHeader1.KolcsonzesVisszaEnabled = FunctionRights.GetFunkcioJog(Page, "KikeresSkontroIrattarbol");
            ListHeader1.KolcsonzesKiadasEnabled = FunctionRights.GetFunkcioJog(Page, "KiadasSkontroIrattarbol");
        }
        ListHeader1.UgyintIdoUjraSzamolasEnabled = ListHeader1.UgyintIdoUjraSzamolasVisible = FunctionRights.GetFunkcioJog(Page, CommandName.UgyiratUgyintezesiIdoUjraSzamolas);
        ListHeader1.TomegesOlvasasiJogEnabled = FunctionRights.GetFunkcioJog(Page, "TomegesOlvasasiJog");
    }

    #endregion

    #region Master List

    public string GetDefaultSortExpression()
    {
        // BUG_6459
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.LISTA_RENDEZETTSEG_IKTATOSZAM, false))
        {
            return Search.FullSortExpression + "EREC_IraIktatokonyvek.Ev DESC, EREC_IraIktatokonyvek.Iktatohely, EREC_UgyUgyiratok.Foszam DESC";
        }
        else
        {
            return "EREC_UgyUgyiratok.LetrehozasIdo";
        }
    }

    public void UgyUgyiratokGridViewBind()
    {
        //String sortExpression = Search.GetSortExpressionFromViewState("UgyUgyiratokGridView", ViewState, "EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam");
        // Létrehozás ideje szerint rendezünk alapból
        String sortExpression = Search.GetSortExpressionFromViewState("UgyUgyiratokGridView", ViewState, this.GetDefaultSortExpression());
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("UgyUgyiratokGridView", ViewState, SortDirection.Descending);

        UgyUgyiratokGridViewBind(sortExpression, sortDirection);
    }

    protected void UgyUgyiratokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        if (!this.IsActive)
            return;

        // BUG_8359
        if (SortExpression == Contentum.eUtility.Search.FullSortExpression)
        {
            SortExpression = this.GetDefaultSortExpression();
            SortDirection = SortDirection.Descending;
        }


        DokumentumVizualizerComponent.ClearDokumentElements();

        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        // BUG_12682
        service.Timeout = 1800000;
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_UgyUgyiratokSearch search = null;
        if (Startup == Constants.Startup.Skontro)
        {
            search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true), Constants.CustomSearchObjectSessionNames.SkontroSearch);
        }
        else
        {
            search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject(Page, new EREC_UgyUgyiratokSearch(true));
        }

        // Van-e szûrés kérés paraméterben?
        // (Csak elsõ kéréskor figyeljük)
        if (!IsPostBack)
        {

            if (Startup == Constants.Startup.FromVezetoiPanel_Ugyirat) // ha vezetoi panelról indítottuk
            {
                //search = new EREC_UgyUgyiratokSearch(true);
                //Ugyiratok.FilterObject filterObject = new Ugyiratok.FilterObject();
                //filterObject.QsDeserialize(Page.Request.QueryString);
                //filterObject.SetSearchObject(ExecParam, ref search);

                //Search.SetSearchObject(Page, search);
                Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch);
            }
            else
            {
                if (Startup == Constants.Startup.FromFeladataim) // ha feladataim panelról indítottuk
                {
                    // új search objektumot hozunk létre, és sessionbe tesszük
                    search = new EREC_UgyUgyiratokSearch(true);

                    // Állapotszûrés
                    //('52','70' -- Irattározás jóváhagyás, Skontróba helyezés jóváhagyás
                    //  ,'0','03','04','06','07','09','11','13','54','50','57','60','99')
                    search.Manual_Allapot_Filter.Value = "'" + KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.SkontrobaHelyezesJovahagyasa
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Szignalt
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Iktatott
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Skontroban
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Szerelt
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Elintezett + "'";
                    search.Manual_Allapot_Filter.Operator = Contentum.eQuery.Query.Operators.inner;

                    Search.SetSearchObject(Page, search);
                }

                string filter = Request.QueryString.Get(QueryStringVars.Filter);
                if (filter == Constants.Sajat)
                {
                    search.Manual_Sajat.Value =
                         Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page));
                    search.Manual_Sajat.Operator = Contentum.eQuery.Query.Operators.equals;

                    // Nem kell:
                    // (Iktatásra elõkészített, Irattárban õrzött,
                    // Jegyzékre helyezett, Lezárt jegyzékben lévõ, Selejtezett,
                    // Levéltárba adott, Irattárból elkért, Egedélyezett kikérõn lévõ, Sztornózott)
                    string NotInAllapotok = "'" + KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Jegyzekre_helyezett
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Selejtezett
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.LeveltarbaAdott
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo
                         + "','" + KodTarak.UGYIRAT_ALLAPOT.Sztornozott + "'";

                    search.Manual_Sajat_Allapot.Value = NotInAllapotok;
                    search.Manual_Sajat_Allapot.Operator = Contentum.eQuery.Query.Operators.notinner;

                    search.Manual_Sajat_TovabbitasAlattAllapot.Value = NotInAllapotok;
                    search.Manual_Sajat_TovabbitasAlattAllapot.Operator = Contentum.eQuery.Query.Operators.isnullornotinner;

                    search.ReadableWhere = "Saját ";

                    if (Search.IsSearchObjectInSession(Page, typeof(EREC_UgyUgyiratokSearch)) == false)
                    {
                        Search.SetSearchObject(Page, search);
                    }
                }
                else if (filter == Constants.JovahagyandokFilter.Jovahagyandok)
                {
                    Ugyiratok.SetSearchObjectTo_Jovahagyandok(search, Page);

                    if (Search.IsSearchObjectInSession(Page, typeof(EREC_UgyUgyiratokSearch)) == false)
                    {
                        Search.SetSearchObject(Page, search);
                    }
                }
                else if (filter == Constants.JovahagyandokFilter.Irattarozasra)
                {
                    Ugyiratok.SetSearchObjectTo_JovahagyandokIrattarozasra(search, Page);

                    if (Search.IsSearchObjectInSession(Page, typeof(EREC_UgyUgyiratokSearch)) == false)
                    {
                        Search.SetSearchObject(Page, search);
                    }
                }
                else if (filter == Constants.JovahagyandokFilter.SkontrobaHelyezes)
                {
                    Ugyiratok.SetSearchObjectTo_JovahagyandokSkontrobaHelyezes(search, Page);

                    if (Search.IsSearchObjectInSession(Page, typeof(EREC_UgyUgyiratokSearch)) == false)
                    {
                        Search.SetSearchObject(Page, search);
                    }
                }
                else if (filter == Constants.JovahagyandokFilter.Elintezesre)
                {
                    Ugyiratok.SetSearchObjectTo_JovahagyandokElintezesre(search, Page);

                    if (Search.IsSearchObjectInSession(Page, typeof(EREC_UgyUgyiratokSearch)) == false)
                    {
                        Search.SetSearchObject(Page, search);
                    }
                }

            }
        }

        // CR3289 Vezetõi panel kezelés
        if (Startup == Constants.Startup.FromVezetoiPanel_Ugyirat)
        {
            // Keresési objektum kivétele a sessionbõl; ha nincs benne, a default szûréssel beletesszük
            if (!Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch))
            {
                search = (EREC_UgyUgyiratokSearch)Search.CreateSearchObjectWithDefaultFilter(typeof(EREC_UgyUgyiratokSearch), Page);

                Ugyiratok.FilterObject filterObject = new Ugyiratok.FilterObject();
                filterObject.QsDeserialize(Page.Request.QueryString);
                filterObject.SetSearchObject(ExecParam, ref search);

                if (!Search.IsDefaultSearchObject(typeof(EREC_UgyUgyiratokSearch), search))
                {
                    // sessionbe mentés
                    Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch);

                    //                        Search.SetSearchObject(Page, search);
                }
            }
            else
            {
                search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true), Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch);
                Ugyiratok.FilterObject filterObject = new Ugyiratok.FilterObject();
                filterObject.QsDeserialize(Page.Request.QueryString);
                filterObject.SetSearchObject(ExecParam, ref search);
                Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch);
            }

            //Search.SetSearchObject(Page, search);

        }
        // Keresési objektum kivétele a sessionbõl; ha nincs benne, a default szûréssel beletesszük
        // (Skontró módnál nem kell)
        else if (!Search.IsSearchObjectInSession(Page, typeof(EREC_UgyUgyiratokSearch)) && Startup != Constants.Startup.Skontro)
        {
            search = (EREC_UgyUgyiratokSearch)Search.CreateSearchObjectWithDefaultFilter(typeof(EREC_UgyUgyiratokSearch), Page);
            Search.SetSearchObject(Page, search);
        }

        // skontró mód figyelése, szûrés ha kell
        if (Startup == Constants.Startup.Skontro)
        {
            Ugyiratok.SetSearchObjectTo_Skontroban(search);
        }
        else
        {
            /// Alap szûrési feltétel
            Ugyiratok.SetDefaultFilter(search);
        }

        search.OrderBy = Search.GetOrderBy("UgyUgyiratokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        //LZS - BUG_8844
        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LISTA_ELEMSZAM_MAX);



        // Lapozás beállítása:
        UI.SetPaging(ExecParam, ListHeader1);
        //SetPaging(ExecParam, ListHeader1);

        if (!string.IsNullOrEmpty(this.AttachedDosszieTreeView.SelectedId) && this.AttachedDosszieTreeView.FilterAttachedList)
        {
            search.Extended_KRT_MappakSearch = new KRT_MappakSearch();

            search.Extended_KRT_MappakSearch.Id.Value = this.AttachedDosszieTreeView.SelectedId;
            search.Extended_KRT_MappakSearch.Id.Operator = Query.Operators.equals;
        }

        // BUG_8847
        Result res;
        if (Startup == Constants.Startup.FromVezetoiPanel_Ugyirat)
        {
            res = service.GetAllWithExtensionAndJogosultak(ExecParam, search, false);

            // AddMetaColumnsToGridView(res, IraIratokGridView, true);
        }
        else
            res = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);
        //Result res = service.GetAllWithExtension(ExecParam, search);

        if (!string.IsNullOrEmpty(this.AttachedDosszieTreeView.SelectedId) && this.AttachedDosszieTreeView.FilterAttachedList)
        {
            search.Extended_KRT_MappakSearch = null;
        }

        UI.GridViewFill(UgyUgyiratokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
        //GridViewFill(UgyUgyiratokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView_RowDataBound_SetSzerelesInfo(e);
        GridView_RowDataBound_SetCsatolasInfo(e);
        GridView_RowDataBound_SetVegyesInfo(e);
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        UI.SetFeladatInfo(e, Constants.TableNames.EREC_UgyUgyiratok);
    }

    private void GridView_RowDataBound_SetVegyesInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
        {
            return;
        }

        System.Data.DataRowView dataRowView = (System.Data.DataRowView)e.Row.DataItem;
        string csatolmanyCount = String.Empty;

        if (dataRowView.Row.Table.Columns.Contains("CsatolmanyCount") && dataRowView["CsatolmanyCount"] != null)
        {
            csatolmanyCount = dataRowView["CsatolmanyCount"].ToString();
        }
        ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");

        if (string.IsNullOrEmpty(csatolmanyCount))
        {
            if (CsatolmanyImage != null)
            {
                CsatolmanyImage.OnClientClick = "";
                CsatolmanyImage.Visible = false;
            }
            return;
        }
        if (CsatolmanyImage == null)
        {
            CsatolmanyImage.Visible = false;
            return;
        }

        CsatolmanyImage.OnClientClick = "";
        int count = 0;
        Int32.TryParse(csatolmanyCount, out count);
        if (count == 0)
        {
            CsatolmanyImage.Visible = false;
            return;
        }
        //tárolt eljárás vizsgálja
        //#region BLG_577
        //if (!UtilityCsatolmanyok.CheckRights_CsatolmanyokByDokumentum(Page, drw["Dokumentum_Id"].ToString()))
        //{   /*NINCS JOGA*/
        //    CsatolmanyImage.Visible = false;
        //    return;
        //}
        //#endregion

        CsatolmanyImage.Visible = true;

        string iratHelye = dataRowView.Row["FelhasznaloCsoport_Id_Orzo"] == null ? null : dataRowView.Row["FelhasznaloCsoport_Id_Orzo"].ToString();
        if (!Contentum.eRecord.BaseUtility.UIJogosultsagok.CanUserViewAttachements(Page, iratHelye))
        {
            CsatolmanyImage.ToolTip = Resources.Form.UI_CsatolmanyImage_ToolTip_UserHasNoRight;
            CsatolmanyImage.Enabled = false;
            CsatolmanyImage.OnClientClick = "";
            return;
        }

        CsatolmanyImage.OnClientClick = "alert('" + Resources.Form.UI_NavigationError_UndefinedObject + "');";
        CsatolmanyImage.AlternateText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
        CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);

        if (count == 1 && dataRowView.Row.Table.Columns.Contains("Dokumentum_Id") && !String.IsNullOrEmpty(dataRowView["Dokumentum_Id"].ToString()))
        {
            CsatolmanyImage.OnClientClick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", dataRowView["Dokumentum_Id"]);
            DokumentumVizualizerComponent.AddDokumentElement(CsatolmanyImage.ClientID, dataRowView["Dokumentum_Id"].ToString());
        }
        else
        {
            // set "link"
            string id = "";

            if (dataRowView["Id"] != null)
            {
                id = dataRowView["Id"].ToString();
            }

            string Azonosito = ""; // iktatószám, a ReadableWhere fogja használni a dokumentumok listáján
            if (dataRowView.Row.Table.Columns.Contains("Foszam_Merge") && dataRowView["Foszam_Merge"] != null)
            {
                Azonosito = dataRowView["Foszam_Merge"].ToString();
            }

            if (!String.IsNullOrEmpty(id))
            {
                CsatolmanyImage.OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.CsatolmanyokTab
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
        }





    }

    private void GridView_RowDataBound_SetCsatolasInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolsaCount = String.Empty;

            if (drw["CsatolasCount"] != null)
            {
                csatolsaCount = drw["CsatolasCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolsaCount))
            {
                Image CsatoltImage = (Image)e.Row.FindControl("CsatoltImage");

                if (CsatoltImage != null)
                {
                    int count = 0;
                    Int32.TryParse(csatolsaCount, out count);
                    if (count > 0)
                    {
                        CsatoltImage.Visible = true;
                        CsatoltImage.ToolTip = String.Format("Csatolás: {0} darab", csatolsaCount);
                        if (drw["Id"] != null)
                        {
                            string ugyiratId = drw["Id"].ToString();
                            string onclick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                            , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + ugyiratId +
                            "&" + QueryStringVars.SelectedTab + "=" + "Csatolas"
                             , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID);
                            CsatoltImage.Attributes.Add("onclick", onclick);

                        }
                    }
                    else
                    {
                        CsatoltImage.Visible = false;
                    }
                }

            }
            else
            {
                Image CsatoltImage = (Image)e.Row.FindControl("CsatoltImage");
                if (CsatoltImage != null)
                {
                    CsatoltImage.Visible = false;
                }

            }
        }
    }

    private void GridView_RowDataBound_SetSzerelesInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string szereltCount = String.Empty;

            if (drw["SzereltCount"] != null)
            {
                szereltCount = drw["SzereltCount"].ToString();
            }

            string elokeszitettSzerelesCount_str = String.Empty;
            if (drw["ElokeszitettSzerelesCount"] != null)
            {
                elokeszitettSzerelesCount_str = drw["ElokeszitettSzerelesCount"].ToString();
            }

            /// ha van szerelésre elõkészített ügyirat (UgyUgyiratId_Szulo ki van töltve erre, de nem szerelt még az állapota),
            /// akkor a piros szerelt ikont rakjuk ki, amúgy a feketét
            /// 
            int elokeszitettSzerelesCount = 0;
            if (!String.IsNullOrEmpty(elokeszitettSzerelesCount_str))
            {
                Int32.TryParse(elokeszitettSzerelesCount_str, out elokeszitettSzerelesCount);
            }

            if (elokeszitettSzerelesCount > 0)
            {
                Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");

                if (SzereltImage != null)
                {
                    SzereltImage.Visible = true;
                    SzereltImage.ImageUrl = "~/images/hu/ikon/szerelt_piros.gif";
                    SzereltImage.ToolTip = "Elõkészített szerelés";
                    SzereltImage.AlternateText = SzereltImage.ToolTip;
                }
            }
            else if (!String.IsNullOrEmpty(szereltCount))
            {
                Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");

                if (SzereltImage != null)
                {
                    int count = 0;
                    Int32.TryParse(szereltCount, out count);
                    if (count > 0)
                    {
                        SzereltImage.Visible = true;
                    }
                    else
                    {
                        SzereltImage.Visible = false;
                    }
                }

            }
            else
            {
                Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");
                if (SzereltImage != null)
                {
                    SzereltImage.Visible = false;
                }

            }
        }
    }

    protected void UgyUgyiratokGridView_PreRender(object sender, EventArgs e)
    {
        //int prev_PageIndex = UgyUgyiratokGridView.PageIndex;

        //UgyUgyiratokGridView.PageIndex = ListHeader1.PageIndex;
        //ListHeader1.PageCount = UgyUgyiratokGridView.PageCount;

        //if (prev_PageIndex != UgyUgyiratokGridView.PageIndex)
        //{
        //    //scroll állapotának törlése
        //    JavaScripts.ResetScroll(Page, UgyUgyiratokCPE);
        //    UgyUgyiratokGridViewBind();
        //    //ActiveTabClear();
        //}
        //else
        //{
        UI.GridViewSetScrollable(ListHeader1.Scrollable, UgyUgyiratokCPE);
        //}

        //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(UgyUgyiratokGridView);
        //ListHeader1.PagerLabel = ListHeader1.PageIndex.ToString() + "/";

        ListHeader1.RefreshPagerLabel();

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        UgyUgyiratokGridViewBind();
        //ActiveTabClear();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, UgyUgyiratokCPE);
        UgyUgyiratokGridViewBind();
    }

    protected void UgyUgyiratokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(UgyUgyiratokGridView, selectedRowNumber, "check");

            //string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            //ActiveTabRefresh(TabContainer1, id);

        }
    }


    private bool SkontrobolKivetelVisible(Ugyiratok.Statusz ugyiratStatusz)
    {
        bool ret = ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontroban
             || ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert
             || ugyiratStatusz.TovabbitasAlattiAllapot == KodTarak.UGYIRAT_ALLAPOT.Skontroban
             || ugyiratStatusz.TovabbitasAlattiAllapot == KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert;

        return ret;
    }


    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                     , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                     , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID);
            string tableName = "EREC_UgyUgyiratok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                 , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                 , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, UgyUgyiratokUpdatePanel.ClientID);

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = id;

            EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            Result result_ugyiratGet = service_ugyiratok.Get(execParam);
            if (result_ugyiratGet.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ugyiratGet);
                ErrorUpdatePanel.Update();
            }

            EREC_UgyUgyiratok ugyiratObj = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

            ListHeader1.PrintOnClientClick = "javascript:window.open('" + GetPrintUrl(ugyiratObj) + "')";

            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(ugyiratObj);
            ErrorDetails errorDetail;

            // Módosítás
            if (!Ugyiratok.Modosithato(ugyiratStatusz, execParam, out errorDetail))
            {
                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_UgyiratMegtekintes
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "')) {" + JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                + "} else return false;";

                // ügyirattérkép View módban
                ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                    + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                     , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                     , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

                // ügyirattérkép Modify módban
                ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID);

            }

            // Szignálás
            if (Ugyiratok.Szignalhato(ugyiratStatusz, execParam, out errorDetail))
            {
                ListHeader1.SzignalasOnClientClick = JavaScripts.SetOnClientClick("SzignalasForm.aspx",
                     QueryStringVars.UgyiratId + "=" + id
                     , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.SzignalasOnClientClick = "alert('" + Resources.Error.ErrorCode_52152
                     + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                     + "'); return false;";
            }

            /*// Átvétel ügyintézésre
			if (Ugyiratok.AtvehetoUgyintezesre(ugyiratStatusz, execParam, out errorDetail))
			{
				 ListHeader1.AtvetelUgyintezesreOnClientClick = JavaScripts.SetOnClientClick("AtvetelUgyintezesreForm.aspx",
					  QueryStringVars.UgyiratId + "=" + id
					  , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
			}
			else
			{
				 ListHeader1.AtvetelUgyintezesreOnClientClick = "alert('" + Resources.Error.ErrorCode_52161
					  + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
					  + "'); return false;";
			}*/

            //Központi Irattarba kuldes
            // BUG_6460
            //if (ugyiratStatusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
            //{
            //    if (Ugyiratok.IrattarbaKuldheto(ugyiratStatusz, execParam, out errorDetail))
            //    {
            //        if (ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott
            //                || ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott)
            //        {
            //            ListHeader1.IrattarozasraOnClientClick = "if (confirm('"
            //            + Resources.Question.UIConfirmHeader_KolcsonzesVissza
            //            + "')) { } else { return false; }";
            //        }
            //        else
            //            //ListHeader1.IrattarozasraOnClientClick = "";
            //            ListHeader1.IrattarozasraOnClientClick = JavaScripts.SetOnClientClick("IrattarbaAdasForm.aspx",
            //                    QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id +
            //                    "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromUgyirat
            //                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            //    }
            //    else
            //    {
            //        //ListHeader1.IrattarozasraOnClientClick = "alert('" + Resources.Error.ErrorCode_52202 + "'); return false;";
            //        ListHeader1.IrattarozasraOnClientClick = "alert('" + Resources.Error.ErrorCode_52209
            //                + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
            //    }
            //}
            //else
            //{
            //    ListHeader1.IrattarozasraToolTip = Resources.Buttons.Irattarozas_IrattarozasJovahagyasToolTip;
            //    if (Ugyiratok.IrattarbaKuldesJovahagyhato(ugyiratStatusz, execParam, out errorDetail))
            //    {
            //        ListHeader1.IrattarozasraOnClientClick = JavaScripts.SetOnClientClick("IrattarbaAdasForm.aspx",
            //                QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id +
            //                    "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromUgyirat +
            //                    "&" + QueryStringVars.Mode + "=" + CommandName.UgyiratIrattarozasJovahagyasa
            //                , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            //    }
            //    else
            //    {
            //        ListHeader1.IrattarozasraOnClientClick = "alert('" + Resources.Error.ErrorCode_52208 +
            //                ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
            //    }
            //}
            // A skontróba helyezés/kivétel közül egyszerre mindig csak egy látszódjon:
            if (SkontrobolKivetelVisible(ugyiratStatusz))
            {
                ListHeader1.SkontrobaHelyezVisible = false;
                ListHeader1.SkontrobolKivetelVisible = true;
            }
            else
            {
                ListHeader1.SkontrobaHelyezVisible = true;
                ListHeader1.SkontrobolKivetelVisible = false;
            }

            // Skontróba helyezés
            if (ugyiratStatusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.SkontrobaHelyezesJovahagyasa)
            {
                if (Ugyiratok.SkontrobaHelyezheto(ugyiratStatusz, execParam, out errorDetail))
                {
                    ListHeader1.SkontrobaHelyezOnClientClick = JavaScripts.SetOnClientClick("SkontroInditasForm.aspx",
                         QueryStringVars.UgyiratId + "=" + id
                         , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                }
                else
                {
                    ListHeader1.SkontrobaHelyezOnClientClick = "alert('" + Resources.Error.ErrorCode_52171
                         + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                         + "'); return false;";
                }
            }
            else
            {
                if (Ugyiratok.SkontroVisszavonhato(ugyiratStatusz, execParam, out errorDetail))
                {
                    ListHeader1.SkontrobaHelyezToolTip = Resources.Buttons.Skontro_SkontrobaHelyezesVisszavonasToolTip;
                    ListHeader1.SkontrobaHelyezOnClientClick = JavaScripts.SetOnClientClick("SkontroInditasForm.aspx",
                              QueryStringVars.UgyiratId + "=" + id + "&" + QueryStringVars.Command + "=" + CommandName.UgyiratSkontrobaHelyezesVisszavonasa
                              , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                }
                else
                {
                    ListHeader1.SkontrobaHelyezToolTip = Resources.Buttons.Skontro_SkontrobaHelyezesJovahagyasToolTip;
                    if (Ugyiratok.SkontroJovahagyhato(ugyiratStatusz, execParam, out errorDetail))
                    {
                        ListHeader1.SkontrobaHelyezOnClientClick = JavaScripts.SetOnClientClick("SkontroInditasForm.aspx",
                             QueryStringVars.UgyiratId + "=" + id + "&" + QueryStringVars.Command + "=" + CommandName.UgyiratSkontrobaHelyezesJovahagyasa
                             , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                    }
                    else
                    {
                        ListHeader1.SkontrobaHelyezOnClientClick = "alert('" + Resources.Error.ErrorCode_52175 +
                             ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
                    }
                }
            }

            // Skontróból kivétel tömeges lett:
            //// Skontróból kivétel
            //if (ugyiratStatusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.Skontroban)
            //{               
            //    ListHeader1.SkontrobolKivetelOnClientClick = "alert('" + Resources.Error.ErrorCode_52182 + "'); return false;";
            //}
            //else if (Ugyiratok.SkontrobolKiveheto(ugyiratStatusz, execParam))
            //{
            //    //ListHeader1.SkontrobolKivetelOnClientClick = "if (confirm('"
            //    //    + Resources.Question.UIConfirmHeader_SkontrobolKivesz
            //    //    + "')) { } else { return false; }";

            //    ListHeader1.SkontrobolKivetelOnClientClick = JavaScripts.SetOnClientClick("SkontroInditasForm.aspx",
            //        QueryStringVars.UgyiratId + "=" + id + "&" + QueryStringVars.Mode + "=" + CommandName.SkontrobolKivesz
            //        , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            //}
            //else
            //{
            //    ListHeader1.SkontrobolKivetelOnClientClick = "alert('" + Resources.Error.ErrorCode_52181 + "'); return false;";
            //}

            // Sztornózás:
            if (ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Sztornozott)
            {
                // Az ügyirat már sztornózva van!
                ListHeader1.SztornoOnClientClick = "alert('" + Resources.Error.ErrorCode_52195 + "'); return false;";
            }
            else if (Ugyiratok.Sztornozhato(ugyiratStatusz, execParam, out errorDetail))
            {
                SztornoPopup.SetOnclientClickShowFunction(ListHeader1.SztornoButton);
            }
            else
            {
                // Nem sztornózható:
                ListHeader1.SztornoOnClientClick = "alert('" + Resources.Error.ErrorCode_52192
                     + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                     + "'); return false;";
            }

            //// Átadásra kijelölés:
            //if (Ugyiratok.AtadasraKijelolheto(ugyiratStatusz, execParam))
            //{
            //    ListHeader1.AtadasraKijelolOnClientClick = JavaScripts.SetOnClientClick("AtadasForm.aspx",
            //        QueryStringVars.UgyiratId + "=" + id
            //        , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            //}
            //else
            //{
            //    ListHeader1.AtadasraKijelolOnClientClick = "alert('" + Resources.Error.ErrorCode_52201 + "'); return false;";
            //}

            // Átvétel:
            if (Ugyiratok.AtvehetoAllapotu(ugyiratStatusz, out errorDetail))
            {
                ListHeader1.AtvetelOnClientClick = "if (confirm('"
                     + Resources.Question.UIConfirmHeader_Atvetel
                     + "')) { } else { return false; }";
            }
            else
            {
                // Nem vehetõ át:
                ListHeader1.AtvetelOnClientClick = "alert('" + Resources.Error.ErrorCode_52232
                     + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
            }

            // az esetleg felesleges hívások elkerülése miatt nem végezzük el a teljes ellenõrzést
            //if (Ugyiratok.CheckVisszakuldhetoWithFunctionRight(Page, statusz, out errorDetail))
            if (Ugyiratok.Visszakuldheto(execParam, ugyiratStatusz, out errorDetail))
            {
                VisszakuldesPopup.SetOnclientClickShowFunction(ListHeader1.VisszakuldesButton);
            }
            else
            {
                // Nem küldhetõ vissza:
                ListHeader1.VisszakuldesOnClientClick = "alert('" + Resources.Error.UINemVisszakuldhetoTetel
                     + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                     + "'); return false;";
            }

            // Lezárás:
            if (!TomegesLezarasEnabled)
            {
                if (Ugyiratok.Lezarhato(ugyiratStatusz, execParam, out errorDetail))
                {
                    ListHeader1.LezarasOnClientClick = JavaScripts.SetOnClientClick("LezarasForm.aspx",
                         QueryStringVars.UgyiratId + "=" + id
                         , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                }
                else
                {
                    // Nem zárható le:
                    ListHeader1.LezarasOnClientClick = "alert('" + Resources.Error.ErrorCode_52261
                         + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                         + "'); return false;";
                }
            }

            ErrorDetails errorDetail_elokeszitettSzereles = null;
            // Szerelés:
            // Ha szerelhetõ bele ügyirat VAGY ha elõkészített szerelés van rá
            if (Ugyiratok.SzerelhetoBeleUgyirat(ugyiratStatusz, execParam, out errorDetail)
                 || Ugyiratok.ElokeszitettSzerelesVegrehajthato(ugyiratStatusz, ugyiratObj.UgyUgyirat_Id_Szulo, execParam, out errorDetail_elokeszitettSzereles))
            {
                ListHeader1.SzerelesOnClientClick = JavaScripts.SetOnClientClick("SzerelesForm.aspx",
                     QueryStringVars.UgyiratId + "=" + id
                     , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                string errorDetailMsg = String.Empty;
                if (errorDetail != null && !errorDetail.IsEmpty)
                {
                    errorDetailMsg = ResultError.GetErrorDetailMsgForAlert(errorDetail, Page);
                }
                else
                {
                    errorDetailMsg = ResultError.GetErrorDetailMsgForAlert(errorDetail_elokeszitettSzereles, Page);
                }
                // Nem szerelhetõ bele másik ügyirat:
                ListHeader1.SzerelesOnClientClick = "alert('" + Resources.Error.ErrorCode_52271 + errorDetailMsg + "'); return false;";
            }

            // Csatolás:
            if (Ugyiratok.Csatolhato(ugyiratStatusz, out errorDetail) == true)
            {
                ListHeader1.CsatolasOnClientClick = JavaScripts.SetOnClientClick("CsatolasForm.aspx",
                     QueryStringVars.Id + "=" + id + "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromUgyirat
                     , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.CsatolasOnClientClick = "alert('" + Resources.Error.ErrorCode_55010
                     + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                     + "'); return false;";
            }

            // Elintézetté nyilvánítás
            if (ugyiratStatusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa)
            {
                ListHeader1.ElintezetteNyilvanitas.ToolTip = Resources.Buttons.ElintezetteNyilvanitas;
                ListHeader1.ElintezetteNyilvanitas.AlternateText = Resources.Buttons.ElintezetteNyilvanitas;
                ListHeader1.ElintezetteNyilvanitas.CommandName = CommandName.ElintezetteNyilvanitas;
                if (Ugyiratok.ElintezetteNyilvanithato(ugyiratStatusz, execParam, out errorDetail))
                {
                    ListHeader1.ElintezetteNyilvanitasOnClientClick = JavaScripts.SetOnClientClick("UgyiratElintezettForm.aspx"
                         , QueryStringVars.Id + "=" + id
                         , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                }
                else
                {
                    ListHeader1.ElintezetteNyilvanitasOnClientClick = "alert('" + Resources.Error.ErrorCode_52242
                         + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                         + "'); return false;";
                }
            }
            else
            {
                ListHeader1.ElintezetteNyilvanitas.ToolTip = Resources.Buttons.ElintezetteNyilvanitasJovahagyasa;
                ListHeader1.ElintezetteNyilvanitas.AlternateText = Resources.Buttons.ElintezetteNyilvanitasJovahagyasa;
                ListHeader1.ElintezetteNyilvanitas.CommandName = CommandName.ElintezetteNyilvanitasJovahagyasa;
                if (Ugyiratok.ElintezetteNyilvanitasJovahagyhato(ugyiratStatusz, execParam, out errorDetail))
                {
                    ListHeader1.ElintezetteNyilvanitasOnClientClick = JavaScripts.SetOnClientClick("UgyiratElintezettForm.aspx"
                         , QueryStringVars.Id + "=" + id
                         , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                }
                else
                {
                    ListHeader1.ElintezetteNyilvanitasOnClientClick = "alert('" + Resources.Error.ErrorCode_52245 +
                         ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
                }
            }

            if (Ugyiratok.TeljessegEllenorizheto(ugyiratStatusz, execParam, out errorDetail))
            {
                ListHeader1.TeljessegEllenorzesOnClientClick = String.Empty;
            }
            else
            {
                ListHeader1.TeljessegEllenorzesOnClientClick = "alert('" + Resources.Error.ErrorCode_58001 + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "');return false;";
            }

            if (ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt && RendszerParameterUgyIdoFelfuggesztesFancy == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.SIMPLE)
                ListHeader1.FelfuggesztesVisible = true;
            else
                ListHeader1.FelfuggesztesVisible = false;

            Ugyiratok.SetSkontroIrattarKikeresVisszaFunkciogomb(id, ugyiratStatusz, ListHeader1.KolcsonzesVissza, UgyUgyiratokUpdatePanel);

        }
    }

    protected void UgyUgyiratokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    UgyUgyiratokGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView));
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    deleteSelectedUgyUgyiratok();
        //    UgyUgyiratokGridViewBind();
        //}
        //bernat.laszlo added: Excel Export (Grid)
        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());

            List<double> columnSizeList = new List<double>() { 2.57, 4.71, 5.29, 2.8, 16.14, 13.57, 13.86, 14.86, 10.57, 22.29, 11, 13.3, 18 };
            ex_Export.SaveGridView_ToExcel(exParam, UgyUgyiratokGridView, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, columnSizeList, browser);
        }
        //bernat.laszlo eddig
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;
        String RecordId = UI.GetGridViewSelectedRecordId(UgyiratokGridView);

        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedIraUgyUgyiratRecords();
                UgyUgyiratokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedIraUgyUgyiratRecords();
                UgyUgyiratokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedUgyUgyiratok();
                break;
            //case CommandName.SkontrobolKivesz:
            //    Ugyiratok.SkontrobolKivesz(UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView), Page, EErrorPanel1, ErrorUpdatePanel);
            //    UgyUgyiratokGridViewBind();
            //    break;
            case CommandName.Sztorno:
                Ugyiratok.Sztornozas(UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView), SztornoPopup.SztornoIndoka, Page, EErrorPanel1, ErrorUpdatePanel);
                UgyUgyiratokGridViewBind();
                break;
            case CommandName.TeljessegEllenorzes:
                {
                    string ugyiratId = UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView);
                    string warningMsg;
                    TeljessegEllenorzesResultPanel1.Ellenorzes(ugyiratId, true, true, out warningMsg);
                    break;
                }
            case CommandName.DossziebaHelyez:
                sb = new System.Text.StringBuilder();
                foreach (string s in ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sb.Append(s + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                Session[Constants.SelectedUgyiratIds] = sb.ToString();

                Session[Constants.SelectedBarcodeIds] = null;
                Session[Constants.SelectedKuldemenyIds] = null;
                Session[Constants.SelectedIratPeldanyIds] = null;
                Session[Constants.SelectedDosszieIds] = null;

                js = JavaScripts.SetOnClientClickWithTimeout("DosszieKapcsolatokForm.aspx", CommandName.Command + "=" + CommandName.DossziebaHelyez + "&" + QueryStringVars.Id + "=" + this.AttachedDosszieTreeView.SelectedId, Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratDossziebaMozgatas", js, true);
                break;
            case CommandName.DossziebolKivesz:
                sb = new System.Text.StringBuilder();
                foreach (string s in ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sb.Append(s + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                Session[Constants.SelectedUgyiratIds] = sb.ToString();

                Session[Constants.SelectedBarcodeIds] = null;
                Session[Constants.SelectedKuldemenyIds] = null;
                Session[Constants.SelectedIratPeldanyIds] = null;
                Session[Constants.SelectedDosszieIds] = null;

                js = JavaScripts.SetOnClientClickWithTimeout("DosszieKapcsolatokForm.aspx", CommandName.Command + "=" + CommandName.DossziebolKivesz + "&" + QueryStringVars.Id + "=" + this.AttachedDosszieTreeView.SelectedId, Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratDossziebaMozgatas", js, true);
                break;
            case CommandName.Atvetel:
                {
                    bool mindigTomegesAtvetel = Rendszerparameterek.GetBoolean(Page, "TOMEGES_ATVETEL_ENABLED", false);

                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    // elvileg nem biztos, hogy a kiválasztott sor egyben a bepipált sor is
                    if (UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check") == 1
                         && !String.IsNullOrEmpty(RecordId)
                         && lstGridViewSelectedRows.Contains(RecordId) && !mindigTomegesAtvetel)
                    {
                        if (lstGridViewSelectedRows.Count > 0)
                        {
                            ErrorDetails errorDetail;
                            bool atveheto = Ugyiratok.CheckAtvehetoWithFunctionRight(Page, RecordId, out errorDetail);

                            if (!atveheto)
                            {
                                js = String.Format("alert('{0}{1}');", Resources.Error.ErrorCode_52232, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemVehetoAt", js, true);
                            }
                            else
                            {
                                Ugyiratok.Atvetel(RecordId, Page, EErrorPanel1, ErrorUpdatePanel);
                                UgyUgyiratokGridViewBind();
                            }
                        }
                    }
                    else
                    {
                        sb = new System.Text.StringBuilder();
                        foreach (string s in lstGridViewSelectedRows)
                        {
                            sb.Append(s + ",");
                        }
                        sb.Remove(sb.Length - 1, 1);
                        Session[Constants.SelectedUgyiratIds] = sb.ToString();

                        Session[Constants.SelectedBarcodeIds] = null;
                        Session[Constants.SelectedKuldemenyIds] = null;
                        Session[Constants.SelectedIratPeldanyIds] = null;
                        Session[Constants.SelectedDosszieIds] = null;

                        js = JavaScripts.SetOnClientClickWithTimeout("AtvetelForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratAtadas", js, true);
                    }
                }
                //Ugyiratok.Atvetel(UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView), Page, EErrorPanel1, ErrorUpdatePanel);
                //UgyUgyiratokGridViewBind();
                break;
            case CommandName.AtvetelUgyintezesre:
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    // elvileg nem biztos, hogy a kiválasztott sor egyben a bepipált sor is
                    /*   if (UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check") == 1
							 && !String.IsNullOrEmpty(RecordId)
							 && lstGridViewSelectedRows.Contains(RecordId))
						{
							 if (lstGridViewSelectedRows.Count > 0)
							 {
								  ErrorDetails errorDetail;
								  bool atveheto = Ugyiratok.CheckAtvehetoUgyintezesreWithFunctionRight(Page, RecordId, out errorDetail);

								  if (!atveheto)
								  {
										js = String.Format("alert('{0}{1}');", Resources.Error.ErrorCode_52161, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
										ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemVehetoAtUgyintezesre", js, true);
								  }
								  else
								  {
										Ugyiratok.AtvetelUgyintezesre(RecordId, Page, EErrorPanel1, ErrorUpdatePanel);
										UgyUgyiratokGridViewBind();
								  }
							 }
						}
						else*/
                    {
                        sb = new System.Text.StringBuilder();
                        foreach (string s in lstGridViewSelectedRows)
                        {
                            sb.Append(s + ",");
                        }
                        sb.Remove(sb.Length - 1, 1);
                        Session[Constants.SelectedUgyiratIds] = sb.ToString();

                        js = JavaScripts.SetOnClientClickWithTimeout("AtvetelUgyintezesreTomegesForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratAtvetelUgyintezesre", js, true);
                    }
                }
                break;
            case CommandName.Visszakuldes:
                {
                    ErrorDetails errorDetail;
                    bool visszakuldheto = Ugyiratok.CheckVisszakuldhetoWithFunctionRight(Page, RecordId, out errorDetail);

                    if (!visszakuldheto)
                    {
                        js = String.Format("alert('{0}{1}');", Resources.Error.UINemVisszakuldhetoTetel, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemKuldhetoVissza", js, true);
                    }
                    else
                    {
                        Ugyiratok.Visszakuldes(RecordId, VisszakuldesPopup.VisszakuldesIndoka, Page, EErrorPanel1, ErrorUpdatePanel);
                        UgyUgyiratokGridViewBind();
                    }
                }
                break;
            case CommandName.Irattarozasra:
                {
                    // BUG_6460
                    bool iskolcsonzes = false;
                    ErrorDetails errorDetails = null;
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);

                    //egyszeres kijelölés
                    //  if (!String.IsNullOrEmpty(RecordId))
                    if (!String.IsNullOrEmpty(RecordId) && (lstGridViewSelectedRows.Count == 1))

                    {
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        execParam.Record_Id = RecordId;

                        EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                        Result result_ugyiratGet = service_ugyiratok.Get(execParam);
                        if (result_ugyiratGet.IsError)
                        {
                            // hiba:
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ugyiratGet);
                            ErrorUpdatePanel.Update();
                        }

                        EREC_UgyUgyiratok ugyiratObj = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                        Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(ugyiratObj);
                        // BUG_8005
                        if (ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
                        {
                            if (Ugyiratok.IrattarbaKuldesJovahagyhato(ugyiratStatusz, execParam, out errorDetails))
                            {
                                if (ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott
                                        || ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott)
                                {
                                    iskolcsonzes = true;
                                }
                            }
                        }
                        else
                        //if (ugyiratStatusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
                        {
                            if (Ugyiratok.IrattarbaKuldheto(ugyiratStatusz, execParam, out errorDetails))
                            {
                                if (ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott
                                        || ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott)
                                {
                                    iskolcsonzes = true;
                                }
                            }
                        }
                    }
                    if (iskolcsonzes)
                    {
                        EREC_IrattariKikeroService kikeroService = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                        ExecParam kikeroExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                        kikeroExecParam.Record_Id = UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView);
                        Result kikeroResult = kikeroService.KolcsonzesVissza(kikeroExecParam);
                        if (!string.IsNullOrEmpty(kikeroResult.ErrorCode))
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, kikeroResult);
                            // BUG_8287
                            ErrorUpdatePanel.Update();
                        }
                        else
                            UgyUgyiratokGridViewBind();
                        break;

                    }
                    // tömeges
                    else
                    {
                        //ErrorDetails errorDetails = null;
                        //List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                        lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);

                        ExecParam ep = UI.SetExecParamDefault(Page, new ExecParam());
                        if (lstGridViewSelectedRows.Count == 0)
                        {
                            js = string.Format("alert('{0}')", Resources.List.UI_NoSelectedItem);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "HibaNincsKijelolve", js, true);
                            return;
                        }
                        // BUG_8005
                        errorDetails = null;
                        EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch(true);

                        //ExecParam ep = execParam.Clone();
                        ep.Record_Id = lstGridViewSelectedRows.ToArray()[0];
                        // Statusz statusz = new Statusz();

                        EREC_UgyUgyiratokService srv = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        Result res = srv.Get(ep);
                        if (res.IsError || res.Record == null)
                        {
                            //errorDetails = statusz.CreateErrorDetail("Ügyirat adatok lekérése sikertelen!");
                            //return false;
                        }
                        EREC_UgyUgyiratok ugyiratObj = res.Record as EREC_UgyUgyiratok;
                        //if (!String.IsNullOrEmpty(ugyiratObj.IraIrattariTetel_Id.ToString()))
                        //{
                        search.Id.Value = Search.GetSqlInnerString(lstGridViewSelectedRows.ToArray());
                        search.Id.Operator = Query.Operators.inner;

                        search.Allapot.Value = ugyiratObj.Allapot;
                        search.Allapot.Operator = Query.Operators.notequals;
                        search.TopRow = 1;

                        res = srv.GetAll(ep, search);
                        if (res.IsError || res.Ds == null)
                        {
                        }

                        if (res.Ds.Tables[0].Rows.Count > 0)
                        {
                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "A kijelölt ügyiratok között eltérnek a státuszok, csak azonos státuszú ugyiratokat lehet együtt kezelni!");
                            ErrorUpdatePanel.Update();
                            return;

                        }
                        bool ok = false;
                        string queryString = Constants.Startup.StartupName + "=" + Constants.Startup.FromUgyirat;
                        if (ugyiratObj.Allapot == KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
                        {
                            ok = Ugyiratok.IrattarbaKuldesJovahagyhatoTomeges(ep, lstGridViewSelectedRows.ToArray(), out errorDetails);
                            queryString += "&Mode=" + CommandName.UgyiratIrattarozasJovahagyasa;
                        }
                        else
                            ok = Ugyiratok.IrattarbaKuldhetoTomeges(ep, lstGridViewSelectedRows.ToArray(), out errorDetails);
                        if (errorDetails != null || !ok)
                        {
                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", errorDetails.Message);
                            ErrorUpdatePanel.Update();
                            return;
                        }

                        sb = new System.Text.StringBuilder();

                        foreach (string s in lstGridViewSelectedRows)
                        {
                            sb.Append(s + ",");
                        }
                        sb.Remove(sb.Length - 1, 1);
                        Session[Constants.SelectedUgyiratIds] = sb.ToString();

                        js = JavaScripts.SetOnClientClickWithTimeout("IrattarbaAdasTomegesForm.aspx", queryString, Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemAdhatoAt", js, true);
                    }
                }
                break;
            case CommandName.LezarasVisszavonas:
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    sb = new System.Text.StringBuilder();
                    foreach (string s in lstGridViewSelectedRows)
                    {
                        sb.Append(s + ",");
                    }
                    sb.Remove(sb.Length - 1, 1);
                    Session[Constants.SelectedUgyiratIds] = sb.ToString();

                    js = JavaScripts.SetOnClientClickWithTimeout("LezarasVisszavonasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "LezarasVisszavonas", js, true);
                }
                break;
            case CommandName.AtadasraKijeloles:
                sb = new System.Text.StringBuilder();
                foreach (string s in ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sb.Append(s + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                Session[Constants.SelectedUgyiratIds] = sb.ToString();

                Session[Constants.SelectedKuldemenyIds] = null;
                Session[Constants.SelectedBarcodeIds] = null;
                Session[Constants.SelectedIratPeldanyIds] = null;
                Session[Constants.SelectedDosszieIds] = null;

                js = JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratokAtadas", js, true);
                break;

            case CommandName.SkontrobolKivesz:
                sb = new System.Text.StringBuilder();
                foreach (string s in ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sb.Append(s + ",");
                }
                if (sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }
                Session[Constants.SelectedUgyiratIds] = sb.ToString();
                //Session[Constants.SelectedKuldemenyIds] = null;
                //Session[Constants.SelectedBarcodeIds] = null;
                //Session[Constants.SelectedIratPeldanyIds] = null;

                js = JavaScripts.SetOnClientClickWithTimeout("SkontroInditasForm.aspx", QueryStringVars.Mode + "=" + CommandName.SkontrobolKivesz
                     , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray()); ;

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratokSkontrobolKi", js, true);
                break;

            case CommandName.EloadoiIv:
                {
                    List<string> selectedItemsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    List<string> selectedItemsLetrehozasIdo = UI.GetGridViewColumnValuesOfSelectedRows(UgyiratokGridView, "labelLetrehozasiIdo");

                    if (selectedItemsList.Count.Equals(0))
                    {
                        js = "alert('Nincsen kijelölve tétel!');";
                    }
                    else
                    {
                        bool isBefore2012 = false;
                        bool isAfter2012 = false;
                        int Ev = -1;

                        foreach (string letrehozasIdo in selectedItemsLetrehozasIdo)
                        {
                            if (!String.IsNullOrEmpty(letrehozasIdo))
                            {
                                DateTime dt;
                                if (DateTime.TryParse(letrehozasIdo, out dt))
                                {
                                    if (dt.Year > 2011)
                                        isAfter2012 = true;
                                    else
                                        isBefore2012 = true;

                                    if (dt.Year > Ev) Ev = dt.Year;
                                }
                            }
                        }

                        if (isAfter2012 && isBefore2012)
                        {
                            js = "alert('A 2012 elõtti és utáni ügyiratokhoz nem nyomtatható egyszerre elõadóív!');";
                        }
                        else
                        {

                            string tetelIds = "";
                            /*if (selectedItemsList.Count > 30)
							{
								 for (int i = 0; i < 30; i++)
								 {
									  tetelIds = tetelIds + "$" + selectedItemsList[i] + "$,";
								 }
							}
							else
							{*/
                            for (int i = 0; i < selectedItemsList.Count; i++)
                            {
                                tetelIds = tetelIds + "$" + selectedItemsList[i] + "$,";
                            }
                            //}
                            if (tetelIds.Length > 0)
                            {
                                tetelIds = tetelIds.Remove(tetelIds.Length - 1);
                            }
                            Session["SelectedUgyiratok"] = tetelIds;
                            hfSelectedUgyiratok.Value = tetelIds;
                            // blg 1868 
                            string url = "UgyUgyiratokPrintFormSSRSEloadoiIvTomeges.aspx";
                            string qs = QueryStringVars.HiddenFieldId + "=" + hfSelectedUgyiratok.ClientID;
                            if (Ev > -1)
                            {
                                qs += "&" + QueryStringVars.Ev + "=" + Ev.ToString();
                            }

                            // Bug 6295
                            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.SSRS_AUTO_PDF_RENDER))
                            {
                                js = JavaScripts.SetSSRSPrintOnClientClick(url, qs);
                            }
                            else
                            {
                                js = JavaScripts.SetOnCLientClick_NoPostBack(url, qs, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
                            }
                            js = js.TrimEnd("return false;".ToCharArray());
                        }
                    }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "valami", js, true);
                }
                break;
            case CommandName.PeresEloadoiIv:
                List<string> selectedItemsList1 = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                if (selectedItemsList1.Count.Equals(0))
                {
                    js = "alert('Nincsen kijelölve tétel!'); return false;";
                }
                else
                {
                    string tetelIds = "";
                    /*if (selectedItemsList.Count > 30)
					{
						 for (int i = 0; i < 30; i++)
						 {
							  tetelIds = tetelIds + "$" + selectedItemsList[i] + "$,";
						 }
					}
					else
					{*/
                    for (int i = 0; i < selectedItemsList1.Count; i++)
                    {
                        tetelIds = tetelIds + "$" + selectedItemsList1[i] + "$,";
                    }
                    //}
                    if (tetelIds.Length > 0)
                    {
                        tetelIds = tetelIds.Remove(tetelIds.Length - 1);
                    }
                    Session["SelectedUgyiratok"] = tetelIds;
                    hfSelectedUgyiratok.Value = tetelIds;

                    js = JavaScripts.SetOnClientClickIFramePrintForm("PeresEloadoiIvekPrintForm.aspx?" + QueryStringVars.HiddenFieldId + "=" + hfSelectedUgyiratok.ClientID);
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "valami", js, true);
                break;
            case CommandName.KolcsonzesVissza:
                {
                    EREC_IrattariKikeroService kikeroService = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                    ExecParam kikeroExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                    string ugyiratId = UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView);

                    Result kikeroResult = kikeroService.SkontroKikeresSztorno(kikeroExecParam, ugyiratId);
                    if (!string.IsNullOrEmpty(kikeroResult.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, kikeroResult);
                        // BUG_8287
                        ErrorUpdatePanel.Update();
                    }
                    else
                        UgyUgyiratokGridViewBind();
                }
                break;
            case CommandName.Kolcsonzes:
                {
                    List<string> SelectedIds = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    if (SelectedIds != null && SelectedIds.Count > 0)
                    {
                        Session[Constants.SessionNames.SelectedIds] = SelectedIds;

                        js = JavaScripts.SetOnClientClickWithTimeout("IrattariKikeroForm.aspx", QueryStringVars.Command + "=" + CommandName.KikeresSkontroIrattarbol, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Kikeres", js, true);
                    }
                    else
                    {
                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.List.UI_NoSelectedItem);
                        ErrorUpdatePanel.Update();
                    }
                }
                break;
            case CommandName.KolcsonzesKiadas:
                {
                    // Skontró irattárból kiadás:
                    List<string> SelectedIds = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    if (SelectedIds != null && SelectedIds.Count > 0)
                    {
                        Session[Constants.SelectedUgyiratIds] = SelectedIds;

                        js = JavaScripts.SetOnClientClickWithTimeout("SkontroInditasForm.aspx", QueryStringVars.Command + "=" + CommandName.KiadasSkontroIrattarbol, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Kiadas", js, true);
                    }
                    else
                    {
                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.List.UI_NoSelectedItem);
                        ErrorUpdatePanel.Update();
                    }
                    break;
                }
            case CommandName.Felfuggesztes:
                {
                    // FELFÜGGESZTÉS
                    List<string> SelectedIds = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    if (SelectedIds != null && SelectedIds.Count > 0)
                    {
                        Session[Constants.SelectedUgyiratIds] = SelectedIds;
                        string formattedIds = ConvertToServiceIdList(SelectedIds);
                        Session["SelectedFelfuggesztendoUgyiratIds"] = formattedIds;
                        Result result = FindAllUgyiratok(formattedIds);

                        if (result == null || result.ErrorCode != null)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                            // BUG_8287
                            ErrorUpdatePanel.Update();
                            return;
                        }
                        if (!Check_UgyiratFelfuggesztheto_Felelos(result.Ds.Tables[0].Rows))
                        {
                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.MessageFelfuggesztesFelelosHiba);
                            // BUG_8287
                            ErrorUpdatePanel.Update();
                            return;
                        }
                        if (!Check_UgyiratFelfuggesztheto_Allapot(result.Ds.Tables[0].Rows))
                        {
                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.MessageFelfuggesztesStatuszHiba);
                            // BUG_8287
                            ErrorUpdatePanel.Update();
                            return;
                        }
                        js = JavaScripts.SetOnClientClickWithTimeout("UgyiratFelfuggesztesForm.aspx", string.Empty, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Ügyirat felfüggesztés", js, true);
                    }
                    else
                    {
                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.List.UI_NoSelectedItem);
                        ErrorUpdatePanel.Update();
                    }
                }
                break;
            case CommandName.Lezaras:
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    sb = new System.Text.StringBuilder();
                    foreach (string s in lstGridViewSelectedRows)
                    {
                        sb.Append(s + ",");
                    }
                    sb.Remove(sb.Length - 1, 1);
                    Session[Constants.SelectedUgyiratIds] = sb.ToString();

                    js = FelhasznaloProfil.OrgIsBOPMH(Page) ? "alert('Ügyirat lezárása előtt kérjük a teljességellenőrzés elvégzését!');" : "";
                    js += JavaScripts.SetOnClientClickWithTimeout("LezarasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratAtvetelUgyintezesre", js, true);
                }
                break;
            case CommandName.TomegesSzignalas:
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    if (lstGridViewSelectedRows.Count > 0)
                    {
                        Session[Constants.SelectedUgyiratIds] = string.Join(",", lstGridViewSelectedRows.ToArray());
                        Session[Constants.SelectedIratIds] = null;
                        Session[Constants.SelectedBarcodeIds] = null;
                        Session[Constants.SelectedKuldemenyIds] = null;
                        Session[Constants.SelectedIratPeldanyIds] = null;
                        Session[Constants.SelectedDosszieIds] = null;

                        js = JavaScripts.SetOnClientClickWithTimeout("TomegesSzignalas.aspx", String.Empty, Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "TomegesSzignalasScript", js, true);
                    }
                }
                break;
            case CommandName.Visszavetel:
                #region BLG1131Visszavétel
                sb = new System.Text.StringBuilder();
                foreach (string s in ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sb.Append(s + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                Session["SelectedIratPeldanyIds"] = null;

                Session["SelectedKuldemenyIds"] = null;
                Session["SelectedBarcodeIds"] = null;
                Session["SelectedUgyiratIds"] = sb.ToString();
                Session["SelectedDosszieIds"] = null;
                Session["TUKVisszavetel"] = "1";

                js = JavaScripts.SetOnClientClickWithTimeout("AtvetelForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedKuldemenyAtadas", js, true);
                #endregion
                break;
            case CommandName.UgyintezesiIdoUjraSzamolas:
                {
                    UgyiratUgyintezesiIdoUjraSzamolasProcedure();
                }
                break;
            case "TomegesOlvasasiJog":
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    Session[Constants.SelectedUgyiratIds] = String.Join(",", lstGridViewSelectedRows.ToArray());
                    js = JavaScripts.SetOnClientClickWithTimeout("TomegesOlvasasiJog.aspx", QueryStringVars.ObjektumType + "=" + Constants.TableNames.EREC_UgyUgyiratok, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "TomegesOlvasasiJog", js, true);
                }
                break;

            case "ElintezetteNyilvanitasTomeges":
                {
                    js = "";
                    var itszColumnIndex = UI.GetGridViewColumnIndex(UgyiratokGridView, "Merge_IrattariTetelszam");
                    List<string> selectedItemsItsz = UI.GetGridViewColumnValuesOfSelectedRowsByIndex(UgyiratokGridView, itszColumnIndex);
                    if (selectedItemsItsz != null)
                    {
                        if (selectedItemsItsz.Count == 0)
                        {
                            js = "alert('Nincsen kijelölve tétel!');";
                        }
                        else
                        {
                            var firstItsz = "";
                            var wasDiffItsz = false;
                            foreach (var itsz in selectedItemsItsz)
                            {
                                if (!String.IsNullOrEmpty(itsz))
                                {
                                    if (firstItsz == "")
                                    {
                                        firstItsz = itsz;
                                    }
                                    else if (firstItsz != itsz)
                                    {
                                        wasDiffItsz = true;
                                        break;
                                    }
                                }
                            }

                            var n = "UgyiratElintezettTomegesFormHiba";
                            if (wasDiffItsz)
                            {
                                js = "alert('Tömeges elintézetté nyilvánítás csak azonos irattári tételszámú ügyek esetén használható!');";
                            }
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), n, js, true);
                        }
                    }

                    if (String.IsNullOrEmpty(js))
                    {
                        List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(UgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                        Session[Constants.SelectedUgyiratIds] = String.Join(",", lstGridViewSelectedRows.ToArray());
                        js = JavaScripts.SetOnClientClickWithTimeout("UgyiratElintezettTomegesForm.aspx", QueryStringVars.ObjektumType + "=" + Constants.TableNames.EREC_UgyUgyiratok, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "UgyiratElintezettTomegesForm", js, true);
                    }
                    break;
                }
        }
    }

    private void LockSelectedIraUgyUgyiratRecords()
    {
        LockManager.LockSelectedGridViewRecords(UgyUgyiratokGridView, "EREC_UgyUgyiratok"
             , "UgyiratLock", "UgyiratForceLock"
              , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedIraUgyUgyiratRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(UgyUgyiratokGridView, "EREC_UgyUgyiratok"
             , "UgyiratLock", "UgyiratForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }


    /// <summary>
    /// Elkuldi emailben a UgyUgyiratokGridView -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedUgyUgyiratok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_UgyUgyiratok");
        }
    }

    protected void UgyUgyiratokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        UgyUgyiratokGridViewBind(e.SortExpression, UI.GetSortToGridView("UgyUgyiratokGridView", ViewState, e.SortExpression));
        //ActiveTabClear();
    }

    protected void GetJelleg(object oJelleg, out string JellegLabel, out string JellegTooltip)
    {
        string jelleg = (oJelleg ?? String.Empty) as string;

        switch (jelleg)
        {
            case KodTarak.UGYIRAT_JELLEG.Elektronikus:
                JellegLabel = "E";
                JellegTooltip = "Elektronikus";
                break;
            case KodTarak.UGYIRAT_JELLEG.Papir:
                JellegLabel = "P";
                JellegTooltip = "Papír";
                break;
            case KodTarak.UGYIRAT_JELLEG.Vegyes:
                JellegLabel = "V";
                JellegTooltip = "Vegyes";
                break;
            default:
                goto case KodTarak.UGYIRAT_JELLEG.Papir;
        }
    }

    protected string GetJellegLabel(object oJelleg)
    {
        string label;
        string tooltip;
        GetJelleg(oJelleg, out label, out tooltip);
        return label;
    }

    protected string GetJellegToolTip(object oJelleg)
    {
        string label;
        string tooltip;
        GetJelleg(oJelleg, out label, out tooltip);
        return tooltip;
    }

    private string GetPrintUrl(EREC_UgyUgyiratok ugyirat)
    {
        string url = "UgyUgyiratokPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ugyirat.Id;
        if (!ugyirat.Base.Typed.LetrehozasIdo.IsNull)
        {
            url += "&" + QueryStringVars.Ev + "=" + ugyirat.Base.Typed.LetrehozasIdo.Value.Year.ToString();
        }
        return url;
    }
    #endregion

    private Result FindAllKrtCimek(string ids)
    {
        if (string.IsNullOrEmpty(ids))
            return null;

        KRT_CimekSearch search = new KRT_CimekSearch();

        search.Id.Value = ids;
        search.Id.Operator = Query.Operators.inner;

        Contentum.eAdmin.Service.KRT_CimekService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CimekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service.GetAll(execParam, search);
        return result;
    }

    private Result FindAllUgyiratok(string ids)
    {
        if (string.IsNullOrEmpty(ids))
            return null;

        EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();

        search.Id.Value = ids;
        search.Id.Operator = Query.Operators.inner;

        EREC_UgyUgyiratokService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service.GetAll(execParam, search);
        return result;
    }
    private bool Check_UgyiratFelfuggesztheto_Allapot(DataRowCollection collection)
    {
        foreach (DataRow item in collection)
        {
            if (item["Allapot"].ToString() != KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt)
                return false;
        }
        return true;
    }
    private bool Check_UgyiratFelfuggesztheto_Felelos(DataRowCollection collection)
    {
        foreach (DataRow item in collection)
        {
            if (!CheckUgyiratFelelos(item["FelhasznaloCsoport_Id_Ugyintez"].ToString()))
                return false;
        }
        return true;
    }
    /// <summary>
    /// Ha az ügyfelelõs nem a saját szervezet, vagy nem a saját szervezetbeli ember, akkor nem lehet felfüggeszteni
    /// </summary>
    /// <param name="erec_UgyUgyiratok"></param>
    private bool CheckUgyiratFelelos(string ugyintezo)
    {
        // ha nincs megadva az ügyfelelõs, nem felfüggeszthetõ
        if (String.IsNullOrEmpty(ugyintezo))
            return false;

        string sajatId = FelhasznaloProfil.FelhasznaloId(Page);
        if (ugyintezo == sajatId)
            return true;

        return false;
    }

    private string ConvertToServiceIdList(List<string> ids)
    {
        StringBuilder result = new StringBuilder();
        bool first = true;
        foreach (var item in ids)
        {
            if (first)
                first = false;
            else
                result.Append(",");

            result.Append("'");
            result.Append(item);
            result.Append("'");
        }
        return result.ToString();
    }

    #region LOTUS POSTBACK
    /// <summary>
    /// VisiblePostBackCommandForLotusNotes
    /// </summary>
    /// <returns></returns>
    protected bool VisiblePostBackCommandForLotusNotes()
    {
        if (string.IsNullOrEmpty(Request.QueryString["Lotus"]))
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// GetIratHatasaTipusIkon
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    protected string GetPostBackCommandForLotusNotes(object type)
    {
        if (type == null || !(type is System.Data.DataRowView))
            return string.Empty;

        DataRow row = ((System.Data.DataRowView)type).Row;
        LotusNotesUgyiratObj obj = new LotusNotesUgyiratObj();
        obj.UgyiratID = row["Id"].ToString();
        obj.Iktatoszam = row["Foszam_Merge"].ToString();
        obj.UgyiratTargy = row["Targy"].ToString();
        obj.UgyiratVonalkod = row["BARCODE"].ToString();
        string json = JSONFunctions.SerializeLotusNotesUgyiratObj(obj);
        return string.Format("window.parent.postMessage({0}, \"*\");", json);
    }
    #endregion

    /// <summary>
    /// GetIratHatasaTipusIkon
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    protected string GetIratHatasaTipusIkon(object type)
    {
        return Contentum.eUtility.Sakkora.GetIratHatasaTipusIkon(type);
    }
    /// <summary>
    /// GetIratHatasaTipusColor
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public System.Drawing.Color GetIratHatasaTipusColor(object type)
    {
        return Contentum.eUtility.Sakkora.GetIratHatasaTipusColor(type);
    }

    /// <summary>
    /// HatralevoNapokSzovegesen
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static string HatralevoNapokSzovegesen(object type, object sakkora)
    {
        if (type == null)
            return string.Empty;
        if (("STOP").Equals(sakkora.ToString()))
        {
            return string.Empty;
        }
        int value;
        if (int.TryParse(type.ToString(), out value))
        {
            if (value > 0)
                return string.Format(Resources.Form.HatralevoNapokSzovegesenPlusFormat, value);
            else
                return string.Format(Resources.Form.HatralevoNapokSzovegesenMinusFormat, Math.Abs(value));
        }
        else
            return string.Empty;

    }
    /// <summary>
    /// HatralevoMunkaNapokSzovegesen
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static string HatralevoMunkaNapokSzovegesen(object type, object sakkora)
    {
        if (type == null)
            return string.Empty;
        if (("STOP").Equals(sakkora.ToString()))
        {
            return string.Empty;
        }
        int value;
        if (int.TryParse(type.ToString(), out value))
        {
            if (value > 0)
                return string.Format(Resources.Form.HatralevoMunkaNapokSzovegesenPlusFormat, value);
            else
                return string.Empty;
        }
        else
            return string.Empty;
    }
    /// <summary>
    /// HatralevoNapokSzinesen
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static System.Drawing.Color HatralevoNapokSzinesen(object type)
    {
        if (type == null)
            return System.Drawing.Color.Black;
        int value;
        if (int.TryParse(type.ToString(), out value))
        {
            if (value > 0)
                return System.Drawing.Color.DarkGreen;
            else
                return System.Drawing.Color.Red;
        }
        else
            return System.Drawing.Color.Black;
    }
    /// <summary>
    /// UgyiratUgyintezesiIdoUjraSzamolasProcedure
    /// </summary>
    private void UgyiratUgyintezesiIdoUjraSzamolasProcedure()
    {
        List<string> selectedItemsList1 = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
        if (selectedItemsList1.Count.Equals(0))
        {
            Contentum.eUtility.ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba !", Resources.List.UI_NoSelectedItem);
            ErrorUpdatePanel.Update();
        }
        else if (selectedItemsList1.Count > 1)
        {
            Contentum.eUtility.ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba !", Resources.List.UI_MORESelectedItem);
            ErrorUpdatePanel.Update();
        }
        else
        {
            SakkoraService sakkoraService = Contentum.eUtility.eRecordService.ServiceFactory.Get_SakkoraService();
            ExecParam execParamSakkora = Contentum.eUtility.UI.SetExecParamDefault(Page, new ExecParam());
            Result resultUjraszamolas;
            foreach (string selectedUgy in selectedItemsList1)
            {
                resultUjraszamolas = sakkoraService.SakkoraUgyintezesiIdoUjraSzamolas(execParamSakkora, selectedUgy);
                if (resultUjraszamolas.IsError)
                {
                    Contentum.eUtility.ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultUjraszamolas);
                    ErrorUpdatePanel.Update();
                    return;
                }
            }
            UgyUgyiratokGridViewBind();

            string jsMsg = "alert('" + Resources.Form.UgyintezesiIdoUjraSzamolasSikeres + "'); return false;";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "UgyiratUgyintezesiIdoUjraSzamolasProcedureOk", jsMsg, true);
        }
    }

    /// <summary>
    /// hatralveo napok lathatosag ellenorzes
    /// </summary>
    /// <param name="ugyAllapota"></param>
    /// <param name="sakkoraAllapota"></param>
    /// <param name="ugyFajtaja"></param>
    /// <returns></returns>
    public static bool HatralevoNapokLathatoak(object type)
    {
        if (type == null || string.IsNullOrEmpty(type.ToString()))
            return true;

        string[] splitted = type.ToString().Split('|');
        string ugyAllapota = splitted[0];
        string sakkoraAllapota = splitted[1];
        string ugyFajtaja = splitted[2];

        ExecParam xpm = UI.SetExecParamDefault(new Page(), new ExecParam());
        return Contentum.eUtility.Sakkora.HatralevoNapokLathatoak(xpm, ugyAllapota, sakkoraAllapota, ugyFajtaja);
    }


    // BUG_13398
    /// <summary>
    /// Ügyintézési határidő lathatosag ellenorzes
    /// </summary>
    /// <returns></returns>
    public bool UgyintezesiHataridoLathatosag(object type)
    {
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.UGYINTEZESI_HATARIDO_MINDIG_MEGJELENIK))
        {
            return true;
        }
        else
            return SakkoraMezokLathatosaga(type);
    }

    /// <summary>
    /// SakkoraMezokLathatosaga
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public bool SakkoraMezokLathatosaga(object type)
    {
        if (type == null || !(type is System.Data.DataRowView))
            return true;

        DataRow row = ((System.Data.DataRowView)type).Row;

        string sakkoraAllapotTipus = row["SakkoraAllapotTipus"].ToString();
        string ugyAllapota = row["Allapot"].ToString();
        string ugyFajtaja = row["Ugy_Fajtaja"].ToString();
        ExecParam xpm = UI.SetExecParamDefault(new Page(), new ExecParam());

        if (RendszerParameterUgyIdoFelfuggesztesFancy == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.SIMPLE)
        {
            return SakkoraLathatosagUgyAllapotaAlapjan(ugyAllapota);
        }

        if (sakkoraAllapotTipus.Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE)
            || sakkoraAllapotTipus.Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS)
            || sakkoraAllapotTipus.Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE)
            || sakkoraAllapotTipus.Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP))
        {
            return false;
        }
        else
        {
            return SakkoraLathatosagUgyAllapotaAlapjan(ugyAllapota);
        }
    }
    /// <summary>
    /// SakkoraLathatosagUgyAllapotaAlapjan
    /// </summary>
    /// <param name="ugyAllapota"></param>
    /// <returns></returns>
    private bool SakkoraLathatosagUgyAllapotaAlapjan(string ugyAllapota)
    {
        switch (ugyAllapota)
        {
            case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
            case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                return true;
            default:
                return false;
        }
    }

    public string NoteJSONParser(string noteJSONString)
    {
        string result = "";
        if (!string.IsNullOrEmpty(noteJSONString))
        {
            Note_JSON note;

            try
            {
                note = JsonConvert.DeserializeObject<Note_JSON>(noteJSONString);
            }
            catch (Exception)
            {
                note = null;
            }

            if (note != null)
                result = note.Megjegyzes;
        }

        return result;
    }

    //LZS - BUG_11081
    //Az SZK mező felületi megjelenítésénél TÜK rendszer esetén(TUK rendszer paraméter = 1), 
    //és a FELELOS_SZERV_KOD_MEGJELENITES rsz.paraméter = 1, ha az EREC_UgyUgyiratok.Note mezőjében van SZK element, 
    //akkor annak az értékét kell bele írni az SZK mezőbe.
    //Ha nincs ilyen element, vagy üres az értéke, akkor a jelenlegi módon 
    //(az adatbázisból lekérdezett érték alapján) kell megjeleníteni az értékét.
    public string GetSZK(string noteJSONString, string db_UgyFelelos_SzervezetKod, string UgyiratId)
    {
        ExecParam _ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        bool isTUK = Rendszerparameterek.GetBoolean(_ExecParam, Rendszerparameterek.TUK, false);
        string result = "";

        //LZS - BUG_11081 - TÜK esetén
        if (isTUK && Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
        {
            if (!string.IsNullOrEmpty(noteJSONString))
            {
                Note_JSON note;
                try
                {
                    note = JsonConvert.DeserializeObject<Note_JSON>(noteJSONString);
                }
                catch (Exception)
                {
                    note = null;
                }

                if (note != null && !string.IsNullOrEmpty(note.SZK))
                {
                    result = note.SZK;
                }//Ha van a note mezőben bejegyzés, de abban nincs SZK érték:
                else if (note != null && string.IsNullOrEmpty(note.SZK))
                {
                    if (!String.IsNullOrEmpty(db_UgyFelelos_SzervezetKod))
                    {
                        FillSZK(db_UgyFelelos_SzervezetKod, UgyiratId);
                    }

                    result = db_UgyFelelos_SzervezetKod;
                }
                else
                {
                    result = db_UgyFelelos_SzervezetKod;
                }
            }
            //LZS - BUG_11081 - Ha üres a noteJSONString, akkor a db_UgyFelelos_SzervezetKod-ot beleírjuk, és ezen a ponton a 
            //db értéket írjuk vissza (iktatás utáni első megnyitás):
            else
            {
                FillSZK(db_UgyFelelos_SzervezetKod, UgyiratId);

                result = db_UgyFelelos_SzervezetKod;
            }
        }
        else //LZS - BUG_11081 - Nem TÜK esetén marad a db-ből jövő érték
        {
            result = db_UgyFelelos_SzervezetKod;
        }

        return result;
    }

    private void FillSZK(string db_UgyFelelos_SzervezetKod, string UgyiratId)
    {
        if (!String.IsNullOrEmpty(UgyiratId))
        {
            Note_JSON note = new Note_JSON();
            note.SZK = db_UgyFelelos_SzervezetKod;

            EREC_UgyUgyiratokService serviceUgyUgyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParamUgyUgyiratok = UI.SetExecParamDefault(Page, new ExecParam());
            execParamUgyUgyiratok.Record_Id = UgyiratId;

            Result resGet = serviceUgyUgyiratok.Get(execParamUgyUgyiratok);

            if (resGet.IsError)
                return;

            EREC_UgyUgyiratok eREC_UgyUgyirat = resGet.Record as EREC_UgyUgyiratok;
            eREC_UgyUgyirat.Updated.SetValueAll(false);
            eREC_UgyUgyirat.Base.Updated.SetValueAll(false);
            eREC_UgyUgyirat.Base.Updated.Ver = true;

            eREC_UgyUgyirat.Base.Note = JsonConvert.SerializeObject(note);
            eREC_UgyUgyirat.Base.Updated.Note = true;

            Result resUpdate = serviceUgyUgyiratok.Update(execParamUgyUgyiratok, eREC_UgyUgyirat);

            //Hibakezelés, megjelenítés
            if (resUpdate.IsError)
            {
                return;
            }
        }
    }

    // NOTE: Eval("Hatarido", "{0:yyyy.MM.dd}") nem működik, ezért kell
    public static string ReformatDate(string valueFromDatabase, string dbFormat, string outFormat)
    {
        DateTime value;
        if (!DateTime.TryParseExact(valueFromDatabase, dbFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out value))
        {
            return string.Empty;
        }

        return value.ToString(outFormat, CultureInfo.CurrentCulture);
    }
}
