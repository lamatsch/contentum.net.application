﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VisszakuldesPopup.ascx.cs" Inherits="eRecordComponent_VisszakuldesPopup" %>
<%@ Register Src="~/Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc"%>
<asp:Panel ID="pnlVisszakuldes" runat="server" CssClass="emp_Panel" style="display: none">
    <div style="padding-bottom:8px">
        <h2 runat="server" id="header" class="szmp_HeaderWrapper">
            <asp:Label ID="labelHeader" runat="server" Text="Visszaküldés" CssClass="emp_Header"></asp:Label>
        </h2>
        <div id="errorText" class="szmp_Body">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="mrUrlapCaption_nowidth">
                        <asp:Label ID="LabelReq" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                        <asp:Label ID="labelVisszakuldesOka" runat="server" Text="Visszaküldés oka:"></asp:Label>
                    </td>
                    <td class="mrUrlapMezo">
                        <uc:RequiredTextBox ID="txtVisszakuldesOka" LabelRequiredIndicatorID="LabelReq"  runat="server" Width="200" Rows="2" TextBoxMode="MultiLine"/>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td style="width:50%;text-align:center;">
                    <asp:ImageButton ID="btnOk" runat="server" 
                        ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                        onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')" 
                        />    
                </td>
                <td style="width:50%;text-align:center;">
                    <asp:ImageButton ID="btnCancel" runat="server" 
                        ImageUrl="~/images/hu/ovalgomb/megsem.jpg" 
                        onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')" 
                        CausesValidation="false"/>   
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
<asp:Button ID="targetControl" runat="server" Text="Button" style="display:none;"/>
<ajaxToolkit:ModalPopupExtender ID="mdeVisszakuldes" runat="server" TargetControlID="targetControl"
    PopupControlID="pnlVisszakuldes" OkControlID="targetControl" BackgroundCssClass="emp_modalBackground"
    CancelControlID="btnCancel" DropShadow="true" PopupDragHandleControlID="header" />