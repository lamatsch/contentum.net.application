﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;

public partial class eRecordComponent_VisszakuldesPopup : System.Web.UI.UserControl
{
    public string VisszakuldesIndoka
    {
        get
        {
            return txtVisszakuldesOka.Text;
        }
    }

    public void SetOnclientClickShowFunction(ImageButton target)
    {
        //target elmentése, postback miatt
        this.Target = target;
        target.OnClientClick = GetOnClientClickShow(); ;
    }

    private string GetOnClientClickShow()
    {
        string js = "$find('" + mdeVisszakuldes.ClientID + @"').show();
                     $get('" + txtVisszakuldesOka.TextBox.ClientID + "').focus();return false;";
        return js;
    }

    private ImageButton target;

    private ImageButton Target
    {
        get { return target; }
        set { target = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        btnOk.ValidationGroup = txtVisszakuldesOka.ValidationGroup = this.ClientID;
        RegisterJavaScripts();
        txtVisszakuldesOka.Text = String.Empty;
    }

    private void RegisterJavaScripts()
    {
        if (Target != null)
        {
            btnOk.OnClientClick = "if (typeof(Page_ClientValidate) == 'function' && !Page_ClientValidate('" + btnOk.ValidationGroup + "')) {return false;}"
                                  + "else {$find('" + mdeVisszakuldes.ClientID + "').hide();};"
                                  + Page.ClientScript.GetPostBackEventReference(
                                  new PostBackOptions(Target,"","",true,false,false,true,true,btnOk.ValidationGroup)) 
                                  + ";return false;";
        }
        mdeVisszakuldes.OnCancelScript = "$get('" + txtVisszakuldesOka.TextBox.ClientID + "').value = '';";
    }

}
