<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VonalKodTextBox.ascx.cs" Inherits="eRecordComponent_VonalKodTextBox" %>

<asp:TextBox ID="vonalkodTextBox" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
<% var Command = Request.QueryString.Get(Contentum.eRecord.Utility.CommandName.Command);
   %>
        <%--<%
        if (Command != Contentum.eRecord.Utility.CommandName.New)
        { %>--%>
            <asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="vonalkodTextBox" SetFocusOnError="true"
                Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                <ajaxToolkit:ValidatorCalloutExtender
                    ID="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1">
                <Animations>
                        <OnShow>
                        <Sequence>
                            <HideAction Visible="true" />
                            <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                        </Sequence>    
                        </OnShow>
                 </Animations>
                </ajaxToolkit:ValidatorCalloutExtender>
             <asp:CustomValidator ID="CustomValidatorBarcodeCheckSum" runat="server" ControlToValidate="vonalkodTextBox" Display="None" SetFocusOnError="true"></asp:CustomValidator>
             <ajaxToolkit:ValidatorCalloutExtender
                    ID="ValidatorCalloutExtenderBarcodeCheckSum" runat="server" TargetControlID="CustomValidatorBarcodeCheckSum">
                <Animations>
                        <OnShow>
                        <Sequence>
                            <HideAction Visible="true" />
                            <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                        </Sequence>    
                        </OnShow>
                 </Animations>
                </ajaxToolkit:ValidatorCalloutExtender>
<%-- <%   }
%>--%>