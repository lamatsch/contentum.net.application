﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VonalkodListBox.ascx.cs" Inherits="eRecordComponent_VonalkodListBox" %>

<%@ Register Src="VonalKodTextBox.ascx" TagName="VonalKodTextBox" TagPrefix="uc1" %>

<table cellspacing="0" cellpadding="0" width="100%" align="center">
    <tr class="urlapSor">
        <td class="mrUrlapCaption_short" style="height: 24px">
            <asp:Label ID="labelVonalkod" runat="server" Text="Vonalkód:"></asp:Label>
        </td>
        <td class="mrUrlapMezo">
            <uc1:vonalkodtextbox id="VonalkodTextBoxVonalkod" runat="server" validate="false" />
        </td>
        <td class="mrUrlapMezo">
            <asp:Image ID="ImageAdd" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                AlternateText="Hozzáadás a listához" ToolTip="Hozzáadás a listához" />
           <asp:CheckBox ID="cbSoundEffect" runat="server"
                ToolTip="Sikeres beolvasás hangjelzés" />
            <ajaxToolkit:ToggleButtonExtender ID="ToggleButtonExtender_SoundEffect" runat="server" TargetControlID="cbSoundEffect"
                UncheckedImageUrl="../images/hu/egyeb/sound_off.jpg" UncheckedImageAlternateText="Sikeres beolvasás hangjelzés bekapcsolása"
                CheckedImageUrl="../images/hu/egyeb/sound_on.jpg" CheckedImageAlternateText="Sikeres beolvasás hangjelzés kikapcsolása"
                ImageWidth="13" ImageHeight="13"
                >
            </ajaxToolkit:ToggleButtonExtender>
        </td>
    </tr>
    <tr valign="top">
        <td class="mrUrlapCaption_short" style="height: 24px">
            <asp:Label ID="labelVonalkodList" runat="server" Text="Beolvasott vonalkódok:"></asp:Label>
        </td>
        <td class="mrUrlapMezo">
            <asp:ListBox ID="VonalkodokListBox" runat="server" Rows="5" Width="100%" SelectionMode="Multiple"></asp:ListBox>
            <asp:HiddenField ID="hfVonalkodClientState" runat="server" />
        </td>
        <td class="mrUrlapMezo">
            <asp:Image ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                AlternateText="Vonalkód kivétele a listából" ToolTip="Vonalkód kivétele a listából" />
            <br />

        </td>
    </tr>
</table>
