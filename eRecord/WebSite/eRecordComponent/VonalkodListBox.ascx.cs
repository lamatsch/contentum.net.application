﻿using Contentum.eAdmin.Utility;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class eRecordComponent_VonalkodListBox : System.Web.UI.UserControl, IScriptControl, Contentum.eUtility.ISearchComponent
{
    // BUG_2107
    // private const int barcodeLength = 13; // a vonalkódok hossza
    private int barcodeLength = 13; // a vonalkódok hossza

    private int maxElementsNumber = 500;

    public int MaxElementsNumber
    {
        get { return maxElementsNumber; }
        set { maxElementsNumber = value; }
    }

    public ListBox ListBox
    {
        get
        {
            return this.VonalkodokListBox;
        }
    }

    public TextBox TextBox
    {
        get
        {
            return this.VonalkodTextBoxVonalkod.TextBox;
        }
    }

    public int Rows
    {
        get
        {
            return VonalkodokListBox.Rows;
        }
        set
        {
            VonalkodokListBox.Rows = value;
        }
    }

    public string VonalkodClientList
    {
        get
        {
            return hfVonalkodClientState.Value.Trim();
        }
        set
        {
            hfVonalkodClientState.Value = value;
        }
    }

    private int maxSearchTextElementsCount = 3;
    /// <summary>
    /// Legfeljebb ennyi vonalkód elem kerül a GetSearchText által generált kimenetbe.
    /// Ha ennél több vonalkód is meg van adva, azt "..." jelzi a szöveg végén
    /// </summary>
    public int MaxSearchTextElementsCount
    {
        get { return maxSearchTextElementsCount; }
        set
        {
            if (value < 0) value = 0;
            maxSearchTextElementsCount = value;
        }
    }

    public void Reset()
    {
        VonalkodokListBox.Items.Clear();
        hfVonalkodClientState.Value = String.Empty;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // BUG_2107
        string CheckPrefix = Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_PREFIX);
        string CheckVektor = Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_ELLENORZO_VEKTOR);

        if (Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("SAVOS"))
        {
            barcodeLength = CheckPrefix.Length + CheckVektor.Length + 1;
        }
        else
            barcodeLength = CheckVektor.Length + 1;

    }

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.DesignMode)
        {
            // Test for ScriptManager and register if it exists
            sm = ScriptManager.GetCurrent(Page);

            if (sm == null)
                throw new HttpException("A ScriptManager control must exist on the current page.");


            sm.RegisterScriptControl(this);
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.DesignMode)
        {
            sm.RegisterScriptDescriptors(this);
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.ListBoxBehavior", this.VonalkodokListBox.ClientID);
        descriptor.AddProperty("BarCodeBehaviorId", VonalkodTextBoxVonalkod.BehaviorID);
        descriptor.AddProperty("ImageAddId", ImageAdd.ClientID);
        descriptor.AddProperty("ImageDeleteId", DeSelectingRowsImageButton.ClientID);
        descriptor.AddProperty("HiddenFieldClientStateId", hfVonalkodClientState.ClientID);
        descriptor.AddProperty("MaxElementsNumber", MaxElementsNumber);
        descriptor.AddProperty("ListIsFullMessage", Resources.Form.UI_ListIsFull);
        descriptor.AddProperty("CbSoundEffectId", cbSoundEffect.ClientID);

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/ListBoxes.js";

        return new ScriptReference[] { reference };
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        string vonalkodok = "";
        //BUG_13408
        if (string.IsNullOrEmpty(VonalkodClientList))
            vonalkodok = TextBox.Text;
        else
            vonalkodok = VonalkodClientList;

        int length = maxSearchTextElementsCount * barcodeLength;
        if (maxSearchTextElementsCount > 0)
        {
            // vesszők
            length += maxSearchTextElementsCount;   // belevesszük az utolsó vesszőt is, ezért nem maxSearchTextElementsCount-1
        }

        if (vonalkodok.Length > length)
        {
            vonalkodok = vonalkodok.Substring(0, length) + "...";
        }



        return vonalkodok;
    }

    #endregion
}
