﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeFile="eBeadvanyAdatokUserControl.ascx.cs"
    Inherits="eRecordComponent_eBeadvanyAdatokUserControl" %>

<%@ Register Src="../Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="ucSLH" %>

<asp:ObjectDataSource ID="ObjectDataSourceEBeadvanyok" runat="server" SelectMethod="Get"
    TypeName="UIBeadvanyokModel" OldValuesParameterFormatString="original_{0}" OnSelecting="ObjectDataSourceEBeadvanyok_Selecting">
    <SelectParameters>
        <asp:Parameter Name="execParam" Type="Object" />
        <asp:QueryStringParameter Name="id" QueryStringField="eBeadvanyokId" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

<eUI:eFormPanel ID="EFormPanel1" runat="server">
    <asp:ImageButton runat="server" ImageUrl="../images/hu/ovalgomb/eBeadvany.jpg" ID="ImageView_ViewEBeadvanyokDetails"
        OnClick="ImageView_ViewEBeadvanyokDetails_Click" CommandName="ViewEBeadvanyokDetails" CausesValidation="false"
        AlternateText="eBeadvány adatok"
        onmouseover="swapByName(this.id,'eBeadvany.jpg')" onmouseout="swapByName(this.id,'eBeadvany.jpg')" />
    <asp:Panel runat="server" ID="panelMain" Visible="false">
        <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional" Visible="false">
            <ContentTemplate>
                <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
                </eUI:eErrorPanel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <eUI:eFormPanel ID="eBeadvanyAdatokPanel" runat="server">
            <ajaxToolkit:TabContainer ID="TabContainerMain" runat="server" Width="100%">
                <ajaxToolkit:TabPanel ID="TabPanel_MAIN" runat="server">
                    <HeaderTemplate>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:Label runat="server" Text="eBeadvány adatok"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="Panel_A" runat="server" Width="100%">

                                    <asp:DetailsView ID="DetailsViewMain" runat="server" AutoGenerateRows="False" DataSourceID="ObjectDataSourceEBeadvanyok" EnableModelValidation="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal">
                                        <RowStyle Font-Bold="True" ForeColor="Black" BorderColor="#f3f3f3" BorderWidth="1px" HorizontalAlign="Left"/>
                                        <Fields>
                                            <asp:BoundField DataField="Irany" HeaderText="Irany" SortExpression="Irany" />
                                            <asp:BoundField DataField="Allapot" HeaderText="Allapot" SortExpression="Allapot" />
                                            <asp:BoundField DataField="KuldoRendszer" HeaderText="KuldoRendszer" SortExpression="KuldoRendszer" />
                                            <asp:BoundField DataField="UzenetTipusa" HeaderText="UzenetTipusa" SortExpression="UzenetTipusa" />
                                            <asp:BoundField DataField="FeladoTipusa" HeaderText="FeladoTipusa" SortExpression="FeladoTipusa" />
                                            <asp:BoundField DataField="PartnerKapcsolatiKod" HeaderText="Kapcsolati kód" SortExpression="PartnerKapcsolatiKod" />
                                            <asp:BoundField DataField="PartnerNev" HeaderText="Feladó neve" SortExpression="PartnerNev" />
                                            <asp:BoundField DataField="PartnerEmail" HeaderText="Feladó e-mail címe" SortExpression="PartnerEmail" />
                                            <asp:BoundField DataField="PartnerRovidNev" HeaderText="Hivatal rövid neve" SortExpression="PartnerRovidNev" />
                                            <asp:BoundField DataField="PartnerMAKKod" HeaderText="Hivatal MÁK kódja" SortExpression="PartnerMAKKod" />
                                            <asp:BoundField DataField="PartnerKRID" HeaderText="Hivatal PartnerKRID-je" SortExpression="PartnerKRID" />
                                            <asp:BoundField DataField="Partner_Id" HeaderText="Partner_Id" SortExpression="Partner_Id" />
                                            <asp:BoundField DataField="Cim_Id" HeaderText="Cim_Id" SortExpression="Cim_Id" />
                                            <asp:BoundField DataField="KR_HivatkozasiSzam" HeaderText="Hivatkozási szám" SortExpression="KR_HivatkozasiSzam" />
                                            <asp:BoundField DataField="KR_ErkeztetesiSzam" HeaderText="Érkeztetési szám" SortExpression="KR_ErkeztetesiSzam" />
                                            <asp:BoundField DataField="Contentum_HivatkozasiSzam" HeaderText="Contentum_HivatkozasiSzam" SortExpression="Contentum_HivatkozasiSzam" />
                                            <asp:BoundField DataField="PR_HivatkozasiSzam" HeaderText="PR_HivatkozasiSzam" SortExpression="PR_HivatkozasiSzam" />
                                            <asp:BoundField DataField="PR_ErkeztetesiSzam" HeaderText="Elektronikus érkeztető azonosító" SortExpression="PR_ErkeztetesiSzam" />
                                            <asp:BoundField DataField="KR_DokTipusHivatal" HeaderText="<%$Forditas:labelKR_DokTipusHivatal|Dokumentum típus hivatal:%>" SortExpression="KR_DokTipusHivatal" />
                                            <asp:BoundField DataField="KR_DokTipusAzonosito" HeaderText="<%$Forditas:labelKR_DokTipusAzonosito|Dokumentum típus azonosító:%>" SortExpression="KR_DokTipusAzonosito" />
                                            <asp:BoundField DataField="KR_DokTipusLeiras" HeaderText="<%$Forditas:labelKR_DokTipusLeiras|Dokumentum típus leírás:%>" SortExpression="KR_DokTipusLeiras" />
                                            <asp:BoundField DataField="KR_Megjegyzes" HeaderText="Megjegyzés" SortExpression="KR_Megjegyzes" />
                                            <asp:BoundField DataField="KR_ErvenyessegiDatum" HeaderText="Érvényességi dátum" SortExpression="KR_ErvenyessegiDatum" />
                                            <asp:BoundField DataField="KR_ErkeztetesiDatum" HeaderText="Érkeztetési dátum" SortExpression="KR_ErkeztetesiDatum" />
                                            <asp:BoundField DataField="KR_FileNev" HeaderText="Fájlnév" SortExpression="KR_FileNev" />
                                            <asp:BoundField DataField="KR_Kezbesitettseg" HeaderText="Kézbesítettség" SortExpression="KR_Kezbesitettseg" />
                                            <asp:BoundField DataField="KR_Idopecset" HeaderText="Időpecsét" SortExpression="KR_Idopecset" />
                                            <asp:BoundField DataField="KR_Valasztitkositas" HeaderText="Válasz titkosítás" SortExpression="KR_Valasztitkositas" />
                                            <asp:BoundField DataField="KR_Valaszutvonal" HeaderText="Válasz útvonal" SortExpression="KR_Valaszutvonal" />
                                            <asp:BoundField DataField="KR_Rendszeruzenet" HeaderText="Rendszerüzenet" SortExpression="KR_Rendszeruzenet" />
                                            <asp:BoundField DataField="KR_Tarterulet" HeaderText="Tárterület" SortExpression="KR_Tarterulet" />
                                            <asp:BoundField DataField="KR_ETertiveveny" HeaderText="ETértivevény" SortExpression="KR_ETertiveveny" />
                                            <asp:BoundField DataField="KR_Lenyomat" HeaderText="KR_Lenyomat" SortExpression="KR_Lenyomat" />
                                            <asp:BoundField DataField="KuldKuldemeny_Id" HeaderText="KuldKuldemeny_Id" SortExpression="KuldKuldemeny_Id" />
                                            <asp:BoundField DataField="IraIrat_Id" HeaderText="IraIrat_Id" SortExpression="IraIrat_Id" />
                                            <asp:BoundField DataField="IratPeldany_Id" HeaderText="IratPeldany_Id" SortExpression="IratPeldany_Id" />
                                            <asp:BoundField DataField="Cel" HeaderText="Cel" SortExpression="Cel" />
                                            <asp:BoundField DataField="PR_Parameterek" HeaderText="PR_Parameterek" SortExpression="PR_Parameterek" />
                                            <asp:BoundField DataField="KR_Fiok" HeaderText="KR_Fiok" SortExpression="KR_Fiok" />
                                        </Fields>
                                        <FieldHeaderStyle BackColor="#f3f3f3" />
                                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                                        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                                    </asp:DetailsView>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel ID="TabPanel_CSATOLMANYOK" runat="server">
                    <HeaderTemplate>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:Label runat="server" Text="eBeadvány csatolmányok"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="Panel_B" runat="server" Width="100%">
                                    <asp:UpdatePanel ID="updatePaneleBeadvanyCsatolmanyok" runat="server">
                                        <ContentTemplate>
                                            <asp:Panel ID="paneleBeadvanyCsatolmanyok" runat="server" Width="100%">
                                                <ucSLH:SubListHeader ID="eBeadvanyCsatolmanyokSubListHeader" runat="server" Visible="false" />
                                                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                            <ajaxToolkit:CollapsiblePanelExtender ID="cpeeBeadvanyCsatolmanyok" runat="server" TargetControlID="paneleBeadvanyCsatolmanyokList"
                                                                CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                                                AutoCollapse="false" AutoExpand="false" ExpandedSize="0">
                                                            </ajaxToolkit:CollapsiblePanelExtender>
                                                            <asp:Panel ID="paneleBeadvanyCsatolmanyokList" runat="server">

                                                                <asp:GridView ID="gridVieweBeadvanyCsatolmanyok" runat="server" AutoGenerateColumns="False"
                                                                    CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True">
                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                    <EmptyDataTemplate>Nincsenek adatok !</EmptyDataTemplate>
                                                                    <Columns>
                                                                        <asp:TemplateField SortExpression="Nev" HeaderText="Fájlnév">
                                                                            <ItemTemplate>
                                                                                <%# GetDocumentLink(Eval("Dokumentum_Id") as Guid?, Eval("Nev") as string) %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <PagerSettings Visible="False" />
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel ID="TabPanel_FORM_XML" runat="server">
                    <HeaderTemplate>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:Label runat="server" Text="eBeadvány FORM.XML"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="Panel_C" runat="server" Width="100%">
                                    <%--<h3>Form</h3>--%>
                                    <table runat="server" id="table_Form" cellspacing="0" cellpadding="0" style="width: 100%">
                                        <tbody>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A form azonosítója" ToolTip="form.id"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_FormId" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A form neve" ToolTip="form.type_name"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_FormTypeName" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="Megjegyzés" ToolTip="text001_input"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_text001_input" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A beküldő azonosítója" ToolTip="form.userid"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_form_userid" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="Beküldő neve" ToolTip="form.username"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_form_username" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A beküldő szervezet azonosítója" ToolTip="form.company_id"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_form_company_id" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="Beküldő szervezet HIR azonosítója" ToolTip="form.hir_id"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_form_hir_id" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A beküldő szervezet neve" ToolTip="form.company_name"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_form_company_name" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="Beküldő címének települése" ToolTip="form.company_address_city"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_form_company_address_city" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A beküldő irányítószáma" ToolTip="form.company_address_zip"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_form_company_address_zip" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="Beküldő címe" ToolTip="form.company_address_id"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_form_company_address_id" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td class="mrUrlapCaption_short"></td>
                                                <td class="mrUrlapMezo"></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <%--<h3>Szervezet</h3>--%>
                                    <table runat="server" id="table_Szervezet" cellspacing="0" cellpadding="0" style="width: 100%">
                                        <tbody>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A beküldő szervezet neve" ToolTip="cpf:Szervezet"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_cpf_Szervezet" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A beküldő kapcsolattartó neve" ToolTip="cpf:Kapcsolattarto"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_cpf_Kapcsolattarto" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="Beküldő email címe" ToolTip="cpf:emailAddress"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_cpf_emailAddress" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td class="mrUrlapCaption_short"></td>
                                                <td class="mrUrlapMezo"></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <%--<h3>Beküldő</h3>--%>
                                    <table runat="server" id="table_Bekuldo" cellspacing="0" cellpadding="0" style="width: 100%">
                                        <tbody>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A beküldő nevének előtagja" ToolTip="cpf:DrCimJelzes"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_cpf_DrCimJelzes" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A beküldő családi neve" ToolTip="cpf:CsaladiNev"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_cpf_CsaladiNev" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="Beküldő utóneve1" ToolTip="cpf:UtoNev1"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_cpf_UtoNev1" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="Beküldő utóneve2" ToolTip="cpf:UtoNev1"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_cpf_UtoNev2" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A beküldő irányítószáma" ToolTip="cpf:Benyujto_Lakcim_Irsz"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_cpf_Benyujto_Lakcim_Irsz" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A beküldő település" ToolTip="cpf:Benyujto_Lakcim_Telepules"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_cpf_Benyujto_Lakcim_Telepules" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor_kicsi">
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A beküldő címe" ToolTip="cpf:Benyujto_Lakcim"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_cpf_Benyujto_Lakcim" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td class="mrUrlapCaption_short">
                                                    <asp:Label runat="server" Text="A beküldő email címe" ToolTip="cpf:emailAddress"></asp:Label>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_cpf_benyujto_emailAddress" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
        </eUI:eFormPanel>
    </asp:Panel>
</eUI:eFormPanel>



