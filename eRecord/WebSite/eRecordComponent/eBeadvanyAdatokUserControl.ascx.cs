﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eUtility.EKF;
using Contentum.eUtility.EKF.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eRecordComponent_eBeadvanyAdatokUserControl : System.Web.UI.UserControl
{
    #region VARIABLES
    public string eBeadvanyId { get; set; }
    internal UI ui = new UI();
    #endregion

    #region PUBLIC
    protected void Page_Init(object sender, EventArgs e)
    {
        eBeadvanyId = Request.QueryString.Get("eBeadvanyokId");
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Start();
    }
    public void Start()
    {
        if (string.IsNullOrEmpty(this.eBeadvanyId))
            return;

        eBeadvanyCsatolmanyokGridViewBind();
        LoadAndSetFormXml();
    }
    /// <summary>
    /// GetDocumentLink
    /// </summary>
    /// <param name="dokumentumId"></param>
    /// <param name="fileNev"></param>
    /// <returns></returns>
    public string GetDocumentLink(Guid? dokumentumId, string fileNev)
    {
        if (dokumentumId != null)
        {
            return String.Format("<a href=\"javascript:void(0);\" onclick=\"window.open('GetDocumentContent.aspx?id={0}'); return false;\" style=\"text-decoration:underline\">{1}</a>", dokumentumId, fileNev);
        }
        else
        {
            return fileNev;
        }
    }
    #endregion

    #region DATASOURCE
    protected void ObjectDataSourceEBeadvanyok_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["execParam"] = UI.SetExecParamDefault(Page, new ExecParam());
    }
    #endregion

    #region HELPER
    protected void ImageView_ViewEBeadvanyokDetails_Click(object sender, ImageClickEventArgs e)
    {
        panelMain.Visible = !panelMain.Visible;
        if (panelMain.Visible)
            Start();
    }
    /// <summary>
    /// LoadAndSetFormXml
    /// </summary>
    private void LoadAndSetFormXml()
    {
        if (string.IsNullOrEmpty(this.eBeadvanyId))
            return;

        EBeadvanyFormXmlHelper helper = new EBeadvanyFormXmlHelper();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        EKF_Form_XML_Model model = helper.ExtractDataFromEBeadvanyFormXml(new Guid(this.eBeadvanyId), execParam);
        FillFormXmlControls(model);
    }
    /// <summary>
    /// FillFormXmlControls
    /// </summary>
    /// <param name="model"></param>
    private void FillFormXmlControls(EKF_Form_XML_Model model)
    {
        if (model == null)
            return;

        SetFormXmlControlsValue(model);
        SetFormXmlControlsVisibility(model);
    }
    /// <summary>
    /// SetFormXmlControlsValue
    /// </summary>
    /// <param name="model"></param>
    private void SetFormXmlControlsValue(EKF_Form_XML_Model model)
    {
        TextBox_FormId.Text = model.FormId;
        TextBox_FormTypeName.Text = model.FormTypeName;
        TextBox_text001_input.Text = model.Text001;
        TextBox_form_userid.Text = model.FormUserId;
        TextBox_form_username.Text = model.FormUserName;
        TextBox_form_company_id.Text = model.FormCompanyId;
        TextBox_form_hir_id.Text = model.FormHirId;
        TextBox_form_company_name.Text = model.FormCompanyName;
        TextBox_form_company_address_city.Text = model.FormCompanyAddressCity;
        TextBox_form_company_address_zip.Text = model.FormCompanyAddressZip;
        TextBox_form_company_address_id.Text = model.FormCompanyAddressId;

        TextBox_cpf_Szervezet.Text = model.CpfSzervezet;
        TextBox_cpf_Kapcsolattarto.Text = model.CpfKapcsolattarto;
        TextBox_cpf_emailAddress.Text = model.CpfEmailAddress;

        TextBox_cpf_DrCimJelzes.Text = model.CpfDrCimJelzes;
        TextBox_cpf_CsaladiNev.Text = model.CpfCsaladiNev;
        TextBox_cpf_UtoNev1.Text = model.CpfUtonev1;
        TextBox_cpf_UtoNev2.Text = model.CpfUtonev2;

        TextBox_cpf_Benyujto_Lakcim_Irsz.Text = model.CpfBenyujtoLakcimIrsz;
        TextBox_cpf_Benyujto_Lakcim_Telepules.Text = model.CpfBenyujtoLakcimTelepules;
        TextBox_cpf_Benyujto_Lakcim.Text = model.CpfBenyujtoLakcim;
        TextBox_cpf_benyujto_emailAddress.Text = model.CpfEmailAddress;
    }
    /// <summary>
    /// SetFormXmlControlsVisibility
    /// </summary>
    /// <param name="model"></param>
    private void SetFormXmlControlsVisibility(EKF_Form_XML_Model model)
    {
        switch (model.Type)
        {
            case 1:
                table_Form.Visible = true;
                table_Szervezet.Visible = false;
                table_Bekuldo.Visible = false;
                break;
            case 2:
                table_Form.Visible = false;
                table_Szervezet.Visible = true;
                table_Bekuldo.Visible = false;
                break;
            case 3:
                table_Form.Visible = false;
                table_Szervezet.Visible = false;
                table_Bekuldo.Visible = true;
                break;
            default:
                table_Form.Visible = false;
                table_Szervezet.Visible = false;
                table_Bekuldo.Visible = false;
                break;
        }
    }
    /// <summary>
    /// A eBeadvanyCsatolmanyok GridView adatkötése a EmailBoritekID és rendezési paraméterek függvényében
    /// </summary>
    protected void eBeadvanyCsatolmanyokGridViewBind()
    {
        if (!string.IsNullOrEmpty(this.eBeadvanyId))
        {
            EREC_eBeadvanyCsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_eBeadvanyCsatolmanyokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_eBeadvanyCsatolmanyokSearch _EREC_eBeadvanyCsatolmanyokSearch = (EREC_eBeadvanyCsatolmanyokSearch)Search.GetSearchObject(Page, new EREC_eBeadvanyCsatolmanyokSearch());
            _EREC_eBeadvanyCsatolmanyokSearch.eBeadvany_Id.Value = eBeadvanyId;
            _EREC_eBeadvanyCsatolmanyokSearch.eBeadvany_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result res = service.GetAll(ExecParam, _EREC_eBeadvanyCsatolmanyokSearch);
            UI.GridViewFill(gridVieweBeadvanyCsatolmanyok, res, eBeadvanyCsatolmanyokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
        }
        else
        {
            ui.GridViewClear(gridVieweBeadvanyCsatolmanyok);
        }
    }
    #endregion


}