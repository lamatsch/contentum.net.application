﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="eBeadvanyKuldesiAdatokTab.ascx.cs" Inherits="eRecordComponent_eBeadvanyKuldesiAdatokTab" %>

<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc" %>

<%--Hiba megjelenites--%>
<asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
        </eUI:eErrorPanel>
    </ContentTemplate>
</asp:UpdatePanel>
<%--/Hiba megjelenites--%>

<asp:UpdatePanel ID="eBeadvanyUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="false">
            <%-- HiddenFields --%>
            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
            <%-- /HiddenFields --%>
            <eUI:eFormPanel ID="eBeadvanyForm" runat="server">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tbody>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelKR_HivatkozasiSzam" runat="server" Text="<%$Forditas:labelKR_HivatkozasiSzam|Hivatkozási szám:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="KR_HivatkozasiSzam" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_DokTipusHivatal" runat="server" Text="<%$Forditas:labelKR_DokTipusHivatal|Hivatal:%>" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_DokTipusHivatal" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_DokTipusAzonosito" runat="server" Text="<%$Forditas:labelKR_DokTipusAzonosito|Típus:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_DokTipusAzonosito" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_DokTipusLeiras" runat="server" Text="<%$Forditas:labelKR_DokTipusLeiras|Típus név:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_DokTipusLeiras" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_Megjegyzes" runat="server" Text="<%$Forditas:labelKR_Megjegyzes|Megjegyzés:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_Megjegyzes" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_Valaszutvonal" runat="server" Text="<%$Forditas:labelKR_Valaszutvonal|Válasz útvonal:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:KodtarakDropDownList ID="KR_Valaszutvonal" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_FileNev" runat="server" Text="<%$Forditas:labelKR_FileNev|Fájlnév:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="KR_FileNev" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKR_Valasztitkositas" runat="server" Text="<%$Forditas:labelKR_Valasztitkositas|Válasz titkosítás:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="KR_Valasztitkositas" runat="server" CssClass="urlapCheckbox"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelInputFormat" runat="server" Text="<%$Forditas:labelInputFormat|Bemeneti formátum:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="InputFormat" runat="server" CssClass="mrUrlapInput" ReadOnly="true"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelOutputFormat" runat="server" Text="<%$Forditas:labelOutputFormat|Kimeneti formátum:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="OutputFormat" runat="server" CssClass="mrUrlapInput" ReadOnly="true"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelOutputFormatDefinition" runat="server" Text="<%$Forditas:labelOutputFormatDefinition|Kimeneti formátum definíció:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <div class="mrUrlapInput" style="max-height:200px;margin-top:5px; overflow:auto;">
                                    <asp:label ID="OutputFormatDefinition" runat="server" ForeColor="Black" />
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br />
                <%-- TabFooter --%>
                <uc:TabFooter ID="TabFooter1" runat="server" />
                <%-- /TabFooter --%>
            </eUI:eFormPanel>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>