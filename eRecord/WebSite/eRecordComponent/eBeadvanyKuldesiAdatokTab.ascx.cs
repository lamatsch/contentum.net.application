﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eRecordComponent_eBeadvanyKuldesiAdatokTab : System.Web.UI.UserControl
{
    private const string kcs_Valaszutvonal = KodTarak.EBEADVANY_KODCSOPORTOK.VALASZUTVONAL;

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    public string Command = "";

    private PageView pageView = null;

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(QueryStringVars.Command);

        ParentId = Request.QueryString.Get(QueryStringVars.Id);

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
    }

    public void ReLoadTab()
    {
        ReLoadTab(true);
    }

    private void ReLoadTab(bool loadRecord)
    {
        // nem lehet New
        if (Command == CommandName.New)
        {
            return;
        }

        if (_ParentForm == Constants.ParentForms.IraIrat)
        {
            if (!MainPanel.Visible) return;

            if (Command == CommandName.View || Command == CommandName.Modify)
            {

                if (String.IsNullOrEmpty(ParentId))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                    ErrorUpdatePanel1.Update();
                }
                else
                {
                    if (loadRecord)
                    {
                        // rekord betöltése

                        EREC_eBeadvanyokService service = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();

                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        Result resGet = service.GetKuldesiAdatok(execParam, ParentId);

                        if (String.IsNullOrEmpty(resGet.ErrorCode))
                        {
                            EREC_eBeadvanyok eBeadvany = resGet.Record as EREC_eBeadvanyok;

                            if (eBeadvany == null)
                            {
                                eBeadvany = new EREC_eBeadvanyok();
                            }

                            LoadComponentsFromBusinessObject(eBeadvany);
                        }
                        else
                        {
                            // nem sikerült lekérdezni
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resGet);
                            ErrorUpdatePanel1.Update();
                        }

                    }
                }
            }


            // Ez az egyes tab fulek belso commandjat hatarozza meg!
            TabFooter1.CommandArgument = Command;

            if (Command == CommandName.DesignView)
            {
                Load_ComponentSelectModul();
            }
            else
            {
                pageView.SetViewOnPage(Command);
            }

            // Komponensek láthatóságának beállítása:
            SetComponentsVisibility(Command);
        }
        else
        {
            // ha egyéb oldalra lenne betéve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }

    }

    /// <summary>
    /// Komponensek láthatóságának beállítása
    /// </summary>
    private void SetComponentsVisibility(String _command)
    {
        #region TabFooter gombok állítása

        if (_command == CommandName.Modify)
        {
            TabFooter1.ImageButton_Save.Visible = true;
            TabFooter1.ImageButton_SaveAndClose.Visible = true;
            TabFooter1.ImageButton_SaveAndNew.Visible = false;
        }

        // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
        TabFooter1.ImageButton_Cancel.Visible = false;

        #endregion



        #region Form komponensek (TextBoxok,...) állítása


        // A gombok láthatósága alapból a Modify esetre van beállítva,
        // View-nál pluszban még azokat kell letiltani, amik módosításnál látszanának
        if (_command == CommandName.View)
        {
            this.SetViewControls();
        }

        #endregion

    }

    private void SetViewControls()
    {
        KR_HivatkozasiSzam.ReadOnly = true;
        KR_DokTipusHivatal.ReadOnly = true;
        KR_DokTipusAzonosito.ReadOnly = true;
        KR_DokTipusLeiras.ReadOnly = true;
        KR_Megjegyzes.ReadOnly = true;
        KR_FileNev.ReadOnly = true;
        KR_Valaszutvonal.ReadOnly = true;
        KR_Valasztitkositas.Enabled = false;
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_eBeadvanyok EREC_eBeadvanyok)
    {
        KR_HivatkozasiSzam.Text = EREC_eBeadvanyok.KR_HivatkozasiSzam;
        KR_DokTipusHivatal.Text = EREC_eBeadvanyok.KR_DokTipusHivatal;
        KR_DokTipusAzonosito.Text = EREC_eBeadvanyok.KR_DokTipusAzonosito;
        KR_DokTipusLeiras.Text = EREC_eBeadvanyok.KR_DokTipusLeiras;
        KR_Megjegyzes.Text = EREC_eBeadvanyok.KR_Megjegyzes;
        KR_Valaszutvonal.FillAndSetSelectedValue(kcs_Valaszutvonal, EREC_eBeadvanyok.KR_Valaszutvonal, true, EErrorPanel1);
        KR_FileNev.Text = EREC_eBeadvanyok.KR_FileNev;
        KR_Valasztitkositas.Checked = EREC_eBeadvanyok.KR_Valasztitkositas == "1" ? true : false;

        PR_Parameterek PR_Parameterek = EREC_eBeadvanyok.Get_PR_Parameterek();
        InputFormat.Text = PR_Parameterek.InputFormat;
        OutputFormat.Text = PR_Parameterek.OutputFormat;
        OutputFormatDefinition.Text = HttpUtility.HtmlEncode(PR_Parameterek.OutputFormatDefinition);

        Record_Ver_HiddenField.Value = EREC_eBeadvanyok.Base.Ver;
    }

    // form --> business object
    private EREC_eBeadvanyok GetBusinessObjectFromComponents()
    {
        EREC_eBeadvanyok EREC_eBeadvanyok = new EREC_eBeadvanyok();
        EREC_eBeadvanyok.Updated.SetValueAll(false);
        EREC_eBeadvanyok.Base.Updated.SetValueAll(false);

        EREC_eBeadvanyok.KR_HivatkozasiSzam = KR_HivatkozasiSzam.Text;
        EREC_eBeadvanyok.Updated.KR_HivatkozasiSzam = true;
        EREC_eBeadvanyok.KR_DokTipusHivatal = KR_DokTipusHivatal.Text;
        EREC_eBeadvanyok.Updated.KR_DokTipusHivatal = true;
        EREC_eBeadvanyok.KR_DokTipusAzonosito = KR_DokTipusAzonosito.Text;
        EREC_eBeadvanyok.Updated.KR_DokTipusAzonosito = true;
        EREC_eBeadvanyok.KR_DokTipusLeiras = KR_DokTipusLeiras.Text;
        EREC_eBeadvanyok.Updated.KR_DokTipusLeiras = true;
        EREC_eBeadvanyok.KR_Megjegyzes = KR_Megjegyzes.Text;
        EREC_eBeadvanyok.Updated.KR_Megjegyzes = true;
        EREC_eBeadvanyok.KR_Valaszutvonal = KR_Valaszutvonal.SelectedValue;
        EREC_eBeadvanyok.Updated.KR_Valaszutvonal = true;
        //EREC_eBeadvanyok.KR_FileNev = KR_FileNev.Text;
        //EREC_eBeadvanyok.Updated.KR_FileNev = true;
        EREC_eBeadvanyok.KR_Valasztitkositas = KR_Valasztitkositas.Checked ? "1" : "0";
        EREC_eBeadvanyok.Updated.KR_Valasztitkositas = true;


        EREC_eBeadvanyok.Base.Ver = Record_Ver_HiddenField.Value;
        EREC_eBeadvanyok.Base.Updated.Ver = true;

        //PR_Parameterek PR_Parameterek = new PR_Parameterek();
        //PR_Parameterek.InputFormat = InputFormat.Text;
        //PR_Parameterek.OutputFormat = OutputFormat.Text;
        //PR_Parameterek.OutputFormatDefinition = OutputFormatDefinition.Text;

        //EREC_eBeadvanyok.Set_PR_Parameterek(PR_Parameterek);
        //EREC_eBeadvanyok.Updated.PR_Parameterek = true;

        return EREC_eBeadvanyok;
    }

    private void Load_ComponentSelectModul()
    {
    }

    private void TabFooterButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save ||
          e.CommandName == CommandName.SaveAndClose)
        {
            if (true)
            {
                bool voltHiba = false;

                switch (Command)
                {
                    case CommandName.New:
                        {
                            // New nem lehetséges
                            return;
                        }
                    case CommandName.Modify:
                        {
                            if (String.IsNullOrEmpty(ParentId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                            }
                            else
                            {
                                EREC_eBeadvanyokService service = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
                                ExecParam execparam = UI.SetExecParamDefault(Page);

                                EREC_eBeadvanyok eBeadvany = GetBusinessObjectFromComponents();

                                Result result = service.SetKuldesiAdatok(execparam, ParentId, eBeadvany);
                                if (!String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    // hiba
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel1.Update();
                                    voltHiba = true;
                                }
                            }

                            if (!voltHiba)
                            {

                                if (e.CommandName == CommandName.Save)
                                {
                                    Util.IncrementHiddenFieldValue(Record_Ver_HiddenField);
                                    ReLoadTab(false);
                                    return;
                                }
                                else if (e.CommandName == CommandName.SaveAndClose)
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, ParentId);
                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                                }
                            }


                            break;

                        }
                }   // switch
            }
            else
            {
                // TODO: right check? UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
            }
        }
    }
}