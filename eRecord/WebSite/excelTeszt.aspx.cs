using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.IO;

public partial class excelTeszt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FileStream fs = new FileStream("c:\\Documents and Settings\\kiss.gergely\\Asztal\\asdf.xls", FileMode.Open, FileAccess.Read);
        BinaryReader br = new BinaryReader(fs);

        byte[] fileRD = br.ReadBytes((int)fs.Length);

        br.Close();
        fs.Close();

        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result result = service.GetAllWithExtension(execParam, erec_KuldKuldemenyekSearch);

        string xml = result.Ds.GetXml();

        Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
        result = tms.GetExcelDocument(fileRD, xml, false);

        byte[] res = (byte[])result.Record;

        EREC_UgyUgyiratokService service1 = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

        Result result1 = service1.GetAllWithExtension(execParam, erec_UgyUgyiratokSearch);

        string xml1 = result1.Ds.GetXml();

        result1 = tms.GetExcelDocument(res, xml1, false);

        byte[] res1 = (byte[])result1.Record;

        Response.Clear();
        Response.ClearContent();
        Response.ClearHeaders();
        Response.ContentType = "application/vnd.ms-excel";
        /*Response.AddHeader("content-disposition", "inline; filename="+"vezetoi_stat"+".xls;");
        Response.AddHeader("Cache-Control", " ");
        Response.AddHeader("Expires", " Mon, 26 Jul 1997 05:00:00 GMT");
        Response.AddHeader("Last-Modified", " Mon, 25 Jul 1997 05:00:00 GMT");
        Response.AddHeader("Cache-Control", "post-check=0, pre-check=0, max-age=0");*/
        
        Response.OutputStream.Write(res1, 0, res.Length);
        Response.OutputStream.Flush();
        Response.End();
    }
}
