﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="gyak.aspx.cs" Inherits="gyak" EnableEventValidation="false"%>

<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc" %>
<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox"
    TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox"
TagPrefix="uc" %>    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>teszt</title>
    <script src="Javascripts\jquery.js" type="text/javascript"></script>
</head>
<body>

    <form id="mainForm" runat="server">
    <div>
        <div>
            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager >
            <ajaxToolkit:TabContainer ID="ListTabContainer" runat="server" Width="100%"
                    OnActiveTabChanged="ListTabContainer_ActiveTabChanged"
                    ActiveTabIndex="0" AutoPostBack="true">   
                         
                    <ajaxToolkit:TabPanel ID="TabUgyiratPanel" runat="server" TabIndex="0">
                        <HeaderTemplate>
                            <asp:Label ID="Label1" Text="Ügyiratok" runat="server"/>
                        </HeaderTemplate>
                        <ContentTemplate>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    
                    <ajaxToolkit:TabPanel ID="TabKuldemenyekPanel" runat="server" TabIndex="1">
                        <HeaderTemplate>
                            <asp:Label ID="Label3" Text="Küldemények" runat="server"/>
                        </HeaderTemplate>
                        <ContentTemplate>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="TabIratpeldanyokPanel" runat="server" TabIndex="2">
                        <HeaderTemplate>
                            <asp:Label ID="Label4" Text="Iratpéldányok" runat="server" />
                        </HeaderTemplate>
                        <ContentTemplate>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                                                            
                </ajaxToolkit:TabContainer>
        </div>
    </form>
</body>
</html>