﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Reflection;
using System.Globalization;
using System.Threading;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eRecord.Service;
using System.Diagnostics;
using log4net.Core;
using System.Text;
using System.Net;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Security.Principal;
using Contentum.eRecord.Utility;
using Contentum.eDocument.Service;
using System.Collections.Generic;
using System.Xml;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;

public partial class gyak : Contentum.eUtility.UI.PageBase
{

    protected void Page_Init(object sender, EventArgs e)
    {
        Authentication.CheckLogin(Page);
        ScriptManager1.RegisterAsyncPostBackControl(ListTabContainer);
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void ListTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
    }

}
