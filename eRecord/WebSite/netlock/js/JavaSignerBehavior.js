﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

function checkCertificate(serialnumber) {
        YAHOO.log("serialnumber: " + serialnumber, "info", "NlJavaSigner");
        //alert("kapott serial number:" + serialnumber);
        return 1;
    }

Type.registerNamespace("Netlock");

Netlock.JavaSignerBehavior = function (element) {
    Netlock.JavaSignerBehavior.initializeBase(this, [element]);

    // Beállítható paraméterek
    this.baseDownloadURL = "./netlock/applet";
    this.signatureType = "NlPDFClientServer";
    // nyelv és az ország (lokalizáció)
    this.localeLanguage = 'hu';
    this.localeCountry = 'HU';
    this.NlServerPostURL = 'http://ax-vfphtst03/cgi-bin/upload.cgi';
    //NetLock Server feldolgozó script címe

    //********************************* GLOBÁLIS VÁLTOZÓK, KONFIGURÁCIÓS BEÁLLÍTÁSOK ********************************/
    this.InitialisationSuccesfull = false;
    this.YCM = null; //YUI connection manager objektuma
    this.YUIConnectionSWF = './netlock/js/yui/assets/connection.swf';
    this.LogEnabled = false; //Loggolás YUI Loggerbe és böngésző konzolára
    this.LogWindowVisible = false //YUI Logger látható
    this.JAVACheckOK = false; //JAVA meglétének és verziójának ellenőrzése
    this.javaversion = '1.7.0_65'; //támogatott JAVA verzió
    this.AppletFullyLoaded = false; //ha az applet teljesen betöltődött, akkor értéke "true" lesz
    this.AppletTimer = null;
    this.NlJAVASignerApplet; //NetLock JAVA aláíró modul appletje
    this.appletNotFoundText;
    this.defaultAppletNotFoundText = "Az applet még nem töltődött be, kérjük várjon egy kicsit és próbálja meg újra";
    this.UseFlashTransport = false; //Használja-e a YUI Connection Manager a Flash plugint a kommunikációhoz - cross-domain (eltérő domianek közötti kommunikáció) esetén mindenképpen true-nak kell lennie
    this.CommunicationTimeOut = 120000; //NetLock serverrel történő kommunikáció során használt timeout ms-ban megadva
    this.LogLevel = "OFF"; //JAVA Console Loggolás szintje: ERROR, WARN, INFO, DEBUG, OFF

    this.HashName = 'SHA256'; //PDF aláíráshoz használt hash algoritmus neve - SHA1, SHA256
    this.allKeyUsagesFilter = false; //false: csak aláíró tanúsítványok jelenjenek meg

    this.useBrowserKeystore = false;
    this.useIExplorerKeystore = true;
    this.useMozillaKeystore = true;

    this.PDFSignType = "NlPDFClientServer"; //PDF aláíró típus konstans
    this.PDFURLMode = true; //PDF URL mód

    //Folyamatjelző sáv során használt változók
    this.ProgressBarValue = 0;
    this.ProgressBarProcessing = false;
    this.AppletCheckProcessing = false;
    this.IncreasingProgress = false;
    this.readtimerIntervalId = null;
    this.appletIntervalId = null;

    //Belső kommunikációs változók
    this.SignerCert = ""; //aláíró tanúsítvány base64-es formában
    this.SignedData = ""; //aláírt adat
    this.SignableData = ""; //aláírandó adat
    this.SourceData = ""; //server oldali aláírandó adat
    this.PKCS7 = ""; //PKCS#7-es objektum base64-es formában
    this.DocId = ""; //dokumentum azonosító
    this.PDFIdentifier = ""; //PDF belső azonosító

    //egyéb paraméterek
    this.signbutton = this.get_element();
    this.contentToSign = null;
    this.asyncPostBackUrl = null;
    this.fileToSign = null;
    this.clickHandler = null;
    this.iratId = null;
    this.procId = null;
    this.filesToSign = null;

}

Netlock.JavaSignerBehavior.prototype =
{

    initialize: function () {
        Netlock.JavaSignerBehavior.callBaseMethod(this, 'initialize');

        try {
            this.clickHandler = Function.createDelegate(this, this.onClick);
            $addHandler(this.signbutton, 'click', this.clickHandler);

            //jQuery gombbá alakítjuk a szabványos html gombokat
            //$(this.signbutton).button();
            this.signbutton.disabled = true;

            this.SetupDialogs();

            //YUI inicializálása
            this.InitYUI();

            //FLASH ellenőrzés: csak cross-domain kezelésnél szükséges
            if (!this.checkFLASH()) {
                this.DisplayFlashErrorDialog();
                return;
            }

            //JAVA ellenőrzése
            if (!this.CheckJAVA()) {
                return;
            }

            //-- Ha a Java verzió nem legalább 1.6.0_10, megjelenítjük a figyelmeztető ablakot	
            if (deployJava.versionCheck("1.7.0_65+")) {
                this.loadSignatureApplet(false);
            } else {
                $("#divWarning").dialog('open');
            }

            this.LogInfo("Start loading the applet end");

            this.InitialisationSuccesfull = true;

            //sign ();

        }
        catch (err) {
            this.InitialisationSuccesfull = false;

            this.HandleException(err, "Hiba történt a NetLock JAVA aláíró modul inicializálása közben.\n\n");
        }

    },

    dispose: function () {

        if (this.clickHandler) {
            $removeHandler(this.signbutton, 'click', this.clickHandler);
            this.clickHandler = null;
        }

        this.NlJAVASignerApplet = null;

        Netlock.JavaSignerBehavior.callBaseMethod(this, 'dispose');
    },

    Log: function (msg, level) {
        if ((this.LogEnabled == true) && (YAHOO != null)) {
            YAHOO.log(msg, level, "NlJavaSigner");
        }
    },

    LogError: function (msg) {
        if ((this.LogEnabled == true) && (YAHOO != null)) {
            YAHOO.log(msg, "error", "NlJavaSigner");
        }
    },

    LogInfo: function (msg) {
        if ((this.LogEnabled == true) && (YAHOO != null)) {
            YAHOO.log(msg, "info", "NlJavaSigner");
        }
    },

    //Új deploy.js esetén szükséges módosítás
    docWriteWrapper: function (func) {
        var writeTo = document.createElement('del'),
      oldwrite = document.write,
      content = '';
        writeTo.id = "NlJAVASignerAppletWrapper";
        try {
            document.write = function (text) {
                content += text;
            }
            func();
            writeTo.innerHTML += content;
        } finally {
            document.write = oldwrite;
        }
        document.body.appendChild(writeTo);
    },

    CheckAppletLoaded: function () {
        try {
            if ((this.NlJAVASignerApplet == undefined) || (this.NlJAVASignerApplet == null)) {
                this.LogInfo("Applet még nem töltődött be, applet objektum lekérdezése");
                this.NlJAVASignerApplet = document.getElementById("NlJAVASignerApplet");
            }
            this.AppletFullyLoaded = (this.NlJAVASignerApplet != undefined) && (this.NlJAVASignerApplet != null);

            //if (this.AppletFullyLoaded && !IsIE8) {
            if (this.AppletFullyLoaded) {
                try {
                    this.NlJAVASignerApplet.getJavaVersion();
                    this.AppletFullyLoaded = true;
                } catch (ex) {
                    this.AppletFullyLoaded = false;
                }
            }
            //this.AppletFullyLoaded = this.AppletFullyLoaded && (this.NlJAVASignerApplet.status < 2);
        } catch (e) {
            this.AppletFullyLoaded = false;
        }
        if (this.AppletFullyLoaded) {
            //this.LogInfo("Clearing Applet timer id: " + this.AppletTimer);
            //window.clearInterval(this.AppletTimer);    
            this.LogInfo("Applet sikeresen betöltődött");
            this.hideUpdateProgress();
        } else {
            this.LogInfo("Applet még nem töltődött be, timer újra aktíválása");
            this.AppletTimer = window.setTimeout(Function.createDelegate(this, this.CheckAppletLoaded), 1000);
        }
    },

    /*
    * Aláíró applet betöltése
    */
    loadSignatureApplet: function (force) {

        this.LogInfo("Start loading the applet");
        this.showUpdateProgress("Applet betöltése folymatban...");

        // Ha az applet már be van töltve, nem lépünk tovább
        if ((force == false) && (this.NlJAVASignerApplet != undefined)) {
            return;
        }

        var attributes = { id: 'NlJAVASignerApplet',
            width: 1,
            height: 1
        };

        jnlpfile = 'arangiClientSignature.jnlp'; //XAdES támogatás
        if (this.IsPDFMode()) {
            jnlpfile = 'arangiClientSignature_pdf.jnlp'; //XAdES támogatás nincs, csak PDF aláírás -> optimálisabb betöltés, kevesebb JAR szükséges
        }

        //var parameters = {jnlp_href:baseDownloadURL + '/' + 'arangiClientSignature.jnlp',
        var parameters = {
            jnlp_href: this.baseDownloadURL + '/' + jnlpfile,
            separate_jvm: true,
            language: this.localeLanguage,
            country: this.localeCountry,
            userAgent: window.navigator.userAgent,
            mayscript: true
        };
        if (deployJava.versionCheck("1.7.0_65+") == true) {
            parameters.java_vm_args = '-Djnlp.packEnabled=true -Xms512M -Xms512M -Xmx512M';
        }
        else {
            parameters.java_arguments = '-Djnlp.packEnabled=true -Xms512M -Xms512M -Xmx512M';
        }

        this.LogInfo("before runApplet");

        //Új deploy.js miatt szükséges módosítás
        //http://stackoverflow.com/questions/13517790/using-deployjava-runapplet-to-target-specific-element
        this.docWriteWrapper(function () {
            try {
                deployJava.runApplet(attributes, parameters, this.javaversion);
            } catch (ex) {
                this.HandleException(ex, null);
            }
        });

        this.LogInfo("after runApplet");

        this.LogInfo(this.NlJAVASignerApplet);

        //másodpercenként ellenőrizzük, hogy az applet teljesen betöltődött-e
        //this.AppletTimer = window.setInterval(this.CheckAppletLoaded, 1000);  
        this.AppletTimer = window.setTimeout(Function.createDelegate(this, this.CheckAppletLoaded), 1000);
        this.LogInfo("Applet timer id: " + this.AppletTimer);
    },

    //Event handler called when the transaction begins:
    handleStart: function (e, a) {
        this.LogInfo("Kommunikáció a NetLock serverrel. Tranzakció " + a[0].tId + " elkezdődött.");
    },

    //Event handler for the success event 
    handleSuccess: function (o) {
        this.LogInfo("A servertől kapott válasz: " + o.responseText);
		//JSON válasz parsolása objektummá
        var Reply = undefined;
        try {
            Reply = YAHOO.lang.JSON.parse(o.responseText);
        } catch (err) {
            Reply = undefined;
        }

        if (Reply) {
            this.LogInfo("A servertől kapott válasz: " + o.responseText);

            //Sikeres aláírás történt
            if ((Reply.statuscode != undefined) && (Reply.statuscode == 0) && (this.SignedData != "") && (Reply.docid == this.DocId)) {
                if (Reply.signeddata != undefined) {
                    this.LogInfo("Sikeres PDF aláírás, azonosító: " + Reply.signeddata);
                    $('#SourceData').attr('value', this.SourceData);
                    $('#signeddata').attr('value', Reply.signeddata);

                    this.ShowSuccessfullDialog(1);
                } else {
                    this.LogError("Sikertelen PDF aláírás");
                }
                return true;
            }

            //Server oldali hiba történt
            if ((Reply.statuscode != undefined) && (Reply.statuscode != 0)) {
                msg = "Hiba történt a server oldali feldolgozás során. Hibakód: " + Reply.statuscode + " Hibaüzenet:" + Reply.statustext;
                this.LogError(msg);
                this.CloseDialogStopProgress(msg);
                return false;
            }

            //Dokumentum azonosító hiba
            if (Reply.docid != this.DocId) {
                msg = "Hiba történt a server oldali feldolgozás során, a kapott válasz dokumentum azonosítója nem egyezik meg a küldöttel.";
                this.LogError(msg);
                this.CloseDialogStopProgress(msg);
                return false;
            }

            this.LogInfo("this.PKCS7: " + Reply.pkcs7);
            this.LogInfo("DocID: " + Reply.docid);
            this.LogInfo("this.SignableData: " + Reply.signabledata);
            this.LogInfo("this.PDFIdentifier: " + Reply.pdfidentifier);
            this.LogInfo("this.SourceData: " + Reply.sourcedata);

            //Ha még nem volt aláírás, akkor aláírunk
            if (this.SignableData == "") {
                this.SignableData = Reply.signabledata;
                this.PDFIdentifier = Reply.pdfidentifier;
                this.PKCS7 = Reply.pkcs7;

                this.NlJAVASignerApplet.setContentToSign(this.SignableData, null);

                this.SignedData = this.NlJAVASignerApplet.makePKCS1();
                this.LogInfo("Aláírt PKCS#1 adat: " + this.SignedData, "info", "NlJavaSigner");
                if ((this.SignedData != null) && (this.SignedData != "")) {
                    this.PostData(this.NlServerPostURL, '', this.DocId, 'pdf', this.SignerCert, '', this.PKCS7, this.SignedData, this.PDFIdentifier);
                } else {
                    msg = "Hiba az aláírás készítése során.";
                    this.LogError(msg);
                    this.CloseDialogStopProgress(msg);
                }
            }

        } else {
            this.LogError("Nem kaptunk vissza értelmes választ a servertől.");
            this.CloseDialogStopProgress("Nem kaptunk vissza értelmes választ a servertől.");
        }
    },

    //In the event that the HTTP status returned is > 399, a
    //failure is reported and this function is called:
    handleFailure: function (o) {
        msg = "";
        if (o.status == -1) {
            msg = "A NetLock serverrel történő kommunikáció megszakadt.";
        } else if (o.status == 0) {
            msg = "A NetLock serverrel történő kommunikáció során hiba történt.";
        } else {
            msg = "A NetLock serverrel történő kapcsolat során hiba történt.";
        }
        msg = msg + ' - ' + o.statusText;
        this.LogError("HIBA - Failure: " + o.tId + " - " + o.status + " -" + msg);
        this.CloseDialogStopProgress(msg);
    },

    //Set up the callback object used for the transaction.
    callback: {
        success: this.handleSuccess,
        failure: this.handleFailure,
        timeout: this.CommunicationTimeOut,
        xdr: this.UseFlashTransport
    },

    //Elküldi az adatokat a NetLock servernek  
    PostData: function (url, sourcedata, docid, doctype, cert, hashname, pkcs7, pkcs1, Identifier) {
        //JSON objektum összeállítása
        myData = {
            doctype: doctype,
            docid: docid,
            cert: cert
        };

        if (sourcedata != '') {
            myData.sourcedata = sourcedata;
        }
        if (Identifier != '') {
            myData.pdfidentifier = Identifier;
        }
        if (cert != '') {
            myData.cert = cert;
        }
        if (hashname != '') {
            myData.hashname = hashname;
        }
        if (pkcs7 != '') {
            myData.pkcs7 = pkcs7;
        }
        if (pkcs1 != '') {
            myData.pkcs1 = pkcs1;
        }

        try {
            jsonStr = YAHOO.lang.JSON.stringify(myData);
            this.LogInfo("JSON adat postolása NetLock servernek");
            var obj = this.YCM.asyncRequest('POST', url, this.callback, jsonStr);
        } catch (err) {
            this.CloseDialogStopProgress('');
            this.HandleException(err, "Hiba az adatok küldése során.");
        }
    },

    //Egyedi dokumentum azonosító generálása
    GenerateDocID: function () {
        //var currentDate = new Date();
        //GeneratedDocID = "NlJavaSigner_" + currentDate.toString("yyyy'-'mm'-'dd'T'HH':'mm':'ss") + 'R' + (Math.floor(Math.random()*100)+1);
        GeneratedDocID = "NlJavaSigner_" + dateFormat(new Date(), "yyyy-mm-dd'T'HH-MM-ss") + 'R' + (Math.floor(Math.random() * 100) + 1);
        return GeneratedDocID;
    },

    StopProgressBar: function () {
        clearInterval(this.readtimerIntervalId);
    },

    CloseDialogStopProgress: function (errtxt) {
        this.StopProgressBar();

        if (errtxt != "") {
            //setter
            $("#ProgressDialog").dialog("option", "buttons", [
          {
              text: "Bezárás",
              click: function () { $(this).dialog("close"); }
          }
        ]);
            $("#ProgressDialog").dialog("option", "title", 'Az aláírás során hiba történt');

            $('#errortext').html(errtxt);
            $('#progressblock').hide();
            $('#errorblock').show();
        } else {
            $("#ProgressDialog").dialog('close');
        }
    },

    ShowSuccessfullDialog: function (closedlg) {
        if (closedlg == 1) {
            this.CloseDialogStopProgress("");
        }
        $("#SuccesDialog").dialog({ modal: true, width: 500, show: 'fade', hide: 'slide', closeText: 'Bezárás', buttons: { "OK": function () { $(this).dialog("close"); } } });
    },

    ProgressBarCallback: function () {
        if (this.ProgressBarProcessing) {
            return true;
        }
        this.ProgressBarProcessing = true;
        try {
            if (this.IncreasingProgress) {
                this.ProgressBarValue = this.ProgressBarValue + 10;
            } else {
                this.ProgressBarValue = this.ProgressBarValue - 10;
            }
            if (this.IncreasingProgress && this.ProgressBarValue >= 100) {
                this.IncreasingProgress = false;
            } else if ((this.IncreasingProgress == false) && this.ProgressBarValue <= 0) {
                this.IncreasingProgress = true;
            }

            $("#progressbar").progressbar({ value: this.ProgressBarValue });
        } finally {
            this.ProgressBarProcessing = false;
        }
        return true;
    },

    SetProgressbar: function () {
        var _this = this;
        this.readtimerIntervalId = setInterval(function () { _this.ProgressBarCallback(); }, 1000);
    },

    IsPDFMode: function () {
        return (this.signatureType == "NlPDFClientServer");
    },

    IsPDFURLMode: function () {
        return ((this.signatureType == "NlPDFClientServer") && this.PDFURLMode);
    },

    //XAdES aláírás
    xades_sign: function () {
        var signatureB64 = this.sign();
        if ((signatureB64 != null) && (signatureB64 != false)) {
            $("#divResult").html(signatureB64);
            $("#divResult").dialog('open');
        }
    },

    HandleException: function (err, msg) {
        if (msg != null) {
            txt = msg;
        } else {
            txt = "Hiba történt a NetLock JAVA aláíró modul meghívása során.\n\n";
        }
        txt += "Hiba leírása: " + err.message + "\n\n";
        txt += "A folytatáshoz kattintson az OK gombra.\n\n";

        this.handleError(txt);

        this.LogError(txt);
    },

    HandleAppletNotLoaded: function () {
        this.handleError("HIBA! A NETLOCK aláíró modul nem érhető el (nincs teljesen betöltve ill. a Java nincs engedélyezve az oldalon)!");
    },



    send: function (o) {
        if (!this.AppletCallable()) {
            this.HandleAppletNotLoaded();
            return;
        }

        try {
            this.LogInfo("JAVA verzió: " + this.NlJAVASignerApplet.getJavaVersion());
            this.LogInfo("NetLock JAVA FormSigning verzió: " + this.NlJAVASignerApplet.getVersion());
        } catch (ex) {
            //IE bug
            if (ex.message == "Target JVM seems to have already exited") {
                this.HandleException(ex, "Belső Internet Explorer hiba az aláíró modul (applet) elérése során. Kérjük, zárja be a böngészőt, majd próbálja újra a műveletet.");
            } else {
                this.HandleException(ex, "Hiba az aláíró modul (applet) elérése során. Kérjük, zárja be a böngészőt, majd próbálja újra a műveletet.");
            }
            return;
        }

        if (this.IsPDFURLMode()) {
            this.SourceData = this.contentToSign; //aláírandó PDF fájl azonosító
            if (this.SourceData == "") {
                this.handleError("Kérjük, töltse ki a PDF letöltési URL-jét!");
                return false;
            }
            //PDF aláírás
            for (var i = 0; i < this.filesToSign.length; i++) {
                this.fileToSign = this.filesToSign[i];

                if (i == (this.filesToSign.length - 1)) {
                    this.fileToSign.IsLastFile = true;
                }
                else {
                    this.fileToSign.IsLastFile = false;
                }

                if (!this.sign()) {
                    return false;
                }
            }
            
            return true;
        }

        if (!this.IsPDFMode()) {
            //XAdES aláírás
            this.xades_this.sign();
            return;
        } else {
            this.SourceData = this.contentToSign; //aláírandó PDF fájl azonosító
            if (this.SourceData == "") {
                this.handleError("Kérjük, töltse ki a PDF dokumentum azonosítót!");
                return false;
            }

            //PDF aláírás előkészítés
            if (!this.sign()) {
                return false;
            }
        }

        //PDF aláírás
        this.LogInfo("Aláírás folyamat kezdete, kommunikáció kezdete a NL serverrel.");

        //Folyamatjelző ablak inicializálás
        $("#ProgressDialog").dialog({ modal: true, width: 500, title: 'Aláírás folyamatban', closeText: 'Bezárás', autoOpen: false, show: 'blind', hide: 'slide',
            buttons: [
                {
                    text: "Mégsem",
                    click: function () { $(this).dialog("close"); }
                }
              ]
        });
        $("#progressbar").progressbar({ value: 0 });
        $('#errorblock').hide();
        $('#progressblock').show();
        $("#ProgressDialog").dialog("open");

        this.LogInfo("Állapotjelző dialógusablak betöltődött");

        this.SetProgressbar();

        this.ClearVariables();

        //Most kezdődik az aláírási folyamat
        if (this.SignableData == "") {
            this.SourceData = this.contentToSign.value; //aláírandó PDF fájl azonosító
            this.DocId = this.GenerateDocID();

            try {
                this.SignerCert = this.NlJAVASignerApplet.selectCertificate(); //tanúsítványkiválasztás
            } catch (err) {
                this.HandleException(err);
                this.CloseDialogStopProgress('');
                return;
            }

            if (this.SignerCert != null) {
                serial = this.NlJAVASignerApplet.GetSerialNumberFromCertificate();
                alert(serial);
                this.PostData(this.NlServerPostURL, this.SourceData, this.DocId, 'pdf', this.SignerCert, this.HashName, this.PKCS7, this.SignedData);
            } else {
                this.CloseDialogStopProgress('');
            }
        }
    },

    //Változók törlése
    ClearVariables: function () {
        this.SignerCert = "";
        this.SignedData = "";
        this.SignableData = "";
        this.PKCS7 = "";
        this.DocId = "";
        this.PDFIdentifier = "";
        this.SourceData = "";
    },

    //YUI inicializálása
    InitYUI: function () {
        this.YCM = YAHOO.util.Connect;

        // Loggolás kezelése
        if (this.LogEnabled == true) {
            if (this.LogWindowVisible == true) {
                this.myLogReader = new YAHOO.widget.LogReader();
            }
            YAHOO.widget.Logger.enableBrowserConsole();
        }

        //Listen for Connection Manager's start event.
        this.YCM.startEvent.subscribe(this.handleStart);

        if (this.UseFlashTransport) {
            //Initialize the Flash transport.
            //this.YCM.transport(this.YUIConnectionSWF + '?t=' + new Date().valueOf().toString()); //IE miatt kell a dátum a végére  
            this.YCM.transport(this.YUIConnectionSWF); //IE miatt kell a dátum a végére  

            //Az aláíró gomb engedélyezése, ha a YUI Flash kommunikációs objektum betöltődött    
            this.YCM.xdrReadyEvent.subscribe(function () {
                this.signbutton.disabled = false;
                //$(this.signbutton).button("enable");
                this.LogInfo("YUI Flash transport inicializálódott");
            });
        } else {
            //$(this.signbutton).button("enable");
            this.signbutton.disabled = false;
        }
    },

    ShowErrorDlg: function (errtxt) {
        $("#ProgressDialog").dialog({ modal: true, width: 500, 'title': 'Hiba', closeText: 'Bezárás', show: 'blind', hide: 'slide',
            buttons: [
                      {
                          text: "Bezár",
                          click: function () { $(this).dialog("close"); }
                      }
                    ]
        });
        this.CloseDialogStopProgress(errtxt);
    },

    DisplayFlashErrorDialog: function () {
        $('#main_cont').hide();
        $('#flash_not_installed').show();
        $("#FlashErrorDialog").dialog({ modal: true, width: 500, closeText: 'Bezárás', show: 'fade', hide: 'slide',
            buttons: { "OK": function () {
                $(this).dialog("close");
            }
            }
        });
    },

    //FLASH ellenőrzés: csak cross-domain kezelésnél szükséges
    checkFLASH: function () {
        try {
            if (!this.UseFlashTransport) {
                return true;
            }
            var playerVersion = swfobject.getFlashPlayerVersion();
            //var output = "You have Flash player " + playerVersion.major + "." + playerVersion.minor + "." + playerVersion.release + " installed";       

            if (playerVersion.major >= 10) {
                return true;
            }
            if (playerVersion.major < 9) {
                return false;
            }
            if ((playerVersion.major == 9) && (playerVersion.minor > 0)) {
                return true;
            }
            if ((playerVersion.major == 9) && (playerVersion.minor == 0) && (playerVersion.release >= 124)) {
                return true;
            } else {
                return false;
            }

        } catch (err) {
            return false;
        }
    },

    CheckJAVA: function () {
        if (deployJava.versionCheck("1.7+") == false) {
            $('#main_cont').hide();
            this.JAVACheckOK = false;
            $("#JAVAErrorDialog").dialog({ modal: true, width: 500, closeText: 'Bezárás', show: 'fade', hide: 'slide',
                buttons: { "JAVA Telepítése": function () {
                    $(this).dialog("close");
                    $('#java_not_installed').show();
                    deployJava.installLatestJRE();
                },
                    "Mégsem": function () {
                        $(this).dialog("close");
                        $('#java_not_installed').show();
                    }
                }
            });
            return false;
        } else {
            this.JAVACheckOK = navigator.javaEnabled();
            if (!this.JAVACheckOK) {
                $('#main_cont').hide();
                $('#java_not_enabled').show();
            }

            return this.JAVACheckOK;
        }
    },

    GetNlFormSigningApplet: function () {
        if ((this.NlJAVASignerApplet == null) || (this.NlJAVASignerApplet == undefined)) {
            this.NlJAVASignerApplet = document.getElementById('NlJAVASignerApplet');
        }
    },

    AppletCallable: function () {
        //return (this.InitialisationSuccesfull && (this.NlJAVASignerApplet != null) && (this.NlJAVASignerApplet != undefined)); 
        return this.AppletFullyLoaded;
    },

    SetupDialogs: function () {
        $("#divWarning").dialog({
            autoOpen: false,
            modal: true,
            closeText: 'Bezárás',
            width: 550,
            minWidth: 550,
            height: 250,
            minHeight: 250,
            buttons: {
                'OK': function () {
                    $(this).dialog("close");
                }
            }
        });
        $("#divError").dialog({
            autoOpen: false,
            closeText: 'Bezárás',
            width: 550,
            minWidth: 550,
            height: 250,
            minHeight: 250,
            buttons: {
                'OK': function () {
                    $(this).dialog("close");
                }
            }
        });
        if (this.IsPDFURLMode()) {
            $("#divResultPDF").dialog({
                autoOpen: false,
                modal: true,
                closeText: 'Bezárás',
                width: 550,
                minWidth: 550,
                height: 250,
                minHeight: 250,
                buttons: {
                    'OK': function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
        if (!this.IsPDFMode()) {
            $("#divResult").dialog({
                autoOpen: false,
                modal: true,
                closeText: 'Bezárás',
                width: 550,
                minWidth: 550,
                height: 250,
                minHeight: 250,
                buttons: {
                    'OK': function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    },

    /* Hibaüzenet megjelenítése ablakban */
    handleError: function (message) {
        try {
            $("#divError").html(message);
            $("#divError").dialog('open');
        } catch (err) {
            alert(message);
        }
    },


    sign: function () {

        if (this.NlJAVASignerApplet == undefined) {
            if (appletNotFoundText != undefined) {
                this.handleError(appletNotFoundText);
            } else {
                this.handleError(this.defaultAppletNotFoundText);
            }
            return false;
        }
        /*IE-ben nem megy:
        if (this.NlJAVASignerApplet.setthis.LogLevel == undefined) {
        this.HandleAppletNotLoaded();    
        return false;
        }*/

        //JAVA Console Loggolás szintje és hibakezelés
        try {
            this.NlJAVASignerApplet.setLogLevel(this.LogLevel);
        } catch (ex) {
            this.HandleAppletNotLoaded();
            return false;
        }

        // Aláírás formátum megadása  
        this.NlJAVASignerApplet.setSignatureType(this.signatureType);

        if (this.useBrowserKeystore != undefined) {
            this.NlJAVASignerApplet.setUseBrowserKeystore(this.useBrowserKeystore);
        }
        if (this.useIExplorerKeystore != undefined) {
            this.NlJAVASignerApplet.setIExplorerKeystore(this.useIExplorerKeystore);
        }
        if (this.useMozillaKeystore != undefined) {
            this.NlJAVASignerApplet.setMozillaKeystore(this.useMozillaKeystore);
        }

        if (this.IsPDFMode() || this.IsPDFURLMode()) {
            this.DocId = this.GenerateDocID();

            this.NlJAVASignerApplet.SetPDFParams(this.NlServerPostURL, this.DocId, this.HashName);
        }

        // Tanúsítvány szűrő meghatározása
        try {
            if (this.allKeyUsagesFilter != undefined) {
                this.NlJAVASignerApplet.allKeyUsagesFilter(this.allKeyUsagesFilter);
            }
        } catch (e) { }

        // Tárgy mező szűrési szabályai (ha vannak)
        try {
            if (arraySubjectFilterRules != undefined && arraySubjectFilterRules != null) {
                for (var i = 0; i < arraySubjectFilterRules.length; i++) {
                    this.NlJAVASignerApplet.addSubjectDNFilterRule(arraySubjectFilterRules[i]);
                }
            }
        } catch (e) { }

        // Kiadó mező szűrési szabályai (ha vannak)
        try {
            if (arrayIssuerFilterRules != undefined && arrayIssuerFilterRules != null) {
                for (var i = 0; i < arrayIssuerFilterRules.length; i++) {
                    this.NlJAVASignerApplet.addIssuerDNFilterRule(arrayIssuerFilterRules[i]);
                }
            }
        } catch (e) { }

        // Sorozatszám mező szűrési szabályai (ha vannak)
        try {
            if (arraySerialNumberFilterRules != undefined && arraySerialNumberFilterRules != null) {
                for (var i = 0; i < arraySerialNumberFilterRules.length; i++) {
                    this.NlJAVASignerApplet.addSerialNumberFilterRule(arraySerialNumberFilterRules[i]);
                }
            }
        } catch (e) { }

        // Jelenjen-e meg a tanúsítvány kiválasztó ablak, ha csak egy tanúsítványt találtunk
        mostrarSeleccionSiSoloUno = false;
        try {
            if (mostrarSeleccionSiSoloUno != undefined) {
                this.NlJAVASignerApplet.setMostrarSeleccionSiSoloUno(mostrarSeleccionSiSoloUno);
            }
        } catch (e) { }


        // Detached aláírás esetében a referencia elem lekérdezése
        var reference = null;
        if (this.IsPDFURLMode()) {

                this.NlJAVASignerApplet.setContentToSign(this.fileToSign.FileUrl, reference);

                // aláírás
                var signatureB64 = this.NlJAVASignerApplet.sign();

                if (signatureB64 != null) {
                    this.fileToSign.Error = null;
                    this.PDFSigned(signatureB64, this.fileToSign.IsLastFile);
                    return signatureB64;
                }
                if (signatureB64 == null) {
                    this.fileToSign.Error = this.NlJAVASignerApplet.getError();
                    this.PDFSigned(null, false);
                    this.handleError(this.NlJAVASignerApplet.getError());
                    return null;
                }

            
            return true;
        }

        if (!this.IsPDFMode()) {
            if ($("#signatureReference").length > 0 && $("#signatureReference").val() != '') {
                reference = $("#signatureReference").val();
            }
            // Aláírandó dokumentum meghatározása: lehet elérési út vagy fájl tartalom
            if ($("#fileToSign").length > 0 && $("#fileToSign").val() != '') {
                this.NlJAVASignerApplet.setFileToSign($("#fileToSign").val(), reference);
            }
            if ($("#contentToSignBase64").length > 0 && $("#contentToSignBase64").val() != '') {
                this.NlJAVASignerApplet.setContentToSignBase64($("#contentToSignBase64").val(), reference);
            }

            if ($("#contentToSign").length > 0 && $("#contentToSign").val() != '') {
                this.NlJAVASignerApplet.setContentToSign($("#contentToSign").val(), reference);
            }

            if (this.contentToSign) {
                this.NlJAVASignerApplet.setContentToSign(this.contentToSign, reference);
            }

            // XAdES aláírás - PDF külön van kezelve - NlJavaSigner.js-ben
            var signatureB64 = this.NlJAVASignerApplet.sign();
            if (signatureB64 != null) {
                return signatureB64;
            } else {
                this.handleError(this.NlJAVASignerApplet.getError());
                return false;
            }
        }

        return true;
    },

    save: function (documentB64) {

        if (this.NlJAVASignerApplet == undefined) {
            if (appletNotFoundText != undefined) {
                this.handleError(appletNotFoundText);
            } else {
                this.handleError(this.defaultAppletNotFoundText);
            }
            return;
        }

        // Applet lekérdezése
        var applet;
        if (this.NlJAVASignerApplet != undefined) {
            applet = this.NlJAVASignerApplet;
        }

        // Mentés
        if (!applet.save(documentB64)) {
            if (applet.getError() != null) {
                this.handleError(applet.getError());
                return false;
            } else {
                //-- Műveletet megszakította a felhasználó
                this.handleOperationCancelled();
                return false;
            }
        }

        return true;
    },

    getPath: function (textFieldID) {

        if (this.NlJAVASignerApplet == undefined) {
            if (appletNotFoundText != undefined) {
                this.handleError(appletNotFoundText);
            } else {
                this.handleError(this.defaultAppletNotFoundText);
            }
            return;
        }

        // Applet lekérdezése
        var applet;
        if (this.NlJAVASignerApplet != undefined) {
            applet = this.NlJAVASignerApplet;
        }

        //-- Felhasználó elérési út választása
        path = applet.getPath();
        if (path != null) {
            document.getElementById(textFieldID).value = path;
        }
    },

    onClick: function (e) {
        this.send(e);
        return false;
    },

    PDFSigned: function (url, closeWindow) {
        if (this.asyncPostBackUrl) {
            this.fileToSign.SignedFileUrl = url;
            var params = { signedFile: this.fileToSign };
            var _this = this;

            this.showUpdateProgress('Feldolgozás folyamatban...');
            $.ajax({
                type: "POST",
                url: this.asyncPostBackUrl,
                data: Sys.Serialization.JavaScriptSerializer.serialize(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    _this.hideUpdateProgress();
                    if (msg.d) {
                        var errorMessage = msg.d;
                        _this.handleError(errorMessage);
                    }
                    else {
                        if (closeWindow) {
                            window.returnValue = true;
                            if (window.opener && window.returnValue && window.opener.__doPostBack) { window.opener.__doPostBack('', window.opener.postBackArgument); }
                            window.close();
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    _this.hideUpdateProgress();
                    var errorMessage = xhr.responseText;
                    try {
                        var exception = Sys.Serialization.JavaScriptSerializer.deserialize(xhr.responseText)
                        var errorMessage = exception.Message;
                    } catch (e) { };
                    _this.handleError(errorMessage);

                }
            });
        }
        else {
            $("#divResultPDF").html(url);
            $("#divResultPDF").dialog('open');
        }


    },

    showUpdateProgress: function (text) {
        if (text) {
            $('#updateProgressText').html(text);
        }
        $('#UpdateProgressPanel').show();
    },

    hideUpdateProgress: function () {
        $('#UpdateProgressPanel').hide();
    },

    //
    // Behavior properties
    //

    get_NlServerPostURL: function () {
        return this.NlServerPostURL;
    },

    set_NlServerPostURL: function (value) {
        if (this.NlServerPostURL !== value) {
            this.NlServerPostURL = value;
            this.raisePropertyChanged('NlServerPostURL');
        }
    },

    get_ContentToSign: function () {
        return this.contentToSign;
    },

    set_ContentToSign: function (value) {
        if (this.contentToSign !== value) {
            this.contentToSign = value;
            this.raisePropertyChanged('ContentToSign');
        }
    },

    get_AsyncPostBackUrl: function () {
        return this.asyncPostBackUrl;
    },

    set_AsyncPostBackUrl: function (value) {
        if (this.asyncPostBackUrl !== value) {
            this.asyncPostBackUrl = value;
            this.raisePropertyChanged('AsyncPostBackUrl');
        }
    },

    get_FileToSign: function () {
        return this.filesToSign;
    },

    set_FileToSign: function (value) {
        if (this.fileToSign !== value) {
            this.fileToSign = value;
            this.set_ContentToSign(value.FileUrl);
            this.raisePropertyChanged('FileToSign');
        }
    },

    get_IratId: function () {
        return this.filesToSign;
    },

    set_IratId: function (value) {
        if (this.iratId !== value) {
            this.iratId = value;
            this.raisePropertyChanged('IratId');
        }
    },

    get_ProcId: function () {
        return this.procId;
    },

    set_ProcId: function (value) {
        if (this.procId !== value) {
            this.procId = value;
            this.raisePropertyChanged('ProcId');
        }
    },

    get_FilesToSign: function () {
        return this.filesToSign;
    },

    set_FilesToSign: function (value) {
        if (this.filesToSign !== value) {
            this.filesToSign = value;
            this.set_FileToSign(value);
            this.raisePropertyChanged('FilesToSign');
        }
    }
}




Netlock.JavaSignerBehavior.registerClass('Netlock.JavaSignerBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();