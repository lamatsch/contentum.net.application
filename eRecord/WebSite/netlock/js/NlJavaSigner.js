﻿//********************************* GLOBÁLIS VÁLTOZÓK, KONFIGURÁCIÓS BEÁLLÍTÁSOK ********************************/
var InitialisationSuccesfull = false;
var YCM = null; //YUI connection manager objektuma
var YUIConnectionSWF = './js/yui/assets/connection.swf';
var LogEnabled = false; //Loggolás YUI Loggerbe és böngésző konzolára
var LogWindowVisible = false; //YUI Logger látható
var JAVACheckOK = false; //JAVA meglétének és verziójának ellenőrzése
var javaversion = '1.7.0_65'; //támogatott JAVA verzió
var AppletFullyLoaded = false; //ha az applet teljesen betöltődött, akkor értéke "true" lesz
var AppletTimer = null; 
var NlJAVASignerApplet;//NetLock JAVA aláíró modul appletje
var appletNotFoundText;
var defaultAppletNotFoundText = "Az applet még nem töltődött be, kérjük várjon egy kicsit és próbálja meg újra";
var UseFlashTransport =false; //Használja-e a YUI Connection Manager a Flash plugint a kommunikációhoz - cross-domain (eltérő domianek közötti kommunikáció) esetén mindenképpen true-nak kell lennie
var CommunicationTimeOut = 120000; //NetLock serverrel történő kommunikáció során használt timeout ms-ban megadva
var LogLevel = "DEBUG"; //JAVA Console Loggolás szintje: ERROR, WARN, INFO, DEBUG, OFF
//var NlServerPostURL = 'http://10.0.10.177/upload.cgi'; //NetLock Server feldolgozó script címe

//var NlServerPostURL = 'http://10.0.10.177/upload.cgi';
var NlServerPostURL = 'http://172.22.0.206/cgi-bin/upload.cgi';

var HashName = 'SHA256'; //PDF aláíráshoz használt hash algoritmus neve - SHA1, SHA256
var allKeyUsagesFilter = false; //false: csak aláíró tanúsítványok jelenjenek meg

var useBrowserKeystore = false;
var useIExplorerKeystore = true;
var useMozillaKeystore = true;

var PDFSignType = "NlPDFClientServer"; //PDF aláíró típus konstans
var PDFURLMode = true; //PDF URL mód

//Folyamatjelző sáv során használt változók
var ProgressBarValue = 0;
var ProgressBarProcessing = false;
var AppletCheckProcessing = false;
var IncreasingProgress = false;
var readtimerIntervalId = null;
var appletIntervalId = null;

//Belső kommunikációs változók
var SignerCert = ""; //aláíró tanúsítvány base64-es formában
var SignedData = ""; //aláírt adat
var SignableData = ""; //aláírandó adat
var SourceData = ""; //server oldali aláírandó adat
var PKCS7 = ""; //PKCS#7-es objektum base64-es formában
var DocId = ""; //dokumentum azonosító
var PDFIdentifier = ""; //PDF belső azonosító

/******************************************  FÜGGVÉNYEK, ESEMÉNYEK ********************************/
function Log(msg, level)	{
	if ((LogEnabled == true) && (YAHOO != null)) {
		YAHOO.log(msg, level, "NlJavaSigner");	
	}
}

function LogError(msg)	{
	if ((LogEnabled == true) && (YAHOO != null)) {
		YAHOO.log(msg, "error", "NlJavaSigner");	
	}
}

function LogInfo(msg)	{
	if ((LogEnabled == true) && (YAHOO != null)) {
		YAHOO.log(msg, "info", "NlJavaSigner");	
	}
}

//Új deploy.js esetén szükséges módosítás
function docWriteWrapper(func) {
	var writeTo = document.createElement('del'),
	oldwrite = document.write,
	content = '';
	writeTo.id = "NlJAVASignerAppletWrapper";
	try 
	{
	    document.write = function(text) {
		content += text;
	    }
	    func();
	    writeTo.innerHTML += content;
	} finally {
		document.write = oldwrite;
	}
	document.body.appendChild(writeTo);
}

function CheckAppletLoaded() {
	try {
		if ((NlJAVASignerApplet == undefined) || (NlJAVASignerApplet == null)) {
			LogInfo("Applet még nem töltődött be, applet objektum lekérdezése");
			NlJAVASignerApplet = document.getElementById("NlJAVASignerApplet");
		}
		AppletFullyLoaded = (NlJAVASignerApplet != undefined) && (NlJAVASignerApplet != null);
		
		//if (AppletFullyLoaded && !IsIE8) {
		if (AppletFullyLoaded) {
			try {
				NlJAVASignerApplet.getJavaVersion();				
				AppletFullyLoaded = true;
			} catch (ex) {
				AppletFullyLoaded = false;
			}
		}		
	//AppletFullyLoaded = AppletFullyLoaded && (NlJAVASignerApplet.status < 2);
	}	catch (e) {
		AppletFullyLoaded = false;
	}
	if (AppletFullyLoaded) {		
		//LogInfo("Clearing Applet timer id: " + AppletTimer);
		//window.clearInterval(AppletTimer);		
		LogInfo("Applet sikeresen betöltődött");
	}	else	{
		LogInfo("Applet még nem töltődött be, timer újra aktíválása");
		AppletTimer = window.setTimeout(CheckAppletLoaded, 1000);	
	}
}

/*
 * Aláíró applet betöltése
 */
function loadSignatureApplet(force) {

	LogInfo("Start loading the applet");
	
	// Ha az applet már be van töltve, nem lépünk tovább
	if ((force == false) && (NlJAVASignerApplet != undefined)) {
		return;
	}

	// Beállítható paraméterek a nyelv és az ország (lokalizáció)
	// A minimum Java verzió az 1.6.0 update 10
	try {
		if (localeLanguage == undefined || localeLanguage == null || localeLanguage == '') {
			localeLanguage = 'hu';
		}
	} catch (e) {
		var localeLanguage = 'hu';
	}
	try {
		if (localeCountry == undefined || localeCountry == null || localeCountry == '') {
			localeCountry = 'HU';
		}
	} catch (e) {
		var localeCountry = 'HU';
	}

	var attributes = {id:'NlJAVASignerApplet',
	 		  width:1,
	 		  height:1
	};

	jnlpfile = 'arangiClientSignature.jnlp'; //XAdES támogatás
	if (IsPDFMode()) {
		jnlpfile = 'arangiClientSignature_pdf.jnlp'; //XAdES támogatás nincs, csak PDF aláírás -> optimálisabb betöltés, kevesebb JAR szükséges
	}
	
	//var parameters = {jnlp_href:baseDownloadURL + '/' + 'arangiClientSignature.jnlp',
	var parameters = {
			  jnlp_href:baseDownloadURL + '/' + jnlpfile,	
			  separate_jvm:true,
			  language:localeLanguage,
			  country:localeCountry,
			  userAgent:window.navigator.userAgent,
			  mayscript:true			  		  
	};
	if( deployJava.versionCheck("1.7.0_65+")==true ) {
		parameters.java_vm_args = '-Djnlp.packEnabled=true -Xms512M -Xms512M -Xmx512M';
	}
	else	{
		parameters.java_arguments = '-Djnlp.packEnabled=true -Xms512M -Xms512M -Xmx512M';
	}
	
	var javaversion = '1.7.0_65';

 	LogInfo("before runApplet");
		
	//Új deploy.js miatt szükséges módosítás
	//http://stackoverflow.com/questions/13517790/using-deployjava-runapplet-to-target-specific-element
	docWriteWrapper(function () {
		try {
			deployJava.runApplet(attributes, parameters, javaversion);
		} catch (ex) {
			HandleException(ex, null);
		}
	});
		
	LogInfo("after runApplet");
	
	LogInfo(NlJAVASignerApplet);
	
	//másodpercenként ellenőrizzük, hogy az applet teljesen betöltődött-e
	//AppletTimer = window.setInterval(CheckAppletLoaded, 1000);	
	AppletTimer = window.setTimeout(CheckAppletLoaded, 1000);	
	LogInfo("Applet timer id: " + AppletTimer);
}

//Event handler called when the transaction begins:
var handleStart = function(e, a) {
	LogInfo("Kommunikáció a NetLock serverrel. Tranzakció " + a[0].tId + " elkezdődött.");
}

//Event handler for the success event 
var handleSuccess = function(o) {
	//JSON válasz parsolása objektummá
	var Reply = undefined;
	try	{
		Reply = YAHOO.lang.JSON.parse(o.responseText);
	}	catch(err)	{
		Reply = undefined;
	}

	if (Reply) {		
		LogInfo("A servertől kapott válasz: " + o.responseText);		
		
		//Sikeres aláírás történt
		if ((Reply.statuscode != undefined) && (Reply.statuscode == 0) && (SignedData != "") && (Reply.docid == DocId))	{			
			if (Reply.signeddata != undefined)	{			
				LogInfo("Sikeres PDF aláírás, azonosító: " + Reply.signeddata);
				$('#SourceData').attr('value',SourceData);
				$('#signeddata').attr('value',Reply.signeddata);
				
				ShowSuccessfullDialog(1);				
			}	else	{
				LogError("Sikertelen PDF aláírás");
			}
			return true;	
		}
			
		//Server oldali hiba történt
		if ((Reply.statuscode != undefined) && (Reply.statuscode != 0))	{
			msg = "Hiba történt a server oldali feldolgozás során. Hibakód: " + Reply.statuscode + " Hibaüzenet:"  + Reply.statustext;
			LogError(msg);		
			CloseDialogStopProgress(msg);
			return false;	
		}
		
		//Dokumentum azonosító hiba
		if (Reply.docid != DocId)	{
			msg = "Hiba történt a server oldali feldolgozás során, a kapott válasz dokumentum azonosítója nem egyezik meg a küldöttel.";
			LogError(msg);		
			CloseDialogStopProgress(msg);
			return false;	
		}
		
		LogInfo("PKCS7: " + Reply.pkcs7);		
		LogInfo("DocID: " + Reply.docid);		
		LogInfo("SignableData: " + Reply.signabledata);		
		LogInfo("PDFIdentifier: " + Reply.pdfidentifier);
		LogInfo("SourceData: " + Reply.sourcedata);		
		
		//Ha még nem volt aláírás, akkor aláírunk
		if (SignableData == "")	{
			SignableData = Reply.signabledata;		
			PDFIdentifier = Reply.pdfidentifier;
			PKCS7 = Reply.pkcs7;
			
			NlJAVASignerApplet.setContentToSign(SignableData, null);
			
			SignedData = NlJAVASignerApplet.makePKCS1();
			LogInfo("Aláírt PKCS#1 adat: " + SignedData, "info", "NlJavaSigner");		
			if ((SignedData != null) && (SignedData != ""))	{
				PostData(NlServerPostURL, '', DocId, 'pdf', SignerCert, '', PKCS7, SignedData, PDFIdentifier);					
			}	else	{
				msg = "Hiba az aláírás készítése során.";
				LogError(msg);		
				CloseDialogStopProgress(msg);	
			}
		}
		
	}	else	{		
		LogError("Nem kaptunk vissza értelmes választ a servertől.");				
		CloseDialogStopProgress("Nem kaptunk vissza értelmes választ a servertől.");
	}
}

//In the event that the HTTP status returned is > 399, a
//failure is reported and this function is called:
var handleFailure = function(o) {	
	msg = "";
	if (o.status == -1)	{
		msg = "A NetLock serverrel történő kommunikáció megszakadt.";
	}	else	if (o.status == 0)	{
		msg = "A NetLock serverrel történő kommunikáció során hiba történt.";
	}	else	{
		msg = "A NetLock serverrel történő kapcsolat során hiba történt.";
	}
	msg = msg + ' - ' + o.statusText;
	LogError("HIBA - Failure: " + o.tId + " - " + o.status + " -" + msg);
	CloseDialogStopProgress(msg);
}

//Set up the callback object used for the transaction.
var callback = {
	success: handleSuccess,
	failure: handleFailure,
	timeout: CommunicationTimeOut,
	xdr: UseFlashTransport
};

//Elküldi az adatokat a NetLock servernek	
function PostData(url, sourcedata, docid, doctype, cert, hashname, pkcs7, pkcs1, Identifier)	{
	//JSON objektum összeállítása
	myData = {	    				  
		  doctype: doctype, 
		  docid: docid,			  
		  cert: cert
		}; 		

	if (sourcedata != '')	{
		myData.sourcedata = sourcedata;
	}
	if (Identifier != '')	{
		myData.pdfidentifier = Identifier;
	}	
	if (cert != '')	{
		myData.cert = cert;
	}
	if (hashname != '')	{
		myData.hashname = hashname;
	}	
	if (pkcs7 != '')	{
		myData.pkcs7 = pkcs7;
	}		
	if (pkcs1 != '')	{
		myData.pkcs1 = pkcs1;
	}	
	
	try {
		jsonStr = YAHOO.lang.JSON.stringify(myData);
		LogInfo("JSON adat postolása NetLock servernek");
		var obj = YCM.asyncRequest('POST', url, callback, jsonStr);
	} catch(err) {
		CloseDialogStopProgress('');
		HandleException(err, "Hiba az adatok küldése során.");
	}
}

//Egyedi dokumentum azonosító generálása
function GenerateDocID()	{
	//var currentDate = new Date();
	//GeneratedDocID = "NlJavaSigner_" + currentDate.toString("yyyy'-'mm'-'dd'T'HH':'mm':'ss") + 'R' + (Math.floor(Math.random()*100)+1);
	GeneratedDocID = "NlJavaSigner_" + dateFormat(new Date(), "yyyy-mm-dd'T'HH-MM-ss")  + 'R' + (Math.floor(Math.random()*100)+1);
	return GeneratedDocID;	
}

function StopProgressBar()	{
	clearInterval(readtimerIntervalId);
}

function CloseDialogStopProgress(errtxt)	{
	StopProgressBar();	
	
	if (errtxt != "")	{
		//setter
		$( "#ProgressDialog" ).dialog( "option", "buttons", [
			{
				text: "Bezárás",
				click: function() { $(this).dialog("close"); }
			}
		] );
		$( "#ProgressDialog" ).dialog( "option", "title", 'Az aláírás során hiba történt' );		
		
		$('#errortext').html(errtxt);
		$('#progressblock').hide();	
		$('#errorblock').show();		
	}	else	{
		$("#ProgressDialog").dialog('close');
	}
}

function ShowSuccessfullDialog(closedlg)	{
	if (closedlg == 1) {
		CloseDialogStopProgress("");	
	}
	$("#SuccesDialog").dialog({ modal: true, width: 500, show: 'fade', hide: 'slide', closeText: 'Bezárás', buttons: { "OK": function() { $(this).dialog("close"); } } });	
}

function ProgressBarCallback()	{
	if (ProgressBarProcessing)	{
		return true;
	}
	ProgressBarProcessing = true;
	try	{
		if (IncreasingProgress)	{
			ProgressBarValue = ProgressBarValue + 10;
		}	else	{
			ProgressBarValue = ProgressBarValue - 10;
		}
		if (IncreasingProgress && ProgressBarValue >= 100)	{
			IncreasingProgress = false;
		}	else	if ((IncreasingProgress==false) && ProgressBarValue <= 0)	{
			IncreasingProgress = true;
		}
				
		$("#progressbar").progressbar({ value: ProgressBarValue });
	} finally	{
		ProgressBarProcessing = false;
	}
	return true;
}

function SetProgressbar()	{
	readtimerIntervalId = setInterval('ProgressBarCallback()', 1000);		
}		

function IsPDFMode() {
	return (signatureType == "NlPDFClientServer");	
}

function IsPDFURLMode() {
	return ((signatureType == "NlPDFClientServer") && PDFURLMode);	
}

//XAdES aláírás
function xades_sign() {
	var signatureB64 = sign();
	if ((signatureB64 != null) && (signatureB64 != false)) {		
			$("#divResult").html(signatureB64);
			$("#divResult").dialog('open');		
	}
}

function HandleException(err, msg) {
	if (msg != null) {
		txt = msg;
	}	else	{
		txt = "Hiba történt a NetLock JAVA aláíró modul meghívása során.\n\n";
	}
  	txt+="Hiba leírása: " + err.message + "\n\n";
  	txt+="A folytatáshoz kattintson az OK gombra.\n\n";
  	
	handleError(txt);

	LogError(txt);
}

function HandleAppletNotLoaded() {
	handleError("HIBA! A NETLOCK aláíró modul nem érhető el (nincs teljesen betöltve ill. a Java nincs engedélyezve az oldalon)!");
}

function checkCertificate(serialnumber) {
		LogInfo("serialnumber: " + serialnumber);
		//alert("kapott serial number:" + serialnumber);
		return 1;
}

var send = function(o) {
	if (!AppletCallable()) {		
		HandleAppletNotLoaded();
		return;
	}

	try {
		LogInfo("JAVA verzió: " + NlJAVASignerApplet.getJavaVersion());
		LogInfo("NetLock JAVA FormSigning verzió: " + NlJAVASignerApplet.getVersion());
	} catch(ex) {
		//IE bug
		if (ex.message == "Target JVM seems to have already exited")	{		
			HandleException(ex, "Belső Internet Explorer hiba az aláíró modul (applet) elérése során. Kérjük, zárja be a böngészőt, majd próbálja újra a műveletet.");
		}	else	{
			HandleException(ex, "Hiba az aláíró modul (applet) elérése során. Kérjük, zárja be a böngészőt, majd próbálja újra a műveletet.");
		}
		return;
	}
	
	if (IsPDFURLMode()) {
		SourceData = document.getElementById('contentToSign').value; //aláírandó PDF fájl azonosító
		if (SourceData == "") {
			handleError("Kérjük, töltse ki a PDF letöltési URL-jét!");
			return false;
		}
		//PDF aláírás
		if (!sign ()) {
			return false;
		}			
		return true;
	}
		
	if (!IsPDFMode()) {
		//XAdES aláírás
		xades_sign();
		return;	
	}	else	{
		SourceData = document.getElementById('contentToSign').value; //aláírandó PDF fájl azonosító
		if (SourceData == "") {
			handleError("Kérjük, töltse ki a PDF dokumentum azonosítót!");
			return false;
		}
		
		//PDF aláírás előkészítés
		if (!sign ()) {
			return false;
		}
	}	
	
	//PDF aláírás
	LogInfo("Aláírás folyamat kezdete, kommunikáció kezdete a NL serverrel.");	
	
	//Folyamatjelző ablak inicializálás
	$("#ProgressDialog").dialog({ modal: true, width: 500, title: 'Aláírás folyamatban', closeText: 'Bezárás', autoOpen: false, show: 'blind', hide: 'slide',
								 buttons: [
									{
										text: "Mégsem",
										click: function() { $(this).dialog("close"); }
									}
								]
	});
	$("#progressbar").progressbar({ value: 0 });
	$('#errorblock').hide();
	$('#progressblock').show();	
	$("#ProgressDialog").dialog("open");	

	LogInfo("Állapotjelző dialógusablak betöltődött");
	
	SetProgressbar();	
	
	ClearVariables();
	
	//Most kezdődik az aláírási folyamat
	if (SignableData == "")	{		
		SourceData = document.getElementById('contentToSign').value; //aláírandó PDF fájl azonosító
		DocId = GenerateDocID();		
		
		try {
			SignerCert = NlJAVASignerApplet.selectCertificate(); //tanúsítványkiválasztás
		}	catch (err)	{
			HandleException(err);			
			CloseDialogStopProgress('');
			return;
		}
		
		if (SignerCert != null) {		
			serial = NlJAVASignerApplet.GetSerialNumberFromCertificate();
			alert(serial);
			PostData(NlServerPostURL, SourceData, DocId, 'pdf', SignerCert, HashName, PKCS7, SignedData);
		}	else	{
			CloseDialogStopProgress('');
		}
	}
}

//Változók törlése
function ClearVariables()	{
	SignerCert = "";
	SignedData = "";
	SignableData = "";
	PKCS7 = "";
	DocId = "";
	PDFIdentifier = "";
	SourceData = "";
}

//YUI inicializálása
function InitYUI()	{
	YCM = YAHOO.util.Connect;
	
	// Loggolás kezelése
	if (LogEnabled == true)	{
		if (LogWindowVisible == true)	{
			this.myLogReader = new YAHOO.widget.LogReader();
		}
		YAHOO.widget.Logger.enableBrowserConsole();	
	}
	
	//Listen for Connection Manager's start event.
	YCM.startEvent.subscribe(handleStart);
	
	if (UseFlashTransport)	{
		//Initialize the Flash transport.
		//YCM.transport(YUIConnectionSWF + '?t=' + new Date().valueOf().toString()); //IE miatt kell a dátum a végére	
		YCM.transport(YUIConnectionSWF); //IE miatt kell a dátum a végére	
		
		//Az aláíró gomb engedélyezése, ha a YUI Flash kommunikációs objektum betöltődött		
		YCM.xdrReadyEvent.subscribe(function() {		
			document.getElementById('signbutton').disabled = false;
			$("#signbutton").button("enable");
			LogInfo("YUI Flash transport inicializálódott");
		});
	}	else	{
		$("#signbutton").button("enable");		
	}
}

function ShowErrorDlg(errtxt) {
	$("#ProgressDialog").dialog({ modal: true, width: 500, 'title': 'Hiba', closeText: 'Bezárás', show: 'blind', hide: 'slide',
								 buttons: [
									{
										text: "Bezár",
										click: function() { $(this).dialog("close"); }
									}
								]
	});
	CloseDialogStopProgress(errtxt);	
}

function DisplayFlashErrorDialog()	{
	$('#main_cont').hide();			
	$('#flash_not_installed').show();	
	$("#FlashErrorDialog").dialog({ modal: true, width: 500, closeText: 'Bezárás', show: 'fade', hide: 'slide', 
		buttons: { "OK": function() { 				
				$(this).dialog("close"); 				
			    }
		} 
	});	
}

//FLASH ellenőrzés: csak cross-domain kezelésnél szükséges
function checkFLASH()	{
	try	{
		if (!UseFlashTransport) {
			return true;
		}
		var playerVersion = swfobject.getFlashPlayerVersion(); 
		//var output = "You have Flash player " + playerVersion.major + "." + playerVersion.minor + "." + playerVersion.release + " installed"; 			
		
		if (playerVersion.major >= 10)	{
			return true;
		}		
		if (playerVersion.major < 9)	{
			return false;
		}		
		if ((playerVersion.major == 9) && (playerVersion.minor > 0))	{
			return true;
		}
		if ((playerVersion.major == 9) && (playerVersion.minor == 0) && (playerVersion.release >= 124))	{
			return true;
		}	else	{
			return false;
		}
		
	}catch (err)	{
		return false;
	}
}

function CheckJAVA() {
	if( deployJava.versionCheck("1.7+")==false )
	{ 		
		$('#main_cont').hide();			
		JAVACheckOK = false;
		$("#JAVAErrorDialog").dialog({ modal: true, width: 500, closeText: 'Bezárás', show: 'fade', hide: 'slide', 
			buttons: { "JAVA Telepítése": function() { 
					$(this).dialog("close"); 
					$('#java_not_installed').show();
					deployJava.installLatestJRE();
				    }, 
				    "Mégsem": function() { 
					$(this).dialog("close"); 
					$('#java_not_installed').show();
				    }
			} 
		});	
		return false;			
	}	else	{			
		JAVACheckOK = navigator.javaEnabled();			
		if (!JAVACheckOK)	{
			$('#main_cont').hide();	
			$('#java_not_enabled').show();
		}			

		return JAVACheckOK;
	}
}

function GetNlFormSigningApplet()	{
	if ((NlJAVASignerApplet == null) || (NlJAVASignerApplet == undefined))	{
		NlJAVASignerApplet = document.getElementById('NlJAVASignerApplet');
	}
}

function AppletCallable() {
	//return (InitialisationSuccesfull && (NlJAVASignerApplet != null) && (NlJAVASignerApplet != undefined)); 
	return AppletFullyLoaded; 	
}
	
function SetupDialogs() {
	$("#divWarning").dialog({ 
  		autoOpen: false,
  		modal: true,
  		closeText: 'Bezárás',
  		width: 550,
 	  	minWidth: 550,
 	  	height: 250,
 	  	minHeight: 250,
	  	buttons: {
			'OK': function() {
    			$(this).dialog("close");
    		}
  		}
	});
  	$("#divError").dialog({ 
  		autoOpen: false,
  		closeText: 'Bezárás',
  		width: 550,
 	  	minWidth: 550,
 	  	height: 250,
 	  	minHeight: 250,
	  	buttons: {
			'OK': function() {
    			$(this).dialog("close");
    		}
  		}
	});
	if (IsPDFURLMode()) {  			
  		$("#divResultPDF").dialog({ 
  	  		autoOpen: false,
  	  		modal: true,
  	  		closeText: 'Bezárás',
  	  		width: 550,
  	 	  	minWidth: 550,
  	 	  	height: 250,
  	 	  	minHeight: 250,
  		  	buttons: {
  				'OK': function() {
  	    			$(this).dialog("close");
  	    		}
  	  		}
  		});
  	}
  	if (!IsPDFMode()) {  			
  		$("#divResult").dialog({ 
  	  		autoOpen: false,
  	  		modal: true,
  	  		closeText: 'Bezárás',
  	  		width: 550,
  	 	  	minWidth: 550,
  	 	  	height: 250,
  	 	  	minHeight: 250,
  		  	buttons: {
  				'OK': function() {
  	    			$(this).dialog("close");
  	    		}
  	  		}
  		});
  	}	
  		
}

//Dokumentum inicializálás
jQuery(document).ready(function(){
	try	{
		$('#signbutton').click(function () {
		    send();	
		});				
					
		//jQuery gombbá alakítjuk a szabványos html gombokat
		$("#signbutton").button();
		
		SetupDialogs();
		
		//YUI inicializálása
		InitYUI();	
		
		//FLASH ellenőrzés: csak cross-domain kezelésnél szükséges
		if (!checkFLASH())	{
			DisplayFlashErrorDialog();		
			return;
		}
				
		//JAVA ellenőrzése
		if (!CheckJAVA())	{		
			return;
		}
		
		//-- Ha a Java verzió nem legalább 1.6.0_10, megjelenítjük a figyelmeztető ablakot	
		if (deployJava.versionCheck("1.7.0_65+")) {			
			loadSignatureApplet(false);
		} else {
			$("#divWarning").dialog('open');
		}
		
		LogInfo("Start loading the applet end");
		
		InitialisationSuccesfull = true;
		
		//sign ();
		
	}	catch (err)	{
		InitialisationSuccesfull = false;
		
		HandleException(err, "Hiba történt a NetLock JAVA aláíró modul inicializálása közben.\n\n");		
	}
});

/* Hibaüzenet megjelenítése ablakban */
function handleError (message) {
	try {
		$("#divError").html(message);
		$("#divError").dialog('open');
	} catch(err) {
		alert(message);
	}
}

function sign () {

	if (NlJAVASignerApplet == undefined) { 
		if (appletNotFoundText != undefined) {
			handleError(appletNotFoundText);
		} else {
			handleError(defaultAppletNotFoundText);
		}
		return false;
	}
	/*IE-ben nem megy:
	if (NlJAVASignerApplet.setLogLevel == undefined) {
		HandleAppletNotLoaded();		
		return false;
	}*/
	
	//JAVA Console Loggolás szintje és hibakezelés
	try {
		NlJAVASignerApplet.setLogLevel(LogLevel);
	} catch(ex) {
		HandleAppletNotLoaded();		
		return false;		
	} 
	
	// Aláírás formátum megadása	
	NlJAVASignerApplet.setSignatureType(signatureType);
	
	if (useBrowserKeystore != undefined) {
		NlJAVASignerApplet.setUseBrowserKeystore(useBrowserKeystore);
	}
	if (useIExplorerKeystore != undefined) {
		NlJAVASignerApplet.setIExplorerKeystore(useIExplorerKeystore);
	}
	if (useMozillaKeystore != undefined) {
		NlJAVASignerApplet.setMozillaKeystore(useMozillaKeystore);
	}
	
	if (IsPDFMode() || IsPDFURLMode()) {
		var DocId = GenerateDocID();					

		NlJAVASignerApplet.SetPDFParams(NlServerPostURL, DocId, HashName);
	}
	
	// Tanúsítvány szűrő meghatározása
	try {
		if (allKeyUsagesFilter != undefined) {
			NlJAVASignerApplet.allKeyUsagesFilter(allKeyUsagesFilter);
		}
	} catch (e) {}
	
	// Tárgy mező szűrési szabályai (ha vannak)
	try {
		if (arraySubjectFilterRules != undefined && arraySubjectFilterRules != null) {
			for (var i=0;i<arraySubjectFilterRules.length;i++) {
				NlJAVASignerApplet.addSubjectDNFilterRule(arraySubjectFilterRules[i]);
			}
		}
	} catch (e) {}
	
	// Kiadó mező szűrési szabályai (ha vannak)
	try {
		if (arrayIssuerFilterRules != undefined && arrayIssuerFilterRules != null) {
			for (var i=0;i<arrayIssuerFilterRules.length;i++) {
				NlJAVASignerApplet.addIssuerDNFilterRule(arrayIssuerFilterRules[i]);
			}
		}
	} catch (e) {}

	// Sorozatszám mező szűrési szabályai (ha vannak)
	try {
		if (arraySerialNumberFilterRules != undefined && arraySerialNumberFilterRules != null) {
			for (var i=0;i<arraySerialNumberFilterRules.length;i++) {
				NlJAVASignerApplet.addSerialNumberFilterRule(arraySerialNumberFilterRules[i]);
			}
		}
	} catch (e) {}
	
	// Jelenjen-e meg a tanúsítvány kiválasztó ablak, ha csak egy tanúsítványt találtunk
	try {
		if (mostrarSeleccionSiSoloUno != undefined) {
			NlJAVASignerApplet.setMostrarSeleccionSiSoloUno(mostrarSeleccionSiSoloUno);
		}
	} catch (e) {}
	
	
	// Detached aláírás esetében a referencia elem lekérdezése
	var reference = null;
	if (IsPDFURLMode()) {
		if ($("#contentToSignBase64").length > 0 && $("#contentToSignBase64").val() != '') {
			NlJAVASignerApplet.setContentToSignBase64($("#contentToSignBase64").val(), reference);
		}
		
		if ($("#contentToSign").length > 0 && $("#contentToSign").val() != '') {
			NlJAVASignerApplet.setContentToSign($("#contentToSign").val(), reference);
		}
		
		// aláírás
		signatureB64 = NlJAVASignerApplet.sign();
		
		if (signatureB64 != null) {			
			$("#divResultPDF").html(signatureB64);
			$("#divResultPDF").dialog('open');			
			return signatureB64;
		}		
		if (signatureB64 == null) {			
			handleError (NlJAVASignerApplet.getError());
			return null;
		}
		return true;
	}
		
	if (!IsPDFMode()) {
		if ($("#signatureReference").length > 0 && $("#signatureReference").val() != '') {
			reference = $("#signatureReference").val();
		}
		// Aláírandó dokumentum meghatározása: lehet elérési út vagy fájl tartalom
		if ($("#fileToSign").length > 0 && $("#fileToSign").val() != '') {
			NlJAVASignerApplet.setFileToSign($("#fileToSign").val(), reference);
		}
		if ($("#contentToSignBase64").length > 0 && $("#contentToSignBase64").val() != '') {
			NlJAVASignerApplet.setContentToSignBase64($("#contentToSignBase64").val(), reference);
		}
		
		if ($("#contentToSign").length > 0 && $("#contentToSign").val() != '') {
			NlJAVASignerApplet.setContentToSign($("#contentToSign").val(), reference);
		}
		
		// XAdES aláírás - PDF külön van kezelve - NlJavaSigner.js-ben
		var signatureB64 = NlJAVASignerApplet.sign();
		if (signatureB64 != null) {
			return signatureB64;
		}  else {
			handleError (NlJAVASignerApplet.getError());
			return false;
		}
	}	
	
	return true;
}

function save (documentB64) {
	
	if (NlJAVASignerApplet == undefined) { 
		if (appletNotFoundText != undefined) {
			handleError(appletNotFoundText);
		} else {
			handleError(defaultAppletNotFoundText);
		}
		return;
	}
	
	// Applet lekérdezése
	var applet;
	if (NlJAVASignerApplet != undefined) {
		applet = NlJAVASignerApplet;
	}
	
	// Mentés
	if (!applet.save(documentB64)) {
		if (applet.getError() != null) {
			handleError (applet.getError());
			return false;
		} else {
			//-- Műveletet megszakította a felhasználó
			handleOperationCancelled ();
			return false;
		}
	}

	return true;
}

//
function getPath (textFieldID) {
	
	if (NlJAVASignerApplet == undefined) { 
		if (appletNotFoundText != undefined) {
			handleError(appletNotFoundText);
		} else {
			handleError(defaultAppletNotFoundText);
		}
		return;
	}
	
	// Applet lekérdezése
	var applet;
	if (NlJAVASignerApplet != undefined) {
		applet = NlJAVASignerApplet;
	}
	
	//-- Felhasználó elérési út választása
	path = applet.getPath();
	if (path != null) {
		document.getElementById(textFieldID).value=path;
	}
}
