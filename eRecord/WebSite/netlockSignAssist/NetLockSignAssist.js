// Call example: see below
function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var NetLockSignAssist =
    /*#__PURE__*/
    function () {
        function NetLockSignAssist(hwcrypto, urlBase, files, signatureLevel, resultFunction, loggerFunction, selectedCertificate) {
            _classCallCheck(this, NetLockSignAssist);

            // urlBase = 'https://nltokendemo.hu:8443/api';
            this._hwcrypto = hwcrypto;
            this._urlBase = urlBase;
            this._files = files;
            this._index = -1;
            this._file = null;

            this._signatureLevel = signatureLevel;
            this._resultFunction = resultFunction;
            this._loggerFunction = loggerFunction; // Constants

            this.DIGEST_ALGORITHM = "SHA256";
            this.SIGNATURE_PACKAGING = "ENVELOPED";
            this.CERTIFICATE_TYPE = 'SHA-256';

            this._selectedCertificate = selectedCertificate;
        }

        _createClass(NetLockSignAssist, [{
            key: "logError",
            value: function logError(message) {
                this.logText("Error:" + message);
            }
        }, {
            key: "logText",
            value: function logText(message) {
                console.log(message);

                if (this._loggerFunction != null) {
                    this._loggerFunction(message);
                }
            }
        }, {
            key: "fromHex",
            value: function fromHex(hexData) {
                var result = '';

                for (var i = 0; i < hexData.length; i += 2) {
                    result += String.fromCharCode(parseInt(hexData.substr(i, 2), 16));
                }

                return result;
            }
        }, {
            key: "buf2hex",
            value: function buf2hex(buffer) {
                return Array.prototype.map.call(buffer, function (byte) {
                    return ('0' + (byte & 0xFF).toString(16)).slice(-2);
                }).join('');
            }
        }, {
            key: "Sign",
            value: function Sign() {
                this._index = -1;
                this.SignNextFile();
            }
        }, {
            key: "SignNextFile",
            value: function SignNextFile() {
                this._index++;
                if (this._index < this._files.length)
                    this.SignFile(this._files[this._index]);
                else {
                    if (this._resultFunction) {
                        this._resultFunction(this._files, this._selectedCertificate);
                    }
                }
            }
        }, {
            key: "SignFile",
            value: function SignFile(file) {
                var thisIsMe = this;
                thisIsMe._file = file;
                thisIsMe.DownloadFile(file.FileUrl, function (blob) {
                    file.Blob = blob;
                    thisIsMe.StartSession();
                }
                );
            }
        }, {
            key: "DownloadFile",
            value: function DownloadFile(fileUrl, resolve) {
                var thisIsMe = this;
                var xmlRequest = new XMLHttpRequest();
                xmlRequest.open('GET', fileUrl, true);
                xmlRequest.responseType = 'blob';

                xmlRequest.onload = function (e) {
                    if (this.status == 200) {
                        var resultBlob = this.response;
                        resolve(resultBlob);
                    }
                    else {
                        thisIsMe.logError("Download failed. " + xmlRequest.statusText);
                        return;
                    }
                };

                xmlRequest.send();
            }
        }, {
            key: "StartSession",
                value: function StartSession() {
                var thisIsMe = this;
                var xmlRequest = new XMLHttpRequest();
                var url = thisIsMe._urlBase + '/ext-signature-sessions/';
                xmlRequest.open('put', url, true);

                xmlRequest.onreadystatechange = function () {
                    if (xmlRequest.readyState == 4 && xmlRequest.status == 200) {
                        thisIsMe._sessionId = xmlRequest.responseText;
                        thisIsMe.SessionGetCertificate();
                    }
                }, function (error) {
                    thisIsMe.logError("Starting session failed. " + error.message);
                };
                xmlRequest.send();
            }
        }, {
            key: "SessionGetCertificate",
            value: function SessionGetCertificate() {
                var thisIsMe = this;
                var lang = "hu"; // var backend = "chrome"; //"auto"

                var backend = "auto";
                if (!thisIsMe._hwcrypto.use(backend)) { }

                if (!thisIsMe._selectedCertificate) {
                    thisIsMe._hwcrypto.getCertificate({
                        lang: lang
                    }).then(function (response) {
                        thisIsMe._selectedCertificate = response;
                        thisIsMe._selectedCertificateHex = thisIsMe._selectedCertificate.hex;
                        thisIsMe.SessionSetParameters();
                        }, function (err) {
                        thisIsMe.logError(err.message);
                    });
                }
                else {
                    thisIsMe._selectedCertificateHex = thisIsMe._selectedCertificate.hex;
                    thisIsMe.SessionSetParameters();
                }
            }
        }, {
            key: "SessionSetParameters",
            // Set parameters for sign
            value: function SessionSetParameters() {
                var thisIsMe = this;
                var selectedCertificateBase64 = new Array();

                for (var i = 0; i < this._selectedCertificateHex.length / 2; i++) {
                    var h = this._selectedCertificateHex.substr(i * 2, 2);

                    selectedCertificateBase64[i] = parseInt(h, 16);
                }

                var parameters = JSON.stringify({
                    "signatureLevel": thisIsMe._signatureLevel,
                    "digestAlgorithm": thisIsMe.DIGEST_ALGORITHM,
                    "signaturePackaging": thisIsMe.SIGNATURE_PACKAGING,
                    "signingCertificate": {
                        "encodedCertificate": selectedCertificateBase64
                    }
                });
                var url = thisIsMe._urlBase + '/ext-signature-sessions/' + thisIsMe._sessionId + '/parameters';
                var xmlRequest = new XMLHttpRequest();
                xmlRequest.open('put', url, true);
                xmlRequest.setRequestHeader("Content-type", "application/json; charset=UTF-8");
                xmlRequest.onreadystatechange = function () {
                    if (xmlRequest.readyState == 4) {
                        if (xmlRequest.status != 204) {
                            thisIsMe.logError("Setting parameters failed. " + xmlRequest.responseText);
                            return;
                        }

                        thisIsMe.SessionPresign();
                    }
                }, function (error) {
                    thisIsMe.logError("Setting parameters failed. " + error.message);
                };
                xmlRequest.send(parameters);
            }
        }, {
            key: "SessionPresign",
                value: function SessionPresign() {
                var thisIsMe = this; // var reader = new FileReader();
                // reader.readAsArrayBuffer(thisIsMe._file);        

                var url = thisIsMe._urlBase + '/ext-signature-sessions/' + thisIsMe._sessionId + '/presign?doc-name=' + thisIsMe._fileName + '&format=SHA256&skip-validate=true';
                var xmlRequest = new XMLHttpRequest();
                xmlRequest.open('post', url, true);

                xmlRequest.onreadystatechange = function () {
                    if (xmlRequest.readyState == 4) {
                        if (xmlRequest.status != 200) {
                            thisIsMe.logError("Presign failed. " + xmlRequest.statusText);
                            return;
                        }
                        var resultDocumentContent = new Uint8Array(xmlRequest.response);
                        thisIsMe._hashToSign = thisIsMe.buf2hex(resultDocumentContent);
                        thisIsMe.SessionPresignHashSign();
                    }
                }, function (error) {
                    thisIsMe.logError("Presign failed. " + error.message);
                }
                xmlRequest.responseType = 'arraybuffer';
                xmlRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                xmlRequest.send(thisIsMe._file.Blob); // xmlRequest.send(thisIsMe._file);        
            }
        }, {
            key: "SessionPresignHashSign",
            value: function SessionPresignHashSign() {
                var thisIsMe = this;
                var language = "hu"; // var backend = "chrome"; //"auto"

                var backend = "auto";

                if (!window.hwcrypto.use(backend)) { }

                var hash = thisIsMe._hashToSign;
                window.hwcrypto.sign(thisIsMe._selectedCertificate, {
                    type: thisIsMe.CERTIFICATE_TYPE,
                    hex: hash
                }, {
                        lang: language
                    }).then(function (signature) {
                        thisIsMe._hashSignature = signature.value;
                        thisIsMe.SessionPostsign();
                    }, function (error) {
                        thisIsMe.logError("Signing failed. " + error.message);
                    });
            }
        }, {
            key: "SessionPostsign",
                value: function SessionPostsign() {
                var thisIsMe = this;
                var fileName = this._file.FileName;
                var signedFileName = fileName.slice(0, -4) + "-signed." + fileName.slice(-3);
                var url = thisIsMe._urlBase + '/ext-signature-sessions/' + thisIsMe._sessionId + '/postsign?doc-name=' + signedFileName;
                var xmlRequest = new XMLHttpRequest();
                xmlRequest.open("post", url); // xmlRequest.setRequestHeader("Content-type", "application/json");

                xmlRequest.responseType = "arraybuffer";

                xmlRequest.onload = function () {
                    if (xmlRequest.readyState == 4) {
                        if (xmlRequest.status != 200) {
                            thisIsMe.logError("Postsign failed. " + xmlRequest.statusText);
                            return;
                        }

                        thisIsMe.convertBlobToBase64(new Blob([xmlRequest.response]),
                            function (base64data) {
                                thisIsMe._file.Blob = null;
                                thisIsMe._file.SignedFileData = base64data;
                                thisIsMe.SignNextFile();
                            });
                    }
                }, function (error) {
                    thisIsMe.logError("Postsign failed. " + error.message);
                };

                xmlRequest.send();
            }
        }, {
            key: "convertBlobToBase64",
            value: function (blob, resolve) {
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                    var base64data = reader.result;
                    base64data = base64data.split(',')[1];
                    resolve(base64data);
                }
            }
        }]);

        return NetLockSignAssist;
    }();

var NetLockSignAssistForIratok =
    /*#__PURE__*/
    function () {
        function NetLockSignAssistForIratok(hwcrypto, urlBase, iratok, signatureLevel, iratResultFunction, resultFunction, loggerFunction) {
            _classCallCheck(this, NetLockSignAssistForIratok);

            // urlBase = 'https://nltokendemo.hu:8443/api';
            this._hwcrypto = hwcrypto;
            this._urlBase = urlBase;
            this._iratok = iratok;
            this._index = -1;
            this._irat = null;

            this._signatureLevel = signatureLevel;
            this._resultFunction = resultFunction;
            this._loggerFunction = loggerFunction;
            this._iratResultFunction = iratResultFunction;
        }

        _createClass(NetLockSignAssistForIratok, [{
            key: "logError",
            value: function logError(message) {
                this.logText("Error:" + message);
            }
        }, {
            key: "logText",
            value: function logText(message) {
                console.log(message);

                if (this._loggerFunction != null) {
                    this._loggerFunction(message);
                }
            }
        }, {
            key: "Sign",
            value: function Sign() {
                this._index = -1;
                this.SignNextIrat();
            }
        }, {
            key: "SignNextIrat",
                value: function SignNextIrat() {
                this._index++;
                    if (this._index < this._iratok.length)
                        this.SignIrat(this._iratok[this._index]);
                    else {
                        if (this._resultFunction) {
                            this._resultFunction();
                        }
                    }
            }
            }, {
                key: "SignIrat",
                value: function SignIrat(irat) {
                    var thisIsMe = this;
                    thisIsMe._irat = irat;
                    var netLockSignAssist = new NetLockSignAssist(this._hwcrypto, this._urlBase, this._irat.Files, this._signatureLevel,
                        function (files, selectedCertificate) {
                            thisIsMe._selectedCertificate = selectedCertificate;
                            thisIsMe._iratResultFunction(irat);
                        }, this._loggerFunction, this._selectedCertificate);

                    netLockSignAssist.Sign();
                }
            }]);

        return NetLockSignAssistForIratok;
    }();


