
@SET LN= 

@SET LN=%LN% -D:eRecordWebSite=C:\Contentum\eRecord\ 
@SET LN=%LN% -D:AVDH_MODE=NORMAL 
@SET LN=%LN% -D:AVDH_DELAY=15 

REM NORMAL
REM TESZT
@SET LN=%LN% -D:AVDH_DHSZ_SSL_URL=https://teszt.avdh.gov.hu 
@SET LN=%LN% -D:AVDH_TKASZ_SSL_URL=https://teszt.avdh-cert.gov.hu 

REM ÉLES
REM @SET LN=%LN% -D:AVDH_DHSZ_SSL_URL=https://avdh.gov.hu 
REM @SET LN=%LN% -D:AVDH_TKASZ_SSL_URL=https://avdh-cert.gov.hu 

@SET LN=%LN% -D:AVDH_XAdES=/avdh/XadesSigner  
@SET LN=%LN% -D:AVDH_PAdES=/avdh/PadesSigner 
@SET LN=%LN% -D:AVDH_ASiC=/avdh/AsicSigner 
@SET LN=%LN% -D:AVDH_TKASZ=/tkasz/TkaszSigner 
@SET LN=%LN% -D:AVDH_DOWNLOAD=/avdh/download?documentId=[docid] 
@SET LN=%LN% -D:AVDH_DELETE=/avdh/Delete 
@SET LN=%LN% -D:AVDH_LOGOUT=/avdh/logout 


REM DHSZ
REM TESZT
REM @SET LN=%LN% -D:AVDH_DHSZ_SSL_URL=https://dhsz.teszt.gov.hu 
REM @SET LN=%LN% -D:AVDH_TKASZ_SSL_URL=https://tkasz.dhsz.teszt.gov.hu 

REM ÉLES
REM @SET LN=%LN% -D:AVDH_DHSZ_SSL_URL=https://dhsz.gov.hu 
REM @SET LN=%LN% -D:AVDH_TKASZ_SSL_URL=https://tkasz.dhsz.gov.hu 

REM @SET LN=%LN% -D:AVDH_XAdES=/avdh/DhszXadesSigner 
REM @SET LN=%LN% -D:AVDH_PAdES=/avdh/DhszPadesSigner 
REM @SET LN=%LN% -D:AVDH_ASiC=/avdh/DhszAsicSigner 
REM @SET LN=%LN% -D:AVDH_TKASZ=/tkasz/TkaszSigner 
REM @SET LN=%LN% -D:AVDH_DOWNLOAD=/avdh/download?documentId=[docid] 
REM @SET LN=%LN% -D:AVDH_DELETE=/avdh/Delete 
REM @SET LN=%LN% -D:AVDH_LOGOUT=/avdh/logout 


c:\nant-0.85\bin\nant -f:webconfigUpdate_AVDH.build all -l:Log\webconfigUpdate_AVDH_NMHH_Ugyfel_TUKTEST.build.log %LN%