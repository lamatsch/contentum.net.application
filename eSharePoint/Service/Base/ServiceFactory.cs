
using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eSharePoint.Service
{
    public partial class ServiceFactory
    {
        string _BusinessServiceType = "SOAP";
        string _BusinessServiceUrl = "";
        string _BusinessServiceAuthentication = "Windows";
        string _BusinessServiceUserDomain = "";
        string _BusinessServiceUserName = "";
        string _BusinessServicePassword = "";

        public string BusinessServiceType
        {
            get { return _BusinessServiceType; }
            set { _BusinessServiceType = value; }
        }

        public string BusinessServiceUrl
        {
            get { return _BusinessServiceUrl; }
            set { _BusinessServiceUrl = value; }
        }

        public string BusinessServiceAuthentication
        {
            get { return _BusinessServiceAuthentication; }
            set { _BusinessServiceAuthentication = value; }
        }

        public string BusinessServiceUserDomain
        {
            get { return _BusinessServiceUserDomain; }
            set { _BusinessServiceUserDomain = value; }
        }

        public string BusinessServiceUserName
        {
            get { return _BusinessServiceUserName; }
            set { _BusinessServiceUserName = value; }
        }

        public string BusinessServicePassword
        {
            get { return _BusinessServicePassword; }
            set { _BusinessServicePassword = value; }
        }

        public FolderService GetFolderService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                FolderService _Service = new FolderService(_BusinessServiceUrl + "FolderService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public DocumentService GetDocumentService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                DocumentService _Service = new DocumentService(_BusinessServiceUrl + "DocumentService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public SiteService GetSiteService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                SiteService _Service = new SiteService(_BusinessServiceUrl + "SiteService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public ListUtilService GetListUtilService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                ListUtilService _Service = new ListUtilService(_BusinessServiceUrl + "ListUtilService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public PermissionService GetPermissionService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                PermissionService _Service = new PermissionService(_BusinessServiceUrl + "PermissionService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

    }
        
}