﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.832
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.42.
// 
namespace Contentum.eSharePoint.Service {
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "DocumentServiceSoap", Namespace = "Contentum.eSharePoint.WebService")]
    public partial class DocumentService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {

        private System.Threading.SendOrPostCallback UploadOperationCompleted;

        private System.Threading.SendOrPostCallback GetOperationCompleted;

        private System.Threading.SendOrPostCallback CheckInOperationCompleted;

        private System.Threading.SendOrPostCallback CheckOutOperationCompleted;

        private System.Threading.SendOrPostCallback UndoCheckOutOperationCompleted;

        private System.Threading.SendOrPostCallback CopyOperationCompleted;

        /// <remarks/>
        public DocumentService(string url)
        {
            this.Url = url;
        }

        /// <remarks/>
        public event UploadCompletedEventHandler UploadCompleted;

        /// <remarks/>
        public event GetCompletedEventHandler GetCompleted;

        /// <remarks/>
        public event CheckInCompletedEventHandler CheckInCompleted;

        /// <remarks/>
        public event CheckOutCompletedEventHandler CheckOutCompleted;

        /// <remarks/>
        public event UndoCheckOutCompletedEventHandler UndoCheckOutCompleted;

        /// <remarks/>
        public event CopyCompletedEventHandler CopyCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eSharePoint.WebService/Upload", RequestNamespace = "Contentum.eSharePoint.WebService", ResponseNamespace = "Contentum.eSharePoint.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result Upload(string siteName, string listName, string folderNames, string filename, [System.Xml.Serialization.XmlElementAttribute(DataType = "base64Binary")] byte[] cont)
        {
            object[] results = this.Invoke("Upload", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename,
                        cont});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginUpload(string siteName, string listName, string folderNames, string filename, byte[] cont, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("Upload", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename,
                        cont}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndUpload(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void UploadAsync(string siteName, string listName, string folderNames, string filename, byte[] cont)
        {
            this.UploadAsync(siteName, listName, folderNames, filename, cont, null);
        }

        /// <remarks/>
        public void UploadAsync(string siteName, string listName, string folderNames, string filename, byte[] cont, object userState)
        {
            if ((this.UploadOperationCompleted == null))
            {
                this.UploadOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUploadOperationCompleted);
            }
            this.InvokeAsync("Upload", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename,
                        cont}, this.UploadOperationCompleted, userState);
        }

        private void OnUploadOperationCompleted(object arg)
        {
            if ((this.UploadCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UploadCompleted(this, new UploadCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eSharePoint.WebService/Get", RequestNamespace = "Contentum.eSharePoint.WebService", ResponseNamespace = "Contentum.eSharePoint.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result Get(string siteName, string listName, string folderNames, string filename)
        {
            object[] results = this.Invoke("Get", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGet(string siteName, string listName, string folderNames, string filename, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("Get", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGet(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAsync(string siteName, string listName, string folderNames, string filename)
        {
            this.GetAsync(siteName, listName, folderNames, filename, null);
        }

        /// <remarks/>
        public void GetAsync(string siteName, string listName, string folderNames, string filename, object userState)
        {
            if ((this.GetOperationCompleted == null))
            {
                this.GetOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetOperationCompleted);
            }
            this.InvokeAsync("Get", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename}, this.GetOperationCompleted, userState);
        }

        private void OnGetOperationCompleted(object arg)
        {
            if ((this.GetCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetCompleted(this, new GetCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eSharePoint.WebService/CheckIn", RequestNamespace = "Contentum.eSharePoint.WebService", ResponseNamespace = "Contentum.eSharePoint.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result CheckIn(string siteName, string listName, string folderNames, string filename, string komment, string checkInMode)
        {
            object[] results = this.Invoke("CheckIn", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename,
                        komment,
                        checkInMode});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginCheckIn(string siteName, string listName, string folderNames, string filename, string komment, string checkInMode, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("CheckIn", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename,
                        komment,
                        checkInMode}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndCheckIn(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void CheckInAsync(string siteName, string listName, string folderNames, string filename, string komment, string checkInMode)
        {
            this.CheckInAsync(siteName, listName, folderNames, filename, komment, checkInMode, null);
        }

        /// <remarks/>
        public void CheckInAsync(string siteName, string listName, string folderNames, string filename, string komment, string checkInMode, object userState)
        {
            if ((this.CheckInOperationCompleted == null))
            {
                this.CheckInOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCheckInOperationCompleted);
            }
            this.InvokeAsync("CheckIn", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename,
                        komment,
                        checkInMode}, this.CheckInOperationCompleted, userState);
        }

        private void OnCheckInOperationCompleted(object arg)
        {
            if ((this.CheckInCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CheckInCompleted(this, new CheckInCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eSharePoint.WebService/CheckOut", RequestNamespace = "Contentum.eSharePoint.WebService", ResponseNamespace = "Contentum.eSharePoint.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result CheckOut(string siteName, string listName, string folderNames, string filename)
        {
            object[] results = this.Invoke("CheckOut", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginCheckOut(string siteName, string listName, string folderNames, string filename, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("CheckOut", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndCheckOut(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void CheckOutAsync(string siteName, string listName, string folderNames, string filename)
        {
            this.CheckOutAsync(siteName, listName, folderNames, filename, null);
        }

        /// <remarks/>
        public void CheckOutAsync(string siteName, string listName, string folderNames, string filename, object userState)
        {
            if ((this.CheckOutOperationCompleted == null))
            {
                this.CheckOutOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCheckOutOperationCompleted);
            }
            this.InvokeAsync("CheckOut", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename}, this.CheckOutOperationCompleted, userState);
        }

        private void OnCheckOutOperationCompleted(object arg)
        {
            if ((this.CheckOutCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CheckOutCompleted(this, new CheckOutCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eSharePoint.WebService/UndoCheckOut", RequestNamespace = "Contentum.eSharePoint.WebService", ResponseNamespace = "Contentum.eSharePoint.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result UndoCheckOut(string siteName, string listName, string folderNames, string filename)
        {
            object[] results = this.Invoke("UndoCheckOut", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginUndoCheckOut(string siteName, string listName, string folderNames, string filename, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("UndoCheckOut", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndUndoCheckOut(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void UndoCheckOutAsync(string siteName, string listName, string folderNames, string filename)
        {
            this.UndoCheckOutAsync(siteName, listName, folderNames, filename, null);
        }

        /// <remarks/>
        public void UndoCheckOutAsync(string siteName, string listName, string folderNames, string filename, object userState)
        {
            if ((this.UndoCheckOutOperationCompleted == null))
            {
                this.UndoCheckOutOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUndoCheckOutOperationCompleted);
            }
            this.InvokeAsync("UndoCheckOut", new object[] {
                        siteName,
                        listName,
                        folderNames,
                        filename}, this.UndoCheckOutOperationCompleted, userState);
        }

        private void OnUndoCheckOutOperationCompleted(object arg)
        {
            if ((this.UndoCheckOutCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UndoCheckOutCompleted(this, new UndoCheckOutCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eSharePoint.WebService/Copy", RequestNamespace = "Contentum.eSharePoint.WebService", ResponseNamespace = "Contentum.eSharePoint.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result Copy(string fromSiteName, string fromDoclibName, string fromFoldersName, string filename, string toSiteName, string toDoclibName, string toFoldersName)
        {
            object[] results = this.Invoke("Copy", new object[] {
                        fromSiteName,
                        fromDoclibName,
                        fromFoldersName,
                        filename,
                        toSiteName,
                        toDoclibName,
                        toFoldersName});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginCopy(string fromSiteName, string fromDoclibName, string fromFoldersName, string filename, string toSiteName, string toDoclibName, string toFoldersName, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("Copy", new object[] {
                        fromSiteName,
                        fromDoclibName,
                        fromFoldersName,
                        filename,
                        toSiteName,
                        toDoclibName,
                        toFoldersName}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndCopy(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void CopyAsync(string fromSiteName, string fromDoclibName, string fromFoldersName, string filename, string toSiteName, string toDoclibName, string toFoldersName)
        {
            this.CopyAsync(fromSiteName, fromDoclibName, fromFoldersName, filename, toSiteName, toDoclibName, toFoldersName, null);
        }

        /// <remarks/>
        public void CopyAsync(string fromSiteName, string fromDoclibName, string fromFoldersName, string filename, string toSiteName, string toDoclibName, string toFoldersName, object userState)
        {
            if ((this.CopyOperationCompleted == null))
            {
                this.CopyOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCopyOperationCompleted);
            }
            this.InvokeAsync("Copy", new object[] {
                        fromSiteName,
                        fromDoclibName,
                        fromFoldersName,
                        filename,
                        toSiteName,
                        toDoclibName,
                        toFoldersName}, this.CopyOperationCompleted, userState);
        }

        private void OnCopyOperationCompleted(object arg)
        {
            if ((this.CopyCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CopyCompleted(this, new CopyCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }


        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void UploadCompletedEventHandler(object sender, UploadCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class UploadCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal UploadCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetCompletedEventHandler(object sender, GetCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void CheckInCompletedEventHandler(object sender, CheckInCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class CheckInCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal CheckInCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void CheckOutCompletedEventHandler(object sender, CheckOutCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class CheckOutCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal CheckOutCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void UndoCheckOutCompletedEventHandler(object sender, UndoCheckOutCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class UndoCheckOutCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal UndoCheckOutCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void CopyCompletedEventHandler(object sender, CopyCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class CopyCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal CopyCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
    }
}
