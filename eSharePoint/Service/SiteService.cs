﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.832
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.42.
// 
namespace Contentum.eSharePoint.Service {
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "SiteServiceSoap", Namespace = "Contentum.eSharePoint.WebService")]
    public partial class SiteService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {

        private System.Threading.SendOrPostCallback CreateOperationCompleted;

        private System.Threading.SendOrPostCallback IsExistsSiteOperationCompleted;

        /// <remarks/>
        public SiteService(string url)
        {
            this.Url = url;
        }

        /// <remarks/>
        public event CreateCompletedEventHandler CreateCompleted;

        /// <remarks/>
        public event IsExistsSiteCompletedEventHandler IsExistsSiteCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eSharePoint.WebService/Create", RequestNamespace = "Contentum.eSharePoint.WebService", ResponseNamespace = "Contentum.eSharePoint.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result Create(string path, string siteName, string siteTitle, string siteTemplateName)
        {
            object[] results = this.Invoke("Create", new object[] {
                        path,
                        siteName,
                        siteTitle,
                        siteTemplateName});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginCreate(string path, string siteName, string siteTitle, string siteTemplateName, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("Create", new object[] {
                        path,
                        siteName,
                        siteTitle,
                        siteTemplateName}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndCreate(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void CreateAsync(string path, string siteName, string siteTitle, string siteTemplateName)
        {
            this.CreateAsync(path, siteName, siteTitle, siteTemplateName, null);
        }

        /// <remarks/>
        public void CreateAsync(string path, string siteName, string siteTitle, string siteTemplateName, object userState)
        {
            if ((this.CreateOperationCompleted == null))
            {
                this.CreateOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCreateOperationCompleted);
            }
            this.InvokeAsync("Create", new object[] {
                        path,
                        siteName,
                        siteTitle,
                        siteTemplateName}, this.CreateOperationCompleted, userState);
        }

        private void OnCreateOperationCompleted(object arg)
        {
            if ((this.CreateCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CreateCompleted(this, new CreateCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eSharePoint.WebService/IsExistsSite", RequestNamespace = "Contentum.eSharePoint.WebService", ResponseNamespace = "Contentum.eSharePoint.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result IsExistsSite(string path, string siteTitle)
        {
            object[] results = this.Invoke("IsExistsSite", new object[] {
                        path,
                        siteTitle});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginIsExistsSite(string path, string siteTitle, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("IsExistsSite", new object[] {
                        path,
                        siteTitle}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndIsExistsSite(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void IsExistsSiteAsync(string path, string siteTitle)
        {
            this.IsExistsSiteAsync(path, siteTitle, null);
        }

        /// <remarks/>
        public void IsExistsSiteAsync(string path, string siteTitle, object userState)
        {
            if ((this.IsExistsSiteOperationCompleted == null))
            {
                this.IsExistsSiteOperationCompleted = new System.Threading.SendOrPostCallback(this.OnIsExistsSiteOperationCompleted);
            }
            this.InvokeAsync("IsExistsSite", new object[] {
                        path,
                        siteTitle}, this.IsExistsSiteOperationCompleted, userState);
        }

        private void OnIsExistsSiteOperationCompleted(object arg)
        {
            if ((this.IsExistsSiteCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.IsExistsSiteCompleted(this, new IsExistsSiteCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void CreateCompletedEventHandler(object sender, CreateCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class CreateCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal CreateCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void IsExistsSiteCompletedEventHandler(object sender, IsExistsSiteCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class IsExistsSiteCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal IsExistsSiteCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
    }
}
