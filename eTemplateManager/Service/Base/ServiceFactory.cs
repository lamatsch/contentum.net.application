
using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eTemplateManager.Service
{
    public partial class ServiceFactory
    {
        string _BusinessServiceType = "SOAP";
        string _BusinessServiceUrl = "";
        string _BusinessServiceAuthentication = "Windows";
        string _BusinessServiceUserDomain = "";
        string _BusinessServiceUserName = "";
        string _BusinessServicePassword = "";

        private static void SetDefaults(System.Web.Services.Protocols.SoapHttpClientProtocol service)
        {
            service.PreAuthenticate = true;
            // BUG_11753
            service.AllowAutoRedirect = true;
        }

        public string BusinessServiceType
        {
            get { return _BusinessServiceType; }
            set { _BusinessServiceType = value; }
        }

        public string BusinessServiceUrl
        {
            get { return _BusinessServiceUrl; }
            set { _BusinessServiceUrl = value; }
        }

        public string BusinessServiceAuthentication
        {
            get { return _BusinessServiceAuthentication; }
            set { _BusinessServiceAuthentication = value; }
        }

        public string BusinessServiceUserDomain
        {
            get { return _BusinessServiceUserDomain; }
            set { _BusinessServiceUserDomain = value; }
        }

        public string BusinessServiceUserName
        {
            get { return _BusinessServiceUserName; }
            set { _BusinessServiceUserName = value; }
        }

        public string BusinessServicePassword
        {
            get { return _BusinessServicePassword; }
            set { _BusinessServicePassword = value; }
        }

        public TemplateManagerService GetTemplateManagerService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                TemplateManagerService _Service = new TemplateManagerService(_BusinessServiceUrl + "TemplateManagerService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }
   }
        
}