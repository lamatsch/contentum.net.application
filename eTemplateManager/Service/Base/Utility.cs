using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eTemplateManager.Service
{
    public class Utility
    {
        public static System.Net.CredentialCache GetCacheSetting(string Url, string _BusinessServiceAuthentication, string _BusinessServiceUserDomain, string _BusinessServiceUserName, string _BusinessServicePassword)
        {
            System.Net.CredentialCache cache = new System.Net.CredentialCache();

            if (_BusinessServiceAuthentication == "Windows")
            {               
                cache.Add(new Uri(Url), // Web service URL
                          "Negotiate",         // Kerberos or NTLM
                           System.Net.CredentialCache.DefaultNetworkCredentials);
                return cache;
                
            }
            if (_BusinessServiceAuthentication == "Basic")
            {
                cache.Add(new Uri(Url), // Web service URL
                          "Basic",         // Kerberos or NTLM
                           new System.Net.NetworkCredential(_BusinessServiceUserName, _BusinessServicePassword, _BusinessServiceUserDomain));
                return cache;

            }
            if (_BusinessServiceAuthentication == "NTLM")
            {
                cache.Add(new Uri(Url), // Web service URL
                          "Negotiate",         // Kerberos or NTLM
                           new System.Net.NetworkCredential(_BusinessServiceUserName, _BusinessServicePassword, _BusinessServiceUserDomain));
                return cache;

            }

            return null;
        }
    }
}
