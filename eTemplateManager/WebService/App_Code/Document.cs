// =====================================================================================
// Document.cs
//
// This class creates and filles in documents from xml string based on templates
//
// TODO: + log events
//       - release resources in catch
//
// =====================================================================================

using System;
using System.Collections;
using System.Data;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;
using docConverter; // COM wrapper for Pdf converter
using Contentum.eTemplateManager.Service;
using Contentum.eBusinessDocuments;
// COM references
//using Office = Microsoft.Office.Core;
//using Excel = Microsoft.Office.Interop.Excel;
// Native pdf handler
using ICSharpCode.SharpZipLib;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;

using System.Runtime.InteropServices;
using HWND = System.IntPtr;


namespace Contentum.eTemplateManager.WebService
{
    // bel�lre, hogy ne okozzon n�vt�rkonfliktust
    using Excel = Microsoft.Office.Interop.Excel;
    /// <summary>
    /// This class creates and filles in documents from xml based on
    /// document templates
    /// </summary>
    public class DocumentHandler
    {
        #region Win32 types

        /// <summary>
        /// Calling PostMessage Win32 API
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="wMsg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        [DllImport("user32")]
        public static extern int PostMessage(HWND hwnd, int wMsg, int wParam, int lParam);

        /// <summary>
        /// WM_QUIT Win32 messageid
        /// </summary>
        public const int WM_QUIT = 0x12;

        #endregion Win32 types

        #region Enums

        /// <summary>
        /// Template types
        /// </summary>
        enum TemplateTypeName
        {
            /// <summary>Word document</summary>
            DOC,
            /// <summary>Word ml document</summary>
            WML,
            /// <summary>Pdf</summary>
            PDF,
            /// <summary>Excel document</summary>
            XLS
        }

        #endregion Enums

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public DocumentHandler()
        {
        }

        #endregion Constructors

        #region Constants
        public const string TEMPFILE_PATH = "C:\\temp\\ContentumNet";
        // alap�rtelmez�sben megpr�b�ljuk l�trehozni a k�nyvt�rat
        // az ideiglenes f�jlok sz�m�ra, ha m�g nem l�tezik
        public const bool CREATE_TEMPFILE_PATH_IFDOESNOTEXIST = true;
        #endregion Constants

        #region Methods

        /*public void CreateWordFile(byte[] result, bool convertToPdf)
        {
            string outputFile = "";
            if (convertToPdf)
            {
                outputFile = "c:\\Documents and Settings\\kiss.gergely\\Asztal\\test2.pdf";
            }
            else
            {
                outputFile = "c:\\Documents and Settings\\kiss.gergely\\Asztal\\test2.doc";
            }
            FileStream fs = new FileStream(outputFile, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(result);
            bw.Close();
        }

        public void CreateExcelFile(byte[] result, bool convertToPdf)
        {
            string outputFile = "";
            if (convertToPdf)
            {
                outputFile = "c:\\Documents and Settings\\kiss.gergely\\Asztal\\test.pdf";
            }
            else
            {
                outputFile = "c:\\Documents and Settings\\kiss.gergely\\Asztal\\test.xls";
            }
            FileStream fs = new FileStream(outputFile, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(result);
            bw.Close();
        }

        public string GetXml(string xml)
        {
            StreamReader reader = new StreamReader(xml);
            xml = reader.ReadToEnd();
            return xml;
        }*/

        /// <summary>
        /// Load file into a byte array
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private byte[] GetFile(string filePath)
        {

            try
            {
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);

                byte[] fileRD = br.ReadBytes((int)fs.Length);

                br.Close();
                fs.Close();

                return fileRD;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string TrimStartXmlString(string xml)
        {
            string result = String.Empty;

            try
            {
                // Skip invalid beginner characters in result xml
                char[] chars = xml.ToCharArray();
                for (long i = 0; i < chars.LongLength; i++)
                {
                    if (chars[i] == (char)0x3c)
                    {
                        break;
                    }
                    chars[i] = (char)0x20; // space
                }

                UTF8Encoding coderIn = new UTF8Encoding(true);
                String buffer = new String(chars);
                result = buffer.Trim();

            }
            catch (Exception ex)
            {
                throw new Exception("Hiba l�pett fel a string �rv�nytelen kezd� karaktereinek lev�g�sa sor�n.", ex);
            }

            return result;
        }


        /// <summary>
        /// Ideiglenes f�jlokhoz f�jln�v gener�l�sa
        /// - megadott kiterjeszt�ssel (ha az extension param�ter ki van t�ltve)
        /// - v�letlenszer� kiterjeszt�ssel (ha az extension param�ter �res vagy null)
        /// </summary>
        /// <param name="extension">Ideiglenes f�jl nev�nek kiterjeszt�se (ponttal vagy an�lk�l) - lehet null vagy �res is, ekkor a kiterjeszt�s v�letlenszer�</param>
        /// <returns>Gener�lt f�ljn�v</returns>
        public static string GetRandomFileNameForTemporaryFile(string extension)
        {
            if (string.IsNullOrEmpty(extension))
            {
                return System.IO.Path.GetRandomFileName();
            }
            else
            {
                return System.IO.Path.ChangeExtension(System.IO.Path.GetRandomFileName().Replace(".", ""), extension);
            }
        }

        // try-catchben haszn�land�!
        public static string GetTemporaryFileNameWithPath(string extension)
        {
            return GetTemporaryFileNameWithPath(extension, DocumentHandler.CREATE_TEMPFILE_PATH_IFDOESNOTEXIST); // default
        }

        // try-catchben haszn�land�!
        public static string GetTemporaryFileNameWithPath(string extension, bool createPathIfDoesNotExist)
        {
            String path = DocumentHandler.TEMPFILE_PATH; // "c:\\temp\\ContentumNet";
            if (createPathIfDoesNotExist)
            {
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }
            }

            return System.IO.Path.Combine(path, DocumentHandler.GetRandomFileNameForTemporaryFile(extension));
        }
        #endregion Methods

        #region Methods - Excel related

        /// <summary>
        /// Get excel document
        /// </summary>
        /// <param name="application"></param>
        /// <param name="template"></param>
        /// <param name="state"></param>
        /// <param name="xml"></param>
        /// <param name="convertToPdf"></param>
        /// <returns>Binary content of Excel file </returns>
        public byte[] GetExcelDocument(byte[] template,
          string xml,
          bool convertToPdf)
        {
            byte[] result = null;
            string filePath = String.Empty;
            string filePathOut = String.Empty;

            try
            {
                //xml = GetXml(xml);

                //String Path = "c:\\temp\\ContentumNet";
                //if (!System.IO.Directory.Exists(Path))
                //{
                //    System.IO.Directory.CreateDirectory(Path);
                //}
                //Random r = new Random();
                //String FileName = r.Next().ToString() + ".xls";
                filePath = GetTemporaryFileNameWithPath(".xls");
                System.IO.FileStream fs = new System.IO.FileStream(filePath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                System.IO.BinaryWriter bw = new System.IO.BinaryWriter(fs);
                bw.Write(template);
                bw.Close();
                fs.Close();

                // Fill in Excel workbook with xml data
                ImportXmltoXls(filePath, xml);
                // Convert an Excel workbook to a pdf
                if (convertToPdf)
                {
                    filePathOut = ConvertToPdf(filePath);
                }
                else
                {
                    filePathOut = filePath;
                }

                // Get binary result from file
                result = GetFile(filePathOut);
                // Delete temporary file
                try
                {
                    File.Delete(filePath);
                    File.Delete(filePathOut);
                }
                catch
                {
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Hiba l�pett fel a dokumentum el��ll�t�sa sor�n.", ex);
            }

            return result;
        }

        /// <summary>
        /// Get excel document from a dataset that includes also the control information
        /// to determine the cells to fill in and the data to fill cells with
        /// </summary>
        /// <param name="template"></param>
        /// <param name="ds"></param>
        /// <param name="targetCellsColumnName"></param>
        /// <param name="targetSeparators"></param>
        /// <param name="targetCellSeparators"></param>
        /// <returns>Binary content of Excel file as Result</returns>
        public byte[] GetExcelDocumentFromDataSet(byte[] template,
            DataSet ds,
            string targetCellsColumnName, char[] targetSeparators, char[] targetCellSeparators,
            bool convertToPdf)
        {
            byte[] result = null;
            string filePath = String.Empty;
            string filePathOut = String.Empty;

            try
            {
                //xml = GetXml(xml);

                //String Path = "c:\\temp\\ContentumNet";
                //if (!System.IO.Directory.Exists(Path))
                //{
                //    System.IO.Directory.CreateDirectory(Path);
                //}
                //Random r = new Random();
                //String FileName = r.Next().ToString() + ".xls";
                filePath = GetTemporaryFileNameWithPath(".xls");
                System.IO.FileStream fs = new System.IO.FileStream(filePath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                System.IO.BinaryWriter bw = new System.IO.BinaryWriter(fs);
                bw.Write(template);
                bw.Close();
                fs.Close();

                // Fill in Excel workbook with data
                ImportDataSetToXls(filePath, ds, targetCellsColumnName, targetSeparators, targetCellSeparators);

                // Convert an Excel workbook to a pdf
                if (convertToPdf)
                {
                    filePathOut = ConvertToPdf(filePath);
                }
                else
                {
                    filePathOut = filePath;
                }

                // Get binary result from file
                result = GetFile(filePathOut);
                // Delete temporary file
                try
                {
                    File.Delete(filePath);
                    File.Delete(filePathOut);
                }
                catch
                {
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Hiba l�pett fel a dokumentum el��ll�t�sa sor�n.", ex);
            }

            return result;
        }


        #region KILLING HANGING EXCEL PROCESSES

        [DllImport("user32.dll")]
        static extern void EndTask(IntPtr hWnd);
        [DllImport("user32.dll")]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll")]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);
        [DllImport("kernel32.dll")]
        static extern void SetLastError(uint dwErrCode);

        private void EnsureProcessKilled(IntPtr mainWindowHandle, string caption)
        {
            uint iRes, iProcID;

            SetLastError(0);

            if (IntPtr.Equals(mainWindowHandle, IntPtr.Zero))
                mainWindowHandle = FindWindow(null, caption);

            if (IntPtr.Equals(mainWindowHandle, IntPtr.Zero))
                return;

            iRes = GetWindowThreadProcessId(mainWindowHandle, out iProcID);
            if (iProcID == 0)
            {
                EndTask(mainWindowHandle);
                if (Marshal.GetLastWin32Error() != 0)
                    return;
                throw new ApplicationException("Failed to close Excel Process.");
            }

            System.Diagnostics.Process proc = System.Diagnostics.Process.GetProcessById((int)iProcID);
            proc.CloseMainWindow();
            proc.Refresh();
            if (proc.HasExited)
                return;

            proc.Kill();
        }

        #endregion

        /// <summary>
        /// Import xml file into excel workbook
        /// </summary>
        /// <param name="excelFilePath">Excel file path</param>
        /// <param name="xml">Xml data</param>
        private void ImportXmltoXls(string excelFilePath, string xml)
        {
            bool isOpenedExcel = false;
            bool isOpenedWorkbook = false;
            Excel.Workbook excelWorkbook = null;
            Excel.Application excelAppl = null;

            IntPtr iHandle = IntPtr.Zero;

            try
            {
                // Load Excel template
                excelAppl = new Excel.ApplicationClass();
                // alert-ek letilt�sa, p�ld�ul ment�sn�l compatibility check ne akad�lyozza a ment�st
                excelAppl.DisplayAlerts = false;
                excelAppl.Visible = false;
                excelAppl.Caption = System.Guid.NewGuid().ToString().ToUpper();
                string sVer  = excelAppl.Version;
                
                iHandle = IntPtr.Zero;

                if (double.Parse(sVer.Replace(',','.'), NumberStyles.AllowDecimalPoint, new CultureInfo("en-GB")) >= 10)
                {
                    iHandle = new IntPtr((int)excelAppl.Parent.Hwnd);
                }

                isOpenedExcel = true;

                excelWorkbook = excelAppl.Workbooks.Open(excelFilePath, 0, false, 5, "", "",
                  false, Excel.XlPlatform.xlWindows, "", true, false, 0, false, false, false);

                isOpenedWorkbook = true;

                // Load xml data
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);

                Excel.Sheets excelSheets = excelWorkbook.Worksheets;

                if (excelSheets.Count > 0)
                {
                    // Import arraydata

                    // Iterate through all table in xml data and find the corresponding Excel worksheet 
                    XmlNodeList tables = GetTableNodes(xmlDoc);
                    if (tables != null)
                    {
                        for (int i = 0; i < tables.Count; i++)
                        {
                            string tableNum = String.Empty;
                            // Get table number
                            XmlNode tableNode = tables.Item(i);
                            XmlNode node = tableNode.SelectSingleNode("NAME");
                            if (node != null)
                            {
                                tableNum = node.InnerText;

                                // Find worksheet by name
                                Excel.Worksheet currentExcelWorksheet = FindWorksheetByName(excelSheets, tableNum);
                                if (currentExcelWorksheet != null)
                                {
                                    currentExcelWorksheet.Visible = Excel.XlSheetVisibility.xlSheetVisible;
                                    currentExcelWorksheet.Activate();

                                    // Get arraydata and import them into the active worksheet
                                    ArrayData data = GetArrayData(tableNode);
                                    if (data != null)
                                    {
                                        // Import array data into the active worksheet
                                        Excel.Range range = currentExcelWorksheet.get_Range(data.ExcelPosition, data.ExcelPosition);
                                        range = range.get_Resize(data.RowCount, data.ColumnCount);
                                        range.Value2 = data.Data;
                                    }
                                }
                            }
                        }
                    }

                    // Import xml data by xml maps

                    // Iterate through all worksheet and import xml data
                    for (int i = 0; i < excelSheets.Count; i++)
                    {
                        Excel.Worksheet currentExcelWorksheet = (Excel.Worksheet)excelSheets.get_Item(i + 1);
                        //currentExcelWorksheet.Visible = Excel.XlSheetVisibility.xlSheetVisible;
                        //currentExcelWorksheet.Activate();

                        // Get Xml map collection and import xml into all worksheet
                        for (int j = 1; j <= excelWorkbook.XmlMaps.Count; j++)
                        {
                            Excel.XmlMap currentMap = excelWorkbook.XmlMaps.get_Item(j);

                            // Import xml data into the current worksheet
                            excelWorkbook.XmlMaps.get_Item(currentMap.Name).ImportXml(xml, true);
                        }
                    }
                    for (int i = 0; i < excelSheets.Count; i++)
                    {
                        Excel.PivotTables pt = null;

                        Excel.Worksheet currentExcelWorksheet = (Excel.Worksheet)excelSheets.get_Item(i + 1);
                        //currentExcelWorksheet.Visible = Excel.XlSheetVisibility.xlSheetVisible;
                        //currentExcelWorksheet.Activate();

                        pt = (Excel.PivotTables)currentExcelWorksheet.PivotTables(Type.Missing);
                        
                        if (pt.Count > 0)
                        {
                            for (int j = 1; j <= pt.Count; j++)
                            {
                                pt.Item(j).RefreshTable();
                            }
                        }
                    }
                }

                // Save and close the workbook
                excelWorkbook.Save();
                excelWorkbook.Close(false, "", false);

                excelAppl.Workbooks.Close();

                isOpenedWorkbook = false;

                // Quit from excel
                excelAppl.Quit();
                // Force to quit from excel
                //PostMessage((HWND)excelAppl.Hwnd, WM_QUIT, 0, 0);

                // force Excel instance to end itself (dangeroues, do not use it in web methods)
                //System.Diagnostics.Process[] pProcesses = null;
                //pProcesses = System.Diagnostics.Process.GetProcessesByName("Excel");
                //if (pProcesses != null)
                //{
                //    foreach (System.Diagnostics.Process pProcess in pProcesses)
                //    {
                //        pProcess.Kill();
                //    }
                //}

                EnsureProcessKilled(iHandle, excelAppl.Caption);
                excelWorkbook = null;

                isOpenedExcel = false;
                excelAppl = null;
            }
            catch (Exception ex)
            {
                // Close workbook and quit from excel
                if (isOpenedWorkbook && excelWorkbook != null)
                {
                    excelWorkbook.Close(false, "", false);
                }
                if (isOpenedExcel && excelAppl != null)
                {
                    excelAppl.Quit();

                    // Force to quit from excel
                    EnsureProcessKilled(iHandle, excelAppl.Caption);
                    //PostMessage((HWND)excelAppl.Hwnd, WM_QUIT, 0, 0);
                }

                throw new Exception("Hiba l�pett fel az Excel sablon kit�lt�se sor�n.", ex);
            }

            return;
        }


        /// <summary>
        /// Import xml file into excel workbook
        /// </summary>
        /// <param name="excelFilePath">Excel file path</param>
        /// <param name="ds">DataSet including the target cell information of the Excel sheets</param>
        /// <param name="targetCellsColumnName">Name of the column in the dataset tables that includes the target cell information of the Excel sheets
        /// Format of the target cell information has to be:
        /// "<sheetname><targetCellSeparators><cellname><targetCellSeparators><columnname>[[<targetSeparators><sheetname><targetCellSeparators><cellname><targetCellSeparators><columnname><targetSeparators>]...]"
        /// </param>
        /// <param name="targetSeparators">Separator chars separating the target informations in the target cell information string</param>
        /// <param name="targetCellSeparators">Separator chars separating the sheet, cell, column informations within a target cell information token</param>
        /// 
        private void ImportDataSetToXls(string excelFilePath, DataSet ds, string targetCellsColumnName, char[] targetSeparators, char[] targetCellSeparators)
        {
            using (new ExcelUILanguageHelper())
            {

                bool isOpenedExcel = false;
                bool isOpenedWorkbook = false;
                Excel.Workbook excelWorkbook = null;
                Excel.Application excelAppl = null;

                IntPtr iHandle = IntPtr.Zero;

                string strTargetCells = "";
                string[] astrTargetCells = null;
                string[] targetElements = null;
                string fillErrors = ""; // sz�ks�g eset�n ennek seg�ts�g�vel visszanyerhet�k a ki nme t�lt�tt mez�k
                try
                {
                    // Load Excel template
                    excelAppl = new Excel.ApplicationClass();
                    // alert-ek letilt�sa, p�ld�ul ment�sn�l compatibility check ne akad�lyozza a ment�st
                    excelAppl.DisplayAlerts = false;
                    excelAppl.Visible = false;
                    excelAppl.Caption = System.Guid.NewGuid().ToString().ToUpper();
                    string sVer = excelAppl.Version;

                    iHandle = IntPtr.Zero;

                    if (double.Parse(sVer.Replace(',', '.'), NumberStyles.AllowDecimalPoint, new CultureInfo("en-GB")) >= 10)
                    {
                        iHandle = new IntPtr((int)excelAppl.Parent.Hwnd);
                    }

                    isOpenedExcel = true;

                    excelWorkbook = excelAppl.Workbooks.Open(excelFilePath, 0, false, 5, "", "",
                      false, Excel.XlPlatform.xlWindows, "", true, false, 0, false, false, false);

                    isOpenedWorkbook = true;

                    Excel.Sheets excelSheets = excelWorkbook.Worksheets;

                    if (excelSheets.Count > 0)
                    {
                        for (int k = 0; k < ds.Tables.Count; k++)
                        {
                            for (int i = 0; i < ds.Tables[k].Rows.Count; i++)
                            {
                                strTargetCells = ds.Tables[k].Rows[i][targetCellsColumnName].ToString();
                                astrTargetCells = strTargetCells.Split(targetSeparators, System.StringSplitOptions.RemoveEmptyEntries);
                                for (int j = 0; j < astrTargetCells.Length; j++)
                                {
                                    targetElements = astrTargetCells[j].Split(targetCellSeparators, System.StringSplitOptions.RemoveEmptyEntries);
                                    Excel.Worksheet excelSheet = FindWorksheetByName(excelSheets, targetElements[0]);
                                    try
                                    {
                                        //if (excelSheet != null)
                                        //{
                                        excelSheet.get_Range(targetElements[1], targetElements[1]).set_Value(System.Reflection.Missing.Value, ds.Tables[k].Rows[i][targetElements[2]].ToString());
                                        //}
                                    }
                                    catch (Exception ex)
                                    {
                                        fillErrors += "<br />" + targetElements[0] + "!" + targetElements[1] + " (" + targetElements[2] + ")";
                                    }
                                }

                            }
                        }

                    }

                    if (!String.IsNullOrEmpty(fillErrors))
                    {
                        throw new Exception("Ki nem t�lt�tt mez�k: " + fillErrors);
                    }

                    // Save and close the workbook
                    excelWorkbook.Save();
                    excelWorkbook.Close(false, "", false);

                    excelAppl.Workbooks.Close();

                    isOpenedWorkbook = false;

                    // Quit from excel
                    excelAppl.Quit();

                    EnsureProcessKilled(iHandle, excelAppl.Caption);
                    excelWorkbook = null;

                    isOpenedExcel = false;
                    excelAppl = null;
                }
                catch (Exception ex)
                {
                    // Close workbook and quit from excel
                    if (isOpenedWorkbook && excelWorkbook != null)
                    {
                        excelWorkbook.Close(false, "", false);
                    }
                    if (isOpenedExcel && excelAppl != null)
                    {
                        excelAppl.Quit();

                        // Force to quit from excel
                        EnsureProcessKilled(iHandle, excelAppl.Caption);
                        //PostMessage((HWND)excelAppl.Hwnd, WM_QUIT, 0, 0);
                    }

                    throw new Exception("Hiba l�pett fel az Excel sablon kit�lt�se sor�n.", ex);
                }

            }
        }

        /// <summary>
        /// Find Excel worksheet by name beginner
        /// </summary>
        /// <param name="excelSheets"></param>
        /// <param name="tableNum"></param>
        /// <returns></returns>
        private Excel.Worksheet FindWorksheetByName(Excel.Sheets excelSheets, string tableNum)
        {
            Excel.Worksheet result = null;

            try
            {
                // Check input parameters  
                if (excelSheets == null)
                {
                    throw new Exception("Az Excel munkaf�zet nem l�tezik.");
                }

                // Iterate through all worksheet
                for (int i = 0; i < excelSheets.Count; i++)
                {
                    Excel.Worksheet sheet = (Excel.Worksheet)excelSheets.get_Item(i + 1);
                    // Compare name beginner
                    if (sheet.Name.StartsWith(tableNum) == true)
                    {
                        result = sheet;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        /// <summary>
        /// Get BODY/TABLES node list
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        private XmlNodeList GetTableNodes(XmlDocument xmlDoc)
        {
            XmlNodeList result = null;

            try
            {
                string xPath = String.Empty;

                // Check input parameters  
                if (xmlDoc == null)
                {
                    throw new Exception("Hiba l�pett fel az xml adat bet�lt�se sor�n.");
                }

                // Skip application defined root node
                XmlNode rootNode = null;
                XmlNodeList nodeList = xmlDoc.ChildNodes;
                if (nodeList.Count == 1)
                {
                    rootNode = nodeList.Item(0);
                }
                else
                {
                    rootNode = nodeList.Item(1);
                }

                // Build xpath
                xPath = "BODY/TABLES";

                // Get Tables node
                XmlNode node = rootNode.SelectSingleNode(xPath);
                if (node == null)
                {
                    return result;
                }

                // Get child nodes
                nodeList = node.ChildNodes;

                // Set result
                result = nodeList;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        /// <summary>
        /// Convert xml data to an array data for Excel import and retrieve some
        /// other node value
        /// </summary>
        /// <param name="tableNode"></param>
        /// <returns></returns>
        private ArrayData GetArrayData(XmlNode tableNode)
        {
            ArrayData result = null;

            try
            {
                object[,] dataArray = null;
                string data = String.Empty;
                string excelPosition = String.Empty;
                string tableName = String.Empty;
                long rowCount = 0;
                long columnCount = 0;

                // Check input parameters  
                if (tableNode == null)
                {
                    throw new Exception("A t�bla xml csom�pont nem l�tezik.");
                }

                // Build xpathes
                string xPathExcelPosition = "EXCELPOSITION";
                string xPathTableName = "NAME";
                string xPath = "ARRAYDATA";

                // Get excel position node value
                XmlNode node = tableNode.SelectSingleNode(xPathExcelPosition);
                if (node == null)
                {
                    return result;
                }
                excelPosition = node.InnerText;

                // Get table name node value
                node = tableNode.SelectSingleNode(xPathTableName);
                if (node == null)
                {
                    return result;
                }
                tableName = node.InnerText;

                // Get innertext of arraydata node by xpath
                node = tableNode.SelectSingleNode(xPath);
                if (node == null)
                {
                    return result;
                }
                data = node.InnerText;
                data = data.Trim();
                if (data.Length == 0)
                {
                    return result;
                }

                // Get row count
                string[] rows = data.Split('\n');
                rowCount = rows.LongLength;
                if (rowCount == 0)
                {
                    return result;
                }
                // Get column count
                string[] columnsFirst = rows[0].Split('\t');
                columnCount = columnsFirst.LongLength;
                if (rowCount == 0)
                {
                    return result;
                }

                // Convert data to an array
                dataArray = new Object[rowCount, columnCount];
                if (dataArray != null)
                {
                    for (int i = 0; i < rowCount; i++)
                    {
                        rows[i] = rows[i].TrimEnd('\r');

                        string[] columns = rows[i].Split('\t');

                        for (int j = 0; j < columnCount; j++)
                        {
                            dataArray[i, j] = columns[j];
                        }
                    }
                }

                // Set result
                result = new ArrayData(rowCount, columnCount, dataArray, excelPosition, tableName);
            
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }


        /// <summary>
        /// Convert xml data to an array data for Excel import and retrieve some
        /// other node value
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="tableNum"></param>
        /// <returns></returns>
        private ArrayData GetArrayData(XmlDocument xmlDoc, int tableNum)
        {

            ArrayData result = null;

            try
            {
                object[,] dataArray = null;
                string data = String.Empty;
                string excelPosition = String.Empty;
                string tableName = String.Empty;
                long rowCount = 0;
                long columnCount = 0;

                // Check input parameters  
                if (xmlDoc == null)
                {
                    throw new Exception("Hiba l�pett fel az xml adat bet�lt�se sor�n.");
                }

                // Skip application defined root node
                XmlNode rootNode = null;
                XmlNodeList nodeList = xmlDoc.ChildNodes;
                if (nodeList.Count == 1)
                {
                    rootNode = nodeList.Item(0);
                }
                else
                {
                    rootNode = nodeList.Item(1);
                }

                // Build xpathes
                StringBuilder xPath = new StringBuilder();
                xPath.Append("BODY/TABLES/TABLE");
                xPath.Append(Convert.ToString(tableNum).PadLeft(3, '0'));

                StringBuilder xPathExcelPosition = new StringBuilder();
                xPathExcelPosition.Append(xPath.ToString());
                xPathExcelPosition.Append("/EXCELPOSITION");

                StringBuilder xPathTableName = new StringBuilder();
                xPathTableName.Append(xPath.ToString());
                xPathTableName.Append("/NAME");

                xPath.Append("/ARRAYDATA");

                // Get excel position node value
                XmlNode node = rootNode.SelectSingleNode(xPathExcelPosition.ToString());
                if (node == null)
                {
                    return result;
                }
                excelPosition = node.InnerText;

                // Get table name node value
                node = rootNode.SelectSingleNode(xPathTableName.ToString());
                if (node == null)
                {
                    return result;
                }
                tableName = node.InnerText;

                // Get innertext of arraydata node by xpath
                node = rootNode.SelectSingleNode(xPath.ToString());
                if (node == null)
                {
                    return result;
                }
                data = node.InnerText;
                data = data.Trim();
                if (data.Length == 0)
                {
                    return result;
                }

                // Get row count
                string[] rows = data.Split('\n');
                rowCount = rows.LongLength;
                if (rowCount == 0)
                {
                    return result;
                }
                // Get column count
                string[] columnsFirst = rows[0].Split('\t');
                columnCount = columnsFirst.LongLength;
                if (rowCount == 0)
                {
                    return result;
                }

                // Convert data to an array
                dataArray = new Object[rowCount, columnCount];
                if (dataArray != null)
                {
                    for (int i = 0; i < rowCount; i++)
                    {
                        rows[i] = rows[i].TrimEnd('\r');

                        string[] columns = rows[i].Split('\t');

                        for (int j = 0; j < columnCount; j++)
                        {
                            dataArray[i, j] = columns[j];
                        }
                    }
                }

                // Set result
                result = new ArrayData(rowCount, columnCount, dataArray, excelPosition, tableName);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        /// <summary>
        /// Convert a file to a Pdf
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>Pdf file path</returns>
        private string ConvertToPdf(string filePath)
        {
            string filePathOut = String.Empty;

            docConverter.docConverterClassClass converter = new docConverter.docConverterClassClass();

            try
            {
                int result = 0;

                // Convert file
                converter.DocumentOutputFormat = "PDF";
                converter.DocumentOutputFolder = Path.GetDirectoryName(filePath);
                converter.PDFAutoRotatePage = "All";
                // TODO: set parameters from configuration or database
                converter.DocumentResolution = 1200;
                converter.TimeOut = -1;
                converter.PDFCompressPages = true;
                converter.Watermark = false;

                result = converter.SubmitFile(filePath, "");
                if (result != 0)
                {
                    throw new Exception("Sikertelen konverzi�, Neevia hibak�d: " + result.ToString());
                }

                // Check the converter that ready to work
                if (Convert.ToBoolean(converter.isConverterActive()) == false)
                {
                    throw new Exception("A Pdf konverter nem akt�v!");
                }

                // Get converted file path
                filePathOut = Path.Combine(Path.GetDirectoryName(filePath), Path.GetFileNameWithoutExtension(filePath) + ".pdf");

                // Waiting for completion
                int status = converter.CheckStatus(filePathOut, "");
                while (status == 1 || status == 2)
                {
                    // Reduce the CPU usage
                    converter.DoSleep(100);
                    status = converter.CheckStatus(filePathOut, "");

                    // Check the converter that ready to work
                    if (Convert.ToBoolean(converter.isConverterActive()) == false)
                    {
                        throw new Exception("A Pdf konverter nem akt�v!");
                    }
                }

                // Logoff with technical user, revert to self
                converter.RevertToSelf();

            }
            catch (Exception ex)
            {
                throw new Exception("Hiba l�pett fel az Excel-Pdf konverzi� sor�n.", ex);
            }

            return filePathOut;
        }

        #endregion

        #region Methods - Word related

        /*private static Thread[] threads;
        public static string[] ThreadStates;
        public delegate void ThreadStateChangeDelegate(int ThreadNumber, string NewState);
        public static event ThreadStateChangeDelegate ThreadStateChange;

        public byte[] GetWordDocument_Thread(string template,
          Result ds_result,
          bool convertToPdf,
          int prior)
        {
            if (a)
            {
                int ThreadNumber = Convert.ToInt32(Contentum.eUtility.UI.GetAppSetting("NumberOFThreads"));
                string s = Contentum.eUtility.UI.GetAppSetting("ThreadPriorities");
                int ThreadSleep = Convert.ToInt32(Contentum.eUtility.UI.GetAppSetting("ThreadSleep"));

                threads = new Thread[ThreadNumber];
                ThreadStates = new string[ThreadNumber];

                int[] minPrior = new int[s.Split(';').Length];
                int x = 0;

                foreach (string splitted in s.Split(';'))
                {
                    if (!string.IsNullOrEmpty(splitted))
                        minPrior[x] = int.Parse(splitted);
                    x++;
                }

                for (int i = 0; i < ThreadNumber; i++)
                {
                    threads[i] = new Thread(ThreadStarter);
                    
                    ThreadParam tp = new ThreadParam(minPrior[i], ThreadSleep, template, ds_result, convertToPdf);
                    tp.ThreadNumber = i;
                    threads[i].Start(tp);
                    ThreadStates[i] = eTemplateManager.WebService.ThreadStates.Started;
                    ThreadStateChange(i, eTemplateManager.WebService.ThreadStates.Started);
                }
            }
        }

        private static void ThreadStarter(object ThreadParam)
        {
            if (!Type.Equals(ThreadParam.GetType(), new eTemplateManager.WebService.ThreadParam().GetType()))
                return;

            eTemplateManager.WebService.ThreadParam tp = (ThreadParam)ThreadParam;

            DocumentHandler docHandler = new DocumentHandler();
            docHandler.GetWordDocument_DataSet(tp.template, tp.ds_result, tp.convertToPdf);
        }*/

        public byte[] GetWordDocument_DataSet(string template,
          Result ds_result,
          bool convertToPdf)
        {
            byte[] result = null;
            string filePath = String.Empty;
            string filePathOut = String.Empty;

            try
            {
                WordDocument wordtemplate = null;

                wordtemplate = new WordDocument();

                //String Path = "c:\\temp\\ContentumNet";
                //if (!System.IO.Directory.Exists(Path))
                //{
                //    System.IO.Directory.CreateDirectory(Path);
                //}
                //Random r = new Random();
                //String FileName = r.Next().ToString() + ".doc";
                filePath = GetTemporaryFileNameWithPath(".doc");

                /*FileStream fs = new FileStream(filePath, FileMode.Create);
                StreamWriter sw = new StreamWriter(fs);
                sw.Write(template);
                sw.Close();*/

                XmlDocument xmlDoc_template = null;
                xmlDoc_template = new XmlDocument();
                xmlDoc_template.LoadXml(template);

                // Wordml -> Xsl conversation
                string xsl = wordtemplate.CreateXslFromWordml(xmlDoc_template);

                //ds_result.Ds.WriteXml(filePath);

                /*FileInfo fi = new FileInfo(filePath);

                using (StreamReader sr = fi.OpenText())
                {

                    String xmlDataSet = sr.ReadToEnd();
                    sr.Close();
                }*/
                
                /*XmlDocument xmlDoc = null;
                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(ds_result.Ds.GetXml());*/

                // Xsl transformation (overwrite template file with a new one)
                wordtemplate.XslTransformation(xsl, ds_result, filePath);


                // Wordml -> Doc conversation
                //filePath = wordtemplate.ConvertWordmlToDoc(filePath, true);

                // Convert the file to a pdf
                if (convertToPdf)
                {
                    filePathOut = ConvertToPdf(filePath);
                }
                else
                {
                    filePathOut = filePath;
                }

                // Get binary result from file
                result = GetFile(filePathOut);
                // Delete temporary files
                try
                {
                    File.Delete(filePath);
                    File.Delete(filePathOut);
                }
                catch
                {
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Hiba l�pett fel a dokumentum el��ll�t�sa sor�n. ", ex);
            }
            return result;
        }

        public byte[] GetWordDocument_ForXml(string template,
          Result ds_result,
          bool convertToPdf)
        {
            byte[] result = null;
            string filePath = String.Empty;
            string filePathOut = String.Empty;

            try
            {
                WordDocument wordtemplate = null;

                wordtemplate = new WordDocument();

                //String Path = "c:\\temp\\ContentumNet";
                //if (!System.IO.Directory.Exists(Path))
                //{
                //    System.IO.Directory.CreateDirectory(Path);
                //}
                //Random r = new Random();
                //String FileName = r.Next().ToString() + ".doc";

                filePath = DocumentHandler.GetTemporaryFileNameWithPath(".doc");
                /*FileStream fs = new FileStream(filePath, FileMode.Create);
                StreamWriter sw = new StreamWriter(fs);
                sw.Write(template);
                sw.Close();*/

                XmlDocument xmlDoc_template = null;
                xmlDoc_template = new XmlDocument();
                xmlDoc_template.LoadXml(template);

                // Wordml -> Xsl conversation
                string xsl = wordtemplate.CreateXslFromWordml(xmlDoc_template);

                //ds_result.Ds.WriteXml(filePath);

                /*FileInfo fi = new FileInfo(filePath);

                using (StreamReader sr = fi.OpenText())
                {

                    String xmlDataSet = sr.ReadToEnd();
                    sr.Close();
                }*/

                /*XmlDocument xmlDoc = null;
                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(ds_result.Ds.GetXml());*/

                // Xsl transformation (overwrite template file with a new one)
                wordtemplate.XslTransformation_ForXml(xsl, ds_result, filePath);


                // Wordml -> Doc conversation
                //filePath = wordtemplate.ConvertWordmlToDoc(filePath, true);

                // Convert the file to a pdf
                if (convertToPdf)
                {
                    filePathOut = ConvertToPdf(filePath);
                }
                else
                {
                    filePathOut = filePath;
                }

                // Get binary result from file
                result = GetFile(filePathOut);
                // Delete temporary files
                try
                {
                    File.Delete(filePath);
                    File.Delete(filePathOut);
                }
                catch
                {
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Hiba l�pett fel a dokumentum el��ll�t�sa sor�n. ", ex);
            }
            return result;
        }

        public byte[] GetWordDocument(string template,
          string xml,
          bool convertToPdf)
        {
            byte[] result = null;
            string filePath = String.Empty;
            string filePathOut = String.Empty;

            try
            {
                WordDocument wordtemplate = null;
                //xml = GetXml(xml);

                wordtemplate = new WordDocument();

                //String Path = "c:\\temp\\ContentumNet";
                //if (!System.IO.Directory.Exists(Path))
                //{
                //    System.IO.Directory.CreateDirectory(Path);
                //}
                //Random r = new Random();
                //String FileName = r.Next().ToString() + ".xml";

                filePath = DocumentHandler.GetTemporaryFileNameWithPath(".doc"); // .xml helyett .doc
                /*FileStream fs = new FileStream(filePath, FileMode.Create);
                StreamWriter sw = new StreamWriter(fs);
                sw.Write(template);
                sw.Close();*/

                XmlDocument xmlDoc_template = null;
                xmlDoc_template = new XmlDocument();
                xmlDoc_template.LoadXml(template);

                // Wordml -> Xsl conversation
                string xsl = wordtemplate.CreateXslFromWordml(xmlDoc_template);
                
                /*XmlDocument xmlDoc = null;
                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(TrimStartXmlString(xml));*/

                // Xsl transformation (overwrite template file with a new one)
                wordtemplate.XslTransformation_string(xsl, xml, filePath);


                // Wordml -> Doc conversation
                //filePath = wordtemplate.ConvertWordmlToDoc(filePath, true);

                // Convert the file to a pdf
                if (convertToPdf)
                {
                    filePathOut = ConvertToPdf(filePath);
                }
                else
                {
                    filePathOut = filePath;
                }

                // Get binary result from file
                result = GetFile(filePathOut);
                // Delete temporary files
                try
                {
                    File.Delete(filePath);
                    File.Delete(filePathOut);
                }
                catch
                {
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Hiba l�pett fel a dokumentum el��ll�t�sa sor�n. ", ex);
            }
            return result;
        }

        #endregion Methods - Word related - (Not implemented)

        #region Utility classes

        /// <summary>
        /// Class for storing arraydata from xml data
        /// </summary>
        class ArrayData
        {
            #region Members

            /// <summary>
            /// Row count member
            /// </summary>
            long m_row = 0;

            /// <summary>
            /// Column count member
            /// </summary>
            long m_column = 0;

            /// <summary>
            /// Object array member
            /// </summary>
            object[,] m_dataArray = null;

            /// <summary>
            /// Excel position
            /// </summary>
            string m_excelPosition = String.Empty;

            /// <summary>
            /// Table name
            /// </summary>
            string m_tableName = String.Empty;

            #endregion Members

            #region Properties

            /// <summary>
            /// Row count
            /// </summary>
            public long RowCount
            {
                get
                {
                    return m_row;
                }
            }

            /// <summary>
            /// Column count
            /// </summary>
            public long ColumnCount
            {
                get
                {
                    return m_column;
                }
            }

            /// <summary>
            /// Object array
            /// </summary>
            public object[,] Data
            {
                get
                {
                    return m_dataArray;
                }
            }

            /// <summary>
            /// Excel position
            /// </summary>
            public string ExcelPosition
            {
                get
                {
                    return m_excelPosition;
                }
            }

            /// <summary>
            /// Table name
            /// </summary>
            public string TableName
            {
                get
                {
                    return m_tableName;
                }
            }

            #endregion Properties

            #region Constructors

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="row">row count</param>
            /// <param name="column">column count</param>
            /// <param name="dataArray">object array</param>
            /// <param name="excelPosition">excel worksheet cella position</param>
            /// <param name="tableName">table name, number from the sheet name</param>
            public ArrayData(long row, long column, object[,] dataArray, string excelPosition, string tableName)
            {
                m_row = row;
                m_column = column;
                m_dataArray = dataArray;
                m_excelPosition = excelPosition;
                m_tableName = tableName;
            }

            #endregion Constructors
        }

        class ExcelUILanguageHelper : IDisposable
        {
            private CultureInfo m_CurrentCulture;
            public ExcelUILanguageHelper()
            {
                // save current culture and set culture to en-US
                m_CurrentCulture = Thread.CurrentThread.CurrentCulture;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            }
            #region IDisposable Members 
            public void Dispose()
            {
                // return to normal culture
                Thread.CurrentThread.CurrentCulture = m_CurrentCulture;
            }
            #endregion
        }

        #endregion Utility class
    }
}
