//====================================================================
// This file is generated as part of Web project conversion.
// The extra class 'MenuItem' in the code behind file in 'webcontrols\Header.ascx.cs' is moved to this file.
//====================================================================




namespace Contentum.eTemplateManager.WebService.WebControls
 {

	using System;
	using System.Data;
	using System.Drawing;
  using System.Collections;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

  public class MenuItem 
  {
    private string m_id;
    private string m_caption;
    private string m_link;
 
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="id"></param>
    /// <param name="caption"></param>
    public MenuItem(string id, string caption, string link) 
    {
      this.m_id = id;
      this.m_caption = caption;
      this.m_link = link;
    }
 
    /// <summary>
    /// Menu item identity
    /// </summary>
    public string ID 
    {
      get 
      {
        return m_id;
      }
    }
 
    /// <summary>
    /// Menu item caption
    /// </summary>
    public string Caption
    {
      get 
      {
        return this.m_caption;
      }
    }

    /// <summary>
    /// Menu item link
    /// </summary>
    public string Link
    {
      get 
      {
        return this.m_link;
      }
    }
    
  }

}