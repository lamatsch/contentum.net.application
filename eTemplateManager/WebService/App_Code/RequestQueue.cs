﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Threading;

using Contentum.eBusinessDocuments;
using System.Reflection;

namespace Contentum.eTemplateManager.WebService
{
    public class RequestQueue
    {
        private static List<RequestItem> queue = new List<RequestItem>();

        public static void AddItemToQueue(RequestItem item)
        {
            queue.Add(item);
        }

        public static void ProcessNextItem(int ThreadNumber)
        {
            RequestItem nextItem = RequestQueue.GetNextItem(ThreadManager.MinPriors[ThreadNumber]);

            if (nextItem == null || ThreadManager.threadStates[ThreadNumber] != ThreadStates.Free)
                return;

            ThreadManager.threadStates[ThreadNumber] = ThreadStates.Started;
            nextItem.State = RequestItemStates.InProgress;

            if (nextItem.ForXml)
                ThreadManager.Threads[ThreadNumber] = new Thread(ThreadManager.Threaded_GetWordDocument_ForXml);
            else
                ThreadManager.Threads[ThreadNumber] = new Thread(ThreadManager.Threaded_GetWordDocument_DataSet);

            Result returnResult = new Result();
            GetWordDocumentThreadParam tpResult = new GetWordDocumentThreadParam(returnResult, nextItem.template, nextItem.ds_result, nextItem.convertToPdf, nextItem.name);

            ThreadManager.Threads[ThreadNumber].Start(tpResult);

            // megvárjuk ameddig lefut a szál
            ThreadManager.Threads[ThreadNumber].Join();
            queue.Remove(nextItem);

            ThreadManager.FreeThread(ThreadNumber);

            // meghívjuk a cél webmethodot
            ThreadManager.CallBack(tpResult.returnResult, nextItem.name);
        }

        public static RequestItem GetNextItem(int minPrior)
        {
            foreach (RequestItem item in queue)
            {
                if (item.State == RequestItemStates.Waiting && item.Priority >= minPrior)
                {
                    return item;
                }
            }

            return null;
        }
    }

    public class RequestItem
    {
        public string name;
        public string userId;

        public string template;
        public Result ds_result;
        public bool convertToPdf;
        public int Priority;

        public RequestItemStates State;
        public bool ForXml;


        public RequestItem()
        {
        }

        public RequestItem(string name, string userId, string template, Result ds_result, bool convertToPdf, int Priority, bool ForXml)
        {
            this.name = name;
            this.userId = userId;
            this.template = template;
            this.ds_result = ds_result;
            this.convertToPdf = convertToPdf;
            this.Priority = Priority;

            this.State = RequestItemStates.Waiting;
            this.ForXml = ForXml;
        }
    }

    public enum RequestItemStates
    {
        Waiting,
        InProgress
    }
}