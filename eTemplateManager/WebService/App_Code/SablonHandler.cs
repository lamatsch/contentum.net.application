﻿using Contentum.eBusinessDocuments;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

/// <summary>
/// Summary description for SablonHandler
/// </summary>
public class SablonHandler
{
    public SablonHandler()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public byte[] DokumentumGeneralas_IratSablon(byte[] sablon, DataSet iratAdatok)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            ms.Write(sablon, 0, (int)sablon.Length);
            using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open(ms, true))
            {

                IDictionary<String, BookmarkStart> bookmarkMap = new Dictionary<String, BookmarkStart>();

                foreach (BookmarkStart bookmarkStart in wordprocessingDocument.MainDocumentPart.RootElement.Descendants<BookmarkStart>())
                {
                    bookmarkMap[bookmarkStart.Name] = bookmarkStart;
                }

                foreach (DataRow row in iratAdatok.Tables[0].Rows)
                {
                    string kod = row["KOD"].ToString();
                    string ertek = row["ERTEK"].ToString();

                    if (bookmarkMap.ContainsKey(kod))
                    {
                        InsertIntoBookmark(bookmarkMap[kod], ertek);
                    }
                }
            }

            return ms.ToArray();
        }

    }

    void InsertIntoBookmark(BookmarkStart bookmarkStart, string text)
    {
        OpenXmlElement elem = bookmarkStart.NextSibling();

        while (elem != null && !(elem is BookmarkEnd))
        {
            OpenXmlElement nextElem = elem.NextSibling();
            elem.Remove();
            elem = nextElem;
        }

        bookmarkStart.Parent.InsertAfter<Run>(new Run(new Text(text)), bookmarkStart);
    }
}