// ====================================================================================
//
// TemplateManager.asmx.cs
//
//  This class implements TemplateManager webservice
//
// ====================================================================================
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.IO;
using System.Threading;
using Contentum.eTemplateManager.Service;
using Contentum.eBusinessDocuments;

namespace Contentum.eTemplateManager.WebService
{
    /// <summary>
    /// This class implements webservice for getting reports based on word
    /// and excel template. It filles in the templates from xml string.
    /// </summary>
    [WebService(Namespace = "Contentum.eTemplateManager.WebService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class TemplateManagerService : System.Web.Services.WebService// ITemplateManagerFacade
    {
        #region Constructors

        /// <summary>
        /// Default constructor of the Template service class
        /// </summary>
        public TemplateManagerService()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Component Designer generated code

        //Required by the Web Services Designer 
        private IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion

        #region Web methods

        /// <summary>
        /// Word dokumentum k�sz�t�se
		///  A be�rkezett sablont(template) felt�lti a kapott xml-ben l�v� adatokkal, majd pdf form�tumra konvert�lja, ha sz�ks�ges. Az k�sz word dokumentumot a Result.Record-ban adja vissza byte t�mbk�nt.
        /// </summary>
        /// <param name="template">XML form�tum� Word  template</param>
        /// <param name="xml">XML -ben l�v� adathalmaz</param>
        /// <param name="converToPdf"></param>
        /// <returns></returns>
        [WebMethod(Description = "A be�rkezett sablont(template) felt�lti a kapott xml-ben l�v� adatokkal, majd pdf form�tumra konvert�lja, ha sz�ks�ges. Az k�sz word dokumentumot a Result.Record-ban adja vissza byte t�mbk�nt.")]
        public Result GetWordDocument(string template,
          string xml,
          bool convertToPdf)
        {
            Result res = new Result();

            byte[] result = null;

            try
            {
                DocumentHandler docHandler = new DocumentHandler();
                result = docHandler.GetWordDocument(template, xml, convertToPdf);
            }
            catch (Exception ex)
            {
                //ExceptionManager.Publish(ex);
                //TraceCallReturnEvent.Raise(false);
                //throw new Exception("Hiba l�pett fel a dokumentum feldolgoz�sa sor�n. " + ex.Message, ex);
                res.ErrorCode = "200002";
                res.ErrorMessage = ex.Message;
            }
            res.Record = result;
            return res;
        }

        /// <summary>
        /// Word dokumentum k�sz�t�se
		/// A be�rkezett sablont(template) felt�lti a kapott Result (ds_result) DataSet-j�ben l�v� adatokkal, majd pdf form�tumra konvert�lja, ha sz�ks�ges. Az k�sz word dokumentumot a Result.Record-ban adja vissza byte t�mbk�nt.
        /// </summary>
        /// <param name="template">XML form�tum� Word  template</param>
        /// <param name="ds_result">rekordhalmaz Microsoft DataSet -ben</param>
        /// <param name="converToPdf"></param>
        /// <returns></returns>
        [WebMethod(Description = "A be�rkezett sablont(template) felt�lti a kapott Result (ds_result) DataSet-j�ben l�v� adatokkal, majd pdf form�tumra konvert�lja, ha sz�ks�ges. Az k�sz word dokumentumot a Result.Record-ban adja vissza byte t�mbk�nt.")]
        public Result GetWordDocument_DataSet(string template,
          Result ds_result,
          bool convertToPdf)
        {
            Result res = new Result();

            byte[] result = null;

            try
            {
                DocumentHandler docHandler = new DocumentHandler();
                result = docHandler.GetWordDocument_DataSet(template, ds_result, convertToPdf);
            }
            catch (Exception ex)
            {
                //ExceptionManager.Publish(ex);
                //TraceCallReturnEvent.Raise(false);
                //throw new Exception("Hiba l�pett fel a dokumentum feldolgoz�sa sor�n. " + ex.Message, ex);
                res.ErrorCode = "200002";
                res.ErrorMessage = ex.Message;
            }
            res.Record = result;
            return res;
        }

        /// <summary>
        /// Word dokumentum k�sz�t�se
		/// A be�rkezett sablont(template) felt�lti a kapott Result (ds_result) DataSet-j�ben l�v� adatokkal, majd pdf form�tumra konvert�lja, ha sz�ks�ges. A dokumentum k�sz�t�s�t t�bb sz�l v�gzi, a prior az adott k�r�s priorit�sa, a name az elk�sz�lt dokumentum neve, a timeout pedig az az id�tartam, ami ut�n a gener�l�s a h�tt�rben fut. Az k�sz word dokumentumot a Result.Record-ban adja vissza byte t�mbk�nt.
        /// </summary>
        /// <param name="template">XML form�tum� Word  template</param>
        /// <param name="ds_result">rekordhalmaz Microsoft DataSet -ben</param>
        /// <param name="converToPdf"></param>
        /// <param name="prior">Fut�si priorit�s</param>
        /// <param name="name"></param>
        /// <param name="timeout">Mennyi id�� m�lva fusson a h�tt�rben</param>
        /// <returns></returns>
        [WebMethod(Description = "A be�rkezett sablont(template) felt�lti a kapott Result (ds_result) DataSet-j�ben l�v� adatokkal, majd pdf form�tumra konvert�lja, ha sz�ks�ges. A dokumentum k�sz�t�s�t t�bb sz�l v�gzi, a prior az adott k�r�s priorit�sa, a name az elk�sz�lt dokumentum neve, a timeout pedig az az id�tartam, ami ut�n a gener�l�s a h�tt�rben fut. Az k�sz word dokumentumot a Result.Record-ban adja vissza byte t�mbk�nt.")]
        public Result GetWordDocument_DataSet_Thread(string template,
          Result ds_result,
          bool convertToPdf, 
          int prior,
          string name,
          int timeout)
        {
            Result res = new Result();

            //byte[] result = null;

            try
            {
                res = ThreadManager.GetWordDocument_DataSet(template, ds_result, convertToPdf, prior, name, timeout);
            }
            catch (Exception ex)
            {
                //ExceptionManager.Publish(ex);
                //TraceCallReturnEvent.Raise(false);
                //throw new Exception("Hiba l�pett fel a dokumentum feldolgoz�sa sor�n. " + ex.Message, ex);
                res.ErrorCode = "200002";
                res.ErrorMessage = ex.Message;
            }
            //res.Record = result;
            return res;
        }

        /// <summary>
        /// Word dokumentum k�sz�t�se
		/// A be�rkezett sablont(template) felt�lti a kapott Result (ds_result) DataSet-j�nek 0. t�bl�j�nak 0. sor�nak �string_xml� oszlop�ban l�v� adatokkal, majd pdf form�tumra konvert�lja, ha sz�ks�ges. A dokumentum k�sz�t�s�t t�bb sz�l v�gzi, a prior az adott k�r�s priorit�sa, a name az elk�sz�lt dokumentum neve, a timeout pedig az az id�tartam, ami ut�n a gener�l�s a h�tt�rben fut. Az k�sz word dokumentumot a Result.Record-ban adja vissza byte t�mbk�nt.
        /// </summary>
        /// <param name="template">XML form�tum� Word  template</param>
        /// <param name="ds_result">rekordhalmaz Microsoft DataSet -ben</param>
        /// <param name="converToPdf"></param>
        /// <param name="prior">a feladat priorit�sa </param>
        /// <param name="name"></param>
        /// <param name="timeout">Mennyi id� m�lva fusson a h�tt�rben</param>
        /// <returns></returns>
        [WebMethod(Description = "A be�rkezett sablont(template) felt�lti a kapott Result (ds_result) DataSet-j�nek 0. t�bl�j�nak 0. sor�nak �string_xml� oszlop�ban l�v� adatokkal, majd pdf form�tumra konvert�lja, ha sz�ks�ges. A dokumentum k�sz�t�s�t t�bb sz�l v�gzi, a prior az adott k�r�s priorit�sa, a name az elk�sz�lt dokumentum neve, a timeout pedig az az id�tartam, ami ut�n a gener�l�s a h�tt�rben fut. Az k�sz word dokumentumot a Result.Record-ban adja vissza byte t�mbk�nt.")]
        public Result GetWordDocument_ForXml_Thread(string template,
          Result ds_result,
          bool convertToPdf,
          int prior,
          string name,
          int timeout)
        {
            Result res = new Result();

            //byte[] result = null;

            try
            {
                res = ThreadManager.GetWordDocument_ForXml(template, ds_result, convertToPdf, prior, name, timeout);
            }
            catch (Exception ex)
            {
                //ExceptionManager.Publish(ex);
                //TraceCallReturnEvent.Raise(false);
                //throw new Exception("Hiba l�pett fel a dokumentum feldolgoz�sa sor�n. " + ex.Message, ex);
                res.ErrorCode = "200002";
                res.ErrorMessage = ex.Message;
            }
            //res.Record = result;
            return res;
        }

        /// <summary>
        /// Word dokumentum k�sz�t�se
		/// A be�rkezett sablont(template) felt�lti a kapott Result (ds_result) DataSet-j�nek 0. t�bl�j�nak 0. sor�nak �string_xml� oszlop�ban l�v� adatokkal, majd pdf form�tumra konvert�lja, ha sz�ks�ges. Az k�sz word dokumentumot a Result.Record-ban adja vissza byte t�mbk�nt.
        /// </summary>
        /// <param name="template">XML form�tum� Word  template</param>
        /// <param name="ds_result">rekordhalmaz Microsoft DataSet -ben</param>
        /// <param name="converToPdf"></param>
        /// <returns></returns>
        [WebMethod(Description = "A be�rkezett sablont(template) felt�lti a kapott Result (ds_result) DataSet-j�nek 0. t�bl�j�nak 0. sor�nak �string_xml� oszlop�ban l�v� adatokkal, majd pdf form�tumra konvert�lja, ha sz�ks�ges. Az k�sz word dokumentumot a Result.Record-ban adja vissza byte t�mbk�nt.")]
        public Result GetWordDocument_ForXml(string template,
          Result ds_result,
          bool convertToPdf)
        {
            Result res = new Result();

            byte[] result = null;

            try
            {
                DocumentHandler docHandler = new DocumentHandler();
                result = docHandler.GetWordDocument_ForXml(template, ds_result, convertToPdf);
            }
            catch (Exception ex)
            {
                //ExceptionManager.Publish(ex);
                //TraceCallReturnEvent.Raise(false);
                //throw new Exception("Hiba l�pett fel a dokumentum feldolgoz�sa sor�n. " + ex.Message, ex);
                res.ErrorCode = "200002";
                res.ErrorMessage = ex.Message;
            }
            res.Record = result;
            return res;
        }

        /// <summary>
        /// Excel dokumentum k�sz�t�se
		/// A be�rkezett sablont(template) felt�lti a kapott xml-ben l�v� adatokkal, majd pdf form�tumra konvert�lja, ha sz�ks�ges. Az k�sz excel dokumentumot a Result.Record-ban adja vissza byte t�mbk�nt.
        /// </summary>
        /// <param name="template">form�tum� Excel template</param>
        /// <param name="xml"> adathalmaz XML -ben</param>
        /// <param name="converToPdf"></param>
        /// <returns></returns>
        [WebMethod(Description = "A be�rkezett sablont(template) felt�lti a kapott xml-ben l�v� adatokkal, majd pdf form�tumra konvert�lja, ha sz�ks�ges. Az k�sz excel dokumentumot a Result.Record-ban adja vissza byte t�mbk�nt.")]
        public Result GetExcelDocument(byte[] template,
          string xml,
          bool convertToPdf)
        {
            Result res = new Result();

            byte[] result = null;

            try
            {
                DocumentHandler docHandler = new DocumentHandler();
                result = docHandler.GetExcelDocument(template, xml, convertToPdf);
            }
            catch (Exception ex)
            {
                //ExceptionManager.Publish(ex);
                //TraceCallReturnEvent.Raise(false);
                //throw new Exception("Hiba l�pett fel a dokumentum feldolgoz�sa sor�n. " + ex.Message, ex);
                res.ErrorCode = "200001";
                res.ErrorMessage = ex.Message;
                //// tesztel�shez r�szletesebb hiba�zenet
                //Exception innerEx = ex.InnerException;
                //int cnt = 1;
                //while (innerEx != null)
                //{
                //    res.ErrorMessage += "<br />Bels� hiba " + ((Int16)cnt).ToString() + ": " + innerEx.GetType().ToString() + ": " + innerEx.Message;
                //    res.ErrorMessage += "<br />--------- StackTrace ----------<br />" + innerEx.StackTrace;
                //    res.ErrorMessage += "<br />--------- Source ---------->>> " + innerEx.Source;
                //    cnt++;
                //    innerEx = innerEx.InnerException;
                //}
                //res.ErrorMessage += "<br />---------  User  ---------->>> " + System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            }
            res.Record = result;
            return res;
        }

        /// <summary>
        /// Excel dokumentum k�sz�t�se
        /// </summary>
        /// <param name="template">A kit�ltend� Excel sablon bin�ris t�mbk�nt.</param>
        /// <param name="ds">A kit�lt�si adatokat tartalmaz� DataSet, bele�rtve a vez�rl� (munkalap �s cella kijel�l�) inform�ci�kat is.</param>
        /// <param name="targetCellsColumnName">A DataSetben l�v� t�bl�kban a vez�rl� inform�ci�kat tatalmaz� oszlop neve (minden t�bl�ban azonosnak kell lennie).
        /// A vez�rl� inform�ci�kat tartalmaz� cella tartalm�nak a k�vetkez� form�tumot kell k�vetnie:
        /// "<munkalap n�v><targetCellSeparators><cella n�v><targetCellSeparators><adatoszlop n�v>[[<targetSeparators><munkalap n�v><targetCellSeparators><cella n�v><targetCellSeparators><adatoszlop n�v><targetSeparators>]...]"
        /// </param>
        /// <param name="targetSeparators">Elv�laszt� karakterek t�mbje, melyek a vez�rl�si inform�ci�n bel�l az egyes cell�k kit�lt�s�re vonatkoz� inform�ci�kat tartalmaz� egys�geket (munkalap n�v + cella c�me + az adatot tartalmaz� oszlop neve) v�lasztja el egym�st�l.</param>
        /// <param name="targetCellSeparators">Elv�laszt� karakterek t�mbje, melyek a cellakit�lt�si inform�ci�k egys�gein bel�l az egyes elemeket (munkalap n�v, cella c�me, az adatot tartalmaz� oszlop neve) v�lasztj�k el egym�st�l.</param>
        /// <param name="convertToPdf">Flag, mely megmutatja, kell-e PDF-et gener�lni a kit�lt�tt dokumentumb�l.</param>
        /// <returns></returns>
        [WebMethod(Description = "A be�rkezett sablont(template) felt�lti a kapott DataSet-ben (ds) l�v� adatokkal, az ugyancsak a DataSetben �tadott vez�rl� inform�ci�knak (munkalap n�v + cella c�me + az adatot tartalmaz� oszlop neve egys�gek) megfelel�en. A ds t�bl�iban a vez�rl� inform�ci�kat tartalmaz� oszlop nev�t a targetCellsColumnName adja meg (minden t�bl�ban azonosnak kell lennie). A vez�rl� inform�ci� egys�geit elv�laszt� karaktereket a targetSeparators, az egyes egys�geken bel�l az elemeket elv�laszt� karaktereket a targetCellSeparators param�terben kell �tadni. A kit�lt�tt sablont pdf form�tumra konvert�lja, ha sz�ks�ges. Az k�sz excel dokumentumot a Result.Record-ban adja vissza byte t�mbk�nt.")]
        public Result GetExcelDocumentFromDataSet(byte[] template,
          DataSet ds,
          string targetCellsColumnName, char[] targetSeparators, char[] targetCellSeparators,
          bool convertToPdf)
        {
            Result res = new Result();

            byte[] result = null;

            try
            {
                DocumentHandler docHandler = new DocumentHandler();
                result = docHandler.GetExcelDocumentFromDataSet(template, ds, targetCellsColumnName, targetSeparators, targetCellSeparators, convertToPdf);
            }
            catch (Exception ex)
            {
                //ExceptionManager.Publish(ex);
                //TraceCallReturnEvent.Raise(false);
                //throw new Exception("Hiba l�pett fel a dokumentum feldolgoz�sa sor�n. " + ex.Message, ex);
                res.ErrorCode = "200001";
                res.ErrorMessage = ex.Message;
                //// tesztel�shez r�szletesebb hiba�zenet
                //Exception innerEx = ex.InnerException;
                //int cnt = 1;
                //while (innerEx != null)
                //{
                //    res.ErrorMessage += "<br />Bels� hiba " + ((Int16)cnt).ToString() + ": " + innerEx.GetType().ToString() + ": " + innerEx.Message;
                //    res.ErrorMessage += "<br />--------- StackTrace ----------<br />" + innerEx.StackTrace;
                //    res.ErrorMessage += "<br />--------- Source ---------->>> " + innerEx.Source;
                //    cnt++;
                //    innerEx = innerEx.InnerException;
                //}
                //res.ErrorMessage += "<br />---------  User  ---------->>> " + System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            }
            res.Record = result;
            return res;
        }

        [WebMethod()]
        public Result DokumentumGeneralas_IratSablon(byte[] sablon, DataSet iratAdatok)
        {
            Result res = new Result();

            byte[] result = null;

            try
            {
                SablonHandler sablonHandler = new SablonHandler();
                result = sablonHandler.DokumentumGeneralas_IratSablon(sablon, iratAdatok);
            }
            catch (Exception ex)
            {
                res = eUtility.ResultException.GetResultFromException(ex);
            }
            res.Record = result;
            return res;
        }

        #endregion Web methods
    }
}
