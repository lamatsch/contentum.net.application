﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Threading;
using Contentum.eBusinessDocuments;

namespace Contentum.eTemplateManager.WebService
{
    public class ThreadManager
    {
        private static int threadNumber = 0;
        public static Thread[] Threads;
        public static string[] threadStates;
        public static int[] MinPriors;

        public delegate void ThreadFinishedDelegate(int ThreadNumber);
        public static event ThreadFinishedDelegate ThreadFinished;


        #region eRecordból hívható methodok


        public static Result GetWordDocument_ForXml(string template, Result ds_result, bool convertToPdf, int Priority, string name, int TimeOut)
        {
            int tn = ThreadManager.GetFreeThread(Priority);
            if (tn == -1)
            {
                Result res = new Result();
                res.Record = null;
                res.ErrorCode = "99998";
                res.ErrorMessage = "NoFreeThread";

                RequestQueue.AddItemToQueue(new RequestItem(name, ds_result.Uid, template, ds_result, convertToPdf, Priority, false));
                return res;
            }

            threadStates[tn] = ThreadStates.Started;
            Threads[tn] = new Thread(Threaded_GetWordDocument_ForXml);

            Result returnResult = new Result();
            GetWordDocumentThreadParam tpResult = new GetWordDocumentThreadParam(returnResult, template, ds_result, convertToPdf, name);

            Threads[tn].Start(tpResult);


            // megvárjuk ameddig lefut vagy timeout-ol a szál
            if (Threads[tn].Join(TimeOut * 1000) == true)
            {
                // befejezte a feladatot

                FreeThread(tn);
                return tpResult.returnResult;
            }
            else
            {
                // timeout
                Result res = new Result();
                res.Record = null;
                res.ErrorCode = "99999";
                res.ErrorMessage = "TimeoutExpired";

                tpResult.thread = Threads[tn];
                Thread t = new Thread(LongThreadHandler);
                t.Start(tpResult);

                return res;
            }
        }

        public static void Threaded_GetWordDocument_ForXml(object param)
        {
            GetWordDocumentThreadParam tp = (GetWordDocumentThreadParam)param;
            try
            {
                DocumentHandler docHandler = new DocumentHandler();
                byte[] result = docHandler.GetWordDocument_ForXml(tp.template, tp.ds_result, tp.convertToPdf);
                tp.returnResult = new Result();
                tp.returnResult.Record = result;
                tp.returnResult.Uid = tp.ds_result.Uid;
            }
            catch (Exception ex)
            {
                tp.returnResult.ErrorCode = "200013";
                tp.returnResult.ErrorMessage = ex.Message;
            }
        }

        public static Result GetWordDocument_DataSet(string template, Result ds_result, bool convertToPdf, int Priority, string name, int TimeOut)
        {
            int tn = ThreadManager.GetFreeThread(Priority);
            if (tn == -1)
            {
                Result res = new Result();
                res.Record = null;
                res.ErrorCode = "99998";
                res.ErrorMessage = "NoFreeThread";

                RequestQueue.AddItemToQueue(new RequestItem(name,ds_result.Uid,template,ds_result,convertToPdf,Priority,false));
                return res;
            }

            threadStates[tn] = ThreadStates.Started;
            Threads[tn] = new Thread(Threaded_GetWordDocument_DataSet);

            Result returnResult = new Result();
            GetWordDocumentThreadParam tpResult = new GetWordDocumentThreadParam(returnResult, template, ds_result, convertToPdf, name);

            Threads[tn].Start(tpResult);
            

            // megvárjuk ameddig lefut vagy timeout-ol a szál
            if (Threads[tn].Join(TimeOut * 1000) == true)
            {
                // befejezte a feladatot

                FreeThread(tn);
                return tpResult.returnResult;
            }
            else
            {
                // timeout
                Result res = new Result();
                res.Record = null;
                res.ErrorCode = "99999";
                res.ErrorMessage = "TimeoutExpired";

                tpResult.thread = Threads[tn];
                Thread t = new Thread(LongThreadHandler);
                t.Start(tpResult);

                return res;
            }
        }

        public static void Threaded_GetWordDocument_DataSet(object param)
        {
            GetWordDocumentThreadParam tp = (GetWordDocumentThreadParam)param;

            try
            {
                DocumentHandler docHandler = new DocumentHandler();
                byte[] result = docHandler.GetWordDocument_DataSet(tp.template, tp.ds_result, tp.convertToPdf);
                tp.returnResult = new Result();
                tp.returnResult.Record = result;
                tp.returnResult.Uid = tp.ds_result.Uid;
            }
            catch (Exception ex)
            {
                tp.returnResult.ErrorCode = "200013";
                tp.returnResult.ErrorMessage = ex.Message;
            }
        }

        private static void LongThreadHandler(object threadedParam)
        {
            GetWordDocumentThreadParam tp = (GetWordDocumentThreadParam)threadedParam;
            tp.thread.Join();

            for (int i = 0; i < threadNumber; i++)
            {
                if (tp.thread.IsAlive && Threads[i].Equals(tp.thread))
                {
                    FreeThread(i);
                }
            }

            CallBack(tp.returnResult, tp.FileName);
        }


        #endregion eRecordból hívható methodok


        public static void CallBack(Result result, string fileName)
        {
            eTMWebService.eTemplateManagerService etms = new eTMWebService.eTemplateManagerService();
            etms.Credentials = System.Net.CredentialCache.DefaultCredentials;
            etms.UseDefaultCredentials = true;

            eTMWebService.Result res = new eTMWebService.Result();
            res.Ds = result.Ds;
            res.ErrorCode = result.ErrorCode;
            res.ErrorMessage = result.ErrorMessage;
            res.ErrorType = result.ErrorType;
            res.IsLogExist = result.IsLogExist;
            res.Record = result.Record;
            res.Uid = result.Uid;

            int ret = etms.CallBack(res, fileName);
        }

        public static int GetFreeThread(int Prior)
        {
            InitFields();

            for (int i = 0; i < threadNumber; i++)
            {
                if (threadStates[i] == ThreadStates.Free && MinPriors[i] >= Prior)
                    return i;
            }

            return -1;
        }

        public static void FreeThread(int ThreadNumber)
        {
            threadStates[ThreadNumber] = ThreadStates.Free;
            ThreadFinished(ThreadNumber);
        }

        private static void InitFields()
        {
            if (threadNumber == 0)
                threadNumber = Convert.ToInt32(Contentum.eUtility.UI.GetAppSetting("NumberOFThreads"));

            if (Threads == null)
            {
                Threads = new Thread[threadNumber];

                ThreadFinished += RequestQueue.ProcessNextItem;
            }

            if (threadStates == null)
            {
                threadStates = new string[threadNumber];

                for (int i = 0; i < threadNumber; i++)
                {
                    threadStates[i] = ThreadStates.Free;
                }
            }

            if (MinPriors == null)
            {
                MinPriors = new int[threadNumber];

                for (int i = 0; i < threadNumber; i++)
                {
                    MinPriors[i] = Convert.ToInt32(Contentum.eUtility.UI.GetAppSetting("MinPriors" + i));
                }

            }

        }
    }

    public static class ThreadStates
    {
        public static string Started = "Elindítva";
        public static string Stopped = "Megállítva";
        public static string Sleeping = "Várakozik";
        public static string Free = "Szabad";
    }

    public class GetWordDocumentThreadParam
    {
        public Result returnResult;
        public string template;
        public Result ds_result;
        public bool convertToPdf;
        public Thread thread;
        public string FileName;

        public GetWordDocumentThreadParam(Result returnResult, string template, Result ds_result, bool convertToPdf, string FileName)
        {
            this.returnResult = returnResult;
            this.template = template;
            this.ds_result = ds_result;
            this.convertToPdf = convertToPdf;
            this.FileName = FileName;
            
            this.thread = null;
        }
    }
}