// =====================================================================================
// WordDocument.cs
//
// This class handles Word documents for filling it from xml by xsl transformation.
// Tasks: wordml - xsl conversation,
//        xsl transformation,
//        wordml - doc / doc - wordml conversation
//
// =====================================================================================
using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Xml.Schema;
using Contentum.eBusinessDocuments;
// COM references
//using Word = Microsoft.Office.Interop.Word;

namespace Contentum.eTemplateManager.WebService
{
  /// <summary>
	/// This class handles Word documents for filling from xml
	/// </summary>
  public class WordDocument
  {
    #region Constants

    /// <summary>
    /// Wordml namespace URI
    /// </summary>
    private const string c_NamespaceUriWordml = "http://schemas.microsoft.com/office/word/2003/wordml";

    /// <summary>
    /// Xsl namespace URI
    /// </summary>
    private const string c_NamespaceUriXsl = "http://www.w3.org/1999/XSL/Transform";

    #endregion Constants

    #region Enums

    /// <summary>
    /// Types of custom xsd schema node for converting wordml to xsl
    /// </summary>
    public enum SchemaNodeType
    {
      /// <summary>Unknown type</summary>
      None,
      /// <summary>Text type for xsl:value-of mapping</summary>
      Text,
      /// <summary>Repeater type for xsl:for-each mapping</summary>
      Repeater,
      /// <summary>Image link type for xsl:attribute-set mapping</summary>
      ImageLink
    };

    /// <summary>
    /// Sorting order identifiers
    /// </summary>
    public enum SortOrder
    {
      /// <summary>Ascendent</summary>
      Asc,
      /// <summary>Descendent</summary>
      Desc
    };

    #endregion Enums

    #region Constructors

    /// <summary>
    /// Default constructor
    /// </summary>
    public WordDocument()
    {
    }

    #endregion Constructors

    #region Methods

    /// <summary>
    /// Create an xsl file from a wordml by embedded custom xsd schema
    /// </summary>
    /// <param name="wordXmlPath">wordml path</param>
    /// <returns></returns>
    public string CreateXslFromWordml (XmlDocument xmlDoc)
    {
      string result = String.Empty;

      try
      {
        string nameSpace = String.Empty;
        string nameSpaceUri = String.Empty;
        string repeaterXPath = String.Empty;
        bool existRepeater = false;
        StringBuilder xpath = null;
        StringBuilder xpathFull = null;
        StringBuilder xpathItem = null;
        StringBuilder xslAttrubute = null;
        //XmlDocument xmlDoc = null;
        XmlDocument xmlNewDoc = null;
        XmlElement root = null;
        XmlNamespaceManager nsmgr = null;
        XmlNode body = null;
        SchemaNodeDescriptorList descList = null;
        int imageNum = 0;

        xslAttrubute = new StringBuilder ();

        // Get root element
        root = xmlDoc.DocumentElement;

        // Create an XmlNamespaceManager for resolving namespaces
        nsmgr = new XmlNamespaceManager (xmlDoc.NameTable);
        for (int i = 0; i < root.Attributes.Count; i ++)
        {
          XmlAttribute attr = (XmlAttribute) root.Attributes.Item (i);
          if (attr.Prefix.Equals ("xmlns"))
          {
            string prefix = attr.Name.Substring (attr.Prefix.Length + 1);
            // TODO: get namespace by the embedded schema name instead of 
            // the first occurrence of the embedded schema
            if (prefix.StartsWith ("ns"))
            {
              if (nameSpace.Length == 0)
              {
                // Save the namespace of the embedded schema
                nameSpace = prefix;
                nameSpaceUri = attr.Value;
              }
            }
            nsmgr.AddNamespace (prefix, attr.Value);
          }
        }

        //
        // Get and save embedded schema nodes for building xsl expressions
        //

        // Get the root of the embedded schema
        xpath = new StringBuilder ();
        xpath.AppendFormat ("//{0}:*", nameSpace);

        body = root.SelectSingleNode (xpath.ToString (), nsmgr);
        if (body == null)
        {
        }
        else
        {
          descList = new SchemaNodeDescriptorList ();
          repeaterXPath = String.Empty;

          // Get nodes of the embedded schema (it does not contain namespace mark)
          xpath.Length = 0;
          xpath.Append (".//*[not(contains(name(), ':'))]");
          XPathNavigator nav = body.CreateNavigator ();
          if (nav != null)
          {
            XPathNodeIterator nodeIterator = nav.Select (xpath.ToString ());
            while (nodeIterator.MoveNext ())
            {
              SchemaNodeType type = SchemaNodeType.None;

              //
              // Set custom node type for xsl transformation
              //

              if (nodeIterator.Current.HasAttributes)
              {
                string attrValue = String.Empty;
               
                // Is repeater?
                attrValue = nodeIterator.Current.GetAttribute ("repeater", nameSpaceUri);
                if (attrValue.Length > 0 && Convert.ToBoolean (attrValue))
                {
                  type = SchemaNodeType.Repeater;
                  existRepeater = true;
                }

                // Is image link?
                attrValue = nodeIterator.Current.GetAttribute ("imagelink", nameSpaceUri);
                if (attrValue.Length > 0 && Convert.ToBoolean (attrValue))
                {
                  type = SchemaNodeType.ImageLink;
                }
              }

              // Is text?
              if (type == SchemaNodeType.None)
              {
                // Is this the last custom schema node
                XPathNodeIterator childnodeIterator = nodeIterator.Current.Select (xpath.ToString ());
                if (childnodeIterator.Count == 0)
                {
                  type = SchemaNodeType.Text;
                }
              }

              //
              // Build XPath of the nodes of the embedded schema for xsl transformation
              //

              xpathItem = new StringBuilder ();
              xpathFull = new StringBuilder ();
              XPathNodeIterator parents = nodeIterator.Current.SelectAncestors (XPathNodeType.Element, true);
              while (parents.MoveNext ())
              {
                xpathFull.AppendFormat ("{0}/", parents.Current.Name);

                // Skip nodes that contains WordML namespace marks
                if (parents.Current.Name.IndexOf (":") < 1 || 
                  parents.Current.Name.StartsWith (nameSpace))
                {
                  if (parents.Current.Name.StartsWith (nameSpace))
                  {
                    xpathItem.AppendFormat ("{0}/", parents.Current.Name.Substring (nameSpace.Length + 1));
                  }
                  else
                  {
                    xpathItem.AppendFormat ("{0}/", parents.Current.Name);
                  }
                }
              }

              xpathItem.Remove (xpathItem.Length - 1, 1);

              // Rotate pathes
              string xpathString = RotateString (xpathItem.ToString (), new char [] {'/'});
              string xpathFullString = RotateString (xpathFull.ToString (), new char [] {'/'});

              // Repeater node: get first child node of the embedded schema and add its name to the path
              if (type == SchemaNodeType.Repeater)
              {
                XPathNavigator navChild = body.CreateNavigator ();
                if (navChild != null)
                {
                  string xpathChild = String.Format ("{0}/*[not(contains(name(), ':'))]", xpathFullString);

                  XmlNodeList nodelist = root.SelectNodes (xpathChild, nsmgr);
                  if (nodelist != null)
                  {
                    IEnumerator nodeChildEnum = nodelist.GetEnumerator ();
                    while (nodeChildEnum.MoveNext ())
                    {
                      XmlNode childNode = (XmlNode) nodeChildEnum.Current;
                      // Skip nodes that contains WordML namespace marks
                      if (childNode.Name.IndexOf (":") < 1 || 
                        childNode.Name.StartsWith (nameSpace))
                      {
                        if (childNode.Name.StartsWith (nameSpace))
                        {
                          xpathString = String.Format ("{0}/{1}", xpathString, childNode.Name.Substring (nameSpace.Length + 1));
                        }
                        else
                        {
                          xpathString = String.Format ("{0}/{1}", xpathString, childNode.Name);
                        }
                        break;
                      }
                    }
                  }
                }
              }

              // Store datas of the embedded schema nodes in private document class
              if (type != SchemaNodeType.None)
              {
                SchemaNodeDescriptor desc = new SchemaNodeDescriptor (xpathString, xpathFullString, type);
                // Add only a new item
                if (descList.IsXlsXPathExist (desc) == false)
                {
                  descList.Add (desc);
                }
              }
            }
          }
          
          if (existRepeater)
          {
            // Format XPath for Xsl (xsl:for-each)
            descList = FormatXslXPath (body, nsmgr, descList);
          }

          //
          // Create xsl document
          //

          if (descList != null && descList.Count > 0)
          {
            XmlNode newNode = null;
            XmlElement newElement = null;
            XmlNode xslNode = null;

            IEnumerator descriptors = descList.GetEnumerator ();
            while (descriptors.MoveNext ())
            {
              SchemaNodeDescriptor desc = (SchemaNodeDescriptor) descriptors.Current;
        
              // Get nodes of the embedded schema
              xpath.Length = 0;
              xpath.AppendFormat ("{0}", desc.XPath);

              XmlNodeList xslNodes = body.SelectNodes (xpath.ToString (), nsmgr);
              if (xslNodes != null)
              {
                for (int nodeIndex = 0; nodeIndex < xslNodes.Count; nodeIndex ++)
                {
                  xslNode = xslNodes [nodeIndex];
                  if (xslNode != null && desc.Type == SchemaNodeType.Text)
                  {
                    // Insert xsl expression (xsl:value-of) into the wordml node which type is text
                    InsertWmlTextNode (xmlDoc, xslNode, desc.XslExpression, nsmgr);
                  }
                }
              }
            }
            
            //
            // Create image link nodes (xsl:attribute-set)
            //
            
            imageNum = 0;
            descriptors = descList.GetEnumerator ();
            while (descriptors.MoveNext ())
            {
              SchemaNodeDescriptor desc = (SchemaNodeDescriptor) descriptors.Current;
        
              // Get node of the embedded schema
              xpath.Length = 0;
              xpath.AppendFormat ("{0}", desc.XPath);
              xslNode = body.SelectSingleNode (xpath.ToString (), nsmgr);
              if (xslNode != null && desc.Type == SchemaNodeType.ImageLink)
              {
                imageNum ++;

                XmlNode xslImgNode = xslNode.SelectSingleNode (".//v:imagedata", nsmgr);
                if (xslImgNode != null)
                {
                  // Remove src attribute
                  XmlNode imgAttr = xslImgNode.Attributes.GetNamedItem ("src");
                  xslImgNode.Attributes.Remove ((XmlAttribute) imgAttr);
                  // Add xsl expression attribute
                  XmlAttribute newImgAttr = xmlDoc.CreateAttribute ("xsl", "use-attribute-sets", c_NamespaceUriXsl);
                  newImgAttr.Value = String.Format ("image{0}-src", Convert.ToString (imageNum));
                  xslImgNode.Attributes.Append (newImgAttr);

                  xslAttrubute.AppendFormat ("<xsl:attribute-set name=\"image{0}-src\">" + 
                    "<xsl:attribute name=\"src\"><xsl:value-of select=\"{1}\" />" + 
                    "</xsl:attribute></xsl:attribute-set>",
                    Convert.ToString (imageNum), desc.XslXPath);
                }
              }
            }

            //
            // Create repeater node (xsl:for-each)
            //
            
            descriptors = descList.GetEnumerator ();
            while (descriptors.MoveNext ())
            {
              SchemaNodeDescriptor desc = (SchemaNodeDescriptor) descriptors.Current;
        
              // Get node of the embedded schema
              xpath.Length = 0;
              xpath.AppendFormat ("{0}", desc.XPath);
              xslNode = body.SelectSingleNode (xpath.ToString (), nsmgr);
              if (xslNode == null && desc.Type == SchemaNodeType.Repeater)
              {
                // Get embedded repeater nodes of the repeater node
                xpath.Length = 0;
                string [] items = desc.XPath.ToString ().Split ('/');
                if (items.Length - 1 > 0)
                {
                  xpath.AppendFormat ("//{0}", items [items.Length - 1]);
                }
                XmlNodeList xslNodes = body.SelectNodes (xpath.ToString (), nsmgr);
                if (xslNodes != null)
                {
                  for (int nodeIndex = 0; nodeIndex < xslNodes.Count; nodeIndex ++)
                  {
                    xslNode = xslNodes [nodeIndex];
                    if (xslNode != null)
                    {
                      // Find the first child node called xsl:for-each
                      xpath.Length = 0;
                      xpath.Append ("/for-each");
                      XmlNode foreachNode = xslNode.FirstChild;
                      if (foreachNode == null || ! foreachNode.Name.Equals ("xsl:for-each"))
                      {
                        // Save child nodes
                        XmlNode cloneNode = xslNode.Clone ();

                        // Remove child nodes
                        xslNode.RemoveAll ();
                        // Insert a new child node for the xsl transformation
                        newElement = xmlDoc.CreateElement("xsl", "for-each", c_NamespaceUriXsl);
                        newElement.SetAttribute ("select", desc.XslXPath);
                        newNode = xslNode.PrependChild (newElement);
                        // Restore child nodes
                        XmlNode subNode = xslNode.FirstChild;
                        if (cloneNode.HasChildNodes)
                        {
                          for (int k = 0; k < cloneNode.ChildNodes.Count; k ++)
                          {
                            subNode.PrependChild (cloneNode.ChildNodes.Item (k));
                          }
                        }
                      }
                    }
                  }
                }
              }
              else if (xslNode != null && desc.Type == SchemaNodeType.Repeater)
              {
                // Save child nodes
                XmlNode cloneNode = xslNode.Clone ();

                // Remove child nodes
                xslNode.RemoveAll ();
                // Insert a new child node for the xsl transformation
                newElement = xmlDoc.CreateElement("xsl", "for-each", c_NamespaceUriXsl);
                newElement.SetAttribute ("select", desc.XslXPath);
                newNode = xslNode.PrependChild (newElement);
                // Restore child nodes
                XmlNode subNode = xslNode.FirstChild;
                if (cloneNode.HasChildNodes)
                {
                  int childCount = cloneNode.ChildNodes.Count;
                  for (int k = 0; k < childCount; k ++)
                  {
                    subNode.PrependChild (cloneNode.ChildNodes.Item (k).Clone ());
                  }
                }
              }
            }

            XmlElement newRoot = null;
            XmlNode newBody = null;

            // Build xsl:stylesheet main frame
            StringBuilder newDoc = new StringBuilder ();
            newDoc.Append ("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
            newDoc.Append ("<?mso-application progid=\"Word.Document\"?>");
            newDoc.Append ("<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\">");
            newDoc.Append ("<xsl:output method=\"xml\"/>");
            newDoc.Append ("<xsl:template match=\"/\" />");
            newDoc.Append (xslAttrubute.ToString ());
            newDoc.Append ("</xsl:stylesheet>");

            xmlNewDoc = new XmlDocument();
            xmlNewDoc.LoadXml (newDoc.ToString ());
            newRoot = xmlNewDoc.DocumentElement;

            // Add the namespaces for the document from the original document
            XmlNamespaceManager newNsmgr = new XmlNamespaceManager (xmlDoc.NameTable);
            for (int i = 0; i < root.Attributes.Count; i ++)
            {
              XmlAttribute attr = (XmlAttribute) root.Attributes.Item (i);
              if (attr.Prefix.Equals ("xmlns"))
              {
                string prefix = attr.Name.Substring (attr.Prefix.Length + 1);
                newNsmgr.AddNamespace (prefix, attr.Value);
              }
            }
            newNsmgr.AddNamespace ("xsl", c_NamespaceUriXsl);
            
            // Insert w:wordDocument and all subnodes
            newBody = newRoot.SelectSingleNode ("//xsl:template", newNsmgr);
            if (newBody != null)
            {
              // Import original root
              newNode = xmlNewDoc.ImportNode (root, true);
              newBody.AppendChild (newNode);
            }
          }
        }

        // Final touches
        if (xmlNewDoc != null)
        {
          result = xmlNewDoc.OuterXml;
          result = result.Replace ("&lt;", "<");
          result = result.Replace ("&gt;", ">");
        }
      }
      catch (Exception ex)
      {
        throw new Exception ("Hiba l�pett fel a wordml-xsl transzform�ci� sor�n.", ex);
      }

      return result;
    }

    /// <summary>
    /// Format XPath for xsl:for-each
    /// </summary>
    /// <param name="body">Word xml body node</param>
    /// <param name="nsmgr">Namespace manager</param>
    /// <param name="descList">Schema node descriptor container</param>
    /// <returns>Schema node descriptor container with formated XPathes</returns>
    private SchemaNodeDescriptorList FormatXslXPath (XmlNode body, XmlNamespaceManager nsmgr, SchemaNodeDescriptorList descList)
    {
      SchemaNodeDescriptorList result = null;

      try
      {
        if (descList == null)
        {
          throw new Exception ("�rv�nytelen descList (SchemaNodeDescriptorList) param�ter.");
        }
        if (body == null)
        {
          throw new Exception ("�rv�nytelen body (XmlNode) param�ter.");
        }

        // Create SchemaNodeDescriptorList for result
        SchemaNodeDescriptorList descListOut = new SchemaNodeDescriptorList ();

        // Iterate through the schema node
        IEnumerator coll = descList.GetEnumerator ();
        while (coll.MoveNext ())
        {
          SchemaNodeDescriptor desc = (SchemaNodeDescriptor) coll.Current;
          string newXslXPath = String.Empty;

          // Get first repeater parent node
          XmlNode repeaterNode = GetFirstRepeaterParent (body, nsmgr, desc.XPath);
          if (repeaterNode != null)
          {
            // Get xpath of the node
            string parentRepeaterXPath = GetXPath (repeaterNode);

            // Get schema xpath of the repeater node
            SchemaNodeDescriptor descParent = descList.GetItem (parentRepeaterXPath);
            if (descParent != null)
            {
              // Remove the xpath of the first repeater parent node
              if (descParent.XslXPath.Length < desc.XslXPath.Length)
              {
                string sub = desc.XslXPath.Substring (descParent.XslXPath.Length);
                if (sub != String.Empty)
                {
                  newXslXPath = sub;
                
                  // Skip beginner '/'
                  if (newXslXPath.StartsWith("/"))
                  {
                    newXslXPath = newXslXPath.Substring (1);
                  }
                }
              }
            }
          }

          // Add to the result
          SchemaNodeDescriptor descNew = new SchemaNodeDescriptor ((newXslXPath.Length == 0 ? desc.XslXPath : newXslXPath),
            desc.XPath,
            desc.Type);
          descListOut.Add (descNew);
        }

        result = descListOut;
      }
      catch (Exception ex)
      {
        throw new Exception ("Hiba l�pett fel az Xsl xpath form�z�sa sor�n.", ex);
      }
      return result;
    }

    /// <summary>
    /// Get first parent node that his repeater attribute is true
    /// </summary>
    /// <param name="body">Word xml body node</param>
    /// <param name="nsmgr">Namespace manager</param>
    /// <param name="nodeXPath">XPath of the source node </param>
    /// <returns>Result node</returns>
    XmlNode GetFirstRepeaterParent (XmlNode body, XmlNamespaceManager nsmgr, string nodeXPath)
    {
      XmlNode result = null;

      try
      {
        XmlNode node = null;

        if (body == null)
        {
          throw new Exception ("�rv�nytelen body (XmlNode) param�ter.");
        }
        if (body == null)
        {
          throw new Exception ("�rv�nytelen nsmgr (XmlNamespaceManager) param�ter.");
        }
        if (nodeXPath == null || nodeXPath.Length == 0)
        {
          throw new Exception ("�rv�nytelen nodeXPath (string) param�ter.");
        }

        node = body.SelectSingleNode (nodeXPath, nsmgr);
        if (node == null)
        {
        }
        else
        {
          node = node.ParentNode;
          while (node != null && (node.ToString ().Equals ("System.Xml.XmlDocument") == false)) //typeof (node) == System.Type.)
          {
            // Check attribute of the node
            for (int i = 0; i < node.Attributes.Count; i ++)
            {
              XmlNode attrNode = node.Attributes.Item (i);
              if (attrNode != null
                && attrNode.LocalName.Equals ("repeater")
                && attrNode.Value.ToLower ().Equals ("true"))
              {
                result = node;
                break;
              }
            }

            if (result != null)
            {
              break;
            }

            node = node.ParentNode;
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception ("Hiba l�pett fel a az els� sz�l� repeater node meghat�roz�sa sor�n.", ex);
      }
      return result;
    }

    /// <summary>
    /// Get xpath of the xml node
    /// </summary>
    /// <param name="node">Xml node</param>
    /// <returns>XPath string </returns>
    string GetXPath (XmlNode node)
    {
      string result = null;

      try
      {
        XmlNode parentNode = null;
        StringBuilder xPath = new StringBuilder ();

        if (node == null)
        {
          throw new Exception ("�rv�nytelen node (XmlNode) param�ter.");
        }

        xPath.AppendFormat ("{0}", node.Name);

        parentNode = node.ParentNode;

        while (parentNode != null && (parentNode.ToString ().Equals ("System.Xml.XmlDocument") == false))
        {
          xPath.Insert (0, "/");
          xPath.Insert (0, parentNode.Name);

          parentNode = parentNode.ParentNode;
        }
        xPath.Insert (0, "/");

        result = xPath.ToString ();
      }
      catch (Exception ex)
      {
        throw new Exception ("Hiba l�pett fel az XmlNode XPath-�nak meghat�roz�sa sor�n.", ex);
      }
      return result;
    }

    /// <summary>
    /// Insert value of a wordml node
    /// </summary>
    /// <param name="xmlDoc"></param>
    /// <param name="rootNode"></param>
    /// <param name="nodeValue"></param>
    /// <param name="nsmgr"></param>
    /// <returns></returns>
    private XmlNode InsertWmlTextNode (
      XmlDocument xmlDoc,
      XmlNode rootNode,
      string nodeValue,
      XmlNamespaceManager nsmgr)
    {
//      TraceCallEnterEvent.Raise();

      XmlNode result = null;

      try
      {
        XmlNode node = null;
        XmlNode parNode = null;
        XmlNode runNode = null;
        XmlNode newNode = null;
        XmlElement newElement = null;

        if (xmlDoc == null)
        {
          throw new Exception ("�rv�nytelen XmlDocument param�ter.");
        }

        if (rootNode == null)
        {
          throw new Exception ("�rv�nytelen XmlNode param�ter.");
        }

        if (nsmgr == null)
        {
          throw new Exception ("�rv�nytelen XmlNamespaceManager param�ter.");
        }

        // Find text element
        node = rootNode.SelectSingleNode (".//w:t", nsmgr);
        if (node == null)
        {
          // Find paragraph element
          parNode = rootNode.SelectSingleNode (".//w:p", nsmgr);
          if (parNode == null)
          {
            // Insert run | text element
            newElement = xmlDoc.CreateElement ("w", "r", c_NamespaceUriWordml);
            newNode = rootNode.AppendChild (newElement);
                  
            newElement = xmlDoc.CreateElement ("w", "t", c_NamespaceUriWordml);
            newElement.InnerText = nodeValue;
            result = newNode.AppendChild (newElement);
          }
          else
          {
            // Find run element
            runNode = parNode.SelectSingleNode (".//w:r", nsmgr);
            if (runNode == null)
            {
              // Insert run | text element
              newElement = xmlDoc.CreateElement ("w", "r", c_NamespaceUriWordml);
              newNode = parNode.AppendChild (newElement);
                  
              newElement = xmlDoc.CreateElement ("w", "t", c_NamespaceUriWordml);
              newElement.InnerText = nodeValue;
              result = newNode.AppendChild (newElement);
            }
            else
            {
              // Insert text element
              newElement = xmlDoc.CreateElement ("w", "t", c_NamespaceUriWordml);
              newElement.InnerText = nodeValue;
              result = runNode.AppendChild (newElement);
            }
          }
        }
        else
        {
          node.InnerText = nodeValue;
          result = node;
        }
      }
      catch (Exception ex)
      {
        throw new Exception ("Hiba l�pett fel a wordml text node l�trehoz�sa sor�n.", ex);
      }
      
      return result;
    }

    public static string GetRandomFileNameWithPathForWordDocument()
    {
        return Contentum.eTemplateManager.WebService.DocumentHandler.GetTemporaryFileNameWithPath(".doc");
    }

    /// <summary>
    /// Preparing Xsl tranformation
    /// </summary>
    /// <param name="xsl"></param>
    /// <param name="xml"></param>
    /// <param name="pathXmlOut"></param>
    /// <returns></returns>
    public string XslTransformation(string xsl, Result ds_result, string pathXmlOut)
    {
        // a DataSet tartalm�t XML-k�nt
        // - ideiglenes f�jlba �rjuk, ha �rt�ke true,
        // - mem�ria streambe �rjuk, ha �rt�ke false
        // �s onnan olvassuk vissza a transzform�ci�hoz
        // A f�jl haszn�lata WorkAround a System.OutOfMemoryException hiba kezel�s�hez nagy XML adathalmaz este�n
        bool useTempFile = true;

        string result = String.Empty;

        try
        {
            //XmlDocument xmlDoc = null;
            XmlDocument xslDoc = null;
            string buffer = String.Empty;
            string bufferIn = String.Empty;

            //
            // Preparing tranformation
            //

            // Load xml data
            //xmlDoc = new XmlDocument();

            // Skip invalid beginner characters in result xml
            //bufferIn = TrimStartXmlString (xml);

            // Test
            // Logger.WriteSingleLog ("TemplateData_Xsl_" + Guid.NewGuid().ToString() + ".xsl", bufferIn);
            //xmlDoc.LoadXml (bufferIn);

            //XmlElement root = xmlDoc.DocumentElement;

            // Load xsl data
            xslDoc = new XmlDocument();
            xslDoc.LoadXml(xsl);
            XmlElement rootXsl = xslDoc.DocumentElement;
            XPathNavigator navXsl = rootXsl.CreateNavigator();

            // Load xsl navigator to transformation object
            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(navXsl);

            XmlReader reader = null;
            string filePath = null;
            if (useTempFile)
            {
                //String Path = "c:\\temp\\ContentumNet";
                //if (!System.IO.Directory.Exists(Path))
                //{
                //    System.IO.Directory.CreateDirectory(Path);
                //}
                //Random r = new Random();
                //String FileName = r.Next().ToString() + ".doc";
                filePath = WordDocument.GetRandomFileNameWithPathForWordDocument();
                ds_result.Ds.WriteXml(filePath);

                // Create the XmlReader object.
                reader = XmlReader.Create(filePath);
            }
            else // MemoryStream
            {
                MemoryStream streamWriteXml = new MemoryStream();
                ds_result.Ds.WriteXml(streamWriteXml);
                streamWriteXml.Position = 0;

                // Create the XmlReader object.
                reader = XmlReader.Create(streamWriteXml);
            }

            // Transform xml and pack it to a stream
            //XPathNavigator nav = root.CreateNavigator ();
            //MemoryStream stream = new MemoryStream();


            //XPathDocument nav = new XPathDocument(reader);
            //transform.Transform ( nav, null, stream);

            FileStream _fs = new FileStream(pathXmlOut, FileMode.Create);
            transform.Transform(reader, null, _fs);
            reader.Close();

            if (useTempFile)
            {
                try
                {
                    File.Delete(filePath);
                }
                catch
                {
                }
            }

            //Formatting result xml


            // Skip invalid beginner characters in result xml
            //buffer = TrimStartXmlString (stream);

            // Load xml data
            /*XmlDocument xmlResult = new XmlDocument();
            xmlResult.LoadXml (buffer);
            XmlElement rootResult = xmlResult.DocumentElement;

            // Add a processing instruction for showing the xml with MS Word
            XmlProcessingInstruction pi = null;
            pi = xmlResult.CreateProcessingInstruction ("mso-application", "progid=\"Word.Document\"");
            xmlResult.InsertBefore (pi, rootResult);*/

            //
            // Save result to a file
            //

            //FileStream _fs = new FileStream(pathXmlOut, FileMode.Create);
            //stream.WriteTo(_fs);
            //stream.Close();
            _fs.Close();

            //WriteToFile (pathXmlOut, xmlResult.InnerXml);

            result = pathXmlOut;

        }
        catch (Exception ex)
        {
            throw new Exception("Hiba l�pett fel az xsl transzform�ci� sor�n.", ex);
        }

        return result;
    }

    public string XslTransformation_ForXml(string xsl, Result ds_result, string pathXmlOut)
    {
        string result = String.Empty;

        try
        {
            //XmlDocument xmlDoc = null;
            //XmlDocument xslDoc = null;
            string buffer = String.Empty;
            string bufferIn = String.Empty;

            // Load xsl data
            /*xslDoc = new XmlDocument();
            xslDoc.LoadXml(xsl);
            XmlElement rootXsl = xslDoc.DocumentElement;
            XPathNavigator navXsl = rootXsl.CreateNavigator();*/

            // Load xsl navigator to transformation object
            XslCompiledTransform transform = new XslCompiledTransform();
            XmlReader xsl_reader = XmlReader.Create(new StringReader(xsl));
            transform.Load(xsl_reader);

            MemoryStream stream = new MemoryStream();

            // Create the XmlReader object.
            XmlReader reader = XmlReader.Create(new StringReader(ds_result.Ds.Tables[0].Rows[0]["string_xml"].ToString()));

            //XPathDocument nav = new XPathDocument(reader);
            transform.Transform(reader, null, stream);

            FileStream _fs = new FileStream(pathXmlOut, FileMode.Create);
            stream.WriteTo(_fs);
            stream.Close();
            _fs.Close();

            result = pathXmlOut;

        }
        catch (Exception ex)
        {
            throw new Exception("Hiba l�pett fel az xsl transzform�ci� sor�n.", ex);
        }

        return result;
    }

    /// <summary>
    /// Preparing Xsl tranformation
    /// </summary>
    /// <param name="xsl"></param>
    /// <param name="xml"></param>
    /// <param name="pathXmlOut"></param>
    /// <returns></returns>
    public string XslTransformation_string(string xsl, string xml, string pathXmlOut)
    {
        string result = String.Empty;

        try
        {
            XmlDocument xmlDoc = null;
            XmlDocument xslDoc = null;
            string buffer = String.Empty;
            string bufferIn = String.Empty;

            //
            // Preparing tranformation
            //

            // Load xml data
            xmlDoc = new XmlDocument();

            // Skip invalid beginner characters in result xml
            bufferIn = TrimStartXmlString(xml);

            // Test
            // Logger.WriteSingleLog ("TemplateData_Xsl_" + Guid.NewGuid().ToString() + ".xsl", bufferIn);
            xmlDoc.LoadXml(bufferIn);

            XmlElement root = xmlDoc.DocumentElement;

            // Load xsl data
            xslDoc = new XmlDocument();
            xslDoc.LoadXml(xsl);
            XmlElement rootXsl = xslDoc.DocumentElement;
            XPathNavigator navXsl = rootXsl.CreateNavigator();

            // Load xsl navigator to transformation object
            XslTransform transform = new XslTransform();
            transform.Load(navXsl);

            // Transform xml and pack it to a stream
            XPathNavigator nav = root.CreateNavigator();
            MemoryStream stream = new MemoryStream();
            transform.Transform(nav, null, stream);

            //
            // Formatting result xml
            //

            // Skip invalid beginner characters in result xml
            buffer = TrimStartXmlString(stream);

            // Load xml data
            XmlDocument xmlResult = new XmlDocument();
            xmlResult.LoadXml(buffer);
            XmlElement rootResult = xmlResult.DocumentElement;

            // Add a processing instruction for showing the xml with MS Word
            XmlProcessingInstruction pi = null;
            pi = xmlResult.CreateProcessingInstruction("mso-application", "progid=\"Word.Document\"");
            xmlResult.InsertBefore(pi, rootResult);

            //
            // Save result to a file
            //

            WriteToFile(pathXmlOut, xmlResult.InnerXml);

            result = pathXmlOut;
        }
        catch (Exception ex)
        {
            throw new Exception("Hiba l�pett fel az xsl transzform�ci� sor�n.", ex);
        }

        return result;
    }

    /// <summary>
    /// Convert a Wordml file to a Word document file
    /// </summary>
    /// <param name="xmlPath">the wordml file to convert</param>
    /// <param name="replaceFile">if true replace the wordml file with the created file</param>
    /// <returns></returns>
    /*public string ConvertWordmlToDoc (string xmlPath, bool replaceFile)
    {
      return ConvertWord (xmlPath, Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatDocument, replaceFile);
    }*/

    /// <summary>
    /// Convert a Word document file to a Wordml file
    /// </summary>
    /// <param name="docPath">the word document file to convert</param>
    /// <param name="replaceFile">if true replace the word document file with the created file</param>
    /// <returns></returns>
    /*public string ConvertDocToWordml (string docPath, bool replaceFile)
    {
      return ConvertWord (docPath, Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatXML, replaceFile);
    }*/

    /// <summary>
    /// Convert a Word file to an another type Word file
    /// </summary>
    /// <param name="file">file to convert</param>
    /// <param name="format">result file format</param>
    /// <param name="replaceFile">if true replace the file with the created file</param>
    /// <returns></returns>
    /*private string ConvertWord (string file, Microsoft.Office.Interop.Word.WdSaveFormat format, bool replaceFile)
    {
      string result = String.Empty;
      Word.ApplicationClass wordAppl = null;
      Word.Document doc = null;
      bool isOpenedWord = false;
      bool isOpenedDocument = false;

      object vk_read_only = false;
      object vk_visible = false;
      object vk_false = false;
      object vk_true = true;
      object vk_dynamic = 2;
      object vk_missing = System.Reflection.Missing.Value;

      try
      {
        string fileExt = String.Empty;
        object filePathOut = null;
        object filePathIn = null;
        object fileFormat = null;

        // Create application
        wordAppl = new Word.ApplicationClass ();
        if (wordAppl == null)
        {
          throw new Exception ("Hiba l�pett fel a Word alkalmaz�s l�trehoz�sa sor�n.");
        }
        
        wordAppl.Visible = false;

        isOpenedWord = true;

        filePathIn = file;

        doc = wordAppl.Documents.Open (ref filePathIn,
          ref vk_missing, ref vk_read_only, ref vk_missing, ref vk_missing,
          ref vk_missing, ref vk_missing, ref vk_missing, ref vk_missing,
          ref vk_missing, ref vk_missing, ref vk_visible, ref vk_missing,
          ref vk_missing, ref vk_missing, ref vk_missing);
        if (doc == null)
        {
          throw new Exception ("Hiba l�pett fel a Word dokumentum megnyit�sa sor�n.");
        }

        isOpenedDocument = true;
        doc.Activate ();*/

        // Get converted file path
        /*switch (format)
        {
          case Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatDocument:
            fileExt = ".doc";
            break;
          case Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatXML:
            fileExt = ".xml";
            break;
          default:
            fileExt = ".doc";
            break;        
        }*/

        /*fileExt = ".doc";

        filePathOut = Path.Combine (Path.GetDirectoryName (file), Path.GetFileNameWithoutExtension (file) + fileExt);
        fileFormat = format;

        doc.SaveAs (ref filePathOut, ref fileFormat,
          ref vk_missing, ref vk_missing, ref vk_missing, ref vk_missing,
          ref vk_missing, ref vk_missing, ref vk_missing, ref vk_missing,
          ref vk_missing, ref vk_missing, ref vk_missing, ref vk_missing,
          ref vk_missing, ref vk_missing);*/

//        // Close the document
//        doc.Close (ref vk_false, ref vk_missing, ref vk_missing);
//
//        isOpenedDocument = false;
//        doc = null;

//        // Quit from the Word
//        wordAppl.Quit (ref vk_false, ref vk_missing, ref vk_missing);
//
//        isOpenedWord = false;
//        wordAppl = null;

        // Replace wordml file with the created doc file by deleting
        /*if (replaceFile)
        {
          File.Delete (file);
          file = Convert.ToString (filePathOut);
        }

        // Set result
        result = Convert.ToString (filePathOut);
      }
      catch (Exception ex)
      {
        throw new Exception ("Hiba l�pett fel a Word Xml-Doc konverzi� sor�n.", ex);
      }
      finally
      {
        try
        {
          doc.Close (ref vk_false, ref vk_missing, ref vk_missing);
          // Quit from the Word
          wordAppl.Quit (ref vk_false, ref vk_missing, ref vk_missing);
          wordAppl = null;
        }
        catch{}
      }

      return result;
    }*/

    /// <summary>
    /// Rotate a string by a separator character
    /// </summary>
    /// <param name="rotate"></param>
    /// <param name="separator"></param>
    /// <returns></returns>
    private string RotateString (string rotate, char [] separator)
    {
//      TraceCallEnterEvent.Raise();

      string result = String.Empty;

      try
      {
        StringBuilder newItem = new StringBuilder ();

        // Rotate string
        string [] items = rotate.ToString ().Split (separator);
        for (int i = items.Length - 1; i >= 0; i --)
        {
          if (items [i].Length > 0)
          {
            newItem.AppendFormat ("{0}{1}", separator [0].ToString (), items [i]);
          }
        } 

        result = newItem.ToString ();

//        TraceCallReturnEvent.Raise ();
      }
      catch (Exception ex)
      {
        throw new Exception ("Hiba l�pett fel a string t�kr�z�se sor�n.", ex);
      }

      return result;
    }

    /// <summary>
    /// Trim invalid beginner characters from the string
    /// </summary>
    /// <param name="xml"></param>
    /// <returns></returns>
    private string TrimStartXmlString (string xml)
    {
      string result = String.Empty;

      try
      {
        // Skip invalid beginner characters in result xml
        char [] chars = xml.ToCharArray ();
        for (long i = 0; i < chars.LongLength; i ++) 
        { 
          if (chars [i] == (char)0x3c)
          {
            break;
          }
          chars [i] = (char) 0x20; // space
        } 

        UTF8Encoding coderIn = new UTF8Encoding (true); 
        String buffer = new String (chars); 
        result = buffer.Trim ();

      }
      catch (Exception ex)
      {
        throw new Exception ("Hiba l�pett fel a string �rv�nytelen kezd� karaktereinek lev�g�sa sor�n.", ex);
      }

      return result;
    }

    /// <summary>
    /// Trim invalid beginner characters from the stream and put it into result string
    /// </summary>
    /// <param name="stream"></param>
    /// <returns></returns>
    private string TrimStartXmlString (MemoryStream stream)
    {
      string result = String.Empty;

      try
      {
        // Skip invalid beginner characters in result xml
        byte [] bytes = stream.ToArray ();
        for (long i = 0; i < bytes.LongLength; i ++) 
        { 
          if (bytes [i] == 0x3c)
          {
            break;
          }
          bytes [i] = 0x20; // space
        } 

        UTF8Encoding coder = new UTF8Encoding (true); 
        String buffer = new String (coder.GetChars (bytes)); 
        result = buffer.Trim ();

      }
      catch (Exception ex)
      {
        throw new Exception ("Hiba l�pett fel a stream �rv�nytelen kezd� karaktereinek lev�g�sa sor�n.", ex);
      }

      return result;
    }

    /// <summary>
    /// Write data to a file
    /// </summary>
    /// <param name="file"></param>
    /// <param name="content"></param>
    private void WriteToFile (string file, string content)
    {
      try
      {
        FileStream fs = null;
        BinaryWriter writer = null;

        // Delete the file if it exists
        if (File.Exists (file))
        {
          File.Delete (file);
        }

        fs = new FileStream (file, FileMode.OpenOrCreate, FileAccess.Write);
        writer = new BinaryWriter (fs);
        // Write char-by-char for eliminating invalid beginner characters (BinaryWriter bug)
        char [] chars = content.ToCharArray ();
        for (long i = 0; i < chars.LongLength; i ++) 
        {
          writer.Write (chars [i]);
        }
        writer.Flush ();
        writer.Close ();
        fs.Close ();

      }
      catch (Exception ex)
      {
        throw new Exception ("Hiba l�pett fel az adatok f�jlba t�rt�n� �r�sa sor�n.", ex);
      }
      return;
    }

    #endregion Methods

    #region Utility classes

    /// <summary>
    /// Container class for storing SchemaNodeDescriptor objects.
    /// </summary>
    public class SchemaNodeDescriptorList: ArrayList
    {
      #region Methods

      /// <summary>
      /// Adds a new SchemaNodeDescriptor to the list
      /// </summary>
      /// <param name="descriptor">SchemaNodeDescriptor class instance</param>
      public void Add (SchemaNodeDescriptor descriptor)
      {
        base.Add (descriptor);
      }

      /// <summary>
      /// Check existance of the item in the arraylist by XPath and XslXPath properties
      /// </summary>
      /// <param name="descriptor"></param>
      /// <returns></returns>
      public bool IsXlsXPathExist (SchemaNodeDescriptor descriptor)
      {
        bool result = false;

        IEnumerator coll = this.GetEnumerator ();
        while (coll.MoveNext ())
        {
          SchemaNodeDescriptor desc = (SchemaNodeDescriptor) coll.Current;
          if (String.Compare (desc.XslXPath, descriptor.XslXPath) == 0 && 
            String.Compare (desc.XPath, descriptor.XPath) == 0)
          {
            result = true;
            break;
          }
        }
        return result;
      }
     
      /// <summary>
      /// Get item in the list by XPath
      /// </summary>
      /// <param name="descriptor"></param>
      /// <returns></returns>
      public SchemaNodeDescriptor GetItem (string xPath)
      {
        SchemaNodeDescriptor result = null;

        IEnumerator coll = this.GetEnumerator ();
        while (coll.MoveNext ())
        {
          SchemaNodeDescriptor desc = (SchemaNodeDescriptor) coll.Current;
          if (String.Compare (desc.XPath, xPath) == 0)
          {
            result = desc;
            break;
          }
        }
        return result;
      }

      #endregion Methods
    }

    /// <summary>
    /// Class for stroring a node descriptor of a custom xml schema
    /// for xsl transformation.
    /// </summary>
    public class SchemaNodeDescriptor
    {
      #region Members

      /// <summary>
      /// XPath for xsl
      /// </summary>
      private string m_xslXPath = String.Empty;

      /// <summary>
      /// Wordml xpath
      /// </summary>
      private string m_xpath = String.Empty;

      /// <summary>
      /// Type
      /// </summary>
      private SchemaNodeType m_type = SchemaNodeType.None;

      /// <summary>
      /// Depth of the xpath from the root
      /// </summary>
      private int m_xpathDepth = 0;

      #endregion Members

      #region Constructors

      /// <summary>
      /// Default constructor, can be used only by descendant classes.
      /// </summary>
      protected SchemaNodeDescriptor()
      {
      }

      /// <summary>
      /// Creates an SchemaNodeDescriptor item
      /// </summary>
      public SchemaNodeDescriptor (string xslXPath, string xpath, SchemaNodeType type)
      {
        try
        {
          m_xslXPath = xslXPath;
          m_xpath = xpath;
          m_type = type;

          string [] items = xpath.ToString ().Split ('/');
          if (items != null)
          {
            m_xpathDepth = items.Length;
          }
        }
        catch (Exception ex)
        {
          throw new Exception ("Hiba l�pett fel a s�ma node le�r� l�trehoz�sakor.", ex);
        }
      }

      #endregion Constructors

      #region Properties

      /// <summary>
      /// Gets the Wordml XPath of the item.
      /// </summary>
      public string XPath 
      {
        get
        {
          return m_xpath;
        }
      }

      /// <summary>
      /// Gets the Xsl XPath of the item.
      /// </summary>
      public string XslXPath 
      {
        get
        {
          return m_xslXPath;
        }
        set
        {
          m_xslXPath = value;
        }
      }

      /// <summary>
      /// Gets the type of the item.
      /// </summary>
      public SchemaNodeType Type
      {
        get
        {
          return m_type;
        }
      }

      /// <summary>
      /// Gets the depth of the current xpath item from the root
      /// </summary>
      public int XPathDepth
      {
        get
        {
          return m_xpathDepth;
        }
      }

      /// <summary>
      /// Gets the xsl expression of the item.
      /// </summary>
      public string XslExpression
      {
        get
        {
          StringBuilder xsl = new StringBuilder ();

          try
          {
            switch (m_type)
            {
              case SchemaNodeType.ImageLink:
                xsl.AppendFormat ("<v:imagedata xsl:use-attribute-sets=\"{0}\" />", "image-src");
                xsl.AppendFormat ("\r\n");
                xsl.AppendFormat ("<xsl:attribute-set name=\"{0}\">", "image-src");
                xsl.AppendFormat ("<xsl:attribute name=\"src\">");
                xsl.AppendFormat ("<xsl:value-of select=\"{1}\" />", m_xslXPath);
                xsl.AppendFormat ("</xsl:attribute></xsl:attribute-set>");
                break;
              case SchemaNodeType.Repeater:
                xsl.AppendFormat ("<xsl:for-each select=\"{0}\" />", m_xslXPath);
                break;
              case SchemaNodeType.Text:
                xsl.AppendFormat ("<xsl:value-of select=\"{0}\" />", m_xslXPath);
                break;
              default:
                break;
            }
          }
          catch (Exception ex)
          {
            throw new Exception ("Hiba l�pett fel az XslExpression el��ll�t�sa sor�n.", ex);
          }

          return xsl.ToString ();
        }
      }

      #endregion Properties
    }

    /// <summary>
    /// SchemaNodeDescriptor comparer
    /// </summary>
    public class Comparer : System.Collections.IComparer 
    {
      #region Members

      /// <summary>
      /// Comparer property
      /// </summary>
      protected string m_property = null;

      /// <summary>
      /// Sort order
      /// </summary>
      protected SortOrder m_order = SortOrder.Asc;

      #endregion Members

      #region Constructors

      /// <summary>
      /// Default constructor
      /// </summary>
      public Comparer () 
      {
      }

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="comparedBy">Comparer property</param>
      public Comparer (string comparedBy) 
      {
        m_property = comparedBy;
      }

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="order">Comparer property</param>
      public Comparer (SortOrder order) 
      {
        m_order = order;
      }

      #endregion Constructors

      #region Methods

      /// <summary>
      /// Compare objects by a property
      /// </summary>
      /// <param name="x">First object for comparing</param>
      /// <param name="y">Second object for comparing</param>
      /// <returns>
      /// -1, x < y
      ///  0, x = y
      ///  1, x > y
      ///  </returns>
      public int Compare (object x, object y) 
      {
        int result = 0;

        if (x is SchemaNodeDescriptor && y is SchemaNodeDescriptor) 
        {
          SchemaNodeDescriptor descX = (SchemaNodeDescriptor) x;
          SchemaNodeDescriptor descY = (SchemaNodeDescriptor) y;
          if (descX.XPathDepth == descY.XPathDepth)
          {
            result = 0;
          }
          else if (descX.XPathDepth > descY.XPathDepth)
          {
            result = (m_order == SortOrder.Asc ? 1 : -1);
          }
          else
          {
            result = (m_order == SortOrder.Asc ? -1 : 1);
          }
        }

        return result;
      }

      #endregion Methods
    }

    #endregion Utility classes
  }
}
