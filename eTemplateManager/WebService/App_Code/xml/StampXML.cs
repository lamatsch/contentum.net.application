// ====================================================================================
//
// StampXML.cs
//
//  Stamp description classes for handling XML data of stamps.
//
//  Xml structure
//	~~~~~~~~~~~~~
//	<?xml version="1.0" encoding="utf-8" ?> 
//	<document>
//	<header>
//	<template>
//		<name />
//		<type>PDF</type>
//	</template>
//	<caller>
//		<application />
//		<user>
//			<Name />
//			<OU />
//			<Email />
//			<Manager>
//			<Name />
//			<Email />
//			</Manager>
//		</user>
//	</caller>
//	</header>
//	<body>
//	<Stamps>
//		<Stamp>
//		<State>
//			<ID>NEW</ID>
//			<Name />
//		</State>
//		<Shape>
//			<Name />
//		</Shape>
//		<Size>
//			<Name />
//		</Size>
//		<Number />
//		<Text> />
//		<OwnerGroup>
//			<Name />
//		</OwnerGroup>
//		<ResponsiblePerson>
//			<Name />
//			<OU />
//			<Email />
//			<Manager>
//			<Name />
//			<Email />
//			</Manager>
//		</ResponsiblePerson>
//		<ValidFrom />
//		<ValidTo />
//		<Image>[CDATA???]</Image>
//		<Comment />
//		<User>
//			<Name />
//			<OU />
//			<Email />
//			<Manager>
//			<Name />
//			<Email />
//			</Manager>
//		</User>
//		<LastModifier>
//			<Name />
//			<OU />
//			<Email />
//			<Manager>
//			<Name />
//			<Email />
//			</Manager>
//		</LastModifier>
//		<LastModified />
//		<History>
//			<Usage>
//			<Name />
//			<OU />
//			<Email />
//			<Manager>
//				<Name />
//				<Email />
//			</Manager>
//			<From />
//			<To />
//			</Usage>
//			<Usage>
//			<Name />
//			<OU />
//			<Email />
//			<Manager>
//				<Name />
//				<Email />
//			</Manager>
//			<From />
//			<To/>
//			</Usage>
//		</History>
//		</Stamp>
//	</Stamps>
//	</body>
//	</document>
//
// ====================================================================================

// TODO: exeption handling
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.Web;

namespace Contentum.eTemplateManager.Stamp
{
	/// <summary>
	/// Configuration handling class
	/// </summary>
	public class StampXML
	{
		#region Members

    /// <summary>
    /// Header item xmlpath
    /// </summary>
		public static string XPATH_HEADER_TEMPLATE_NAME = "header/template/name";

    /// <summary>
    /// Header item xmlpath
    /// </summary>
    public static string XPATH_HEADER_TEMPLATE_TYPE = "header/template/type";

    /// <summary>
    /// Header item xmlpath
    /// </summary>
    public static string XPATH_HEADER_CALLER_APPLICATION = "header/caller/application";

    /// <summary>
    /// Header item xmlpath
    /// </summary>
    public static string XPATH_HEADER_CALLER_USER_NAME = "header/caller/user/Name";

    /// <summary>
    /// Header item xmlpath
    /// </summary>
    public static string XPATH_HEADER_CALLER_USER_OU = "header/caller/user/OU";

    /// <summary>
    /// Header item xmlpath
    /// </summary>
    public static string XPATH_HEADER_CALLER_USER_EMAIL = "header/caller/user/Email";

    /// <summary>
    /// Header item xmlpath
    /// </summary>
    public static string XPATH_HEADER_CALLER_USER_MANAGER_NAME = "header/caller/user/Manager/Name";

    /// <summary>
    /// Header item xmlpath
    /// </summary>
    public static string XPATH_HEADER_CALLER_USER_MANAGER_EMAIL = "header/caller/user/Manager/Email";

    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_BODY_STAMPS_STAMP = "body/Stamps/Stamp";
		
		/// <summary>
		/// Config file representation
		/// </summary>
		private static XmlDocument m_configXml = new XmlDocument();
		
		#endregion

		#region Properties

		/// <summary>
		/// Template name from header section
		/// </summary>
		public string HeaderTemplateName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_configXml.DocumentElement.SelectSingleNode(XPATH_HEADER_TEMPLATE_NAME);
				if (oNode != null)
				{
						nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Template type from header section
		/// </summary>
		public string HeaderTemplateType
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_configXml.DocumentElement.SelectSingleNode(XPATH_HEADER_TEMPLATE_TYPE);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Caller application from header section
		/// </summary>
		public string HeaderCallerApplication
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_configXml.DocumentElement.SelectSingleNode(XPATH_HEADER_CALLER_APPLICATION);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Caller user name from header section
		/// </summary>
		public string HeaderCallerUserName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_configXml.DocumentElement.SelectSingleNode(XPATH_HEADER_CALLER_USER_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Caller user OU from header section
		/// </summary>
		public string HeaderCallerUserOU
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_configXml.DocumentElement.SelectSingleNode(XPATH_HEADER_CALLER_USER_OU);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Caller user e-mail from header section
		/// </summary>
		public string HeaderCallerUserEmail
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_configXml.DocumentElement.SelectSingleNode(XPATH_HEADER_CALLER_USER_EMAIL);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Caller user manager name from header section
		/// </summary>
		public string HeaderCallerUserManagerName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_configXml.DocumentElement.SelectSingleNode(XPATH_HEADER_CALLER_USER_MANAGER_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Caller user manager e-mail from header section
		/// </summary>
		public string HeaderCallerUserManagerEmail
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_configXml.DocumentElement.SelectSingleNode(XPATH_HEADER_CALLER_USER_MANAGER_EMAIL);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}


		/// <summary>
		/// Collection of stamps
		/// </summary>
		public StampList Stamps
		{
			get
			{
				XmlNodeList stampNodes = m_configXml.DocumentElement.SelectNodes(XPATH_BODY_STAMPS_STAMP);
				StampList list = new StampList();

				foreach (XmlNode stampNode in stampNodes)
				{
					Stamp stamp = new Stamp (stampNode);
					list.Add(stamp);
				}

				return list;
			}
		}

		#endregion

		#region Constructor

/*
    /// <summary>
		/// Constructor, load xml file.
		/// </summary>
		public StampXML (string sConfigPath)
		{
			try
			{
				m_configXml.Load (sConfigPath);
			}
			catch (Exception e) 
			{
				throw new FphTemplateManagerBaseException ("Error loading '" + sConfigPath + "' xml file into XmlDocument!", e);
			}
		}
*/

		/// <summary>
		/// Constructor, load xml string file.
		/// </summary>
		public StampXML (string xmlString)
		{
			try
			{
				m_configXml.LoadXml (xmlString);
			}
			catch (Exception e) 
			{
				throw new FphTemplateManagerBaseException ("Error loading '" + xmlString + "' xml string into XmlDocument!", e);
			}
		}
		#endregion
  }

	/// <summary>
	/// StampList class for representing Stamps xml node.
	/// </summary>
	public class StampList: ArrayList
	{
		#region Members

		/// <summary>
		/// Adds a new stamp to the list
		/// </summary>
		/// <param name="item">Stamp class instance</param>
		public void Add (Stamp item)
		{
			base.Add (item);
		}

		#endregion Members
	}

	/// <summary>
	/// Stamp class for representing stamp xml node.
	/// </summary>
	public class Stamp
	{
		#region Members

    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_STATE_ID = "State/ID";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_STATE_NAME = "State/Name";

    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_SHAPE_ID = "Shape/ID";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_SHAPE_NAME = "Shape/Name";

    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_SIZE_ID = "Size/ID";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_SIZE_NAME = "Size/Name";

    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_NUMBER = "Number";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_TEXT = "Text";
		
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_OWNERGROUP_NAME = "OwnerGroup/Name";
		
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_RESPONSIBLEPERSON_NAME = "ResponsiblePerson/Name";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_RESPONSIBLEPERSON_OU = "ResponsiblePerson/OU";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_RESPONSIBLEPERSON_EMAIL = "ResponsiblePerson/Email";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_RESPONSIBLEPERSON_MANAGER_NAME = "ResponsiblePerson/Manager/Name";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_RESPONSIBLEPERSON_MANAGER_EMAIL = "ResponsiblePerson/Manager/Email";
		
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_VALIDFROM = "ValidFrom";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_VALIDTO = "ValidTo";

    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_IMAGE = "Image";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_COMMENT = "Comment";
		
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_USER_NAME = "User/Name";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_USER_OU = "User/OU";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_USER_EMAIL = "User/Email";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_USER_MANAGER_NAME = "User/Manager/Name";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_USER_MANAGER_EMAIL = "User/Manager/Email";

    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_LASTMODIFIER_NAME = "LastModifier/Name";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_LASTMODIFIER_OU = "LastModifier/OU";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_LASTMODIFIER_EMAIL = "LastModifier/Email";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_LASTMODIFIER_MANAGER_NAME = "LastModifier/Manager/Name";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_LASTMODIFIER_MANAGER_EMAIL = "LastModifier/Manager/Email";

    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_LASTMODIFIED = "LastModified";

    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_HISTORY = "History/Usage";
		
		/// <summary>
		/// Stamp xml node
		/// </summary>
		private static XmlNode m_xmlNode = new XmlDocument();

		#endregion Members

		#region Constructors

		/// <summary>
		/// Default constructor, can be used only by descendant classes.
		/// </summary>
		protected Stamp()
		{
		}

		/// <summary>
		/// Creates a Stamp item
		/// </summary>
		public Stamp (XmlNode node)
		{
			m_xmlNode = node;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// State Id from stamp section
		/// </summary>
		public string StateId
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_STATE_ID);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// State name from stamp section
		/// </summary>
		public string StateName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_STATE_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Shape id from stamp section
		/// </summary>
		public string ShapeId
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_SHAPE_ID);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Shape name from stamp section
		/// </summary>
		public string ShapeName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_SHAPE_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Size id from stamp section
		/// </summary>
		public string SizeId
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_SIZE_ID);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Size name from stamp section
		/// </summary>
		public string SizeName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_SIZE_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Number from stamp section
		/// </summary>
		public string Number
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_NUMBER);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Text from stamp section
		/// </summary>
		public string Text
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_TEXT);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Name of owner group from stamp section
		/// </summary>
		public string OwnerGroupName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_OWNERGROUP_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Name of responsible from stamp section
		/// </summary>
		public string ResponsiblePersonName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_RESPONSIBLEPERSON_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Organization unit of responsible from stamp section
		/// </summary>
		public string ResponsiblePersonOU
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_RESPONSIBLEPERSON_OU);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// E-mail of responsible from stamp section
		/// </summary>
		public string ResponsiblePersonEmail
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_RESPONSIBLEPERSON_EMAIL);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Name of manager of responsible from stamp section
		/// </summary>
		public string ResponsiblePersonManagerName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_RESPONSIBLEPERSON_MANAGER_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// E-mail of manager of responsible from stamp section
		/// </summary>
		public string ResponsiblePersonManagerEmail
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_RESPONSIBLEPERSON_MANAGER_EMAIL);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// ValidFrom from stamp section
		/// </summary>
		public string ValidFrom
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_VALIDFROM);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// ValidTo from stamp section
		/// </summary>
		public string ValidTo
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_VALIDTO);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Image binary data from stamp section
		/// </summary>
		public string /*byte []*/ Image
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_IMAGE);
				if (oNode != null)
				{
					// TODO: convert image binary data
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Comment from stamp section
		/// </summary>
		public string Comment
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_COMMENT);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Name of user from stamp section
		/// </summary>
		public string UserName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_USER_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Organisation unit of user from stamp section
		/// </summary>
		public string UserOU
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_USER_OU);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// E-mail of user from stamp section
		/// </summary>
		public string UserEmail
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_USER_EMAIL);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Name of manager of user from stamp section
		/// </summary>
		public string UserManagerName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_USER_MANAGER_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// E-mail of manager of user from stamp section
		/// </summary>
		public string UserManagerEmail
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_USER_MANAGER_EMAIL);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Name of LastModifier from stamp section
		/// </summary>
		public string LastModifierName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_LASTMODIFIER_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Organisation unit of LastModifier from stamp section
		/// </summary>
		public string LastModifierOU
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_LASTMODIFIER_OU);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// E-mail unit of LastModifier from stamp section
		/// </summary>
		public string LastModifierEmail
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_LASTMODIFIER_EMAIL);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Name of manager of LastModifier from stamp section
		/// </summary>
		public string LastModifierManagerName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_LASTMODIFIER_MANAGER_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// E-mail of manager of LastModifier from stamp section
		/// </summary>
		public string LastModifierManagerEmail
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_LASTMODIFIER_MANAGER_EMAIL);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}


		/// <summary>
		/// Last modification date from stamp section
		/// </summary>
		public string LastModified
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_LASTMODIFIED);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// History collection from stamp section
		/// </summary>
		public StampUsageList History
		{
			get
			{
				XmlNodeList stampUsageNodes = m_xmlNode.SelectNodes(XPATH_HISTORY);
				StampUsageList list = new StampUsageList ();

				foreach (XmlNode stampUsageNode in stampUsageNodes)
				{
					StampUsage usage = new StampUsage (stampUsageNode);
					list.Add(usage);
				}

				return list;
			}
		}

		#endregion Properties
	}


	/// <summary>
	/// StampUsageList class for representing History xml nodes.
	/// </summary>
	public class StampUsageList: ArrayList
	{
		#region Members

		/// <summary>
		/// Adds a new stamp to the list
		/// </summary>
		/// <param name="item">StampUsage class instance</param>
		public void Add (StampUsage item)
		{
			base.Add (item);
		}

		#endregion Members
	}

	/// <summary>
	/// Stamp class for representing stamp xml node.
	/// </summary>
	public class StampUsage
	{
		#region Members

    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_NAME = "Name";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_OU = "OU";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_EMAIL = "Email";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_MANAGER_NAME = "Manager/Name";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_MANAGER_EMAIL = "Manager/Email";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_FROM = "From";
    /// <summary>
    /// Body item xmlpath
    /// </summary>
    public static string XPATH_TO = "To";
		
		/// <summary>
		/// Stamp xml node
		/// </summary>
		private static XmlNode m_xmlNode = new XmlDocument();

		#endregion Members

		#region Constructors

		/// <summary>
		/// Default constructor, can be used only by descendant classes.
		/// </summary>
		protected StampUsage()
		{
		}

		/// <summary>
		/// Creates a Stamp item
		/// </summary>
		public StampUsage (XmlNode node)
		{
			m_xmlNode = node;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// Name from history section
		/// </summary>
		public string Name
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Organisation unit from history section
		/// </summary>
		public string OU
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_OU);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// E-mail from history section
		/// </summary>
		public string Email
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_EMAIL);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Name of manager from history section
		/// </summary>
		public string ManagerName
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_MANAGER_NAME);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// Email of manager from history section
		/// </summary>
		public string ManagerEmail
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_MANAGER_EMAIL);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// From date from history section
		/// </summary>
		public string From
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_FROM);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}

		/// <summary>
		/// To date from history section
		/// </summary>
		public string To
		{
			get
			{
				string nodeValue = String.Empty;
				XmlNode oNode = m_xmlNode.SelectSingleNode(XPATH_TO);
				if (oNode != null)
				{
					nodeValue = oNode.InnerText;
				}
				return nodeValue;
			}
		}
		#endregion Properties
	}

	#region Exception class

	/// <summary>
	/// Exception handlig class for the ....
	/// </summary>
	public class FphTemplateManagerBaseException : Exception
	{
    /// <summary>
    /// Constructor
    /// </summary>
    public FphTemplateManagerBaseException(string message, Exception e) : base(message, e) 
		{
		}
	}

	#endregion
}
