namespace Contentum.eTemplateManager.WebService.WebControls
{
	using System;
	using System.Data;
	using System.Drawing;
  using System.Collections;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///	This class implements page header custom control
	/// </summary>
	public partial class Header : System.Web.UI.UserControl
	{
    #region Constants

    private const string c_CaptionStarPage = "Kezd� oldal";
    private const string c_CaptionAdministation = "Alkalmaz�sok �s sablonok";

    #endregion Constants

    #region Controls

    /// <summary>
    /// Menu repeater
    /// </summary>
    /// <summary>
    /// Error label
    /// </summary>
    /// <summary>
    /// A lista cell�ja
    /// </summary>
    /// <summary>
    /// A hiba�zenet cell�ja
    /// </summary>

    #endregion Controls

    #region Web Form Designer generated code
    /// <summary>
    /// Web Form Designer generated code
    /// </summary>
    /// <param name="e"></param>
    override protected void OnInit(EventArgs e)
    {
      //
      // CODEGEN: This call is required by the ASP.NET Web Form Designer.
      //
      InitializeComponent();
      base.OnInit(e);
    }
		
    /// <summary>
    ///		Required method for Designer support - do not modify
    ///		the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.repMenu.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.repMenu_ItemDataBound);
    }
    #endregion

    #region Members and properties

    /// <summary>
    /// Selected menu ID
    /// </summary>
    public string Selected
    {
      get
      {
        if (ViewState["Selected"] != null)
        {
          return (string) ViewState["Selected"];
        }
        else
        {
          return null;
        }
      }
      set
      {
        ViewState["Selected"] = value;
      }
    }

    /// <summary>
    /// List of menu item descriptor
    /// </summary>
    private ArrayList m_MenuList = new ArrayList ();

    /// <summary>
    /// Menu item list 
    /// </summary>
    public ArrayList MenuList
    {
      get
      {
        return m_MenuList;
      }
      set
      {
        m_MenuList = value;
      }
    }

    #endregion

    #region Page load

    /// <summary>
    /// Load page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, System.EventArgs e)
    {
      if(! Page.IsPostBack)
      {
        try
        {
          int minID = 1;
          int maxID = 2;

          // Get id of the selected tabsheet
          if (Request["Selected"] != null)
          {
            if (Convert.ToInt32(Request["Selected"]) > maxID)
            {
              this.Selected = maxID.ToString ();
            }
            else if (Convert.ToInt32(Request["Selected"]) < minID)
            {
              this.Selected = minID.ToString();
            }
            else
            {
              this.Selected = Request["Selected"];
            }
          }
          else
          {
            this.Selected = "1";
          }

          // Load menu bar
          LoadMenu();
        }
        catch(Exception ex)
        {
          lblError.Visible = true;
          lblError.Text = ex.Message;
        }
      }
    }

    #endregion Page load

    #region Methods

    /// <summary>
    /// Load menu bar
    /// </summary>
    public void LoadMenu()
    {
      try
      {
        // Create menu descriptor list
        m_MenuList.Add(new MenuItem ("1", c_CaptionStarPage, "Default.aspx"));
        m_MenuList.Add(new MenuItem ("2", c_CaptionAdministation, "ApplicationSelect.aspx"));
 
        // Set the DataSource of the Repeater. 
        repMenu.DataSource = m_MenuList;
        repMenu.DataBind();
      }
      catch(Exception ex)
      {
        throw ex;
      }
    }

    #endregion Methods

    #region Event handlers

    /// <summary>
    /// Repeater event hander for initializing menu bar
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void repMenu_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
      try
      {
        MenuItem menuItem = (MenuItem)e.Item.DataItem;

        // Get controls
        System.Web.UI.WebControls.Label lblItem = e.Item.FindControl("lblItem") as System.Web.UI.WebControls.Label; 
        System.Web.UI.WebControls.HyperLink lnkLink = e.Item.FindControl("lnkLink") as System.Web.UI.WebControls.HyperLink; 

        System.Web.UI.HtmlControls.HtmlTableCell td1 = e.Item.FindControl("td1") as System.Web.UI.HtmlControls.HtmlTableCell;
        System.Web.UI.HtmlControls.HtmlTableCell td2 = e.Item.FindControl("td2") as System.Web.UI.HtmlControls.HtmlTableCell;
        System.Web.UI.HtmlControls.HtmlTableCell td3 = e.Item.FindControl("td3") as System.Web.UI.HtmlControls.HtmlTableCell;
        System.Web.UI.HtmlControls.HtmlImage img1 = e.Item.FindControl("img1") as System.Web.UI.HtmlControls.HtmlImage;
        System.Web.UI.HtmlControls.HtmlImage img2 = e.Item.FindControl("img2") as System.Web.UI.HtmlControls.HtmlImage;

        // Set binder information for repeater elements
        if (lblItem != null)
        {
          lblItem.Text = menuItem.Caption;
        }
        if (lnkLink != null)
        {
          // Set url for links
          if (menuItem.Caption.CompareTo (c_CaptionStarPage) == 0)
          {
            lnkLink.NavigateUrl = "../" + menuItem.Link + "?Selected=1";
          }
          else if (menuItem.Caption.CompareTo (c_CaptionAdministation) == 0)
          {
            lnkLink.NavigateUrl = "../" + menuItem.Link + "?Selected=2";
          }

          // Paint tabsheets face
          if (this.Selected != null)
          {
            if (menuItem.ID.CompareTo(this.Selected) == 0)
            {
              lnkLink.CssClass = "BpIntra_TabActiveLink";
              td1.Attributes.Add("background","images/bgfulbal_act.gif");
              td2.Attributes.Add("class","BpIntra_TabActive");
              td3.Attributes.Add("background","images/bgfuljobb_act.gif");
              img1.Src="images/fulbal_act.gif";
              img2.Src="images/fuljobb_act.gif";
            }
            else
            {
              lnkLink.CssClass = "BpIntra_TabInactiveLink";
              td1.Attributes.Add("background","images/bgfulbal_inact.gif");
              td2.Attributes.Add("class","BpIntra_TabInactive");
              td3.Attributes.Add("background","images/bgfuljobb_inact.gif");
              img1.Src="images/fulbal_inact.gif";
              img2.Src="images/fuljobb_inact.gif";
            }
          }
        }
      }
      catch(Exception ex)
      {
        throw ex;
      }
    }

    #endregion Event handlers
	}
  
  #region MenuItem class

  /// <summary>
  /// This class implements property sheet like menu item descriptor
  /// </summary>

  #endregion MenuItem class
}
