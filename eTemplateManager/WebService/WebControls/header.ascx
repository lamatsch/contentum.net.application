<%@ Control Language="c#" Inherits="Contentum.eTemplateManager.WebService.WebControls.Header" CodeFile="Header.ascx.cs" %>
<TABLE class="FS100" height="60" cellSpacing="0" cellPadding="0" width="100%" border="0">
  <TBODY>
    <TR>
      <TD class="BpIntra_PgTitleBg">
        <TABLE class="FS100" height="60" cellSpacing="0" cellPadding="0" width="100%" border="0">
          <TBODY>
            <TR>
              <TD width="107"><A href="http://www.intra.fph.hu/engine.aspx?page=home"><IMG height="59" src="images/bp_logo.jpg" width="107" border="0"></A></TD>
              <TD class="BpIntra_PgDesc" vAlign="middle" width="200">
                <P>
                  <TABLE class="FS100" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
                    <TR>
                      <TD>
                        <A class="BpIntra_Caption" href="http://www.intra.fph.hu/engine.aspx?page=home">Budapest 
                          Főpolgármesteri Hivatal</A>
                      </TD>
                    </TR>
                    <TR>
                      <TD class="BpIntra_Captionsub">&nbsp;&nbsp;Sablonkezelő adminisztrátor
                      </TD>
                    </TR>
                  </TABLE>
                </P>
              </TD>
              <TD vAlign="middle" align="right">
                <TABLE class="FS100" height="59" cellSpacing="0" cellPadding="0" width="100%" border="0">
                  <TBODY>
                    <TR>
                      <TD vAlign="top" align="right">
                        <TABLE class="FS100" cellSpacing="0" cellPadding="0">
                          <TBODY>
                            <TR>
                              <TD vAlign="top">
                                <table cellspacing="0" cellpadding="0" class="FS100">
                                  <tr>
                                    <td valign="top">
                                    </td>
                                  </tr>
                                </table>
                                <table width="100%" height="9" border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td class="BpIntra_PtVSpace"></td>
                                  </tr>
                                </table>
                              </TD>
                            </TR>
                          </TBODY>
                        </TABLE>
                        <TABLE height="9" cellSpacing="0" cellPadding="0" width="100%" border="0">
                          <TBODY>
                            <TR>
                              <TD class="BpIntra_PtVSpace"></TD>
                            </TR>
                          </TBODY>
                        </TABLE> <!--/Portlet--> <!-- zone end --></TD>
                    </TR>
                    <TR>
                      <TD align="right">&nbsp;</TD>
                    </TR>
                  </TBODY>
                </TABLE>
              </TD>
            </TR>
          </TBODY>
        </TABLE>
      </TD>
    </TR>
  </TBODY>
</TABLE>
<TABLE class="BpIntra_PgHeadBG FS100" height="25" cellSpacing="0" cellPadding="0" width="100%"
  border="0">
  <TBODY>
    <TR>
      <TD vAlign="bottom" align="left">
        <TABLE class="FS100" cellSpacing="0" cellPadding="0" border="0">
          <TBODY>
            <TR>
              <TD vAlign="bottom"><!--Portlet-->
                <TABLE class="BpIntra_Portlet FS100" cellSpacing="0" cellPadding="0" width="100%">
                  <TBODY>
                    <TR id="pid7113_body">
                      <TD class="BpIntra_PtBody BpIntra_FS" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px"
                        vAlign="top">
                        <TABLE class="FS100" cellSpacing="0" cellPadding="0" border="0">
                          <TBODY>
                            <TR>
                              <TD width="4"><IMG src="images/spacer.gif" width="4"></TD>
                              <TD id="tdTableList" runat="server">
                                <asp:Repeater ID="repMenu" Runat="server" EnableViewState="True">
                                  <ItemTemplate>
                                    <TD id="td1" runat="server" vAlign="top" background="images/bgfulbal_act.gif"><IMG id="img1" runat="server" height="19" src="images/fulbal_act.gif" width="5"></TD>
                                    <TD id="td2" runat="server" class="BpIntra_TabActive" align="center">
                                      <P class="BpIntra_P BpIntra_Bold">
                                        <asp:HyperLink CssClass="BpIntra_TabInactiveLink" NavigateUrl="Default.aspx" ID="lnkLink" Runat="server">
                                          <asp:Label ID="lblItem" Runat="server"></asp:Label>
                                        </asp:HyperLink></P>
                                    </TD>
                                    <TD id="td3" runat="server" vAlign="top" background="images/bgfuljobb_act.gif"><IMG id="img2" runat="server" height="19" src="images/fuljobb_act.gif" width="5"></TD>
                                    <TD width="4"><IMG src="images/spacer.gif" width="4"></TD>
                                  </ItemTemplate>
                                </asp:Repeater>
                              </TD>
                              <TD id="tdError" runat="server">
                                <asp:Label ID="lblError" Runat="server" Visible="False"></asp:Label>
                              </TD>
                            </TR>
                          </TBODY>
                        </TABLE>
                      </TD>
                    </TR>
                  </TBODY>
                </TABLE> <!--/Portlet--></TD>
            </TR>
          </TBODY>
        </TABLE>
      </TD>
      <TD vAlign="bottom" align="right">
        <TABLE class="FS100" height="25" cellSpacing="0" cellPadding="0" border="0">
          <TBODY>
            <TR vAlign="middle">
              <TD class="BpIntra_PgDesc BpIntra_FS" vAlign="middle" align="right" width="10">&nbsp;</TD>
              <TD class="BpIntra_PgDesc BpIntra_FS" vAlign="middle" align="right" colSpan="5"><!-- Bejelentkezett user portlet --> 
                Bejelentkezve:&nbsp; 
                <%= System.DateTime.Now %> <!-- zone end --></TD>
              <TD align="left" width="10">&nbsp;</TD>
            </TR>
          </TBODY>
        </TABLE>
      </TD>
    </TR>
  </TBODY>
</TABLE>
<BR>
