﻿using AjaxControlToolkit;
using System;
using System.Web.UI;

namespace Contentum.eUIControls
{
    public class CustomCascadingDropDown : CascadingDropDown
    {
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            CheckParentControl();
            try
            {
                base.Render(writer);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CheckParentControl()
        {
            if (!String.IsNullOrEmpty(this.ParentControlID))
            {
                Control c = this.FindControl(this.ParentControlID);
                if (c != null)
                {
                    if (!c.Visible)
                    {
                        this.ParentControlID = String.Empty;
                    }
                }
            }
        }
    }
}
