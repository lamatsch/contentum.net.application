using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

namespace Contentum.eUIControls
{
    public class CustomFunctionImageButton : System.Web.UI.WebControls.ImageButton
    {

        private const string CssClass_disableditem = "disableditem";
        private const string CssClass_hiddenitem = "hiddenitem";
        private const string CssClass_item_default = "highlightit";

        [DefaultValue(false)]
        public bool HideIfDisabled
        {
            get
            {
                object o = ViewState["HideIfDisabled"];
                if (o != null)
                    return (bool)ViewState["HideIfDisabled"];
                return false;
            }
            set { ViewState["HideIfDisabled"] = value; }
        }

        [DefaultValue(CssClass_disableditem)]
        public string CssClassDisabledItem
        {
            get
            {
                object o = ViewState["CssClassDisabledItem"];
                if (o != null)
                    return ViewState["CssClassDisabledItem"].ToString();
                return CssClass_disableditem;
            }
            set { ViewState["CssClassDisabledItem"] = value; }
        }

        [DefaultValue(CssClass_hiddenitem)]
        public string CssClassHiddenItem
        {
            get
            {
                object o = ViewState["CssClassHiddenItem"];
                if (o != null)
                    return ViewState["CssClassHiddenItem"].ToString();
                return CssClass_hiddenitem;
            }
            set { ViewState["CssClassHiddenItem"] = value; }
        }

        [DefaultValue(CssClass_item_default)]
        public string CssClassItemDefault
        {
            get
            {
                object o = ViewState["CssClassItemDefault"];
                if (o != null)
                    return ViewState["CssClassItemDefault"].ToString();
                return CssClass_item_default;
            }
            set { ViewState["CssClassItemDefault"] = value; }
        }

        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;

                if (value == true)
                {
                    AddCssClass(CssClassItemDefault);
                    RemoveCssClass(CssClassDisabledItem);
                    RemoveCssClass(CssClassHiddenItem);
                }
                else
                {
                    if (HideIfDisabled)
                    {
                        AddCssClass(CssClassHiddenItem);// teljes elrejt�s
                        RemoveCssClass(CssClassItemDefault);
                        RemoveCssClass(CssClassDisabledItem);
                    }
                    else
                    {
                        AddCssClass(CssClassDisabledItem);// sz�rk�t�s
                        RemoveCssClass(CssClassItemDefault);
                        RemoveCssClass(CssClassHiddenItem);
                    }
                }
            }
        }

        #region Utils
        protected void AddCssClass(string className)
        {
            if (!String.IsNullOrEmpty(className) && !base.CssClass.Contains(className))
            {
                if (!String.IsNullOrEmpty(base.CssClass))
                {
                    base.CssClass += " ";
                }

                base.CssClass += className;
            }
        }

        protected void RemoveCssClass(string className)
        {
            if (!String.IsNullOrEmpty(className) && base.CssClass.Contains(className))
            {
                string newCssClass = base.CssClass.Replace(className, "").Trim();
                // remove multiple white spaces
                newCssClass = System.Text.RegularExpressions.Regex.Replace(newCssClass, @"\s+", " ");

                base.CssClass = newCssClass;
            }
        }
        #endregion Utils

        #region constructors

        public CustomFunctionImageButton()
            : base()
        {
            this.CssClassDisabledItem = CssClass_disableditem;
            this.CssClassHiddenItem = CssClass_hiddenitem;
            this.CssClassItemDefault = CssClass_item_default;
            this.HideIfDisabled = false;
        }

        public CustomFunctionImageButton(bool hideIfDisabled)
            : this()
        {
            this.HideIfDisabled = hideIfDisabled;
        }

        public CustomFunctionImageButton(bool hideIfDisabled, string cssclass_item_default)
            : this(hideIfDisabled)
        {            
            this.CssClassItemDefault = cssclass_item_default;
        }
        #endregion constructors

        #region property setters

        public void SetProperties(bool hideIfDisabled, string cssclass_item_default)
        {
            this.HideIfDisabled = hideIfDisabled;
            this.CssClassItemDefault = cssclass_item_default;
        }

        public void SetProperties(string cssclass_item_default, string cssclass_disableditem, string cssclass_hiddenitem)
        {
            this.CssClassDisabledItem = cssclass_disableditem;
            this.CssClassHiddenItem = cssclass_hiddenitem;
            this.CssClassItemDefault = cssclass_item_default;
        }


        public void SetProperties(bool hideIfDisabled, string cssclass_item_default, string cssclass_disableditem, string cssclass_hiddenitem)
        {
            this.HideIfDisabled = hideIfDisabled;
            this.CssClassDisabledItem = cssclass_disableditem;
            this.CssClassHiddenItem = cssclass_hiddenitem;
            this.CssClassItemDefault = cssclass_item_default;
        }
        #endregion property setters
    }
}
