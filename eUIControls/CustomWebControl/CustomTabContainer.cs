﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUIControls
{
    /// <summary>
    /// Summary description for CustomTabContainer
    /// </summary>
    public class CustomTabContainer : AjaxControlToolkit.TabContainer
    {
        public CustomTabContainer()
            : base()
        {
        }

        /// <summary>
        /// .net 3.5 framework probléma javítása, csak egyszer fusson le az ActiveTabChanged esemény
        /// 3.5-ös Ajax Control Toolkit esetén nem kell
        /// </summary>
        private bool isActiveTabChangedFired = false;
        protected override void OnActiveTabChanged(EventArgs e)
        {
            if (!isActiveTabChangedFired)
            {
                base.OnActiveTabChanged(e);
                isActiveTabChangedFired = true;
            }
        }
    }
}
