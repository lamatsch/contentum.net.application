using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUIControls
{
    public class CustomTextBox : System.Web.UI.WebControls.TextBox
    {
        public override bool ReadOnly
        {
            get
            {
                return base.ReadOnly;
            }
            set
            {
                base.ReadOnly = value;
                if (value == true)
                {
                    Utility.AddCssClass(this, Utility.ReadOnlyStyle);
                }
                else
                {
                    Utility.RemoveCssClass(this, Utility.ReadOnlyStyle);
                }
            }
        }
    }
}
