﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.ComponentModel;


namespace Contentum.eUIControls
{
    /// <summary>
    /// Summary description for DynamicValueControl
    /// </summary>
    public class DynamicValueControl : System.Web.UI.WebControls.WebControl
    {
        private void SetControlProperty(System.Web.UI.Control control, string propertyName, object propertyValue)
        {
            if (control == null || String.IsNullOrEmpty(propertyName)) return;

            switch (propertyName)
            {
                case "ReadOnly":
                    {
                        #region ReadOnly properties
                        bool value = (bool)propertyValue;
                        System.Reflection.PropertyInfo pi = control.GetType().GetProperty("ReadOnly", typeof(bool));
                        if (pi != null)
                        {
                            pi.SetValue(control, value, null);
                        }
                        else
                        {
                            pi = control.GetType().GetProperty("Enabled", typeof(bool));
                            if (pi != null)
                            {
                                pi.SetValue(control, !value, null);
                            }
                        }
                        #endregion ReadOnly properties
                    }
                    break;
                case "Validate":
                    {
                        #region Validate properties
                        bool value = (bool)propertyValue;
                        System.Reflection.PropertyInfo pi = control.GetType().GetProperty("Validate", typeof(bool));
                        if (pi != null)
                        {
                            pi.SetValue(control, value, null);
                        }
                        #endregion Validate properties
                    }
                    break;
                case "Value":
                    {
                        #region Value properties
                        string value = (string)propertyValue;
                        System.Reflection.PropertyInfo pi = control.GetType().GetProperty("SelectedValue", typeof(String));
                        if (pi != null)
                        {
                            pi.SetValue(control, value, null);
                        }
                        else
                        {
                            pi = control.GetType().GetProperty("Value", typeof(String));
                            if (pi != null)
                            {
                                pi.SetValue(control, value, null);
                            }
                            else
                            {
                                pi = control.GetType().GetProperty("SelectedDate", typeof(DateTime));
                                if (pi != null)
                                {
                                    DateTime dt;
                                    if (DateTime.TryParse(value, out dt))
                                    {
                                        pi.SetValue(control, dt, null);
                                    }
                                    else
                                    {
                                        pi.SetValue(control, new DateTime(), null);
                                    }

                                }
                                else
                                {
                                    pi = control.GetType().GetProperty("Checked", typeof(bool));
                                    if (pi != null)
                                    {
                                        bool bValue;
                                        if (!Boolean.TryParse(value, out bValue))
                                        {
                                            bValue = false;
                                        }
                                        pi.SetValue(control, bValue, null);
                                    }
                                    else
                                    {
                                        pi = control.GetType().GetProperty("Id_HiddenField", typeof(String));
                                        if (pi != null)
                                        {
                                            pi.SetValue(control, value, null);
                                        }
                                        else
                                        {
                                            pi = control.GetType().GetProperty("Text", typeof(String));
                                            if (pi != null)
                                            {
                                                pi.SetValue(control, value, null);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion Value properties
                    }
                    break;
                // BLG_608
                case "Ismetlodo":
                    {
                        #region Ismetlodo properties
                        bool value = (bool)propertyValue;
                        System.Reflection.PropertyInfo pi = control.GetType().GetProperty("Ismetlodo", typeof(bool));
                        if (pi != null)
                        {
                            pi.SetValue(control, value, null);
                        }
                        #endregion Ismetlodo properties
                    }
                    break;
                default:
                    {
                        #region default properties
                        System.Reflection.PropertyInfo pi = control.GetType().GetProperty(propertyName);
                        if (pi != null)
                        {
                            pi.SetValue(control, propertyValue, null);
                        }
                        #endregion default properties
                    }
                    break;

            }
        }

        private bool _ReadOnly = false;
        public bool ReadOnly
        {
            get
            {
                //EnsureChildControls();
                if (this.Controls.Count > 0)
                {
                    System.Reflection.PropertyInfo pi = this.Controls[0].GetType().GetProperty("ReadOnly", typeof(bool));
                    if (pi != null)
                    {
                        return (bool)pi.GetValue(this.Controls[0], null);
                    }
                    else
                    {
                        pi = this.Controls[0].GetType().GetProperty("Enabled", typeof(bool));
                        if (pi != null)
                        {
                            return !(bool)pi.GetValue(this.Controls[0], null);
                        }
                    }
                }

                return this._ReadOnly;
            }
            set
            {
                //EnsureChildControls();
                if (this.Controls.Count > 0)
                {
                    SetControlProperty(this.Controls[0], "ReadOnly", value);   
                }
                this._ReadOnly = value;
                ViewState.SetItemDirty("ReadOnly", true);
                
            }
        }

        private bool _SearchMode = false;
        public bool SearchMode
        {
            get
            {
                //EnsureChildControls();
                if (this.Controls.Count > 0)
                {
                    System.Reflection.PropertyInfo pi = this.Controls[0].GetType().GetProperty("SearchMode", typeof(bool));
                    if (pi != null)
                    {
                        return (bool)pi.GetValue(this.Controls[0], null);
                    }
                }
                return this._SearchMode;
            }
            set
            {
                //EnsureChildControls();
                if (this.Controls.Count > 0)
                {
                    SetControlProperty(this.Controls[0], "SearchMode", value);
                }
                this._SearchMode = value;
                ViewState.SetItemDirty("SearchMode", true);

            }
        }

        private bool _ViewMode = false;
        public bool ViewMode
        {
            get
            {
                //EnsureChildControls();
                if (this.Controls.Count > 0)
                {
                    System.Reflection.PropertyInfo pi = this.Controls[0].GetType().GetProperty("ViewMode", typeof(bool));
                    if (pi != null)
                    {
                        return (bool)pi.GetValue(this.Controls[0], null);
                    }
                }
                return this._ViewMode;
            }
            set
            {
                //EnsureChildControls();
                if (this.Controls.Count > 0)
                {
                    SetControlProperty(this.Controls[0], "ViewMode", value);
                }
                this._ViewMode = value;
                ViewState.SetItemDirty("ViewMode", true);

            }
        }

        private string _CssClass = String.Empty;
        public override string CssClass
        {
            get
            {
                if (this.Controls.Count > 0)
                {
                    System.Reflection.PropertyInfo pi = this.Controls[0].GetType().GetProperty("CssClass", typeof(string));
                    if (pi != null)
                    {
                        return (string)pi.GetValue(this.Controls[0], null);
                    }
                }
                return this._CssClass;
            }
            set
            {
                //EnsureChildControls();
                if (this.Controls.Count > 0)
                {
                    SetControlProperty(this.Controls[0], "CssClass", value);
                }
                this._CssClass = value;
                ViewState.SetItemDirty("CssClass", true);

            }
        }


        private bool _Validate = false;
        public bool Validate
        {
            get
            {
                //EnsureChildControls();
                if (this.Controls.Count > 0)
                {
                    System.Reflection.PropertyInfo pi = this.Controls[0].GetType().GetProperty("Validate", typeof(bool));
                    if (pi != null)
                    {
                        return (bool)pi.GetValue(this.Controls[0], null);
                    }
                }

                return this._Validate;
            }
            set
            {
                //EnsureChildControls();
                if (this.Controls.Count > 0)
                {
                    SetControlProperty(this.Controls[0], "Validate", value);
                }
                this._Validate = value;
                ViewState.SetItemDirty("Validate", true);
                
            }
        }

        private string _Value = String.Empty;
        [Bindable(true)]
        public String Value
        {
            get
            {
                //EnsureChildControls();
                if (this.Controls.Count > 0)
                {

                    System.Reflection.PropertyInfo pi = this.Controls[0].GetType().GetProperty("SelectedValue", typeof(String));
                    if (pi != null)
                    {
                        return (String)pi.GetValue(this.Controls[0], null);
                    }
                    else
                    {
                        pi = this.Controls[0].GetType().GetProperty("Value", typeof(String));
                        if (pi != null)
                        {
                            return (String)pi.GetValue(this.Controls[0], null);
                        }
                        else
                        {
                            pi = this.Controls[0].GetType().GetProperty("SelectedDate", typeof(DateTime));
                            if (pi != null)
                            {
                                DateTime dt = (DateTime)pi.GetValue(this.Controls[0], null);
                                if (dt != DateTime.MinValue)
                                {
                                    return dt.ToShortDateString();
                                }
                                else
                                {
                                    return string.Empty;
                                }
                            }
                            else
                            {
                                pi = this.Controls[0].GetType().GetProperty("Checked", typeof(bool));
                                if (pi != null)
                                {
                                    return (bool)pi.GetValue(this.Controls[0], null) == true ? Boolean.TrueString : Boolean.FalseString;
                                }
                                else
                                {
                                    pi = this.Controls[0].GetType().GetProperty("Id_HiddenField", typeof(String));
                                    if (pi != null)
                                    {
                                        return (String)pi.GetValue(this.Controls[0], null);
                                    }
                                    else
                                    {
                                        pi = this.Controls[0].GetType().GetProperty("Text", typeof(String));
                                        if (pi != null)
                                        {
                                            return (String)pi.GetValue(this.Controls[0], null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return this._Value;

            }
            set
            {
                EnsureChildControls();
                if (this.Controls.Count > 0)
                {
                    SetControlProperty(this.Controls[0], "Value", value);
                }
                this._Value = value;
                ViewState.SetItemDirty("Value", true);
            }
        }

        private String _DefaultControlTypeSource = typeof(TextBox).AssemblyQualifiedName;
        public String DefaultControlTypeSource
        {
            get { return _DefaultControlTypeSource; }
            set { _DefaultControlTypeSource = value; }
        }

        // BLG_608
        private bool _Ismetlodo = false;
        public bool Ismetlodo
        {
            get
            {
                //EnsureChildControls();
                if (this.Controls.Count > 0)
                {
                    System.Reflection.PropertyInfo pi = this.Controls[0].GetType().GetProperty("Ismetlodo", typeof(bool));
                    if (pi != null)
                    {
                        return (bool)pi.GetValue(this.Controls[0], null);
                    }
                }

                return this._Ismetlodo;
            }
            set
            {
                //EnsureChildControls();
                if (this.Controls.Count > 0)
                {
                    SetControlProperty(this.Controls[0], "Ismetlodo", value);
                }
                this._Ismetlodo = value;
                ViewState.SetItemDirty("Ismetlodo", true);

            }
        }
        // ezt most csak lokálisan használjuk
        private Type DefaultControlType
        {
            get
            {
                if (!String.IsNullOrEmpty(_DefaultControlTypeSource))
                {
                    return Type.GetType(_DefaultControlTypeSource);
                }
                return null;
            }
        }

        private String _ControlTypeName = null;
        [Bindable(true)]
        public String ControlTypeName
        {
            get
            {
                EnsureChildControls();
                if (_ControlType != null)
                {
                    return _ControlType.Name;
                }
                return "";
            }
            //set { _ControlTypeName = value; }
        }

        private Type _ControlType = null;
        [Bindable(true)]
        public Type ControlType
        {
            get {
                if (this.Controls.Count > 0)
                {
                    return this.Controls[0].GetType();
                }
                else
                {
                    return _ControlType;
                }
            }
            set
            {
                _ControlType = value;
                ViewState.SetItemDirty("_ControlType", true);
            }
        }

        private String _ControlTypeSource = null;
        [Bindable(true)]
        public String ControlTypeSource
        {
            get { return String.IsNullOrEmpty(_ControlTypeSource) ? _DefaultControlTypeSource : _ControlTypeSource; }
            set
            {

                //if (value != _ControlTypeSource)
                //{
                    System.Diagnostics.Debug.WriteLine(String.Format("Setting _ControlTypeSource: {0} : ViewState: {1} Old: {2}", value, (string)ViewState["_ControlTypeSource"], this._ControlTypeSource));
                    _ControlTypeSource = value;

                    if (this.Controls.Count > 0)
                    {
                        System.Diagnostics.Debug.WriteLine("Calling CreateChildControls...");
                        this.CreateChildControls();
                    }
                    ViewState.SetItemDirty("_ControlTypeSource", true);
                //}
            }
        }

        public System.Web.UI.Control Control
        {
            get {
                //EnsureChildControls();
                return this.Controls.Count > 0 ? this.Controls[0] : null;
            }
        }

        public String Text
        {
            get
            {
                //EnsureChildControls();
                if (this.Controls.Count > 0)
                {
                    System.Reflection.PropertyInfo pi = this.Controls[0].GetType().GetProperty("SelectedDateText", typeof(string));
                    if (pi != null)
                    {
                        return (string)pi.GetValue(this.Controls[0], null);
                    }
                    else
                    {
                        pi = this.Controls[0].GetType().GetProperty("Text", typeof(string));
                        if (pi != null)
                        {
                            return (string)pi.GetValue(this.Controls[0], null);
                        }
                        else
                        {
                            pi = this.Controls[0].GetType().GetProperty("SelectedText", typeof(string));
                            if (pi != null)
                            {
                                return (string)pi.GetValue(this.Controls[0], null);
                            }
                            else
                            {
                                pi = this.Controls[0].GetType().GetProperty("SelectedItem", typeof(ListItem));
                                if (pi != null)
                                {
                                    return ((ListItem)pi.GetValue(this.Controls[0], null)).Text;
                                }

                            }
                        }
                    }
                }
                return this.Value;
            }
        }

        //public DynamicValueControl()
        //{
        //    //
        //    // TODO: Add constructor logic here
        //    //
        //}

        public override void DataBind()
        {
            base.OnDataBinding(EventArgs.Empty);

            // ...
        }

        protected override void CreateChildControls()
        {
            this.Controls.Clear();

            System.Web.UI.Control ctrl = null;

            System.Diagnostics.Debug.WriteLine(String.Format("Creating child control: {0} : ViewState: {1} : this.CssClass: {2}", ControlTypeSource, (string)ViewState["_ControlTypeSource"], this.CssClass));
            try
            {
                if (!String.IsNullOrEmpty(ControlTypeSource))
                {
                    string typeSource = String.Empty;
                    string strBaseAssemblyName = String.Empty;

                    string[] arrayTypeSource = ControlTypeSource.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    if (arrayTypeSource.Length > 0)
                    {
                        typeSource = arrayTypeSource[0];
                        if (arrayTypeSource.Length > 1)
                        {
                            strBaseAssemblyName = arrayTypeSource[1];
                        }
                    }

                    Type typefromsource = Type.GetType(typeSource);
                    if (typefromsource != null)
                    {
                        ctrl = Activator.CreateInstance(typefromsource) as System.Web.UI.Control;
                    }
                    else if (ControlTypeSource.EndsWith(".ascx"))
                    {
                        ctrl = this.Page.LoadControl(typeSource);
                    }
                    else if (!String.IsNullOrEmpty(strBaseAssemblyName))
                    {
                        System.Reflection.Assembly assembly = Array.Find<System.Reflection.Assembly>(System.AppDomain.CurrentDomain.GetAssemblies(),
                            delegate (System.Reflection.Assembly a) { return (a.FullName.Split(new char[] { ',' })[0] == strBaseAssemblyName); }
                            );

                        if (assembly != null)
                        {
                            String strTypeName = String.Format("{0},{1}", typeSource, assembly.FullName);

                            Type typefromassembly = Type.GetType(strTypeName);
                            if (typefromassembly != null)
                            {
                                ctrl = Activator.CreateInstance(typefromassembly) as System.Web.UI.Control;
                            }
                        }
                    }
                }
                else if (ControlType != null)
                {
                    ctrl = Activator.CreateInstance(ControlType) as System.Web.UI.Control;
                }
            }
            catch (Exception) { }

            // default control, ha eddig semmi nem működött...
            if (ctrl == null)
            {
                if (DefaultControlType != null)
                {
                    ctrl = Activator.CreateInstance(DefaultControlType) as System.Web.UI.Control;
                }
            }

            if (ctrl != null)
            {
                ControlType = ctrl.GetType();

                SetControlProperty(ctrl, "CssClass", this.CssClass);
                SetControlProperty(ctrl, "SearchMode", this.SearchMode);
                SetControlProperty(ctrl, "ViewMode", this.ViewMode);
                SetControlProperty(ctrl, "Visible", this.Visible);
                SetControlProperty(ctrl, "ReadOnly", this.ReadOnly);
                SetControlProperty(ctrl, "Validate", this.Validate);
                SetControlProperty(ctrl, "Value", this.Value);
                // BLG_608
                SetControlProperty(ctrl, "Ismetlodo", this.Ismetlodo);

                //ctrl.EnableViewState = false;
                ctrl.ID = String.IsNullOrEmpty(this.ID) ? "DVC_CONTROL" : this.ID + "_Control";
                this.Controls.Add(ctrl);

                this.ChildControlsCreated = true;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            // ...
            base.OnPreRender(e);
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            // ...
            base.Render(writer);
        }


        #region ViewState

        protected override void LoadViewState(object savedState)
        {
            System.Diagnostics.Debug.WriteLine("Loading ViewState...");

            if (savedState != null)
            {
                System.Diagnostics.Debug.WriteLine("SavedState is not null...");
                base.LoadViewState(savedState);

                if (ViewState != null)
                {
                    System.Diagnostics.Debug.WriteLine("ViewState is not null...");
                    System.Collections.IDictionary viewStateAsIDictionary = (System.Collections.IDictionary)ViewState;

                    if (viewStateAsIDictionary.Contains("_ControlTypeSource"))
                    {
                        _ControlTypeSource = (string)ViewState["_ControlTypeSource"];
                        System.Diagnostics.Debug.WriteLine(String.Format("Loading ViewState _ControlTypeSource: {0}", _ControlTypeSource));

                    }

                    if (viewStateAsIDictionary.Contains("ReadOnly"))
                    {
                        ReadOnly = (bool)ViewState["ReadOnly"];
                    }

                    if (viewStateAsIDictionary.Contains("CssClass"))
                    {
                        CssClass = (string)ViewState["CssClass"];
                    }

                    if (viewStateAsIDictionary.Contains("SearchMode"))
                    {
                        SearchMode = (bool)ViewState["SearchMode"];
                    }

                    if (viewStateAsIDictionary.Contains("ViewMode"))
                    {
                        ViewMode = (bool)ViewState["ViewMode"];
                    }

                    if (viewStateAsIDictionary.Contains("Validate"))
                    {
                        Validate = (bool)ViewState["Validate"];
                    }

                    if (viewStateAsIDictionary.Contains("Value"))
                    {
                        Value = (string)ViewState["Value"];
                    }

                    if (viewStateAsIDictionary.Contains("_ControlType"))
                    {
                        _ControlType = (Type)ViewState["_ControlType"];
                    }
                    // BLG_608
                    if (viewStateAsIDictionary.Contains("Ismetlodo"))
                    {
                        Ismetlodo = (bool)ViewState["Ismetlodo"];
                    }
                }
            }
            else
            {
                base.LoadViewState(null);
            }
        }

        protected override object SaveViewState()
        {
            System.Collections.IDictionary viewStateAsIDictionary = (System.Collections.IDictionary)ViewState;
            System.Diagnostics.Debug.WriteLine("Saving ViewState...");
            if (!viewStateAsIDictionary.Contains("_ControlTypeSource") || ViewState.IsItemDirty("_ControlTypeSource"))
            {
                System.Diagnostics.Debug.WriteLine(String.Format("Saving ViewState _ControlTypeSource: {0}", _ControlTypeSource));
                ViewState["_ControlTypeSource"] = _ControlTypeSource;
            }

            if (!viewStateAsIDictionary.Contains("ReadOnly") || ViewState.IsItemDirty("ReadOnly"))
            {
                ViewState["ReadOnly"] = ReadOnly;
            }

            if (!viewStateAsIDictionary.Contains("CssClass") || ViewState.IsItemDirty("CssClass"))
            {
                ViewState["CssClass"] = CssClass;
            }

            if (!viewStateAsIDictionary.Contains("SearchMode") || ViewState.IsItemDirty("SearchMode"))
            {
                ViewState["SearchMode"] = SearchMode;
            }

            if (!viewStateAsIDictionary.Contains("ViewMode") || ViewState.IsItemDirty("ViewMode"))
            {
                ViewState["ViewMode"] = ViewMode;
            }

            if (!viewStateAsIDictionary.Contains("Validate") || ViewState.IsItemDirty("Validate"))
            {
                ViewState["Validate"] = Validate;
            }

            if (!viewStateAsIDictionary.Contains("Value") || ViewState.IsItemDirty("Value"))
            {
                ViewState["Value"] = Value;
            }

            if (!viewStateAsIDictionary.Contains("_ControlType") || ViewState.IsItemDirty("_ControlType"))
            {
                ViewState["_ControlType"] = _ControlType;
            }
            // BLG_608
            if (!viewStateAsIDictionary.Contains("Ismetlodo") || ViewState.IsItemDirty("Ismetlodo"))
            {
                ViewState["Ismetlodo"] = Ismetlodo;
            }

            return base.SaveViewState();
        }

        #endregion ViewState
    }
}
