﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;

[assembly: System.Web.UI.WebResource("Contentum.eUIControls.JavaScripts.ListSearchExtender.js", "text/javascript")]

namespace Contentum.eUIControls
{
    public class eDropDownList : System.Web.UI.WebControls.DropDownList
    {
        private eUIControls.eListSearchExtender listSearchExtender = new eListSearchExtender();
        private bool listSearchExtenderEnabled = true;

        #region ScriptManager
        private bool isScriptManagerSearched = false;
        private ScriptManager sm = null;
        private ScriptManager _ScriptManager
        {
            get
            {
                if (Page == null) return null;
                if (isScriptManagerSearched)
                    return this.sm;
                this.sm = ScriptManager.GetCurrent(Page);
                isScriptManagerSearched = true;
                return this.sm;
            }
        }
        private bool IsScriptManager
        {
            get
            {
                if (_ScriptManager == null)
                    return false;
                return true;
            }
        }
        #endregion

        #region DeafultValues
        private const string AppSettingKey_ListSearchExtenderEnabled = "ListSearchExtenderEnabled";
        private const string AppSettingKey_ListSearchExtenderQueryTimeout = "ListSearchExtenderQueryTimeout";

        private const string DEF_ListSearchExtender_PromptCssClass = "ListSearchExtenderPrompt";
        private const string DEF_ListSearchExtender_PromptText = "";
        private const AjaxControlToolkit.ListSearchPromptPosition DEF_ListSearchExtender_PromptPosition = AjaxControlToolkit.ListSearchPromptPosition.Top;
        private const AjaxControlToolkit.ListSearchQueryPattern DEF_ListSearchExtender_QueryPattern = AjaxControlToolkit.ListSearchQueryPattern.Contains;
        private const bool DEF_ListSearchExtender_IsSorted = false;
        private const bool DEF_ListSearchExtender_RaiseImmediateOnChange = false;
        private const int _DEF_ListSearchExtender_QueryTimeout = 2000;
        private int DEF_ListSearchExtender_QueryTimeout
        {
            get
            {
                int timeout = _DEF_ListSearchExtender_QueryTimeout;
                string timeoutValue = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(AppSettingKey_ListSearchExtenderQueryTimeout);
                if (!String.IsNullOrEmpty(timeoutValue))
                {
                    if (Int32.TryParse(timeoutValue, out timeout))
                    {
                    }
                    else
                    {
                        timeout = _DEF_ListSearchExtender_QueryTimeout;
                    }
                }
                return timeout; 
            }
        }
        private const bool _DEF_ListSearchExtender_Enabled = true;
        private bool DEF_ListSearchExtender_Enabled
        {
            get 
            {
                bool enabled = _DEF_ListSearchExtender_Enabled;
                string enabledValue = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(AppSettingKey_ListSearchExtenderEnabled);
                if (!String.IsNullOrEmpty(enabledValue))
                {
                    if (Boolean.TryParse(enabledValue, out enabled))
                    {
                    }
                    else
                    {
                        enabled = _DEF_ListSearchExtender_Enabled;
                    }
                }
                return enabled; 
            }
        } 

        #endregion

        #region Public Properties

        public AjaxControlToolkit.ListSearchExtender ListSearchExtender
        {
            get { return this.listSearchExtender; }
        }

        public string LSEX_PromptCssClass
        {
            get
            {
                return this.listSearchExtender.PromptCssClass;
            }
            set
            {
                this.listSearchExtender.PromptCssClass = value;
            }
        }

        public string LSEX_PromptText
        {
            get
            {
                return this.listSearchExtender.PromptText;
            }
            set
            {
                this.listSearchExtender.PromptText = value;
            }
        }

        public bool LSEX_IsSorted
        {
            get
            {
                return this.listSearchExtender.IsSorted;
            }
            set
            {
                this.listSearchExtender.IsSorted = value;
            }
        }

        public bool LSEX_Enabled
        {
            get
            {
                return this.listSearchExtender.Enabled;
            }
            set
            {
                listSearchExtenderEnabled = value;
                this.listSearchExtender.Enabled = value;
            }
        }

        public bool LSEX_RaiseImmediateOnChange
        {
            get
            {
                return this.listSearchExtender.RaiseImmediateOnChange;
            }
            set
            {
                this.listSearchExtender.RaiseImmediateOnChange = value;
            }
        }

        public int LSEX_QueryTimeout
        {
            get
            {
                return this.listSearchExtender.QueryTimeout;
            }
            set
            {
                this.listSearchExtender.QueryTimeout = value;
            }
        }

        public AjaxControlToolkit.ListSearchPromptPosition LSEX_PromptPosition
        {
            get
            {
                return this.listSearchExtender.PromptPosition;
            }
            set
            {
                this.listSearchExtender.PromptPosition = value;
            }
        }

        public AjaxControlToolkit.ListSearchQueryPattern LSEX_QueryPattern
        {
            get
            {
                return this.listSearchExtender.QueryPattern;
            }
            set
            {
                this.listSearchExtender.QueryPattern = value;
            }
        }

        public string LSEX_ID
        {
            get
            {
                return this.listSearchExtender.ID;
            }
            set
            {
                this.listSearchExtender.ID = value;
            }
        }

        public string LSEX_ClientID
        {
            get
            {
                return this.listSearchExtender.ClientID;
            }
        }

        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                if (listSearchExtenderEnabled)
                    this.listSearchExtender.Enabled = value;
                base.Enabled = value;
            }
        }

        [DefaultValue(false)]
        public bool ReadOnly
        {
            get
            {
                object o = ViewState["ReadOnly"];
                if (o != null)
                    return (bool)ViewState["ReadOnly"];
                return false;
            }
            set
            {
                if (ReadOnly != value)
                {
                    ViewState["ReadOnly"] = value;
                    this.Enabled = !value;
                    if (value)
                    {
                        this.Attributes.Add("readonly", "readonly");
                        Utility.AddCssClass(this, Utility.ReadOnlyStyle);
                    }
                    else
                    {
                        this.Attributes.Remove("readonly");
                        Utility.RemoveCssClass(this, Utility.ReadOnlyStyle);
                    }
                }
            }
        }

        public string ReadOnlyClientID
        {
            get
            {
                if (this.ReadOnly)
                    return this.ClientID + "_text";
                return this.ClientID;
            }
        }

        #endregion

        public eDropDownList()
            : base()
        {
            this.listSearchExtender.PromptText = DEF_ListSearchExtender_PromptText;
            this.listSearchExtender.PromptPosition = DEF_ListSearchExtender_PromptPosition;
            this.listSearchExtender.PromptCssClass = DEF_ListSearchExtender_PromptCssClass;
            this.listSearchExtender.QueryPattern = DEF_ListSearchExtender_QueryPattern;
            this.listSearchExtender.IsSorted = DEF_ListSearchExtender_IsSorted;
            this.LSEX_Enabled = DEF_ListSearchExtender_Enabled;
            this.listSearchExtender.RaiseImmediateOnChange = DEF_ListSearchExtender_RaiseImmediateOnChange;
            this.listSearchExtender.QueryTimeout = DEF_ListSearchExtender_QueryTimeout;
        }

        protected override void OnInit(EventArgs e)
        {
            this.InitComponent();
            base.OnInit(e);
        }

        protected void InitComponent()
        {
            if (this.IsScriptManager)
            {
                Utility.RegisterListSearchExtenderScript(Page);
                this.listSearchExtender.TargetControlID = this.ID;
                this.listSearchExtender.ID = this.ID + "_ListSearchExtender";
                this.Parent.Init += new EventHandler(Parent_Init);
            }
        }

        protected void Parent_Init(object sender, EventArgs e)
        {
            if (this.IsScriptManager)
            {
                this.Parent.Controls.Add(listSearchExtender);
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (ReadOnly && this.IsScriptManager)
            {

                Utility.SetDropDrownListReadOnly(this);
            }
            base.Render(writer);
        }
    }
}
