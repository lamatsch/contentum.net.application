﻿using System;
using System.Collections.Generic;
using System.Text;
using AjaxControlToolkit;
using System.ComponentModel;

namespace Contentum.eUIControls
{
    public class eListSearchExtender: AjaxControlToolkit.ListSearchExtender
    {
        [ExtenderControlProperty]
        [ClientPropertyName("raiseImmediateOnChange")]
        [DefaultValue(false)]
        public bool RaiseImmediateOnChange
        {
            get { return GetPropertyValue<bool>("RaiseImmediateOnChange", false); }
            set { SetPropertyValue<bool>("RaiseImmediateOnChange", value); }
        }
    }
}
