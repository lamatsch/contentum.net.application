﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />

Type.registerNamespace("Utility");

Utility.ModifyListSearchExtender = function()
{
    if(typeof(AjaxControlToolkit) != 'undefined' &&  AjaxControlToolkit.ListSearchBehavior != undefined)
    {
        AjaxControlToolkit.ListSearchBehavior.prototype._doLinearSearch = Utility.doLinearSearch;
        AjaxControlToolkit.ListSearchBehavior.prototype._originalOnKeyPress = AjaxControlToolkit.ListSearchBehavior.prototype._onKeyPress;
        AjaxControlToolkit.ListSearchBehavior.prototype._onKeyPress = Utility.onKeyPress;
        AjaxControlToolkit.ListSearchBehavior.prototype._originalOnFocus = AjaxControlToolkit.ListSearchBehavior.prototype._onFocus;
        AjaxControlToolkit.ListSearchBehavior.prototype._onFocus = Utility.onFocus;
    }
}

Utility.doLinearSearch = function(options, value, left, right)
{
    var resultIndex = -1;
    if(this._queryPattern == AjaxControlToolkit.ListSearchQueryPattern.StartsWith)
    {     
        for(var i = left; i <= right; i++) 
        {
            if(options[i].text.toLowerCase().startsWith(value)) 
            {
                return i;
            }
        }
    }
    else if (this._queryPattern == AjaxControlToolkit.ListSearchQueryPattern.Contains) 
    {   
        var minIndex = -1;
        for(var i = left; i <= right; i++) 
        {
            var index = options[i].text.toLowerCase().indexOf(value);
            if(index >= 0 && (index < minIndex || minIndex == -1))              
            {
                resultIndex = i;
                minIndex = index;
                if (minIndex == 0)
                {
                    return resultIndex;
                }
            }
        }
    }

    return resultIndex;
}

Utility.onFocus = function(e)
{
   var element = this.get_element();
   this._focusIndex = element.selectedIndex;
}

Utility.onKeyPress = function(e)
{    
    if(this._isNormalChar(e))
    {
        if(!this._promptDiv)
        {
            this._originalOnFocus(e);
        }
    }
    
    if(this._showingPromptText) 
    {
        var promptDiv = this._promptDiv;
        if (promptDiv)
            promptDiv.innerHTML = '';
        this._searchText = '';
        this._showingPromptText = false;
        var element = this.get_element();
        if(element)
            this._binarySearch = this._checkIfSorted(element.options);   // Delayed until required
    }     
        
    this._originalOnKeyPress(e);
}

Sys.Application.add_init(Utility.ModifyListSearchExtender)

Utility.Set_DropDownListReadOnly = function(clientId)
{
    var drop = $get(clientId);
    if(drop && drop.attributes.getNamedItem('readonly'))
    {
       var par = drop.parentNode; 
       var bounds= Sys.UI.DomElement.getBounds(drop);
       var text = document.createElement('input'); 
       text.type = 'text';
       text.id = drop.id + '_text';
       if(drop.selectedIndex > -1) text.value = drop.options[drop.selectedIndex].text;
       text.className = drop.className;
       var width = 0; 
       if(drop.offsetWidth > 0) {width = drop.offsetWidth;}
       else
       {
          if(drop.currentStyle) width = drop.currentStyle.width; 
          else{if(window.getComputedStyle) width = window.getComputedStyle(drop,null).width;}
       }
       width = parseInt(width); 
       if(!isNaN(width) && width > 0) text.style.width = width - 5 + 'px'; 
       text.readOnly = true; 
       drop.style.display = 'none';
       par.insertBefore(text,drop.nextSibling);
    }
}

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();