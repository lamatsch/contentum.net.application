﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Contentum.eUIControls
{
    internal class Utility
    {
        public const string ReadOnlyStyle = "ReadOnlyWebControl";

        public static void AddCssClass(WebControl wc, string className)
        {
            if (!wc.CssClass.Contains(className))
            {
                if (!String.IsNullOrEmpty(wc.CssClass))
                {
                    wc.CssClass += " ";
                }

                wc.CssClass += className;
            }
        }

        public static void RemoveCssClass(WebControl wc, string className)
        {
            if (wc.CssClass.Contains(className))
            {
                string spClassName = " " + className;
                if (wc.CssClass.Contains(spClassName))
                {
                    wc.CssClass = wc.CssClass.Replace(spClassName, "");
                }
                else
                {
                    wc.CssClass = wc.CssClass.Replace(className, "");
                }

                wc.CssClass = wc.CssClass.Trim();
            }
        }

        public static void RegisterListSearchExtenderScript(Page page)
        {
            ScriptManager sm = ScriptManager.GetCurrent(page);
            if (sm != null)
            {
                ScriptReference sr = new ScriptReference();
                sr.Assembly = "Contentum.eUIControls";
                sr.Name = "Contentum.eUIControls.JavaScripts.ListSearchExtender.js";

                if (!sm.Scripts.Contains(sr))
                    sm.Scripts.Add(sr);
            }
            
        }

        public static void SetDropDrownListReadOnly(DropDownList ddlist)
        {
            string js = "Utility.Set_DropDownListReadOnly('" + ddlist.ClientID + "');";
            ScriptManager.RegisterStartupScript(ddlist, ddlist.GetType(),"replace_" + ddlist.ClientID, js, true);
        }
    }

    public enum FormPanelRenderMode
    {
        FormPanel,
        Panel
    }
}
