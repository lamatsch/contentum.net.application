using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;

namespace Contentum.eUIControls
{
    [DefaultProperty("Header")]
    [ToolboxData("<{0}:eErrorPanel runat=server></{0}:eErrorPanel>")]
    [Designer(typeof(eFormPanelDesigner))]
    public class eErrorPanel : Panel,IPostBackEventHandler
    {
        private Boolean update = false; 
        
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Header
        {
            get
            {
                String s = (String)ViewState["Header"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                    ViewState["Header"] = value;

                    if (!String.IsNullOrEmpty(value)) update = true;

            }
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Body
        {
            get
            {
                String s = (String)ViewState["Body"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["Body"] = value;

                    if (!String.IsNullOrEmpty(value)) update = true;

            }
        }


        public string ErrorCode
        {
            get
            {
                String s = (String)ViewState["ErrorCode"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["ErrorCode"] = value;
            }
        }

        public string ErrorMessage
        {
            get
            {
                String s = (String)ViewState["ErrorMessage"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["ErrorMessage"] = value;
            }
        }

        public string CurrentLogEntry
        {
            get
            {
                String s = (String)ViewState["CurrentLogEntry"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["CurrentLogEntry"] = value;
            }
        }

        public bool IsWarning
        {
            get
            {
                object s = ViewState["IsWarning"];
                return ((s == null) ? false : (bool)s);
            }

            set
            {
                ViewState["IsWarning"] = value;
            }
        }

        private string EmailButtonVisible
        {
            get
            {
                if (IsWarning)
                {
                    return "none";
                }
                else
                {
                    return "block";
                }
            }
        }

        private string HeaderStyle
        {
            get
            {
                if (IsWarning)
                {
                   return "warningHeader";
                }
                else
                {
                    return "hibaHeader";
                }
            }
        }

        private string BodyStyle
        {
            get
            {
                if (IsWarning)
                {
                    return "warningBody";
                }
                else
                {
                    return "hibaBody";
                }
            }
        }

        private string ImageSource
        {
            get
            {
                if (IsWarning)
                {
                    return "images/hu/design/warning.jpg";
                }
                else
                {
                    return "images/hu/design/hiba.jpg";
                }
            }
        }

        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            if (writer == null) return;            
            base.RenderBeginTag(writer);
            string html = "<table class=\"mrUrlap\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color: rgb(233, 233, 233); width: 100%;\">" +
                        "<tr>" +
                            "<td style=\"width: 8px;\"><img src=\"images/hu/design/hiba_bal_fel.jpg\" alt=\" \" /></td>" +
                            "<td style=\"border-top: 1px solid rgb(192, 192, 192);\"><img src=\"images/hu/design/spacertrans.gif\" alt=\" \" /></td>" +
                            "<td style=\"width: 8px;\"><img src=\"images/hu/design/hiba_jobb_fel.jpg\" alt=\" \" /></td>" +
                        "</tr>" +
                        "</table>" +
                        "<table class=\"mrUrlap\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\">" +
                        "<tr>" +
                        "	<td style=\"border-left: 1px solid rgb(192, 192, 192); width: 2px;\"><img src=\"images/hu/design/spacertrans.gif\" alt=\" \" /></td>" +
                        "	<td style=\"background-repeat: repeat-x; background-color: rgb(233, 233, 233);	text-align: center;\">";                                   
            writer.Write(html);
        }

        public override void RenderEndTag(HtmlTextWriter writer)
        {
            if (writer == null) return;
            string html = "</td>" +
                            "<td style=\"border-right: 1px solid rgb(192, 192, 192); width: 2px;\"><img src=\"images/hu/design/spacertrans.gif\" alt=\" \" /></td>" +
                        "</tr>" +
                        "</table>" +
                        "<table class=\"mrUrlap\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\">" +
                        "<tr>" +
                            "<td style=\"width: 8px;\"><img src=\"images/hu/design/hiba_bal_al.jpg\" alt=\" \" /></td>" +
                            "<td style=\"background-image: url('images/hu/design/box_al.jpg'); background-repeat: repeat-x;\"><img src=\"images/hu/design/spacertrans.gif\" alt=\" \" /></td>" +
                            "<td style=\"width: 8px;\"><img src=\"images/hu/design/hiba_jobb_al.jpg\" alt=\" \" /></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td style=\"height: 5px;\" colspan=\"3\"></td>" +
                        "</tr>" +
                        "</table>";
            writer.Write(html);

            base.RenderEndTag(writer);

        }

        //protected override void CreateChildControls()
        //    {
        //        base.CreateChildControls();
        //    }

        public class eFormPanelDesigner : ControlDesigner
        {
            public override string GetDesignTimeHtml()
            {
                return base.GetDesignTimeHtml();
            }
            //public override string GetDesignTimeHtml(DesignerRegionCollection regions)
            //{            
            //    return base.GetDesignTimeHtml(regions);
            //}        
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (writer == null) return;
            string html = "<table class=\"mrHiba\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%\">"
            + "        <tr>"
            //+ "            <td style=\"	border-left: 1px solid rgb(192, 192, 192);width: 2px;\">"
            //+ "                <img runat=\"server\" id=\"image4\" src=\"images/hu/design/spacertrans.gif\" alt=\" \" /></td>"
            + "            <td class=\"hibaHatter\" styl>"
            + "                <div class=\"ErrorPanelEmailButton\">"
            + "                     <input type=\"image\" id=\"errorPanelEmailButton\" onmouseover=\"swapByName(this.id,'email_send.jpg')\" onmouseout=\"swapByName(this.id,'email_send_keret_nelkul.jpg')\" src=\"images/hu/ikon/email_send_keret_nelkul.jpg\" "
            + "                         alt=\"Hiba elküldése\" style=\"border-width:0px;display:" + EmailButtonVisible + "\" onclick=\"javascript:this.disabled = true;this.src='images/hu/egyeb/activity_indicator.gif';" + Page.ClientScript.GetPostBackEventReference(this, "SendMail") + "\" />"
            + "                </div>" 
            + "                <table>"
            + "                    <tr>"
            + "                        <td align=\"center\" style=\"width: 80px; vertical-align: middle;\">"
            + "                            <img runat=\"server\" id=\"image5\" src=\"" + ImageSource + "\" alt=\" \" />"
            + "                        </td>"
            + "                        <td class=\"" + HeaderStyle + "\" style=\"width:auto; vertical-align: middle;\">"
            + "                            &nbsp;" + Header
            + "                            </td>"
            + "                    </tr>"
            + "                    <tr>"
            + "                        <td align=\"center\" style=\"width: 80px; vertical-align: middle;\">"
            + "                        </td>"
            + "                        <td class=\"" + BodyStyle + "\" style=\"width:auto; vertical-align: middle;\">"
            + "                            &nbsp;" + Body 
            + "                            </td>"
            + "                    </tr>"
            + "                </table>"
            + "            </td>"
            //+ "            <td  style=\"border-right: 1px solid rgb(192, 192, 192);width: 2px;\">"
            //+ "                <img runat=\"server\" id=\"image6\" src=\"images/hu/design/spacertrans.gif\" alt=\" \" /></td>"
            + "        </tr>"
            + "    </table>";
            writer.Write(html);
            //output.Write(output);
        }



        public event CommandEventHandler EmailButtonClick;

        protected void OnEmailButtonClick(CommandEventArgs e)
        {
            if (EmailButtonClick != null)
                EmailButtonClick(this, e);
        }

        #region IPostBackEventHandler Members

        public void RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument == "SendMail")
            {
                string errorMesage = (ErrorMessage == String.Empty) ? Body : ErrorMessage;
                string argument = ErrorCode + ";" + errorMesage + ";" + CurrentLogEntry;
                CommandEventArgs e = new CommandEventArgs(eventArgument, argument);
                OnEmailButtonClick(e);
            }
        }

        #endregion
    }


}
