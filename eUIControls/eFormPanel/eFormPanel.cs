using System;
using System.ComponentModel;
using System.Drawing;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;

[assembly: CLSCompliant(true)]
namespace Contentum.eUIControls
{
/// <summary>
/// Summary description for eFormPanel
/// </summary>
[
AspNetHostingPermission(SecurityAction.Demand,
    Level = AspNetHostingPermissionLevel.Minimal),
AspNetHostingPermission(SecurityAction.InheritanceDemand,
    Level = AspNetHostingPermissionLevel.Minimal),
ToolboxData("<{0}:eFormPanel runat=\"server\"> </{0}:eFormPanel>"),
PersistChildren(true), 
ParseChildren(false)
]
    [Designer(typeof(eFormPanelDesigner))]
    public class eFormPanel : Panel
    {

    public FormPanelRenderMode RenderMode
    {
        get
        {
            object o = ViewState["RenderMode"];
            if (o != null)
                return (FormPanelRenderMode)o;
            return FormPanelRenderMode.FormPanel;
        }
        set
        {
            ViewState["RenderMode"] = value;
        }
    }

	public eFormPanel()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public override void  RenderBeginTag(HtmlTextWriter writer)
    {
        if (writer == null) return;
        base.RenderBeginTag(writer);
        if (RenderMode != FormPanelRenderMode.Panel)
        {
            string html = "<table class=\"mrUrlap\" cellspacing=\"0\" cellpadding=\"0\" style=\"width: 100%;\">" +
                        "<tr>" +
                            "<td style=\"width: 8px;\"><img src=\"images/hu/design/box_bal_fel.jpg\" alt=\" \" /></td>" +
                            "<td style=\"border-top: 1px solid rgb(192, 192, 192); background-color: white;\"><img src=\"images/hu/design/spacertrans.gif\" alt=\" \" /></td>" +
                            "<td style=\"width: 8px;\"><img src=\"images/hu/design/box_jobb_fel.jpg\" alt=\" \" /></td>" +
                        "</tr>" +
                        "</table>" +
                        "<table class=\"mrUrlap\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\">" +
                        "<tr>" +
                        "	<td style=\"border-left: 1px solid rgb(192, 192, 192); width: 2px;\"><img src=\"images/hu/design/spacertrans.gif\" alt=\" \" /></td>" +
                        "	<td style=\"background-image: url('images/hu/design/box_gradiens.jpg');	background-repeat: repeat-x; background-color: rgb(233, 233, 233);	text-align: center;\">";
            writer.Write(html);
        }
    }

    public override void  RenderEndTag(HtmlTextWriter writer)
    {
        if (writer == null) return;
        if (RenderMode != FormPanelRenderMode.Panel)
        {
            string html = "</td>" +
                            "<td style=\"border-right: 1px solid rgb(192, 192, 192); width: 2px;\"><img src=\"images/hu/design/spacertrans.gif\" alt=\" \" /></td>" +
                        "</tr>" +
                        "</table>" +
                        "<table class=\"mrUrlap\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\">" +
                        "<tr>" +
                            "<td style=\"width: 8px;\"><img src=\"images/hu/design/box_bal_al.jpg\" alt=\" \" /></td>" +
                            "<td style=\"background-image: url('images/hu/design/box_al.jpg'); background-repeat: repeat-x;\"><img src=\"images/hu/design/spacertrans.gif\" alt=\" \" /></td>" +
                            "<td style=\"width: 8px;\"><img src=\"images/hu/design/box_jobb_al.jpg\" alt=\" \" /></td>" +
                        "</tr>" +
                        "</table>";
            writer.Write(html);
        }

        base.RenderEndTag(writer);
    }

    //protected override void CreateChildControls()
    //    {
    //        base.CreateChildControls();
    //    }

    public class eFormPanelDesigner : ContainerControlDesigner
    {
        public override string GetDesignTimeHtml()
        {
            return base.GetDesignTimeHtml();
        }
        //public override string GetDesignTimeHtml(DesignerRegionCollection regions)
        //{            
        //    return base.GetDesignTimeHtml(regions);
        //}        
    }
}

}