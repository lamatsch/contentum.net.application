﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Transactions;
using UnitTestProject.Models;

namespace UnitTestProject
{
    [TestClass]
    public abstract class ContentumBaseTest
    {
        public ExecParam BaseExecParam { get; set; }

        internal bool NeedInvalidateAll = true;
        private List<Model2Cleanup> model2Invalidate = new List<Model2Cleanup>();
        private TransactionScope scope;

        [TestInitialize]
        public void Setup()
        {
            Console.WriteLine("SETUP START.");
            System.Diagnostics.Debug.WriteLine("SETUP START");

            this.scope = new TransactionScope();
            BaseExecParam = UnitTestHelper.GetExecParam();

            Console.WriteLine("SETUP STOP.");
            System.Diagnostics.Debug.WriteLine("SETUP STOP");
        }

        [TestCleanup]
        public void Cleanup()
        {
            Console.WriteLine("CLEANUP START.");
            System.Diagnostics.Debug.WriteLine("CLEANUP START");

            InvalidateAll();
            this.scope.Dispose();

            Console.WriteLine("CLEANUP STOP.");
            System.Diagnostics.Debug.WriteLine("CLEANUP STOP");
        }

        public void Add2Cleanup(UnitTestConstants.EnumModel2CleanupType type, string identity)
        {
            if (string.IsNullOrEmpty(identity))
                return;

            model2Invalidate.Add(new Model2Cleanup(type, identity));
        }

        private void InvalidateAll()
        {
            if (!NeedInvalidateAll)
                return;

            if (model2Invalidate == null || model2Invalidate.Count < 1)
                return;

            ExecParam execParam = UnitTestHelper.GetExecParam();
            if (execParam == null)
                return;

            Console.WriteLine("INVALIDATE ALL START:");
            System.Diagnostics.Debug.WriteLine("INVALIDATE ALL START:");

            Result result = null;
            for (int i = 0; i < model2Invalidate.Count; i++)
            {
                execParam.Record_Id = model2Invalidate[i].Identity;
                switch (model2Invalidate[i].Type)
                {
                    case UnitTestConstants.EnumModel2CleanupType.Irat:
                        EREC_KuldKuldemenyekService serviceI = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                        if (serviceI == null)
                            continue;
                        result = serviceI.Invalidate(execParam);
                        break;
                    case UnitTestConstants.EnumModel2CleanupType.Ugyirat:
                        EREC_UgyUgyiratokService serviceU = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        if (serviceU == null)
                            continue;
                        result = serviceU.Invalidate(execParam);
                        break;
                    case UnitTestConstants.EnumModel2CleanupType.IratPeldany:
                        EREC_PldIratPeldanyokService serviceP = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                        if (serviceP == null)
                            continue;
                        result = serviceP.Invalidate(execParam);
                        break;
                    case UnitTestConstants.EnumModel2CleanupType.Kuldemeny:
                        EREC_KuldKuldemenyekService serviceK = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                        if (serviceK == null)
                            continue;
                        result = serviceK.Invalidate(execParam);
                        break;
                    default:
                        break;
                }
            }

            Console.WriteLine("INVALIDATE ALL STOP:");
            System.Diagnostics.Debug.WriteLine("INVALIDATE ALL STOP:");
        }
    }
}
