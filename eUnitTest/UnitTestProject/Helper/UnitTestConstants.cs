﻿using System.Configuration;

namespace UnitTestProject
{
    public static class UnitTestConstants
    {
        public static string Note = "UnitTest";

        public static string ErkeztetoHely = ConfigurationManager.AppSettings["ErkeztetoHely"] ?? "T";
        public static string IktatoHely = ConfigurationManager.AppSettings["IktatoHely"] ?? "AX";

        public static string ExecParam_Felhasznalo_Id = ConfigurationManager.AppSettings["ExecParam_Felhasznalo_Id"] ?? "54E861A5-36ED-44CA-BAA7-C287D125B309";//Admin
        public static string ExecParam_FelhasznaloSzervezet_Id = ConfigurationManager.AppSettings["ExecParam_FelhasznaloSzervezet_Id"] ?? "BD00C8D0-CF99-4DFC-8792-33220B7BFCC6"; //Adminisztrátorok

        public static string Szignal_Irat_Felhasznalo_Id = ConfigurationManager.AppSettings["Szignal_Irat_Felhasznalo_Id"] ?? "da12362d-e842-e811-80c7-00155d027ea9";
        public static string Szignal_Irat_Szervezet_Id = ConfigurationManager.AppSettings["Szignal_Irat_Szervezet_Id"] ?? "BD00C8D0-CF99-4DFC-8792-33220B7BFCC6";

        public static string Szignal_Ugyirat_Felhasznalo_Id = ConfigurationManager.AppSettings["Szignal_Ugyirat_Felhasznalo_Id"] ?? "54E861A5-36ED-44CA-BAA7-C287D125B309";// Admin
        public static string Szignal_Ugyirat_Szervezet_Id = ConfigurationManager.AppSettings["Szignal_Ugyirat_Szervezet_Id"] ?? "b84dc50d-1d0e-e811-80c5-00155d027ea9";//	Alkalmazás Fejlesztés és Üzemeltetés Osztály

        public static string eRecordWebSiteUrl = ConfigurationManager.AppSettings["eRecordWebSiteUrl"] ?? "";
        public static string eRecordWebSiteForms = ConfigurationManager.AppSettings["eRecordWebSiteForms"] ?? "";

        public static string eAdminWebSiteUrl = ConfigurationManager.AppSettings["eAdminWebSiteUrl"] ?? "";
        public static string eAdminWebSiteForms = ConfigurationManager.AppSettings["eAdminWebSiteForms"] ?? "";


        public enum EnumModel2CleanupType
        {
            Irat,
            Ugyirat,
            IratPeldany,
            Kuldemeny,
        }

        public static class ErrorMessages
        {
            public static string E50101 = "[50101]";
        }

        public enum ReflectionMethod
        {
            Get,
            GetAll,
            GetAllWithExtension
        }

        public enum FlowNumber : int
        {
            UNTITTEST_SAKKORA_FLOW_0,
            UNTITTEST_SAKKORA_FLOW_1,
            UNTITTEST_SAKKORA_FLOW_2,
            UNTITTEST_SAKKORA_FLOW_3,
            UNTITTEST_SAKKORA_FLOW_4,
            UNTITTEST_SAKKORA_FLOW_UJRAINDITAS,
            UNTITTEST_SAKKORA_FLOW_5,
            UNTITTEST_SAKKORA_FLOW_6,
            UNTITTEST_SAKKORA_FLOW_7,
            UNTITTEST_SAKKORA_FLOW_8,

            UNITTEST_BEJOVOIRATIKTATAS_FLOW_1,
            UNITTEST_BEJOVOIRATIKTATAS_ALSZAMRA_FLOW_1
        }

        /// <summary>
        /// IntezesiIdo
        /// </summary>
        public enum IntezesiIdo : int
        {
            I1 = 1,
            I5 = 5,
            I10 = 10,
            I30 = 30
        }

        public enum StepNumber
        {
            STEP1 = 1,
            STEP2 = 2,
            STEP3 = 3,
            STEP4 = 4,
            STEP5 = 5,
            STEP6 = 6,
            STEP7 = 7,
            STEP8 = 8,
            STEP9 = 9,
            STEP10 = 10,
            STEP11 = 11,
            STEP12 = 12,
            STEP13 = 13,
            STEP14 = 14,
            STEP15 = 15,
        }

        public enum ElteltNapok : int
        {
            MINUS3 = -3,
            MINUS2 = -2,
            MINUS1 = -1,
            ZERO = 0,
            ONE = 1,
            TWO = 2,
            THREE = 3,
            FOUR = 4,
            FIVE = 5,
            SIX = 6,
            SEVEN = 7,
        }
        public enum HatralevoNapok : int
        {
            MINUS3 = -3,
            MINUS2 = -2,
            MINUS1 = -1,
            ZERO = 0,
            ONE = 1,
            TWO = 2,
            THREE = 3,
            FOUR = 4,
            FIVE = 5,
            SIX = 6,
            SEVEN = 7,
            EIGHT = 8,
        }
    }
}
