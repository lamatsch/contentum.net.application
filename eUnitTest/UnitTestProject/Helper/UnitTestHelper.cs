﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Linq;

namespace UnitTestProject
{
    public class UnitTestHelper
    {
        public static ExecParam GetExecParam()
        {
            return new ExecParam()
            {
                Felhasznalo_Id = UnitTestConstants.ExecParam_Felhasznalo_Id,
                FelhasznaloSzervezet_Id = UnitTestConstants.ExecParam_FelhasznaloSzervezet_Id
            };
        }

        public static ExecParam GetExecParamForIratAtvetel()
        {
            return new ExecParam()
            {
                Felhasznalo_Id = UnitTestConstants.Szignal_Irat_Felhasznalo_Id,
                FelhasznaloSzervezet_Id = UnitTestConstants.Szignal_Irat_Szervezet_Id
            };
        }
        public static ExecParam GetExecParamForUgyiratAtvetel()
        {
            return new ExecParam()
            {
                Felhasznalo_Id = UnitTestConstants.Szignal_Ugyirat_Felhasznalo_Id,
                FelhasznaloSzervezet_Id = UnitTestConstants.Szignal_Ugyirat_Szervezet_Id
            };
        }

        public static string GetErkeztetoKonyvId(ExecParam param, string inErkeztetoHely)
        {
            string erkeztetoKonyv_Id = null;
            string erkeztetoHely = inErkeztetoHely;
            if (!string.IsNullOrEmpty(erkeztetoKonyv_Id))
            {
                return erkeztetoKonyv_Id;
            }
            else if (!string.IsNullOrEmpty(erkeztetoHely))
            {
                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam xparamEBeadvGet = param;
                EREC_IraIktatoKonyvekSearch src = new EREC_IraIktatoKonyvekSearch();
                src.Ev.Value = DateTime.Now.Year.ToString();
                src.Ev.Operator = Query.Operators.equals;

                src.MegkulJelzes.Value = erkeztetoHely;
                src.MegkulJelzes.Operator = Query.Operators.equals;

                src.IktatoErkezteto.Value = "E";
                src.IktatoErkezteto.Operator = Query.Operators.equals;
                src.TopRow = 1;

                Result result = service.GetAll(param, src);
                if (!string.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
                if (result.Ds.Tables[0].Rows.Count < 1)
                {
                    throw new Exception(string.Format("Nem található érkeztetőkönvy {0}", erkeztetoHely));
                }
                return result.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            else
            {
                throw new Exception("Érkeztetőkönyv nem található !");
            }
        }
        public static string GetIktatoKonyvId(ExecParam param, string inIktatoHely)
        {
            string iktatoKonyvId = null;
            string Iktatohely = inIktatoHely;
            if (!string.IsNullOrEmpty(iktatoKonyvId))
            {
                return iktatoKonyvId;
            }
            else if (!string.IsNullOrEmpty(Iktatohely))
            {
                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam xparamEBeadvGet = param;
                EREC_IraIktatoKonyvekSearch src = new EREC_IraIktatoKonyvekSearch();
                src.Ev.Value = DateTime.Now.Year.ToString();
                src.Ev.Operator = Query.Operators.equals;

                src.Iktatohely.Value = Iktatohely;
                src.Iktatohely.Operator = Query.Operators.equals;

                src.IktatoErkezteto.Value = "I";
                src.IktatoErkezteto.Operator = Query.Operators.equals;
                src.TopRow = 1;

                Result result = service.GetAll(param, src);
                if (!string.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
                if (result.Ds.Tables[0].Rows.Count < 1)
                {
                    throw new Exception(string.Format("Nem található iktatókönvy {0}", Iktatohely));
                }
                return result.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            return null;
        }

        #region KULDEMENY KEZB MODJA
        /// <summary>
        /// GetKezbesitesModja
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="kuldKuldemeny"></param>
        public static void SetKuldemenyKezbesitesModja(ExecParam execParam, ref EREC_KuldKuldemenyek kuldKuldemeny)
        {
            string[] kodtarak = new string[] {
                KodTarak.KULD_KEZB_MODJA.e_ugyintezes,
                KodTarak.KULD_KEZB_MODJA.elektronikus_uton_Adatkapu,
                KodTarak.KULD_KEZB_MODJA.elektronikus_uton_Ugyfelkapu,
                KodTarak.KULD_KEZB_MODJA.elektronikus_uton_HKP,
            };

            try
            {
                string validKodtarElem = CheckKodtarakExistsAndReturnFirstValid(execParam, KodTarak.KULD_KEZB_MODJA.KCS, kodtarak);
                if (string.IsNullOrEmpty(validKodtarElem))
                {
                    kuldKuldemeny.KezbesitesModja = KodTarak.KULD_KEZB_MODJA.e_ugyintezes;
                    return;
                }
                else
                {
                    kuldKuldemeny.KezbesitesModja = validKodtarElem;
                    return;
                }
            }
            catch (Exception exc)
            {
                Logger.Error("EKF.SetKuldemenyKezbesitesModja", exc);
            }
            kuldKuldemeny.KuldesMod = KodTarak.KULD_KEZB_MODJA.e_ugyintezes;
        }
        /// <summary>
        /// CheckKodtarExist
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="kodCsoport"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        private static string CheckKodtarakExistsAndReturnFirstValid(ExecParam execParam, string kodCsoport, string[] ids)
        {
            #region KCS
            KRT_KodCsoportokService svcKcs = eAdminService.ServiceFactory.GetKRT_KodCsoportokService();
            ExecParam execParamKcs = execParam.Clone();
            KRT_KodCsoportokSearch srcKcs = new KRT_KodCsoportokSearch();
            srcKcs.Kod.Value = kodCsoport;
            srcKcs.Kod.Operator = Query.Operators.equals;
            srcKcs.TopRow = 1;
            Result resultKcs = svcKcs.GetAll(execParamKcs.Clone(), srcKcs);
            resultKcs.CheckError();

            if (resultKcs.Ds.Tables[0].Rows.Count < 1)
                return string.Empty;

            string kodCsoportId = resultKcs.Ds.Tables[0].Rows[0]["Id"].ToString();
            #endregion

            KRT_KodTarakService svc = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            KRT_KodTarakSearch src = new KRT_KodTarakSearch();
            src.KodCsoport_Id.Value = kodCsoportId;
            src.KodCsoport_Id.Operator = Query.Operators.equals;
            Result result = svc.GetAll(execParam.Clone(), src);
            result.CheckError();

            if (result.Ds.Tables[0].Rows.Count < 1)
                return string.Empty;

            for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
            {
                if (ids.Contains(result.Ds.Tables[0].Rows[i]["Kod"]))
                {
                    return result.Ds.Tables[0].Rows[i]["Kod"].ToString();
                }
            }
            return string.Empty;
        }

        #endregion
        /// <summary>
        /// SetKuldemenyKuldesModja
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="kuldKuldemeny"></param>
        public static void SetKuldemenyKuldesModja(ExecParam execParam, ref EREC_KuldKuldemenyek kuldKuldemeny)
        {
            string[] kodtarak = new string[] {
                KodTarak.KULDEMENY_KULDES_MODJA.Elhisz,
                KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu,
            };

            try
            {
                string validKodtarElem = CheckKodtarakExistsAndReturnFirstValid(execParam, KodTarak.KULDEMENY_KULDES_MODJA.KodcsoportKod, kodtarak);
                if (string.IsNullOrEmpty(validKodtarElem))
                {
                    kuldKuldemeny.KuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.Elhisz;
                    return;
                }
                else
                {
                    kuldKuldemeny.KuldesMod = validKodtarElem;
                    return;
                }
            }
            catch (Exception exc)
            {
                Logger.Error("EKF.SetKuldemenyKuldesModja", exc);
            }
            kuldKuldemeny.KuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.Elhisz;
        }

        public static Result AddKuldemeny(ExecParam execParam, EREC_KuldKuldemenyek kuldemeny)
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            Result result = service.Insert(execParam, kuldemeny);

            if (result.IsError)
                throw new ResultException(result);

            return result;
        }

        public static Result AddUgyirat(ExecParam execParam, EREC_UgyUgyiratok ugy)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            Result result = service.Insert(execParam, ugy);

            if (result.IsError)
                throw new ResultException(result);

            return result;
        }

        public static Result GetUgyirat(ExecParam execParam, EREC_UgyUgyiratokSearch src)
        {
            EREC_UgyUgyiratokService svc = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            if (src == null)
                src = new EREC_UgyUgyiratokSearch();

            Result result = svc.GetAll(execParam.Clone(), src);
            result.CheckError();

            return result;
        }
        public static Result GetKuldemeny(ExecParam execParam, EREC_KuldKuldemenyekSearch src)
        {
            EREC_KuldKuldemenyekService svc = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            if (src == null)
                src = new EREC_KuldKuldemenyekSearch();

            Result result = svc.GetAll(execParam.Clone(), src);
            result.CheckError();

            return result;
        }

        public static Result AddEmailBoritek(ExecParam execParam, EREC_eMailBoritekok boritek)
        {
            EREC_eMailBoritekokService service = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
            Result result = service.Insert(execParam, boritek);

            if (result.IsError)
                throw new ResultException(result);

            return result;
        }
        public static EREC_eMailBoritekok GetEmailBoritek(ExecParam execParam, string id)
        {
            EREC_eMailBoritekokService service = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
            execParam.Record_Id = id;
            Result result = service.Get(execParam);

            if (result.IsError)
                throw new ResultException(result);

            return (EREC_eMailBoritekok)result.Record;
        }

        public static Result GetIratPeldanyokForAtvetel(ExecParam execParam, string iratId)
        {
            Contentum.eRecord.Service.EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

            EREC_PldIratPeldanyokSearch src = new EREC_PldIratPeldanyokSearch();
            src.IraIrat_Id.Value = iratId;
            src.IraIrat_Id.Operator = Query.Operators.equals;
            src.Allapot.Value = KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt;
            src.Allapot.Operator = Query.Operators.equals;
            Result result = service.GetAll(execParam, src);

            if (result.IsError)
                throw new ResultException(result);

            return result;
        }

        public static Result GetEREC_IraKezbesitesiTetelek(ExecParam execParam, string objId)
        {
            Contentum.eRecord.Service.EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();

            EREC_IraKezbesitesiTetelekSearch src = new EREC_IraKezbesitesiTetelekSearch();
            src.Obj_Id.Value = objId;
            src.Obj_Id.Operator = Query.Operators.equals;
            src.TopRow = 1;
            Result result = service.GetAll(execParam, src);

            if (result.IsError)
                throw new ResultException(result);

            return result;
        }

        public static Result AddEREC_eBeadvanyok(ExecParam execParam, EREC_eBeadvanyok item)
        {
            EREC_eBeadvanyokService service = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            Result result = service.Insert(execParam, item);

            if (result.IsError)
                throw new ResultException(result);

            return result;
        }
        /// <summary>
        /// GetIrat
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Result GetIrat(string id)
        {
            ExecParam execParam = UnitTestHelper.GetExecParam();

            EREC_IraIratokService serviceIrat = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParamGet = UnitTestHelper.GetExecParam();
            execParamGet.Record_Id = id;
            Result result = serviceIrat.Get(execParamGet);
            if (result.IsError)
                throw new ResultException(result);

            return result;
        }
        /// <summary>
        /// GetKuldemeny
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Result GetKuldemeny(string id)
        {
            ExecParam execParam = UnitTestHelper.GetExecParam();

            EREC_KuldKuldemenyekService serviceIrat = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParamGet = UnitTestHelper.GetExecParam();
            execParamGet.Record_Id = id;
            Result result = serviceIrat.Get(execParamGet);
            if (result.IsError)
                throw new ResultException(result);

            return result;
        }
        /// <summary>
        /// GetAssertMessageFromResult
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static string GetAssertMessageFromResult(Result result)
        {
            return
                result.ErrorCode + " - " +
                result.ErrorMessage +
                result.ErrorDetail != null ?
                      string.Format(" - {0} - {1} - {2}", result.ErrorDetail.Message, result.ErrorDetail.ColumnValue, result.ErrorDetail.ObjectId)
                    : string.Empty;
        }
        /// <summary>
        /// GetRendszerParameterErtek
        /// </summary>
        /// <param name="xpm"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetRendszerParameterErtek(ExecParam xpm, string key)
        {
            Contentum.eAdmin.Service.KRT_ParameterekService service = eAdminService.ServiceFactory.GetKRT_ParameterekService();
            Result res = new Result();
            KRT_ParameterekSearch paramsearch = new KRT_ParameterekSearch();
            paramsearch.Nev.Value = key;
            paramsearch.Nev.Operator = Query.Operators.equals;
            paramsearch.TopRow = 1;

            res = service.GetAll(xpm, paramsearch);
            res.CheckError();

            if (res == null || res.Ds == null || res.Ds.Tables.Count < 1 || res.Ds.Tables[0].Rows.Count < 1)
                return null;

            return res.Ds.Tables[0].Rows[0]["Ertek"].ToString();
        }

    }
}
