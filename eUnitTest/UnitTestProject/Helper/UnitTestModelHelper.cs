﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;

namespace UnitTestProject
{
    public static class UnitTestModelHelper
    {
        public static EREC_KuldKuldemenyek GetNewKuldemeny(ExecParam execParam, string erkeztetoKonyv_Id, string postazasIranya, string source)
        {
            EREC_KuldKuldemenyek kuldKuldemeny = new EREC_KuldKuldemenyek();
            kuldKuldemeny.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            //kuldKuldemeny.ElsodlegesAdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;

            kuldKuldemeny.BeerkezesIdeje = DateTime.Now.ToString();
            kuldKuldemeny.HivatkozasiSzam = DateTime.Now.ToString();
            kuldKuldemeny.Targy = source + " " + DateTime.Now.ToString();
            kuldKuldemeny.IraIktatokonyv_Id = erkeztetoKonyv_Id;
            kuldKuldemeny.NevSTR_Bekuldo = "UnitTest bekuldo";
            kuldKuldemeny.CimSTR_Bekuldo = "UnitTest cim";

            UnitTestHelper.SetKuldemenyKuldesModja(execParam, ref kuldKuldemeny);

            kuldKuldemeny.Surgosseg = KodTarak.SURGOSSEG.Normal;
            kuldKuldemeny.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            kuldKuldemeny.Erkeztetes_Ev = DateTime.Now.Year.ToString();
            kuldKuldemeny.PeldanySzam = "1";
            kuldKuldemeny.Munkaallomas = "UnitTest Munkaallomas";
            kuldKuldemeny.FelhasznaloCsoport_Id_Bonto = execParam.Felhasznalo_Id;
            kuldKuldemeny.CimzesTipusa = KodTarak.KULD_CIMZES_TIPUS.nevre_szolo;
            UnitTestHelper.SetKuldemenyKezbesitesModja(execParam, ref kuldKuldemeny);

            kuldKuldemeny.Azonosito = DateTime.Now.ToString();
            kuldKuldemeny.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
            kuldKuldemeny.Tipus = KodTarak.KULDEMENY_TIPUS.Szerzodes;

            kuldKuldemeny.FelhasznaloCsoport_Id_Bonto = execParam.Felhasznalo_Id;
            kuldKuldemeny.FelbontasDatuma = kuldKuldemeny.BeerkezesIdeje;

            kuldKuldemeny.BelyegzoDatuma = DateTime.Now.ToString();
            kuldKuldemeny.Base.Note = UnitTestConstants.Note;

            if (postazasIranya != null)
                kuldKuldemeny.PostazasIranya = postazasIranya;
            return kuldKuldemeny;
        }
        /// <summary>
        /// GetNewIrat
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public static EREC_IraIratok GetNewIrat(ExecParam execParam, string source)
        {
            #region UJ IRAT
            EREC_IraIratok irat = new EREC_IraIratok();

            irat.Targy = source + " " + DateTime.Now.ToShortDateString();
            irat.Updated.Targy = true;

            irat.KiadmanyozniKell = "0";
            irat.Updated.KiadmanyozniKell = true;

            irat.ExpedialasModja = KodTarak.KULDEMENY_KULDES_MODJA.Postai_sima;
            irat.Updated.ExpedialasModja = true;

            //irat.FelhasznaloCsoport_Id_Ugyintez = execParam.Felhasznalo_Id;
            //irat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

            irat.Irattipus = KodTarak.IRATTIPUS.Hivatalbol;
            irat.Updated.Irattipus = true;

            //irat.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            //irat.Updated.AdathordozoTipusa = true;

            //irat.Jelleg = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            //irat.Updated.Jelleg = true;

            irat.Ugy_Fajtaja = KodTarak.UGY_FAJTAJA.AllamigazgatasiUgy;
            irat.Updated.Ugy_Fajtaja = true;

            irat.Base.Note = UnitTestConstants.Note;

            return irat;
            #endregion
        }
        /// <summary>
        /// GetNewIrat4Sakkora
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public static EREC_IraIratok GetNewIrat4Sakkora(ExecParam execParam, string source)
        {
            #region UJ IRAT
            EREC_IraIratok irat = new EREC_IraIratok();
            irat.Targy = source + " " + DateTime.Now.ToShortDateString();
            irat.KiadmanyozniKell = "0";
            irat.ExpedialasModja = KodTarak.KULDEMENY_KULDES_MODJA.Elhisz;

            irat.FelhasznaloCsoport_Id_Ugyintez = execParam.Felhasznalo_Id;

            irat.Irattipus = KodTarak.IRATTIPUS.Hivatalbol;
            irat.AdathordozoTipusa = KodTarak.AdathordozoTipus.Egyeb;
            irat.Jelleg = KodTarak.IRAT_JELLEG.Webes;

            ////irat.Ugy_Fajtaja = KodTarak.UGY_FAJTAJA.Egyeb_Hatosagi_Ugy;
            irat.Base.Note = UnitTestConstants.Note;

            return irat;
            #endregion
        }
        public static EREC_UgyUgyiratok GetNewUgyIrat(ExecParam execParam, string source)
        {
            EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();
            ugyirat.Hatarido = DateTime.Now.AddDays(30).ToString();
            ugyirat.Targy = source + " " + DateTime.Now.ToShortDateString();
            //ugyirat.UgyTipus = KodTarak.UGYTIPUS.KozgyulesiEloterjesztes;
            ugyirat.Ugy_Fajtaja = KodTarak.UGY_FAJTAJA.AllamigazgatasiUgy;
            //ugyirat.IraIrattariTetel_Id = null;

            ugyirat.NevSTR_Ugyindito = "UnitTest Ugyindito";
            ugyirat.Csoport_Id_Felelos = execParam.Felhasznalo_Id;
            ugyirat.FelhasznaloCsoport_Id_Ugyintez = null;

            ugyirat.IntezesiIdo = "30";
            ugyirat.IntezesiIdoegyseg = KodTarak.IDOEGYSEG.Nap;

            ugyirat.Base.Note = UnitTestConstants.Note;

            return ugyirat;
        }
        /// <summary>
        /// GetNewUgyIrat4Sakkora
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public static EREC_UgyUgyiratok GetNewUgyIrat4Sakkora(ExecParam execParam, string source, DateTime beerkezedDatuma)
        {
            EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();
            ugyirat.Targy = source + " " + DateTime.Now.ToShortDateString();
            ////ugyirat.Ugy_Fajtaja = KodTarak.UGY_FAJTAJA.Egyeb_Hatosagi_Ugy;
            ugyirat.NevSTR_Ugyindito = "UnitTest Ugyindito";
            ugyirat.Csoport_Id_Felelos = execParam.Felhasznalo_Id;
            //ugyirat.FelhasznaloCsoport_Id_Ugyintez = null;
            if (beerkezedDatuma != null)
                ugyirat.UgyintezesKezdete = beerkezedDatuma.ToString();
            ugyirat.Base.Note = UnitTestConstants.Note;
            return ugyirat;
        }

        public static EREC_eBeadvanyok GetNewEBeadvany(ExecParam execParam, string source)
        {
            EREC_eBeadvanyok item = new EREC_eBeadvanyok();
            item.Id = Guid.NewGuid().ToString();
            item.KuldoRendszer = "UnitTest";
            item.Irany = "0";
            item.FeladoTipusa = "1"; //"2"
            item.PartnerNev = "UnitTest";
            item.PartnerRovidNev = "UnitTest";
            item.KR_HivatkozasiSzam = "000000000000000000000000001";
            item.KR_ErkeztetesiSzam = "000000000000000000000000002";
            item.KR_DokTipusHivatal = "UnitTest";
            item.KR_DokTipusAzonosito = "UnitTest";
            item.KR_DokTipusLeiras = "UnitTest";
            item.KR_Megjegyzes = "UnitTest";
            item.KR_ErvenyessegiDatum = DateTime.Now.AddMonths(1).ToString();
            item.KR_ErkeztetesiDatum = DateTime.Now.ToString();
            //item.KR_FileNev = "UnitTest.txt";
            item.KR_Kezbesitettseg = "0";
            item.KR_Valasztitkositas = "0";
            item.KR_Valaszutvonal = "0";
            item.KR_Rendszeruzenet = "1";
            item.KR_Tarterulet = "0";
            item.Cel = KodTarak.eBeadvany_CelRendszer.WEB;

            item.Base.Note = UnitTestConstants.Note;

            return item;
        }

        public static IktatasiParameterek GetIktatasParameterek()
        {
            IktatasiParameterek iktatasParameterek = new IktatasiParameterek();
            iktatasParameterek.IratpeldanyVonalkodGeneralasHaNincs = false;
            iktatasParameterek.Adoszam = null;
            iktatasParameterek.UgykorId = null;
            iktatasParameterek.Ugytipus = null;
            iktatasParameterek.UgyiratUjranyitasaHaLezart = false;
            iktatasParameterek.UgyiratPeldanySzukseges = false;
            iktatasParameterek.KeszitoPeldanyaSzukseges = false;
            iktatasParameterek.Atiktatas = false;
            iktatasParameterek.EmptyUgyiratSztorno = false;
            iktatasParameterek.MunkaPeldany = false;
            return iktatasParameterek;
        }

        public static EREC_PldIratPeldanyok GetIratPeldany()
        {
            EREC_PldIratPeldanyok peldany = new EREC_PldIratPeldanyok();
            peldany.CimSTR_Cimzett = "8000 Székesfehérvár, Seregelyesi ut 96.";
            peldany.NevSTR_Cimzett = "UnitTest Cimzett";
            //peldany.Partner_Id_Cimzett = null;
            peldany.UgyintezesModja = "0";
            peldany.VisszaerkezesiHatarido = DateTime.Today.AddMonths(1).ToString();
            peldany.Base.Note = UnitTestConstants.Note;
            peldany.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            return peldany;
        }

        public static EREC_eMailBoritekok GetBoritek()
        {
            EREC_eMailBoritekok boritek = new EREC_eMailBoritekok();
            boritek.Uzenet = "UnitTest Uznet";
            boritek.Targy = "UnitTest EmailErkeztetesIktatas";
            boritek.EmailForras = "UnitTest";
            boritek.ErkezesDatuma = DateTime.Now.ToString();
            boritek.FeladasDatuma = DateTime.Now.ToString();
            boritek.Uzenet = "UnitTest " +
                "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms " +
                "of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal " +
                "blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. " +
                "These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing " +
                "prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. " +
                "But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur " +
                "that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this " +
                "principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.";
            boritek.Base.Ver = "1";
            boritek.Base.Note = UnitTestConstants.Note;
            return boritek;
        }
        public static EREC_Csatolmanyok GetERECCsatolmanyokForIrat(string IraIrat_Id)
        {
            EREC_Csatolmanyok csatolmanyok = new EREC_Csatolmanyok();
            csatolmanyok.IraIrat_Id = IraIrat_Id;
            csatolmanyok.Leiras = "UnitTest_Upload_" + DateTime.Now.ToFileTimeUtc();
            return csatolmanyok;
        }

        public static Csatolmany GetCsatolmany()
        {
            Csatolmany csatolmany = new Csatolmany();
            string fileNamePrefix = "UnitTest_Upload_" + DateTime.Now.ToFileTimeUtc();
            csatolmany.Nev = fileNamePrefix + ".txt";
            csatolmany.Tartalom = System.Text.Encoding.ASCII.GetBytes(fileNamePrefix);
            return csatolmany;
        }
    }
}
