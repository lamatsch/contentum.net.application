﻿using System;

namespace UnitTestProject.Models
{
    public class Model2Cleanup
    {
        public Model2Cleanup(UnitTestConstants.EnumModel2CleanupType type, string identity)
        {
            Type = type;
            Identity = identity;
        }

        public UnitTestConstants.EnumModel2CleanupType Type { get; set; }
        public string Identity { get; set; }
    }
}
