﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestProject
{
    public partial class UnitTest_Erkeztetes
    {
        public void CallErkeztetes(DateTime beerkezesDatuma, string flow, string postazasIranya, out string kuldemenyId)
        {
            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS

            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, postazasIranya, flow);
            kuldKuldemeny.BeerkezesIdeje = beerkezesDatuma.ToString();
            kuldKuldemeny.Updated.BeerkezesIdeje = true;
            kuldKuldemeny.BelyegzoDatuma = kuldKuldemeny.BeerkezesIdeje;
            kuldKuldemeny.Updated.BelyegzoDatuma = true;

            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_KuldKuldemenyekService iraIratokService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            Result result = iraIratokService.Erkeztetes(
                        execParam,
                        kuldKuldemeny,
                        feladatok,
                        erkeztetesParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(result));
            }

            ErkeztetesIktatasResult erkeztetesResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesResult != null)
            {
                kuldemenyId = string.IsNullOrEmpty(erkeztetesResult.KuldemenyId) ? kuldemenyId : erkeztetesResult.KuldemenyId;
            }
            #endregion ERKEZTETES

            Assert.IsNotNull(kuldemenyId);
        }
    }
}
