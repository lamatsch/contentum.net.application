﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    public partial class UnitTest_IratPldFunkciok
    {
        /// Expeial
        /// </summary>
        /// <param name="iratId"></param>
        public void CallExpedial(string iratId, out string iratPldId)
        {
            iratPldId = string.Empty;
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region FIND IRAT PLD
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            Result resultFind = service.GetElsoIratPeldanyByIraIrat(execParam, iratId);
            if (!string.IsNullOrEmpty(resultFind.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(resultFind));
            }

            EREC_PldIratPeldanyok iratPld = (EREC_PldIratPeldanyok)resultFind.Record;
            if (iratPld == null)
                Assert.Fail("Nem található iratpéldány az irathoz !");

            iratPldId = iratPld.Id;
            #endregion

            #region EXPEDIAL
            Result resultExpedial = service.Expedialas(
                     execParam,
                     new string[] { iratPld.Id },
                     string.Empty,
                     iratPld,
                     null
                     );

            if (!string.IsNullOrEmpty(resultExpedial.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(resultExpedial));
            }
            #endregion
        }
    }
}
