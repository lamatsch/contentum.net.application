﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    public partial class UnitTest_KuldemenyFunkciok
    {
        /// <summary>
        /// Postaz
        /// </summary>
        /// <param name="iratPldId"></param>
        public void CallPostaz(string iratPldId)
        {
            ExecParam execParam = UnitTestHelper.GetExecParam();

            EREC_Kuldemeny_IratPeldanyaiService serviceKIIP = eRecordService.ServiceFactory.GetEREC_Kuldemeny_IratPeldanyaiService();
            EREC_Kuldemeny_IratPeldanyaiSearch src = new EREC_Kuldemeny_IratPeldanyaiSearch();
            src.Peldany_Id.Value = iratPldId;
            src.Peldany_Id.Operator = Query.Operators.equals;
            src.TopRow = 1;

            Result resultKIIP = serviceKIIP.GetAll(execParam, src);
            if (!string.IsNullOrEmpty(resultKIIP.ErrorCode))
            {
                Assert.Fail(resultKIIP.ErrorCode + " - " + resultKIIP.ErrorMessage);
            }
            if (resultKIIP.Ds.Tables[0].Rows.Count < 1)
                Assert.Fail("Nem található kimenő küldemény az iratpéldányhoz !");


            Result resultKuldemeny = UnitTestHelper.GetKuldemeny(resultKIIP.Ds.Tables[0].Rows[0]["KuldKuldemeny_Id"].ToString());
            EREC_KuldKuldemenyek kuldemeny = (EREC_KuldKuldemenyek)resultKuldemeny.Record;

            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            Result resultPostaz = service.Postazas(execParam, kuldemeny, string.Empty);
            if (!string.IsNullOrEmpty(resultPostaz.ErrorCode))
            {
                Assert.Fail(resultPostaz.ErrorCode + " - " + resultPostaz.ErrorMessage);
            }
        }
    }
}
