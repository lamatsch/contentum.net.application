﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestProject
{
    public partial class UnitTest_Sakkora : ContentumBaseTest
    {
        #region HELPER
        /// <summary>
        /// GetUgyirat
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ugyiratId"></param>
        /// <returns></returns>
        internal EREC_UgyUgyiratok GetUgyirat(ExecParam execParam, string ugyiratId)
        {
            EREC_UgyUgyiratokService ugyIratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParamUgy = execParam.Clone();
            execParamUgy.Record_Id = ugyiratId;

            Result resultUgyGet = ugyIratokService.Get(execParamUgy);
            if (!string.IsNullOrEmpty(resultUgyGet.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(resultUgyGet));
            }
            return (EREC_UgyUgyiratok)resultUgyGet.Record;
        }
        /// <summary>
        /// GetIrat
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="iratId"></param>
        /// <returns></returns>
        internal EREC_IraIratok GetIrat(ExecParam execParam, string iratId)
        {
            EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParamUgy = execParam.Clone();
            execParamUgy.Record_Id = iratId;

            Result resultUgyGet = iratokService.Get(execParamUgy);
            if (!string.IsNullOrEmpty(resultUgyGet.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(resultUgyGet));
            }
            return (EREC_IraIratok)resultUgyGet.Record;
        }
        /// <summary>
        /// UpdateUgyIntezesiIdo
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ugyiratId"></param>
        /// <param name="intezesiIdo"></param>
        internal void UpdateUgyIntezesiIdo(ExecParam execParam, string ugyiratId, int intezesiIdo)
        {
            EREC_UgyUgyiratokService ugyIratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParamUgy = execParam.Clone();
            execParamUgy.Record_Id = ugyiratId;

            Result resultUgyGet = ugyIratokService.Get(execParamUgy);
            if (!string.IsNullOrEmpty(resultUgyGet.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(resultUgyGet));
            }
            EREC_UgyUgyiratok ugy = (EREC_UgyUgyiratok)resultUgyGet.Record;

            ugy.IntezesiIdo = intezesiIdo.ToString();
            ugy.Updated.IntezesiIdo = true;

            ugy.IntezesiIdoegyseg = KodTarak.IDOEGYSEG.Nap;
            ugy.Updated.IntezesiIdoegyseg = true;

            DateTime ugyKezdete;
            DateTime.TryParse(ugy.UgyintezesKezdete, out ugyKezdete);

            Assert.IsNotNull(ugy.UgyintezesKezdete, "Ügyintézés kezdete üres !");

            ugy.Hatarido = ugyKezdete.AddDays(intezesiIdo).ToString();
            ugy.Updated.Hatarido = true;

            ugy.Ugy_Fajtaja = KodTarak.UGY_FAJTAJA.Egyeb_Hatosagi_Ugy;
            ugy.Updated.Ugy_Fajtaja = true;

            Result resultUgyUpdate = ugyIratokService.Update(execParamUgy, ugy);
            if (!string.IsNullOrEmpty(resultUgyUpdate.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(resultUgyUpdate));
            }
        }

        /// <summary>
        /// UpdateIratSakkoraErtek
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="iratId"></param>
        /// <param name="sakkoraErtek"></param>
        internal void UpdateIratSakkoraErtek(ExecParam execParam, string iratId, string sakkoraErtek)
        {
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParamUgy = execParam.Clone();
            execParamUgy.Record_Id = iratId;

            Result resultUgyGet = service.Get(execParamUgy);
            if (!string.IsNullOrEmpty(resultUgyGet.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(resultUgyGet));
            }
            EREC_IraIratok ugy = (EREC_IraIratok)resultUgyGet.Record;

            ugy.IratHatasaUgyintezesre = sakkoraErtek;
            ugy.Updated.IratHatasaUgyintezesre = true;

            Result resultUgyUpdate = service.Update(execParamUgy, ugy);
            if (!string.IsNullOrEmpty(resultUgyUpdate.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(resultUgyUpdate));
            }
        }

        /// <summary>
        /// BejoviIratIktatasa
        /// </summary>
        /// <param name="ugyiratId"></param>
        /// <param name="iratId"></param>
        /// <param name="kuldemenyId"></param>
        internal void BejovoIratIktatasaSakkora(ref string ugyiratId, ref string iratId, ref string kuldemenyId, DateTime beerkezedDatuma, UnitTestConstants.FlowNumber flowNum)
        {
            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            ugyiratId = Guid.NewGuid().ToString();
            iratId = Guid.NewGuid().ToString();
            kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS

            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            UnitTest_Erkeztetes erkeztetes = new UnitTest_Erkeztetes();
            erkeztetes.CallErkeztetes(beerkezedDatuma, flowNum.ToString(), null, out kuldemenyId);
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_UGYUGYIRATOK
            EREC_UgyUgyiratok ugyirat = UnitTestModelHelper.GetNewUgyIrat4Sakkora(execParam, "UnitTest " + flowNum.ToString(), beerkezedDatuma);

            #endregion EREC_UGYUGYIRATOK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat4Sakkora(execParam, "UnitTest " + flowNum.ToString());
            irat.KuldKuldemenyek_Id = kuldemenyId;
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            iktatasParameterek.KuldemenyId = kuldemenyId;
            #endregion IKTATASIPARAMETEREK

            EREC_PldIratPeldanyok pldIrat = UnitTestModelHelper.GetIratPeldany();

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES + IKTATAS
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.BejovoIratIktatasa(
                        execParam,
                        iktatooKonyv_Id,
                        ugyirat,
                        darabok,
                        irat,
                        feladatok,
                        iktatasParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(result));
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }

            #endregion ERKEZTETES + IKTATAS

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);
        }
        /// <summary>
        /// BejovoIratIktatasaAlszamraSakkora
        /// </summary>
        /// <param name="ugyiratId"></param>
        /// <param name="iratId"></param>
        /// <param name="kuldemenyId"></param>
        /// <param name="beerkezedDatuma"></param>
        /// <param name="flowNum"></param>
        internal void BejovoIratIktatasaAlszamraSakkora(string ugyiratId, ref string iratId, ref string kuldemenyId, DateTime beerkezedDatuma, UnitTestConstants.FlowNumber flowNum)
        {
            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            iratId = Guid.NewGuid().ToString();
            kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS

            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            UnitTest_Erkeztetes erkeztetes = new UnitTest_Erkeztetes();
            erkeztetes.CallErkeztetes(beerkezedDatuma, flowNum.ToString(), null, out kuldemenyId);
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat4Sakkora(execParam, "UnitTest " + flowNum.ToString());
            irat.KuldKuldemenyek_Id = kuldemenyId;
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            iktatasParameterek.KuldemenyId = kuldemenyId;
            #endregion IKTATASIPARAMETEREK

            EREC_PldIratPeldanyok pldIrat = UnitTestModelHelper.GetIratPeldany();

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES + IKTATAS
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.BejovoIratIktatasa_Alszamra(
                        execParam,
                        iktatooKonyv_Id,
                        ugyiratId,
                        darabok,
                        irat,
                        feladatok,
                        iktatasParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(result));
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                //ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }

            #endregion ERKEZTETES + IKTATAS

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);
        }
        ///// <summary>
        ///// CallErkeztetesSakkora
        ///// </summary>
        ///// <param name="beerkezesDatuma"></param>
        ///// <param name="kuldemenyId"></param>
        //internal void CallErkeztetesSakkora(DateTime beerkezesDatuma, out string kuldemenyId)
        //{
        //    #region INIT
        //    ExecParam execParam = UnitTestHelper.GetExecParam();

        //    #region IDS
        //    kuldemenyId = Guid.NewGuid().ToString();
        //    #endregion IDS

        //    string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);

        //    #region EREC_KULDKULDEMENYEK
        //    EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, null, "UnitTest Erkeztetes");
        //    kuldKuldemeny.BeerkezesIdeje = beerkezesDatuma.ToString();
        //    kuldKuldemeny.Updated.BeerkezesIdeje = true;
        //    kuldKuldemeny.BelyegzoDatuma = kuldKuldemeny.BeerkezesIdeje;
        //    kuldKuldemeny.Updated.BelyegzoDatuma = true;

        //    kuldKuldemeny.Id = kuldemenyId;
        //    #endregion EREC_KULDKULDEMENYEK

        //    ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
        //    EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
        //    #endregion

        //    #region ERKEZTETES
        //    string ugyintezo = execParam.Felhasznalo_Id;

        //    EREC_KuldKuldemenyekService iraIratokService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        //    Result result = iraIratokService.Erkeztetes(
        //                execParam,
        //                kuldKuldemeny,
        //                feladatok,
        //                erkeztetesParameterek
        //                );

        //    if (!string.IsNullOrEmpty(result.ErrorCode))
        //    {
        //        Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
        //    }

        //    ErkeztetesIktatasResult erkeztetesResult = (ErkeztetesIktatasResult)result.Record;
        //    if (erkeztetesResult != null)
        //    {
        //        kuldemenyId = string.IsNullOrEmpty(erkeztetesResult.KuldemenyId) ? kuldemenyId : erkeztetesResult.KuldemenyId;
        //    }
        //    #endregion ERKEZTETES

        //    Assert.IsNotNull(kuldemenyId);
        //}
        /// <summary>
        /// CallElteltIdoNoveles
        /// </summary>
        /// <param name="ugyiratId"></param>
        internal void CallSakkoraElteltIdoNoveles(string ugyiratId)
        {
            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();
            #endregion

            SakkoraService sakkoraService = eRecordService.ServiceFactory.Get_SakkoraService();
            Result result = sakkoraService.SakkoraElteltIdoNoveles(execParam, ugyiratId);
            //Assert.IsNotNull(result.ErrorCode);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(result));
            }
        }
        /// <summary>
        /// CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles
        /// </summary>
        /// <param name="ugyiratId"></param>
        internal void CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(string ugyiratId)
        {
            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();
            #endregion

            SakkoraService sakkoraService = eRecordService.ServiceFactory.Get_SakkoraService();
            Result result = sakkoraService.SakkoraFelfuggesztettAllapotEsetenHataridoNoveles(execParam, ugyiratId);
            //Assert.IsNotNull(result.ErrorCode);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(result));
            }
        }
        /// <summary>
        /// CheckSakkoraDate
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        internal void CheckSakkoraDate(DateTime expected, DateTime actual, UnitTestConstants.StepNumber step, string type)
        {
            Assert.AreEqual(expected.Year, actual.Year, string.Format("({0})  TIPUS:{3} ÉV: Expected {1} != Actual {2}", step.ToString(), expected.Year, actual.Year, type));
            Assert.AreEqual(expected.Month, actual.Month, string.Format("({0}) TIPUS:{3} HÓ: Expected {1} != Actual {2}", step.ToString(), expected.Month, actual.Month, type));
            Assert.AreEqual(expected.Day, actual.Day, string.Format("({0})  TIPUS:{3} NAP: Expected {1} != Actual {2}", step.ToString(), expected.Day, actual.Day, type));
        }

        /// <summary>
        /// CheckSakkoraHatralevoNapok
        /// </summary>
        /// <param name="intezesiIdo"></param>
        /// <param name="elteltNapok"></param>
        /// <param name="hatralevoNapok"></param>
        internal void CheckSakkoraHatralevoNapok(int intezesiIdo, int elteltNapok, string hatralevoNapok, UnitTestConstants.StepNumber step)
        {
            Assert.AreEqual((intezesiIdo + elteltNapok).ToString(), hatralevoNapok,
                string.Format("({0})  Intezesi ido {1} nem egyezik meg a hatralevo napok szamaval {2}", step.ToString(), intezesiIdo, hatralevoNapok));
        }
        /// <summary>
        /// CheckSakkoraElteltIdo
        /// </summary>
        /// <param name="expectedElteltIdo"></param>
        /// <param name="actualElteltIdo"></param>
        /// <param name="step"></param>
        internal void CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok expectedElteltIdo, string actualElteltIdo, UnitTestConstants.StepNumber step)
        {
            Assert.AreEqual(((int)expectedElteltIdo).ToString(), actualElteltIdo,
                 string.Format("({0})  Az eltelt idő {1} != {2}", step.ToString(), actualElteltIdo, expectedElteltIdo));
        }

        /// <summary>
        /// CheckSakkoraHataridoUres
        /// </summary>
        /// <param name="actualHatarido"></param>
        /// <param name="step"></param>
        internal void CheckSakkoraHataridoUres(string actualHatarido, UnitTestConstants.StepNumber step)
        {
            Assert.IsNotNull(actualHatarido, string.Format("({0})  Hatarido ures !", step.ToString()));
        }
        /// <summary>
        /// CheckSakkoraUgyintezesKezdeteUres
        /// </summary>
        /// <param name="actualUgyintezesKezdete"></param>
        /// <param name="step"></param>
        internal void CheckSakkoraUgyintezesKezdeteUres(string actualUgyintezesKezdete, UnitTestConstants.StepNumber step)
        {
            Assert.IsNotNull(actualUgyintezesKezdete, string.Format("({0})  Ugyintezes kezdete ures !", step.ToString()));
        }
        /// <summary>
        /// CheckSakkoraElteltIdoAllapot
        /// </summary>
        /// <param name="actualElteltIdoAllapot"></param>
        /// <param name="expectedAllapot"></param>
        /// <param name="step"></param>
        internal void CheckSakkoraElteltIdoAllapot(string actualElteltIdoAllapot, string expectedAllapot, UnitTestConstants.StepNumber step)
        {
            Assert.AreEqual(expectedAllapot.ToString(), actualElteltIdoAllapot,
                 string.Format("({0})  Az eltelt ido allapota {1} != {2}",
                 step.ToString(), expectedAllapot.ToString(), actualElteltIdoAllapot));
        }

        /// <summary>
        /// DebugStepInfo
        /// </summary>
        /// <param name="flow"></param>
        /// <param name="step"></param>
        internal void DebugStepInfo(UnitTestConstants.FlowNumber flow, UnitTestConstants.StepNumber step)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("START: {0} {1}", flow.ToString(), step.ToString()));
            System.Console.WriteLine(string.Format("START: {0} {1}", flow.ToString(), step.ToString()));
        }
        /// <summary>
        /// Sakkora engedelyezett rendszerparameter alapjan
        /// </summary>
        /// <param name="execParam"></param>
        /// <returns></returns>
        internal bool IsSakkoraEnabled(ExecParam execParam)
        {
            string ertek = UnitTestHelper.GetRendszerParameterErtek(execParam, Rendszerparameterek.UGYINTEZESI_IDO_FELFUGGESZTES.ToString());
            if (ertek == null)
                return false;

            return ertek.Equals(Constants.EnumUgyintezesiIdoFelfuggesztes.FANCY.ToString()) ? true : false;
        }
        #endregion
    }
}
