﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    public partial class UnitTest_UgyiratFunkciok
    {
        /// <summary>
        /// SakkoraUgyintezesiIdoUjraSzamolas
        /// </summary>
        /// <param name="ugyIratId"></param>
        public void CallSakkoraUgyintezesiIdoUjraSzamolas(string ugyIratId)
        {
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region CALL
            SakkoraService service = eRecordService.ServiceFactory.Get_SakkoraService();
            Result result = service.SakkoraUgyintezesiIdoUjraSzamolas(execParam, ugyIratId);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(UnitTestHelper.GetAssertMessageFromResult(result));
            }
            #endregion
        }
    }
}
