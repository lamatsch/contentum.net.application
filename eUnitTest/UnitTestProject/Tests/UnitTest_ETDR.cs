﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest_ETDR : ContentumBaseTest
    {
        #region 
        [TestMethod]
        public void Login_Case1()
        {
            ServiceReference_ETDR.NT_ETDRServiceSoapClient client = new ServiceReference_ETDR.NT_ETDRServiceSoapClient();
            ServiceReference_ETDR.LoginResponse responseLogin = client.Login(new ServiceReference_ETDR.Login()
            {
                LoginNev = "LOGIN",
                Password = "PASSWORD",
            }
            );

            Assert.IsNotNull(responseLogin);
            Assert.IsNull(responseLogin.ErrMess, responseLogin.ErrMess);
        }

        [TestMethod]
        public void Add_Erkezteto_Alap_Case1()
        {
            ServiceReference_ETDR.NT_ETDRServiceSoapClient client = new ServiceReference_ETDR.NT_ETDRServiceSoapClient();
            ServiceReference_ETDR.LoginResponse responseLogin = client.Login(new ServiceReference_ETDR.Login()
            {
                LoginNev = "LOGIN",
                Password = "PASSWORD",
            }
            );

            Assert.IsNotNull(responseLogin);
            Assert.IsNull(responseLogin.ErrMess, responseLogin.ErrMess);

            ServiceReference_ETDR.Add_Erkezteto_Alap item = new ServiceReference_ETDR.Add_Erkezteto_Alap();
            item.LoginAzon = responseLogin.LoginResult;
            item.BeerkezesDate = DateTime.Now.ToString();
            item.bejovo = true;
            item.Hazszam = "12";
            item.Irsz = "8000";
            item.LoginNev = "LOGIN";
            item.Nev = "ALMAFA";
            item.Telepules = "SZEKESFEHERVAR";
            item.Utca = "CONTENTUM";
            item.Ugyint_Id = 131;
            ServiceReference_ETDR.Add_Erkezteto_AlapResponse respIktat = client.Add_Erkezteto_Alap(item);

            Assert.IsNotNull(respIktat);
            Assert.IsNull(respIktat.ErrMess, respIktat.ErrMess);
        }
        #endregion
    }
}
