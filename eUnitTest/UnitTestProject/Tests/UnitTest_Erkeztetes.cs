﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Transactions;

namespace UnitTestProject
{
    [TestClass]
    public partial class UnitTest_Erkeztetes : ContentumBaseTest
    {
        [TestMethod]
        public void Erkeztetes()
        {
            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            string kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS

            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, null, "UnitTest Erkeztetes");
            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_KuldKuldemenyekService iraIratokService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            Result result = iraIratokService.Erkeztetes(
                        execParam,
                        kuldKuldemeny,
                        feladatok,
                        erkeztetesParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesResult != null)
            {
                kuldemenyId = string.IsNullOrEmpty(erkeztetesResult.KuldemenyId) ? kuldemenyId : erkeztetesResult.KuldemenyId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
        }

        [TestMethod]
        public void HivataliKapusErkeztetes()
        {
            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            string kuldemenyId = Guid.NewGuid().ToString();
            string eBeadvanyId = Guid.NewGuid().ToString();
            #endregion IDS

            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, null, "UnitTest HivataliKapusErkeztetesIktatas");
            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            EREC_PldIratPeldanyok pldIrat = UnitTestModelHelper.GetIratPeldany();

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region EBEADVANY
            EREC_eBeadvanyok eBeadvany = UnitTestModelHelper.GetNewEBeadvany(execParam, "UnitTest HivataliKapusErkeztetesIktatas");
            eBeadvany.Id = eBeadvanyId;
            Result resultAddEBeadvany = UnitTestHelper.AddEREC_eBeadvanyok(execParam, eBeadvany);
            eBeadvanyId = resultAddEBeadvany.Uid;
            #endregion EBEADVANY

            #region ERKEZTETES + IKTATAS
            EREC_KuldKuldemenyekService iraIratokService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            Result result = iraIratokService.ErkeztetesHivataliKapubol(
                   execParam,
                   kuldKuldemeny,
                   eBeadvanyId,
                   feladatok,
                   erkeztetesParameterek);

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
        }
    }
}
