﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest_HAIR : ContentumBaseTest
    {
        #region 
        [TestMethod]
        public void IratPeldanyLetrehozas_Case1()
        {
            ServiceReferenceEIntegrator.NT_HAIRServiceSoapClient client = new ServiceReferenceEIntegrator.NT_HAIRServiceSoapClient();
            List<ServiceReferenceEIntegrator.UjIratpeldany> items = new List<ServiceReferenceEIntegrator.UjIratpeldany>();
            items.Add(new ServiceReferenceEIntegrator.UjIratpeldany()
            {
                Alapadatok = new ServiceReferenceEIntegrator.Alapadatok()
                {
                    KulsoAzonosito = "",
                },
                IktatoszamAlszam = new ServiceReferenceEIntegrator.IktatoszamAlszam()
                {
                    //	ADO /855 - 9 /2019
                    Alszam = 9,
                    AlszamSpecified = true,
                    Elotag = "ADO",
                    Ev = 2019,
                    EvSpecified = true,
                    Foszam = 855,
                    FoszamSpecified = true
                },
                IratpeldanyAdatok = new ServiceReferenceEIntegrator.IratpeldanyAdatok()
                {
                    Felelos = "Admin",
                    KuldesModja = "192",
                    Ugyfel = new ServiceReferenceEIntegrator.Partner()
                    {
                        PartnerNev = "Adminisztrátor",
                        PartnerAzonosito = "",
                        PartnerCim = "1214 BUDAPEST VÖRÖSMARTY U. 4.",
                        PartnerCimAzonosito = ""
                    },
                    UgyintezesModja = "1",
                }
            }
            );
            ServiceReferenceEIntegrator.Valasz1[] valasz = client.IratpeldanytLetrehoz(items.ToArray());
        }

        [TestMethod]
        public void Postaz_Case1()
        {
            ServiceReferenceEIntegrator.NT_HAIRServiceSoapClient client = new ServiceReferenceEIntegrator.NT_HAIRServiceSoapClient();
            List<ServiceReferenceEIntegrator.Postazas> items = new List<ServiceReferenceEIntegrator.Postazas>();
            items.Add(new ServiceReferenceEIntegrator.Postazas()
            {
                PostazasAdatok = new ServiceReferenceEIntegrator.PostazasAdatok()
                {
                    Felado = "admin",
                    KuldemenyAzonosito = "",
                    Postakonyv = "POSTA",
                    PostazasDatuma = DateTime.Now,
                },
                IratAdatok= new ServiceReferenceEIntegrator.IratAdatok1()
                {
                    Iktatoszam = new ServiceReferenceEIntegrator.IktatoszamSorszam()
                    {
                        Alszam = 9,
                        AlszamSpecified = true,
                        Elotag = "ADO",
                        Ev = 2019,
                        EvSpecified = true,
                        Foszam = 855,
                        FoszamSpecified = true
                    },
                },
            }
            );
            ServiceReferenceEIntegrator.Valasz2[] valasz = client.Postaz(items.ToArray());
        }
        #endregion
    }
}
