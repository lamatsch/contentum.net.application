﻿using System;
using System.Transactions;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest_Iktatas : ContentumBaseTest
    {
        [TestMethod]
        public void EmailErkeztetesIktatas()
        {
            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            string ugyiratId = Guid.NewGuid().ToString();
            string iratId = Guid.NewGuid().ToString();
            string kuldemenyId = Guid.NewGuid().ToString();
            string boritekId = Guid.NewGuid().ToString();
            #endregion IDS
            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, null, "UnitTest EmailErkeztetesIktatas");
            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_UGYUGYIRATOK
            EREC_UgyUgyiratok ugyirat = UnitTestModelHelper.GetNewUgyIrat(execParam, "UnitTest EmailErkeztetesIktatas");
            ugyirat.Id = ugyiratId;
            #endregion EREC_UGYUGYIRATOK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat(execParam, "UnitTest EmailErkeztetesIktatas");
            irat.KuldKuldemenyek_Id = kuldemenyId;
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            #endregion IKTATASIPARAMETEREK

            #region BORITEK
            EREC_eMailBoritekok boritek = UnitTestModelHelper.GetBoritek();
            Result resultAddBoritek = UnitTestHelper.AddEmailBoritek(execParam, boritek);

            boritek = UnitTestHelper.GetEmailBoritek(execParam, resultAddBoritek.Uid);
            #endregion

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES + IKTATAS
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.EmailErkeztetesIktatas(
                        execParam,
                        kuldKuldemeny,
                        boritek,
                        iktatooKonyv_Id,
                        ugyirat,
                        darabok,
                        irat,
                        feladatok,
                        iktatasParameterek,
                        erkeztetesParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, iratId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyiratId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);
        }

        [TestMethod]
        public void EgyszerusitettIktatas()
        {

            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            string ugyiratId = Guid.NewGuid().ToString();
            string iratId = Guid.NewGuid().ToString();
            string kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS
            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, null, "UnitTest EgyszerusitettIratIkt");
            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_UGYUGYIRATOK
            EREC_UgyUgyiratok ugyirat = UnitTestModelHelper.GetNewUgyIrat(execParam, "UnitTest EgyszerusitettIratIkt");
            ugyirat.Id = ugyiratId;
            #endregion EREC_UGYUGYIRATOK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat(execParam, "UnitTest EgyszerusitettIratIkt");
            irat.KuldKuldemenyek_Id = kuldemenyId;
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            #endregion IKTATASIPARAMETEREK

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES + IKTATAS
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.EgyszerusitettIktatasa(
                        execParam,
                        iktatooKonyv_Id,
                        ugyirat,
                        darabok,
                        irat,
                        feladatok,
                        kuldKuldemeny,
                        iktatasParameterek,
                        erkeztetesParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, iratId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyiratId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);

        }

        [TestMethod]
        public void BelsoIratIktatasa()
        {
            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            string ugyiratId = Guid.NewGuid().ToString();
            string iratId = Guid.NewGuid().ToString();
            string kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS
            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, null, "UnitTest EgyszerusitettIratIkt");
            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_UGYUGYIRATOK
            EREC_UgyUgyiratok ugyirat = UnitTestModelHelper.GetNewUgyIrat(execParam, "UnitTest EgyszerusitettIratIkt");
            ugyirat.Id = ugyiratId;
            #endregion EREC_UGYUGYIRATOK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat(execParam, "UnitTest EgyszerusitettIratIkt");
            irat.KuldKuldemenyek_Id = kuldemenyId;
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            #endregion IKTATASIPARAMETEREK

            EREC_PldIratPeldanyok pldIrat = UnitTestModelHelper.GetIratPeldany();

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES + IKTATAS
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.BelsoIratIktatasa(
                        execParam,
                        iktatooKonyv_Id,
                        ugyirat,
                        darabok,
                        irat,
                        feladatok,
                        pldIrat,
                        iktatasParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, iratId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyiratId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);

        }

        [TestMethod]
        public void BejovoIratIktatasa()
        {

            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            string ugyiratId = Guid.NewGuid().ToString();
            string iratId = Guid.NewGuid().ToString();
            string kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS
            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            CallErkeztetes(DateTime.Now, out kuldemenyId);
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_UGYUGYIRATOK
            EREC_UgyUgyiratok ugyirat = UnitTestModelHelper.GetNewUgyIrat(execParam, "UnitTest EgyszerusitettIratIkt");
            #endregion EREC_UGYUGYIRATOK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat(execParam, "UnitTest EgyszerusitettIratIkt");
            irat.KuldKuldemenyek_Id = kuldemenyId;
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            iktatasParameterek.KuldemenyId = kuldemenyId;
            #endregion IKTATASIPARAMETEREK

            EREC_PldIratPeldanyok pldIrat = UnitTestModelHelper.GetIratPeldany();

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES + IKTATAS
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.BejovoIratIktatasa(
                        execParam,
                        iktatooKonyv_Id,
                        ugyirat,
                        darabok,
                        irat,
                        feladatok,
                        iktatasParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, iratId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyiratId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);

        }

        [TestMethod]
        public void HivataliKapusErkeztetesIktatas()
        {
            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            string ugyiratId = Guid.NewGuid().ToString();
            string iratId = Guid.NewGuid().ToString();
            string kuldemenyId = Guid.NewGuid().ToString();
            string eBeadvanyId = Guid.NewGuid().ToString();
            #endregion IDS
            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, null, "UnitTest HivataliKapusErkeztetesIktatas");
            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_UGYUGYIRATOK
            EREC_UgyUgyiratok ugyirat = UnitTestModelHelper.GetNewUgyIrat(execParam, "UnitTest HivataliKapusErkeztetesIktatas");
            #endregion EREC_UGYUGYIRATOK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat(execParam, "UnitTest HivataliKapusErkeztetesIktatas");
            irat.KuldKuldemenyek_Id = kuldemenyId;
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            iktatasParameterek.KuldemenyId = kuldemenyId;
            #endregion IKTATASIPARAMETEREK

            EREC_PldIratPeldanyok pldIrat = UnitTestModelHelper.GetIratPeldany();

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region EBEADVANY
            EREC_eBeadvanyok eBeadvany = UnitTestModelHelper.GetNewEBeadvany(execParam, "UnitTest HivataliKapusErkeztetesIktatas");
            eBeadvany.Id = eBeadvanyId;
            Result resultAddEBeadvany = UnitTestHelper.AddEREC_eBeadvanyok(execParam, eBeadvany);
            eBeadvanyId = resultAddEBeadvany.Uid;
            #endregion EBEADVANY

            #region ERKEZTETES + IKTATAS
            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.HivataliKapusErkeztetesIktatas(
                   execParam,
                   kuldKuldemeny,
                   eBeadvanyId,
                   iktatooKonyv_Id,
                   ugyirat,
                   darabok,
                   irat,
                   feladatok,
                   iktatasParameterek,
                   erkeztetesParameterek);

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, iratId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyiratId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);
        }
        /// <summary>
        /// CallErkeztetes
        /// </summary>
        /// <param name="beerkezesDatuma"></param>
        /// <param name="kuldemenyId"></param>
        internal void CallErkeztetes(DateTime beerkezesDatuma, out string kuldemenyId)
        {
            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS

            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, null, "UnitTest Erkeztetes");
            kuldKuldemeny.BeerkezesIdeje = beerkezesDatuma.ToString();
            kuldKuldemeny.Updated.BeerkezesIdeje = true;
            kuldKuldemeny.BelyegzoDatuma = kuldKuldemeny.BeerkezesIdeje;
            kuldKuldemeny.Updated.BelyegzoDatuma = true;

            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_KuldKuldemenyekService iraIratokService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            Result result = iraIratokService.Erkeztetes(
                        execParam,
                        kuldKuldemeny,
                        feladatok,
                        erkeztetesParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesResult != null)
            {
                kuldemenyId = string.IsNullOrEmpty(erkeztetesResult.KuldemenyId) ? kuldemenyId : erkeztetesResult.KuldemenyId;
            }
            #endregion ERKEZTETES

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
        }
    }
}
