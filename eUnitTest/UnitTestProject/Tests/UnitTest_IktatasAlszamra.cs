﻿using System;
using System.Transactions;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest_IktatasAlszamra : ContentumBaseTest
    {
        //[TestMethod]
        public void BejovoIratIktatasa_Alszamra()
        {

            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            string ugyiratId = Guid.NewGuid().ToString();
            string iratId = Guid.NewGuid().ToString();
            string kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS
            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, KodTarak.POSTAZAS_IRANYA.Bejovo, "UnitTest EgyszerusitettIratIkt");
            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_UGYUGYIRATOK
            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            search.Allapot.Value = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
            search.Allapot.Operator = Query.Operators.equals;
            search.TopRow = 1;

            Result resultGetUgy = UnitTestHelper.GetUgyirat(execParam, search);
            if (resultGetUgy.Ds.Tables[0].Rows.Count < 1)
                throw new Exception("Nincs felhasználható az ügyirat BejovoIratIktatasa_Alszamra funkcióhoz");
            ugyiratId = resultGetUgy.Ds.Tables[0].Rows[0]["Id"].ToString();
            #endregion EREC_UGYUGYIRATOK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat(execParam, "UnitTest EgyszerusitettIratIkt");
            irat.KuldKuldemenyek_Id = kuldemenyId;
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            #endregion IKTATASIPARAMETEREK

            EREC_PldIratPeldanyok pldIrat = UnitTestModelHelper.GetIratPeldany();

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES + IKTATAS
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.BejovoIratIktatasa_Alszamra(
                        execParam,
                        iktatooKonyv_Id,
                        ugyiratId,
                        darabok,
                        irat,
                        feladatok,
                        iktatasParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, iratId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyiratId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);

        }

        /// <summary>
        /// BelsoIratIktatasa_Alszamra
        /// </summary>
        /// <param name="ugyiratId"></param>
        /// <param name="sakkoraKod"></param>
        /// <param name="flow"></param>
        /// <param name="iratId"></param>
        /// <param name="kuldemenyId"></param>
        public void CallBelsoIratIktatasa_Alszamra(string ugyiratId, string sakkoraKod, DateTime beerkezedDatuma, UnitTestConstants.FlowNumber flow, string postazasIranya, bool kiadmanyoznikell, out string iratId, out string kuldemenyId)
        {
            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            iratId = Guid.NewGuid().ToString();
            kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS
            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            UnitTest_Erkeztetes erkeztetes = new UnitTest_Erkeztetes();
            erkeztetes.CallErkeztetes(beerkezedDatuma, flow.ToString(), postazasIranya, out kuldemenyId);
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat(execParam, flow.ToString());
            if (!string.IsNullOrEmpty(postazasIranya))
            {
                irat.PostazasIranya = postazasIranya;
                irat.Updated.PostazasIranya = true;
            }
            if (kiadmanyoznikell)
            {
                irat.KiadmanyozniKell = "1";
                irat.Updated.KiadmanyozniKell = true;

                irat.FelhasznaloCsoport_Id_Kiadmany = execParam.FelhasznaloSzervezet_Id;
                irat.Updated.FelhasznaloCsoport_Id_Kiadmany = true;
            }
            irat.KuldKuldemenyek_Id = kuldemenyId;
            irat.Updated.KuldKuldemenyek_Id = true;
            if (!string.IsNullOrEmpty(sakkoraKod))
            {
                irat.IratHatasaUgyintezesre = sakkoraKod;
                irat.Updated.IratHatasaUgyintezesre = true;
            }

            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            #endregion IKTATASIPARAMETEREK

            EREC_PldIratPeldanyok pldIrat = UnitTestModelHelper.GetIratPeldany();

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES + IKTATAS
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.BelsoIratIktatasa_Alszamra(
                        execParam,
                        iktatooKonyv_Id,
                        ugyiratId,
                        darabok,
                        irat,
                        feladatok,
                        pldIrat,
                        iktatasParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, iratId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyiratId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);

        }
    }
}
