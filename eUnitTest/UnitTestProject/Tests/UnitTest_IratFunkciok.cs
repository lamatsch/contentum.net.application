﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Transactions;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest_IratFunkciok : ContentumBaseTest
    {
        [TestMethod]
        public void IratSzignalas()
        {

            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            string ugyiratId = Guid.NewGuid().ToString();
            string iratId = Guid.NewGuid().ToString();
            string kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS
            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, null, "UnitTest EgyszerusitettIratIkt + Szignal");
            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_UGYUGYIRATOK
            EREC_UgyUgyiratok ugyirat = UnitTestModelHelper.GetNewUgyIrat(execParam, "UnitTest EgyszerusitettIratIkt + Szignal");
            ugyirat.Id = ugyiratId;
            #endregion EREC_UGYUGYIRATOK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat(execParam, "UnitTest EgyszerusitettIratIkt + Szignal");
            irat.KuldKuldemenyek_Id = kuldemenyId;
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            #endregion IKTATASIPARAMETEREK

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES + IKTATAS
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.EgyszerusitettIktatasa(
                        execParam,
                        iktatooKonyv_Id,
                        ugyirat,
                        darabok,
                        irat,
                        feladatok,
                        kuldKuldemeny,
                        iktatasParameterek,
                        erkeztetesParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, iratId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyiratId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);

            Result resultSzignal = iraIratokService.Szignalas(execParam, iratId, UnitTestConstants.ExecParam_FelhasznaloSzervezet_Id, new EREC_HataridosFeladatok());
            if (!string.IsNullOrEmpty(resultSzignal.ErrorCode))
            {
                Assert.Fail(resultSzignal.ErrorCode + " - " + resultSzignal.ErrorMessage);
            }

        }

        [TestMethod]
        public void IratAtvetel()
        {

            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            string ugyiratId = Guid.NewGuid().ToString();
            string iratId = Guid.NewGuid().ToString();
            string kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS
            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, null, "UnitTest EgyszerusitettIratIkt + Szignal + Atvetel");
            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_UGYUGYIRATOK
            EREC_UgyUgyiratok ugyirat = UnitTestModelHelper.GetNewUgyIrat(execParam, "UnitTest EgyszerusitettIratIkt + Szignal + Atvetelt");
            ugyirat.Id = ugyiratId;
            #endregion EREC_UGYUGYIRATOK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat(execParam, "UnitTest EgyszerusitettIratIkt + Szignal + Atvetel");
            irat.KuldKuldemenyek_Id = kuldemenyId;
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            #endregion IKTATASIPARAMETEREK

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES + IKTATAS
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.EgyszerusitettIktatasa(
                        execParam,
                        iktatooKonyv_Id,
                        ugyirat,
                        darabok,
                        irat,
                        feladatok,
                        kuldKuldemeny,
                        iktatasParameterek,
                        erkeztetesParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, iratId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyiratId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);

            #region SZIGNAL
            Result resultSzignal = iraIratokService.Szignalas(execParam, iratId, UnitTestConstants.ExecParam_FelhasznaloSzervezet_Id, new EREC_HataridosFeladatok());
            if (!string.IsNullOrEmpty(resultSzignal.ErrorCode))
            {
                Assert.Fail(resultSzignal.ErrorCode + " - " + resultSzignal.ErrorMessage);
            }
            #endregion

            #region ATVETEL
            Result resultPldIrat = UnitTestHelper.GetIratPeldanyokForAtvetel(execParam, iratId);
            if (resultPldIrat == null || resultPldIrat.Ds.Tables[0].Rows.Count < 1)
            {
                Assert.Fail("Nem található iratpéldány !");
            }

            ExecParam execParamAtvetel = UnitTestHelper.GetExecParamForIratAtvetel();
            EREC_IraKezbesitesiTetelekService serviceIraKezbTetel = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
            string kezbTetelId;
            Result resultIraKezbTetel;
            Result resultAtvetel;
            for (int i = 0; i < resultPldIrat.Ds.Tables[0].Rows.Count; i++)
            {
                resultIraKezbTetel = UnitTestHelper.GetEREC_IraKezbesitesiTetelek(execParamAtvetel.Clone(), resultPldIrat.Ds.Tables[0].Rows[i]["Id"].ToString());
                if (resultIraKezbTetel == null || resultIraKezbTetel.Ds.Tables[0].Rows.Count < 1)
                {
                    Assert.Fail("Nem található kézbesítési tétel az irathoz !");
                }

                for (int j = 0; j < resultIraKezbTetel.Ds.Tables[0].Rows.Count; j++)
                {
                    kezbTetelId = resultIraKezbTetel.Ds.Tables[0].Rows[j]["Id"].ToString();

                    resultAtvetel = serviceIraKezbTetel.Atvetel(execParamAtvetel, kezbTetelId);
                    if (!string.IsNullOrEmpty(resultAtvetel.ErrorCode))
                    {
                        Assert.Fail(resultAtvetel.ErrorCode + " - " + resultAtvetel.ErrorMessage);
                    }
                }
            }
            #endregion
        }

        [TestMethod]
        public void IratCsatolmanyUpload()
        {

            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            string ugyiratId = Guid.NewGuid().ToString();
            string iratId = Guid.NewGuid().ToString();
            string kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS
            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, null, "UnitTest EgyszerusitettIratIkt + CsatolmanyUpload");
            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_UGYUGYIRATOK
            EREC_UgyUgyiratok ugyirat = UnitTestModelHelper.GetNewUgyIrat(execParam, "UnitTest EgyszerusitettIratIkt + CsatolmanyUpload");
            ugyirat.Id = ugyiratId;
            #endregion EREC_UGYUGYIRATOK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat(execParam, "UnitTest EgyszerusitettIratIkt + CsatolmanyUpload");
            irat.KuldKuldemenyek_Id = kuldemenyId;
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            #endregion IKTATASIPARAMETEREK

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES + IKTATAS
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.EgyszerusitettIktatasa(
                        execParam,
                        iktatooKonyv_Id,
                        ugyirat,
                        darabok,
                        irat,
                        feladatok,
                        kuldKuldemeny,
                        iktatasParameterek,
                        erkeztetesParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, iratId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyiratId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);

            #region CSATOLAS
            ExecParam execParamUpload = execParam.Clone();
            execParamUpload.Record_Id = iratId;

            EREC_Csatolmanyok csatolmanyok = UnitTestModelHelper.GetERECCsatolmanyokForIrat(iratId);
            Csatolmany csatolmany = UnitTestModelHelper.GetCsatolmany();

            Result resultUpload = iraIratokService.CsatolmanyUpload(execParam, csatolmanyok, csatolmany);
            if (!string.IsNullOrEmpty(resultUpload.ErrorCode))
            {
                Assert.Fail(resultUpload.ErrorCode + " - " + resultUpload.ErrorMessage);
            }
            #endregion

        }
    }
}
