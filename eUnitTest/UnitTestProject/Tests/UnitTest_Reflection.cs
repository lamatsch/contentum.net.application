﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest_Reflection : ContentumBaseTest
    {
        #region REFLECTION
        [TestMethod]
        public void Reflection_EREC_Get()
        {
            ExecuteProxyGet("GetEREC", "Service");
        }

        [TestMethod]
        public void Reflection_KRT_Get()
        {
            ExecuteProxyGet("GetKRT", "Service");
        }

        [TestMethod]
        public void Reflection_EREC_GetAll()
        {
            ExecuteProxyGetAll("GetEREC", "Service");
        }

        [TestMethod]
        public void Reflection_KRT_GetAll()
        {
            ExecuteProxyGetAll("GetKRT", "Service");
        }

        private void ExecuteProxyGet(string serviceNamePrefix, string serviceNamePostFix)
        {
            ExecParam execParam = base.BaseExecParam;
            execParam.Record_Id = Guid.Empty.ToString();

            Type typeA = null;
            Type typeB = null;
            System.Reflection.MethodInfo methodInfoA;
            System.Reflection.MethodInfo methodInfoB;

            object magicClassObjectA = null;

            typeA = eRecordService.ServiceFactory.GetType();
            var methodNames = typeA.GetMethods().Where(m => m.Name.StartsWith(serviceNamePrefix) && m.Name.EndsWith(serviceNamePostFix));
            if (methodNames != null)
                foreach (var method in methodNames)
                {
                    methodInfoA = typeA.GetMethod(method.Name);
                    magicClassObjectA = eRecordService.ServiceFactory;
                    Object serviceObject = methodInfoA.Invoke(magicClassObjectA, null);

                    typeB = serviceObject.GetType();
                    methodInfoB = typeB.GetMethod(UnitTestConstants.ReflectionMethod.Get.ToString());
                    if (methodInfoB != null)
                    {
                        Object resultGet = methodInfoB.Invoke(serviceObject, new object[] { execParam });
                        Trace.WriteLine(string.Format("Executed {0} {1}", methodInfoA.Name, methodInfoB.Name));
                        if (resultGet is Result)
                        {
                            Result result = (Result)resultGet;

                            if (result.ErrorMessage != UnitTestConstants.ErrorMessages.E50101)
                            {
                                Trace.WriteLine(string.Format("FAIL {0} {1}", methodInfoA.Name, methodInfoB.Name));
                                Assert.Fail(string.Format("FAIL {0} {1}", methodInfoA.Name, methodInfoB.Name));
                            }
                            else
                            {
                                Trace.WriteLine(string.Format("Executed {0} {1}", methodInfoA.Name, methodInfoB.Name));
                            }
                        }
                        else
                        {
                            Assert.Fail(string.Format("Internal error {0} {1}", methodInfoA.Name, methodInfoB.Name));
                        }
                    }
                }
        }

        private void ExecuteProxyGetAll(string serviceNamePrefix, string serviceNamePostFix)
        {
            ExecParam execParam = base.BaseExecParam;

            Type typeA = null;
            Type typeB = null;
            System.Reflection.MethodInfo methodInfoA;
            System.Reflection.MethodInfo methodInfoB;

            object magicClassObjectA = null;

            typeA = eRecordService.ServiceFactory.GetType();
            var methodNames = typeA.GetMethods().Where(m => m.Name.StartsWith(serviceNamePrefix) && m.Name.EndsWith(serviceNamePostFix));
            if (methodNames != null)
                foreach (var method in methodNames)
                {
                    methodInfoA = typeA.GetMethod(method.Name);
                    magicClassObjectA = eRecordService.ServiceFactory;
                    Object serviceObject = methodInfoA.Invoke(magicClassObjectA, null);

                    typeB = serviceObject.GetType();
                    methodInfoB = typeB.GetMethod(UnitTestConstants.ReflectionMethod.GetAll.ToString());

                    if (methodInfoB == null || methodInfoB.GetParameters().Count() < 2)
                        continue;

                    ParameterInfo parameterInfoSearchObj = methodInfoB.GetParameters()[1];

                    object parameterSearchObj = Activator.CreateInstance(parameterInfoSearchObj.ParameterType);

                    #region TOPROW
                    FieldInfo field = parameterSearchObj.GetType().GetField("TopRow", BindingFlags.Public | BindingFlags.Instance);
                    if (field != null)
                    {
                        field.SetValue(parameterSearchObj, 100);
                    }
                    #endregion

                    if (methodInfoB != null)
                    {
                        Object resultGet = methodInfoB.Invoke(serviceObject, new object[] { execParam, parameterSearchObj });
                        Trace.WriteLine(string.Format("Executed {0} {1}", methodInfoA.Name, methodInfoB.Name));
                        if (resultGet is Result)
                        {
                            Result result = (Result)resultGet;
                            if (!string.IsNullOrEmpty(result.ErrorMessage))
                            {
                                Trace.WriteLine(string.Format("FAIL {0} {1}  {2}", methodInfoA.Name, methodInfoB.Name, result.ErrorMessage));
                                Assert.Fail(string.Format("FAIL {0} {1}  {2}", methodInfoA.Name, methodInfoB.Name, result.ErrorMessage));
                            }
                            else
                            {
                                Trace.WriteLine(string.Format("Executed {0} {1}", methodInfoA.Name, methodInfoB.Name));
                            }
                        }
                        else
                        {
                            Assert.Fail(string.Format("Internal error {0} {1}", methodInfoA.Name, methodInfoB.Name));
                        }
                    }
                }
        }
        #endregion
    }
}
