﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestProject
{
    [TestClass]
    public partial class UnitTest_Sakkora : ContentumBaseTest
    {
        [TestMethod]
        public void SakkoraFlowJobsStart()
        {
            CallSakkoraElteltIdoNoveles(Guid.Empty.ToString());
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(Guid.Empty.ToString());
        }

        [TestMethod]
        public void SakkoraFlow0()
        {
            ExecParam execParam = UnitTestHelper.GetExecParam();
            if (!IsSakkoraEnabled(execParam))
                return;

            UnitTestConstants.StepNumber actualStep;
            UnitTestConstants.FlowNumber actualFow = UnitTestConstants.FlowNumber.UNTITTEST_SAKKORA_FLOW_0;

            #region IDS
            string ugyirat_Id = Guid.NewGuid().ToString();
            string irat_Id_1 = Guid.NewGuid().ToString();
            string kuldemeny_Id_1 = Guid.NewGuid().ToString();
            #endregion IDS

            int intezesiIdo = (int)UnitTestConstants.IntezesiIdo.I5;
            DateTime beerkezesDatuma = DateTime.Now.AddDays((int)UnitTestConstants.ElteltNapok.ZERO);

            #region ___________STEP 1___________
            actualStep = UnitTestConstants.StepNumber.STEP1;
            DebugStepInfo(actualFow, actualStep);

            BejovoIratIktatasaSakkora(ref ugyirat_Id, ref irat_Id_1, ref kuldemeny_Id_1, beerkezesDatuma, actualFow);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_1);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_1);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyirat_Id);
            #endregion

            #endregion

            #region ___________STEP 2___________
            actualStep = UnitTestConstants.StepNumber.STEP2;
            DebugStepInfo(actualFow, actualStep);

            UpdateUgyIntezesiIdo(execParam, ugyirat_Id, intezesiIdo);
            UpdateIratSakkoraErtek(execParam, irat_Id_1, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy);
            EREC_UgyUgyiratok ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 2
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            DateTime ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            DateTime expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            DateTime hatarido = DateTime.Parse(ugy.Hatarido);
            DateTime expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ZERO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.START, actualStep);

            #endregion

            #endregion

            #region ___________STEP 3___________
            actualStep = UnitTestConstants.StepNumber.STEP3;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 3
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ONE, ugy.ElteltIdo, actualStep);

            #endregion
            #endregion

            #region ___________STEP 4___________
            actualStep = UnitTestConstants.StepNumber.STEP4;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 4
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region UJRASZAMOLAS
            if (!base.NeedInvalidateAll)
            {
                UnitTest_UgyiratFunkciok ugyiratFunc = new UnitTest_UgyiratFunkciok();
                ugyiratFunc.CallSakkoraUgyintezesiIdoUjraSzamolas(ugy.Id);
            }
            #endregion
        }

        [TestMethod]
        public void SakkoraFlow1()
        {
            ExecParam execParam = UnitTestHelper.GetExecParam();
            if (!IsSakkoraEnabled(execParam))
                return;

            UnitTestConstants.StepNumber actualStep;
            UnitTestConstants.FlowNumber actualFow = UnitTestConstants.FlowNumber.UNTITTEST_SAKKORA_FLOW_1;

            #region IDS
            string ugyirat_Id = Guid.NewGuid().ToString();
            string irat_Id_1 = Guid.NewGuid().ToString();
            string kuldemeny_Id_1 = Guid.NewGuid().ToString();
            #endregion IDS

            int intezesiIdo = (int)UnitTestConstants.IntezesiIdo.I5;
            DateTime beerkezesDatuma = DateTime.Now.AddDays((int)UnitTestConstants.ElteltNapok.ZERO);

            #region ___________STEP 1___________
            actualStep = UnitTestConstants.StepNumber.STEP1;
            DebugStepInfo(actualFow, actualStep);

            BejovoIratIktatasaSakkora(ref ugyirat_Id, ref irat_Id_1, ref kuldemeny_Id_1, beerkezesDatuma, actualFow);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_1);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_1);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyirat_Id);
            #endregion
            #endregion

            #region ___________STEP 2___________
            actualStep = UnitTestConstants.StepNumber.STEP2;
            DebugStepInfo(actualFow, actualStep);

            UpdateUgyIntezesiIdo(execParam, ugyirat_Id, intezesiIdo);
            UpdateIratSakkoraErtek(execParam, irat_Id_1, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy);
            EREC_UgyUgyiratok ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 2
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            DateTime ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            DateTime expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            DateTime hatarido = DateTime.Parse(ugy.Hatarido);
            DateTime expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ZERO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.START, actualStep);

            #endregion

            #endregion

            #region ___________STEP 3___________
            actualStep = UnitTestConstants.StepNumber.STEP3;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 3
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ONE, ugy.ElteltIdo, actualStep);

            #endregion
            #endregion

            #region ___________STEP 4___________
            actualStep = UnitTestConstants.StepNumber.STEP4;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 4
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region UJRASZAMOLAS
            if (!base.NeedInvalidateAll)
            {
                UnitTest_UgyiratFunkciok ugyiratFunc = new UnitTest_UgyiratFunkciok();
                ugyiratFunc.CallSakkoraUgyintezesiIdoUjraSzamolas(ugy.Id);
            }
            #endregion
        }

        [TestMethod]
        public void SakkoraFlow2()
        {
            ExecParam execParam = UnitTestHelper.GetExecParam();
            if (!IsSakkoraEnabled(execParam))
                return;

            UnitTestConstants.StepNumber actualStep;
            UnitTestConstants.FlowNumber actualFow = UnitTestConstants.FlowNumber.UNTITTEST_SAKKORA_FLOW_2;

            #region IDS
            string ugyirat_Id = Guid.NewGuid().ToString();
            string irat_Id_1 = Guid.NewGuid().ToString();
            string kuldemeny_Id_1 = Guid.NewGuid().ToString();
            #endregion IDS

            int intezesiIdo = (int)UnitTestConstants.IntezesiIdo.I5;
            DateTime beerkezesDatuma = DateTime.Now.AddDays((int)UnitTestConstants.ElteltNapok.ZERO);

            #region ___________STEP 1___________
            actualStep = UnitTestConstants.StepNumber.STEP1;
            DebugStepInfo(actualFow, actualStep);

            BejovoIratIktatasaSakkora(ref ugyirat_Id, ref irat_Id_1, ref kuldemeny_Id_1, beerkezesDatuma, actualFow);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_1);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_1);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyirat_Id);
            #endregion
            #endregion

            #region ___________STEP 2___________
            actualStep = UnitTestConstants.StepNumber.STEP2;
            DebugStepInfo(actualFow, actualStep);

            UpdateUgyIntezesiIdo(execParam, ugyirat_Id, intezesiIdo);
            UpdateIratSakkoraErtek(execParam, irat_Id_1, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy);
            EREC_UgyUgyiratok ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 2
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            DateTime ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            DateTime expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            DateTime hatarido = DateTime.Parse(ugy.Hatarido);
            DateTime expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ZERO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.START, actualStep);
            #endregion

            #endregion

            #region ___________STEP 3___________
            actualStep = UnitTestConstants.StepNumber.STEP3;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 3
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ONE, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region ___________STEP 4___________
            actualStep = UnitTestConstants.StepNumber.STEP4;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 4
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 5___________
            actualStep = UnitTestConstants.StepNumber.STEP5;
            DebugStepInfo(actualFow, actualStep);

            UnitTest_IktatasAlszamra alszamFunc = new UnitTest_IktatasAlszamra();
            string irat_Id_2;
            string kuldemeny_Id_2;
            alszamFunc.CallBelsoIratIktatasa_Alszamra(ugyirat_Id, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ketto, beerkezesDatuma, actualFow, KodTarak.POSTAZAS_IRANYA.Kimeno, false, out irat_Id_2, out kuldemeny_Id_2);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_2);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_2);
            #endregion

            #region CHECKS 5
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            Assert.AreEqual(expectedHatarido.Year, hatarido.Year);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 6___________
            actualStep = UnitTestConstants.StepNumber.STEP6;
            DebugStepInfo(actualFow, actualStep);

            UnitTest_IratPldFunkciok iratPldFunc = new UnitTest_IratPldFunkciok();
            string iratPld_Id_2 = string.Empty;
            iratPldFunc.CallExpedial(irat_Id_2, out iratPld_Id_2);

            UnitTest_KuldemenyFunkciok kuldFunc = new UnitTest_KuldemenyFunkciok();
            kuldFunc.CallPostaz(iratPld_Id_2);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 6
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.STOP, actualStep);

            #endregion
            #endregion

            #region ___________STEP 7___________
            actualStep = UnitTestConstants.StepNumber.STEP7;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 7
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo + 1);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ONE, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);

            #endregion
            #endregion

            #region UJRASZAMOLAS
            if (!base.NeedInvalidateAll)
            {
                UnitTest_UgyiratFunkciok ugyiratFunc = new UnitTest_UgyiratFunkciok();
                ugyiratFunc.CallSakkoraUgyintezesiIdoUjraSzamolas(ugy.Id);
            }
            #endregion
        }

        [TestMethod]
        public void SakkoraFlow3()
        {
            ExecParam execParam = UnitTestHelper.GetExecParam();
            if (!IsSakkoraEnabled(execParam))
                return;

            UnitTestConstants.StepNumber actualStep;
            UnitTestConstants.FlowNumber actualFow = UnitTestConstants.FlowNumber.UNTITTEST_SAKKORA_FLOW_3;

            #region IDS
            string ugyirat_Id = Guid.NewGuid().ToString();
            string irat_Id_1 = Guid.NewGuid().ToString();
            string kuldemeny_Id_1 = Guid.NewGuid().ToString();
            #endregion IDS

            int intezesiIdo = (int)UnitTestConstants.IntezesiIdo.I5;
            DateTime beerkezesDatuma = DateTime.Now.AddDays((int)UnitTestConstants.ElteltNapok.ZERO);

            #region ___________STEP 1___________
            actualStep = UnitTestConstants.StepNumber.STEP1;

            BejovoIratIktatasaSakkora(ref ugyirat_Id, ref irat_Id_1, ref kuldemeny_Id_1, beerkezesDatuma, actualFow);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_1);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_1);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyirat_Id);
            #endregion

            #endregion

            #region ___________STEP 2___________
            actualStep = UnitTestConstants.StepNumber.STEP2;
            DebugStepInfo(actualFow, actualStep);

            UpdateUgyIntezesiIdo(execParam, ugyirat_Id, intezesiIdo);
            UpdateIratSakkoraErtek(execParam, irat_Id_1, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy);
            EREC_UgyUgyiratok ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 2
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            DateTime ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            DateTime expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            DateTime hatarido = DateTime.Parse(ugy.Hatarido);
            DateTime expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ZERO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.START, actualStep);

            #endregion

            #endregion

            #region ___________STEP 3___________
            actualStep = UnitTestConstants.StepNumber.STEP3;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 3
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ONE, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region ___________STEP 4___________
            actualStep = UnitTestConstants.StepNumber.STEP4;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 4
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 5___________
            actualStep = UnitTestConstants.StepNumber.STEP5;
            DebugStepInfo(actualFow, actualStep);

            UnitTest_IktatasAlszamra alszamFunc = new UnitTest_IktatasAlszamra();
            string irat_Id_2;
            string kuldemeny_Id_2;
            alszamFunc.CallBelsoIratIktatasa_Alszamra(ugyirat_Id, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ketto, beerkezesDatuma, actualFow, KodTarak.POSTAZAS_IRANYA.Kimeno, false, out irat_Id_2, out kuldemeny_Id_2);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_2);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_2);
            #endregion

            #region CHECKS 5
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region ___________STEP 6___________
            actualStep = UnitTestConstants.StepNumber.STEP6;
            DebugStepInfo(actualFow, actualStep);

            UnitTest_IratPldFunkciok iratPldFunc = new UnitTest_IratPldFunkciok();
            string iratPld_Id_2 = string.Empty;
            iratPldFunc.CallExpedial(irat_Id_2, out iratPld_Id_2);

            UnitTest_KuldemenyFunkciok kuldFunc = new UnitTest_KuldemenyFunkciok();
            kuldFunc.CallPostaz(iratPld_Id_2);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 6
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.STOP, actualStep);

            #endregion
            #endregion

            #region ___________STEP 7___________
            actualStep = UnitTestConstants.StepNumber.STEP7;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 7
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo + 1);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ONE, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 8___________
            actualStep = UnitTestConstants.StepNumber.STEP8;
            DebugStepInfo(actualFow, actualStep);

            int intezesiIdoStep8 = (int)UnitTestConstants.IntezesiIdo.I5;
            DateTime beerkezesDatumaStep8 = DateTime.Now.AddDays((int)UnitTestConstants.ElteltNapok.ONE);
            string irat_Id_3 = Guid.NewGuid().ToString();
            string kuldemeny_Id_3 = Guid.NewGuid().ToString();

            BejovoIratIktatasaAlszamraSakkora(ugyirat_Id, ref irat_Id_3, ref kuldemeny_Id_3, beerkezesDatumaStep8, actualFow);
            UpdateUgyIntezesiIdo(execParam, ugyirat_Id, intezesiIdoStep8);
            UpdateIratSakkoraErtek(execParam, irat_Id_3, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_3);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_3);
            #endregion

            #region CHECKS 8
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ZERO, ugy.ElteltIdo, actualStep);
            #endregion

            #endregion

            #region ___________STEP 9___________
            actualStep = UnitTestConstants.StepNumber.STEP9;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 9
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ONE, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 10___________
            actualStep = UnitTestConstants.StepNumber.STEP10;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 10
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region ___________STEP 11___________
            actualStep = UnitTestConstants.StepNumber.STEP11;
            DebugStepInfo(actualFow, actualStep);

            string irat_Id_4;
            string kuldemeny_Id_4;
            alszamFunc.CallBelsoIratIktatasa_Alszamra(ugyirat_Id, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Harom, beerkezesDatuma, actualFow, KodTarak.POSTAZAS_IRANYA.Kimeno, false, out irat_Id_4, out kuldemeny_Id_4);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_4);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_4);
            #endregion

            #region CHECKS 11
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 12___________
            actualStep = UnitTestConstants.StepNumber.STEP12;
            DebugStepInfo(actualFow, actualStep);

            string iratPld_Id_4 = string.Empty;
            iratPldFunc.CallExpedial(irat_Id_4, out iratPld_Id_4);

            kuldFunc.CallPostaz(iratPld_Id_4);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 12
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.STOP, actualStep);

            #endregion
            #endregion


            #region ___________STEP 13___________
            actualStep = UnitTestConstants.StepNumber.STEP13;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 13
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);

            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region UJRASZAMOLAS
            if (!base.NeedInvalidateAll)
            {
                UnitTest_UgyiratFunkciok ugyiratFunc = new UnitTest_UgyiratFunkciok();
                ugyiratFunc.CallSakkoraUgyintezesiIdoUjraSzamolas(ugy.Id);
            }
            #endregion
        }

        [TestMethod]
        public void SakkoraFlow4()
        {
            ExecParam execParam = UnitTestHelper.GetExecParam();
            if (!IsSakkoraEnabled(execParam))
                return;

            UnitTestConstants.StepNumber actualStep;
            UnitTestConstants.FlowNumber actualFow = UnitTestConstants.FlowNumber.UNTITTEST_SAKKORA_FLOW_4;

            #region IDS
            string ugyirat_Id = Guid.NewGuid().ToString();
            string irat_Id_1 = Guid.NewGuid().ToString();
            string kuldemeny_Id_1 = Guid.NewGuid().ToString();
            #endregion IDS

            int intezesiIdo = (int)UnitTestConstants.IntezesiIdo.I5;
            DateTime beerkezesDatuma = DateTime.Now.AddDays((int)UnitTestConstants.ElteltNapok.ZERO);

            #region ___________STEP 1___________
            actualStep = UnitTestConstants.StepNumber.STEP1;
            DebugStepInfo(actualFow, actualStep);

            BejovoIratIktatasaSakkora(ref ugyirat_Id, ref irat_Id_1, ref kuldemeny_Id_1, beerkezesDatuma, actualFow);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_1);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_1);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyirat_Id);
            #endregion

            #endregion

            #region ___________STEP 2___________
            actualStep = UnitTestConstants.StepNumber.STEP2;
            DebugStepInfo(actualFow, actualStep);

            UpdateUgyIntezesiIdo(execParam, ugyirat_Id, intezesiIdo);
            UpdateIratSakkoraErtek(execParam, irat_Id_1, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy);
            EREC_UgyUgyiratok ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 2
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            DateTime ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            DateTime expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            DateTime hatarido = DateTime.Parse(ugy.Hatarido);
            DateTime expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ZERO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.START, actualStep);

            #endregion

            #endregion

            #region ___________STEP 3___________
            actualStep = UnitTestConstants.StepNumber.STEP3;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 3
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ONE, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region ___________STEP 4___________
            actualStep = UnitTestConstants.StepNumber.STEP4;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 4
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 5___________
            actualStep = UnitTestConstants.StepNumber.STEP5;
            DebugStepInfo(actualFow, actualStep);

            UnitTest_IktatasAlszamra alszamFunc = new UnitTest_IktatasAlszamra();
            string irat_Id_2;
            string kuldemeny_Id_2;
            alszamFunc.CallBelsoIratIktatasa_Alszamra(ugyirat_Id, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ketto, beerkezesDatuma, actualFow, KodTarak.POSTAZAS_IRANYA.Kimeno, false, out irat_Id_2, out kuldemeny_Id_2);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_2);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_2);
            #endregion

            #region CHECKS 5
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region ___________STEP 6___________
            actualStep = UnitTestConstants.StepNumber.STEP6;
            DebugStepInfo(actualFow, actualStep);

            UnitTest_IratPldFunkciok iratPldFunc = new UnitTest_IratPldFunkciok();
            string iratPld_Id_2 = string.Empty;
            iratPldFunc.CallExpedial(irat_Id_2, out iratPld_Id_2);

            UnitTest_KuldemenyFunkciok kuldFunc = new UnitTest_KuldemenyFunkciok();
            kuldFunc.CallPostaz(iratPld_Id_2);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 6
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.STOP, actualStep);

            #endregion
            #endregion

            #region ___________STEP 7___________
            actualStep = UnitTestConstants.StepNumber.STEP7;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 7
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo + 1);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ONE, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 8___________
            actualStep = UnitTestConstants.StepNumber.STEP8;
            DebugStepInfo(actualFow, actualStep);

            int intezesiIdoStep8 = (int)UnitTestConstants.IntezesiIdo.I5;
            DateTime beerkezesDatumaStep8 = DateTime.Now.AddDays((int)UnitTestConstants.ElteltNapok.ONE);
            string irat_Id_3 = Guid.NewGuid().ToString();
            string kuldemeny_Id_3 = Guid.NewGuid().ToString();

            BejovoIratIktatasaAlszamraSakkora(ugyirat_Id, ref irat_Id_3, ref kuldemeny_Id_3, beerkezesDatumaStep8, actualFow);
            UpdateUgyIntezesiIdo(execParam, ugyirat_Id, intezesiIdoStep8);
            UpdateIratSakkoraErtek(execParam, irat_Id_3, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_3);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_3);
            #endregion

            #region CHECKS 8
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ZERO, ugy.ElteltIdo, actualStep);
            #endregion

            #endregion

            #region ___________STEP 9___________
            actualStep = UnitTestConstants.StepNumber.STEP9;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 9
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ONE, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 10___________
            actualStep = UnitTestConstants.StepNumber.STEP10;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 10
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region ___________STEP 11___________
            actualStep = UnitTestConstants.StepNumber.STEP11;
            DebugStepInfo(actualFow, actualStep);

            string irat_Id_4;
            string kuldemeny_Id_4;
            alszamFunc.CallBelsoIratIktatasa_Alszamra(ugyirat_Id, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Harom, beerkezesDatuma, actualFow, KodTarak.POSTAZAS_IRANYA.Kimeno, false, out irat_Id_4, out kuldemeny_Id_4);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_4);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_4);
            #endregion

            #region CHECKS 11
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 12___________
            actualStep = UnitTestConstants.StepNumber.STEP12;
            DebugStepInfo(actualFow, actualStep);

            string iratPld_Id_4 = string.Empty;
            iratPldFunc.CallExpedial(irat_Id_4, out iratPld_Id_4);

            kuldFunc.CallPostaz(iratPld_Id_4);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 12
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.STOP, actualStep);

            #endregion
            #endregion


            #region ___________STEP 13___________
            actualStep = UnitTestConstants.StepNumber.STEP13;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 13
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);

            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region ___________STEP 14___________
            actualStep = UnitTestConstants.StepNumber.STEP14;
            DebugStepInfo(actualFow, actualStep);

            int intezesiIdoStep14 = (int)UnitTestConstants.IntezesiIdo.I5;
            DateTime beerkezesDatumaStep14 = DateTime.Now.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            string irat_Id_5 = Guid.NewGuid().ToString();
            string kuldemeny_Id_5 = Guid.NewGuid().ToString();

            BejovoIratIktatasaAlszamraSakkora(ugyirat_Id, ref irat_Id_5, ref kuldemeny_Id_5, beerkezesDatumaStep14, actualFow);
            UpdateUgyIntezesiIdo(execParam, ugyirat_Id, intezesiIdoStep14);
            UpdateIratSakkoraErtek(execParam, irat_Id_5, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_5);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_5);
            #endregion

            #region CHECKS 14
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatumaStep14.AddDays(1);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep14).AddDays((int)UnitTestConstants.ElteltNapok.FIVE);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep14, (int)UnitTestConstants.HatralevoNapok.FIVE, ugy.HatralevoNapok, actualStep);

            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ZERO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.START, actualStep);

            #endregion
            #endregion

            #region UJRASZAMOLAS
            if (!base.NeedInvalidateAll)
            {
                UnitTest_UgyiratFunkciok ugyiratFunc = new UnitTest_UgyiratFunkciok();
                ugyiratFunc.CallSakkoraUgyintezesiIdoUjraSzamolas(ugy.Id);
            }
            #endregion
        }

        [TestMethod]
        public void SakkoraFlowUjrainditas()
        {
            ExecParam execParam = UnitTestHelper.GetExecParam();
            if (!IsSakkoraEnabled(execParam))
                return;

            UnitTestConstants.StepNumber actualStep;
            UnitTestConstants.FlowNumber actualFow = UnitTestConstants.FlowNumber.UNTITTEST_SAKKORA_FLOW_UJRAINDITAS;

            #region IDS
            string ugyirat_Id = Guid.NewGuid().ToString();
            string irat_Id_1 = Guid.NewGuid().ToString();
            string kuldemeny_Id_1 = Guid.NewGuid().ToString();
            #endregion IDS

            int intezesiIdo = (int)UnitTestConstants.IntezesiIdo.I5;
            DateTime beerkezesDatuma = DateTime.Now.AddDays((int)UnitTestConstants.ElteltNapok.ZERO);

            #region ___________STEP 1___________
            actualStep = UnitTestConstants.StepNumber.STEP1;
            DebugStepInfo(actualFow, actualStep);

            BejovoIratIktatasaSakkora(ref ugyirat_Id, ref irat_Id_1, ref kuldemeny_Id_1, beerkezesDatuma, actualFow);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_1);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_1);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyirat_Id);
            #endregion

            #endregion

            #region ___________STEP 2___________
            actualStep = UnitTestConstants.StepNumber.STEP2;
            DebugStepInfo(actualFow, actualStep);

            UpdateUgyIntezesiIdo(execParam, ugyirat_Id, intezesiIdo);
            UpdateIratSakkoraErtek(execParam, irat_Id_1, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy);
            EREC_UgyUgyiratok ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 2
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            DateTime ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            DateTime expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            DateTime hatarido = DateTime.Parse(ugy.Hatarido);
            DateTime expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ZERO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.START, actualStep);

            #endregion

            #endregion

            #region ___________STEP 3___________
            actualStep = UnitTestConstants.StepNumber.STEP3;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 3
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ONE, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region ___________STEP 4___________
            actualStep = UnitTestConstants.StepNumber.STEP4;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 4
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 5___________
            actualStep = UnitTestConstants.StepNumber.STEP5;
            DebugStepInfo(actualFow, actualStep);

            UnitTest_IktatasAlszamra alszamFunc = new UnitTest_IktatasAlszamra();
            string irat_Id_2;
            string kuldemeny_Id_2;
            alszamFunc.CallBelsoIratIktatasa_Alszamra(ugyirat_Id, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ketto, beerkezesDatuma, actualFow, KodTarak.POSTAZAS_IRANYA.Kimeno, false, out irat_Id_2, out kuldemeny_Id_2);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_2);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_2);
            #endregion

            #region CHECKS 5
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 6___________
            actualStep = UnitTestConstants.StepNumber.STEP6;
            DebugStepInfo(actualFow, actualStep);

            UnitTest_IratPldFunkciok iratPldFunc = new UnitTest_IratPldFunkciok();
            string iratPld_Id_2 = string.Empty;
            iratPldFunc.CallExpedial(irat_Id_2, out iratPld_Id_2);

            UnitTest_KuldemenyFunkciok kuldFunc = new UnitTest_KuldemenyFunkciok();
            kuldFunc.CallPostaz(iratPld_Id_2);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 6
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ZERO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.STOP, actualStep);

            #endregion
            #endregion

            #region ___________STEP 7___________
            actualStep = UnitTestConstants.StepNumber.STEP7;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 7
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatuma;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = beerkezesDatuma.AddDays(intezesiIdo + 1);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.ONE, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 8___________
            actualStep = UnitTestConstants.StepNumber.STEP8;
            DebugStepInfo(actualFow, actualStep);

            int intezesiIdoStep8 = (int)UnitTestConstants.IntezesiIdo.I5;
            DateTime beerkezesDatumaStep8 = DateTime.Now.AddDays((int)UnitTestConstants.ElteltNapok.ONE);
            string irat_Id_3 = Guid.NewGuid().ToString();
            string kuldemeny_Id_3 = Guid.NewGuid().ToString();

            BejovoIratIktatasaAlszamraSakkora(ugyirat_Id, ref irat_Id_3, ref kuldemeny_Id_3, beerkezesDatumaStep8, actualFow);
            UpdateUgyIntezesiIdo(execParam, ugyirat_Id, intezesiIdoStep8);
            UpdateIratSakkoraErtek(execParam, irat_Id_3, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_3);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_3);
            #endregion

            #region CHECKS 8
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ZERO, ugy.ElteltIdo, actualStep);
            #endregion

            #endregion

            #region ___________STEP 9___________
            actualStep = UnitTestConstants.StepNumber.STEP9;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 9
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ONE, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 10___________
            actualStep = UnitTestConstants.StepNumber.STEP10;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 10
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region ___________STEP 11___________
            actualStep = UnitTestConstants.StepNumber.STEP11;
            DebugStepInfo(actualFow, actualStep);

            string irat_Id_4;
            string kuldemeny_Id_4;
            alszamFunc.CallBelsoIratIktatasa_Alszamra(ugyirat_Id, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Harom, beerkezesDatuma, actualFow, KodTarak.POSTAZAS_IRANYA.Kimeno, false, out irat_Id_4, out kuldemeny_Id_4);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_4);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_4);
            #endregion

            #region CHECKS 11
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion


            #region ___________STEP 12___________
            actualStep = UnitTestConstants.StepNumber.STEP12;
            DebugStepInfo(actualFow, actualStep);

            string iratPld_Id_4 = string.Empty;
            iratPldFunc.CallExpedial(irat_Id_4, out iratPld_Id_4);

            kuldFunc.CallPostaz(iratPld_Id_4);

            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 12
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);
            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);

            CheckSakkoraElteltIdoAllapot(ugy.ElteltidoAllapot, KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.STOP, actualStep);

            #endregion
            #endregion


            #region ___________STEP 13___________
            actualStep = UnitTestConstants.StepNumber.STEP13;
            DebugStepInfo(actualFow, actualStep);

            CallSakkoraElteltIdoNoveles(ugy.Id);
            CallSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ugy.Id);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CHECKS 13
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = DateTime.Today.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep8).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep8, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);

            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.TWO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region ___________STEP 14___________
            actualStep = UnitTestConstants.StepNumber.STEP14;
            DebugStepInfo(actualFow, actualStep);

            int intezesiIdoStep14 = (int)UnitTestConstants.IntezesiIdo.I5;
            DateTime beerkezesDatumaStep14 = DateTime.Now.AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            string irat_Id_5 = Guid.NewGuid().ToString();
            string kuldemeny_Id_5 = Guid.NewGuid().ToString();

            BejovoIratIktatasaAlszamraSakkora(ugyirat_Id, ref irat_Id_5, ref kuldemeny_Id_5, beerkezesDatumaStep14, actualFow);
            UpdateUgyIntezesiIdo(execParam, ugyirat_Id, intezesiIdoStep14);
            UpdateIratSakkoraErtek(execParam, irat_Id_5, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemeny_Id_5);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, irat_Id_5);
            #endregion

            #region CHECKS 14
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatumaStep14.AddDays(1);
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep14).AddDays((int)UnitTestConstants.ElteltNapok.FIVE);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdoStep14, (int)UnitTestConstants.HatralevoNapok.FIVE, ugy.HatralevoNapok, actualStep);

            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ZERO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region ___________STEP 15___________
            actualStep = UnitTestConstants.StepNumber.STEP15;
            DebugStepInfo(actualFow, actualStep);

            UpdateIratSakkoraErtek(execParam, irat_Id_4, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE);
            UpdateIratSakkoraErtek(execParam, irat_Id_5, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE);
            ugy = GetUgyirat(execParam, ugyirat_Id);

            EREC_IraIratok irat_1 = GetIrat(execParam, irat_Id_1);

            #region CHECKS 15
            CheckSakkoraUgyintezesKezdeteUres(ugy.UgyintezesKezdete, actualStep);
            ugyKezdete = DateTime.Parse(ugy.UgyintezesKezdete);
            expectedKezdete = beerkezesDatumaStep14;
            CheckSakkoraDate(expectedKezdete, ugyKezdete, actualStep, "UGY.KEZDETE");

            CheckSakkoraHataridoUres(ugy.Hatarido, actualStep);
            hatarido = DateTime.Parse(ugy.Hatarido);
            expectedHatarido = DateTime.Today.AddDays(intezesiIdoStep14).AddDays((int)UnitTestConstants.ElteltNapok.TWO);
            CheckSakkoraDate(expectedHatarido, hatarido, actualStep, "UGY.HATARIDO");

            CheckSakkoraHatralevoNapok(intezesiIdo, (int)UnitTestConstants.HatralevoNapok.TWO, ugy.HatralevoNapok, actualStep);

            CheckSakkoraElteltIdo(UnitTestConstants.ElteltNapok.ZERO, ugy.ElteltIdo, actualStep);
            #endregion
            #endregion

            #region UJRASZAMOLAS
            if (!base.NeedInvalidateAll)
            {
                UnitTest_UgyiratFunkciok ugyiratFunc = new UnitTest_UgyiratFunkciok();
                ugyiratFunc.CallSakkoraUgyintezesiIdoUjraSzamolas(ugy.Id);
            }
            #endregion
        }
    }
}
