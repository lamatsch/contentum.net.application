﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest_ListSelect : ContentumBaseTest
    {
        #region GETALL
        [TestMethod]
        public void GetAllKuldemeny()
        {
            #region INIT
            ExecParam execParam = base.BaseExecParam;
            EREC_KuldKuldemenyekSearch src = new EREC_KuldKuldemenyekSearch();
            src.Id.Value = Guid.Empty.ToString();
            src.Id.Operator = Query.Operators.equals;

            src.BeerkezesIdeje.Value = DateTime.Now.ToString();
            src.BeerkezesIdeje.Operator = Query.Operators.equals;

            src.HivatkozasiSzam.Value = DateTime.Now.ToString();
            src.HivatkozasiSzam.Operator = Query.Operators.equals;

            src.Targy.Value = DateTime.Now.ToString();
            src.Targy.Operator = Query.Operators.equals;

            src.IraIktatokonyv_Id.Value = Guid.Empty.ToString();
            src.IraIktatokonyv_Id.Operator = Query.Operators.equals;

            src.NevSTR_Bekuldo.Value = "UnitTest bekuldo";
            src.NevSTR_Bekuldo.Operator = Query.Operators.equals;

            src.CimSTR_Bekuldo.Value = "UnitTest cim";
            src.CimSTR_Bekuldo.Operator = Query.Operators.equals;

            src.Surgosseg.Value = KodTarak.SURGOSSEG.Normal;
            src.Surgosseg.Operator = Query.Operators.equals;

            src.UgyintezesModja.Value = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            src.UgyintezesModja.Operator = Query.Operators.equals;

            src.Erkeztetes_Ev.Value = DateTime.Now.Year.ToString();
            src.Erkeztetes_Ev.Operator = Query.Operators.equals;

            src.PeldanySzam.Value = "1";
            src.PeldanySzam.Operator = Query.Operators.equals;

            src.Munkaallomas.Value = "UnitTest Munkaallomas";
            src.Munkaallomas.Operator = Query.Operators.equals;

            src.FelhasznaloCsoport_Id_Bonto.Value = execParam.Felhasznalo_Id;
            src.FelhasznaloCsoport_Id_Bonto.Operator = Query.Operators.equals;

            src.CimzesTipusa.Value = KodTarak.KULD_CIMZES_TIPUS.nevre_szolo;
            src.CimzesTipusa.Operator = Query.Operators.equals;

            src.KezbesitesModja.Value = KodTarak.KULD_KEZB_MODJA.elektronikus_uton_Adatkapu;
            src.KezbesitesModja.Operator = Query.Operators.equals;

            src.Azonosito.Value = DateTime.Now.ToString();
            src.Azonosito.Operator = Query.Operators.equals;

            src.IktatniKell.Value = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
            src.IktatniKell.Operator = Query.Operators.equals;

            src.Tipus.Value = KodTarak.KULDEMENY_TIPUS.Szerzodes;
            src.Tipus.Operator = Query.Operators.equals;

            src.FelhasznaloCsoport_Id_Bonto.Value = execParam.Felhasznalo_Id;
            src.FelhasznaloCsoport_Id_Bonto.Operator = Query.Operators.equals;

            src.FelbontasDatuma.Value = src.BeerkezesIdeje.Value;
            src.FelbontasDatuma.Operator = Query.Operators.equals;

            src.BelyegzoDatuma.Value = DateTime.Now.ToString();
            src.BelyegzoDatuma.Operator = Query.Operators.equals;

            src.PostazasIranya.Value = KodTarak.POSTAZAS_IRANYA.Bejovo;
            src.PostazasIranya.Operator = Query.Operators.equals;

            #endregion

            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            Result result = service.GetAll(execParam, src);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }
        }

        [TestMethod]
        public void GetAllIrat()
        {
            #region INIT
            ExecParam execParam = base.BaseExecParam;

            EREC_IraIratokSearch src = new EREC_IraIratokSearch();
            src.Targy.Value = DateTime.Now.ToShortDateString();
            src.Targy.Operator = Query.Operators.equals;

            src.KiadmanyozniKell.Value = "0";
            src.KiadmanyozniKell.Operator = Query.Operators.equals;

            src.ExpedialasModja.Value = KodTarak.KULDEMENY_KULDES_MODJA.Elhisz;
            src.ExpedialasModja.Operator = Query.Operators.equals;

            src.FelhasznaloCsoport_Id_Ugyintez.Value = execParam.Felhasznalo_Id;
            src.FelhasznaloCsoport_Id_Ugyintez.Operator = Query.Operators.equals;

            src.Irattipus.Value = KodTarak.IRATTIPUS.Hivatalbol;
            src.Irattipus.Operator = Query.Operators.equals;

            src.AdathordozoTipusa.Value = KodTarak.AdathordozoTipus.Egyeb;
            src.AdathordozoTipusa.Operator = Query.Operators.equals;

            src.Jelleg.Value = KodTarak.IRAT_JELLEG.Webes;
            src.Jelleg.Operator = Query.Operators.equals;

            src.Ugy_Fajtaja.Value = KodTarak.UGY_FAJTAJA.AllamigazgatasiUgy;
            src.Ugy_Fajtaja.Operator = Query.Operators.equals;
            #endregion
           
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = service.GetAll(execParam, src);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }
        }

        [TestMethod]
        public void GetAllUgyIrat()
        {
            #region INIT
            ExecParam execParam = base.BaseExecParam;

            EREC_UgyUgyiratokSearch src = new EREC_UgyUgyiratokSearch();

            src.Targy.Value = DateTime.Now.ToShortDateString();
            src.Targy.Operator = Query.Operators.equals;

            src.Hatarido.Value = DateTime.Now.AddDays(30).ToString();
            src.Hatarido.Operator = Query.Operators.equals;

            src.Targy.Value = DateTime.Now.ToShortDateString();
            src.Targy.Operator = Query.Operators.equals;

            src.UgyTipus.Value = KodTarak.UGYTIPUS.KozgyulesiEloterjesztes;
            src.UgyTipus.Operator = Query.Operators.equals;

            //src.IraIrattariTetel_Id.Value = "";
            //src.IraIrattariTetel_Id.Operator = Query.Operators.equals;

            src.NevSTR_Ugyindito.Value = "UnitTest Ugyindito";
            src.NevSTR_Ugyindito.Operator = Query.Operators.equals;

            src.Csoport_Id_Felelos.Value = execParam.Felhasznalo_Id;
            src.Csoport_Id_Felelos.Operator = Query.Operators.equals;

            src.FelhasznaloCsoport_Id_Ugyintez.Value = execParam.FelhasznaloSzervezet_Id;
            src.FelhasznaloCsoport_Id_Ugyintez.Operator = Query.Operators.equals;

            src.IntezesiIdo.Value = "30";
            src.IntezesiIdo.Operator = Query.Operators.equals;

            src.IntezesiIdoegyseg.Value = KodTarak.IDOEGYSEG.Nap;
            src.IntezesiIdoegyseg.Operator = Query.Operators.equals;

            src.FelhasznaloCsoport_Id_Ugyintez.Value = execParam.Felhasznalo_Id;
            src.FelhasznaloCsoport_Id_Ugyintez.Operator = Query.Operators.equals;

            src.Jelleg.Value = KodTarak.IRAT_JELLEG.Webes;
            src.Jelleg.Operator = Query.Operators.equals;

            src.Ugy_Fajtaja.Value = KodTarak.UGY_FAJTAJA.AllamigazgatasiUgy;
            src.Ugy_Fajtaja.Operator = Query.Operators.equals;
            #endregion

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            Result result = service.GetAll(execParam, src);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }
        }

        [TestMethod]
        public void GetAllPldIratPeldanyok()
        {
            #region INIT
            ExecParam execParam = base.BaseExecParam;

            EREC_PldIratPeldanyokSearch src = new EREC_PldIratPeldanyokSearch();
            src.Allapot.Value = DateTime.Now.ToShortDateString();
            src.Allapot.Operator = Query.Operators.equals;

            src.AtvetelDatuma.Value = DateTime.Now.AddDays(30).ToString();
            src.AtvetelDatuma.Operator = Query.Operators.equals;

            src.Azonosito.Value = "0";
            src.Azonosito.Operator = Query.Operators.equals;

            src.BarCode.Value = "0";
            src.BarCode.Operator = Query.Operators.equals;

            src.CimSTR_Cimzett.Value = "0";
            src.CimSTR_Cimzett.Operator = Query.Operators.equals;

            src.Cim_id_Cimzett.Value = Guid.Empty.ToString();
            src.Cim_id_Cimzett.Operator = Query.Operators.equals;

            src.CsakAktivIrat = true;

            src.Csoporttagokkal = true;

            src.Csoport_Id_Felelos.Value = Guid.Empty.ToString();
            src.Csoport_Id_Felelos.Operator = Query.Operators.equals;

            src.Csoport_Id_Felelos_Elozo.Value = "30";
            src.Csoport_Id_Felelos_Elozo.Operator = Query.Operators.equals;

            src.Eredet.Value = KodTarak.IDOEGYSEG.Nap;
            src.Eredet.Operator = Query.Operators.equals;

            src.FelhasznaloCsoport_Id_Orzo.Value = Guid.Empty.ToString();
            src.FelhasznaloCsoport_Id_Orzo.Operator = Query.Operators.equals;

            src.FelhCsoport_Id_Selejtezo.Value = Guid.Empty.ToString();
            src.FelhCsoport_Id_Selejtezo.Operator = Query.Operators.equals;

            src.Fizikai_Kezbesitesi_Allapot.Value = "0";
            src.Fizikai_Kezbesitesi_Allapot.Operator = Query.Operators.equals;

            src.Gener_Id.Value = Guid.Empty.ToString();
            src.Gener_Id.Operator = Query.Operators.equals;

            src.Id.Value = Guid.Empty.ToString();
            src.Id.Operator = Query.Operators.equals;

            src.IraIrat_Id.Value = Guid.Empty.ToString();
            src.IraIrat_Id.Operator = Query.Operators.equals;

            src.IraIrat_Id_Kapcsolt.Value = Guid.Empty.ToString();
            src.IraIrat_Id_Kapcsolt.Operator = Query.Operators.equals;

            src.IrattariHely.Value = "0";
            src.IrattariHely.Operator = Query.Operators.equals;

            src.IsTovabbitasAlattAllapotIncluded = true;

            src.Kovetkezo_Felelos_Id.Value = Guid.Empty.ToString();
            src.Kovetkezo_Felelos_Id.Operator = Query.Operators.equals;

            src.Kovetkezo_Orzo_Id.Value = Guid.Empty.ToString();
            src.Kovetkezo_Orzo_Id.Operator = Query.Operators.equals;

            //src.Kuldemeny_Ragszam.Value = "0";
            //src.Kuldemeny_Ragszam.Operator = Query.Operators.equals;

            src.KuldesMod.Value = "0";
            src.KuldesMod.Operator = Query.Operators.equals;

            src.LeveltariAtvevoNeve.Value = "0";
            src.LeveltariAtvevoNeve.Operator = Query.Operators.equals;

            src.NevSTR_Cimzett.Value = "0";
            src.NevSTR_Cimzett.Operator = Query.Operators.equals;

            src.Partner_Id_Cimzett.Value = Guid.Empty.ToString();
            src.Partner_Id_Cimzett.Operator = Query.Operators.equals;

            src.PostazasAllapot.Value = "0";
            src.PostazasAllapot.Operator = Query.Operators.equals;

            src.PostazasDatuma.Value = DateTime.Now.ToString();
            src.PostazasDatuma.Operator = Query.Operators.equals;

            src.Ragszam.Value = "0";
            src.Ragszam.Operator = Query.Operators.equals;

            src.SelejtezesDat.Value = DateTime.Now.ToString();
            src.SelejtezesDat.Operator = Query.Operators.equals;

            src.Sorszam.Value = "0";
            src.Sorszam.Operator = Query.Operators.equals;

            src.SztornirozasDat.Value = DateTime.Now.ToString();
            src.SztornirozasDat.Operator = Query.Operators.equals;

            src.TovabbitasAlattAllapot.Value = "0";
            src.TovabbitasAlattAllapot.Operator = Query.Operators.equals;

            src.Tovabbito.Value = Guid.Empty.ToString();
            src.Tovabbito.Operator = Query.Operators.equals;

            src.UgyintezesModja.Value = "0";
            src.UgyintezesModja.Operator = Query.Operators.equals;

            src.UgyUgyirat_Id_Kulso.Value = Guid.Empty.ToString();
            src.UgyUgyirat_Id_Kulso.Operator = Query.Operators.equals;

            src.ValaszElektronikus.Value = "0";
            src.ValaszElektronikus.Operator = Query.Operators.equals;

            src.VisszaerkezesDatuma.Value = DateTime.Now.ToString();
            src.VisszaerkezesDatuma.Operator = Query.Operators.equals;

            src.VisszaerkezesiHatarido.Value = DateTime.Now.ToString();
            src.VisszaerkezesiHatarido.Operator = Query.Operators.equals;

            src.Visszavarolag.Value = "0";
            src.Visszavarolag.Operator = Query.Operators.equals;
            #endregion

            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            Result result = service.GetAll(execParam, src);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }
        }
        #endregion

        #region GET
        [TestMethod]
        public void GetKuldemeny()
        {
            ExecParam execParam = base.BaseExecParam;
            execParam.Record_Id = Guid.Empty.ToString();

            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            Result result = service.Get(execParam);

            if (result.ErrorMessage != UnitTestConstants.ErrorMessages.E50101)
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }
        }
        [TestMethod]
        public void GetIrat()
        {
            ExecParam execParam = base.BaseExecParam;
            execParam.Record_Id = Guid.Empty.ToString();

            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = service.Get(execParam);

            if (result.ErrorMessage != UnitTestConstants.ErrorMessages.E50101)
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }
        }
        [TestMethod]
        public void GetlUgyIrat()
        {
            ExecParam execParam = base.BaseExecParam;
            execParam.Record_Id = Guid.Empty.ToString();

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            Result result = service.Get(execParam);

            if (result.ErrorMessage != UnitTestConstants.ErrorMessages.E50101)
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }
        }
        [TestMethod]
        public void GetlPldIratPeldanyok()
        {
            ExecParam execParam = base.BaseExecParam;
            execParam.Record_Id = Guid.Empty.ToString();

            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            Result result = service.Get(execParam);

            if (result.ErrorMessage != UnitTestConstants.ErrorMessages.E50101)
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }
        }
        #endregion
    }
}
