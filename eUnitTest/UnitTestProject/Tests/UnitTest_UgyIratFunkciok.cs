﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Transactions;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest_UgyIratFunkciok : ContentumBaseTest
    {
        [TestMethod]
        public void UgyIratSzignalas()
        {

            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            string ugyiratId = Guid.NewGuid().ToString();
            string iratId = Guid.NewGuid().ToString();
            string kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS
            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, null, "UnitTest Iktat + Ugyirat_Szignal_Szervezetre");
            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_UGYUGYIRATOK
            EREC_UgyUgyiratok ugyirat = UnitTestModelHelper.GetNewUgyIrat(execParam, "UnitTest Iktat + Ugyirat_Szignal_Szervezetre");
            ugyirat.Id = ugyiratId;
            #endregion EREC_UGYUGYIRATOK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat(execParam, "UnitTest Iktat + Ugyirat_Szignal_Szervezetre");
            irat.KuldKuldemenyek_Id = kuldemenyId;
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            #endregion IKTATASIPARAMETEREK

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES + IKTATAS
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.EgyszerusitettIktatasa(
                        execParam,
                        iktatooKonyv_Id,
                        ugyirat,
                        darabok,
                        irat,
                        feladatok,
                        kuldKuldemeny,
                        iktatasParameterek,
                        erkeztetesParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, iratId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyiratId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);
            Assert.IsNotNull(ugyiratId);

            EREC_UgyUgyiratokService ugyIratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            Result resultSzignal = ugyIratokService.SzignalasSzervezetre(execParam, ugyiratId, UnitTestConstants.Szignal_Ugyirat_Szervezet_Id, new EREC_HataridosFeladatok());
            if (!string.IsNullOrEmpty(resultSzignal.ErrorCode))
            {
                Assert.Fail(resultSzignal.ErrorCode + " - " + resultSzignal.ErrorMessage);
            }

            //throw new Exception("ROLLBACK");

        }

        [TestMethod]
        public void UgyIratSzignalPlusAtvetel()
        {

            #region INIT
            ExecParam execParam = UnitTestHelper.GetExecParam();

            #region IDS
            string ugyiratId = Guid.NewGuid().ToString();
            string iratId = Guid.NewGuid().ToString();
            string kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS
            string erkeztetoKonyv_Id = UnitTestHelper.GetErkeztetoKonyvId(execParam, UnitTestConstants.ErkeztetoHely);
            string iktatooKonyv_Id = UnitTestHelper.GetIktatoKonyvId(execParam, UnitTestConstants.IktatoHely);

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = UnitTestModelHelper.GetNewKuldemeny(execParam, erkeztetoKonyv_Id, null, "UnitTest EgyszerusitettIratIkt + Ugyirat_Szignal_Szervezetre + Atvetel");
            kuldKuldemeny.Id = kuldemenyId;
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_UGYUGYIRATOK
            EREC_UgyUgyiratok ugyirat = UnitTestModelHelper.GetNewUgyIrat(execParam, "UnitTest EgyszerusitettIratIkt + Ugyirat_Szignal_Szervezetre + Atvetel");
            ugyirat.Id = ugyiratId;
            #endregion EREC_UGYUGYIRATOK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = UnitTestModelHelper.GetNewIrat(execParam, "UnitTest EgyszerusitettIratIkt + Ugyirat_Szignal_Szervezetre + Atvetel");
            irat.KuldKuldemenyek_Id = kuldemenyId;
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = UnitTestModelHelper.GetIktatasParameterek();
            #endregion IKTATASIPARAMETEREK

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            #endregion

            #region ERKEZTETES + IKTATAS
            string ugyintezo = execParam.Felhasznalo_Id;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result result = iraIratokService.EgyszerusitettIktatasa(
                        execParam,
                        iktatooKonyv_Id,
                        ugyirat,
                        darabok,
                        irat,
                        feladatok,
                        kuldKuldemeny,
                        iktatasParameterek,
                        erkeztetesParameterek
                        );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Assert.Fail(result.ErrorCode + " - " + result.ErrorMessage);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                ugyiratId = string.IsNullOrEmpty(erkeztetesIktatasResult.UgyiratId) ? ugyiratId : erkeztetesIktatasResult.UgyiratId;
            }
            #endregion ERKEZTETES + IKTATAS

            #region CLEANUP
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Kuldemeny, kuldemenyId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Irat, iratId);
            base.Add2Cleanup(UnitTestConstants.EnumModel2CleanupType.Ugyirat, ugyiratId);
            #endregion

            Assert.IsNotNull(kuldemenyId);
            Assert.IsNotNull(iratId);
            Assert.IsNotNull(ugyiratId);

            #region SZIGNAL
            EREC_UgyUgyiratokService ugyIratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            Result resultSzignal = ugyIratokService.SzignalasSzervezetre(execParam, ugyiratId, UnitTestConstants.Szignal_Ugyirat_Szervezet_Id, new EREC_HataridosFeladatok());
            if (!string.IsNullOrEmpty(resultSzignal.ErrorCode))
            {
                Assert.Fail(resultSzignal.ErrorCode + " - " + resultSzignal.ErrorMessage);
            }
            #endregion

            #region ATVETEL

            ExecParam execParamAtvetel = UnitTestHelper.GetExecParamForUgyiratAtvetel();
            EREC_IraKezbesitesiTetelekService serviceIraKezbTetel = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
            string kezbTetelId;
            Result resultIraKezbTetel;

            resultIraKezbTetel = UnitTestHelper.GetEREC_IraKezbesitesiTetelek(execParamAtvetel.Clone(), ugyiratId);
            if (resultIraKezbTetel == null || resultIraKezbTetel.Ds.Tables[0].Rows.Count < 1)
            {
                Assert.Fail("Nem található kézbesítési tétel az irathoz !");
            }

            kezbTetelId = resultIraKezbTetel.Ds.Tables[0].Rows[0]["Id"].ToString();

            Result resultAtvetel = serviceIraKezbTetel.Atvetel(execParamAtvetel, kezbTetelId);
            if (!string.IsNullOrEmpty(resultAtvetel.ErrorCode))
            {
                Assert.Fail(resultAtvetel.ErrorCode + " - " + resultAtvetel.ErrorMessage);
            }

            #endregion

            //throw new Exception("ROLLBACK");
        }

    }
}
