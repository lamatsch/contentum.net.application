﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest_WebSiteForms : ContentumBaseTest
    {
        #region REFLECTION
        [TestMethod]
        public void eRecordForms()
        {
            if (string.IsNullOrEmpty(UnitTestConstants.eRecordWebSiteUrl) || string.IsNullOrEmpty(UnitTestConstants.eRecordWebSiteForms))
            {
                return;
            }

            string[] forms = UnitTestConstants.eRecordWebSiteForms.Split(new char[] { ';' });

            HttpWebRequest request;
            foreach (string item in forms)
            {
                request = (HttpWebRequest)WebRequest.Create(UnitTestConstants.eRecordWebSiteUrl.Trim() + item.Trim());
                request.Credentials = CredentialCache.DefaultCredentials;
                try
                {
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        using (Stream dataStream = response.GetResponseStream())
                        {
                            using (StreamReader reader = new StreamReader(dataStream))
                            {
                                string responseFromServer = reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (WebException exc)
                {
                    //Assert.Fail(string.Format("eRecord hiba! FORM={0} OKA={1}", item, exc.Message));

                    Debug.WriteLine(string.Format("eRecord hiba! FORM={0} OKA={1}", item, exc.Message));
                    Console.WriteLine(string.Format("eRecord hiba! FORM={0} OKA={1}", item, exc.Message));

                    //throw;
                }

            }
        }

        [TestMethod]
        public void eAdminForms()
        {
            if (string.IsNullOrEmpty(UnitTestConstants.eAdminWebSiteUrl) || string.IsNullOrEmpty(UnitTestConstants.eAdminWebSiteForms))
            {
                return;
            }

            string[] forms = UnitTestConstants.eAdminWebSiteForms.Split(new char[] { ';' });

            HttpWebRequest request;
            foreach (string item in forms)
            {
                request = (HttpWebRequest)WebRequest.Create(UnitTestConstants.eAdminWebSiteUrl.Trim() + item.Trim());
                request.Credentials = CredentialCache.DefaultCredentials;
                try
                {
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        using (Stream dataStream = response.GetResponseStream())
                        {
                            using (StreamReader reader = new StreamReader(dataStream))
                            {
                                string responseFromServer = reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (WebException exc)
                {
                    //Assert.Fail(string.Format("eRecord hiba! FORM={0} OKA={1}", item, exc.Message));

                    Debug.WriteLine(string.Format("eAdmin hiba! FORM={0} OKA={1}", item, exc.Message));
                    Console.WriteLine(string.Format("eAdmin hiba! FORM={0} OKA={1}", item, exc.Message));

                    //throw;
                }
            }
        }
    }
    #endregion
}
