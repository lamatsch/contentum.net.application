﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUtility
{
    public enum ArgumentTypes
    {
        String = 1,
        Guid = 2,
        Int = 3,
        Datetime = 4,
        Boolean = 5
    }
    public class Argument
    {
        private string name;

        public string Name
        {
            get { return name; }
        }

        private string value;
        private ArgumentTypes type;
        private bool emptyEnabled;
        private List<string> allowedValues;

        public Argument(string name, string value, ArgumentTypes type)
        {
            this.name = name;
            this.value = value;
            this.type = type;
            this.emptyEnabled = false;
            this.allowedValues = null;
        }

        public Argument(string name, string value, ArgumentTypes type, bool emptyEnabled, List<string> allowedValues)
        {
            this.name = name;
            this.value = value;
            this.type = type;
            this.emptyEnabled = emptyEnabled;
            this.allowedValues = allowedValues;
        }

        public bool IsValid()
        {
            //empty vizsgálat
            if (!emptyEnabled)
            {
                if (String.IsNullOrEmpty(value))
                {
                    return false;
                }
            }
            //típus vizsgálat
            switch (type)
            {
                case ArgumentTypes.Guid:
                    {
                        try
                        {
                            Guid guidValue = new Guid(value);
                        }
                        catch(FormatException)
                        {
                            return false;
                        }
                        break;
                    }
                case ArgumentTypes.Int:
                    {
                        Int32 intValue;
                        if (!Int32.TryParse(value, out intValue))
                        {
                            return false;
                        }
                        break;
                    }
                case ArgumentTypes.Datetime:
                    {
                        DateTime dtValue;
                        if (!DateTime.TryParse(value, out dtValue))
                        {
                            return false;
                        }
                        break;
                    }
                case ArgumentTypes.Boolean:
                    {
                        Boolean boolValue;
                        if (!Boolean.TryParse(value, out boolValue))
                        {
                            return false;
                        }
                        break;
                    }
            }
            //engedélyezett értékel vizsgálata
            if (allowedValues != null && allowedValues.Count > 0)
            {
                if (!allowedValues.Contains(value))
                {
                    return false;
                }
            }

            return true;
        }

    }

    public class Arguments
    {
        private List<Argument> argumentsList;

        public Arguments()
        {
            this.argumentsList = new List<Argument>();
        }

        public void Add(Argument argument)
        {
            this.argumentsList.Add(argument);
        }

        /// <summary>
        /// Ha nem valamelyik argumentum ResultExceptiont dob!!!
        /// </summary>
        public void ValidateArguments()
        {
            foreach (Argument arg in argumentsList)
            {
                if (!arg.IsValid())
                {
                    string error = "A következő bemeneti parméter nem megfelelő: {0}";
                    throw new ResultException(String.Format(error,arg.Name));
                }
            }
        }
    }
}
