using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data;
using System.Text.RegularExpressions;

namespace Contentum.eUtility
{
    public class Authentication
    {
        //public void CheckAuth(Page ParentPage)
        //{
        //    if (ParentPage == null) return;

        //    if (ParentPage.Session["isLogged"] == null || ParentPage.Session["isLogged"].ToString() != "True")
        //    {
        //        UI.RedirectPage(ParentPage, "Login.aspx");
        //    }
        //}

        public static bool IsAUserLoggedIn(Page ParentPage)
        {
            if (ParentPage == null) return false;

            if (UI.GetSession(ParentPage, Constants.isLogged) != "true" || UI.GetSession(ParentPage, Constants.isOrgSet) != "true")
            {
                return false;
            }
            return true;
        }

        public static Boolean IfHelyettesitettExist(Page ParentPage, string Helyettesito_Id)
        {
            Logger.DebugStart();
            Logger.Debug("IfHelyettesitettExist start");
            KRT_HelyettesitesekService service = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();
            ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
            execParam.Felhasznalo_Id = Helyettesito_Id;
            //KRT_HelyettesitesekSearch search = (KRT_HelyettesitesekSearch)Search.GetSearchObject(ParentPage, new KRT_HelyettesitesekSearch());
            KRT_HelyettesitesekSearch search = new KRT_HelyettesitesekSearch();

            search.Felhasznalo_ID_helyettesito.Value = Helyettesito_Id;
            search.Felhasznalo_ID_helyettesito.Operator = Query.Operators.equals;

            string str_now = DateTime.Now.ToString();

            // kezd <= now <= vege

            search.HelyettesitesKezd.Value = str_now;
            search.HelyettesitesKezd.Operator = Query.Operators.lessorequal;

            search.HelyettesitesVege.Value = str_now;
            search.HelyettesitesVege.Operator = Query.Operators.greaterorequal;


            Result result = service.GetAll(execParam, search);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Debug("KRT_HelyettesitesekService GetAll nincs hiba");
                if (result.Ds.Tables[0].Rows.Count != 0)
                {
                    Logger.Debug("result.Ds.Tables[0].Rows.Count = " + result.Ds.Tables[0].Rows.Count);
                    Logger.Debug("IfHelyettesitettExist end: return true");
                    Logger.DebugEnd();
                    return true;
                }
                else
                {
                    Logger.Debug("result.Ds.Tables[0].Rows.Count = " + result.Ds.Tables[0].Rows.Count);
                    Logger.Debug("IfHelyettesitettExist end: return false");
                    Logger.DebugEnd();
                    return false;
                }
            }
            else
            {
                Logger.Error(String.Format("KRT_HelyettesitesekService GetAll hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
                Logger.Debug("IfHelyettesitettExist end: return false");
                Logger.DebugEnd();
                return false;
            }
        }

        //public static Boolean IfMoreGroupsExist(Page ParentPage, string Helyettesito_Id)
        //{
        //    Logger.DebugStart();
        //    Logger.Debug("IfMoreGroupsExist start");
        //    KRT_CsoportTagokService service = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        //    ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
        //    execParam.Felhasznalo_Id = Helyettesito_Id;
        //    KRT_CsoportTagokSearch search = new KRT_CsoportTagokSearch();

        //    search.Csoport_Id_Jogalany.Value = Helyettesito_Id;
        //    search.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

        //    Result result = service.GetAll(execParam, search);
        //    if (String.IsNullOrEmpty(result.ErrorCode))
        //    {
        //        Logger.Debug("IfMoreGroupsExist GetAll nincs hiba");
        //        Dictionary<string, string> dict = new Dictionary<string, string>();
        //        foreach(DataRow row in result.Ds.Tables[0].Rows)
        //        {
        //            string Csoport_Id = row["Csoport_Id"].ToString();
        //            if (!dict.ContainsKey(Csoport_Id))
        //            {
        //                dict.Add(Csoport_Id,Csoport_Id);
        //            }
        //        }

        //        if (dict.Count > 1)
        //        {
        //            Logger.Debug("result.Ds.Tables[0].Rows.Count = " + result.Ds.Tables[0].Rows.Count);
        //            Logger.Debug("IfMoreGroupsExist end: return true");
        //            Logger.DebugEnd();
        //            return true;
        //        }
        //        else
        //        {
        //            Logger.Debug("result.Ds.Tables[0].Rows.Count = " + result.Ds.Tables[0].Rows.Count);
        //            Logger.Debug("IfMoreGroupsExist end: return false");
        //            Logger.DebugEnd();
        //            return false;
        //        }
        //    }
        //    else
        //    {
        //        Logger.Error(String.Format("CsoportTagokService GetAll hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        //        Logger.Debug("IfMoreGroupsExist end: return false");
        //        Logger.DebugEnd();
        //        return false;
        //    }
        //}

        public static int GetGroupCount(Page ParentPage, string Helyettesito_Id)
        {
            Logger.DebugStart();
            Logger.Debug("GetGroupCount start");
            KRT_CsoportTagokService service = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
            execParam.Felhasznalo_Id = Helyettesito_Id;
            KRT_CsoportTagokSearch search = new KRT_CsoportTagokSearch();

            search.Csoport_Id_Jogalany.Value = Helyettesito_Id;
            search.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, search);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Debug("GetGroupCount GetAll nincs hiba");
                Dictionary<string, string> dict = new Dictionary<string, string>();
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    string Csoport_Id = row["Csoport_Id"].ToString();
                    if (!dict.ContainsKey(Csoport_Id))
                    {
                        dict.Add(Csoport_Id, Csoport_Id);
                    }
                }


                Logger.Debug(String.Format("result.Ds.Tables[0].Rows.Count = {0}", result.Ds.Tables[0].Rows.Count));
                Logger.Debug(String.Format("GetGroupCount end: return {0}", dict.Count));
                Logger.DebugEnd();

                return dict.Count;
            }
            else
            {
                Logger.Error(String.Format("CsoportTagokService GetAll hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
                Logger.Debug(String.Format("GetGroupCount end: return {0}", -1));
                Logger.DebugEnd();

                return -1;
            }
        }
        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="ParentPage">The parent page.</param>
        public static void ChangePassword(Page ParentPage)
        {
            Logger.DebugStart();
            Logger.Debug("ChangePassword start");

            Contentum.eAdmin.Service.AuthenticationService _AuthenticationService = eAdminService.ServiceFactory.GetAuthenticationService();
            string _oldPassword = UI.GetSession(ParentPage, Constants.OldPassword);
            string _Password = UI.GetSession(ParentPage, Constants.Password);
            Logger.Debug(String.Format("Password: {0}", _Password ?? "NULL"));
            string _LoginType = UI.GetSession(ParentPage, Constants.PasswordConfirm);
            Logger.Debug(String.Format("PasswordConfirmed: {0}", _LoginType ?? "NULL"));
            string _Org = UI.GetSession(ParentPage, Constants.FelhasznaloSzervezetId);
            Logger.Debug(String.Format("Org: {0}", _Org ?? "NULL"));
            string _Id = UI.GetSession(ParentPage, Constants.LoginUserId);

            var passHash = Contentum.eUtility.Crypt.GetSha1Hash(_oldPassword);

            ExecParam ep = UI.SetExecParamDefault(ParentPage);
            ep.Record_Id = _Id;
            ep.Felhasznalo_Id = _Id;
            var svcFelhasznalok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            
            Result result = svcFelhasznalok.Get(ep);

            if (result.Record == null)
            {
                Result res = new Result();
                res.ErrorCode = "[60000:Authentication:Login:UserPasswordPairNotFound]";
                res.ErrorMessage = "[60000]";
                Log.Error.LogIn(ParentPage, res);
                String errorMessage =
                    ResultError.GetErrorMessageFromResultObject(res);
                UI.RedirectToErrorPageByErrorMsg(ParentPage, errorMessage);
                return;
            }
            // Megvan a rekord
            KRT_Felhasznalok record = new KRT_Felhasznalok();
            KRT_Felhasznalok krt_felhasznalok = result.Record as KRT_Felhasznalok;            
            record.Id = _Id;
            record.Base.Updated.SetValueAll(false);
            record.Updated.SetValueAll(false);
            record.Base.Ver = krt_felhasznalok.Base.Ver;
            ep.Org_Id = krt_felhasznalok.Org;
            ParentPage.Session[Constants.FelhasznaloSzervezetId] = krt_felhasznalok.Org;

            if (!(result.Record as KRT_Felhasznalok).Jelszo.Equals(passHash))
            {
                record.WrongPassCnt = (int.Parse(krt_felhasznalok.WrongPassCnt) + 1).ToString();
                record.Updated.WrongPassCnt = true;
                // Ha el�ri a maxim�lis sz�mot
                if (record.WrongPassCnt == Rendszerparameterek.Get(ep, "PASSWORD_POLICY_WRONG_PASS_TRY_NUMBER"))
                {
                    record.Engedelyezett = Constants.Database.No;
                    record.Updated.Engedelyezett = true;
                    eAdmin.Service.EmailService svcEmail = eUtility.eAdminService.ServiceFactory.GetEmailService();
                    string syadminemail = Rendszerparameterek.Get(ep, Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM);
                    bool resEmail = svcEmail.SendEmail(ep,syadminemail, new string[] { syadminemail } ,"Felhaszn�l� felf�ggeszt�se",null,null,true,string.Format("A {0} felhaszn�l� nev� felhaszn�l� a rendszerbe t�bbsz�ri sikertelen bejelentkez�s pr�b�lkoz�s miatt bejelentkez�si enged�lye megvon�sra ker�lt.",krt_felhasznalok.UserNev));
                    if (!resEmail)
                    Logger.Error(string.Format("Az Email elk�ld�se nem siker�lt a {0} felhaszn�l� sz�m�ra, az {1} Email c�mr�l.", syadminemail, syadminemail));
               
                }
                //Hiba�zi : Ne irjuk ki, hogy a felhaszn�l� n�v j�.          
                Result res = new Result();
                res = svcFelhasznalok.UpdateFelhasznalo(ep, record);

                if (!res.IsError) {
                    res.ErrorCode = "[60000:Authentication:Login:UserPasswordPairNotFound]";
                    res.ErrorMessage = "[60000]";
                }
                Log.Error.LogIn(ParentPage, res);
                String errorMessage =
                    ResultError.GetErrorMessageFromResultObject(res);
                UI.RedirectToErrorPageByErrorMsg(ParentPage, errorMessage);
                return;
            }
            else {
                record.JelszoLejaratIdo = (DateTime.Now + new TimeSpan(int.Parse(Rendszerparameterek.Get(ep.Clone(), "PASSWORD_POLICY_EXPIRE_DAYS")), 0, 0, 0)).ToString();
                record.Updated.JelszoLejaratIdo = true;
                record.Base.Updated.Ver = true;
                record.Jelszo = Crypt.GetSha1Hash(_Password);
                record.Updated.Jelszo = true;                
            }
            Result _ret = new Result();
            ExecParam epHistory = ep.Clone();
            epHistory.Record_Id = _Id;
            epHistory.Felhasznalo_Id = _Id;
            int histroyCnt = int.Parse(Rendszerparameterek.Get(ep.Clone(), "PASSWORD_POLICY_HISTORY_NUMBER"));
            int found = 0;
            List<string> pwdhashes = new List<string>();
            RecordHistoryService svcHistory = eUtility.eAdminService.ServiceFactory.GetRecordHistoryService();            
            _ret = svcHistory.GetAllByRecord(epHistory, "KRT_Felhasznalok");

            // Az utols� PASSWORD_POLICY_HISTORY_NUMBER sz�m� jelsz� nem lehet �jra jelsz�. 
            pwdhashes.Add(_oldPassword);
            
            if (_ret.Ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow drFelhasznalok in _ret.Ds.Tables[0].Rows)
                {                   
                    if (drFelhasznalok["ColumnName"].ToString() == "Jelszo")
                    {
                        pwdhashes.Add(drFelhasznalok["NewValue"].ToString());                        
                        if (pwdhashes.Contains(Crypt.GetSha1Hash(_Password)))
                        {                         
                            _ret.ErrorCode = "[60005]";
                            _ret.ErrorMessage = "[60005]";
                            break;
                        }
                        found++;
                    }
                    if (found >= histroyCnt)
                    {
                        break;
                    }
                }
            }

            //Felhaszn�l� nevet nem tartalmazhatja a jelsz�  

            if (_Password.ToUpper().Contains(krt_felhasznalok.UserNev.ToUpper()))
            {
                _ret.ErrorCode = "[60007]";
                _ret.ErrorMessage = "[60007]";                
            }

            if (!_ret.IsError) { 
             _ret = _AuthenticationService.ChangePassword(ep, record);                
            }
            
            
            if (!string.IsNullOrEmpty(_ret.ErrorCode))
            {
                Log.Error.LogIn(ParentPage, _ret);
                String errorMessage =
                    ResultError.GetErrorMessageFromResultObject(_ret);
                UI.RedirectToErrorPageByErrorMsg(ParentPage, errorMessage);
                return;
            }

            //Jelentkezzen be �jra, hogy megkapja a jogookat.
            Logger.Debug(string.Format("Felhaszn�l� {0} jelszava sikeresen megv�ltozott.", (result.Record as KRT_Felhasznalok).UserNev));
            ParentPage.Session.RemoveAll();
            UI.RedirectPage(ParentPage, "Login.aspx");
            
            Logger.DebugEnd();

        }
        public static void Login(Page ParentPage)
        {
            
            Logger.DebugStart();
            Logger.Debug("Login start");
            if (ParentPage == null) return;

            string _UserName = UI.GetSession(ParentPage, Constants.UserName);
            Logger.Debug(String.Format("UserName: {0}", _UserName ?? "NULL"));
            string _Password = UI.GetSession(ParentPage, Constants.Password);
            Logger.Debug(String.Format("Password: {0}", _Password ?? "NULL"));
            string _LoginType = UI.GetSession(ParentPage, Constants.LoginType);
            Logger.Debug(String.Format("LoginType: {0}", _LoginType ?? "NULL"));
            string _Org = UI.GetSession(ParentPage, Constants.FelhasznaloSzervezetId);
            Logger.Debug(String.Format("Org: {0}", _Org ?? "NULL"));

            if (_LoginType == "Windows" || _LoginType == "Basic")
            {
                if (_LoginType == "Windows" && String.IsNullOrEmpty(_UserName))
                {
                    Result res = new Result();
                    res.ErrorCode = "[60003:Authentication:Windows:UserNameEmpty]";
                    res.ErrorMessage = "[60003]";
                    Log.Error.LogIn(ParentPage, res);
                    String errorMessage =
                        ResultError.GetErrorMessageFromResultObject(res);
                    UI.RedirectToErrorPageByErrorMsg(ParentPage, errorMessage);
                }
                if ((_LoginType == "Basic" && (String.IsNullOrEmpty(_UserName) || String.IsNullOrEmpty(_Password))))
                {
                    UI.RedirectPage(ParentPage, "Login.aspx");
                    return;
                }

                //// ide jon a webservice login ellenorzes es annak visszateresi ertekenek fuggvenyeben
                //// beallitjuk a helyi session-t
                ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
                if (String.IsNullOrEmpty((string)ParentPage.Session[Constants.FelhasznaloId]))
                {
                    Contentum.eAdmin.Service.AuthenticationService _AuthenticationService = eAdminService.ServiceFactory.GetAuthenticationService();
                    Contentum.eBusinessDocuments.Login login = new Contentum.eBusinessDocuments.Login();
                    login.FelhasznaloNev = _UserName;
                    login.Jelszo = _Password;
                    login.Tipus = _LoginType;
                    Contentum.eBusinessDocuments.Result _ret = _AuthenticationService.Login(execParam, login);
                    if (!string.IsNullOrEmpty(_ret.ErrorCode))
                    {
                      if (_ret.ErrorCode == "60006")
                        {
                            Log.Error.LogIn(ParentPage, _ret);                            
                            UI.RedirectPage(ParentPage, "ChangePassword.aspx?"+Constants.LoginUserId+"="+_ret.Uid.ToString()+"&fromlogin=1");
                            ParentPage.Session.Clear();       
                            return;
                        }
                        Log.Error.LogIn(ParentPage, _ret);
                        String errorMessage = Regex.Replace(ResultError.GetErrorMessageFromResultObject(_ret),@"\t|\n|\r","");
                        UI.RedirectToErrorPageByErrorMsg(ParentPage, errorMessage);
                        return;
                    }

                    if (_ret.Uid != null && !string.IsNullOrEmpty(_ret.Uid))
                    {
                        ParentPage.Session[Constants.FelhasznaloId] = _ret.Uid;

                        ParentPage.Session[Constants.LoginUserId] = _ret.Uid;
                    }
                    else
                    {
                        Result res = new Result();
                        res.ErrorCode = "[60000:Authentication:Login:UserPasswordPairNotFound]";
                        res.ErrorMessage = "[60000]";
                        Log.Error.LogIn(ParentPage, res);
                        UI.RedirectPage(ParentPage, "Login.aspx");
                        return;
                    }
                }

                bool helyettesitettExist = false;
                bool moreGroupsExist = false;
                int cntGroups = -1;
                bool elozoLoginVisszaAllitva = TryRestorePreviousLogin(ParentPage);
                if (!elozoLoginVisszaAllitva)
                {
                    //SetFelhasznaloSzervezetToSession(ParentPage, execParam, ParentPage.Session[Constants.FelhasznaloId].ToString());

                    if (!string.IsNullOrEmpty(UI.GetSession(ParentPage, Constants.FelhasznaloId)))
                    {
                        //Sikeres bejelentkez�s loggol�sa, ha nincs helyettes�t�s, ha van akkor m�shol logoljuk
                        helyettesitettExist = IfHelyettesitettExist(ParentPage, UI.GetSession(ParentPage, Constants.FelhasznaloId));
                        //moreGroupsExist = IfMoreGroupsExist(ParentPage, UI.GetSession(ParentPage, Constants.FelhasznaloId));
                        cntGroups = GetGroupCount(ParentPage, UI.GetSession(ParentPage, Constants.FelhasznaloId));
                    }
                    else
                    {
                        Logger.Warn("Page.Session: FelhasznaloId is empty!");
                    }

                    if (cntGroups < 0)
                    {
                        Result res = new Result();
                        res.ErrorCode = "[60004:Authentication:Login:ErrorWhileQueryingGroups]";
                        res.ErrorMessage = "[60004]";
                        Log.Error.LogIn(ParentPage, res);
                        String errorMessage =
                            ResultError.GetErrorMessageFromResultObject(res);
                        // kil�ptet�s
                        Log.LogOut(ParentPage);
                        ParentPage.Session.RemoveAll();
                        UI.RedirectToErrorPageByErrorMsg(ParentPage, errorMessage);
                        return;
                    }

                    if (cntGroups == 0)
                    {
                        Result res = new Result();
                        res.ErrorCode = "[60002:Authentication:Login:UserHasNoGroup]";
                        res.ErrorMessage = "[60002]";
                        Log.Error.LogIn(ParentPage, res);
                        String errorMessage =
                            ResultError.GetErrorMessageFromResultObject(res);
                        // kil�ptet�s
                        Log.LogOut(ParentPage);
                        ParentPage.Session.RemoveAll();
                        UI.RedirectToErrorPageByErrorMsg(ParentPage, errorMessage);
                        return;
                    }

                    moreGroupsExist = (cntGroups > 1 ? true : false);

                    if (!helyettesitettExist)
                    {
                        SetLastLoginTimeToSession(ParentPage, UI.GetSession(ParentPage, Constants.FelhasznaloId));

                        Log.LogIn(ParentPage);

                        ////k�s�bbi �t�r�ny�t�s elker�l�se!
                        //ParentPage.Session[Constants.OwnLogin] = "1";
                    }

                    if (!moreGroupsExist && !helyettesitettExist)
                    {
                        SetFelhasznaloSzervezetToSession(ParentPage, execParam, UI.GetSession(ParentPage, Constants.FelhasznaloId));
                        //k�s�bbi �t�r�ny�t�s elker�l�se!
                        ParentPage.Session[Constants.OwnLogin] = "1";
                        SetSimpleLogin(ParentPage);
                    }
                }

                ParentPage.Session[Constants.isLogged] = "true";

                // rendszer parameterek betoltese:
                ParentPage.Session[Constants.MasterRowCount] = "10";
                ParentPage.Session[Constants.DetailRowCount] = "10";
                //LZS - BUG_8844
                ExecParam ex =  UI.SetExecParamDefault(ParentPage, new ExecParam());
                int max = Rendszerparameterek.GetInt(ex, Rendszerparameterek.LISTA_ELEMSZAM_MAX);
                ParentPage.Session[Constants.TopRow] = max.ToString();

                //Log.LogIn(ParentPage);

                //--------------*****************


                //CheckLogin(ParentPage);

                bool isOrgSet = false; // csoporttags�g/csoport be�ll�t�s sikeress�g�t jelzi
                string _PageNameBeforeLogin = UI.GetSession(ParentPage, Constants.PageNameBeforeLogin);
                //ParentPage.Session.Remove(Constants.PageNameBeforeLogin);

                if (ParentPage.Request.Url.AbsoluteUri == _PageNameBeforeLogin)
                {
                    //if (helyettesitettExist)
                    //{
                    //    Log.LogIn(ParentPage);
                    //}
                    if (IsHelyettesitettFelhasznaloSet(ParentPage))
                    {
                        Logger.Debug("IsHelyettesitettFelhasznaloSet");

                        if (UI.GetSession(ParentPage, Constants.isOrgSet) == "true")
                        {
                            isOrgSet = true;
                        }
                        Logger.Debug("_isOrgSet = " + isOrgSet.ToString());
                        if (!isOrgSet)
                        {
                            Result res = new Result();
                            res.ErrorCode = "[60002:Authentication:Login:UserHasNoGroup]";
                            res.ErrorMessage = "[60002]";
                            Log.Error.LogIn(ParentPage, res);
                            String errorMessage =
                                ResultError.GetErrorMessageFromResultObject(res);
                            // kil�ptet�s
                            Log.LogOut(ParentPage);
                            ParentPage.Session.RemoveAll();
                            UI.RedirectToErrorPageByErrorMsg(ParentPage, errorMessage);
                            return;
                        }
                    }

                    Logger.Debug("Login end: (ParentPage.Request.Url.AbsoluteUri == _PageNameBeforeLogin), " + ParentPage.Request.Url.AbsoluteUri);
                    Logger.DebugEnd();
                    return;
                }

                string pageToRedirectTo = String.Empty;
                if (!string.IsNullOrEmpty(_PageNameBeforeLogin))
                {
                    Logger.Debug("_PageNameBeforeLogin is not null or empty: " + _PageNameBeforeLogin);
                    pageToRedirectTo = _PageNameBeforeLogin;
                }
                else
                {
                    Logger.Debug("_PageNameBeforeLogin is null or empty");
                    pageToRedirectTo = "Default.aspx";
                }

                if (helyettesitettExist)
                {
                    Logger.Debug("helyettesitettExist");
                    Logger.Debug("Login end");
                    Logger.DebugEnd();
                    UI.RedirectPage(ParentPage, "Login.aspx");
                }
                if (moreGroupsExist)
                {
                    Logger.Debug("moreGroupsExist");
                    Logger.Debug("Login end");
                    Logger.DebugEnd();
                    UI.RedirectPage(ParentPage, "Login.aspx");
                }
                else
                {
                    Logger.Debug("not helyettesitettExist and not moreGroupsExist");
                    Logger.Debug("Login end");
                    Logger.DebugEnd();

                    if (UI.GetSession(ParentPage, Constants.isOrgSet) == "true")
                    {
                        isOrgSet = true;
                    }
                    Logger.Debug("_isOrgSet = " + isOrgSet.ToString());
                    if (!isOrgSet)
                    {
                        Result res = new Result();
                        res.ErrorCode = "[60002:Authentication:Login:UserHasNoGroup]";
                        res.ErrorMessage = "[60002]";
                        Log.Error.LogIn(ParentPage, res);
                        String errorMessage =
                            ResultError.GetErrorMessageFromResultObject(res);
                        // kil�ptet�s
                        Log.LogOut(ParentPage);
                        ParentPage.Session.RemoveAll();
                        UI.RedirectToErrorPageByErrorMsg(ParentPage, errorMessage);
                        return;
                    }

                    ParentPage.Response.Redirect(pageToRedirectTo, true);
                }

            }
            else
            {
                Logger.Debug("Login end");
                Logger.DebugEnd();
                UI.RedirectPage(ParentPage, "Login.aspx");
                return;
            }
        }

        private static void SetLastLoginTimeToSession(Page page, string felhasznaloId)
        {
            Contentum.eBusinessDocuments.ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
            execParam.Felhasznalo_Id = felhasznaloId;
            execParam.Record_Id = felhasznaloId;

            Contentum.eAdmin.Service.KRT_FelhasznalokService felhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

            Result resultLastLoginTime = felhasznalokService.GetLastLoginTime(execParam, felhasznaloId);
            if (string.IsNullOrEmpty(resultLastLoginTime.ErrorCode) && resultLastLoginTime.Record != null)
            {
                string lastLoginTime = resultLastLoginTime.Record.ToString();

                if (!string.IsNullOrEmpty(lastLoginTime))
                {
                    Logger.Debug("LastLoginTime: " + lastLoginTime);
                }

                page.Session[Constants.SessionNames.LastLoginTime] = lastLoginTime;
            }

            page.Session[Constants.SessionNames.LastLoginTime] = "";
        }

        private static bool TryRestorePreviousLogin(Page page)
        {
            // �j bejelentkez�s, vagy csak lej�rt a session?
            // Volt-e megadva a masterPage-en az el�z� loginra vonatkoz� adat (FelhasznaloId, SzervezetId)
            // Ha volt megadva adat, akkor megpr�b�ljuk azokkal az adatokkal bel�ptetni (helyettes�t�st is figyelembe v�ve)
            FelhasznaloProfil fp = FelhasznaloProfil.GetLastLoginData(page);
            if (fp != null)
            {
                if (fp.LoginType == "Basic")
                {
                    page.Session.Clear();
                    SetPageNameBeforeLogin(page);
                    UI.RedirectPage(page, "Login.aspx");
                }
                if (!string.IsNullOrEmpty(fp.Felhasznalo.Id))
                {
                    if (fp.Felhasznalo.Id != page.Session[Constants.LoginUserId].ToString())
                    {
                        // ha k�l�nb�zik az el�z� login felhaszn�l�id-ja a mostanit�l, akkor helyettes�t�s volt.
                        // Be�ll�tjuk a helyettes�t�st a session-be: (Ellen�rz�ssel, hogy l�tezik-e ez a helyettes�t�s)
                        //SetHelyettesitesToSession(ParentPage, felhId_lastLogin, false);

                        // ha k�l�nb�zik az el�z� login felhaszn�l�id-ja a mostanit�l, akkor helyettes�t�s volt.
                        // Be�ll�tjuk a helyettes�t�st a session-be: (Ellen�rz�ssel, hogy l�tezik-e ez a helyettes�t�s)
                        if (!string.IsNullOrEmpty(fp.Helyettesites.Id))
                        {
                            Logger.Debug("El�z� Login helyre�ll�t�s: SetHelyettesitesToSession");
                            SetHelyettesitesToSession(page, fp.Helyettesites.Id, fp.FelhasznaloCsoportTagsag.Id, false);
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        //// felhaszn�l� Id megegyezik a felhId_lastLogin -nal
                        //// m�g a szervezetet kell be�ll�tani:
                        //SetFelhasznaloSzervezetToSession(ParentPage, execParam, felhId_lastLogin);

                        if (!string.IsNullOrEmpty(fp.FelhasznaloCsoportTagsag.Id))
                        {
                            Logger.Debug("El�z� Login helyre�ll�t�s: SetFelhasznaloCsoportToSession");
                            SetFelhasznaloCsoportToSession(page, fp.FelhasznaloCsoportTagsag.Id);
                            //k�s�bbi �t�r�ny�t�s elker�l�se!
                            page.Session[Constants.OwnLogin] = "1";
                        }
                        else
                        {
                            return false;
                        }
                    }

                    // siker�lt-e be�ll�tani?
                    if (UI.GetSession(page, Constants.FelhasznaloId) == fp.Felhasznalo.Id
                        && UI.GetSession(page, Constants.FelhasznaloCsoportTagId) == fp.FelhasznaloCsoportTagsag.Id
                        && UI.GetSession(page, Constants.FelhasznaloSzervezetId) == fp.FelhasznaloCsoportTagsag.Csoport_Id// ez csak ellen�rz�s
                        && UI.GetSession(page, Constants.HelyettesitesId) == fp.Helyettesites.Id
                        )
                    {
                        return true;
                    }
                 
                }
            }

            return false;

        }

        public static void SetFelhasznaloCsoportToSession(Page ParentPage, string csoportagId)
        {
            ParentPage.Session[Constants.isOrgSet] = "false";
            ParentPage.Session[Constants.FelhasznaloCsoportTagId] = "";
            ParentPage.Session[Constants.FelhasznaloSzervezetId] = "";
            ParentPage.Session[Constants.FelhasznaloSzervezetNev] = "";
            ParentPage.Session[Constants.IsAdminInSzervezet] = "";

            ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());

            KRT_CsoportTagokService csopTagService = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();

            KRT_CsoportTagokSearch search = new KRT_CsoportTagokSearch();
            search.Id.Value = csoportagId;
            search.Id.Operator = Query.Operators.equals;

            execParam.Record_Id = csoportagId;
            //Result csoporttagokResult = csopTagService.Get(execParam);
            Result csoporttagokResult = csopTagService.GetAll(execParam, search);
            if (string.IsNullOrEmpty(csoporttagokResult.ErrorCode))
            {
                if (csoporttagokResult.Ds.Tables[0].Rows.Count > 0)
                {
                    ParentPage.Session[Constants.FelhasznaloCsoportTagId] = csoportagId;
                    //KRT_CsoportTagok krt_CsoportTagok = (KRT_CsoportTagok)csoporttagokResult.Record;
                    //if (krt_CsoportTagok != null)
                    //{


                    string csoport_id = csoporttagokResult.Ds.Tables[0].Rows[0]["Csoport_Id"].ToString(); //krt_CsoportTagok.Csoport_Id;
                    ParentPage.Session[Constants.FelhasznaloSzervezetId] = csoport_id;
                    //if (!string.IsNullOrEmpty(csoport_id))
                    //{
                    //    ParentPage.Session[Constants.isOrgSet] = "true";
                    //}

                    if (!string.IsNullOrEmpty(csoport_id))
                    {
                        KRT_CsoportokService csopService = eAdminService.ServiceFactory.GetKRT_CsoportokService();
                        ExecParam csopExecParam = execParam.Clone();
                        csopExecParam.Record_Id = csoport_id;
                        Result csoportResult = csopService.Get(csopExecParam);

                        if (string.IsNullOrEmpty(csoportResult.ErrorCode))
                        {
                            var csoport = csoportResult.Record as KRT_Csoportok;
                            ParentPage.Session[Constants.FelhasznaloSzervezetNev] = csoport.Nev;
                            ParentPage.Session[Constants.IsAdminInSzervezet] = csoport.Tipus == "R" ? "1" : "0";

                            if (!string.IsNullOrEmpty(csoport_id))
                            {
                                ParentPage.Session[Constants.isOrgSet] = "true";
                            }
                        }
                    }
                }
                else
                {
                    //Logger.Error("KRT_CsoportTagok.Get eredm�nye null!");
                    Logger.Error("KRT_CsoportTagok.GetAll eredm�nye 0 sor!");

                    Result res = new Result();
                    res.ErrorCode = "[60002:Authentication:Login:UserHasNoGroup]";
                    res.ErrorMessage = "[60002]";
                    Log.Error.LogIn(ParentPage, res);
                    String errorMessage =
                        ResultError.GetErrorMessageFromResultObject(res);
                    // kil�ptet�s
                    Log.LogOut(ParentPage);
                    ParentPage.Session.RemoveAll();
                    UI.RedirectToErrorPageByErrorMsg(ParentPage, errorMessage);
                    return;
                }
            }
        }

        // ezt most csak akkor haszn�ljuk, ha a bejelentkez�skor nem egy r�gi login vissza�ll�t�sa t�rt�nt, �s �gy a szervezet m�g nem ismert
        // ha t�bb szervezethez is hozz� van rendelve a felhaszn�l�, akkor a bejelentkez�skor �gyis ki kell majd v�lasztania a list�b�l
        private static void SetFelhasznaloSzervezetToSession(Page ParentPage, ExecParam execParam, string felhasznaloId)
        {
            ParentPage.Session[Constants.isOrgSet] = "false";
            ParentPage.Session[Constants.FelhasznaloCsoportTagId] = "";
            ParentPage.Session[Constants.FelhasznaloSzervezetId] = "";
            ParentPage.Session[Constants.FelhasznaloSzervezetNev] = "";

            KRT_CsoportTagokService csopTagService = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            execParam.Felhasznalo_Id = felhasznaloId;
            Result szervezetResult = csopTagService.GetSzervezet(execParam);
            if (string.IsNullOrEmpty(szervezetResult.ErrorCode))
            {
                ParentPage.Session[Constants.FelhasznaloSzervezetId] = szervezetResult.Uid;
                ParentPage.Session[Constants.FelhasznaloCsoportTagId] = szervezetResult.Ds.Tables[0].Rows[0]["Id"].ToString();

                if (!string.IsNullOrEmpty(szervezetResult.Uid) && !string.IsNullOrEmpty(szervezetResult.Ds.Tables[0].Rows[0]["Id"].ToString()))
                {
                    ParentPage.Session[Constants.isOrgSet] = "true";
                }

                ParentPage.Session[Constants.FelhasznaloSzervezetNev] = szervezetResult.Ds.Tables[0].Rows[0]["Nev"].ToString();

                //KRT_CsoportokService csopService = eAdminService.ServiceFactory.GetKRT_CsoportokService();
                //ExecParam csopExecParam = execParam.Clone();
                //csopExecParam.Record_Id = szervezetResult.Uid;
                //Result csoportResult = csopService.Get(csopExecParam);

                //if (string.IsNullOrEmpty(csoportResult.ErrorCode))
                //{
                //    ParentPage.Session[Constants.FelhasznaloSzervezetNev] = (csoportResult.Record as KRT_Csoportok).Nev;
                //}
            }
        }

        private static bool IsHelyettesitettFelhasznaloSet(Page ParentPage)
        {
            Logger.DebugStart();
            Logger.Debug("IsHelyettesitettFelhasznaloSet start");
            string loggedFelhasznaloId = ParentPage.Request.QueryString.Get(QueryStringVars.LoggedFelhasznaloId);
            string loggedCsoporttagId = ParentPage.Request.QueryString.Get(QueryStringVars.LoggedCsoporttagId);
            string loggedHelyettesitesId = ParentPage.Request.QueryString.Get(QueryStringVars.LoggedHelyettesitesId);

            Logger.Debug("loggedFelhasznaloId: " + loggedFelhasznaloId);
            Logger.Debug("loggedCsoporttagId: " + loggedCsoporttagId);
            Logger.Debug("loggedHelyettesitesId: " + loggedHelyettesitesId);

            if (!string.IsNullOrEmpty(loggedFelhasznaloId))
            {
                string felhasznaloId = FelhasznaloProfil.FelhasznaloId(ParentPage);
                string csoporttagId = FelhasznaloProfil.FelhasznaloCsoportTagId(ParentPage);
                
                Logger.Debug("felhasznaloId: " + felhasznaloId);
                Logger.Debug("csoporttagId: " + csoporttagId);

                if (loggedFelhasznaloId != felhasznaloId)
                {
                    if (!string.IsNullOrEmpty(loggedHelyettesitesId) && !string.IsNullOrEmpty(loggedCsoporttagId))
                    {
                        Logger.Debug("loggedFelhasznaloId != felhasznaloId && (!string.IsNullOrEmpty(loggedHelyettesitesId) && !string.IsNullOrEmpty(loggedCsoporttagId))");
                        SetHelyettesitesToSession(ParentPage, loggedHelyettesitesId, loggedCsoporttagId, false);
                    }
                    else
                    {
                        Logger.Debug("Missing parameters for: Helyettesites");
                        Logger.Debug("IsHelyettesitettFelhasznaloSet end: return false");
                        Logger.DebugEnd();
                        return false;
                    }
                }
                else
                {
                    Logger.Debug("loggedFelhasznaloId == felhasznaloId");
                    if (!string.IsNullOrEmpty(loggedCsoporttagId))
                    {
                        Logger.Debug("IsHelyettesitettFelhasznaloSet: SetFelhasznaloCsoportToSession");
                        SetFelhasznaloCsoportToSession(ParentPage, loggedCsoporttagId);
                        OwnButton_Click(ParentPage, false);
                    }
                    else
                    {
                        Logger.Debug("Missing parameters for: SetFelhasznaloCsoportToSession");
                        Logger.Debug("IsHelyettesitettFelhasznaloSet end: return false");
                        Logger.DebugEnd();
                        return false;
                    }
                }
                Logger.Debug("IsHelyettesitettFelhasznaloSet end: return true");
                Logger.DebugEnd();
                return true;
            }
            else
            {
                Logger.Debug("IsHelyettesitettFelhasznaloSet end: return false");
                Logger.DebugEnd();
                return false;
            }

        }

         /// <summary>
        /// Ellen�rzi, hogy a QueryStringben van-e felhaszn�l�i adat, amennyi el�g lenne egy bel�ptet�shez.
        /// </summary>
        /// <param name="ParentPage"></param>
        /// <returns>Bel�p�shez sz�ks�ges adatok: loggedFelhasznaloId �s loggedCsoportTagId
        /// - jelenleg a helyettes�t�st itt nem vizsg�ljuk, csak a Key l�tez�s�t</returns>
        private static bool IsUserDefinedByQs(Page ParentPage)
        {
            Logger.DebugStart();
            Logger.Debug("IsUserDefinedByQs start");
            string loggedFelhasznaloId = ParentPage.Request.QueryString.Get(QueryStringVars.LoggedFelhasznaloId);
            string loggedCsoporttagId = ParentPage.Request.QueryString.Get(QueryStringVars.LoggedCsoporttagId);

            Logger.Debug("loggedFelhasznaloId: " + loggedFelhasznaloId);
            Logger.Debug("loggedCsoporttagId: " + loggedCsoporttagId);

            if (String.IsNullOrEmpty(loggedFelhasznaloId) || String.IsNullOrEmpty(loggedCsoporttagId))
            {
                Logger.Debug("IsUserDefinedByQs end: return false;");
                Logger.DebugEnd();
                return false;
            }

            // csak a l�tez�s vizsg�lata, lehet �res is

            int i = 0;
            int cntKeys = ParentPage.Request.QueryString.Keys.Count;
            while (i < cntKeys && ParentPage.Request.QueryString.Keys[i] != QueryStringVars.LoggedHelyettesitesId)
            {
                i++;
            }

            if (i >= cntKeys)
            {
                Logger.Debug("loggedHelyettesitesId missing");
                Logger.Debug("IsUserDefinedByQs end: return false;");
                Logger.DebugEnd();
                return false;
            }

            Logger.Debug("IsUserDefinedByQs end: return true;");
            Logger.DebugEnd();
            return true;
        }

        /// <summary>
        /// Ellen�rzi, hogy nincs-e �tadva a QueryStringben is felhaszn�l�i adat, �s ha van, �sszeveti az aktu�lis
        /// bel�p�si adatokkal: ha nem egyezik, egy r�gi bel�p�s adatai "ragadtak bent". Els�sorban az eMigration
        /// eRecordb�l val� h�v�sakor sz�ks�ges ellen�rz�s.
        /// Ha a QueryString nem tartalmaz egy�rtelm� bel�p�si adatokat, akkor is igazat ad vissza! El�tte az
        /// IsUserDefinedByQs h�v�ssal �rdemes meggy�z�dni, hogy van-e megadva el�g adat a QueryStringben!
        /// </summary>
        /// <param name="ParentPage"></param>
        /// <returns></returns>
        private static bool IsCurrentUserDifferentFromUserDefinedByQs(Page ParentPage)
        {
            Logger.DebugStart();
            Logger.Debug("IsCurrentUserDifferentFromUserDefinedByQs start");

            if (IsUserDefinedByQs(ParentPage))
            {
                string loggedFelhasznaloId = ParentPage.Request.QueryString.Get(QueryStringVars.LoggedFelhasznaloId);
                string loggedCsoporttagId = ParentPage.Request.QueryString.Get(QueryStringVars.LoggedCsoporttagId);
                string loggedHelyettesitesId = ParentPage.Request.QueryString.Get(QueryStringVars.LoggedHelyettesitesId);

                Logger.Debug("loggedFelhasznaloId: " + loggedFelhasznaloId);
                Logger.Debug("loggedCsoporttagId: " + loggedCsoporttagId);
                Logger.Debug("loggedHelyettesitesId: " + loggedHelyettesitesId);

                string felhasznaloId = FelhasznaloProfil.FelhasznaloId(ParentPage);
                string csoporttagId = FelhasznaloProfil.FelhasznaloCsoportTagId(ParentPage);
                string helyettesitesId = FelhasznaloProfil.HelyettesitesId(ParentPage);
                Logger.Debug("felhasznaloId: " + felhasznaloId);
                Logger.Debug("csoporttagId: " + csoporttagId);
                Logger.Debug("helyettesitesId: " + helyettesitesId);

                if (loggedFelhasznaloId != felhasznaloId)
                {
                    Logger.Debug("loggedFelhasznaloId != felhasznaloId");
                    Logger.Debug("IsCurrentUserDifferentFromUserDefinedByQs end: return true;");
                    Logger.DebugEnd();
                    return true;
                }
                else if (loggedCsoporttagId != csoporttagId)
                {
                    Logger.Debug("loggedCsoporttagId != csoporttagId");
                    Logger.Debug("IsCurrentUserDifferentFromUserDefinedByQs end: return true;");
                    Logger.DebugEnd();
                    return true;
                }
                else if (loggedHelyettesitesId != helyettesitesId)
                {
                    Logger.Debug("loggedHelyettesitesId != helyettesitesId");
                    Logger.Debug("IsCurrentUserDifferentFromUserDefinedByQs end: return true;");
                    Logger.DebugEnd();
                    return true;
                }

            }
            else
            {
                Logger.Debug("IsCurrentUserDifferentFromUserDefinedByQs end: return true;");
                Logger.DebugEnd();
                return true;
            }

            Logger.Debug("IsCurrentUserDifferentFromUserDefinedByQs end: return false");
            Logger.DebugEnd();
            return false;

        }

        public static void CheckLogin(Page ParentPage)
        {
            Logger.DebugStart();
            Logger.Debug("CheckLogin start");
            if (ParentPage == null) return;

            string _isLogged = UI.GetSession(ParentPage, Constants.isLogged);
            string _isOrgSet = UI.GetSession(ParentPage, Constants.isOrgSet);
            if (_isLogged != "true")
            {
                Logger.Debug("_isLogged != true");
                //ParentPage.Session[Constants.PageNameBeforeLogin] = ParentPage.Request.Url.AbsoluteUri;
                SetPageNameBeforeLogin(ParentPage);
                if (UI.GetAppSetting("DefaultLoginType") == "Windows")
                {
                    ParentPage.Session[Constants.LoginType] = "Windows";
                    ParentPage.Session[Constants.UserName] = ParentPage.User.Identity.Name;
                    ParentPage.Session[Constants.Password] = "";
                    Login(ParentPage);

                    //ParentPage.Session[Constants.PageNameBeforeLogin] = ParentPage.Request.Url.AbsoluteUri;
                    SetPageNameBeforeLogin(ParentPage);
                    string _OwnLogin = UI.GetSession(ParentPage, Constants.OwnLogin);
                    if (_OwnLogin != "1")
                    {
                        Logger.Debug("_OwnLogin != 1");
                        UI.RedirectPage(ParentPage, "Login.aspx");
                    }
                }
                else
                {
                    UI.RedirectPage(ParentPage, "Login.aspx");
                }
            }
            else
            {
                Logger.Debug("_isLogged == true");
                if (_isOrgSet != "true")
                {
                    //ParentPage.Session[Constants.PageNameBeforeLogin] = ParentPage.Request.Url.AbsoluteUri;
                    //SetPageNameBeforeLogin(ParentPage);
                    Logger.Debug("_isOrgSet != true");
                    //Logger.Debug(" -> redirect to Login.aspx");
                    Login(ParentPage);
                }
                else
                {
                    Logger.Debug("_isOrgSet == true");
                    if (IsUserDefinedByQs(ParentPage) && IsCurrentUserDifferentFromUserDefinedByQs(ParentPage))
                    {
                        //ParentPage.Session[Constants.PageNameBeforeLogin] = ParentPage.Request.Url.AbsoluteUri;
                        SetPageNameBeforeLogin(ParentPage);
                        Logger.Debug("IsUserDefinedByQs(ParentPage) && IsCurrentUserDifferentFromUserDefinedByQs(ParentPage)");
                        Login(ParentPage);
                    }
                }
            }
            //Log.StartPage(ParentPage);

            // Felhasznaloi profil ellenorzes es ha nincsen meg vagy lejart az ideje, akkor betolti:

            FunctionRights.FelhasznaloProfilIsCached(ParentPage);

            // nekrisz inserted 
            if (String.IsNullOrEmpty(UI.GetSession(ParentPage, Constants.Munkaallomas)))
            {
                //bernat.laszlo added
                string remoteAddr = ParentPage.Request.ServerVariables["remote_addr"];
                try
                {
                    ParentPage.Session[Constants.Munkaallomas] = System.Net.Dns.GetHostEntry(remoteAddr).HostName;
                }
                catch
                {
                    ParentPage.Session[Constants.Munkaallomas] = remoteAddr;
                }
                //eddig
            }
            Logger.Debug("CheckLogin end");
            Logger.DebugEnd();
        }

        public static void OwnButton_Click(Page ParentPage)
        {
            OwnButton_Click(ParentPage, true);
        }

        public static void OwnButton_Click(Page ParentPage, bool redirectNeed)
        {
            Logger.DebugStart();
            Logger.Debug("OwnButton_Click start");

            // Audit log login bejegyz�s el�tt el�z� login idej�nek lek�r�se, ha m�g nem tett�k meg:
            if (ParentPage.Session[Constants.SessionNames.LastLoginTime] == null
                && ParentPage.Session[Constants.FelhasznaloId] != null)
            {
                SetLastLoginTimeToSession(ParentPage, ParentPage.Session[Constants.FelhasznaloId].ToString());
            }

            //bejelentkez�s loggol�sa ha van helyettes�t�s
            Log.LogIn(ParentPage);

            // Ez a sor azert kell, hogy minden felhasznalo valtaskor ujra toltse a szerepkoroket!!!!
            FunctionRights.FelhasznaloProfilLoadToCache(ParentPage);

            string _PageNameBeforeLogin = UI.GetSession(ParentPage, Constants.PageNameBeforeLogin);

            ParentPage.Session[Constants.OwnLogin] = "1";

            if (redirectNeed)
            {
                Logger.Debug("redirectNeed");
                if (!string.IsNullOrEmpty(_PageNameBeforeLogin) && _PageNameBeforeLogin != "/eadmin/Login.aspx")
                {
                    Logger.Debug("_PageNameBeforeLogin: " + _PageNameBeforeLogin);
                    ParentPage.Response.Redirect(_PageNameBeforeLogin, true);
                }
                else
                {
                    Logger.Debug("Redirect Default.aspx");
                    ParentPage.Response.Redirect("Default.aspx", true);
                }
            }

            Logger.Debug("OwnButton_Click end");
            Logger.DebugEnd();
        }

        public static void CheckHelyettesitettOrMoreGroups(Page ParentPage, string Helyettesito_Id)
        {
            Logger.DebugStart();
            Logger.Debug("CheckHelyettesitettOrMoreGroups start");
            if (Authentication.IfHelyettesitettExist(ParentPage, Helyettesito_Id))
            {
                Logger.Debug("CheckHelyettesitettOrMoreGroups end: helyettesitettExist");
                Logger.DebugEnd();
                return;
            }
            //else if (Authentication.IfMoreGroupsExist(ParentPage, Helyettesito_Id))
            else if (Authentication.GetGroupCount(ParentPage, Helyettesito_Id) > 1)
            {
                Logger.Debug("CheckHelyettesitettOrMoreGroups end: moreGroupsExist");
                Logger.DebugEnd();
                return;
            }
            else
            {
                string _PageNameBeforeLogin = UI.GetSession(ParentPage, Constants.PageNameBeforeLogin);
                ParentPage.Session[Constants.OwnLogin] = "1";
                SetSimpleLogin(ParentPage);
                if (!string.IsNullOrEmpty(_PageNameBeforeLogin))
                {
                    
                    Logger.Debug("CheckHelyettesitettOrMoreGroups end, _PageNameBeforeLogin: " + _PageNameBeforeLogin);
                    Logger.DebugEnd();
                    ParentPage.Response.Redirect(_PageNameBeforeLogin, true);
                }
                else
                {
                    Logger.Debug("CheckHelyettesitettOrMoreGroups end, redirect Default.aspx");
                    Logger.DebugEnd();
                    ParentPage.Response.Redirect("Default.aspx", true);
                }
            }

            Logger.Debug("CheckHelyettesitettOrMoreGroups end");
            Logger.DebugEnd();
        }

        public static void SetHelyettesitesToSession(Page page, string helyettesites_id, string csoporttag_id)
        {
            SetHelyettesitesToSession(page, helyettesites_id, csoporttag_id, true);
        }


        public static void SetHelyettesitesToSession(Page page, string helyettesites_id, string csoporttag_id, bool redirectNeed)
        {
            page.Session[Constants.isOrgSet] = "false";
            page.Session[Constants.FelhasznaloCsoportTagId] = "";
            page.Session[Constants.FelhasznaloSzervezetId] = "";
            page.Session[Constants.FelhasznaloSzervezetNev] = "";

            page.Session[Constants.HelyettesitettId] = "";
            page.Session[Constants.FelhasznaloId] = "";
            page.Session[Constants.HelyettesitettNev] = "";
            page.Session[Constants.HelyettesitesMod] = "";
            page.Session[Constants.LoginUserNev] = "";
            page.Session[Constants.IsAdminInSzervezet] = "";

            Logger.DebugStart();
            Logger.Debug("SetHelyettesitesToSession start");
            string loginUserId = FelhasznaloProfil.LoginUserId(page);
            Logger.Debug("loginUserId: " + loginUserId);

            if (!string.IsNullOrEmpty(helyettesites_id) && !string.IsNullOrEmpty(loginUserId))
            {
                Logger.Debug("helyettesites_id: " + helyettesites_id);
                // Ellen�rz�s, van-e ilyen helyettes�t�s az adatb�zisban:

                KRT_HelyettesitesekService service = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
                execParam.Felhasznalo_Id = loginUserId;

                KRT_HelyettesitesekSearch search = new KRT_HelyettesitesekSearch();

                search.Felhasznalo_ID_helyettesito.Value = loginUserId;
                search.Felhasznalo_ID_helyettesito.Operator = Query.Operators.equals;

                search.Id.Value = helyettesites_id;
                search.Id.Operator = Query.Operators.equals;

                string str_now = DateTime.Now.ToString();

                // kezd <= now <= vege

                search.HelyettesitesKezd.Value = str_now;
                search.HelyettesitesKezd.Operator = Query.Operators.lessorequal;

                search.HelyettesitesVege.Value = str_now;
                search.HelyettesitesVege.Operator = Query.Operators.greaterorequal;

                Result result = service.GetAllWithExtension(execParam, search);
                if (!string.IsNullOrEmpty(result.ErrorCode))
                {
                    Logger.Error(String.Format("KRT_HelyettesitesekService GetAllWithExtension hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
                    return;
                }
                else
                {
                    if (result.Ds != null && result.Ds.Tables[0].Rows.Count == 1)
                    {
                        // helyettes�t�s ok:
                        Logger.Debug("result.Ds != null");
                        Logger.Debug("result.Ds.Tables[0].Rows.Count = " + result.Ds.Tables[0].Rows.Count);
                        page.Session[Constants.HelyettesitesId] = helyettesites_id;

                        System.Data.DataRow row = result.Ds.Tables[0].Rows[0];
                        string helyettesitett_id = row["Felhasznalo_ID_helyettesitett"].ToString();
                        Logger.Debug("helyettesitett_id: " + helyettesitett_id);
                        page.Session[Constants.HelyettesitettId] = helyettesitett_id;
                        page.Session[Constants.FelhasznaloId] = helyettesitett_id;

                        String helyettesitettNev = row["Felhasznalo_Nev_helyettesitett"].ToString();
                        String helyettesitoNev = row["Felhasznalo_Nev_helyettesito"].ToString();

                        page.Session[Constants.HelyettesitettNev] = helyettesitettNev;
                        page.Session[Constants.LoginUserNev] = helyettesitoNev;

                        string helyettesitesmod = row["HelyettesitesMod"].ToString();
                        // r�gi rekordok miatt
                        if (String.IsNullOrEmpty(helyettesites_id))
                        {
                            helyettesitesmod = KodTarak.HELYETTESITES_MOD.Helyettesites;
                        }
                        page.Session[Constants.HelyettesitesMod] = helyettesitesmod;

                        #region Helyettes�tett szervezet�nek lek�r�se (helyettes�t�sn�l azt t�roljuk le sessionbe)
                        string helyettesites_csoporttag_id = row["CsoportTag_ID_helyettesitett"].ToString();

                        //if (!string.IsNullOrEmpty(helyettesites_csoporttag_id))
                        //{
                        //    Logger.Debug("Csoporttag id a helyettes�t�s alapj�n: " + helyettesites_csoporttag_id);
                        //    String csoport_id = row["Csoport_Id_helyettesitett"].ToString();
                        //    String csoport_nev = row["Csoport_Nev_helyettesitett"].ToString();

                        //    //page.Session[Constants.FelhasznaloSzervezetId] = csoport_id;
                        //    //page.Session[Constants.FelhasznaloSzervezetNev] = csoport_nev;

                        //    //page.Session[Constants.FelhasznaloCsoportTagId] = helyettesites_csoporttag_id;

                        //    //if (!string.IsNullOrEmpty(csoport_id))
                        //    //{
                        //    //    page.Session[Constants.isOrgSet] = "true";
                        //    //}

                        //    //Logger.Debug("FelhasznaloSzervezetId: " + csoport_id + " FelhasznaloSzervezetNev: " + csoport_nev);

                        //    SetFelhasznaloCsoportToSession(page, helyettesites_csoporttag_id);

                        //}
                        //else
                        //{
                        // van-e t�bb csoportja is a helyettes�tettnek
                        KRT_CsoportTagokService service_csoporttagok = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                        ExecParam execParam_csoporttagok = UI.SetExecParamDefault(page, new ExecParam());
                        execParam_csoporttagok.Felhasznalo_Id = helyettesitett_id;

                        Result result_csoporttagok = service_csoporttagok.GetSzervezet(execParam_csoporttagok);

                        if (!string.IsNullOrEmpty(result_csoporttagok.ErrorCode))
                        {
                            Logger.Error("KRT_CsoportTagokService GetSzervezet hiba!", execParam_csoporttagok, result_csoporttagok);
                            Logger.Debug("SetHelyettesitesToSession end");
                            Logger.DebugEnd();
                            return;
                        }

                        int cntCsoportok = result_csoporttagok.Ds.Tables[0].Rows.Count;
                        Logger.Debug("Helyettes�tett csoporttags�gainak sz�ma: " + cntCsoportok.ToString());
                        if (cntCsoportok == 0)
                        {
                            Logger.Error("A helyettes�tett szervezete nem azonos�that�!");
                            Logger.Debug("SetHelyettesitesToSession end");
                            Logger.DebugEnd();
                            return;
                        }

                        if (!string.IsNullOrEmpty(csoporttag_id))
                        {
                            Logger.Debug("Csoporttag id �tadott param�ter alapj�n: " + csoporttag_id);
                            System.Data.DataRow[] rows = result_csoporttagok.Ds.Tables[0].Select("Id = '" + csoporttag_id + "'");

                            if (rows.Length == 0)
                            {
                                Logger.Error("Nem tal�lhat� a helyettes�tetthez az �tadott azonos�t�nak megfelel� csoporttags�g!", execParam_csoporttagok, result_csoporttagok);
                                Logger.Debug("SetHelyettesitesToSession end");
                                Logger.DebugEnd();
                                return;
                            }
                            else
                            {
                                string csoport_id = rows[0]["Csoport_Id"].ToString();
                                string csoport_nev = rows[0]["Nev"].ToString();
                                string csoport_tipus = rows[0]["Tipus1"].ToString();

                                if (!string.IsNullOrEmpty(csoport_id))
                                {
                                    page.Session[Constants.FelhasznaloCsoportTagId] = csoporttag_id;
                                    page.Session[Constants.FelhasznaloSzervezetId] = csoport_id;
                                    page.Session[Constants.FelhasznaloSzervezetNev] = csoport_nev;
                                    page.Session[Constants.isOrgSet] = "true";
                                    page.Session[Constants.IsAdminInSzervezet] = csoport_tipus == "R" ? "1" : "0";

                                    Logger.Debug("FelhasznaloSzervezetId: " + csoport_id + " FelhasznaloSzervezetNev: " + csoport_nev);
                                }
                                else
                                {
                                    Logger.Error("Nem tal�lhat� a helyettes�tett szervezete!", execParam_csoporttagok, result_csoporttagok);
                                    Logger.Debug("SetHelyettesitesToSession end");
                                    Logger.DebugEnd();
                                    return;
                                }

                            }
                        }
                        else if (cntCsoportok == 1)
                        {
                            string csoporttag_id_fromDs = result_csoporttagok.Ds.Tables[0].Rows[0]["Id"].ToString();
                            Logger.Debug("Csoporttag id a helyettes�ttett egy�rtelm� csoporttags�ga alapj�n: " + (csoporttag_id_fromDs ?? "<null>"));

                            string csoport_id = result_csoporttagok.Ds.Tables[0].Rows[0]["Csoport_Id"].ToString();
                            string csoport_nev = result_csoporttagok.Ds.Tables[0].Rows[0]["Nev"].ToString();
                            string csoport_tipus = result_csoporttagok.Ds.Tables[0].Rows[0]["Tipus1"].ToString();

                            if (!string.IsNullOrEmpty(csoport_id))
                            {
                                page.Session[Constants.FelhasznaloCsoportTagId] = csoporttag_id_fromDs;
                                page.Session[Constants.FelhasznaloSzervezetId] = csoport_id;
                                page.Session[Constants.FelhasznaloSzervezetNev] = csoport_nev;
                                page.Session[Constants.isOrgSet] = "true";
                                page.Session[Constants.IsAdminInSzervezet] = csoport_tipus == "R" ? "1" : "0";

                                Logger.Debug("FelhasznaloSzervezetId: " + csoport_id + " FelhasznaloSzervezetNev: " + csoport_nev);
                            }
                            else
                            {
                                Logger.Error("Nem tal�lhat� a helyettes�tett szervezete!", execParam_csoporttagok, result_csoporttagok);
                                Logger.Debug("SetHelyettesitesToSession end");
                                Logger.DebugEnd();
                                return;
                            }

                        }

                        //KRT_CsoportTagokService service_csoporttagok = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                        //ExecParam execParam_csoporttagok = UI.SetExecParamDefault(page, new ExecParam());
                        //execParam_csoporttagok.Felhasznalo_Id = loginUserId;
                        //KRT_CsoportTagokSearch search_csoporttagok = new KRT_CsoportTagokSearch();

                        //search_csoporttagok.Id.Value = csoporttag_id;
                        //search_csoporttagok.Id.Operator = Query.Operators.equals;

                        //Result result_csoporttagok = service_csoporttagok.GetAllWithExtension(execParam_csoporttagok, search_csoporttagok);

                        //if (!string.IsNullOrEmpty(result_csoporttagok.ErrorCode))
                        //{
                        //    Logger.Error("Hiba a csoporttagok lek�rdez�se sor�n!", execParam_csoporttagok, result_csoporttagok);
                        //    Logger.Debug("SetHelyettesitesToSession end");
                        //    Logger.DebugEnd();
                        //    return;
                        //}
                        //else if (result_csoporttagok.Ds.Tables[0].Rows.Count == 0)
                        //{
                        //    Logger.Error("Nem tal�lhat� �rv�nyes csoporttag rekord!", execParam_csoporttagok, result_csoporttagok);
                        //    Logger.Debug("SetHelyettesitesToSession end");
                        //    Logger.DebugEnd();
                        //    return;
                        //}

                        //System.Data.DataRow row_csoporttag = result_csoporttagok.Ds.Tables[0].Rows[0];
                        //String csoport_id = row_csoporttag["Csoport_Id"].ToString();
                        //String csoport_nev = row_csoporttag["Csoport_Nev"].ToString();

                        //page.Session[Constants.FelhasznaloSzervezetId] = csoport_id;
                        //page.Session[Constants.FelhasznaloSzervezetNev] = csoport_nev;

                        //page.Session[Constants.FelhasznaloCsoportTagId] = csoporttag_id;

                        //if (!string.IsNullOrEmpty(csoport_id))
                        //{
                        //    page.Session[Constants.isOrgSet] = "true";
                        //}

                        //Logger.Debug("FelhasznaloSzervezetId: " + csoport_id + " FelhasznaloSzervezetNev: " + csoport_nev);
                        //}
                        #endregion


                        Authentication.OwnButton_Click(page, redirectNeed);
                        Logger.Debug("SetHelyettesitesToSession end");
                        Logger.DebugEnd();
                    }
                    else
                    {
                        Logger.Debug("SetHelyettesitesToSession end");
                        Logger.DebugEnd();
                        return;
                    }
                }
            }
            else
            {
                Logger.Debug("SetHelyettesitesToSession end");
                Logger.DebugEnd();
                return;
            }
        }

        public static void SetPageNameBeforeLogin(Page page)
        {
            if (page.Request.Url.AbsoluteUri.IndexOf("Login.aspx", StringComparison.CurrentCultureIgnoreCase) == -1)
            {
                page.Session[Constants.PageNameBeforeLogin] = page.Request.Url.AbsoluteUri;
            }
            else
            {
                page.Session.Remove(Constants.PageNameBeforeLogin);
            }
        }

        public static void SetSimpleLogin(Page page)
        {
            page.Session["SimpleLogin"] = "true";
        }

        public static bool IsSimpleLogin(Page page)
        {
            string simpleLogin = UI.GetSession(page, "SimpleLogin");
            if (simpleLogin == "true")
            {
                return true;
            }
            return false;
        }

        public static void LogOut(Page page)
        {
            //Kijelentkez�s loggol�sa
            Log.LogOut(page);
            page.Session.RemoveAll();
            page.Response.Redirect("Login.aspx");
        }

    }


    public interface ILastLoginData
    {
        string FelhasznaloHiddenFieldUniqueId
        {
            get;
        }

        string SzervezetHiddenFieldUniqueId
        {
            get;
        }

        string FelhasznaloCsoporttagsagHiddenFieldUniqueId
        {
            get;
        }

        string HelyettesitesHiddenFieldUniqueId
        {
            get;
        }

        string LoginTypeHiddenFieldUniqueId
        {
            get;
        }
    }

}
