﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUtility
{
    public class CustomStack<T>
    {
        #region Properties

        private int _capacity;

        public int Capacity
        {

            get { return _capacity; }

            set { _capacity = value; }

        }



        public int Length
        {

            get { return Index + 1; }

        }



        private T[] _elements;

        protected T[] Elements
        {

            get { return _elements; }

            set { _elements = value; }

        }



        private int _index = -1;

        public int Index
        {

            get { return _index; }

            set { _index = value; }

        }

        private bool _fixCapacity = false;

        #endregion

        public CustomStack()
        {
            _fixCapacity = false;
            Elements = new T[Capacity];

        }



        public CustomStack(int capacity)
        {
            if (capacity < 1)
            {
                capacity = 1;
            }

            _fixCapacity = true;
            Capacity = capacity;

            Elements = new T[Capacity];

        }

        public void Push(T element)
        {

            if (this.Length == Capacity)
            {
                if (_fixCapacity)
                {
                    DropFirstElement();
                }
                else
                {
                    IncreaseCapacity();
                }

            }

            Index++;

            Elements[Index] = element;

        }


        public T Pop()
        {

            if (this.Length < 1)
            {

                throw new InvalidOperationException("Stack is empty");

            }



            T element = Elements[Index];

            Elements[Index] = default(T);

            Index--;

            return element;

        }



        public T Peek()
        {

            if (this.Length < 1)
            {

                throw new InvalidOperationException("Stack is empty");

            }



            return Elements[Index];

        }



        private void IncreaseCapacity()
        {

            Capacity++;

            Capacity *= 2;

            T[] newElements = new T[Capacity];

            Array.Copy(Elements, newElements, Elements.Length);

            Elements = newElements;

        }

        private void DropFirstElement()
        {
            T[] newElements = new T[Capacity];

            Array.Copy(Elements, 1, newElements, 0, Elements.Length - 1);

            Elements = newElements;

            Index--;
        }
    }
}
