using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUtility
{
    public class DefaultDates
    {
        public static string Today
        {
            get
            {
                return DateTime.Today.ToShortDateString();
            }
        }

        public static string EndDate
        {
            get
            {
                DateTime ervVege = new DateTime(2099, 12, 31);
                return ervVege.ToShortDateString();
            }
        }
    
    }
}
