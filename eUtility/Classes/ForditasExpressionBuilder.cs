﻿using System;
using System.CodeDom;
using System.Web;
using System.Web.Compilation;
using System.Web.UI;

namespace Contentum.eUtility
{
    /// <summary>
    /// Summary description for LabelExpressionBuilder
    /// </summary>
    [ExpressionPrefix("Forditas")]
    public class ForditasExpressionBuilder : ExpressionBuilder
    {
        public override object ParseExpression(string expression, Type propertyType, ExpressionBuilderContext context)
        {
            ExpressionData data = ExpressionData.Parse(expression);

            if (data == null)
            {
                throw new HttpException(String.Format("Érvénytelen kifejezés: {0}", expression));
            }

            return data;
        }
        public override CodeExpression GetCodeExpression(BoundPropertyEntry entry, object parsedData, ExpressionBuilderContext context)
        {
            //string controlId = entry.ControlID;
            ExpressionData data = parsedData as ExpressionData;
            string virtualPath = context.VirtualPath;


            CodeExpression[] expressionArray =
              new CodeExpression[3];
            expressionArray[0] = new CodePrimitiveExpression(data.LabelId);
            expressionArray[1] = new CodePrimitiveExpression(data.DefaultText);
            expressionArray[2] = new CodePrimitiveExpression(virtualPath);

            return new CodeMethodInvokeExpression(new CodeTypeReferenceExpression(GetType()), "GetForditas", expressionArray);
        }

        public static string GetForditas(string labelId, string defaultText, string virtualPath)
        {
            return GetForditas(labelId, defaultText, virtualPath, HttpContext.Current.Session);
        }

        public static string GetForditas(string labelId, string defaultText, string virtualPath, System.Web.SessionState.HttpSessionState session)
        {
            string nyelv = System.Globalization.CultureInfo.CurrentCulture.Name;
            string org = Contentum.eUtility.FelhasznaloProfil.OrgKod(session);
            string modul = System.Configuration.ConfigurationManager.AppSettings["ApplicationName"];
            string komponens = GetComponentPath(virtualPath);
            return ForditasokManager.GetForditas(nyelv, org, modul, komponens, labelId, defaultText);
        }

        static string GetComponentPath(string virtualPath)
        {
            string appRelativePath = VirtualPathUtility.ToAppRelative(virtualPath);
            string componentPath = appRelativePath.Substring(2);
            return componentPath;

        }

        class ExpressionData
        {
            private string _LabelId;
            public string LabelId
            {
                get
                {
                    return _LabelId;
                }
                set
                {
                    _LabelId = value;
                }
            }
            private string _DefaultText;
            public string DefaultText
            {
                get
                {
                    return _DefaultText;
                }
                set
                {
                    _DefaultText = value;
                }
            }

            public static ExpressionData Parse(string expression)
            {
                string[] parts = expression.Split('|');

                if (parts.Length == 2)
                {
                    ExpressionData item = new ExpressionData();
                    item.LabelId = parts[0].Trim();
                    item.DefaultText = parts[1].Trim();
                    return item;
                }

                return null;
            }
        }
    }
    
}