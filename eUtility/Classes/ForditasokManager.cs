﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Data;

namespace Contentum.eUtility
{
    /// <summary>
    /// Summary description for ForditasokManager
    /// </summary>
    public class ForditasokManager
    {
        private const String cache_key = "ForditasCache";

        private static readonly object _sync = new object();

        const int cacheRefreshPeriod = 600;

        private static System.Web.Caching.Cache _Cache
        {
            get
            {
                // BUG#8104: A háttérszálas folyamatoknál a System.Web.HttpContext.Current az null lesz, ilyenkor máshogy próbáljuk meg elérni a Cache objektumot:
                if (System.Web.HttpContext.Current != null)
                {
                    return System.Web.HttpContext.Current.Cache;
                }
                else
                {
                    return System.Web.HttpRuntime.Cache;
                }
            }
        }

        static string GetCacheKey(string nyelv, string org)
        {
            return String.Format("{0}.{1}", nyelv, org);
        }

        //If last character of objAzonosito is '*' then we dont filter for komponens and modul
        static string GetAzonosito(string modul, string komponens, string objAzonosito)
        {
            if(!string.IsNullOrEmpty(objAzonosito) && objAzonosito[objAzonosito.Length-1] == '*')
            {
                return "*\\*\\"+objAzonosito;
            }
            return String.Format("{0}\\{1}\\{2}", modul, komponens, objAzonosito);
        }

        public const string ADD_COLON_POSTFIX = "_AddColon";
        public static string GetForditas(string nyelv, string org, string modul, string komponens, string objAzonosito, string defaultForditas)
        {
            Dictionary<string, ForditasItem> forditasDictonary = GetForditasDictionary(nyelv, org);
            //When the objAzonosito ends with ADD_COLON_POSTFIX, then we add it to the retrieved value as a ':' char.
            //This ways we can avoid using multiple KRT_Forditasok row with the same value which differs only at the trailing ':' char.
            bool addColon = false;
            if (objAzonosito.EndsWith(ADD_COLON_POSTFIX))
            {
                objAzonosito = objAzonosito.Replace(ForditasokManager.ADD_COLON_POSTFIX,string.Empty);
                addColon = true;
            }
            string azonosito = GetAzonosito(modul, komponens, objAzonosito);

            if (forditasDictonary.ContainsKey(azonosito))
            {
                string result = forditasDictonary[azonosito].Forditas;
                if (addColon)
                {
                    result = result + ':';
                }
                return result;
            }
            else
            {
                return defaultForditas;
            }
        }

        public static void ClearCache()
        {
            System.Web.Caching.Cache cache = _Cache;
            lock (_sync)
            {
                cache.Remove(cache_key);
            }
        }

        static Dictionary<string, Dictionary<string, ForditasItem>> GetForditasCache()
        {
            System.Web.Caching.Cache cache = _Cache;

            Dictionary<string, Dictionary<string, ForditasItem>> forditasCache = cache[cache_key] as Dictionary<string, Dictionary<string, ForditasItem>>;

            if (forditasCache == null)
            {
                lock (_sync)
                {
                    forditasCache = cache[cache_key] as Dictionary<string, Dictionary<string, ForditasItem>>;

                    if (forditasCache == null)
                    {
                        forditasCache = new Dictionary<string, Dictionary<string, ForditasItem>>();
                        cache.Insert(cache_key, forditasCache, null, DateTime.Now.AddMinutes(cacheRefreshPeriod),
                        System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.NotRemovable, null);
                    }
                }
            }

            return forditasCache;
        }

        static Dictionary<string, ForditasItem> GetForditasDictionary(string nyelv, string org)
        {
            Dictionary<string, Dictionary<string, ForditasItem>> forditasCache = GetForditasCache();

            string dictKey = GetCacheKey(nyelv, org);

            Dictionary<string, ForditasItem> forditasDictonary;

            if (!forditasCache.ContainsKey(dictKey))
            {
                lock (_sync)
                {
                    if (!forditasCache.ContainsKey(dictKey))
                    {
                        forditasDictonary = new Dictionary<string, ForditasItem>();
                        List<ForditasItem> forditasokList = GetForditasok(nyelv, org);

                        if (forditasokList != null)
                        {
                            foreach (ForditasItem item in forditasokList)
                            {
                                forditasDictonary.Add(item.GetAzonosito(), item);
                            }
                        }

                        forditasCache.Add(dictKey, forditasDictonary);
                    }
                }
            }

            return forditasCache[dictKey];
        }

        static List<ForditasItem> GetForditasok(string nyelv, string org)
        {
            ExecParam execParam = new ExecParam();

            KRT_ForditasokService _KRT_ForditasokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_ForditasokService();

            KRT_ForditasokSearch _KRT_ForditasokSearch = new KRT_ForditasokSearch();
            _KRT_ForditasokSearch.NyelvKod.Value = nyelv;
            _KRT_ForditasokSearch.NyelvKod.Operator = Contentum.eQuery.Query.Operators.equals;
            _KRT_ForditasokSearch.OrgKod.Value = org;
            _KRT_ForditasokSearch.OrgKod.Operator = Contentum.eQuery.Query.Operators.equals;


            Result res = _KRT_ForditasokService.GetAll(execParam, _KRT_ForditasokSearch);

            if (!res.IsError)
            {
                List<ForditasItem> forditasok = new List<ForditasItem>();

                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    ForditasItem item = ForditasItem.Create(row);
                    forditasok.Add(item);
                }

                return forditasok;
            }
            else
            {
                Logger.Error("GetForditasok", execParam, res);
                return null;
            }

        }

        class ForditasItem
        {
            private string _NyelvKod;
            public string NyelvKod
            {
                get { return _NyelvKod; }
                set { _NyelvKod = value; }
            }
            private string _OrgKod;
            public string OrgKod
            {
                get { return _OrgKod; }
                set { _OrgKod = value; }
            }
            private string _Modul;
            public string Modul
            {
                get { return _Modul; }
                set { _Modul = value; }
            }
            private string _Komponens;
            public string Komponens
            {
                get { return _Komponens; }
                set { _Komponens = value; }
            }
            private string _ObjAzonosito;
            public string ObjAzonosito
            {
                get { return _ObjAzonosito; }
                set { _ObjAzonosito = value; }
            }
            private string _Forditas;
            public string Forditas
            {
                get { return _Forditas; }
                set { _Forditas = value; }
            }

            public string GetAzonosito()
            {
                return ForditasokManager.GetAzonosito(Modul, Komponens, ObjAzonosito);
            }

            public static ForditasItem Create(DataRow row)
            {
                ForditasItem item = new ForditasItem();

                item.NyelvKod = row["NyelvKod"].ToString();
                item.OrgKod = row["OrgKod"].ToString();
                item.Modul = row["Modul"].ToString();
                item.Komponens = row["Komponens"].ToString();
                item.ObjAzonosito = row["ObjAzonosito"].ToString();
                item.Forditas = row["Forditas"].ToString();

                return item;
            }
        }
    }
}