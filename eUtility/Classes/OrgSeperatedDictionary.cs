﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUtility
{
    public class OrgSeperatedDictionary<TKey,TValue>
    {
        private readonly static object _sync = new object();

        private Dictionary<string, Dictionary<TKey, TValue>> items;
        private IEqualityComparer<TKey> comparer;

        public OrgSeperatedDictionary()
        {
            items = new Dictionary<string, Dictionary<TKey, TValue>>(StringComparer.CurrentCultureIgnoreCase);
        }

        public OrgSeperatedDictionary(IEqualityComparer<TKey> comparer)
        {
            this.comparer = comparer;
            items = new Dictionary<string, Dictionary<TKey, TValue>>(StringComparer.CurrentCultureIgnoreCase);
        }

        public Dictionary<TKey, TValue> Get(string Org_Id)
        {
            ResultError.CheckOrg(Org_Id);

            if (!items.ContainsKey(Org_Id))
            {
                lock (_sync)
                {
                    if (!items.ContainsKey(Org_Id))
                    {
                        if (comparer != null)
                        {
                            items[Org_Id] = new Dictionary<TKey, TValue>(comparer);
                        }
                        else
                        {
                            items[Org_Id] = new Dictionary<TKey, TValue>();
                        }
                    }
                }
            }

            return items[Org_Id];
        }

        public TValue this[TKey key]
        {
            get
            {
                foreach (Dictionary<TKey, TValue> item in items.Values)
                {
                    if (item.ContainsKey(key))
                    {
                        return item[key];
                    }
                }

                return default(TValue);
            }
        }

        public string GetOrg_Id(TKey key)
        {
            foreach (string Org_id in items.Keys)
            {
                if (items[Org_id].ContainsKey(key))
                {
                    return Org_id;
                }
            }

            return null;
        }
    }
}
