﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using log4net.Core;

namespace Contentum.eUtility
{
    public sealed class PerformanceCounters
    {
        private static bool categoryExist = PerformanceCounterCategory.Exists(CategoryName);
        public static bool Enabled
        {
            get
            {
                if (!categoryExist)
                    return false;
                return true;
            }
        }

        public static long GetSystemTicks(long TimeSpanTicks)
        {
            double rate = (double)Stopwatch.Frequency / (double)10000000;
            long ret = (long)(TimeSpanTicks * rate);
            return ret;
        }

        private static bool TryGetElapsedTime(string StartDate, string EndDate, out TimeSpan ElapsedTime)
        {
            if (!String.IsNullOrEmpty(StartDate) && !String.IsNullOrEmpty(EndDate))
            {
                DateTime dtPageStart;
                DateTime dtPageEnd;
                if (DateTime.TryParse(StartDate, out dtPageStart) && DateTime.TryParse(EndDate, out dtPageEnd))
                {
                    ElapsedTime = dtPageEnd.Subtract(dtPageStart);
                    return true;
                }
            }
            ElapsedTime = TimeSpan.Zero;
            return false;
        }

        private static bool CountError(Level ErrorLevel)
        {
            if (ErrorLevel.CompareTo(Level.Warn) >= 0)
            {
                return true;
            }
            return false;
        }

        private const string CategoryName = "EDOK Alkalmazás";

        static PerformanceCounters()
        {
        }

        #region Counters

        #region WebSite
        private static readonly PerformanceCounter pageStartPerSec =
            (categoryExist) ? new PerformanceCounter(CategoryName, "Weboldal elindulásának gyakorisága", false): null;

        public static PerformanceCounter PageStartPerSec
        {
            get { return pageStartPerSec; }
        } 

        private static readonly PerformanceCounter pageEndPerSec =
            (categoryExist) ? new PerformanceCounter(CategoryName, "Weboldal befejeződésének gyakorisága", false) : null;

        public static PerformanceCounter PageEndPerSec
        {
            get { return pageEndPerSec; }
        } 

        private static readonly PerformanceCounter pageRuntimePerSec =
            (categoryExist) ? new PerformanceCounter(CategoryName, "Weboldalak össz futási ideje (ms/s)", false) : null;

        public static PerformanceCounter PageRuntimePerSec
        {
            get { return pageRuntimePerSec; }
        } 

        private static readonly PerformanceCounter pageRuntimeAverage =
            (categoryExist) ? new PerformanceCounter(CategoryName, "Weboldal átlagos futási ideje (s)", false) : null;

        public static PerformanceCounter PageRuntimeAverage
        {
            get { return pageRuntimeAverage; }
        } 

        private static readonly PerformanceCounter pageRuntimeAveragBase =
            (categoryExist) ? new PerformanceCounter(CategoryName, "Weboldal átlagos futási ideje alap", false) : null;

        public static PerformanceCounter PageRuntimeAveragBase
        {
            get { return pageRuntimeAveragBase; }
        }

        private static readonly PerformanceCounter pageErrorPerSec =
            (categoryExist) ? new PerformanceCounter(CategoryName, "Weboldalon a hiba gyakorisága", false) : null;

        public static PerformanceCounter PageErrorPerSec
        {
            get { return PerformanceCounters.pageErrorPerSec; }
        } 


        #endregion

        #region WebService
        private static readonly PerformanceCounter wsStartPerSec =
            (categoryExist) ? new PerformanceCounter(CategoryName, "Webszolgáltatás elindulásának gyakorisága", false) : null;

        public static PerformanceCounter WsStartPerSec
        {
            get { return wsStartPerSec; }
        } 

        private static readonly PerformanceCounter wsEndPerSec =
            (categoryExist) ? new PerformanceCounter(CategoryName, "Webszolgáltatás befejeződésének gyakorisága", false) : null;

        public static PerformanceCounter WsEndPerSec
        {
            get { return wsEndPerSec; }
        } 

        private static readonly PerformanceCounter wsRuntimePerSec =
            (categoryExist) ? new PerformanceCounter(CategoryName, "Webszolgáltatások össz futási ideje (ms/s)", false) : null;

        public static PerformanceCounter WsRuntimePerSec
        {
            get { return wsRuntimePerSec; }
        } 

        private static readonly PerformanceCounter wsRuntimeAverage =
            (categoryExist) ? new PerformanceCounter(CategoryName, "Webszolgáltatás átlagos futási ideje (s)", false) : null;

        public static PerformanceCounter WsRuntimeAverage
        {
            get { return wsRuntimeAverage; }
        } 

        private static readonly PerformanceCounter wsRuntimeAveragBase =
            (categoryExist) ? new PerformanceCounter(CategoryName, "Webszolgáltatás átlagos futási ideje alap", false) : null;

        public static PerformanceCounter WsRuntimeAveragBase
        {
            get { return wsRuntimeAveragBase; }
        }

        private static readonly PerformanceCounter wsErrorPerSec =
            (categoryExist) ? new PerformanceCounter(CategoryName, "Webszolgáltatásban a hiba gyakorisága", false) : null;

        public static PerformanceCounter WsErrorPerSec
        {
            get { return PerformanceCounters.wsErrorPerSec; }
        } 

        #endregion

        #endregion

        public static void PageStarted()
        {
            if (Enabled)
            {
                PageStartPerSec.Increment();
            }
        }

        public static void PageErrored(Level ErrorLevel)
        {
            if (Enabled)
            {
                if (CountError(ErrorLevel))
                {
                    PageErrorPerSec.Increment();
                }
            }
        }

        public static void PageEnded(string StartDate, string EndDate)
        {
            if (Enabled)
            {
                PerformanceCounters.PageEndPerSec.Increment();
                TimeSpan runTime;
                if (TryGetElapsedTime(StartDate, EndDate, out runTime))
                {
                    PerformanceCounters.PageRuntimePerSec.IncrementBy((long)runTime.TotalMilliseconds);
                    PerformanceCounters.PageRuntimeAverage.IncrementBy(PerformanceCounters.GetSystemTicks(runTime.Ticks));
                    PerformanceCounters.PageRuntimeAveragBase.Increment();
                }
            }
        }

        public static void WebServiceStarted(bool HasParent)
        {
            if (Enabled)
            {
                WsStartPerSec.Increment();
            }
        }

        public static void WebserviceErrored(Level ErrorLevel, bool HasParent)
        {
            if (Enabled)
            {
                if (CountError(ErrorLevel))
                {
                    WsErrorPerSec.Increment();
                }
            }
        }

        public static void WebServiceEnded(string StartDate, string EndDate, bool HasParent)
        {
            if (Enabled)
            {
                PerformanceCounters.WsEndPerSec.Increment();
                TimeSpan runTime;
                if (TryGetElapsedTime(StartDate, EndDate, out runTime))
                {
                    PerformanceCounters.WsRuntimePerSec.IncrementBy((long)runTime.TotalMilliseconds);
                    PerformanceCounters.WsRuntimeAverage.IncrementBy(PerformanceCounters.GetSystemTicks(runTime.Ticks));
                    PerformanceCounters.WsRuntimeAveragBase.Increment();
                }
            }
        }
    }
}
