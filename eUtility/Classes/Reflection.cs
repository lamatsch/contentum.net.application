﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Contentum.eUtility
{
    public static class Reflection
    {
        public static List<string> GetAttributeNames(Type t)
        {
            List<string> result = new List<string>();
            PropertyInfo[] propertyInfos;
            propertyInfos = t.GetProperties();

            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                result.Add(propertyInfo.Name);
            }

            return result;
        }
    }
}
