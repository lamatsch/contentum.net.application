﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Contentum.eUtility
{
    public class RegExp
    {
        /// <summary>
        /// Megkeresi az összes minta előfordulást a szövegben.
        /// </summary>
        /// <param name="pattern"></param>
        /// <param name="text"></param>
        /// <returns>Visszaadja az összes találatot</returns>
        public static List<string> MatchesAll(string pattern, string text)
        {
            List<string> result = new List<string>();

            Regex rx = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            MatchCollection matches = rx.Matches(text);
            foreach (Match match in matches)
            {
                result.Add(match.Value.ToString());
            }
            return result;
        }
        /// <summary>
        /// Megkeresi az összes email címet a szövegben
        /// </summary>
        /// <param name="pattern"></param>
        /// <param name="text"></param>
        /// <returns>Visszaadja az összes címet. Minden elem csak egyszer fordul elő.</returns>
        public static Dictionary<string, string> EmailAddressMatchesAll(string pattern, string text)
        {
            if (string.IsNullOrEmpty(pattern))
                return null;
            StringBuilder stringItem = new StringBuilder(text);
            Dictionary<string, string> result = new Dictionary<string, string>();

            stringItem = TextEmailReFormat(stringItem);

            string[] splitted = stringItem.ToString().Split(new char[] { ' ' });
            Regex rxp = new Regex(pattern, RegexOptions.IgnoreCase);
            foreach (string item in splitted)
            {
                if (!item.Contains("@"))
                    continue;

                Match m = rxp.Match(item.ToString());
                if (m.Success)
                {
                    if (!result.ContainsKey(m.Value))
                        result.Add(m.Value, m.Value);
                }
            }
            return result;
        }

        public static bool CheckValidEmailAddress(string email)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (match.Success)
                return true;
            else
                return false;
        }

        private static StringBuilder TextEmailReFormat(StringBuilder stringItem)
        {
            if (stringItem.ToString().Contains(">"))
            {
                stringItem = stringItem.Replace(">", "> ");
            }
            if (stringItem.ToString().Contains("<"))
            {
                stringItem = stringItem.Replace("<", "< ");
            }
            if (stringItem.ToString().Contains("mailto:"))
            {
                stringItem = stringItem.Replace("mailto:", "mailto: ");
            }
            if (stringItem.ToString().Contains("\""))
            {
                stringItem = stringItem.Replace("\"", " \" ");
            }

            return stringItem;
        }
    }
}

