using System;
using System.Collections.Generic;
using System.Text;

[assembly: CLSCompliant(true)]
namespace Contentum.eUtility
{
    public class CommandName
    {
        public const string New = "New";
        public const string Modify = "Modify";
        public const string View = "View";
        public const string Invalidate = "Invalidate";
        public const string Default = "Default";
        public const string Command = "Command";
        public const string Commandname = "CommandName";
        public const string Save = "Save";
        public const string Ok = "Ok";
        public const string SaveAndPrint = "SaveAndPrint";
        public const string Search = "Search";
        public const string SaveTemplate = "SaveTemplate";
        public const string LoadTemplate = "LoadTemplate";
        public const string LoadTemplateBase = "LoadTemplateBase";
        public const string NewTemplate = "NewTemplate";
        public const string InvalidateTemplate = "InvalidateTemplate";
        public const string Lock = "Lock";
        public const string Unlock = "Unlock";
        public const string ViewHistory = "ViewHistory";
        public const string DesignView = "DesignView";
        public const string SendObjects = "SendObjects";
        public const string Irattarozasra = "Irattarozasra";
        public const string UgyiratIrattarAtvetel = "UgyiratIrattarAtvetel";
        public const string Back = "Back";
        public const string KolcsonzesJovahagyas = "KolcsonzesJovahagyas";
        public const string RendbenEsNyomtat = "RendbenEsNyomtat";
        public const string Nyomtat = "Nyomtat";
        public const string SkontrobolKivesz = "SkontrobolKivesz";
        public const string SkontrobaHelyez = "SkontrobaHelyez";
        public const string KiadasOsztalyra = "KiadasOsztalyra";
        // kiemelve az eRecord Utility-b�l, eMigration miatt
        public const string SaveAndClose = "SaveAndClose";
        public const string SaveAndNew = "SaveAndNew";
        public const string SelejtezesreKijeloles = "SelejtezesreKijeloles";
        public const string SelejtezesreKijelolesVisszavonasa = "SelejtezesreKijelolesVisszavonasa";
        public const string Selejtezes = "Selejtezes";
        public const string Munkanaplo = "Munkanaplo";
        public const string Revalidate = "Revalidate";

        //bernat.laszlo added
        public const string NewResultList = "NewResultList";
        public const string ExcelExport = "ExcelExport";
        //bernat.laszlo eddig

        #region BLG_2956
        //LZS - Deleg�l�s parancs
        public const string Delegate = "Delegate";
        #endregion

        #region CR3155 - csv export
        public const string CSVExport = "CSVExport";
        #endregion

        #region CR3194 - Kisherceg megoszt�s
        public const string Share = "Share";
        #endregion CR3194 - Kisherceg megoszt�s
        
        #region CR3207 - e-Migrationban �gyiratpotl� lap nyomtat�s
        public const string UgyiratpotloNyomtatasa = "UgyiratpotloNyomtatasa";
        #endregion CR3207 - e-Migrationban �gyiratpotl� lap nyomtat�s
        
        public const string Sztorno = "Sztorno";
        public const string Felulvizsgalat = "Felulvizsgalat";
        public const string JegyzekZarolas = "JegyzekZarolas";
        public const string JegyzekVegrehajtas = "JegyzekVegrehajtas";

        public const string ITSZModosit = "ITSZModosit";

        //BLG 2102
        public const string MegsJegyzNyomtatas = "MegsJegyzNyomtatas";
        public const string PrintSSRSFutar = "PrintSSRSFutar";

        public const string OCR = "OCR";

        public const string UgyiratUgyintezesiIdoUjraSzamolas = "UgyiratUgyintezesiIdoUjraSzamolas";
        public const string UgyintezesiIdoUjraSzamolas = "UgyintezesiIdoUjraSzamolas";

        public const string Kititkosit = "Kititkosit";

        public const string Hash = "Hash";
    }
}
