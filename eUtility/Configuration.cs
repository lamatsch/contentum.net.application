using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Configuration;

namespace Contentum.eUtility
{
    public static class Configuration
    {

        public class PartnerAutoCompleteSection
        {
            private bool enabled = true;

            public bool Enabled
            {
                get { return enabled; }
                set { enabled = value; }
            }
            private int completionSetCount = 20;

            public int CompletionSetCount
            {
                get { return completionSetCount; }
                set { completionSetCount = value; }
            }
            private int minimumPrefixLength = 2;

            public int MinimumPrefixLength
            {
                get { return minimumPrefixLength; }
                set { minimumPrefixLength = value; }
            }

            public PartnerAutoCompleteSection(NameValueCollection config)
            {
                if (config["Enabled"] != null)
                    bool.TryParse(config["Enabled"].ToString(), out this.enabled);

                if (config["CompletionSetCount"] != null)
                    Int32.TryParse(config["CompletionSetCount"].ToString(), out this.completionSetCount);

                if (config["MinimumPrefixLength"] != null)
                    Int32.TryParse(config["MinimumPrefixLength"].ToString(), out this.minimumPrefixLength);
            }


        }

        public static class Ajax
        {


            public static void ConfigurePartnerAutoComplete(AjaxControlToolkit.AutoCompleteExtender autoCompleteExtender)
            {
                NameValueCollection config = (NameValueCollection)ConfigurationSettings.GetConfig("Ajax/PartnerAutoComplete");

                if (config != null)
                {
                    PartnerAutoCompleteSection section = new PartnerAutoCompleteSection(config);
                    if (autoCompleteExtender.Enabled)
                        autoCompleteExtender.Enabled = section.Enabled;
                    autoCompleteExtender.MinimumPrefixLength = section.MinimumPrefixLength;
                    autoCompleteExtender.CompletionSetCount = section.CompletionSetCount;
                }
            }
        }
    }
}
