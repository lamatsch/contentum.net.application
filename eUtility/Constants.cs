using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUtility
{
    public class Constants
    {
        // CR 3054: Kezelend� ORG.KOD-ok
        public static class OrgKod
        {
            public const string FPH = "FPH";
            public const string BOPMH = "BOPMH";
            public const string CSBO = "CSBO";
            public const string CSPH = "CSPH";
            public const string NMHH = "NMHH";
            public const string NMHH_TUK = "NMHH_TUK";
        }

        public const string Connection = "Connection";
        public const string Insert = "Insert";
        public const string Update = "Update";
        public const string ErrorCode = "ErrorCode";

        public const string FelhasznaloProfil = "FelhasznaloProfil";
        public const string ApplicationName = "ApplicationName";
        public const string isLogged = "isLogged";
        public const string isOrgSet = "isOrgSet";
        public const string UserName = "UserName";
        public const string Password = "Password";
        public const string PasswordConfirm = "PasswordConfirm";
        public const string OldPassword = "OldPassword";
        public const string LoginType = "LoginType";
        public const string PageNameBeforeLogin = "PageNameBeforeLogin";

        public const string FelhasznaloId = "FelhasznaloId";
        public const string LoginUserId = "LoginUserId";
        public const string LoginUserNev = "LoginUserNev";
        public const string HelyettesitettId = "HelyettesitettId";
        public const string HelyettesitettNev = "HelyettesitettNev";
        public const string FelhasznaloSzervezetId = "FelhasznaloSzervezetId";
        public const string FelhasznaloSzervezetNev = "FelhasznaloSzervezetNev";
        public const string FelhasznaloCsoportTagId = "FelhasznaloCsoportTagId";
        public const string HelyettesitesMod = "HelyettesitesMod";
        public const string HelyettesitesId = "HelyettesitesId";
        public const string IsAdminInSzervezet = "IsAdminInSzervezet";

        public const string AccessId = "AccessId";
        public const string FelhasznaloNevek = "FelhasznaloNevek";
        public const string MasterRowCount = "MasterRowCount";
        public const string DetailRowCount = "DetailRowCount";
        public const string MasterGridViewScrollable = "MasterGridViewScrollable";
        public const string DetailGridViewScrollable = "DetailGridViewScrollable";
        public const string TopRow = "TopRow";
        public const string OwnLogin = "OwnLogin";
        public const string Munkaallomas = "UserHostAddress";

        public const string LoginTranzId = "LoginTranzId";
        public const string LoginStartDate = "LoginStartDate";
        public const string PageTranzId = "PageTranzId";
        public const string PageStartDate = "PageStartDate";
        public const string PageData = "PageData";

        public const string UgyiratHierarchia = "UgyiratHierarchia";

        public static class BusinessDocument
        {
            public const string nullString = "<null>";
        }

        public static class PartnerKapcsolatTipus
        {
            public const string alarendelt = "alarendelt";
            public const string normal = "normal";
            public const string Minosito = "Minosito";
        }

        public static class IktatoErkezteto
        {
            public const string Iktato = "I";
            public const string Erkezteto = "E";
            public const string Postakonyv = "P";
            // CR3355
            public const string IratkezelesiSegedlet = "S";
        }

        public static class TableNames
        {
            public const string EREC_UgyUgyiratok = "EREC_UgyUgyiratok";
            public const string EREC_UgyUgyiratdarabok = "EREC_UgyUgyiratdarabok";
            public const string EREC_IraIratok = "EREC_IraIratok";
            public const string EREC_PldIratPeldanyok = "EREC_PldIratPeldanyok";
            public const string EREC_KuldKuldemenyek = "EREC_KuldKuldemenyek";
            public const string EREC_KuldTertivevenyek = "EREC_KuldTertivevenyek";
            public const string EREC_KuldMellekletek = "EREC_KuldMellekletek";
            public const string EREC_IratMellekletek = "EREC_IratMellekletek";
            public const string EREC_UgyiratObjKapcsolatok = "EREC_UgyiratObjKapcsolatok";
            public const string EREC_IraKezbesitesiTetelek = "EREC_IraKezbesitesiTetelek";
            public const string EREC_IrattariKikero = "EREC_IrattariKikero";
            public const string EREC_HataridosFeladatok = "EREC_HataridosFeladatok";
            public const string EREC_ObjektumTargyszavai = "EREC_ObjektumTargyszavai";
            public const string EREC_Kuldemeny_IratPeldanyai = "EREC_Kuldemeny_IratPeldanyai"; // nekrisz CR 2961       

            // eAdmin
            public const string KRT_UIMezoObjektumErtekek = "KRT_UIMezoObjektumErtekek";
            public const string KRT_Esemenyek = "KRT_Esemenyek";
            public const string KRT_Mappak = "KRT_Mappak";

            // eMigration
            public const string MIG_Foszam = "MIG_Foszam";
            public const string MIG_Alszam = "MIG_Alszam";

            // eDocument
            public const string KRT_Dokumentumok = "KRT_Dokumentumok";
        }

        public static class ColumnNames
        {
            public static class EREC_UgyUgyiratok
            {
                public const string IratMetaDefinicio_Id = "IratMetaDefinicio_Id";
            }

            public static class EREC_UgyUgyiratdarabok
            {
                public const string IratMetaDefinicio_Id = "IratMetaDefinicio_Id";
            }

            public static class EREC_IraIratok
            {
                public const string IratMetaDefinicio_Id = "IratMetaDef_Id";
                public const string Irattipus = "Irattipus";
                public const string Ugy_Fajtaja = "Ugy_Fajtaja";
            }

            public static class EREC_ObjektumTargyszavai
            {
                // azon oszlopok, melyek eredetileg nem r�szei az alapt�bl�nak, de amelyeket
                // a GetAllWithExtension t�pus� lek�rdez�s kieg�sz�t�sk�ppen belegener�l
                // az eredm�nyhalmazba (m�s, kapcsolt t�bl�b�l �tvett, vagy sz�m�tott mez�k)
                public static class WithExtension
                {
                    public const string ControlTypeSource = "ControlTypeSource";
                    public const string ControlTypeDataSource = "ControlTypeDataSource";
                }
            }

            public static class KRT_Dokumentumok
            {
                public const string Tipus = "Tipus";
            }

            public static class EREC_KuldKuldemenyek
            {
                public const string Tipus = "Tipus";
            }
        }

        public static class KezbesitesiTetel_Allapot
        {
            public const string Atadasra_kijelolt = "1";
            public const string Atadott = "2";
            public const string Atvett = "3";
        }

        public static class KezbesitesiTetel_UpdateMode
        {
            public const string Atadas = "0";
            public const string Atvetel = "1";
        }

        public static class DocumentStoreType
        {
            public const string SharePoint = "SharePoint";
            public const string FileSystem = "FileSystem";
            public const string Database = "Database";
            public const string UCM = "UCM";
            public const string FileShare = "FileShare";
        }

        public static class ErrorType
        {
            public const string Error = "0";
            public const string Warning = "1";
            public const string Default = Error;
        }

        public static class Database
        {
            public const string Yes = "1";
            public const string No = "0";
        }

        public static class FilterType
        {
            public static class Felhasznalok
            {
                public const string FelhasznalokWithoutPartner = "0";
                public const string WithEmail = "1";
            }

            public static class Partnerek
            {
                public const string Szervezet = "Szervezet";
                public const string Szemely = "Szemely";
                public const string All = "All";
                public const string BelsoSzemely = "BelsoSzemely";
                public const string SzervezetKapcsolattartoja = "SzervezetKapcsolattartoja";
            }

            public static class Ugyiratok
            {
                public const string Szereles = "Szereles";
                public const string SzerelesKikeressel = "SzerelesKikeressel";
            }

            public const string Irattar = "Irattar";
            // CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa
            public const string AllAtmenetiIrattar = "AllAtmenetiIrattar";

            public static class ObjTipusok
            {
                public const string Tabla = "DBTable";
                public const string Oszlop = "DBColumn";
                public const string All = "All";
            }

            public static class IratMetaDefinicio
            {
                public const string Ugyirat = "Ugyirat";
                public const string Ugyiratdarab = "Ugyiratdarab";
                public const string Irat = "Irat";
                public const string All = "All";
            }

            //public static class CsoportFilter
            //{
            //    // CsoportTagokLovList QueryString Filter haszn�lja:
            //    // ha Szemely: CSOPORTTAGSAG_TIPUS: vezeto, beosztott vezeto vagy dolgozo
            //    // ha Szervezet: CSOPORTTAGSAG_TIPUS: alszervezet
            //    public const string Szemely = "Szemely";
            //    public const string Szervezet = "Szervezet";
            //}

            public static class IraKezbesitesiTetelekFilter
            {
                public const string Sajat = "Sajat";
                public const string Szervezet = "Szervezet";
                public const string All = "All";
            }
        }

        public static class AutoComplete
        {
            public const string delimeter = " | ";
        }

        public static class SharePointCheckIn
        {
            public const string MinorCheckIn = "0";
            public const string MajorCheckIn = "1";
            public const string OverwriteCheckIn = "2";
        }

        public static class SearchObjectTypes
        {
            public const string EREC_IraKezbesitesiFejekSearch = "EREC_IraKezbesitesiFejekSearch";
            public const string EREC_KuldKuldemenyekSearch = "EREC_KuldKuldemenyekSearch";
            public const string EREC_PldIratPeldanyokSearch = "EREC_PldIratPeldanyokSearch";
            public const string EREC_IraIratokSearch = "EREC_IraIratokSearch";
            public const string EREC_UgyUgyiratokSearch = "EREC_UgyUgyiratokSearch";
            public const string KRT_LogSearch = "KRT_LogSearch";
            public const string EREC_IraIktatoKonyvekSearch = "EREC_IraIktatoKonyvekSearch";
        }

        public static class UgyiratTerkepNodeTypes
        {
            public const int Ugyirat = 0;
            public const int UgyiratDarab = 1;
            public const int Irat = 2;
            public const int IratPeldany = 3;
            public const int Kuldemeny = 4;
        }

        public static class DocumentFromType
        {
            public const string Email = "Email";
            public const string Iktatas = "Iktatas";
            public const string Erkeztetes = "Erkeztetes";
        }

        public static class IratMetaDefinicioTipus
        {
            public const string IratMetaDefinicio = "IratMetaDefinicio";
            public const string EljarasiSzakasz = "EljarasiSzakasz";
            public const string Irattipus = "Irattipus";
        }

        public static class SessionNames
        {
            public const string DefaultTemplatesList = "DefaultTemplatesList";
            public const string DefaultColumnsVisibilityDictionary = "DefaultColumnsVisibilityDictionary";
            public const string PageHistory = "PageHistory";
            public const string UserMenuList = "UserMenuList";
            public const string LastLoginTime = "LastLoginTime";
            public const string ErkeztetokonyvekDictionary = "ErkeztetokonyvekDictionary";
            public const string IktatokonyvekDictionary = "IktatokonyvekDictionary";
            public const string PostakonyvekDictionary = "PostakonyvekDictionary";
            public const string SelectedIds = "SelectedIds";
            // BUG_8359
            public const string DefaultOrderByDictionary = "DefaultOrderByDictionary";
        }

        public static class ErrorDetails
        {
            public static class ObjectTypes
            {
                public const string EREC_UgyUgyiratok = "EREC_UgyUgyiratok";
                public const string EREC_UgyUgyiratdarabok = "EREC_UgyUgyiratdarabok";
                public const string EREC_IraIratok = "EREC_IraIratok";
                public const string EREC_PldIratPeldanyok = "EREC_PldIratPeldanyok";
                public const string EREC_KuldKuldemenyek = "EREC_KuldKuldemenyek";
                public const string EREC_IrattariKikero = "EREC_IrattariKikero";
                public const string EREC_HataridosFeladatok = "EREC_HataridosFeladatok";
                public const string EREC_IraIktatoKonyvek = "EREC_IraIktatoKonyvek";
                public const string KRT_Mappak = "KRT_Mappak";
                public const string EREC_Szamlak = "EREC_Szamlak";
                public const string EREC_IraKezbesitesiTetelek = "EREC_IraKezbesitesiTetelek";
            }

            public static class ColumnTypes
            {
                public const string Allapot = "Allapot";
                public const string TovabbitasAlattAllapot = "TovabbitasAlattAllapot";
                public const string FelhasznaloCsoport_Id_Orzo = "FelhasznaloCsoport_Id_Orzo";
                public const string Csoport_Id_Felelos = "Csoport_Id_Felelos";
                public const string Csoport_Id_Cel = "Csoport_Id_Cel"; // k�zbes�t�si t�tel c�mzettje
                public const string Felhasznalo_Id_Atado_USER = "Felhasznalo_Id_Atado_USER"; // k�zbes�t�si t�tel �tad�ja
                public const string IktatniKell = "IktatniKell";
                public const string Sorszam = "Sorszam";
                public const string SkontrobaHelyezesJovahagyo = "SkontrobaHelyezesJovahagyo";
                public const string IrattarozasJovahagyo = "IrattarozasJovahagyo";
                public const string Felhasznalo_Id_kiado = "Felhasznalo_Id_kiado";
                public const string Csoport_Id_FelelosFelhasznalo = "Csoport_Id_FelelosFelhasznalo";
                public const string Ev = "Ev";
                public const string LezarasDatuma = "LezarasDatuma";
                public const string Zarolo_id = "Zarolo_id";
                public const string ErvKezd = "ErvKezd";
                public const string ErvVege = "ErvVege";
                // �gyirat st�tusz: �gyirat lez�r�s id�pontja, iktat�k�nyv
                public const string LezarasDat = "LezarasDat";
                public const string IraIktatokonyv_Id = "IraIktatokonyv_Id";
                public const string Letrehozo_Id = "Letrehozo_Id";
                public const string Kikero_Id = "Kikero_Id";
                // szerelt/el�k�sz�tett szerelt �gyirat sz�l�
                public const string UgyUgyirat_Id_Szulo = "UgyUgyirat_Id_Szulo";
                public const string PIRAllapot = "PIRAllapot";
                public const string Jelleg = "Jelleg";
                public const string ElintezesJovahagyo = "ElintezesJovahagyo";
                // CR3348 Iktat�k�nyv vlaszt�s ITSZ n�lk�l (ITSZ_ELORE rendszerparam�tert�l f�gg)
                // ITSZ ellen�rz�s lez�r�skor (St�tusz b�v�t�s)
                public const string IraIrattariTetel_Id = "IraIrattariTetel_Id";
                public const string SakkoraAllapot = "SakkoraAllapot";
                public const string Jovahagyo_Id = "Jovahagyo_Id";
            }
        }


        public static class MIG_IratHelye
        {
            // csak 1 karakteres lehet
            public const string Skontro = "1";
            public const string Irattarban = "2";
            public const string Osztalyon = "3";
            public const string KoztHiv = "4";
            public const string Birosag = "5";
            public const string Ugyeszseg = "6";
            public const string Vezetoseg = "7";
            public const string Kozjegyzonel = "8";
            public const string FopolgarmesteriHivatal = "9";
            public const string Sztornozott = "B";
            public const string SelejtezesreVar = "V";
            public const string Selejtezett = "S";
            public const string IrattarbolElkert = "I";
            public const string SkontrobolElkert = "E";
            public const string Lomtar = "L";
            public const string IratTarbaKuldott = "0";
            public const string Leveltar = "T";
            public const string Jegyzek = "J";
            public const string LezartJegyzek = "Z";
            public const string EgyebSzervezetnekAtadott = "A";
            public const string Szerelt = "C";
        }

        /// <summary>
        /// KRT_Folyamatok t�bl�b�l (Al��r�shoz kell)
        /// </summary>
        public static class FolyamatKodok
        {
            public const string BelsoIratIktatas = "BelsoIratIktatas";
            public const string KozvetlenAlairas = "KozvetlenAlairas";
            public const string Digitalizalas = "Digitalizalas";
            public const string BCPAlairas = "BCPAlairas";
            public const string EmailFogadas = "EmailFogadas";
            public const string KuldemenyErkeztetes = "KuldemenyErkeztetes";
            public const string FelulHitelesites = "FelulHitelesites";
            public const string IratJovahagyas = "IratJovahagyas";
            public const string KimenoIratKezbesites = "KimenoIratKezbesites";
        }

        /// <summary>
        /// KRT_DokumentumMuveletek t�bl�b�l (Al��r�shoz kell)
        /// </summary>
        public static class DokumentumMuveletKodok
        {
            public const string BelsoEmailFogadas = "BelsoEmailFogadas";
            public const string BelsoIratJovahagyas = "BelsoIratJovahagyas";
            public const string ExpedialoAlairas = "ExpedialoAlairas";
            public const string IrattervezetFeltoltes = "IrattervezetFeltoltes";
            public const string KimenoIratJovahagyas = "KimenoIratJovahagyas";
            public const string KozvetlenAlairas = "KozvetlenAlairas";
            public const string KuldemenyDigitalizalas = "KuldemenyDigitalizalas";
            public const string KuldemenyFeltoltesAdathordozorol = "KuldemenyFeltoltesAdathordozorol";
            public const string KulsoEmailFogadas = "KulsoEmailFogadas";
            public const string MunkaanyagFeltoltes = "MunkaanyagFeltoltes";
            public const string PKISzunetelesAlairas = "PKISzunetelesAlairas";
        }

        public const int HistoryLength = 5;

        // kiemelve az eRecord Utility.cs-b�l emigration miatt
        public const string UgyiratTerkep = "UgyiratTerkep";


        public const int MasterRowCountNumber = 10;

        // SelectedTab megad�s�hoz (SPS fel�l is)
        public static class Tabs
        {
            public static string HatosagiAdatokTab = "TabOnkormanyzatiAdatokPanel";
            public static string CsatolmanyokTab = "TabCsatolmanyokPanel";
            public static string MellekletekTab = "TabMellekletekPanel";
            public static string UgyiratTerkepTab = "UgyiratTerkep";
            public static string CsatolasTab = "Csatolas";
        }

        public static class Funkcio
        {
            // a munkanapl�hoz val� esem�nydefini�l�s �s a k�zi feladatfelv�tel alap�rtelmezett funkci�ja
            public const string EgyebEsemeny = "EgyebEsemeny";
            // jov�hagy�s n�lk�li v�grehajt�st (iratt�r, skontr�, k�lcs�nz�s) enged�lyez� funkci�k
            public const string UgyiratAtadasIrattarbaJovahagyasNelkul = "UgyiratAtadasIrattarbaJovahagyasNelkul";
            public const string UgyiratSkontrobaHelyezesJovahagyasNelkul = "UgyiratSkontrobaHelyezesJovahagyasNelkul";
            // j�v�hagy�st (iratt�r, skontr�, k�lcs�nz�s) enged�lyez� funkci�k
            public const string UgyiratAtadasIrattarbaJovahagyas = "IrattarbaAdasJovahagyasa";
            public const string UgyiratSkontrobaHelyezesJovahagyas = "UgyiratSkontrobaHelyezesJovahagyasa";
            public const string UgyiratKolcsonzesJovahagyas = "KolcsonzesJovahagyas";
            public const string UgyiratElintezesJovahagyas = "UgyiratElintezesJovahagyas";
            public const string UgyiratAtmenetiIrattarKikeresJovahagyas = "AtmenetiIrattarKikeresJovahagyas";
        }

        public static class IrattarJellege
        {
            public const string AtmenetiIrattar = "AtmenetiIrattar";
            public const string KozpontiIrattar = "KozpontiIrattar";
        }

        public enum WindowType
        {
            Normal = 0,
            Popup = 1
        }

        public static class EMail
        {
            public const string EredetiUzenetFileName = "EredetiOutlookUzenet.msg";
        }

        public static class OrszagViszonylatKod
        {
            public const string Europai = "K1";
            public const string EgyebKulfoldi = "K2";
        }

        //bernat.laszlo added ExcelExport Header be�ll�t�sa miatt
        public static class ExcelHeaderType
        {
            public const string Standard = "Standard";
            public const string None = "None";
            public const string AtmenetiIrattar = "AtmenetiIrattar";
        }
        //bernat.laszlo eddig
        #region CR3155 - CSV export
        public static class CSVParams
        {
            public const string EREC_KuldKuldemenyekTable = "EREC_KuldKuldemenyek";
            public const string ExportToCSV = "csv";
            public const string CSVContentType = "application/text";
            public const string CSVEncoding = "Windows-1250";
            public const string ContentDisposition = "content-disposition";
            public const string Attachment = "attachment;filename=";
            //public const string CharSet = "ISO 8859-2";
            // public static readonly System.Collections.Generic.Dictionary<string, string> CSVHeader = new System.Collections.Generic.Dictionary<string, string>() { { "content-disposition", "attachment;filename=datatable.csv" } };
            public const string sp_CSVExport_Postazas = "sp_CSVExport_Postazas";
            //CR3192 -- iratok with hat.lap
            public const string sp_CSVExport_IratokWithHatosagi = "sp_CSVExport_IratokWithHatosagi";
            public const string EREC_IraIratokTable = "EREC_IraIratok";
        }
        #endregion

        // CR3355
        public static class IktatokonyKezelesTipusa
        {
            public const string Elektronikus = "Elektronikus";
            public const string Papir = "Pap�r";
        }

        #region IRAT HATAS
        /// <summary>
        /// �irat hat�sa az �gyint�z�sre� �rt�k indul�s, meg�ll�t�s vagy v�ge, 
        /// </summary>
        public enum EnumIratHatasaUgyintezesre
        {
            START,
            PAUSE,
            STOP
        }
        #endregion

        public enum EnumUgyintezesiIdoFelfuggesztes
        {
            FANCY,
            SIMPLE
        }
        public const string FuggoKodtarOCRFileTipus = "5baa1825-0b56-46f6-9080-eceac81fb227";

        public enum EnumMetaDefinicioContentType
        {
            Onkormanyzati_hatosagi_ugy,
            Allamigazgatasi_hatosagi_ugy,
        }

        public static class eBeadvanyKuldoRendszer
        {
            public const string Adatkapu = "Adatkapu";
            public const string e_nmhh_Captcha = "e-nmhh_Captcha";
            public const string e_nmhh_VIAZ = "e-nmhh_VIAZ";
        }

        public static class PagePaths
        {
            private const string UGY_UGYIRATOK_LIST_PATH = "~/UgyUgyiratokList.aspx";
            private const string KULD_KULDEMENYEK_LIST_PATH = "~/KuldKuldemenyekList.aspx";
            private const string IRA_IRATOK_LIST_PATH = "~/IraIratokList.aspx";
            public static string IraIratokForm(string Id)
            {
                return String.Format("~/IraIratokForm.aspx?Command=View&Id={0}", Id);
            }

            public static string UgyUgyiratokForm(string Id)
            {
                return String.Format("~/UgyUgyiratokForm.aspx?Command=View&Id={0}", Id);
            }

            public static string PldIraIratPeldanyokForm(string Id)
            {
                return String.Format("~/PldIratPeldanyokForm.aspx?Command=View&Id={0}", Id);
            }

            public static string KuldKuldemenyekForm(string Id)
            {
                return String.Format("~/KuldKuldemenyekForm.aspx?Command=View&Id={0}", Id);

            }

        }

        public static class NoteSpecialValues
        {
            public const string Tomeges = "#Tomeges#";
        }


    }
}
