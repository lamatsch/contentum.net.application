﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;

namespace Contentum.eUtility
{
    public class ContentDownloader
    {
        public void GetFileByHAIR(Page page, string externalLink, string fileName, string mode)
        {
            Logger.Debug("GetFileByHAIR kezdete");
            Logger.Debug(String.Format("externalLink={0}, filename={1}", externalLink, fileName));

            #region Header bejegyzések beállítása

            var hairParameters = new HAIRCsatolmanyFeltoltesParameters(UI.SetExecParamDefault(page));
            string fullURL  = hairParameters.URL + externalLink;

            WebRequest request = WebRequest.Create(fullURL);
            request.Timeout = (int)TimeSpan.FromMinutes(20).TotalMilliseconds;
         
            string auth = "Basic " + System.Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(hairParameters.User + ":" + hairParameters.Password));
            request.PreAuthenticate = true;
            request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
            request.Headers.Add("Authorization", auth);


            WebResponse response = request.GetResponse();
            if (page.Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
            {
                fileName = EncodeFileName(fileName);
            }

            string responseFileName = fileName;
            if (mode == "decrypt")
            {
                if (fileName.EndsWith(".enc"))
                {
                    responseFileName = fileName.Substring(0, fileName.LastIndexOf('.'));
                }
            }

            // Content-Type és fájl nevének beállítása
            page.Response.ContentType = response.ContentType;
            page.Response.CacheControl = "public";
            page.Response.HeaderEncoding = System.Text.Encoding.UTF8;
            page.Response.Charset = "utf-8";
            page.Response.AddHeader("Content-Disposition", "inline;filename=\"" + responseFileName + "\";");

            #endregion Header bejegyzések beállítása

            #region Fájltartalom letöltése és továbbítása a kliensnek

            Stream s = response.GetResponseStream();

            if (mode == "decrypt")
            {
                Decrypt(page,s,fileName);
            }
            else
            {

                byte[] buf = new byte[20480];

                while (true)
                {
                    int readBytes = s.Read(buf, 0, buf.Length);

                    if (readBytes == 0)
                        break;

                    page.Response.OutputStream.Write(buf, 0, readBytes);
                }
            }

            s.Close();
            #endregion Fájltartalom letöltése és továbbítása a kliensnek
            Logger.Debug("GetFileByHAIR vege");
        }
        public System.IO.Stream GetFileByHAIRForZip(Page page,string externalLink, string filename)
        {
            WebRequest request = WebRequest.Create(externalLink);

            var hairParameters = new HAIRCsatolmanyFeltoltesParameters(UI.SetExecParamDefault(page));
            request.Credentials = new NetworkCredential(hairParameters.User, hairParameters.Password);
            externalLink = hairParameters.URL + externalLink;

            WebResponse response = request.GetResponse();

            if (page.Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
            {
                filename = EncodeFileName(filename);
            }

            return response.GetResponseStream();
        }

        public void Decrypt(Page page,Stream s, string filename)
        {
            byte[] buffer = ReadAll(s);
            byte[] decryptedData;

            string error = "";
            Stream decryptedStream = CsatolmanyokUtility.ProcessFile(filename, buffer, out error);

            if (!string.IsNullOrEmpty(error))
            {
                page.Response.ClearHeaders();
                page.Response.Write("Kititkosítás hiba." + " Hibakód: 528001");
                Logger.Warn("GetDocumentumContent decrypt hiba. Filename: " + filename + ". Hiba: " + error);
            }
            else
            {
                try
                {
                    decryptedData = ReadAll(decryptedStream);
                    page.Response.ClearContent();
                    page.Response.BinaryWrite(decryptedData);
                }
                catch (Exception ex)
                {
                   page. Response.ClearHeaders();
                    page.Response.Write("Kititkosítás hiba." + " Hibakód: 528001");
                    Logger.Warn("GetDocumentumContent decrypt hiba. Filename: " + filename + ". Hiba: " + ex.Message);
                }
                finally
                {
                    if (decryptedStream != null)
                    {
                        decryptedStream.Close();
                    }
                }
            }
        }

        public string EncodeFileName(string fileName)
        {
            if (String.IsNullOrEmpty(fileName))
                return String.Empty;

            fileName = HttpUtility.UrlEncode(fileName);
            fileName = fileName.Replace("+", "%20");

            return fileName;
        }

        public byte[] ReadAll(Stream stream)
        {
            byte[] buf = new byte[20480];

            MemoryStream ms = new MemoryStream();

            while (true)
            {
                int readBytes = stream.Read(buf, 0, buf.Length);

                if (readBytes == 0)
                    break;

                ms.Write(buf, 0, readBytes);
            }

            ms.Position = 0;

            byte[] newBuf = new byte[ms.Length];

            ms.Read(newBuf, 0, (int)ms.Length);
            return newBuf;
        }

    }
}
