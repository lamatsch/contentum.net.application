using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Contentum.eUtility
{
    public static class Crypt
    {
        private static String pwd1 = "kkkkggdtw45wsgdt45wrw345wrfdtrr434";
        private static String pwd2 = "dfhrfyrow r9395ushfsodfhq73tyagd7h";
        private static String algo = "SHA1";
        private static int iter = 2;
        private static String initvec = "wer445tgfdt443fw";
        private static int keysize = 256;

        /// <summary>
        /// Ezt haszn�ljuk k�v�lr�l egy string k�dol�s�ra.
        /// </summary>
        /// <param name="jelszo">Megdott jelszo.</param>
        /// <returns></returns>
        public static String Kodolas(String kodolnivalostr)
        {
            if (String.IsNullOrEmpty(kodolnivalostr)) return String.Empty;
            return Encrypt(kodolnivalostr, pwd1, pwd2, algo, iter, initvec, keysize);
        }

        /// <summary>
        /// Ezt haszn�ljuk k�v�lr�l egy string visszafejt�s�re.
        /// </summary>
        /// <param name="jelszo">Megdott k�dolt string.</param>
        /// <returns></returns>
        public static String Dekodolas(String kodoltstr)
        {
            if (String.IsNullOrEmpty(kodoltstr)) return String.Empty;
            return Decrypt(kodoltstr, pwd1, pwd2, algo, iter, initvec, keysize);
        }

        /// <summary>
        /// Megdott sz�veg k�dol�sa.
        /// </summary>
        /// <param name="PlainText">Sz�veg amit k�dolni akarunk.</param>
        /// <param name="Password">Jelsz�/kod�l� string.</param>
        /// <param name="Salt">Jelsz�/kod�l� string2.</param>
        /// <param name="HashAlgorithm">Algoritmus: SHA1 || MD5</param>
        /// <param name="PasswordIterations">K�dol�s h�nyszor.</param>
        /// <param name="InitialVector">16 hossz� char string.</param>
        /// <param name="KeySize">Kulcs hossz 128 || 192 || 256. </param>
        /// <returns>K�dolt sz�veg.</returns>
        private static string Encrypt(string PlainText, string Password, string Salt, string HashAlgorithm, int PasswordIterations, string InitialVector, int KeySize)
        {
            byte[] InitialVectorBytes = Encoding.ASCII.GetBytes(InitialVector);
            byte[] SaltValueBytes = Encoding.ASCII.GetBytes(Salt);
            byte[] PlainTextBytes = Encoding.UTF8.GetBytes(PlainText);
            PasswordDeriveBytes DerivedPassword = new PasswordDeriveBytes(Password, SaltValueBytes, HashAlgorithm, PasswordIterations);
            byte[] KeyBytes = DerivedPassword.GetBytes(KeySize / 8);
            RijndaelManaged SymmetricKey = new RijndaelManaged();
            SymmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform Encryptor = SymmetricKey.CreateEncryptor(KeyBytes, InitialVectorBytes);
            MemoryStream MemStream = new MemoryStream();
            CryptoStream CryptoStream = new CryptoStream(MemStream, Encryptor, CryptoStreamMode.Write);
            CryptoStream.Write(PlainTextBytes, 0, PlainTextBytes.Length);
            CryptoStream.FlushFinalBlock();
            byte[] CipherTextBytes = MemStream.ToArray();
            MemStream.Close();
            CryptoStream.Close();
            return System.Convert.ToBase64String(CipherTextBytes);
        }

        /// <summary>
        /// Megdott sz�veg dek�dol�sa.
        /// </summary>
        /// <param name="CipherText">K�dolt sz�veg.</param>
        /// <param name="Password">Jelsz�/kod�l� string.</param>
        /// <param name="Salt">Jelsz�/kod�l� string2.</param>
        /// <param name="HashAlgorithm">Algoritmus: SHA1 || MD5</param>
        /// <param name="PasswordIterations">K�dol�s h�nyszor.</param>
        /// <param name="InitialVector">16 hossz� char string.</param>
        /// <param name="KeySize">Kulcs hossz 128 || 192 || 256. </param>
        /// <returns>K�dolatlan sz�veg.</returns>
        private static string Decrypt(string CipherText, string Password, string Salt, string HashAlgorithm, int PasswordIterations, string InitialVector, int KeySize)
        {
            byte[] InitialVectorBytes = Encoding.ASCII.GetBytes(InitialVector);
            byte[] SaltValueBytes = Encoding.ASCII.GetBytes(Salt);
            byte[] CipherTextBytes = System.Convert.FromBase64String(CipherText);
            PasswordDeriveBytes DerivedPassword = new PasswordDeriveBytes(Password, SaltValueBytes, HashAlgorithm, PasswordIterations);
            byte[] KeyBytes = DerivedPassword.GetBytes(KeySize / 8);
            RijndaelManaged SymmetricKey = new RijndaelManaged();
            SymmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform Decryptor = SymmetricKey.CreateDecryptor(KeyBytes, InitialVectorBytes);
            MemoryStream MemStream = new MemoryStream(CipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(MemStream, Decryptor, CryptoStreamMode.Read);
            byte[] PlainTextBytes = new byte[CipherTextBytes.Length];
            int ByteCount = cryptoStream.Read(PlainTextBytes, 0, PlainTextBytes.Length);
            MemStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(PlainTextBytes, 0, ByteCount);
        }

        public static string GetSha1Hash(string text)
        {
            if (String.IsNullOrEmpty(text))
                return String.Empty;

            byte[] buffer = Encoding.UTF8.GetBytes(text);
            SHA1CryptoServiceProvider cryptoTransformSHA1 =
            new SHA1CryptoServiceProvider();
            string hash = BitConverter.ToString(
                cryptoTransformSHA1.ComputeHash(buffer)).Replace("-", "");

            return hash;
        }

        public static byte[] SignPKCS7(byte[] data, X509Certificate2 certificate)
        {
            if (data == null)
                throw new ArgumentNullException("data");
            if (certificate == null)
                throw new ArgumentNullException("certificate");

            // setup the data to sign
            ContentInfo content = new ContentInfo(data);
            SignedCms signedCms = new SignedCms(content, false);
            CmsSigner signer = new CmsSigner(SubjectIdentifierType.IssuerAndSerialNumber, certificate);

            // create the signature
            signedCms.ComputeSignature(signer);
            return signedCms.Encode();
        }
    } //class
}
