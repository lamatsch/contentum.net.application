﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Contentum.eUtility
{
    /// <summary>
    /// Summary description for GPGEncryptFileManager
    /// </summary>
    public class GPGEncryptFileManager
    {
        public const string ENCRYPT_SAJAT_PUBLIKUS_KULCS_PATH = "ENCRYPT_SAJAT_PUBLIKUS_KULCS_PATH";
        static string batchFilePath;
        ExecParam execParam;
        DirectoryInfo workingDirectory;

        static GPGEncryptFileManager()
        {
            batchFilePath = ConfigurationManager.AppSettings.Get("GPG_Encrypt_FilePath");
        }

        public GPGEncryptFileManager(ExecParam execParam)
        {
            this.execParam = execParam;
        }

        void Encrypt(string publicKeyFilePath1, string publiucKeyFilePath2, string filePath, string outputFilePath, out string error)
        {
            Logger.Info(String.Format("GPGEncryptFileManager.Encrypt indul: {0}", filePath));
            error = null;

            if (!File.Exists(batchFilePath))
            {
                error = String.Format("GPGEncryptFileManager.Encrypt hiba, batch file nem található: {0}", batchFilePath);
                Logger.Error(error);
                return;
            }

            try
            {
                string args = String.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\"", publicKeyFilePath1, publiucKeyFilePath2, filePath, outputFilePath);

                Logger.Debug(String.Format("Process: {0} {1}", batchFilePath, args));

                ProcessStartInfo process = new ProcessStartInfo
                {
                    CreateNoWindow = false,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    FileName = batchFilePath,
                    Arguments = args
                };


                Process p = new Process();
                p.StartInfo = process;

                StringBuilder errorBuilder = new StringBuilder();
                p.ErrorDataReceived += delegate (object sender, DataReceivedEventArgs e)
                {
                    errorBuilder.Append(e.Data);
                };

                //call this before process start
                p.StartInfo.RedirectStandardError = true;

                p.Start();

                //call this after process start
                p.BeginErrorReadLine();

                string output = p.StandardOutput.ReadToEnd();
                Logger.Debug(String.Format("Process output: {0}", output));

                p.WaitForExit();
                int result = p.ExitCode;

                Logger.Debug(String.Format("ExitCode: {0}", result));
                Logger.Debug(String.Format("errorBuilder: {0}", errorBuilder));

                if (result == 0)
                {
                    Logger.Info(String.Format("GPGEncryptFileManager.Encrypt vege"));
                }
                else
                {
                    error = String.Format("GPGEncryptFileManager.Encrypt hiba, batch exit code: {0}, error: {1}", result, errorBuilder);
                    Logger.Error(error);
                }
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                Logger.Error("GPGEncryptFileManager.Kititkosit hiba", ex);
            }
        }

        static string GetWorkingDirectoryPath()
        {
            return Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
        }

        void CreateWorkingDirectory()
        {
            workingDirectory = CreateDirectory(GetWorkingDirectoryPath());
        }

        static DirectoryInfo CreateDirectory(string directoryPath)
        {
            Logger.Debug(String.Format("CreateDirectory: {0}", directoryPath));
            return Directory.CreateDirectory(directoryPath);
        }

        void DeleteWorkingDirectory()
        {
            if (workingDirectory != null)
            {
                try
                {
                    Directory.Delete(workingDirectory.FullName, true);
                }
                catch (Exception ex)
                {
                    Logger.Error("DeleteWorkingDirectory hiba", ex);
                }
            }
        }

        public byte[] EncryptFile(string fileName, byte[] fileContent, string publicKey, out string error)
        {
            Logger.Info(String.Format("GPGEncryptFileManager.EncryptFile indul: {0}", fileName));
            error = null;
            byte[] encryptedFileContent = null;

            try
            {
                CreateWorkingDirectory();

                string tempFilePath = Path.Combine(workingDirectory.FullName, fileName);

                Logger.Debug(String.Format("File mentese indul: {0}", tempFilePath));

                File.WriteAllBytes(tempFilePath, fileContent);

                string publicKeyFilePath1 = Path.Combine(workingDirectory.FullName, "key_pub.asc");

                Logger.Debug(String.Format("File mentese indul: {0}", publicKeyFilePath1));

                File.WriteAllText(publicKeyFilePath1, publicKey, new UTF8Encoding(false));

                string publicKeyFilePath2 = Rendszerparameterek.Get(execParam, ENCRYPT_SAJAT_PUBLIKUS_KULCS_PATH);

                Logger.Debug(String.Format("publicKeyFilePath2: {0}", publicKeyFilePath2));

                if (!String.IsNullOrEmpty(publicKeyFilePath2) && !File.Exists(publicKeyFilePath2))
                {
                    Logger.Warn(String.Format("A publikus kulcsot tartalmazó fájl nem található: {0}", publicKeyFilePath2));
                    publicKeyFilePath2 = String.Empty;
                }

                string encryptedFilePath = Path.Combine(workingDirectory.FullName, String.Format("{0}.enc", fileName));

                Logger.Debug(String.Format("encryptedFilePath: {0}", encryptedFilePath));

                Encrypt(publicKeyFilePath1, publicKeyFilePath2, tempFilePath, encryptedFilePath, out error);

                if (error == null)
                {
                    encryptedFileContent = File.ReadAllBytes(encryptedFilePath);
                }
                else
                {
                    Logger.Error(String.Format("GPGEncryptFileManager.EncryptFile hiba: {0}", error));
                }
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                Logger.Error("GPGEncryptFileManager.EncryptFile hiba", ex);
            }

            DeleteWorkingDirectory();

            return encryptedFileContent;
        }
    }
}