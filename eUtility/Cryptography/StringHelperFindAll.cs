﻿using System.Collections.Generic;

namespace Contentum.eUtility
{
    public static class StringHelper
    {
        public static IEnumerable<int> FindAll(this string str, string toFind)
        {
            var index = 0;
            while (true)
            {
                index = str.IndexOf(toFind, index + 1);
                if (index == -1)
                    break;
                yield return index;
            }
        }
    }
}
