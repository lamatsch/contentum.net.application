﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;

namespace Contentum.eUtility.eCryptography
{
    /// <summary>
    /// Xml signature by detached form
    /// </summary>
    public class XmlSignatureDetached
    {
        #region ATTRIBS
        private string CertificatePrivateKeyFilePath { get; set; }
        private string CertificateFilePath { get; set; }
        private string CertificateFilePassword { get; set; }
        private X509Certificate2 Certificate { get; set; }
        /*private RSACryptoServiceProvider RSAKeyProvider { get; set; }*/

        private enum PemStringType
        {
            Certificate,
            RsaPrivateKey
        }
        #endregion

        /// <summary>
        /// XmlSignatureDetached
        /// </summary>
        /// <param name="certificatePrivateKeyFilePath"></param>
        /// <param name="certificateFFilePassword"></param>
        /// <param name="certificateFilePath"></param>
        public XmlSignatureDetached(string certificatePrivateKeyFilePath, string certificateFFilePassword)
        {
            Logger.Debug("XmlSignatureDetached.Init.Start:");
            CertificatePrivateKeyFilePath = certificatePrivateKeyFilePath;
            CertificateFilePassword = certificateFFilePassword;

            CheckCertificateFile();

            LoadCertificateFromPFXFile();

            if (!HasPrivateKey())
                throw new NotImplementedException("Certificate key error !");
            Logger.Debug("XmlSignatureDetached.Init.Stop:");
        }
        /// <summary>
        /// XmlSignatureDetached
        /// </summary>
        /// <param name="certificatePrivateKeyFilePath"></param>
        /// <param name="certificateFFilePassword"></param>
        /// <param name="certificateFilePath"></param>
        public XmlSignatureDetached(string certificatePrivateKeyFilePath, string certificateFFilePassword, string certificateFilePath)
        {
            CertificatePrivateKeyFilePath = certificatePrivateKeyFilePath;
            CertificateFilePath = certificateFilePath;
            CertificateFilePassword = certificateFFilePassword;

            CheckCertificateFile();

            FileInfo fi = GetFileInfo();
            if (fi.Extension.Equals(".pfx", StringComparison.InvariantCultureIgnoreCase))
            {
                LoadCertificateFromPFXFile();
            }
            else if (fi.Extension.Equals(".pem", StringComparison.InvariantCultureIgnoreCase)
                  || fi.Extension.Equals(".cer", StringComparison.InvariantCultureIgnoreCase))
            {
                /*Not works properly: "Bad length error" */
                LoadCertificateFromCERFile();
            }
            else
            {
                throw new NotImplementedException("Not supported certificate file format !");
            }

            if (!HasPrivateKey())
                throw new NotImplementedException("Certificate key error !");
        }

        #region SIGN
        /// <summary>
        /// Sign
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="key"></param>
        /// <param name="outSignedValue"></param>
        /// <returns></returns>
        public string Sign(string xml, out string outSignedValue)
        {
            Logger.Debug("XmlSignatureDetached.Sign.Start:");
            outSignedValue = null;

            XmlDocument xmlDoc;
            XmlDocument doc;
            try
            {
                // Create a new XML document.
                xmlDoc = new XmlDocument();

                // Load an XML file into the XmlDocument object.
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.LoadXml(xml);

                // Sign the XML document. 
                doc = SignDetachedXml(xmlDoc, out outSignedValue);
                Logger.Debug("XmlSignatureDetached.Sign.Stop:");
                return GetXmlDocumentAsString(doc);
            }
            catch (Exception e)
            {
                Logger.Error("XmlDetachedSign.Sign.Error:", e);
                Logger.Debug("XmlSignatureDetached.Sign.Stop:");
                return null;
            }
        }

        // Verify the signature of an XML file and return the result.
        public bool VerifyDetachedSignature(XmlDocument xmlDocument)
        {
            // Create a new SignedXMl object.
            SignedXml signedXml = new SignedXml();

            // Find the "Signature" node and create a new
            // XmlNodeList object.
            XmlNodeList nodeList = xmlDocument.GetElementsByTagName("Signature");

            // Load the signature node.
            signedXml.LoadXml((XmlElement)nodeList[0]);

            // Check the signature and return the result.
            return signedXml.CheckSignature();
        }

        /// <summary>
        /// Sign an XML file. 
        /// This document cannot be verified unless the verifying
        /// code has the key with which it was signed.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="key"></param>
        /// <param name="outSignedValue"></param>
        /// <returns></returns>
        private XmlDocument SignDetachedXml(XmlDocument xmlDoc, out string outSignedValue)
        {
            if (xmlDoc == null)
                throw new ArgumentException("xmlDoc");
            outSignedValue = null;

            SignedXml signedXml = new SignedXml(xmlDoc);

            AsymmetricAlgorithm asymmetricAlgorithm = Certificate.PrivateKey;
            signedXml.SigningKey = asymmetricAlgorithm;

            // Create a reference to be signed.
            Reference reference = new Reference();
            reference.Uri = "";

            // Add an enveloped transformation to the reference.
            XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
            reference.AddTransform(env);

            // Add the reference to the SignedXml object.
            signedXml.AddReference(reference);

            // Compute the signature.
            signedXml.ComputeSignature();

            // Get the XML representation of the signature and save
            // it to an XmlElement object.
            XmlElement xmlDigitalSignature = signedXml.GetXml();

            // Append the element to the XML document.
            xmlDoc.DocumentElement.AppendChild(xmlDoc.ImportNode(xmlDigitalSignature, true));

            #region SIGNATURE VALUE
            /*V2*/
            outSignedValue = GetSignatureValueFromXml(xmlDigitalSignature);
            /*V1*/
            //outSignedValue = xmlDigitalSignature.InnerXml;
            #endregion

            return xmlDoc;
        }
        #endregion

        #region CERTIFICATE
        /// <summary>
        /// HasPrivateKey
        /// </summary>
        /// <returns></returns>
        public bool HasPrivateKey()
        {
            if (Certificate == null)
                return false;
            return Certificate.HasPrivateKey;
        }
        /// <summary>
        /// LoadCertificateFromFile
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private void LoadCertificateFromPFXFile()
        {
            Logger.Debug("XmlSignatureDetached.LoadCertificateFromPFXFile.Start: ");

            #region V1
            byte[] bytes;
            try
            {
                Logger.Debug("XmlSignatureDetached.LoadCertificateFromPFXFile.ReadFile: " + CertificatePrivateKeyFilePath);
                bytes = File.ReadAllBytes(CertificatePrivateKeyFilePath);
            }
            catch (Exception exc)
            {
                Logger.Debug(string.Format("XmlSignatureDetached.LoadCertificateFromPFXFile.ReadFile.Error: File:{0} Error:{1} ", CertificatePrivateKeyFilePath, exc.Message));
                throw;
            }

            try
            {
                Certificate = new X509Certificate2();
                if (string.IsNullOrEmpty(CertificateFilePassword))
                {
                    Certificate.Import(bytes);
                }
                else
                    Certificate.Import(bytes, CertificateFilePassword, X509KeyStorageFlags.Exportable);
            }
            catch (Exception exc)
            {
                Logger.Debug(string.Format("XmlSignatureDetached.LoadCertificateFromPFXFile.ReadX509Certificate.Error: File:{0} Error:{1} ", CertificatePrivateKeyFilePath, exc.Message));
                throw;
            }
            #endregion

            Logger.Debug("XmlSignatureDetached.LoadCertificateFromPFXFile.Stop:");
        }
        /// <summary>
        /// LoadCertificateFromCERFile
        /// </summary>
        private void LoadCertificateFromCERFile()
        {
            Logger.Debug("XmlSignatureDetached.LoadCertificateFromCERFile.Start:");
            string privateKey = File.ReadAllText(CertificatePrivateKeyFilePath);
            string publicCert = File.ReadAllText(CertificateFilePath);

            byte[] certBuffer = GetBytesFromPEM(publicCert, PemStringType.Certificate);
            byte[] keyBuffer = GetBytesFromPEM(privateKey, PemStringType.RsaPrivateKey);

            if (string.IsNullOrEmpty(CertificateFilePassword))
                Certificate = new X509Certificate2(certBuffer);
            else
                Certificate = new X509Certificate2(certBuffer, CertificateFilePassword);

            RSACryptoServiceProvider prov = DecodeRSAPrivateKey(keyBuffer);
            Certificate.PrivateKey = prov;
            Logger.Debug("XmlSignatureDetached.LoadCertificateFromCERFile.Stop:");
        }

        private byte[] GetBytesFromPEM(string pemString, PemStringType type)
        {
            string header = string.Empty;
            string footer = string.Empty;
            StringBuilder sb = new StringBuilder(pemString);
            sb = sb.Replace("\r\n", string.Empty);
            switch (type)
            {
                case PemStringType.Certificate:
                    header = "-----BEGIN CERTIFICATE-----";
                    footer = "-----END CERTIFICATE-----";
                    break;
                case PemStringType.RsaPrivateKey:
                    if (sb.ToString().Contains("BEGIN RSA PRIVATE KEY"))
                    {
                        header = "-----BEGIN RSA PRIVATE KEY-----";
                        footer = "-----END RSA PRIVATE KEY-----";
                    }
                    else if (sb.ToString().Contains("BEGIN PRIVATE KEY"))
                    {
                        header = "-----BEGIN PRIVATE KEY-----";
                        footer = "-----END PRIVATE KEY-----";
                    }
                    break;
                default:
                    return null;
            }

            int start = sb.ToString().IndexOf(header) + header.Length;
            int end = sb.ToString().IndexOf(footer, start) - start;
            return System.Convert.FromBase64String(sb.ToString().Substring(start, end));
        }

        private RSACryptoServiceProvider DecodeRSAPrivateKey(byte[] privkey)
        {
            byte[] MODULUS, E, D, P, Q, DP, DQ, IQ;

            // ---------  Set up stream to decode the asn.1 encoded RSA private key  ------
            System.IO.MemoryStream mem = new System.IO.MemoryStream(privkey);
            System.IO.BinaryReader binr = new System.IO.BinaryReader(mem); //wrap Memory Stream with BinaryReader for easy reading
            byte bt = 0;
            ushort twobytes = 0;
            int elems = 0;
            try
            {
                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130) //data read as little endian order (actual data order for Sequence is 30 81)
                    binr.ReadByte(); //advance 1 byte
                else if (twobytes == 0x8230)
                    binr.ReadInt16(); //advance 2 bytes
                else
                    return null;

                twobytes = binr.ReadUInt16();
                if (twobytes != 0x0102) //version number
                    return null;
                bt = binr.ReadByte();
                if (bt != 0x00)
                    return null;

                //------  all private key components are Integer sequences ----
                elems = GetIntegerSize(binr);
                MODULUS = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                E = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                D = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                P = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                Q = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                DP = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                DQ = binr.ReadBytes(elems);

                elems = GetIntegerSize(binr);
                IQ = binr.ReadBytes(elems);

                //  System.Security.Cryptography.RSA.Create();

                // ------- create RSACryptoServiceProvider instance and initialize with public key -----
                System.Security.Cryptography.RSACryptoServiceProvider RSA = new System.Security.Cryptography.RSACryptoServiceProvider();
                System.Security.Cryptography.RSAParameters RSAparams = new System.Security.Cryptography.RSAParameters();
                RSAparams.Modulus = MODULUS;
                RSAparams.Exponent = E;
                RSAparams.D = D;
                RSAparams.P = P;
                RSAparams.Q = Q;
                RSAparams.DP = DP;
                RSAparams.DQ = DQ;
                RSAparams.InverseQ = IQ;
                RSA.ImportParameters(RSAparams);
                return RSA;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                binr.Close();
            }
        }
        private int GetIntegerSize(BinaryReader binr)
        {
            byte bt = 0;
            byte lowbyte = 0x00;
            byte highbyte = 0x00;
            int count = 0;
            bt = binr.ReadByte();
            if (bt != 0x02) //expect integer
                return 0;
            bt = binr.ReadByte();

            if (bt == 0x81)
                count = binr.ReadByte(); // data size in next byte
            else if (bt == 0x82)
            {
                highbyte = binr.ReadByte(); // data size in next 2 bytes
                lowbyte = binr.ReadByte();
                byte[] modint = { lowbyte, highbyte, 0x00, 0x00 };
                count = BitConverter.ToInt32(modint, 0);
            }
            else
            {
                count = bt; // we already have the data size
            }

            while (binr.ReadByte() == 0x00)
            {
                //remove high order zeros in data
                count -= 1;
            }
            binr.BaseStream.Seek(-1, System.IO.SeekOrigin.Current); //last ReadByte wasn't a removed zero, so back up a byte
            return count;
        }
        private bool CompareBytearrays(byte[] a, byte[] b)
        {
            if (a.Length != b.Length)
                return false;
            int i = 0;
            foreach (byte c in a)
            {
                if (c != b[i])
                    return false;
                i++;
            }
            return true;
        }
        #endregion

        #region XML OPERATIONS
        /// <summary>
        /// GetXmlDocumentAsString
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        private string GetXmlDocumentAsString(XmlDocument xmlDoc)
        {
            using (var stringWriter = new StringWriter())
            using (var xmlTextWriter = XmlWriter.Create(stringWriter))
            {
                xmlDoc.WriteTo(xmlTextWriter);
                xmlTextWriter.Flush();
                return stringWriter.GetStringBuilder().ToString();
            }
        }
        /// <summary>
        /// GetSignatureValueFromXml
        /// </summary>
        /// <param name="xmlDigitalSignature"></param>
        /// <returns></returns>
        private string GetSignatureValueFromXml(XmlElement xmlDigitalSignature)
        {
            XmlNodeList nodes = xmlDigitalSignature.GetElementsByTagName("SignatureValue");
            if (nodes.Count < 1)
                throw new Exception("Xml error: SignatureValue");
            XmlNode node = nodes[0];
            return node.InnerXml;
        }
        #endregion

        #region FILE OPERATIONS
        internal byte[] ReadFile(string fileName)
        {
            FileStream f = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            int size = (int)f.Length;
            byte[] data = new byte[size];
            size = f.Read(data, 0, size);
            f.Close();
            return data;
        }
        #endregion

        #region COMMON
        /// <summary>
        /// CheckCertificateFile
        /// </summary>
        private void CheckCertificateFile()
        {
            if (!File.Exists(CertificatePrivateKeyFilePath))
                throw new FileNotFoundException("Certificate file not found !");
        }

        /// <summary>
        /// GetFileInfo
        /// </summary>
        private FileInfo GetFileInfo()
        {
            return new FileInfo(CertificatePrivateKeyFilePath);
        }

        /// <summary>
        /// GetByteAsString
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private string GetByteAsString(byte[] data)
        {
            return System.Text.Encoding.UTF8.GetString(data);
        }
        #endregion
    }
}