using System;
using System.Collections.Generic;
using System.Text;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System.Data;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Web.UI;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Web;

namespace Contentum.eUtility
{
    public class CsoportNevek_Cache
    {
        // Csoportnevek lecache-elve az al�bbi form�ban:
        // Dictionary<String csoport_Id,String csoport_Nev>

        private const String cache_key_csoportNevek = "CsoportNevekDictionary";

        private static readonly object _sync = new object();

        // h�ny percenk�nt �r�tse a cache-b�l a Dictionary-t?
        //private const int CsoportNevekCache_RefreshPeriod_Minute = 30;
        // els� felt�lt�skor max h�ny sort hozzunk az adatb�zisb�l?
        //private const int CsoportNevekCache_MaxRowsFromDBToCache = 2000;



        /// <summary>
        /// Csoport nev�nek lek�r�se Id alapj�n a Cache-b�l
        /// (FIGYELEM: ha nem tal�ljuk, itt nem k�rj�k le az adatb�zisb�l)
        /// </summary>
        /// <param name="Csoport_Id"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        public static String GetCsoportNevFromCache(String Csoport_Id, System.Web.Caching.Cache cache)
        {
            OrgSeperatedDictionary<String, String> CsoportNevekDictionaryCache =
                (OrgSeperatedDictionary<String, String>)cache[cache_key_csoportNevek];

            if (CsoportNevekDictionaryCache == null)
            {
                return "";
            }

            if (cache[cache_key_csoportNevek] != null)
            {
                if (CsoportNevekDictionaryCache[Csoport_Id] != null)
                {
                    return CsoportNevekDictionaryCache[Csoport_Id];
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        public static String GetCsoportNevFromCache(String Csoport_Id, System.Web.UI.Page page)
        {
            return GetCsoportNevFromCache(Csoport_Id, UI.SetExecParamDefault(page), page.Cache);
        }

        /// <summary>
        /// Csoport nev�nek lek�r�se Id alapj�n a Cache-b�l
        /// (ha nem tal�ljuk, lek�rj�k az adatb�zisb�l)
        /// </summary>
        /// <param name="Csoport_Id"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        public static String GetCsoportNevFromCache(String Csoport_Id, ExecParam execParam, System.Web.Caching.Cache cache)
        {
            Logger.Debug("GetCsoportNevFromCache kezdete");

            if (String.IsNullOrEmpty(Csoport_Id)) { Logger.Debug("String.IsNullOrEmpty(Csoport_Id)"); return String.Empty; }

            if (String.IsNullOrEmpty(execParam.Org_Id))
            {
                Logger.Debug("String.IsNullOrEmpty(execParam.Org_Id)");
                string orgId = FelhasznaloNevek_Cache.GetOrgByFelhasznalo(execParam.Felhasznalo_Id, cache);
                Logger.Debug("execParam.Org_Id=" + orgId ?? "NULL");
                execParam.Org_Id = orgId;
            }

            OrgSeperatedDictionary<String, String> CsoportNevekDictionaryCache = null;

            if (cache[cache_key_csoportNevek] == null)
            {
                int CsoportNevekCache_RefreshPeriod_Minute =
                            Rendszerparameterek.GetInt(execParam, Rendszerparameterek.CsoportNevekCache_RefreshPeriod_Minute);
                if (CsoportNevekCache_RefreshPeriod_Minute == 0)
                {
                    CsoportNevekCache_RefreshPeriod_Minute = 600;
                }
                lock (_sync)
                {
                    if (cache[cache_key_csoportNevek] == null)
                    {
                        CsoportNevekDictionaryCache = new OrgSeperatedDictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

                        // Dictionary felv�tele a Cache-be:
                        cache.Insert(cache_key_csoportNevek, CsoportNevekDictionaryCache, null
                            , DateTime.Now.AddMinutes(CsoportNevekCache_RefreshPeriod_Minute), System.Web.Caching.Cache.NoSlidingExpiration
                            , System.Web.Caching.CacheItemPriority.NotRemovable, null);
                    }
                }
            }


            CsoportNevekDictionaryCache = (OrgSeperatedDictionary<String, String>)cache[cache_key_csoportNevek];

            Dictionary<string, string> CsoportNevekDictionary = CsoportNevekDictionaryCache.Get(execParam.Org_Id);

            if (CsoportNevekDictionary.Count == 0)
            {
                lock (_sync)
                {
                    if (CsoportNevekDictionary.Count == 0)
                    {
                        // Csoportnevek lek�r�se az adatb�zisb�l (Csoportok GETALL)

                        KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();

                        KRT_CsoportokSearch search = new KRT_CsoportokSearch();

                        int CsoportNevekCache_MaxRowsFromDBToCache =
                            Rendszerparameterek.GetInt(execParam, Rendszerparameterek.CsoportNevekCache_MaxRowsFromDBToCache);
                        // ha valami�rt nem kapjuk meg, default �rt�k:
                        if (CsoportNevekCache_MaxRowsFromDBToCache == 0)
                        {
                            CsoportNevekCache_MaxRowsFromDBToCache = 10000;
                        }


                        search.TopRow = CsoportNevekCache_MaxRowsFromDBToCache;

                        Result result = service.GetAll(execParam, search);
                        if (result.IsError)
                        {
                            // hiba:
                            Logger.Error("GetCsoportNevFromCache hiba: KRT_CsoportokService.GetAll", execParam, result);
                            return null;
                        }
                        else
                        {
                            // Dictionary felt�lt�se:
                            try
                            {
                                foreach (DataRow row in result.Ds.Tables[0].Rows)
                                {
                                    String Id = row["Id"].ToString();
                                    String Nev = row["Nev"].ToString();

                                    if (!CsoportNevekDictionary.ContainsKey(Id))
                                    {
                                        CsoportNevekDictionary.Add(Id, Nev);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                // hiba:
                                Logger.Error("GetCsoportNevFromCache: Dictionary feltoltese hiba", ex);
                                return null;
                            }

                        }
                    }
                }
            }

            if (CsoportNevekDictionary.ContainsKey(Csoport_Id))
            {
                return CsoportNevekDictionary[Csoport_Id];
            }
            else
            {
                //// Nincs bent a Dictionary-ben, le kell k�rni az adatb�zisb�l: (Csoport GET)
                KRT_Csoportok csoport = GetCsoportFromDB(Csoport_Id, execParam);
                try
                {
                    if (!CsoportNevekDictionary.ContainsKey(csoport.Id))
                    {
                        lock (_sync)
                        if (!CsoportNevekDictionary.ContainsKey(csoport.Id))
                        {
                            CsoportNevekDictionary.Add(csoport.Id, csoport.Nev);
                        }
                    }
                }
                catch (ArgumentException aex)
                {
                    // m�r benne van a Dictionary-ben (esetleg valaki k�zben betette)
                    Logger.Error("CsoportNevekDictionary.Add hiba", aex);
                }

                return csoport.Nev;
            }
        }

        public static KRT_Csoportok GetCsoportFromDB(String Csoport_Id, ExecParam execParam)
        {
            KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            execParam.Record_Id = Csoport_Id;

            Result result = service.Get(execParam);
            if (!String.IsNullOrEmpty(result.ErrorCode) || result.Record == null)
            {
                // hiba:
                Logger.Error(String.Format("GetCsoportNevFromCache: KRT_CsoportokService.Get hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
                return null;
            }
            else
            {
                KRT_Csoportok csoport = (KRT_Csoportok)result.Record;
                return csoport;
            }
        }
    }

    public class Csoportok
    {

        /// <summary>
        /// Szervezet (Csoport) �sszes al�rendelt csoportj�nak lek�r�se �s az Id-k visszaad�sa string t�mbk�nt.
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="Csoport_Id"></param>
        /// <param name="kozvetlenulAlarendelt"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public static string[] GetAllSubCsoport(ExecParam execParam, String Csoport_Id, bool kozvetlenulAlarendelt, KRT_CsoportokSearch search)
        {
            if (execParam == null || String.IsNullOrEmpty(Csoport_Id))
            {
                return null;
            }

            if (search == null)
            {
                search = new KRT_CsoportokSearch();
            }

            KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            Result result = service.GetAllSubCsoport(execParam, Csoport_Id, kozvetlenulAlarendelt, search);

            if (!String.IsNullOrEmpty(result.ErrorCode) || result.Ds.Tables[0].Rows.Count == 0)
            {
                return null;
            }

            string[] csoportokArray = new string[result.Ds.Tables[0].Rows.Count];
            for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
            {
                csoportokArray[i] = result.Ds.Tables[0].Rows[i]["Id"].ToString();
            }
            return csoportokArray;
        }

        /// <summary>
        /// A Felhaszn�l� tagja-e a megadott csoportnak
        /// </summary>
        /// <param name="Felhasznalo_Id"></param>
        /// <param name="Csoport_Id"></param>
        /// <returns></returns>
        public static bool IsMember(String Felhasznalo_Id, String Csoport_Id)
        {
            if (String.IsNullOrEmpty(Felhasznalo_Id) || String.IsNullOrEmpty(Csoport_Id))
            {
                return false;
            }

            if (Felhasznalo_Id == Csoport_Id)
            {
                return true;
            }
            else
            {
                KRT_CsoportTagokService service = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                Result result = service.IsMember(new ExecParam(), Csoport_Id, Felhasznalo_Id);
                if (!string.IsNullOrEmpty(result.ErrorCode) || !bool.Parse(result.Record.ToString()))
                    return false;
                else
                    return true;
            }

        }

        /// <summary>
        /// A Felhaszn�l� vagy valamely szervezete tagja-e a megadott csoportnak
        /// </summary>
        /// <param name="Felhasznalo_Id"></param>
        /// <param name="Csoport_Id"></param>
        /// <returns></returns>
        public static bool IsMember(String Felhasznalo_Id, List<string> FelhasznaloFelettesSzervezetList, String Csoport_Id)
        {
            /// TODO: nincs m�g meg, egyel�re csak �sszehasonl�tja a k�t Id-t (az egyszem�lyes csoportokra ez ok)

            if (FelhasznaloFelettesSzervezetList == null || FelhasznaloFelettesSzervezetList.Count == 0 ||
                String.IsNullOrEmpty(Csoport_Id) || String.IsNullOrEmpty(Felhasznalo_Id))
            {
                return false;
            }

            if (Felhasznalo_Id == Csoport_Id)
            {
                return true;
            }

            if (FelhasznaloFelettesSzervezetList.Contains(Csoport_Id))
            {
                return true;
            }


            return false;

        }

        /// <summary>
        /// A Felhaszn�l�hoz tartot� �sszes felettes szervezet
        /// Figyelem exception-t dobhat, a h�v� oldalon figyelni kell!!!
        /// </summary>
        /// <param name="Felhasznalo_Id"></param>
        /// <param name="Csoport_Id"></param>
        /// <returns></returns>
        public static List<string> GetFelhasznaloFelettesCsoportjai(Page page, String Felhasznalo_Id)
        {
            /// TODO: nincs m�g meg, egyel�re csak �sszehasonl�tja a k�t Id-t (az egyszem�lyes csoportokra ez ok)
            if (String.IsNullOrEmpty(Felhasznalo_Id))
            {
                return null;
            }

            ExecParam xpm = UI.SetExecParamDefault(page, new ExecParam());
            KRT_CsoportTagokService svc = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch sch = new KRT_CsoportTagokSearch();

            sch.Csoport_Id_Jogalany.Value = Felhasznalo_Id;
            sch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            Result res = svc.GetAll(xpm, sch);


            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                throw new ResultException(res);
            }

            List<string> Csoportok = new List<string>();

            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                Csoportok.Add(row["Csoport_Id"].ToString());
            }

            return Csoportok;

        }

        /// <summary>
        /// Mely csoportokban van a felhaszn�l�nak vezet�i tags�ga
        /// Figyelem exception-t dobhat, a h�v� oldalon figyelni kell!!!
        /// </summary>
        /// <param name="Felhasznalo_Id"></param>
        /// <returns></returns>
        public static List<string> GetFelhasznaloVezetettCsoportjai(Page page, String Felhasznalo_Id)
        {
            /// TODO: nincs m�g meg, egyel�re csak �sszehasonl�tja a k�t Id-t (az egyszem�lyes csoportokra ez ok)
            if (String.IsNullOrEmpty(Felhasznalo_Id))
            {
                return null;
            }

            ExecParam xpm = UI.SetExecParamDefault(page, new ExecParam());
            KRT_CsoportTagokService svc = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch sch = new KRT_CsoportTagokSearch();

            sch.Csoport_Id_Jogalany.Value = Felhasznalo_Id;
            sch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            sch.Tipus.Value = "3"; // vezeto
            sch.Tipus.Operator = Query.Operators.equals;

            Result res = svc.GetAll(xpm, sch);


            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                throw new ResultException(res);
            }

            List<string> Csoportok = new List<string>();

            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                Csoportok.Add(row["Csoport_Id"].ToString());
            }

            return Csoportok;

        }


        /// <summary>
        /// Visszaadja a felhaszn�l� saj�t, egyszem�lyes csoportj�nak Id-j�t
        /// (ez jelenleg megegyezik a felhaszn�l� id-j�val)
        /// </summary>
        /// <param name="Felhasznalo_Id"></param>
        /// <returns></returns>
        public static String GetFelhasznaloSajatCsoportId(String Felhasznalo_Id)
        {
            return Felhasznalo_Id;
        }

        public static String GetFelhasznaloSajatCsoportId(ExecParam execParam)
        {
            return GetFelhasznaloSajatCsoportId(execParam.Felhasznalo_Id);
        }

        public static string GetFelhasznaloVezetoID(ExecParam p_execParam)
        {
            return GetCsoportVezetoID(p_execParam, p_execParam.FelhasznaloSzervezet_Id);
        }

        public static string GetCsoportVezetoID(ExecParam p_execParam, string p_csoportID)
        {
            Logger.Debug("eUtility.Csoportok.GetCsoportVezetoje start", p_execParam);

            Arguments args = new Arguments();
            args.Add(new Argument("Csoport azonos�t�", p_csoportID, ArgumentTypes.Guid));
            args.ValidateArguments();

            Logger.Debug("p_csoport_Id: " + p_csoportID);

            Logger.Debug("KRT_CsoportokService GetLeader start");
            KRT_CsoportokService svcCsoportok = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            ExecParam xpmCsoportok = p_execParam.Clone();
            Result resCsoportok = svcCsoportok.GetLeader(xpmCsoportok, p_csoportID);

            if (!String.IsNullOrEmpty(resCsoportok.ErrorCode))
            {
                Logger.Error("KRT_CsoportokService GetLeader hiba", xpmCsoportok, resCsoportok);
                throw new ResultException(resCsoportok);
            }
            Logger.Debug("KRT_CsoportokService GetLeader end");

            KRT_Csoportok csoportVezeto = (KRT_Csoportok)resCsoportok.Record;

            if (csoportVezeto == null || String.IsNullOrEmpty(csoportVezeto.Id))
            {
                //A csoport vezet�je nem tal�lhat�
                Logger.Error("csoportVezeto == null || String.IsNullOrEmpty(csoportVezeto.Id)", xpmCsoportok);
                throw new ResultException(53201);
            }


            string vezetoID = csoportVezeto.Id;
            Logger.Debug("vezetoID: " + vezetoID);
            Logger.Debug("eUtility.Csoportok.GetCsoportVezetoje end", p_execParam);
            return vezetoID;
        }

        public static string GetFirstRightedLeader_ID(ExecParam p_execParam, bool bIncludeSelfIfRightedLeader, string functionRight)
        {
            Logger.Debug("eUtility.Csoportok.GetFirstRightedLeader_ID start", p_execParam);

            Arguments args = new Arguments();
            args.Add(new Argument("Felhaszn�l� szervezet azonos�t�", p_execParam.FelhasznaloSzervezet_Id, ArgumentTypes.Guid));
            args.ValidateArguments();

            Logger.Debug("p_csoport_Id: " + p_execParam.FelhasznaloSzervezet_Id);

            Logger.Debug("KRT_CsoportokService GetFirstRightedLeader start");
            KRT_CsoportokService svcCsoportok = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            ExecParam xpmCsoportok = p_execParam.Clone();
            Result resCsoportok = svcCsoportok.GetFirstRightedLeader(xpmCsoportok, bIncludeSelfIfRightedLeader, functionRight);

            if (!String.IsNullOrEmpty(resCsoportok.ErrorCode))
            {
                Logger.Error("KRT_CsoportokService GetFirstRightedLeader hiba", xpmCsoportok, resCsoportok);
                throw new ResultException(resCsoportok);
            }
            Logger.Debug("KRT_CsoportokService GetFirstRightedLeader end");

            KRT_Csoportok csoportVezeto = (KRT_Csoportok)resCsoportok.Record;
            if (csoportVezeto == null || String.IsNullOrEmpty(csoportVezeto.Id))
            {
                //A csoport vezet�je nem tal�lhat�
                Logger.Error("csoportVezeto == null || String.IsNullOrEmpty(csoportVezeto.Id)", xpmCsoportok);
                throw new ResultException(53202);
            }

            string vezetoID = csoportVezeto.Id;
            Logger.Debug("vezetoID: " + vezetoID);
            Logger.Debug("eUtility.Csoportok.GetFirstRightedLeader_ID end", p_execParam);
            return vezetoID;
        }

        //private static KRT_Csoportok mindenki = null;

        //public static KRT_Csoportok GetMindenki(ExecParam execParam)
        //{
        //    if (mindenki != null)
        //    {
        //        return mindenki;
        //    }
        //    else
        //    {
        //        KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
        //        string kod = Rendszerparameterek.Get(execParam, Rendszerparameterek.CSOPORT_MINDENKI);
        //        KRT_Csoportok csoport = new KRT_Csoportok();
        //        csoport.Kod = kod;
        //        Result res = service.GetByKod(execParam, csoport);
        //        if (String.IsNullOrEmpty(res.ErrorCode))
        //        {
        //            KRT_Csoportok csoportMindenki = (KRT_Csoportok)res.Record;
        //            if (csoportMindenki != null)
        //            {
        //                mindenki = csoportMindenki;
        //                return csoportMindenki;
        //            }
        //            else
        //            {
        //                throw new ResultException("Mindenki csoport not exist");
        //            }
        //        }
        //        else
        //        {
        //            throw new ResultException(res);
        //        }
        //    }
        //}

        #region CsoportFilterObject:  A sz�r�si param�terek �tad�s�ra, ajax �s query string is

        public class CsoportFilterObject
        {
            public enum CsoprtTipus
            {
                Disabled = -1,  // nem adhat� meg csoport, pl. a kiv�lasztott szign�l�si t�pushoz 
                Empty = 0,
                Szervezet = 1,
                Dolgozo = 2,
                Irattar = 3,
                SajatSzervezetDolgozoi = 4,    // hierarchi�ban lefel�, alszervezetek dolgoz�i is
                SajatCsoportDolgozoi = 5,    // csak a k�zvetlen�l hozz�rendeltek, az alszervezetek� nem
                OsszesDolgozo = 6,
                Bizottsag = 7,
                // CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa
                AllAtmenetiIrattar = 8
            }

            public string FelhasznaloId = String.Empty;
            public string SzervezetId = String.Empty;
            public string IktatokonyvId = String.Empty;
            // a LovListtel val� kommunik�ci�hoz (mivel a Tipus nem string, ez�rt JavaScript nem adhat� �t)
            public string SzignalasTipus = String.Empty;
            public CsoprtTipus Tipus = CsoprtTipus.Empty;

            public CsoportFilterObject()
            {
            }

            public CsoportFilterObject(string serializedString)
            {
                this.JsDeserialize(serializedString);
            }

            public string JsSerialize()
            {
                Logger.Debug("A serialization kezdete");

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                serializer.RegisterConverters(new JavaScriptConverter[] { new CsoportFilterObjectConverter() });

                string result = serializer.Serialize(this);

                Logger.Debug("A serialization eredm�nye: " + result);

                return result;
            }

            public void JsDeserialize(string serializedString)
            {
                Logger.Debug("A deserialization kezdete");
                Logger.Debug("A bemeneti string: " + serializedString);

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                serializer.RegisterConverters(new JavaScriptConverter[] { new CsoportFilterObjectConverter() });

                try
                {
                    CsoportFilterObject filterObject = serializer.Deserialize<CsoportFilterObject>(serializedString);

                    this.FelhasznaloId = filterObject.FelhasznaloId;
                    this.SzervezetId = filterObject.SzervezetId;
                    this.IktatokonyvId = filterObject.IktatokonyvId;
                    this.Tipus = filterObject.Tipus;
                    this.SzignalasTipus = filterObject.SzignalasTipus;

                    Logger.Debug(String.Format("A deserialization eredm�nye: FelhasznaloId: {0}, IktatokonyvId: {1}, Tipus: {2}, SzervezetId: {3}, SzignalasTipus: {4}", this.FelhasznaloId, this.IktatokonyvId, this.Tipus, this.SzervezetId, this.SzignalasTipus));
                }
                catch (Exception e)
                {
                    this.FelhasznaloId = serializedString.Split(';')[0];
                    Logger.ErrorStart();
                    Logger.Error("Javascript deserialization hiba: " + e.Message);
                    Logger.Error("A bemeneti soros�tott string: " + serializedString);
                    Logger.ErrorEnd();
                }
            }

            private const string queryPrefix = "FilterBy";

            public string QsSerialize()
            {
                Logger.Debug("A query string� soros�t�s kezdete");
                string query = String.Empty;
                if (!String.IsNullOrEmpty(this.IktatokonyvId))
                {
                    query += queryPrefix + "IktatokonyvId=" + this.IktatokonyvId;
                }

                if (this.Tipus != CsoprtTipus.Empty)
                {
                    if (!String.IsNullOrEmpty(query))
                    {
                        query += "&";
                    }

                    query += queryPrefix + "Tipus=" + ((int)this.Tipus).ToString();
                }

                if (!String.IsNullOrEmpty(this.SzervezetId))
                {
                    if (!String.IsNullOrEmpty(query))
                    {
                        query += "&";
                    }

                    query += queryPrefix + "SzervezetId=" + this.SzervezetId;
                }

                if (!String.IsNullOrEmpty(this.SzignalasTipus))
                {
                    if (!String.IsNullOrEmpty(query))
                    {
                        query += "&";
                    }

                    query += queryPrefix + "SzignalasTipus=" + this.SzignalasTipus;
                }

                Logger.Debug("A query string� soros�t�s v�ge: " + query);

                return query;
            }

            public void QsDeserialize(System.Collections.Specialized.NameValueCollection queryString)
            {
                Logger.Debug("A query string desoros�t�s kezdete");
                Logger.Debug("A bemenet: " + queryString.ToString());

                string iktatokonyvId = queryString.Get(queryPrefix + "IktatokonyvId");
                string tipus = queryString.Get(queryPrefix + "Tipus");
                string szervezetId = queryString.Get(queryPrefix + "SzervezetId");
                string szignalasTipus = queryString.Get(queryPrefix + "SzignalasTipus");

                if (!String.IsNullOrEmpty(iktatokonyvId))
                {
                    this.IktatokonyvId = iktatokonyvId;
                }

                if (!String.IsNullOrEmpty(tipus))
                {
                    try
                    {
                        this.Tipus = (CsoprtTipus)Int32.Parse(tipus);
                    }
                    catch (InvalidCastException e)
                    {
                        Logger.Warn("Hiba a query string �rtelmez�se k�zben: " + e.Message);
                    }
                }

                if (!String.IsNullOrEmpty(szervezetId))
                {
                    this.SzervezetId = szervezetId;
                }

                if (!String.IsNullOrEmpty(szignalasTipus))
                {
                    this.SzignalasTipus = szignalasTipus;
                }

                Logger.Debug("A query string desoros�t�s v�ge");
                Logger.Debug(String.Format("Eredm�ny: IktatokonyId: {0}, Tipus: {1}, SzervezetId: {2}, SzignalasTipus: {3}", this.IktatokonyvId, this.Tipus.ToString(), this.SzervezetId, this.SzignalasTipus));
            }

            public static CsoprtTipus GetCsoportTipusbySzignalasTipus(string SzignalasTipus)
            {
                switch (SzignalasTipus)
                {
                    case KodTarak.SZIGNALAS_TIPUSA._1_Szervezetre_Ugyintezore_Iktatas:
                    case KodTarak.SZIGNALAS_TIPUSA._2_Szervezetre_Iktatas_Ugyintezore:
                    case KodTarak.SZIGNALAS_TIPUSA._3_Iktatas_Szervezetre_Ugyintezore:
                        return CsoprtTipus.Disabled;
                    case KodTarak.SZIGNALAS_TIPUSA._4_NincsSzervezetreSzignalas:
                        return CsoprtTipus.Szervezet;
                    case KodTarak.SZIGNALAS_TIPUSA._5_AutomatikusSzignalasUgyintezore:
                        return CsoprtTipus.Dolgozo;
                    case KodTarak.SZIGNALAS_TIPUSA._6_IktatoSzabadonValaszthat:
                    default:
                        return CsoprtTipus.Disabled;
                }
            }

            public void SetCsoportTipusbySzignalasTipus(string SzignalasTipus)
            {
                CsoprtTipus tipus = GetCsoportTipusbySzignalasTipus(SzignalasTipus);

                if (tipus != CsoprtTipus.Empty)
                {
                    this.Tipus = tipus;
                }
            }
        }

        public class CsoportFilterObjectConverter : JavaScriptConverter
        {

            public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
            {
                if (dictionary == null)
                    throw new ArgumentNullException("dictionary");

                if (type == typeof(CsoportFilterObject))
                {
                    // Create the instance to deserialize into.
                    CsoportFilterObject csoportFilter = new CsoportFilterObject();

                    // Deserialize the items.
                    Dictionary<string, object> itemList = (Dictionary<string, object>)dictionary["CsoportFilterObject"];

                    csoportFilter.FelhasznaloId = itemList["FelhasznaloId"].ToString();
                    csoportFilter.IktatokonyvId = itemList["IktatokonyvId"].ToString();
                    csoportFilter.SzervezetId = itemList["SzervezetId"].ToString();
                    csoportFilter.SzignalasTipus = itemList["SzignalasTipus"].ToString();
                    try
                    {
                        csoportFilter.Tipus = (CsoportFilterObject.CsoprtTipus)(itemList["Tipus"] ?? CsoportFilterObject.CsoprtTipus.Empty);
                    }
                    catch (InvalidCastException e)
                    {
                        Logger.Warn("Hiba a deserialization �rtelmez�se k�zben" + e.Message);
                    }

                    return csoportFilter;
                }
                return null;
            }

            public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
            {
                CsoportFilterObject objType = obj as CsoportFilterObject;

                if (objType != null)
                {
                    // Create the representation.
                    Dictionary<string, object> result = new Dictionary<string, object>();

                    //Add each entry to the dictionary.
                    Dictionary<string, object> listDict = new Dictionary<string, object>();
                    listDict.Add("FelhasznaloId", objType.FelhasznaloId);
                    listDict.Add("SzervezetId", objType.SzervezetId);
                    listDict.Add("IktatokonyvId", objType.IktatokonyvId);
                    listDict.Add("Tipus", objType.Tipus);
                    listDict.Add("SzignalasTipus", objType.Tipus);

                    result["CsoportFilterObject"] = listDict;

                    return result;
                }
                return new Dictionary<string, object>();
            }

            public override IEnumerable<Type> SupportedTypes
            {
                get { return new ReadOnlyCollection<Type>(new List<Type>(new Type[] { typeof(CsoportFilterObject) })); }
            }
        }

        #endregion


    }
}
