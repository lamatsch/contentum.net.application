using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Contentum.eUtility
{

    public class DataContext
    {
        // Snapshot Isolation Level a f�bb list�k sz�m�ra (�gyiratok, iratok, k�ldem�nyek, ...)
        public const IsolationLevel DefaultGetAllWithExtensionIsolationLevel = IsolationLevel.Snapshot;

        private string connectionString;

        private SqlConnection connection;
        public SqlConnection Connection
        {
            get { return connection; }
        }

        private SqlTransaction transaction;
        public SqlTransaction Transaction
        {
            get { return transaction; }
        }

        private SqlGuid _tranz_id;
        public string Tranz_Id
        {
            get { return _tranz_id.ToString(); }
            set { _tranz_id = SqlGuid.Parse(value); }
        }

        public DataContext(HttpApplicationState AppState)
        {
            connectionString = DataContext.GetConnectionString(AppState);
        }

        public DataContext(string ConnectionString)
        {
            this.connectionString = ConnectionString;
        }

        private SqlConnection GetNewSqlConnection()
        {
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.InfoMessage += new SqlInfoMessageEventHandler(Connection_InfoMessage);
            return sqlconn;
        }

        // Loggol�s
        private void Connection_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            if (e != null && e.Errors.Count > 0)
            {
                SqlError[] arrayOfItems = (SqlError[])(new System.Collections.ArrayList(e.Errors)).ToArray(typeof(SqlError));

                // PRINT-eket nem loggoljuk ki: ErrorNumber > 0
                SqlError[] arrayOfErrors = Array.FindAll<SqlError>(arrayOfItems, delegate(SqlError error) { return error.Number > 0; });

                if (arrayOfErrors != null && arrayOfErrors.Length > 0)
                {
                    Logger.Info(String.Format("SqlInfoMessage: Count: {0}", e.Errors.Count));
                    foreach (SqlError err in e.Errors)
                    {
                        Logger.Info(String.Format("[{0}][{1}/{2}]Procedure: {3} LineNumber: {4} Message: {5}"
                            , err.Number, err.Class, err.State, err.Procedure, err.LineNumber, err.Message));
                    }
                }
            }
        }


        /// <summary>
        /// Megnyitja a connection-t, ha az nincs nyitva
        /// </summary>
        /// <returns>true: ha megnyitotta a connection-t, false: ha m�r nyitva volt</returns>
        public bool OpenConnectionIfRequired()
        {
            if (connection == null)
            {
                connection = GetNewSqlConnection();
            }

            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }

        }

        public bool BeginTransactionIfRequired()
        {
            return BeginTransactionIfRequired(IsolationLevel.ReadCommitted);

            //// ha m�r van tranzakci�, nem kell �jat nyitni
            //if (transaction != null && transaction.Connection != null)
            //{
            //    return false;
            //}
            //else
            //{
            //    // ha nem nyitottak m�g connection-t, hiba:
            //    if (connection == null || connection.State != ConnectionState.Open)
            //    {
            //        throw new Exception();
            //    }
            //    else
            //    {
            //        transaction = connection.BeginTransaction();
            //        _tranz_id = Guid.NewGuid();
            //        return true;
            //    }
            //}            
        }

        public bool BeginTransactionIfRequired(IsolationLevel isolationLevel)
        {
            // ha m�r van tranzakci�, nem kell �jat nyitni
            if (transaction != null && transaction.Connection != null)
            {
                return false;
            }
            else
            {
                // ha nem nyitottak m�g connection-t, hiba:
                if (connection == null || connection.State != ConnectionState.Open)
                {
                    throw new Exception();
                }
                else
                {
                    transaction = connection.BeginTransaction(isolationLevel);
                    _tranz_id = Guid.NewGuid();
                    return true;
                }
            }
        }


        public void CloseConnectionIfRequired(bool needToClose)
        {
            if (needToClose == false)
            {
                return;
            }
            else
            {
                if (connection != null
                    && connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
        }

        public void CommitIfRequired(bool needToCommit)
        {
            if (needToCommit == false)
            {
                return;
            }
            else
            {
                if (transaction == null) { return; }

                try
                {
                    transaction.Commit();
                    _tranz_id = SqlGuid.Null;
                }
                catch
                {
                    // TODO: Commit sikertelen
                }
            }
        }

        public void RollbackIfRequired(bool needToRollBack)
        {
            if (needToRollBack == false)
            {
                return;
            }
            else
            {
                if (transaction == null) { return; }

                try
                {
                    transaction.Rollback();
                    _tranz_id = SqlGuid.Null;
                }
                catch
                {
                    // TODO: RollBack sikertelen
                }
            }
        }

        //public bool IsInTransaction()
        //{
        //    return false;
        //}


        public static String GetConnectionString(HttpApplicationState App)
        {
            ConnectionStringSettings conn = new ConnectionStringSettings();
            conn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"];

            return conn.ConnectionString;
        }

    }
}
