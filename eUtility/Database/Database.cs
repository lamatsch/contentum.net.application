﻿using Contentum.eBusinessDocuments;
using System.Data.SqlClient;

namespace Contentum.eUtility
{
    public static class Database
    {
        public static SqlConnection GetSqlConnection(string connStr)
        {
            return Utility.GetSqlConnection(connStr);
        }
    }
}
