﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.XPath;


namespace Contentum.eUtility
{
    public class Result_List
    {
        #region Adattagok
        private DataTable _Result_List_DB;
        public DataTable Result_List_DB
        {
            get { return _Result_List_DB; }
            set { _Result_List_DB = value; }
        }

        private bool _errorFlag;
        public bool ErrorFlag
        {
            get { return _errorFlag; }
            set { _errorFlag = value; }
        }

        private string _classMsg;
        public string ClassMsg
        {
            get { return _classMsg; }
            set { _classMsg = value; }
        }

        private string _connectionString_Main;
        public string ConnectionString_Main
        {
            get { return _connectionString_Main; }
            set { _connectionString_Main = value; }
        }

        private List<string> _eQuery_ClassnameList;
        public List<string> eQuery_ClassnameList
        {
            get { return _eQuery_ClassnameList; }
            set { _eQuery_ClassnameList = value; }
        }
        #endregion

        #region Konstans értékek
        private const string resultList_TableName = "KRT_Mentett_Talalati_Listak";
        #endregion

        public Result_List(SqlConnection incoming_SqlConnection)
        {
            string _temporaryStringHolder = incoming_SqlConnection.ConnectionString;
            ErrorFlag = checkConn(incoming_SqlConnection);
            if (!ErrorFlag)
            { ConnectionString_Main = _temporaryStringHolder; }

            Result_List_DB = new DataTable();

            //eQuery_ClassnameList = eQuery_ListLoader();
        }

        public void Save(object HeaderType, DateTime saveDate, ExecParam execParam, string workStation, string ListName)
        {
            string objectFullName = "";
            string objectName = "";
            string BodyXML = "";
            string HeaderXML = "";
            SqlCommand sqlCommand = new SqlCommand();

            List<IdValue> objectPropertyList = new List<IdValue>();
            objectFullName = getObjectType_eQuery(HeaderType);
            Result res = new Result();
            if (!string.IsNullOrEmpty(objectFullName))
            {
                objectName = Regex.Replace(objectFullName, "Search", "");
                switch (objectFullName.ToLower())
                {
                    case "erec_kuldkuldemenyeksearch":
                        EREC_KuldKuldemenyekSearch kuldemenySearch = (EREC_KuldKuldemenyekSearch)HeaderType;
                        EREC_KuldKuldemenyekService kuldemenyService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();

                        res = kuldemenyService.GetAllWithExtension(execParam, kuldemenySearch);

                        BodyXML = DataTable_To_XMLstring(res.Ds.Tables[0]);
                        HeaderXML = DataTable_To_XMLstring(BuildHeader_FromSearchObject(HeaderType));
                        break;
                    case "erec_ugyugyiratoksearch":
                        EREC_UgyUgyiratokSearch ugyiratSearch = (EREC_UgyUgyiratokSearch)HeaderType;
                        EREC_UgyUgyiratokService ugyiratService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                        res = ugyiratService.GetAllWithExtension(execParam, ugyiratSearch);

                        BodyXML = DataTable_To_XMLstring(res.Ds.Tables[0]);
                        HeaderXML = DataTable_To_XMLstring(BuildHeader_FromSearchObject(HeaderType));
                        break;
                    case "erec_irairatoksearch":
                        EREC_IraIratokSearch iratokSearch = (EREC_IraIratokSearch)HeaderType;
                        EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                        res = iratokService.GetAllWithExtension(execParam, iratokSearch);
                        if (res.Ds.Tables[0].Columns.Contains("PostazasIranya"))
                        {
                            res.Ds.Tables[0].Columns.Remove(res.Ds.Tables[0].Columns["PostazasIranya"]);
                        }
                        BodyXML = DataTable_To_XMLstring(res.Ds.Tables[0]);
                        HeaderXML = DataTable_To_XMLstring(BuildHeader_FromSearchObject(HeaderType));
                        break;
                    case "erec_pldiratpeldanyoksearch":
                        EREC_PldIratPeldanyokSearch pldiratokSearch = (EREC_PldIratPeldanyokSearch)HeaderType;
                        EREC_PldIratPeldanyokService pldiratokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

                        res = pldiratokService.GetAllWithExtension(execParam, pldiratokSearch);

                        BodyXML = DataTable_To_XMLstring(res.Ds.Tables[0]);
                        HeaderXML = DataTable_To_XMLstring(BuildHeader_FromSearchObject(HeaderType));
                        break;
                    case "krt_dokumentumoksearch":
                        KRT_DokumentumokSearch dokSearch = (KRT_DokumentumokSearch)HeaderType;
                        KRT_DokumentumokService dokService = eAdminService.ServiceFactory.GetKRT_DokumentumokService();

                        res = dokService.GetAll(execParam, dokSearch);

                        BodyXML = DataTable_To_XMLstring(res.Ds.Tables[0]);
                        HeaderXML = DataTable_To_XMLstring(BuildHeader_FromSearchObject(HeaderType));
                        break;
                    default:
                        objectPropertyList = propertyLister_eQuery(HeaderType, true);
                        BodyXML = DataTable_To_XMLstring(BuildQuery(objectPropertyList, objectName,"1000"));
                        HeaderXML = DataTable_To_XMLstring(BuildHeader(objectPropertyList));
                        break;
                }

                DateTime fixed_Erv_Vege = new DateTime(4700, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                sqlCommand.CommandText = "INSERT INTO " + resultList_TableName + " (Id,ListName,SaveDate,Searched_Table,Requestor,HeaderXML,BodyXML,WorkStation,Letrehozo_Id,ErvVege)" +
                    " VALUES ('" + Guid.NewGuid().ToString() + "','" + ListName + "','" + saveDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + objectName + "','" +
                    execParam.LoginUser_Id + "','" + Regex.Replace(HeaderXML, "'", "''") + "','" + Regex.Replace(BodyXML, "'", "''") + "','" + workStation + "','" + execParam.LoginUser_Id + "','" + fixed_Erv_Vege.ToString("yyyy-MM-dd HH:mm:ss") + "')";
                QueryExecute(sqlCommand);
            }
            else
            {
                ErrorFlag = true;
                ClassMsg = "A(z) '" + HeaderType.GetType().Name + "' nevű objektum nem tagja a rendszernek!";
                return;
            }
        }

        public DataTable Load(List<IdValue> _searchList)
        {
            SqlCommand sqlCommand = new SqlCommand();
            Result_List_DB = BuildQuery(_searchList, resultList_TableName, "1000");
            return Result_List_DB;
        }

        public DataTable Load()
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "SELECT * FROM " + resultList_TableName;
            FillDataTable(sqlCommand, Result_List_DB);
            return _Result_List_DB;
        }

        public void Modify(String id_Modify)
        {
            //TODO - Implementálni majd ha kell. A jelenlegi specifikáció alapján nem volt szükséges.
        }

        public void Delete(List<string> id_List)
        {
            SqlCommand sqlCommand = new SqlCommand();
            if (id_List.Count == 0)
            {
                ErrorFlag = true;
                ClassMsg = "No ID was selected to delete from " + resultList_TableName + "!";
                return;
            }

            #region Listaszűrés és értékállítás (SQL Syntax miatt)
            for (int i = 0; i < id_List.Count; i++)
            {
                if (string.IsNullOrEmpty(id_List[i]))
                {
                    id_List.RemoveAt(i);
                }
                id_List[i] = "'" + id_List[i] + "'";
            }
            #endregion

            switch (id_List.Count)
            {
                case 1:
                    sqlCommand.CommandText = "DELETE FROM " + resultList_TableName + " WHERE Id = '" + id_List[0] + "'";
                    break;
                default:
                    sqlCommand.CommandText = "DELETE FROM " + resultList_TableName + " WHERE Id IN [" + string.Join(",", id_List.ToArray()) + "]";
                    break;
            }
            QueryExecute(sqlCommand);
        }

        public void SaveToExcel(String BodyXML, String HeaderXML, String fileName)
        {
            fileName += ".xls";
            HSSFWorkbook workbook = new HSSFWorkbook();
            ISheet worksheet_Body = BuildWorkSheet(XMLstring_To_DataTable(BodyXML), "Keresés eredménye", false, workbook);
            ISheet worksheet_Header = BuildWorkSheet(XMLstring_To_DataTable(HeaderXML), "Keresési paraméterek", true, workbook);
            //TESZT CÉLJÁBÓL

            MemoryStream memoryStream = new MemoryStream();
            workbook.Write(memoryStream);
            HttpResponse response = HttpContext.Current.Response;
            response.Clear();
            response.Buffer = true;
            response.AddHeader("Accept-Header", "0");
            response.AddHeader("Content-Length", memoryStream.Length.ToString());
            string contentDispositon = "inline" + (!String.IsNullOrEmpty(fileName) ? "; filename=" + fileName : "");
            response.AddHeader("Content-Disposition", contentDispositon);
            response.AddHeader("Expires", "0");
            response.AddHeader("Pragma", "cache");
            response.AddHeader("Cache-Control", "private");
            response.ContentType = "application/vnd.ms-excel";
            response.AddHeader("Accept-Ranges", "bytes");
            response.BinaryWrite(memoryStream.GetBuffer());
            response.Flush();
            try
            {
                response.End();
            }
            catch
            {
            }
            /*HttpResponse response = HttpContext.Current.Response;
            response.ContentType = "application/vnd.ms-excel";
            response.CacheControl = "public";
            response.HeaderEncoding = System.Text.Encoding.UTF8;
            response.Charset = "utf-8";
            response.AddHeader("Content-Disposition", string.Format("inline;filename={0}", fileName));
            response.Clear();

            response.BinaryWrite(memoryStream.GetBuffer());
            response.End();*/
            //EDDIG
        }

        #region Saját belső eljárások
        private string getObjectType_eQuery(object _TestingObject)
        {
            string _objectTypeName = _TestingObject.GetType().Name;
            /*string _objectTypeName = "";
            foreach (String _listItem in eQuery_ClassnameList)
            {
                if (_listItem == _TestingObject.GetType().Name)
                {
                    _objectTypeName = _listItem;
                    break;
                }
            }*/

            return _objectTypeName;
        }

        private List<string> eQuery_ListLoader()
        {
            List<string> _temporaryList = new List<string>();
            Type[] raw_AssemblyTypeList = Assembly.GetExecutingAssembly().GetTypes();
            foreach (Type t in raw_AssemblyTypeList)
            {
                if (t.Namespace == "eQuery")
                {
                    _temporaryList.Add(t.Name);
                }
            }
            return _temporaryList;
        }

        private List<IdValue> propertyLister_eQuery(object _eQueryObject, bool isErvenyessegNeeded)
        {
            List<IdValue> _tempList = new List<IdValue>();
            foreach (PropertyInfo _property in _eQueryObject.GetType().GetProperties())
            {
                if (_property.PropertyType.Name.ToLower() != "field") { continue; }
                IdValue _temp = new IdValue();
                 string eQueryObjectName = getObjectType_eQuery(_eQueryObject);
                 switch (eQueryObjectName.ToLower())
                 { 
                    case "erec_kuldkuldemenyeksearch":
                    case "erec_ugyugyiratoksearch":
                    case "erec_irairatoksearch":
                    case "erec_pldiratpeldanyoksearch":
                    case "krt_dokumentumoksearch":
                        if (_property.Name.ToLower().Contains("manual_"))
                        {
                            _temp.Id = Regex.Replace(_property.Name, "Manual_", "");
                        }
                        else
                        { _temp.Id = _property.Name; }
                        break;
                    default:
                        if (_property.Name.ToLower().Contains("manual_"))
                        { continue;  }
                        _temp.Id = _property.Name;
                        break;
                 }
                if (!isErvenyessegNeeded)
                {
                    if (_property.Name.ToLower() == "ervkezd" || _property.Name.ToLower() == "ervvege")
                    { continue; }
                }
 
                object _temporary = _property.GetValue(_eQueryObject, null);
                foreach (PropertyInfo _tempProperty in _temporary.GetType().GetProperties())
                {
                    if (_tempProperty.Name.ToLower() == "value")
                    {
                        _temp.Value = SQL_dataFunction_Converter( _tempProperty.GetValue(_temporary, null).ToString());
                    }
                    if (_tempProperty.Name.ToLower() == "operator")
                    {
                        _temp.Operator = _tempProperty.GetValue(_temporary, null).ToString();
                        if (string.IsNullOrEmpty(_temp.Operator))
                        {
                            _temp.Operator = "equal";
                        }
                    }
                    if (_tempProperty.Name.ToLower() == "valueto")
                    {
                        _temp.ValueTo = _tempProperty.GetValue(_temporary, null).ToString();
                    }
                }
                _tempList.Add(_temp);
            }
            return _tempList;
        }

        //Ez az eljárás ellenőrzi az sqlConnectiont, ha hibás, true értéket küld.
        private bool checkConn(SqlConnection test_Connection)
        {
            try
            {
                using (test_Connection)
                {
                    test_Connection.Open();
                    test_Connection.Close();
                    ClassMsg = "";
                    return false;
                }
            }
            catch (SqlException _sqlTestExc)
            {
                ClassMsg = _sqlTestExc.Message;
                return true;
            }
        }

        private DataTable BuildQuery(List<IdValue> searchList, string tableName, string topNumber)
        {
            SqlCommand sqlCommand = new SqlCommand();
            DataTable _returnTable = new DataTable();
            List<string> WhereParams = new List<string>();
            string select_Head = "";

            foreach (IdValue _listItem in searchList)
            {
                if (string.IsNullOrEmpty(_listItem.Value))
                { continue; }
                SqlParameter _temp = new SqlParameter();
                if (string.IsNullOrEmpty(_listItem.Operator))
                {
                    _listItem.Operator = "equal";
                }
                switch (_listItem.Operator.ToLower())
                {    
                    case Query.Operators.isnullorinner:
                    case Query.Operators.isnullornotinner:
                        break;
                    case Query.Operators.notinner:
                    case Query.Operators.inner:
                    case Query.Operators.between:
                        if (!string.IsNullOrEmpty(_listItem.ValueTo))
                        {
                            _temp.ParameterName = _listItem.Id;
                            _temp.Value = _listItem.Value;
                            sqlCommand.Parameters.Add(_temp);
                        }
                        break;
                    default: 
                        _temp.ParameterName = _listItem.Id;
                        _temp.Value = _listItem.Value;
                        sqlCommand.Parameters.Add(_temp);
                        break;
            }
                string whereItem = getOperatorType(_listItem);
                WhereParams.Add(whereItem);
            }

            switch (topNumber)
            { 
                case null:
                case "":
                case "0":
                    select_Head = "SELECT * FROM ";
                    break;
                default:
                    select_Head = "SELECT TOP " + topNumber + " * FROM ";
                    break;
            }

            switch (WhereParams.Count)
            {
                case 0:
                    sqlCommand.CommandText = select_Head + tableName;
                    break;
                case 1:
                    sqlCommand.CommandText = select_Head + tableName + " WHERE " + WhereParams[0];
                    break;
                default:
                    sqlCommand.CommandText = select_Head + tableName + " WHERE " + string.Join(" AND  ", WhereParams.ToArray());
                    break;
            }

            FillDataTable(sqlCommand, _returnTable);

            return _returnTable;
        }

        private DataTable BuildHeader(List<IdValue> dataList)
        {
            DataTable _returnTable = new DataTable();
            _returnTable.Columns.Add("DataTag");
            _returnTable.Columns.Add("Operator");
            _returnTable.Columns.Add("Value");
            _returnTable.Columns.Add("ValueTo");
            foreach (IdValue _listItem in dataList)
            {
                if (string.IsNullOrEmpty(_listItem.Value))
                { continue; }
                if (_listItem.Id.ToLower().Contains("manual_")) {continue;}
                DataRow _tempRow = _returnTable.NewRow();
                _tempRow["DataTag"] = _listItem.Id;
                _tempRow["Operator"] = _listItem.Operator;
                _tempRow["Value"] = _listItem.Value;
                _tempRow["ValueTo"] = _listItem.ValueTo;
                _returnTable.Rows.Add(_tempRow);
            }
            return _returnTable;
        }

        private DataTable BuildHeader_FromSearchObject(object searchObject)
        {
            DataTable _returnTable = new DataTable();
            _returnTable.Columns.Add("DataTag");
            _returnTable.Columns.Add("Operator");
            _returnTable.Columns.Add("Value");
            _returnTable.Columns.Add("ValueTo");
            string objectFullName = getObjectType_eQuery(searchObject);
            string objectName = Regex.Replace(objectFullName, "Search", "");
            List<IdValue> _PropList = new List<IdValue>();
            switch (objectFullName.ToLower())
            {
                case "erec_kuldkuldemenyeksearch":
                    EREC_KuldKuldemenyekSearch kuldemenySearch = (EREC_KuldKuldemenyekSearch)searchObject;
                    _PropList = propertyLister_eQuery(kuldemenySearch, true);
                    _PropList.AddRange(propertyLister_eQuery(kuldemenySearch.Extended_EREC_IraIktatoKonyvekSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(kuldemenySearch.Extended_EREC_KuldKuldemenyekSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(kuldemenySearch.Extended_EREC_PldIratPeldanyokSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(kuldemenySearch.Extended_EREC_SzamlakSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(kuldemenySearch.Extended_KRT_MappakSearch, false));
                    break;
                case "erec_ugyugyiratoksearch":
                    EREC_UgyUgyiratokSearch ugyiratSearch = (EREC_UgyUgyiratokSearch)searchObject;
                    _PropList = propertyLister_eQuery(ugyiratSearch, true);
                    _PropList.AddRange(propertyLister_eQuery(ugyiratSearch.Extended_EREC_IraIktatoKonyvekSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(ugyiratSearch.Extended_EREC_KuldKuldemenyekSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(ugyiratSearch.Extended_EREC_IraIratokSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(ugyiratSearch.Extended_EREC_IratMetaDefinicioSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(ugyiratSearch.Extended_EREC_IrattariKikeroSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(ugyiratSearch.Extended_EREC_UgyUgyiratdarabokSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(ugyiratSearch.Extended_KRT_MappakSearch, false));
                    break;
                case "erec_irairatoksearch":
                    EREC_IraIratokSearch iratokSearch = (EREC_IraIratokSearch)searchObject;
                    _PropList = propertyLister_eQuery(iratokSearch, true);
                    _PropList.AddRange(propertyLister_eQuery(iratokSearch.Extended_EREC_IraIktatoKonyvekSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(iratokSearch.Extended_EREC_KuldKuldemenyekSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(iratokSearch.Extended_EREC_IraOnkormAdatokSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(iratokSearch.Extended_EREC_IratAlairokSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(iratokSearch.Extended_EREC_PldIratPeldanyokSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(iratokSearch.Extended_EREC_UgyUgyiratdarabokSearch, false));
                    break;
                case "erec_pldiratpeldanyoksearch":
                    EREC_PldIratPeldanyokSearch pldiratokSearch = (EREC_PldIratPeldanyokSearch)searchObject;
                    _PropList = propertyLister_eQuery(pldiratokSearch, true);
                    _PropList.AddRange(propertyLister_eQuery(pldiratokSearch.Extended_EREC_IraIktatoKonyvekSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(pldiratokSearch.Extended_EREC_HataridosFeladatokSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(pldiratokSearch.Extended_EREC_IraIratokSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(pldiratokSearch.Extended_EREC_UgyUgyiratokSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(pldiratokSearch.Extended_KRT_MappakSearch, false));
                    _PropList.AddRange(propertyLister_eQuery(pldiratokSearch.Extended_EREC_UgyUgyiratdarabokSearch, false));
                    break;
                case "krt_dokumentumoksearch":
                    KRT_DokumentumokSearch dokSearch = (KRT_DokumentumokSearch)searchObject;
                    _PropList = propertyLister_eQuery(dokSearch, true);
                    break;
                default:
                    IdValue _error = new IdValue();
                    _error.Id = "HIBA";
                    _error.Value = objectName;
                    _PropList.Add(_error);
                    break;
            }
            if (_PropList.Count != 0)
            {
                _returnTable = BuildHeader(_PropList);
            }
            return _returnTable;
        }

        public string DataTable_To_XMLstring(DataTable convDT)
        {
            XPathDocument xml_Result;  
            string xmlString = "";
            using (MemoryStream memoryStream = new MemoryStream())
            {
                convDT.TableName = "xmlTable";
                convDT.WriteXml(memoryStream);
                memoryStream.Position = 0;
                xml_Result = new XPathDocument(memoryStream);
                xmlString = Encoding.UTF8.GetString(memoryStream.ToArray());
            }
            return xmlString;
        }

        public DataTable XMLstring_To_DataTable(string xmlString)
        {
            DataSet ds = new DataSet();
            StringReader xmlString_Reader = new StringReader(xmlString);
            ds.ReadXml(xmlString_Reader);

            if (ds.Tables.Count != 0)
                { return ds.Tables[0]; }
            else
                { return new DataTable(); }
        }

        private void QueryExecute(SqlCommand _sqlCommand)
        {
            SqlConnection ConnMain = new SqlConnection(ConnectionString_Main);
            _sqlCommand.Connection = ConnMain;
            try
            {
                using (ConnMain)
                {
                    ConnMain.Open();
                    _sqlCommand.ExecuteNonQuery();
                    ConnMain.Close();
                    ErrorFlag = false;
                }
            }
            catch (SqlException ex)
            {
                ErrorFlag = true;
                ClassMsg = ex.Message;
            }
        }

        private void FillDataTable(SqlCommand _sqlCommand, DataTable dt)
        {
            SqlConnection ConnMain = new SqlConnection(ConnectionString_Main);
            _sqlCommand.Connection = ConnMain;
            SqlDataAdapter dataAdapter = new SqlDataAdapter(_sqlCommand);
            try
            {
                using (ConnMain)
                {
                    ConnMain.Open();
                    dataAdapter.Fill(dt);
                    ConnMain.Close();
                    ErrorFlag = false;
                }
            }
            catch (SqlException sqlExc)
            {
                ErrorFlag = true;
                ClassMsg = sqlExc.Message;
                dt = new DataTable();
            }
        }

        private ISheet BuildWorkSheet(DataTable table, string sheetName, bool isHeader, HSSFWorkbook mainBook)
        {
            ISheet _tempWorksheet = mainBook.CreateSheet(sheetName);

            #region Állítható kezdőpont koordináták a Munkafüzeten
            int col_StartNumber = 1;
            int row_StartNumber = 1;
            #endregion

            #region Cellastílusok beállítása
            ICellStyle HeaderCell_Style = mainBook.CreateCellStyle();
            HeaderCell_Style.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightOrange.Index;
            HeaderCell_Style.FillPattern = FillPattern.SolidForeground;
            IFont HeaderFont = mainBook.CreateFont();
            HeaderFont.Boldweight = 5;
            HeaderCell_Style.SetFont(HeaderFont);

            #region Páratlan formázások

            #region Páratlan DateTime formázás
            ICellStyle BodyCell_StyleOdd_DateTime = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleOdd_DateTime = mainBook.CreateDataFormat();
            BodyCell_StyleOdd_DateTime.DataFormat = dataFormat_BodyCell_StyleOdd_DateTime.GetFormat("yyyy.MM.dd HH:mm:ss");
            BodyCell_StyleOdd_DateTime.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightYellow.Index;
            BodyCell_StyleOdd_DateTime.FillPattern = FillPattern.SolidForeground;
            #endregion

            #region Páratlan DateTime formázás (Ha nincs idő)
            ICellStyle BodyCell_StyleOdd_DateTime_WithoutTime = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleOdd_DateTime_WithoutTime = mainBook.CreateDataFormat();
            BodyCell_StyleOdd_DateTime_WithoutTime.DataFormat = dataFormat_BodyCell_StyleOdd_DateTime_WithoutTime.GetFormat("yyyy.MM.dd");
            BodyCell_StyleOdd_DateTime_WithoutTime.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightYellow.Index;
            BodyCell_StyleOdd_DateTime_WithoutTime.FillPattern = FillPattern.SolidForeground;
            #endregion

            #region Páratlan Integer formázás
            ICellStyle BodyCell_StyleOdd_Number = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleOdd_Number = mainBook.CreateDataFormat();
            BodyCell_StyleOdd_Number.DataFormat = dataFormat_BodyCell_StyleOdd_Number.GetFormat("0");
            BodyCell_StyleOdd_Number.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightYellow.Index;
            BodyCell_StyleOdd_Number.FillPattern = FillPattern.SolidForeground;
            #endregion

            #region Páratlan Double formázás
            ICellStyle BodyCell_StyleOdd_Double = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleOdd_Double = mainBook.CreateDataFormat();
            BodyCell_StyleOdd_Double.DataFormat = dataFormat_BodyCell_StyleOdd_Double.GetFormat("#,##0.###");
            BodyCell_StyleOdd_Double.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightYellow.Index;
            BodyCell_StyleOdd_Double.FillPattern = FillPattern.SolidForeground;
            #endregion

            #region Páratlan String formázás
            ICellStyle BodyCell_StyleOdd_String = mainBook.CreateCellStyle();
            BodyCell_StyleOdd_String.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LightYellow.Index;
            BodyCell_StyleOdd_String.FillPattern = FillPattern.SolidForeground;
            #endregion

            #endregion

            #region Páros formázások

            #region Páros DateTime formázás
            ICellStyle BodyCell_StyleDuo_DateTime = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleDuo_DateTime = mainBook.CreateDataFormat();
            BodyCell_StyleDuo_DateTime.DataFormat = dataFormat_BodyCell_StyleDuo_DateTime.GetFormat("yyyy.MM.dd HH:mm:ss");
            BodyCell_StyleDuo_DateTime.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            BodyCell_StyleDuo_DateTime.FillPattern = FillPattern.SolidForeground;
            #endregion

            #region Páros DateTime formázás (Ha nincs idő)
            ICellStyle BodyCell_StyleDuo_DateTime_WithoutTime = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleDuo_DateTime_WithoutTime = mainBook.CreateDataFormat();
            BodyCell_StyleDuo_DateTime_WithoutTime.DataFormat = dataFormat_BodyCell_StyleDuo_DateTime_WithoutTime.GetFormat("yyyy.MM.dd");
            BodyCell_StyleDuo_DateTime_WithoutTime.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            BodyCell_StyleDuo_DateTime_WithoutTime.FillPattern = FillPattern.SolidForeground;
            #endregion

            #region Páros Integer formázás
            ICellStyle BodyCell_StyleDuo_Number = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleDuo_Number = mainBook.CreateDataFormat();
            BodyCell_StyleDuo_Number.DataFormat = dataFormat_BodyCell_StyleDuo_Number.GetFormat("0");
            BodyCell_StyleDuo_Number.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            BodyCell_StyleDuo_Number.FillPattern = FillPattern.SolidForeground;
            #endregion

            #region Páros Double formázás
            ICellStyle BodyCell_StyleDuo_Double = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleDuo_Double = mainBook.CreateDataFormat();
            BodyCell_StyleDuo_Double.DataFormat = dataFormat_BodyCell_StyleDuo_Double.GetFormat("#,##0.###");
            BodyCell_StyleDuo_Double.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            BodyCell_StyleDuo_Double.FillPattern = FillPattern.SolidForeground;
            #endregion

            #region Páros String formázás
            ICellStyle BodyCell_StyleDuo_String = mainBook.CreateCellStyle();
            BodyCell_StyleDuo_String.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            BodyCell_StyleDuo_String.FillPattern = FillPattern.SolidForeground;
            #endregion

            #endregion

            #endregion

                switch (isHeader)
                {
                    case true:
                        int colIndex_Incremental = col_StartNumber;
                        IRow HeaderRow = _tempWorksheet.CreateRow(1);
                        IRow BodyRow_Operator = _tempWorksheet.CreateRow(2);
                        IRow BodyRow_Value = _tempWorksheet.CreateRow(3);
                        IRow BodyRow_ValueTo = _tempWorksheet.CreateRow(4);

                        foreach (DataRow rowItem in table.Rows)
                        {
                            ICell HeaderCell = HeaderRow.CreateCell(colIndex_Incremental);
                            ICell BodyCell_Operator = BodyRow_Operator.CreateCell(colIndex_Incremental);
                            ICell BodyCell_Value = BodyRow_Value.CreateCell(colIndex_Incremental);
                            ICell BodyCell_ValueTo = BodyRow_ValueTo.CreateCell(colIndex_Incremental);

                            #region Tábla fejlécének betöltése az Excelbe
                            HeaderCell.SetCellValue(rowItem[0].ToString());
                            HeaderCell.CellStyle = HeaderCell_Style;
                            #endregion

                            #region Tábla adatainak betöltse az Excelbe
                            BodyCell_Operator.SetCellValue(rowItem[1].ToString());

                            switch (excel_ObjectParser(rowItem[2].ToString()))
                            {
                                case TypeCode.DateTime:
                                    BodyCell_Value.SetCellValue(DateTime.Parse(rowItem[2].ToString()));
                                    break;
                                case TypeCode.Char:
                                    if (rowItem[2].ToString().Equals('1') || rowItem[2].ToString().Equals('0'))
                                    {
                                        BodyCell_Value.SetCellValue(bool.Parse(rowItem[2].ToString()).ToString());
                                    }
                                    else
                                    {
                                        BodyCell_Value.SetCellValue(rowItem[2].ToString());
                                    }
                                    break;
                                case TypeCode.Double:
                                    BodyCell_Value.SetCellValue(Double.Parse(rowItem[2].ToString()));
                                    break;
                                case TypeCode.String:
                                    BodyCell_Value.SetCellValue(rowItem[2].ToString());
                                    break;
                            }
                            switch (excel_ObjectParser(rowItem[3].ToString()))
                            {
                                case TypeCode.DateTime:
                                    BodyCell_ValueTo.SetCellValue(DateTime.Parse(rowItem[3].ToString()));
                                    break;
                                case TypeCode.Char:
                                    if (rowItem[3].ToString().Equals('1') || rowItem[3].ToString().Equals('0'))
                                    {
                                        BodyCell_ValueTo.SetCellValue(bool.Parse(rowItem[3].ToString()).ToString());
                                    }
                                    else
                                    {
                                        BodyCell_ValueTo.SetCellValue(rowItem[3].ToString());
                                    }
                                    break;
                                case TypeCode.Double:
                                    BodyCell_ValueTo.SetCellValue(Double.Parse(rowItem[3].ToString()));
                                    break;
                                case TypeCode.String:
                                    BodyCell_ValueTo.SetCellValue(rowItem[3].ToString());
                                    break;
                            }

                            BodyCell_Operator.CellStyle = BodyCell_StyleDuo_String;
                            BodyCell_Value.CellStyle = BodyCell_StyleDuo_String;
                            BodyCell_ValueTo.CellStyle = BodyCell_StyleDuo_String;
                            #endregion

                            colIndex_Incremental++;
                        }
                        break;
                    case false:
                        #region Tábla fejlécének betöltése az Excelbe
                        IRow new_Row_Header = _tempWorksheet.CreateRow(1);
                        foreach (DataColumn colItem in table.Columns)
                        {
                            ICell new_Cell_Header = new_Row_Header.CreateCell(colItem.Ordinal + col_StartNumber);
                            new_Cell_Header.SetCellValue(colItem.ColumnName);
                            new_Cell_Header.CellStyle = HeaderCell_Style;
                        }
                        #endregion

                        #region Tábla adatainak betöltése az Excelbe
                        int rowIndex_Incremental = row_StartNumber;
                        foreach (DataRow rowItem in table.Rows)
                        {
                            IRow new_Row_Body = _tempWorksheet.CreateRow(rowIndex_Incremental + row_StartNumber);
                            foreach (DataColumn colItem in table.Columns)
                            {
                                ICell new_Cell_Body = new_Row_Body.CreateCell(colItem.Ordinal + col_StartNumber);
                                TypeCode temporaryCode = TypeCode.String;
                                switch (excel_ObjectParser(rowItem[colItem.ColumnName].ToString()))
                                {
                                    case TypeCode.DateTime:
                                        new_Cell_Body.SetCellValue(DateTime.Parse(rowItem[colItem.ColumnName].ToString()));
                                        temporaryCode = TypeCode.DateTime;
                                        break;
                                    case TypeCode.Char:
                                        if (rowItem[colItem.ColumnName].ToString().Equals('1') || rowItem[colItem.ColumnName].ToString().Equals('0'))
                                        {
                                            new_Cell_Body.SetCellValue(bool.Parse(rowItem[colItem.ColumnName].ToString()).ToString());
                                        }
                                        else
                                        {
                                            new_Cell_Body.SetCellValue(rowItem[colItem.ColumnName].ToString());
                                        }
                                        break;
                                    case TypeCode.Double:
                                        new_Cell_Body.SetCellValue(Double.Parse(rowItem[colItem.ColumnName].ToString()));
                                        temporaryCode = TypeCode.Double;
                                        break;
                                    case TypeCode.String:
                                        new_Cell_Body.SetCellValue(rowItem[colItem.ColumnName].ToString());
                                        break;
                                    case TypeCode.Int64:
                                        new_Cell_Body.SetCellValue(Int64.Parse(rowItem[colItem.ColumnName].ToString()));
                                        temporaryCode = TypeCode.Int64;
                                        break;
                                }
                                switch (rowIndex_Incremental % 2 != 0)
                                {
                                    case true:
                                        switch (temporaryCode)
                                        {
                                            case TypeCode.String:
                                                new_Cell_Body.CellStyle = BodyCell_StyleOdd_String;
                                                break;
                                            case TypeCode.Int64:
                                                new_Cell_Body.CellStyle = BodyCell_StyleOdd_Number;
                                                break;
                                            case TypeCode.Double:
                                                new_Cell_Body.CellStyle = BodyCell_StyleOdd_Double;
                                                break;
                                            case TypeCode.DateTime:
                                                DateTime dateTime_Checker = DateTime.Parse(new_Cell_Body.DateCellValue.ToString());
                                                if (dateTime_Checker.Hour == 0 && dateTime_Checker.Minute == 0 && dateTime_Checker.Second == 0)
                                                {
                                                    new_Cell_Body.CellStyle = BodyCell_StyleOdd_DateTime_WithoutTime;
                                                }
                                                else
                                                {
                                                    new_Cell_Body.CellStyle = BodyCell_StyleOdd_DateTime;
                                                }
                                                break;
                                        }
                                        break;
                                    case false:
                                        switch (temporaryCode)
                                        {
                                            case TypeCode.String:
                                                new_Cell_Body.CellStyle = BodyCell_StyleDuo_String;
                                                break;
                                            case TypeCode.Int64:
                                                new_Cell_Body.CellStyle = BodyCell_StyleDuo_Number;
                                                break;
                                            case TypeCode.Double:
                                                new_Cell_Body.CellStyle = BodyCell_StyleDuo_Double;
                                                break;
                                            case TypeCode.DateTime:
                                                DateTime dateTime_Checker = DateTime.Parse(new_Cell_Body.DateCellValue.ToString());
                                                if (dateTime_Checker.Hour == 0 && dateTime_Checker.Minute == 0 && dateTime_Checker.Second == 0)
                                                {
                                                    new_Cell_Body.CellStyle = BodyCell_StyleDuo_DateTime_WithoutTime;
                                                }
                                                else
                                                {
                                                    new_Cell_Body.CellStyle = BodyCell_StyleDuo_DateTime;
                                                }
                                                break;
                                        }
                                        break;
                                }
                            }
                            rowIndex_Incremental++;
                        }
                        #endregion
                        break;
                }
            /* NPOI Oldalbeállítás
                * 
                * worksheet_Body.FitToPage = false; <- ezzel lehet teljesen kikapcsolni azt, hogy az oldal magasságához vagy hosszúságához méretezze az excelt.
                * 
                * worksheet_Body.PrintSetup.FitHeight = 1 <- ez a default, a szám az oldalak számát jelöli, ahova el kell férnie hosszában.
                * worksheet_Body.PrintSetup.FitWidth = 1 <- ugyanaz, mint felette, csak ez a szélességhez kötődik.
            */
            _tempWorksheet.PrintSetup.FitHeight = 9999;
            _tempWorksheet.PrintSetup.Landscape = true;
            return _tempWorksheet;
        }

        private TypeCode excel_ObjectParser(string needToParse)
        {
            DateTime parser_DateTime = new DateTime();
            Double parser_Double = new Double();
            Char parser_Char = new Char();
            Int64 parser_Int = new Int64();

            bool isAzonosito = Regex.IsMatch(needToParse.Replace(" ", ""), @"\d{1,4}\/\d{1,4}\/\d{1,4}");

            if (DateTime.TryParse(needToParse, out parser_DateTime) && !isAzonosito)
            {
                return Type.GetTypeCode(parser_DateTime.GetType());
            }
            if (Char.TryParse(needToParse, out parser_Char))
            {
                return Type.GetTypeCode(parser_Char.GetType());
            }
            if (Int64.TryParse(needToParse, out parser_Int))
            {
                return Type.GetTypeCode(parser_Int.GetType());
            }
            if (Double.TryParse(needToParse, out parser_Double))
            {
                return Type.GetTypeCode(parser_Double.GetType());
            }

            return Type.GetTypeCode(needToParse.GetType());
        }

        private string SQL_dataFunction_Converter(string _rawString)
        {
            switch (_rawString.ToLower())
            {
                case "getdate()":
                    return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                default:
                    return _rawString;
            }
        }
        #endregion

        private string getOperatorType(IdValue input)
        {
            switch (input.Operator)
            {
                case Query.Operators.like:
                    return input.Id + " like '%'+@" + input.Id + "+'%'";
                case Query.Operators.likefull:
                    return input.Id + " like @" + input.Id;
                case Query.Operators.likestart:
                    return input.Id + " like @" + input.Id + "+'%'";
                case Query.Operators.notequals:
                    return input.Id + " <> @" + input.Id;
                case Query.Operators.lessorequal:
                    return input.Id + " <= @" + input.Id;
                case Query.Operators.less:
                    return input.Id + " < @" + input.Id;
                case Query.Operators.greater:
                    return input.Id + " > @" + input.Id;
                case Query.Operators.greaterorequal:
                    return input.Id + " >= @" + input.Id;
                case Query.Operators.isnull:
                    return input.Id + " = NULL";
                case Query.Operators.notnull:
                    return input.Id + " <> NULL";
                case Query.Operators.between:
                    if (string.IsNullOrEmpty(input.ValueTo)) { return input.Id + " = @" + input.Id; }
                    else { return input.Id + " between '" + input.Value + "' AND '" + input.ValueTo + "'"; }
                case Query.Operators.inner:
                    if (string.IsNullOrEmpty(input.ValueTo)) { return input.Id + " = @" + input.Id; }
                    else { return input.Id + " IN ('" + input.Value + "','" + input.ValueTo + "')"; }
                case Query.Operators.notinner:
                    if (string.IsNullOrEmpty(input.ValueTo)) { return input.Id + " = @" + input.Id; }
                    else { return input.Id + " NOT IN ('" + input.Value + "','" + input.ValueTo + "')"; }
                case Query.Operators.isnullorequals:
                    return "(" + input.Id + " = NULL OR " + input.Id + " = @" + input.Id + ")";
                case Query.Operators.isnullorinner:
                    if (string.IsNullOrEmpty(input.ValueTo)) { return input.Id + " = NULL"; }
                    else { return "(" + input.Id + " = NULL OR " + input.Id + " IN ('" + input.Value + "','" + input.ValueTo + "')"; }
                case Query.Operators.isnullornotinner:
                    if (string.IsNullOrEmpty(input.ValueTo)) { return input.Id + " = NULL"; }
                    else { return "(" + input.Id + " = NULL OR " + input.Id + " NOT IN ('" + input.Value + "','" + input.ValueTo + "')"; }
                default: return input.Id + " = @" + input.Id;
            }
        }

    }

    public class IdValue
    {
        private string _Id;
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Value;
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        private string _ValueTo;
        public string ValueTo
        {
            get { return _ValueTo; }
            set { _ValueTo = value; }
        }

        private string _Operator;
        public string Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }
    }
}
