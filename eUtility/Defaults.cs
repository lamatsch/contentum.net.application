using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUtility
{
    public class Defaults
    {
        public const int PopupWidth = 800;
        public const int PopupHeight = 650;
        public const int HistoryPopupWidth = 1024;
        public const int HistoryPopupHeight = 768;
        public const int PopupWidth_Max = 1024;
        public const int PopupHeight_Max = 768;
        //LZS - BUG_3592
        public const int PopupWidth_MaxExtended = 1280;

		public const int PopupWidth_1280 = 1280;
        public const int PopupWidth_1360 = 1360;
        public const int PopupWidth_1536 = 1536;
        public const int PopupWidth_1600 = 1600;
        public const int PopupWidth_1900 = 1900;

        public const int PopupHeight_720 = 720;
        public const int PopupHeight_768 = 768;
        public const int PopupHeight_750 = 750;
        public const int PopupHeight_800 = 800;
        public const int PopupHeight_900 = 900;
    }
}
