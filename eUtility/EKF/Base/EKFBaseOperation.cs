﻿using Contentum.eBusinessDocuments;
using System;
using System.Data;
using System.Text;

namespace Contentum.eUtility.EKF
{
    public partial class EKFBaseOperation : IEKFOperationExecute
    {
        public EnumEKFOperationName OperationName { get; set; }
        public EnumEKFOperationType OperationType { get; set; }

        public EKFOperationParameters ParameterProperty = new EKFOperationParameters();
        public virtual string TypeFullName { get; set; }
        public EKFBaseOperation()
        {
            OperationName = EnumEKFOperationName.ISMERETLEN;
            OperationType = EnumEKFOperationType.UNKNOWN;
            TypeFullName = this.GetType().FullName;
        }
        public void AddParameterValue(string key, string value)
        {
            int idx = ParameterProperty.Parameters.IndexOf(new EKFOperationParameterValue() { IdProperty = key });
            if (idx < 0)
                throw new Exception(string.Format("A {0} paraméter nincs a listában !", key));

            ParameterProperty.Parameters[idx].ValueProperty = value;
        }
        public string GetParameterValue(string key)
        {
            int idx = ParameterProperty.Parameters.IndexOf(new EKFOperationParameterValue() { IdProperty = key });
            if (idx < 0)
                throw new Exception(string.Format("A {0} paraméter nincs a listában !", key));

            return ParameterProperty.Parameters[idx].ValueProperty;
        }
        public virtual bool Execute(EKFRuleClass rule, DataSet inputData, out Result result)
        {
            throw new NotImplementedException("Művelet nem engedélyezett !");
        }
      
    }
}
