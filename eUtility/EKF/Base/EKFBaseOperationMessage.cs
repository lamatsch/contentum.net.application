﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Linq;
using System.Text;

namespace Contentum.eUtility.EKF
{
    public partial class EKFBaseOperation
    {
        public StringBuilder MessagesEKF { get; set; }
      
        public void AddMessage(string msg)
        {
            if (string.IsNullOrEmpty(msg))
                return;

            if (MessagesEKF == null)
                MessagesEKF = new StringBuilder();

            MessagesEKF.Append(msg);
        }
        
        public void AddMessage(Result msg)
        {
            if (msg == null)
                return;

            AddMessage(string.Format("EKF: {0} {1} {2}",
                msg.ErrorCode,
                msg.ErrorMessage,
                msg.ErrorDetail != null ? msg.ErrorDetail.Message : string.Empty));
        }
        
        public void AddMessage(Exception msg)
        {
            if (msg == null)
                return;

            AddMessage(string.Format("EKF: {0} {1}",
                msg.Message,
                msg.InnerException == null ? string.Empty : msg.InnerException.Message));
        }
        //Érkeztetésnél és iktatásnál is figyelni kell, hogy van-e kézbesítési tétel "átadásra kijelölt" állapotban az adott elemre,
        //amit éppen érkeztettünk és iktattunk annak a felhasználónak a nevén, akivel a művelet végrehajtásra került, ha van akkor azt a főművelet után át kell adni.
       public void EnsureKezbesitesiTetelAtadasraKerult(EKFRuleClass rule)
        {
            var kezbesitesiTetelekService = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
            ExecParam atadasExecParam = rule.GetExecParam();

            var kezbesitesiTetelSearch = new EREC_IraKezbesitesiTetelekSearch();
            kezbesitesiTetelSearch.Felhasznalo_Id_AtvevoUser.Value = rule.ParametersProperty.UserIdProperty.ToString();
            kezbesitesiTetelSearch.Felhasznalo_Id_AtvevoUser.Operator = eQuery.Query.Operators.equals;
            kezbesitesiTetelSearch.Allapot.Value = KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt;
            kezbesitesiTetelSearch.Allapot.Operator = eQuery.Query.Operators.equals;

            var kezbesitesiTetelekResult = kezbesitesiTetelekService.GetAll(atadasExecParam, kezbesitesiTetelSearch);
            if (kezbesitesiTetelekResult.IsError)
            {               
                throw new ResultException(kezbesitesiTetelekResult);
            }
            //Ha nincs ilyen tétel végeztünk
            if (!kezbesitesiTetelekResult.HasData())
            {
                return;
            }

            //Átadjuk a tételt amit találtunk
            var tetel = kezbesitesiTetelekResult.GetData<EREC_IraKezbesitesiTetelek>().FirstOrDefault();
            var atadasResult = kezbesitesiTetelekService.Atadas(atadasExecParam, tetel.Id);
            if (atadasResult.IsError)
            {                
                throw new ResultException(atadasResult);
            }
        }
    }
}
