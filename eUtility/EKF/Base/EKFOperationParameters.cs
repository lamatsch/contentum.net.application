﻿using System.Collections.Generic;

namespace Contentum.eUtility.EKF
{
    public class EKFOperationParameters
    {
        public EnumEKFOperationParameterType OperationParameterType { get; set; }
        public string TypeFullName { get; set; }

        public List<EKFOperationParameterValue> Parameters = new List<EKFOperationParameterValue>();

        public EKFOperationParameters()
        {
            OperationParameterType = EnumEKFOperationParameterType.UNKNOWN;
            TypeFullName = this.GetType().FullName;
        }
    }
}
