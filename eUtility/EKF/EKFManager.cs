﻿using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Contentum.eUtility.EKF
{
    /// <summary>
    /// ElektronikusKuldemenyFeldolgozas
    /// </summary>
    public partial class EKFManager
    {
        private DataSet MyData { get; set; }
        private ExecParam MyExecParam { get; set; }
        private SortedList<int, EKFRuleClass> MyRules { get; set; }
        private Guid InDataId { get; set; }
        public bool HasError = false;

        public List<string> OccuredMessages = new List<string>();

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="inDataId">EREC_eBeadvanyok.Id</param>
        /// <param name="inExecParam"></param>
        public EKFManager(Guid inDataId, ExecParam inExecParam)
        {
            InDataId = inDataId;
            HasError = false;
            MyRules = new SortedList<int, EKFRuleClass>();
            MyExecParam = inExecParam;
            try
            {
                LoadDataById(inDataId);
                LoadRules();
            }
            catch (Exception exc)
            {
                HasError = true;
                AddMessage("EKF: " + exc.Message);
                Logger.Error("EKF: ", exc);
                throw;
            }
        }

        public EKFManager(ExecParam inExecParam)
        {
            MyExecParam = inExecParam;
        }
        public override string ToString()
        {
            if (MyRules == null)
                return null;
            StringBuilder result = new StringBuilder();
            bool first = true;
            foreach (var item in MyRules)
            {
                if (first)
                {
                    result.Append(item.ToString());
                    first = false;
                }
                else
                {
                    result.Append(string.Format(" && {0}", item.ToString()));
                }
            }
            return result.ToString();
        }
        /// <summary>
        /// Adatforrás letöltése id alapján.
        /// </summary>
        /// <param name="id"></param>
        private void LoadDataById(Guid id)
        {
            Result eBeadvanyResult = GetEBeadvany(id);
            Result eBeadvanyRCsatolanyokResultGet = GetEBeadvanyCsatolmany(id);

            MyData = new DataSet();
            DataTable dtEBeadv = eBeadvanyResult.Ds.Tables[0].Copy();
            dtEBeadv.TableName = "EREC_eBeadvanyok";
            MyData.Tables.Add(dtEBeadv);

            DataTable dtEBeadvCsat = eBeadvanyRCsatolanyokResultGet.Ds.Tables[0].Copy();
            dtEBeadvCsat.TableName = "EREC_eBeadvanyCsatolmanyok";
            MyData.Tables.Add(dtEBeadvCsat);
        }
        /// <summary>
        /// Szabalyok betöltése
        /// </summary>
        private void LoadRules()
        {
            Result resultGetRules = GetRules();

            if (resultGetRules == null || resultGetRules.Ds.Tables.Count < 1 || resultGetRules.Ds.Tables[0].Rows.Count < 1)
                return;

            EKFRuleClass rule;
            int sorrend = 0;
            foreach (DataRow item in resultGetRules.Ds.Tables[0].Rows)
            {
                rule = EKFManager.EKFDeSerialize(item["Szabaly"].ToString());
                if (rule == null)
                {
                    AddMessage("EKF: Hiba a szabály beolvasásakor !");
                    continue;
                }
                rule.NameProperty = item["Nev"].ToString();
                //if (int.TryParse(item["Sorrend"].ToString(), out sorrend))
                MyRules.Add(sorrend, rule);
                ++sorrend;
            }
        }

        /// <summary>
        /// Folyamat végrehajtása
        /// </summary>
        /// <returns></returns>
        public bool Start()
        {
            OccuredMessages.Clear();
            if (MyRules == null || MyRules.Count < 1)
            {
                AddMessage("EKF: Nincs szabály. ");
                AddMessage("________________________________________________________________");
                return true;
            }
            if (MyData == null || MyData.Tables.Count < 1 || MyData.Tables[0].Rows.Count < 1)
            {
                AddMessage("EKF: Nincs adat.");
                AddMessage("________________________________________________________________");
                return true;
            }

            #region EVAL CONDITIONS
            int idx = 1;
            foreach (var tkey in MyRules)
            {
                AddMessage("________________________________________________________________");
                AddMessage(string.Format("({0} - {1})", idx, tkey.Value.NameProperty));
                AddMessage("______________________________");

                ++idx;
                //Logger.Debug(string.Format("EKF.Rule Indul: ID={0}", tkey.Value.IdProperty));
                EKFRuleClass rule = tkey.Value;
                try
                {
                    #region EVAL CONDITIONS
                    AddMessage("...............................");
                    AddMessage("EKF: Feltetel kiertekeles indul");
                    AddMessage("...............................");

                    if ((rule.Conditions == null || rule.Conditions.Count < 1) && (rule.ConditionsForXML == null || rule.ConditionsForXML.Count < 1))
                    {
                        AddMessage("EKF: Feltetelek hiányosak - a művelet megszakítva !");
                        AddMessage("________________________________________________________________");
                        return false;
                    }

                    if ((rule.Conditions != null && rule.Conditions.Count > 0 && rule.ConditionsForXML != null && rule.ConditionsForXML.Count > 0))
                    {
                        if (!this.EvalConditions(rule, MyData))
                        {
                            AddMessage("EKF: Adatbázis Feltetel nem illesztheto az uzenetre !");
                            AddMessage("________________________________________________________________");
                            continue;
                        }
                        if (!this.EvalConditionsForXML(rule, InDataId.ToString()))
                        {
                            AddMessage("EKF: Xml csatolmány Feltetel sem illesztheto az uzenetre !");
                            AddMessage("________________________________________________________________");
                            continue;
                        }
                    }
                    else if (rule.Conditions != null && rule.Conditions.Count > 0)
                    {
                        if (!this.EvalConditions(rule, MyData))
                        {
                            AddMessage("EKF: Adatbázis Feltetel nem illesztheto az uzenetre !");
                            AddMessage("________________________________________________________________");
                            continue;
                        }
                    }
                    else if (rule.ConditionsForXML != null && rule.ConditionsForXML.Count > 0)
                    {
                        if (!this.EvalConditionsForXML(rule, InDataId.ToString()))
                        {
                            AddMessage("EKF: Xml csatolmány Feltetel nem illesztheto az uzenetre !");
                            AddMessage("________________________________________________________________");
                            continue;
                        }
                    }

                    #endregion EVAL CONDITIONS

                    AddMessage(".....................");
                    AddMessage("EKF: Fo muvelet Indul");
                    AddMessage(".....................");
                    #region EXEC OPERATIONS
                    //LZS - BLG_11345
                    if (rule.Operation != null)
                    {
                        ExecuteMainOperation(rule);
                    }
                    else
                    {
                        AddMessage(".....................");
                        AddMessage("EKF: Nincs főművelet.");
                        AddMessage(".....................");
                    }
                    #endregion EXEC OPERATIONS

                    AddMessage(".....................");
                    AddMessage("EKF: Uto muvelet Indul");
                    AddMessage(".....................");
                    #region EXEC POST OPERATIONS
                    ExecutePostOperations(rule);
                    #endregion EXEC POST OPERATIONS

                    #region REFRESH JOGOSULTAK
                    AddMessage(".....................");
                    AddMessage("EKF: Refresh Jogosultak Indul");
                    AddMessage(".....................");
                    ExecuteRefreshJogosultakFromUcm(rule);
                    #endregion

                    AddEUzenetNaplo(rule);
                    AddMessage(".....................");
                    AddMessage("EKF: Szabály lefuttatva.");
                    AddMessage("________________________________________________________________");
                }
                catch (ResultException exc)
                {
                    string translatedMesage = ResultError.ErrorMessageCodeToDetailedText(exc.ErrorMessage);
                    string msg = string.Format("EKF: {0} {1}", exc.ErrorCode, translatedMesage);

                    AddMessage(msg);
                    Logger.Error(msg, exc);
                }
                catch (Exception exc)
                {
                    AddMessage("EKF: " + exc.Message);
                    Logger.Error("EKF:", exc);
                }
                AddMessage(Environment.NewLine);

                #region RELOAD/REFRESH DATA
                try
                {
                    LoadDataById(InDataId);
                    if (MyData == null || MyData.Tables.Count < 1 || MyData.Tables[0].Rows.Count < 1)
                    {
                        AddMessage("EKF: Nincs több adat.");
                        return true;
                    }
                }
                catch (Exception exc)
                {
                    AddMessage("EKF: " + exc.Message);
                    Logger.Error("EKF:", exc);
                }
                #endregion

                if (rule.ParametersProperty.StopRuleProperty.HasValue && rule.ParametersProperty.StopRuleProperty.Value)
                {
                    AddMessage("EKF: Megallito szabaly");
                    AddMessage("________________________________________________________________");
                    break;
                }

                AddMessage("________________________________________________________________");
                AddMessage(Environment.NewLine);
            }

            #endregion EVAL CONDITIONS
            return true;
        }

        /// <summary>
        /// ExecuteMainOperation
        /// </summary>
        /// <param name="rule"></param>
        private void ExecuteMainOperation(EKFRuleClass rule)
        {
            Logger.Debug("EKF: MainOperations Indul.");
            Result resultOperation = null;

            var resultOperationValue = default(bool);

            resultOperationValue = OperationExecute(rule.Operation, rule, MyData, out resultOperation);

            if (!resultOperationValue)
            {
                string msg = string.Format("EKF: {0} {1}", rule.Operation.ToString(), (resultOperation != null && resultOperation.ErrorDetail != null) ? resultOperation.ErrorDetail.Message.ToString() : string.Empty);
                Logger.Error(msg);
                AddMessage(msg);

                if (!string.IsNullOrEmpty(resultOperation.ErrorCode))
                    throw new ResultException(resultOperation);
            }
        }

        /// <summary>
        /// ExecuteAfterOperations
        /// </summary>
        /// <param name="rule"></param>
        private void ExecutePostOperations(EKFRuleClass rule)
        {
            Logger.Debug("EKF: PostOperations Indul.");
            Result resultPostOperation = null;
            if (rule.PostOperations != null && rule.PostOperations.Count > 0)
            {
                foreach (var postOperation in rule.PostOperations)
                {
                    resultPostOperation = null;
                    try
                    {
                        var resultPostOperationValue = default(bool);

                        resultPostOperationValue = OperationExecute(postOperation, rule, MyData, out resultPostOperation);
                        if (!resultPostOperationValue)
                        {
                            string msg = string.Format("EKF: {0} {1}", postOperation, (resultPostOperation != null && resultPostOperation.ErrorDetail != null) ? resultPostOperation.ErrorDetail.Message.ToString() : string.Empty);
                            Logger.Error(msg);
                            AddMessage(msg);

                            if (!string.IsNullOrEmpty(resultPostOperation.ErrorCode))
                                throw new ResultException(resultPostOperation);
                        }
                    }
                    catch (Exception exc)
                    {
                        string msg = string.Format("EKF: {0}, {1}", postOperation.OperationName, exc.Message);
                        AddMessage(msg);
                        Logger.Error("EKF: ", exc);
                    }
                }
            }
        }

        /// <summary>
        /// Művelet végrehajtása.
        /// Objektumok konvertálásával.
        /// Az új osztályokat majd fel kell venni a cast-olás miatt.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="inRule"></param>
        /// <param name="inData"></param>
        /// <param name="outResultOperation"></param>
        /// <returns></returns>
        private bool OperationExecute(EKFBaseOperation item, EKFRuleClass inRule, DataSet inData, out Result outResultOperation)
        {
            outResultOperation = new Result();
            var resultPostOperationValue = default(bool);
            if (item.TypeFullName.Contains(new EKFMainOperationIktatas().GetType().FullName))
            {
                EKFMainOperationIktatas i = new EKFMainOperationIktatas();
                i.TypeFullName = item.TypeFullName;
                i.ParameterProperty = item.ParameterProperty;
                i.OperationType = item.OperationType;
                try
                {
                    resultPostOperationValue = i.Execute(inRule, inData, out outResultOperation);
                }
                catch (Exception exc)
                {
                    AddMessage(exc);
                    throw;
                }
                finally
                {
                    if (i.MessagesEKF != null && !string.IsNullOrEmpty(i.MessagesEKF.ToString()))
                    {
                        AddMessage("EKF.IKTATAS: " + i.MessagesEKF.ToString());
                    }
                }
            }
            else if (item.TypeFullName.Contains(new EKFMainOperationErkeztetes().GetType().FullName))
            {
                EKFMainOperationErkeztetes i = new EKFMainOperationErkeztetes();
                i.TypeFullName = item.TypeFullName;
                i.ParameterProperty = item.ParameterProperty;
                i.OperationType = item.OperationType;
                try
                {
                    resultPostOperationValue = i.Execute(inRule, inData, out outResultOperation);
                }
                catch (Exception exc)
                {
                    AddMessage(exc);
                    throw;
                }
                finally
                {
                    if (i.MessagesEKF != null && !string.IsNullOrEmpty(i.MessagesEKF.ToString()))
                    {
                        AddMessage("EKF.ERKEZTETES: " + i.MessagesEKF.ToString());
                    }
                }
            }
            //else if (item.TypeFullName.Contains(new EKFMainOperationIrattaroz().GetType().FullName))
            //{
            //    EKFMainOperationIrattaroz i = new EKFMainOperationIrattaroz();
            //    i.TypeFullName = item.TypeFullName;
            //    i.ParameterProperty = item.ParameterProperty;
            //    i.OperationType = item.OperationType;
            //    resultPostOperationValue = i.Execute(inRule, inData, out outResultOperation);
            //}
            else if (item.TypeFullName.Contains(new EKFOperationIktatasFoSzammal().GetType().FullName))
            {
                EKFOperationIktatasFoSzammal i = new EKFOperationIktatasFoSzammal();
                i.TypeFullName = item.TypeFullName;
                i.ParameterProperty = item.ParameterProperty;
                i.OperationType = item.OperationType;
                try
                {
                    resultPostOperationValue = i.Execute(inRule, inData, out outResultOperation);
                }
                catch (Exception exc)
                {
                    AddMessage(exc);
                    throw;
                }
                finally
                {
                    if (i.MessagesEKF != null && !string.IsNullOrEmpty(i.MessagesEKF.ToString()))
                    {
                        AddMessage("EKF.IKTATASFOSZAMMAL: " + i.MessagesEKF.ToString());
                    }
                }
            }

            else if (item.TypeFullName.Contains(new EKFPostOperationAtadas().GetType().FullName))
            {
                EKFPostOperationAtadas i = new EKFPostOperationAtadas();
                i.TypeFullName = item.TypeFullName;
                i.ParameterProperty = item.ParameterProperty;
                i.OperationType = item.OperationType;
                try
                {
                    resultPostOperationValue = i.Execute(inRule, inData, out outResultOperation);
                }
                catch (Exception exc)
                {
                    AddMessage(exc);
                    throw;
                }
                finally
                {
                    if (i.MessagesEKF != null && !string.IsNullOrEmpty(i.MessagesEKF.ToString()))
                    {
                        AddMessage("EKF.ATADAS: " + i.MessagesEKF.ToString());
                    }
                }
            }
            else if (item.TypeFullName.Contains(new EKFPostOperationErtesites().GetType().FullName))
            {
                EKFPostOperationErtesites i = new EKFPostOperationErtesites();
                i.TypeFullName = item.TypeFullName;
                i.ParameterProperty = item.ParameterProperty;
                i.OperationType = item.OperationType;
                try
                {
                    resultPostOperationValue = i.Execute(inRule, inData, out outResultOperation);
                }
                catch (Exception exc)
                {
                    AddMessage(exc);
                    throw;
                }
                finally
                {
                    if (i.MessagesEKF != null && !string.IsNullOrEmpty(i.MessagesEKF.ToString()))
                    {
                        AddMessage("EKF.ERTESITES: " + i.MessagesEKF.ToString());
                    }
                }
            }
            else if (item.TypeFullName.Contains(new EKFPostOperationSzignalas().GetType().FullName))
            {
                EKFPostOperationSzignalas i = new EKFPostOperationSzignalas();
                i.TypeFullName = item.TypeFullName;
                i.ParameterProperty = item.ParameterProperty;
                i.OperationType = item.OperationType;
                try
                {
                    resultPostOperationValue = i.Execute(inRule, inData, out outResultOperation);
                }
                catch (Exception exc)
                {
                    AddMessage(exc);
                    throw;
                }
                finally
                {
                    if (i.MessagesEKF != null && !string.IsNullOrEmpty(i.MessagesEKF.ToString()))
                    {
                        AddMessage("EKF.SZIGNALAS: " + i.MessagesEKF.ToString());
                    }
                }
            }
            else if (item.TypeFullName.Contains(new EKFPostOperationAutoValasz().GetType().FullName))
            {
                EKFPostOperationAutoValasz i = new EKFPostOperationAutoValasz();
                i.TypeFullName = item.TypeFullName;
                i.ParameterProperty = item.ParameterProperty;
                i.OperationType = item.OperationType;
                try
                {
                    resultPostOperationValue = i.Execute(inRule, inData, out outResultOperation);
                }
                catch (Exception exc)
                {
                    AddMessage(exc);
                    throw;
                }
                finally
                {
                    if (i.MessagesEKF != null && !string.IsNullOrEmpty(i.MessagesEKF.ToString()))
                    {
                        AddMessage("EKF.AUTOVALASZ: " + i.MessagesEKF.ToString());
                    }
                }
            }
            else if (item.TypeFullName.Contains(new EKFPostOperationElintezetteNyilvanitas().GetType().FullName))
            {
                EKFPostOperationElintezetteNyilvanitas i = new EKFPostOperationElintezetteNyilvanitas();
                i.TypeFullName = item.TypeFullName;
                i.ParameterProperty = item.ParameterProperty;
                i.OperationType = item.OperationType;
                try
                {
                    resultPostOperationValue = i.Execute(inRule, inData, out outResultOperation);
                }
                catch (Exception exc)
                {
                    AddMessage(exc);
                    throw;
                }
                finally
                {
                    if (i.MessagesEKF != null && !string.IsNullOrEmpty(i.MessagesEKF.ToString()))
                    {
                        AddMessage("EKF.ELINTEZETTENYILVANITAS: " + i.MessagesEKF.ToString());
                    }
                }
            }
            else if (item.TypeFullName.Contains(new EKFPostOperationJogosultak().GetType().FullName))
            {
                EKFPostOperationJogosultak i = new EKFPostOperationJogosultak();
                i.TypeFullName = item.TypeFullName;
                i.ParameterProperty = item.ParameterProperty;
                i.OperationType = item.OperationType;
                try
                {
                    resultPostOperationValue = i.Execute(inRule, inData, out outResultOperation);
                }
                catch (Exception exc)
                {
                    AddMessage(exc);
                    throw;
                }
                finally
                {
                    if (i.MessagesEKF != null && !string.IsNullOrEmpty(i.MessagesEKF.ToString()))
                    {
                        AddMessage("EKF.JOGOSULTAK: " + i.MessagesEKF.ToString());
                    }
                }
            }
            else if (item.TypeFullName.Contains(new EKFPostOperationTargyszavak().GetType().FullName))
            {
                EKFPostOperationTargyszavak i = new EKFPostOperationTargyszavak();
                i.TypeFullName = item.TypeFullName;
                i.ParameterProperty = item.ParameterProperty;
                i.OperationType = item.OperationType;
                try
                {
                    resultPostOperationValue = i.Execute(inRule, inData, out outResultOperation);
                }
                catch (Exception exc)
                {
                    AddMessage(exc);
                    throw;
                }
                finally
                {
                    if (i.MessagesEKF != null && !string.IsNullOrEmpty(i.MessagesEKF.ToString()))
                    {
                        AddMessage("EKF.TARGYSZAVAK: " + i.MessagesEKF.ToString());
                    }
                }
            }
            else if (item.TypeFullName.Contains(new EKFPostOperationCelRendszer().GetType().FullName))
            {
                EKFPostOperationCelRendszer i = new EKFPostOperationCelRendszer();
                i.TypeFullName = item.TypeFullName;
                i.ParameterProperty = item.ParameterProperty;
                i.OperationType = item.OperationType;
                try
                {
                    resultPostOperationValue = i.Execute(inRule, inData, out outResultOperation);
                }
                catch (Exception exc)
                {
                    AddMessage(exc);
                    throw;
                }
                finally
                {
                    if (i.MessagesEKF != null && !string.IsNullOrEmpty(i.MessagesEKF.ToString()))
                    {
                        AddMessage("EKF.CÉLRENDSZER: " + i.MessagesEKF.ToString());
                    }
                }
            }
            else
            {
                try
                {
                    AddMessage(string.Format("EKF.MUVELET.ISMERETLEN", item.TypeFullName, item.OperationType));
                    resultPostOperationValue = item.Execute(inRule, inData, out outResultOperation);
                }
                catch (Exception exc)
                {
                    AddMessage(exc);
                    throw;
                }
                finally
                {
                    if (item.MessagesEKF != null && !string.IsNullOrEmpty(item.MessagesEKF.ToString()))
                    {
                        AddMessage("EKF: " + item.MessagesEKF.ToString());
                    }
                }
            }
            return resultPostOperationValue;
        }

        #region HELPER
        /// <summary>
        /// EUzenet feldolgozás naplózása
        /// </summary>
        /// <param name="rule"></param>
        private void AddEUzenetNaplo(EKFRuleClass rule)
        {
            INT_AutoEUzenetSzabalyNaplo naplo = new INT_AutoEUzenetSzabalyNaplo();
            naplo.Id = Guid.NewGuid().ToString();
            naplo.ESzabalyId = rule.IdProperty.ToString();
            naplo.EUzenetId = MyData.Tables[0].Rows[0]["Id"].ToString();
            naplo.EUzenetTipusId = rule.ParametersProperty.TypeProperty.ToString();
            naplo.Megjegyzes = string.Empty;
            AddEUzenetNaplo(naplo);
        }
        #endregion
        /// <summary>
        /// ExecuteRefreshJogosultakFromUcm
        /// </summary>
        /// <param name="rule"></param>
        /// <returns></returns>
        private bool ExecuteRefreshJogosultakFromUcm(EKFRuleClass rule)
        {
            try
            {
                ExecParam xparam = rule.GetExecParam();

                string documentStoreType = Contentum.eUtility.Rendszerparameterek.Get(xparam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);
                if (string.IsNullOrEmpty(documentStoreType))
                    return true;

                Logger.Debug(String.Format("documentStoreType={0}", documentStoreType));
                if (documentStoreType != Contentum.eUtility.Constants.DocumentStoreType.UCM)
                    return true;

                if (InDataId == null)
                    return true;

                LoadDataById(InDataId);

                EREC_eBeadvanyok resultEBeadvany = new EKFManager(xparam).GetEBeadvanyByDataSet(MyData);

                if (resultEBeadvany == null || string.IsNullOrEmpty(resultEBeadvany.IraIrat_Id))
                    return true;

                UCMDocumentService serviceDoc = eDocumentService.ServiceFactory.GetUCMDocumentService();
                Result resultRefresh = serviceDoc.RefreshJogosultakFromUcm(xparam, resultEBeadvany.IraIrat_Id);
                if (!string.IsNullOrEmpty(resultRefresh.ErrorCode))
                {
                    AddMessage(resultRefresh);
                    throw new ResultException(resultRefresh);
                }

                return true;
            }
            catch (Exception exc)
            {
                string msg = string.Format("EKF: {0}, {1}", "ExecuteRefreshJogosultakFromUcm", exc.Message);
                AddMessage(msg);
                Logger.Error("EKF: ", exc);
                return false;
            }
        }
    }
}
