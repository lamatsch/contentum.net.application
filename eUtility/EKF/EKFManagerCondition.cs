﻿using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Xml;

namespace Contentum.eUtility.EKF
{
    public partial class EKFManager
    {
        /// <summary>
        /// Feltételek kiértékelése
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public bool EvalConditions(EKFRuleClass rule, DataSet inputData)
        {
            bool result = true;

            foreach (EKFCondition condition in rule.Conditions)
            {
                Type fieldType;
                string fieldValue = GetFieldValueFromDataSet(condition, inputData, out fieldType);
                result &= Eval(fieldValue, fieldType, condition);
                if (!result)
                    return false;
            }
            return result;
        }
        /// <summary>
        /// Feltétel kiértékelése
        /// </summary>
        /// <param name="leftValue"></param>
        /// <param name="type"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        private bool Eval(string leftValue, Type type, EKFCondition condition)
        {
            string conditionValue = condition.ConditionProperty;
            object leftValueObj;
            object rightValueObj;

            switch (condition.OperatorProperty)
            {
                case EnumEKFConditionOperator.NULL_VAGY_URES:
                    return string.IsNullOrEmpty(leftValue);
                case EnumEKFConditionOperator.KISEBB:
                    leftValueObj = CheckAndGetValueType(leftValue, type);
                    rightValueObj = CheckAndGetValueType(conditionValue, type);
                    #region TYPES
                    if (type == typeof(int))
                    {
                        return (int)leftValueObj < (int)rightValueObj;
                    }
                    else if (type == typeof(byte))
                    {
                        return (byte)leftValueObj < (byte)rightValueObj;
                    }
                    else if (type == typeof(float))
                    {
                        return (float)leftValueObj < (float)rightValueObj;
                    }
                    else if (type == typeof(double))
                    {
                        return (double)leftValueObj < (double)rightValueObj;
                    }
                    else if (type == typeof(DateTime))
                    {
                        return (DateTime)leftValueObj < (DateTime)rightValueObj;
                    }
                    else
                        throw new NotImplementedException("Nem támogatott adat típus a kisebb operátorra" + type.Name);
                #endregion
                case EnumEKFConditionOperator.NAGYOBB:
                    leftValueObj = CheckAndGetValueType(leftValue, type);
                    rightValueObj = CheckAndGetValueType(conditionValue, type);
                    #region TYPES
                    if (type == typeof(int))
                    {
                        return (int)leftValueObj > (int)rightValueObj;
                    }
                    else if (type == typeof(byte))
                    {
                        return (byte)leftValueObj > (byte)rightValueObj;
                    }
                    else if (type == typeof(float))
                    {
                        return (float)leftValueObj > (float)rightValueObj;
                    }
                    else if (type == typeof(double))
                    {
                        return (double)leftValueObj > (double)rightValueObj;
                    }
                    else if (type == typeof(DateTime))
                    {
                        return (DateTime)leftValueObj > (DateTime)rightValueObj;
                    }
                    else
                        throw new NotImplementedException("Nem támogatott adat típus a nagyobb operátorra: " + type.Name);
                #endregion
                case EnumEKFConditionOperator.KISEBB_EGYENLO:
                    leftValueObj = CheckAndGetValueType(leftValue, type);
                    rightValueObj = CheckAndGetValueType(conditionValue, type);
                    #region TYPES
                    if (type == typeof(int))
                    {
                        return (int)leftValueObj <= (int)rightValueObj;
                    }
                    else if (type == typeof(byte))
                    {
                        return (byte)leftValueObj <= (byte)rightValueObj;
                    }
                    else if (type == typeof(float))
                    {
                        return (float)leftValueObj <= (float)rightValueObj;
                    }
                    else if (type == typeof(double))
                    {
                        return (double)leftValueObj <= (double)rightValueObj;
                    }
                    else if (type == typeof(DateTime))
                    {
                        return (DateTime)leftValueObj <= (DateTime)rightValueObj;
                    }
                    else
                        throw new NotImplementedException("Nem támogatott adat típus a kisebb egyenlő operátorra " + type.Name);
                #endregion
                case EnumEKFConditionOperator.NAGYOBB_EGYENLO:
                    leftValueObj = CheckAndGetValueType(leftValue, type);
                    rightValueObj = CheckAndGetValueType(conditionValue, type);
                    #region TYPES
                    if (type == typeof(int))
                    {
                        return (int)leftValueObj >= (int)rightValueObj;
                    }
                    else if (type == typeof(byte))
                    {
                        return (byte)leftValueObj >= (byte)rightValueObj;
                    }
                    else if (type == typeof(float))
                    {
                        return (float)leftValueObj >= (float)rightValueObj;
                    }
                    else if (type == typeof(double))
                    {
                        return (double)leftValueObj >= (double)rightValueObj;
                    }
                    else if (type == typeof(DateTime))
                    {
                        return (DateTime)leftValueObj >= (DateTime)rightValueObj;
                    }
                    else
                        throw new NotImplementedException("Nem támogatott adat típus a nagyobb operátorra: " + type.Name);
                #endregion
                case EnumEKFConditionOperator.EGYENLO:
                    return leftValue == conditionValue ? true : false;
                case EnumEKFConditionOperator.NEM_EGYENLO:
                    return leftValue != conditionValue ? true : false;
                case EnumEKFConditionOperator.KEZDODIK:
                    return leftValue.StartsWith(conditionValue) ? true : false;
                case EnumEKFConditionOperator.VEGZODIK:
                    return leftValue.EndsWith(conditionValue) ? true : false;
                case EnumEKFConditionOperator.TARTALMAZZA:
                    return leftValue.Contains(conditionValue) ? true : false;
                case EnumEKFConditionOperator.CSILLAGOS:
                    bool result = true;
                    string[] splittedConditionValues = conditionValue.Split(EKFConstants.ConditionValueStarSplitSeparatorArray, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string sConditionValue in splittedConditionValues)
                    {
                        result &= leftValue.Contains(sConditionValue);
                        if (!result)
                            return false;
                    }
                    return result;
                case EnumEKFConditionOperator.BENNE_VAN:
                    string[] splittedRightValues = conditionValue.Split(EKFConstants.ConditionValueSplitSeparatorArray, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string sRightValue in splittedRightValues)
                    {
                        if (leftValue.Equals(sRightValue))
                            return true;
                    }
                    return false;
                case EnumEKFConditionOperator.REGULARIS_KIFEJEZES:
                    Regex rx = new Regex(conditionValue, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                    return rx.IsMatch(leftValue);
                default:
                    throw new NotImplementedException("Nem támogatott operátor " + condition.OperatorProperty);
            }
            //return false;
        }

        //public bool Kisebb<T>(T value, T other) where T : IComparable
        //{
        //    return value.CompareTo(other) < 0;
        //}

        /// <summary>
        /// Típus ellenőrzés és konverzió
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private static object CheckAndGetValueType(string value, Type type)
        {
            object result = null;
            if (type == typeof(byte))
            {
                byte val;
                if (byte.TryParse(value, out val))
                    result = val;
                else
                    throw new Exception(string.Format("Sikertelen a(z) {0} érték konvertálása {1} típussá", value, type.ToString()));
            }
            if (type == typeof(int))
            {
                int val;
                if (int.TryParse(value, out val))
                    result = val;
                else
                    throw new Exception(string.Format("Sikertelen a(z) {0} érték konvertálása {1} típussá", value, type.ToString()));
            }
            else if (type == typeof(double))
            {
                double val;
                if (double.TryParse(value, out val))
                    result = val;
                else
                    throw new Exception(string.Format("Sikertelen a(z) {0} érték konvertálása {1} típussá", value, type.ToString()));
            }
            else if (type == typeof(float))
            {
                float val;
                if (float.TryParse(value, out val))
                    result = val;
                else
                    throw new Exception(string.Format("Sikertelen a(z) {0} érték konvertálása {1} típussá", value, type.ToString()));
            }
            else if (type == typeof(DateTime))
            {
                DateTime val;
                if (DateTime.TryParse(value, out val))
                    result = val;
                else
                    throw new Exception(string.Format("Sikertelen a(z) {0} érték konvertálása {1} típussá", value, type.ToString()));
            }
            else if (type == typeof(String))
            {
                result = value.ToString();
            }
            else if (type == typeof(decimal))
            {
                decimal val;
                if (decimal.TryParse(value, out val))
                    result = val;
                else
                    throw new Exception(string.Format("Sikertelen a(z) {0} érték konvertálása {1} típussá", value, type.ToString()));
            }
            else if (type == typeof(bool))
            {
                bool val;
                if (bool.TryParse(value, out val))
                    result = val;
                else
                    throw new Exception(string.Format("Sikertelen a(z) {0} érték konvertálása {1} típussá", value, type.ToString()));
            }
            else if (type == typeof(Boolean))
            {
                Boolean val;
                if (Boolean.TryParse(value, out val))
                    result = val;
                else
                    throw new Exception(string.Format("Sikertelen a(z) {0} érték konvertálása {1} típussá", value, type.ToString()));
            }
            else if (type == typeof(Byte))
            {
                Byte val;
                if (Byte.TryParse(value, out val))
                    result = val;
                else
                    throw new Exception(string.Format("Sikertelen a(z) {0} érték konvertálása {1} típussá", value, type.ToString()));
            }
            else if (type == typeof(Int16))
            {
                Int16 val;
                if (Int16.TryParse(value, out val))
                    result = val;
                else
                    throw new Exception(string.Format("Sikertelen a(z) {0} érték konvertálása {1} típussá", value, type.ToString()));
            }
            else if (type == typeof(Single))
            {
                Single val;
                if (Single.TryParse(value, out val))
                    result = val;
                else
                    throw new Exception(string.Format("Sikertelen a(z) {0} érték konvertálása {1} típussá", value, type.ToString()));
            }
            else
                throw new NotImplementedException("Adott típus nem engedélyezett " + type.Name);

            return result;
        }
        /// <summary>
        /// Érték kinyerése a datasetből
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="inputData"></param>
        /// <param name="outFieldType"></param>
        /// <returns></returns>
        private string GetFieldValueFromDataSet(EKFCondition condition, DataSet inputData_, out Type outFieldType)
        {
            outFieldType = typeof(string);
            string result = null;

            string[] splitField = SplitField(condition.FieldProperty);
            if (splitField == null || splitField.Length < 1)
                throw new System.Exception("Hiányzik a mező leíró");

            #region CSAK OSZLOP LEÍRÓ VAN
            if (splitField.Length == 1)
            {
                result = EKFManager.GetDataSetValue(inputData_, splitField[0]);
                outFieldType = EKFManager.GetDataSetValueType(inputData_, splitField[0]);
            }
            #endregion

            #region DATATABLE ÉS OSZLOP LEÍRÓ IS VAN
            else if (splitField.Length == 2)
            {
                EKFConstants.EnumDataTableNames enumTableName = GetEnumTableName(splitField[0]);
                result = EKFManager.GetDataSetValue(inputData_, enumTableName, splitField[1]);
                outFieldType = EKFManager.GetDataSetValueType(inputData_, enumTableName, splitField[1]);
            }
            else
            {
                throw new RowNotInTableException();
            }
            #endregion

            return result;
        }

        private string GetXmlElement(string xmlContent, string element)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlContent);

            XmlNodeList elemList = doc.GetElementsByTagName(element);

            if (elemList == null || element.Length < 1)
                return null;

            return elemList[0].Value;
        }
        #region HELPER
        private string[] SplitField(string field)
        {
            return field.Split(EKFConstants.ConditionFieldSplitSeparator, System.StringSplitOptions.RemoveEmptyEntries);
        }
        #endregion

    }

}
