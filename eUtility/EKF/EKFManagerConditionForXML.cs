﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility.EKF;
using Contentum.eUtility.EKF.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Contentum.eUtility.EKF
{
    public partial class EKFManager
    {
        /// <summary>
        /// Feltételek kiértékelése
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public bool EvalConditionsForXML(EKFRuleClass rule, string eBeadvanyId)
        {
            if (rule == null || rule.ConditionsForXML == null || rule.ConditionsForXML.Count < 1)
            {
                return false;
            }

            List<EKF_FileContentModel> XmlFileContents = GetXmlDocuments(rule.GetExecParam(), eBeadvanyId, EKFConstants.CONST_LIKE_FORMULA_XML_FILES, Query.Operators.like);
            if (XmlFileContents == null || XmlFileContents.Count < 1)
            {
                return false;
            }

            List<EKF_FileContentModel> XmlFileContentsFiltered = null;
            foreach (EKFConditionForXML condition in rule.ConditionsForXML)
            {
                XmlFileContentsFiltered = !string.IsNullOrEmpty(condition.FileNameProperty)
                    ? XmlFileContents.Where(f => f.FileName == condition.FileNameProperty).ToList()
                    : XmlFileContents;

                if (XmlFileContentsFiltered == null || XmlFileContentsFiltered.Count < 1)
                {
                    continue;
                }

                string tmpXmlTagValue;
                for (int i = 0; i < XmlFileContentsFiltered.Count; i++)
                {
                    tmpXmlTagValue = GetContentFromXml(XmlFileContentsFiltered[i].Content, condition.XPathProperty);
                    if (string.IsNullOrEmpty(tmpXmlTagValue))
                    {
                        continue;
                    }

                    bool resultEval = EvalInXML(tmpXmlTagValue, typeof(double), condition);
                    if (resultEval)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// Feltétel kiértékelése
        /// </summary>
        /// <param name="leftValue"></param>
        /// <param name="type"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        private bool EvalInXML(string leftValue, Type type, EKFConditionForXML condition)
        {
            string value = condition.ValueProperty.ToString();
            object leftValueObj;
            object rightValueObj;

            switch (condition.OperatorProperty)
            {
                case EnumEKFConditionOperatorForXML.KISEBB:
                    leftValueObj = CheckAndGetValueType(leftValue, type);
                    rightValueObj = CheckAndGetValueType(value, type);
                    #region TYPES
                    if (type == typeof(int))
                    {
                        return (int)leftValueObj < (int)rightValueObj;
                    }
                    else if (type == typeof(byte))
                    {
                        return (byte)leftValueObj < (byte)rightValueObj;
                    }
                    else if (type == typeof(float))
                    {
                        return (float)leftValueObj < (float)rightValueObj;
                    }
                    else if (type == typeof(double))
                    {
                        return (double)leftValueObj < (double)rightValueObj;
                    }
                    else if (type == typeof(DateTime))
                    {
                        return (DateTime)leftValueObj < (DateTime)rightValueObj;
                    }
                    else
                    {
                        throw new NotImplementedException("Nem támogatott adat típus a kisebb operátorra" + type.Name);
                    }
                #endregion
                case EnumEKFConditionOperatorForXML.NAGYOBB:
                    leftValueObj = CheckAndGetValueType(leftValue, type);
                    rightValueObj = CheckAndGetValueType(value, type);
                    #region TYPES
                    if (type == typeof(int))
                    {
                        return (int)leftValueObj > (int)rightValueObj;
                    }
                    else if (type == typeof(byte))
                    {
                        return (byte)leftValueObj > (byte)rightValueObj;
                    }
                    else if (type == typeof(float))
                    {
                        return (float)leftValueObj > (float)rightValueObj;
                    }
                    else if (type == typeof(double))
                    {
                        return (double)leftValueObj > (double)rightValueObj;
                    }
                    else if (type == typeof(DateTime))
                    {
                        return (DateTime)leftValueObj > (DateTime)rightValueObj;
                    }
                    else
                    {
                        throw new NotImplementedException("Nem támogatott adat típus a nagyobb operátorra: " + type.Name);
                    }
                #endregion
                case EnumEKFConditionOperatorForXML.KISEBB_EGYENLO:
                    leftValueObj = CheckAndGetValueType(leftValue, type);
                    rightValueObj = CheckAndGetValueType(value, type);
                    #region TYPES
                    if (type == typeof(int))
                    {
                        return (int)leftValueObj <= (int)rightValueObj;
                    }
                    else if (type == typeof(byte))
                    {
                        return (byte)leftValueObj <= (byte)rightValueObj;
                    }
                    else if (type == typeof(float))
                    {
                        return (float)leftValueObj <= (float)rightValueObj;
                    }
                    else if (type == typeof(double))
                    {
                        return (double)leftValueObj <= (double)rightValueObj;
                    }
                    else if (type == typeof(DateTime))
                    {
                        return (DateTime)leftValueObj <= (DateTime)rightValueObj;
                    }
                    else
                    {
                        throw new NotImplementedException("Nem támogatott adat típus a kisebb egyenlő operátorra" + type.Name);
                    }
                #endregion
                case EnumEKFConditionOperatorForXML.NAGYOBB_EGYENLO:
                    leftValueObj = CheckAndGetValueType(leftValue, type);
                    rightValueObj = CheckAndGetValueType(value, type);
                    #region TYPES
                    if (type == typeof(int))
                    {
                        return (int)leftValueObj >= (int)rightValueObj;
                    }
                    else if (type == typeof(byte))
                    {
                        return (byte)leftValueObj >= (byte)rightValueObj;
                    }
                    else if (type == typeof(float))
                    {
                        return (float)leftValueObj >= (float)rightValueObj;
                    }
                    else if (type == typeof(double))
                    {
                        return (double)leftValueObj >= (double)rightValueObj;
                    }
                    else if (type == typeof(DateTime))
                    {
                        return (DateTime)leftValueObj >= (DateTime)rightValueObj;
                    }
                    else
                    {
                        throw new NotImplementedException("Nem támogatott adat típus a nagyobb operátorra" + type.Name);
                    }
                #endregion
                case EnumEKFConditionOperatorForXML.EGYENLO:
                    return leftValue == value ? true : false;
                case EnumEKFConditionOperatorForXML.NEM_EGYENLO:
                    return leftValue != value ? true : false;
                default:
                    throw new NotImplementedException("Nem támogatott operátor: " + condition.OperatorProperty);
            }
            //return false;
        }

        #region XML HELPER
        /// <summary>
        /// GetXmlDocuments
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="eBeadvanyId"></param>
        /// <param name="searchFajlNev"></param>
        /// <param name="searchFajlNevOperator"></param>
        /// <returns></returns>
        private List<EKF_FileContentModel> GetXmlDocuments(ExecParam execParam, string eBeadvanyId, string searchFajlNev, string searchFajlNevOperator)
        {
            List<EKF_FileContentModel> xmlDocuments = new List<EKF_FileContentModel>();

            eDocument.Service.KRT_DokumentumokService dokumentumokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            KRT_DokumentumokSearch dokumentumokSearch = new KRT_DokumentumokSearch();
            dokumentumokSearch.Id.Value = string.Format("select Dokumentum_Id from EREC_eBeadvanyCsatolmanyok where eBeadvany_Id = '{0}' and GETDATE() between EREC_eBeadvanyCsatolmanyok.ErvKezd and EREC_eBeadvanyCsatolmanyok.ErvVege", eBeadvanyId);
            dokumentumokSearch.Id.Operator = Query.Operators.inner;

            dokumentumokSearch.FajlNev.Value = searchFajlNev;
            dokumentumokSearch.FajlNev.Operator = searchFajlNevOperator;

            Result dokumentumokResult = dokumentumokService.GetAll(execParam, dokumentumokSearch);

            if (dokumentumokResult.IsError)
            {
                throw new ResultException(dokumentumokResult);
            }

            if (dokumentumokResult.Ds.Tables[0].Rows.Count == 0)
            {
                return null;
            }

            string externalLink;
            byte[] docContent;
            for (int i = 0; i < dokumentumokResult.Ds.Tables[0].Rows.Count; i++)
            {
                externalLink = dokumentumokResult.Ds.Tables[0].Rows[i]["External_Link"].ToString();
                if (string.IsNullOrEmpty(externalLink))
                {
                    continue;
                }

                try
                {
                    docContent = GetDocContent(externalLink);
                    if (docContent == null)
                    {
                        continue;
                    }

                    xmlDocuments.Add(new EKF_FileContentModel(dokumentumokResult.Ds.Tables[0].Rows[i]["External_Link"].ToString(), docContent));
                }
                catch (Exception exc)
                {
                    Logger.Error("EKFManagerTargyFormula.DownloadDocument External_Link:" + externalLink, exc);
                }
            }

            return xmlDocuments;
        }
        /// <summary>
        /// GetDocContent
        /// </summary>
        /// <param name="externalLink"></param>
        /// <returns></returns>
        private byte[] GetDocContent(string externalLink)
        {
            return EBeadvanyFormXmlHelper.GetDocContent(externalLink);
        }
        /// <summary>
        /// GetContentFromXml
        /// </summary>
        /// <param name="docContent"></param>
        /// <returns></returns>
        private string GetContentFromXml(byte[] docContent, string xPath)
        {
            return EBeadvanyFormXmlHelper.GetContentFromXml(docContent, xPath);
        }
        #endregion
    }
}
