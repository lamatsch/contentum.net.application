﻿using System;
using System.Data;

namespace Contentum.eUtility.EKF
{
    public partial class EKFManager
    {
        /// <summary>
        /// Visszaadja az értéket a datasetből
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="tableName"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public static string GetDataSetValue(DataSet ds, EKFConstants.EnumDataTableNames tableName, string column)
        {
            CheckDataSetValue(ds, tableName, column);

            return ds.Tables[tableName.ToString()].Rows[0][column].ToString();
        }

        /// <summary>
        /// Visszaadja az értéket a datasetből
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public static string GetDataSetValue(DataSet ds, string column)
        {
            CheckDataSetvalue(ds, column);

            return ds.Tables[0].Rows[0][column].ToString();
        }

        /// <summary>
        /// Visszaadja az értéket a datasetből
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="tableName"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public static Type GetDataSetValueType(DataSet ds, EKFConstants.EnumDataTableNames tableName, string column)
        {
            CheckDataSetValue(ds, tableName, column);

            return ds.Tables[tableName.ToString()].Columns[column].DataType;
        }
        /// <summary>
        /// Visszaadja az értéket a datasetből
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public static Type GetDataSetValueType(DataSet ds, string column)
        {
            CheckDataSetvalue(ds, column);

            return ds.Tables[0].Columns[column].DataType;
        }

        #region CHECK DATA SET VALUE
        /// <summary>
        /// DataSet-ben lévő érték ellenőrzése
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="tableName"></param>
        /// <param name="column"></param>
        private static void CheckDataSetValue(DataSet ds, EKFConstants.EnumDataTableNames tableName, string column)
        {
            if (ds == null)
                throw new Exception("DataSet üres !");
            if (ds.Tables.Count < 1)
                throw new Exception("DataTables üres !");
            if (ds.Tables[tableName.ToString()] == null)
                throw new Exception(string.Format("DataTables nem tartalmaz {0} táblát !", tableName.ToString()));
            if (ds.Tables[tableName.ToString()].Rows.Count < 1)
                throw new Exception(string.Format("DataTables {0} nem tartalmaz sorokat !", tableName.ToString()));
            if (!ds.Tables[tableName.ToString()].Columns.Contains(column))
                throw new Exception(string.Format("DataTables {0} nem tartalmaz {1} oszlopot !", tableName.ToString(), column));
        }
        /// <summary>
        /// DataSet-ben lévő érték ellenőrzése
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="column"></param>
        private static void CheckDataSetvalue(DataSet ds, string column)
        {
            if (ds == null)
                throw new Exception("DataSet üres !");
            if (ds.Tables.Count < 1)
                throw new Exception("DataTables üres !");
            if (ds.Tables[0] == null)
                throw new Exception(string.Format("DataTables nem tartalmaz {0}. táblát !", 0));
            if (ds.Tables[0].Rows.Count < 1)
                throw new Exception(string.Format("DataTables {0}. nem tartalmaz sorokat !", 0));
            if (!ds.Tables[0].Columns.Contains(column))
                throw new Exception(string.Format("DataTables {0}. nem tartalmaz {1} oszlopot !", 0, column));
        }

        #endregion
    }
}