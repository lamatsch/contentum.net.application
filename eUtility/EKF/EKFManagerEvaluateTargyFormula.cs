﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility.EKF.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Contentum.eUtility.EKF
{
    /// <summary>
    /// EKF Tárgy meghatározása formula alapján 
    /// </summary>
    public partial class EKFManagerEvaluateTargyFormula
    {
        #region PROPERTIES
        private EKFRuleClass Rule;
        private string EBeadvanyId;
        private DataSet EBeadvanyDataSet;

        private string TargyFormulaXPath;
        //private string TargyFormulaConstant;
        private List<EKF_ComplexTargyModel> TargyFormulaComplexModel;
        private EKF_DefaultTargyModel TargyFormulaDefaultModel;
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="eBeadvanyDataSet"></param>
        public EKFManagerEvaluateTargyFormula(string eBeadvanyId, EKFRuleClass rule, DataSet eBeadvanyDataSet)
        {
            if (string.IsNullOrEmpty(rule.IdProperty.ToString()))
                throw new Exception("EKF targy init: Érvénytelen e-beadvany paraméter !");

            if (eBeadvanyDataSet == null || eBeadvanyDataSet.Tables.Count < 1 || eBeadvanyDataSet.Tables[0].Rows.Count < 1)
                throw new Exception("EKF targy init: Érvénytelen e-beadvany adatsor !");

            EBeadvanyId = eBeadvanyId;
            Rule = rule;
            EBeadvanyDataSet = eBeadvanyDataSet;

            TargyFormulaComplexModel = JSONUtilityFunctions.DeSerializeEKFTargyModelObj(rule.TargyFormula);
            TargyFormulaDefaultModel = JSONUtilityFunctions.DeSerializeEKFDefaultTargyModelObj(rule.TargyAlapertelmezettFormula);
        }

        #region INTERFACES
        /// <summary>
        /// EvaluateTargyFormula
        /// </summary>
        /// <returns></returns>
        public string EvaluateTargyFormula()
        {
            string result = string.Empty;

            result = EvaluateComplexFormula();
            if (string.IsNullOrEmpty(result))
            {
                Logger.Debug("Tárgy mező meghatározása sikertelen, a komplex formula alapján. Meghatározás default alapján.");
                result = EvaluateDefaultFormula();
            }

            if (string.IsNullOrEmpty(result))
            {
                throw new Exception("Sikertelen a Tárgy mező meghatározása !");
            }

            return result;
        }
        #endregion

        #region EVAL

        #region EVALUATE FORMULAS
        /// <summary>
        /// EvaluateComplexFormula
        /// </summary>
        /// <returns></returns>
        private string EvaluateComplexFormula()
        {
            if (TargyFormulaComplexModel == null || TargyFormulaComplexModel.Count < 1)
            {
                throw new Exception("Hibás a Tárgy formula !");
            }

            StringBuilder result = new StringBuilder();
            EKF_ComplexTargyModel tmpModel = null;
            string tmpContent;

            List<byte[]> xmlAttachments = null;
            bool xmlAttachmentsDownloaded = false;

            for (int i = 0; i < TargyFormulaComplexModel.Count; i++)
            {
                tmpModel = TargyFormulaComplexModel[i];
                if (tmpModel.Type == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.DB)
                {
                    tmpContent = EKFManager.GetDataSetValue(EBeadvanyDataSet, tmpModel.Value_DB);
                    if (string.IsNullOrEmpty(tmpContent))
                    {
                        continue;
                    }

                    result.Append(string.Format("{0}{1}{2}", tmpModel.Prefix, tmpContent, tmpModel.Postfix));
                }
                else if (tmpModel.Type == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.TXT)
                {
                    result.Append(string.Format("{0}{1}{2}", tmpModel.Prefix, tmpModel.Value_TXT, tmpModel.Postfix));
                }
                else if (tmpModel.Type == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.XML)
                {
                    if (!xmlAttachmentsDownloaded)
                    {
                        xmlAttachmentsDownloaded = true;
                        xmlAttachments = DownloadXmlAttachments();
                    }
                    if (xmlAttachments == null || xmlAttachments.Count < 1)
                    {
                        continue;
                    }

                    tmpContent = EvaluateXmlInAttachments(xmlAttachments, tmpModel.Value_XML);
                    if (string.IsNullOrEmpty(tmpContent))
                    {
                        continue;
                    }

                    result.Append(string.Format("{0}{1}{2}", tmpModel.Prefix, tmpContent, tmpModel.Postfix));
                }
                /*result.Append(" ");*/
            }

            return result.ToString();
        }
        /// <summary>
        /// EvaluateDefaultFormula
        /// </summary>
        /// <returns></returns>
        private string EvaluateDefaultFormula()
        {
            if (TargyFormulaDefaultModel == null || string.IsNullOrEmpty(TargyFormulaDefaultModel.Type))
            {
                throw new Exception("Hibás az alapértelmezett Tárgy formula !");
            }

            StringBuilder result = new StringBuilder();

            string tmpContent;

            if (TargyFormulaDefaultModel.Type == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.DB)
            {
                tmpContent = EKFManager.GetDataSetValue(EBeadvanyDataSet, TargyFormulaDefaultModel.Value_DB);
                if (string.IsNullOrEmpty(tmpContent))
                {
                    return string.Empty;
                }

                result.Append(string.Format("{0}", tmpContent));
            }
            else if (TargyFormulaDefaultModel.Type == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.TXT)
            {
                if (string.IsNullOrEmpty(TargyFormulaDefaultModel.Value_TXT))
                {
                    return string.Empty;
                }

                result.Append(string.Format("{0}", TargyFormulaDefaultModel.Value_TXT));
            }
            else if (TargyFormulaDefaultModel.Type == KodTarak.PARAMETER_FORRAS_EUZENETSZABALY_TARGY.XML)
            {
                tmpContent = EvaluateXmlInAttachments(TargyFormulaDefaultModel.Value_XML);
                if (string.IsNullOrEmpty(tmpContent))
                {
                    return string.Empty;
                }

                result.Append(string.Format("{0}", tmpContent));
            }
            return result.ToString();
        }
        #endregion
        /// <summary>
        /// DownloadXmlAttachments
        /// </summary>
        /// <returns></returns>
        public List<byte[]> DownloadXmlAttachments()
        {
            List<byte[]> byteContents = GetXmlDocuments(Rule.GetExecParam(), EBeadvanyId, EKFConstants.CONST_LIKE_FORMULA_XML_FILES, Query.Operators.like);
            return byteContents;
        }
        /// <summary>
        /// EvaluateXmlInAttachments
        /// </summary>
        /// <param name="byteContents"></param>
        /// <param name="xPath"></param>
        /// <returns></returns>
        public string EvaluateXmlInAttachments(List<byte[]> byteContents, string xPath)
        {
            if (byteContents == null || byteContents.Count < 1)
                return null;

            string resultXmlContent = null;
            for (int i = 0; i < byteContents.Count; i++)
            {
                resultXmlContent = GetContentFromXml(byteContents[i], xPath);
                if (!string.IsNullOrEmpty(resultXmlContent))
                {
                    return resultXmlContent;
                }
            }
            return resultXmlContent;
        }
        /// <summary>
        /// EvaluateXmlInAttachments
        /// Search in all xml files
        /// </summary>
        /// <returns></returns>
        public string EvaluateXmlInAttachments(string xPath)
        {
            List<byte[]> byteContents = DownloadXmlAttachments();
            if (byteContents == null || byteContents.Count < 1)
            {
                return null;
            }

            return EvaluateXmlInAttachments(byteContents, xPath);
        }
        /// <summary>
        /// EvaluateFormXmlInAttachments
        /// Search in xml -  "UzenetTipusa".xml (form.xml - original)
        /// </summary>
        /// <returns></returns>
        private string EvaluateFormXmlInAttachments()
        {
            string uzenetTipusa = EKFManager.GetDataSetValue(EBeadvanyDataSet, EKFConstants.eBeadvanyOszlopok.UzenetTipusa.ToString());

            if (string.IsNullOrEmpty(uzenetTipusa))
            {
                return string.Empty;
            }

            string searchXmlFileName = uzenetTipusa + ".xml";

            List<byte[]> byteContents = GetXmlDocuments(Rule.GetExecParam(), EBeadvanyId, searchXmlFileName, Query.Operators.equals);
            if (byteContents == null)
            {
                return null;
            }

            string result = null;
            for (int i = 0; i < byteContents.Count; i++)
            {
                result = GetContentFromXml(byteContents[i], TargyFormulaXPath);
                if (!string.IsNullOrEmpty(result))
                {
                    return result;
                }
            }
            return result;
        }

        #endregion

        #region XML HELPER
        /// <summary>
        /// GetXmlDocuments
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="eBeadvanyId"></param>
        /// <param name="searchFajlNev"></param>
        /// <param name="searchFajlNevOperator"></param>
        /// <returns></returns>
        public List<byte[]> GetXmlDocuments(ExecParam execParam, string eBeadvanyId, string searchFajlNev, string searchFajlNevOperator)
        {
            List<byte[]> xmlDocuments = new List<byte[]>();

            Contentum.eDocument.Service.KRT_DokumentumokService dokumentumokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            KRT_DokumentumokSearch dokumentumokSearch = new KRT_DokumentumokSearch();
            dokumentumokSearch.Id.Value = String.Format("select Dokumentum_Id from EREC_eBeadvanyCsatolmanyok where eBeadvany_Id = '{0}' and GETDATE() between EREC_eBeadvanyCsatolmanyok.ErvKezd and EREC_eBeadvanyCsatolmanyok.ErvVege", eBeadvanyId);
            dokumentumokSearch.Id.Operator = Query.Operators.inner;

            dokumentumokSearch.FajlNev.Value = searchFajlNev;
            dokumentumokSearch.FajlNev.Operator = searchFajlNevOperator;

            Result dokumentumokResult = dokumentumokService.GetAll(execParam, dokumentumokSearch);

            if (dokumentumokResult.IsError)
            {
                throw new ResultException(dokumentumokResult);
            }

            if (dokumentumokResult.Ds.Tables[0].Rows.Count == 0)
            {
                return null;
            }

            string externalLink;
            byte[] docContent;
            for (int i = 0; i < dokumentumokResult.Ds.Tables[0].Rows.Count; i++)
            {
                externalLink = dokumentumokResult.Ds.Tables[0].Rows[i]["External_Link"].ToString();
                if (string.IsNullOrEmpty(externalLink))
                {
                    continue;
                }

                try
                {
                    docContent = GetDocContent(externalLink);
                    if (docContent == null)
                    {
                        continue;
                    }

                    xmlDocuments.Add(docContent);
                }
                catch (Exception exc)
                {
                    Logger.Error("EKFManagerTargyFormula.DownloadDocument External_Link:" + externalLink, exc);
                }
            }

            return xmlDocuments;
        }
        /// <summary>
        /// GetDocContent
        /// </summary>
        /// <param name="externalLink"></param>
        /// <returns></returns>
        private byte[] GetDocContent(string externalLink)
        {
            return EBeadvanyFormXmlHelper.GetDocContent(externalLink);
        }
        /// <summary>
        /// GetContentFromXml
        /// </summary>
        /// <param name="docContent"></param>
        /// <returns></returns>
        internal static string GetContentFromXml(byte[] docContent, string xPath)
        {
            return EBeadvanyFormXmlHelper.GetContentFromXml(docContent, xPath);
        }
        #endregion
    }
}
