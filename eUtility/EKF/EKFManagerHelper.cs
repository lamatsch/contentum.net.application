﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Contentum.eUtility.EKF
{
    public enum EnumTargySzoObjektumTipusKod
    {
        EREC_IraIratok,
        EREC_UgyUgyiratok
    }
    public partial class EKFManager
    {
        public static string GetEKFOperationParameterValue(EKFOperationParameters parameters, string enumName)
        {
            var find = new EKFOperationParameterValue() { IdProperty = enumName.ToString() };

            int index = parameters.Parameters.IndexOf(find);
            if (index == -1)
                throw new Exception(string.Format("Nem található EKFOperationParameterValue {0} paraméter", enumName));
            EKFOperationParameterValue param = parameters.Parameters[index];
            if (param == null)
                throw new Exception(string.Format("Nem található EKFOperationParameterValue {0} paraméter", enumName));
            return param.ValueProperty;
        }

        public static string GetCimSTR_BekuldoFromKuldemenyData(DataSet inputData)
        {
            if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerKRID.ToString())))
                return EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerKRID.ToString());
            else if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerKapcsolatiKod.ToString())))
                return EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerKapcsolatiKod.ToString());
            else if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerEmail.ToString())))
                return EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerEmail.ToString());
            else
                return EKFConstants.EnumEKFbekuldo.Hivatali_kapu.ToString();
        }

        /// <summary>
        /// Visszaadja a EnumDataTableNames enum értékét
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static EKFConstants.EnumDataTableNames GetEnumTableName(string enumValue)
        {
            return (EKFConstants.EnumDataTableNames)Enum.Parse(typeof(EKFConstants.EnumDataTableNames), enumValue);
        }

        #region JSON
        /// <summary>
        /// Szabály sorosítása json formátumba
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string EKFSerialize(EKFRuleClass item)
        {
            if (item == null)
                return null;

            string result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;

            try
            {
                result = JsonConvert.SerializeObject(item, settings);
            }
            catch (Exception exc)
            {
                Logger.Error("ElektronikusKuldemenyFeldolgozas.EKFSerialize hiba:", exc);
            }
            return result;
        }
        /// <summary>
        /// Szabály vissza sorosítása objektum formátumra
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static EKFRuleClass EKFDeSerialize(string json)
        {
            if (string.IsNullOrEmpty(json))
                return null;
            EKFRuleClass result = null;

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.DeserializeObject<EKFRuleClass>(json, settings);
            }
            catch (Exception exc)
            {
                Logger.Error("ElektronikusKuldemenyFeldolgozas.EKFDeSerialize hiba:", exc);
            }
            return result;
        }

        public static class VType
        {
            public static List<Type> GetDerivedTypes(Type baseType, Assembly assembly)
            {
                // Get all types from the given assembly
                Type[] types = assembly.GetTypes();
                List<Type> derivedTypes = new List<Type>();

                for (int i = 0, count = types.Length; i < count; i++)
                {
                    Type type = types[i];
                    if (VType.IsSubclassOf(type, baseType))
                    {
                        // The current type is derived from the base type,
                        // so add it to the list
                        derivedTypes.Add(type);
                    }
                }

                return derivedTypes;
            }

            public static bool IsSubclassOf(Type type, Type baseType)
            {
                if (type == null || baseType == null || type == baseType)
                    return false;

                if (baseType.IsGenericType == false)
                {
                    if (type.IsGenericType == false)
                        return type.IsSubclassOf(baseType);
                }
                else
                {
                    baseType = baseType.GetGenericTypeDefinition();
                }

                type = type.BaseType;
                Type objectType = typeof(object);

                while (type != objectType && type != null)
                {
                    Type curentType = type.IsGenericType ?
                        type.GetGenericTypeDefinition() : type;
                    if (curentType == baseType)
                        return true;

                    type = type.BaseType;
                }

                return false;
            }
        }
        #endregion

        #region KULDEMENY KEZB MODJA
        /// <summary>
        /// GetKezbesitesModja
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="kuldKuldemeny"></param>
        public static void SetKuldemenyKezbesitesModja(EKFRuleClass rule, ref EREC_KuldKuldemenyek kuldKuldemeny)
        {
            string[] kodtarak = new string[] {
                KodTarak.KULD_KEZB_MODJA.e_ugyintezes,
                KodTarak.KULD_KEZB_MODJA.elektronikus_uton_Adatkapu,
                KodTarak.KULD_KEZB_MODJA.elektronikus_uton_Ugyfelkapu,
                KodTarak.KULD_KEZB_MODJA.elektronikus_uton_HKP,
            };

            try
            {
                string validKodtarElem = CheckKodtarakExistsAndReturnFirstValid(rule, KodTarak.KULD_KEZB_MODJA.KCS, kodtarak);
                if (string.IsNullOrEmpty(validKodtarElem))
                {
                    kuldKuldemeny.KezbesitesModja = KodTarak.KULD_KEZB_MODJA.e_ugyintezes;
                    return;
                }
                else
                {
                    kuldKuldemeny.KezbesitesModja = validKodtarElem;
                    return;
                }
            }
            catch (Exception exc)
            {
                Logger.Error("EKF.SetKuldemenyKezbesitesModja", exc);
            }
            kuldKuldemeny.KuldesMod = KodTarak.KULD_KEZB_MODJA.e_ugyintezes;
        }
        /// <summary>
        /// CheckKodtarExist
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="kodCsoport"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        private static string CheckKodtarakExistsAndReturnFirstValid(EKFRuleClass rule, string kodCsoport, string[] ids)
        {
            #region KCS
            KRT_KodCsoportokService svcKcs = eAdminService.ServiceFactory.GetKRT_KodCsoportokService();
            ExecParam execParamKcs = rule.GetExecParam();
            KRT_KodCsoportokSearch srcKcs = new KRT_KodCsoportokSearch();
            srcKcs.Kod.Value = kodCsoport;
            srcKcs.Kod.Operator = Query.Operators.equals;
            srcKcs.TopRow = 1;
            Result resultKcs = svcKcs.GetAll(execParamKcs.Clone(), srcKcs);
            resultKcs.CheckError();

            if (resultKcs.Ds.Tables[0].Rows.Count < 1)
                return string.Empty;

            string kodCsoportId = resultKcs.Ds.Tables[0].Rows[0]["Id"].ToString();
            #endregion

            ExecParam execParam = rule.GetExecParam();
            KRT_KodTarakService svc = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            KRT_KodTarakSearch src = new KRT_KodTarakSearch();
            src.KodCsoport_Id.Value = kodCsoportId;
            src.KodCsoport_Id.Operator = Query.Operators.equals;
            Result result = svc.GetAll(execParam.Clone(), src);
            result.CheckError();

            if (result.Ds.Tables[0].Rows.Count < 1)
                return string.Empty;

            for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
            {
                if (ids.Contains(result.Ds.Tables[0].Rows[i]["Kod"]))
                {
                    return result.Ds.Tables[0].Rows[i]["Kod"].ToString();
                }
            }
            return string.Empty;
        }

        #endregion
        /// <summary>
        /// SetKuldemenyKuldesModja
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="kuldKuldemeny"></param>
        public static void SetKuldemenyKuldesModja(EKFRuleClass rule, ref EREC_KuldKuldemenyek kuldKuldemeny)
        {
            string[] kodtarak = new string[] {
                KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu,
                KodTarak.KULDEMENY_KULDES_MODJA.Elhisz,
            };

            try
            {
                string validKodtarElem = CheckKodtarakExistsAndReturnFirstValid(rule, KodTarak.KULDEMENY_KULDES_MODJA.KodcsoportKod, kodtarak);
                if (string.IsNullOrEmpty(validKodtarElem))
                {
                    kuldKuldemeny.KuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu;
                    return;
                }
                else
                {
                    kuldKuldemeny.KuldesMod = validKodtarElem;
                    return;
                }
            }
            catch (Exception exc)
            {
                Logger.Error("EKF.SetKuldemenyKuldesModja", exc);
            }
            kuldKuldemeny.KuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu;
        }
        /// <summary>
        /// SetIratExpedialasModja
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="irat"></param>
        public static void SetIratExpedialasModja(EKFRuleClass rule, ref EREC_IraIratok irat)
        {
            string[] kodtarak = new string[] {
                KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu,
                KodTarak.KULDEMENY_KULDES_MODJA.Elhisz,
            };

            try
            {
                string validKodtarElem = CheckKodtarakExistsAndReturnFirstValid(rule, KodTarak.KULDEMENY_KULDES_MODJA.KodcsoportKod, kodtarak);
                if (string.IsNullOrEmpty(validKodtarElem))
                {
                    irat.ExpedialasModja = KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu;
                    return;
                }
                else
                {
                    irat.ExpedialasModja = validKodtarElem;
                    return;
                }
            }
            catch (Exception exc)
            {
                Logger.Error("EKF.SetIratExpedialasModja", exc);
            }
            irat.ExpedialasModja = KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu;
        }

        /// <summary>
        /// TryParseGuid
        /// </summary>
        /// <param name="guidString"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static bool TryParseGuid(string guidString, out Guid guid)
        {
            guid = default(Guid);
            if (string.IsNullOrEmpty(guidString))
            {
                return false;
            }

            try
            {
                guid = new Guid(guidString);
                return true;
            }
            catch (FormatException)
            {
                guid = default(Guid);
                return false;
            }
        }
        /// <summary>
        /// GetUgyFajtaFromIratTipus
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="ugyTipus"></param>
        /// <param name="iratatriTetel"></param>
        /// <returns></returns>
        public static string GetUgyFajtaFromUgyTipus(EKFRuleClass rule, string ugyTipus, string iratatriTetel)
        {
            EREC_IratMetaDefinicioService service_IratMetaDefinicio = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
            Result result_IraMetaDefinicio;
            ExecParam execParam = rule.GetExecParam();

            EREC_IratMetaDefinicioSearch search_IratMetaDefinicio = new EREC_IratMetaDefinicioSearch();
            search_IratMetaDefinicio.Ugytipus.Value = ugyTipus;
            search_IratMetaDefinicio.Ugytipus.Operator = Query.Operators.equals;

            search_IratMetaDefinicio.Ugykor_Id.Value = iratatriTetel;
            search_IratMetaDefinicio.Ugykor_Id.Operator = Query.Operators.equals;

            search_IratMetaDefinicio.EljarasiSzakasz.Operator = Query.Operators.isnull;
            search_IratMetaDefinicio.Irattipus.Operator = Query.Operators.isnull;

            search_IratMetaDefinicio.TopRow = 1;

            search_IratMetaDefinicio.ErvKezd.Clear();
            search_IratMetaDefinicio.ErvVege.Clear();

            search_IratMetaDefinicio.OrderBy = "EREC_IratMetaDefinicio.LetrehozasIdo desc";

            result_IraMetaDefinicio = service_IratMetaDefinicio.GetAll(execParam, search_IratMetaDefinicio);

            if (!string.IsNullOrEmpty(result_IraMetaDefinicio.ErrorCode))
            {
                throw new ResultException(result_IraMetaDefinicio);
            }
            else
            {
                if (result_IraMetaDefinicio.Ds.Tables[0].Rows.Count > 0)
                {
                    return result_IraMetaDefinicio.Ds.Tables[0].Rows[0]["UgyFajta"].ToString();
                }
            }
            return null;
        }
        /// <summary>
        /// GetIntezesiHataridoByIdoegyseg
        /// </summary>
        /// <param name="ExecParam"></param>
        /// <param name="Idoegyseg"></param>
        /// <param name="IntezesiIdo"></param>
        /// <param name="DatumTol"></param>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        [Obsolete]
        public static DateTime GetIntezesiHataridoByIdoegyseg(ExecParam ExecParam, string Idoegyseg, string IntezesiIdo, DateTime DatumTol, out string ErrorMessage)
        {
            Logger.Debug("GetIntezesiHataridoByIdoegyseg - START");
            Logger.Debug(String.Format("Bemenõ paraméterek: Idõegység: {0}; Intézési idõ: {1}; Dátum: {2}", Idoegyseg, IntezesiIdo, DatumTol));

            ErrorMessage = String.Empty;
            DateTime dtHatarido = DateTime.MinValue;

            int nIdoegysegParsed = 0;
            int nIntezesiIdoParsed = 0;

            if (String.IsNullOrEmpty(Idoegyseg) || Idoegyseg == "0"
                || String.IsNullOrEmpty(IntezesiIdo) || IntezesiIdo == "0")
            {
                // default érték használata
                int nDefaultIntezesiIdo = Rendszerparameterek.GetInt(ExecParam, Rendszerparameterek.DEFAULT_INTEZESI_IDO);
                int nDefaultIdoegyseg = Rendszerparameterek.GetInt(ExecParam, Rendszerparameterek.DEFAULT_INTEZESI_IDO_IDOEGYSEG);

                if (nDefaultIdoegyseg == 0)
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65101); //"Az alapértelmezett idõegység nem határozható meg!"
                    Logger.Debug(String.Format("Hiba: {0}", ErrorMessage));
                    Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
                    return dtHatarido;
                }

                if (nDefaultIntezesiIdo == 0)
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65102); //"Az alapértelmezett intézési idõ nem határozható meg!"
                    Logger.Debug(String.Format("Hiba: {0}", ErrorMessage));
                    Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
                    return dtHatarido;
                }

                nIdoegysegParsed = nDefaultIdoegyseg;
                nIntezesiIdoParsed = nDefaultIntezesiIdo;
            }
            else
            {
                int nIdoegyseg;
                if (!Int32.TryParse(Idoegyseg, out nIdoegyseg))
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65103); //"Az idõegység nem határozható meg."
                    Logger.Debug(String.Format("Hiba: {0}", ErrorMessage));
                    Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
                    return dtHatarido;
                }

                int nIntezesiIdo;
                if (!Int32.TryParse(IntezesiIdo, out nIntezesiIdo))
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65104); //"Az intézési idõ nem határozható meg!"
                    Logger.Debug(String.Format("Hiba: {0}", ErrorMessage));
                    Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
                    return dtHatarido;
                }

                nIdoegysegParsed = nIdoegyseg;
                nIntezesiIdoParsed = nIntezesiIdo;
            }

            Logger.Debug(String.Format("Elemzett idõegység: {0}; Elemzett intézési idõ: {1}", nIdoegysegParsed, nIntezesiIdoParsed));

            // intézési határidõ meghatározása
            if (nIdoegysegParsed < 0)
            {
                switch (nIdoegysegParsed.ToString())
                {
                    case KodTarak.IDOEGYSEG.Munkanap:   // -1440                    
                        Contentum.eAdmin.Service.KRT_Extra_NapokService service_extra_napok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_Extra_NapokService();

                        Result result_extra_napok = service_extra_napok.KovetkezoMunkanap(ExecParam, DatumTol, nIntezesiIdoParsed);
                        if (result_extra_napok.IsError)
                        {
                            ErrorMessage = ResultError.GetErrorMessageFromResultObject(result_extra_napok);
                        }
                        else
                        {
                            try
                            {
                                dtHatarido = (DateTime)result_extra_napok.Record;
                            }
                            catch
                            {
                                dtHatarido = DateTime.MinValue;
                                ErrorMessage = "Rossz dátum formátum.";
                            }
                        }

                        // óra, perc beállítás
                        dtHatarido = dtHatarido.AddHours(DatumTol.Hour);
                        dtHatarido = dtHatarido.AddMinutes(DatumTol.Minute);
                        break;
                    default:
                        ErrorMessage = ResultError.GetErrorMessageByErrorCode(65105); //"Ismeretlen idõegység. Az idõegységre vonatkozó határidõszámítás nincs implementálva!"
                        break;
                }
            }
            else
            {
                dtHatarido = DatumTol.AddMinutes(nIdoegysegParsed * nIntezesiIdoParsed);
            }

            Logger.Debug(String.Format("Határidõ: {0}", dtHatarido));
            Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
            return dtHatarido;
        }
        /// <summary>
        /// SetUgyiratHatarido
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ugyirat"></param>
        /// <param name="errorMessage"></param>
        public static void SetUgyiratHatarido(ExecParam execParam, ref EREC_UgyUgyiratok ugyirat, out string errorMessage)
        {
            errorMessage = null;
            if (ugyirat == null)
                return;

            try
            {
                Sakkora.SetUgyUgyintezesKezdeteGlobal(execParam, ref ugyirat);
                DateTime kezdet;
                DateTime.TryParse(ugyirat.UgyintezesKezdete, out kezdet);
                //DateTime hatarido = GetIntezesiHataridoByIdoegyseg(execParam, ugyirat.IntezesiIdoegyseg, ugyirat.IntezesiIdo, kezdet, out errorMessage);

                DateTime hatarido = Sakkora.GetUgyUgyintezesHataridoGlobal(execParam, kezdet, ugyirat.IntezesiIdo, ugyirat.IntezesiIdoegyseg, out errorMessage);
                if (hatarido == null)
                {
                    return;
                }

                ugyirat.Hatarido = hatarido.ToString();
            }
            catch (Exception exc)
            {
                errorMessage = exc.Message;
            }
        }
        /// <summary>
        /// SetIratHatarido
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="irat"></param>
        /// <param name="errorMessage"></param>
        public static void SetIratHatarido(ExecParam execParam, ref EREC_IraIratok irat, out string errorMessage)
        {
            errorMessage = null;
            if (irat == null)
            {
                return;
            }

            try
            {
                //DateTime hatarido = GetIntezesiHataridoByIdoegyseg(execParam, irat.IntezesiIdoegyseg, irat.IntezesiIdo, kezdet, out errorMessage);
                DateTime hatarido = Sakkora.GetIratintezesHataridoGlobal(execParam, irat.IktatasDatuma, irat.IntezesiIdo, irat.IntezesiIdoegyseg, out errorMessage);

                if (hatarido == null)
                {
                    return;
                }

                irat.Hatarido = hatarido.ToString();
            }
            catch (Exception exc)
            {
                errorMessage = exc.Message;
            }
        }
        /// <summary>
        /// EBeadvanyKuldemenyBeerkezesIdoMeghatarozas
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="inputData"></param>
        /// <param name="kuldemeny"></param>
        public static void EBeadvanyKuldemenyBeerkezesIdoMeghatarozas(ExecParam execParam, DataSet inputData, ref EREC_KuldKuldemenyek kuldemeny)
        {
            Contentum.eRecord.Service.EREC_eBeadvanyokService svcEBead = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            Result result = svcEBead.IsEUzenetForras_Elhisz(execParam);
            result.CheckError();
            bool isElhisz = (bool)result.Record;
            if (isElhisz)
            {
                string valueErkDatum = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.KR_ErkeztetesiDatum.ToString());
                if (!string.IsNullOrEmpty(valueErkDatum))
                    kuldemeny.BeerkezesIdeje = valueErkDatum;
                else
                    kuldemeny.BeerkezesIdeje = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.LetrehozasIdo.ToString());
            }
            else
                kuldemeny.BeerkezesIdeje = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.LetrehozasIdo.ToString());
        }
    }
}
