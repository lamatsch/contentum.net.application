﻿using Contentum.eBusinessDocuments;
using System;

namespace Contentum.eUtility.EKF
{
    /// <summary>
    /// ElektronikusKuldemenyFeldolgozas
    /// </summary>
    public partial class EKFManager
    {
        private void AddMessage(string msg)
        {
            OccuredMessages.Add(msg);
            Logger.Error(msg);
        }
        /// <summary>
        /// Add Result to log
        /// </summary>
        /// <param name="msg"></param>
        public void AddMessage(Result msg)
        {
            if (msg == null)
                return;

            AddMessage(string.Format("EKF: {0} {1} {2}",
                msg.ErrorCode,
                msg.ErrorMessage,
                msg.ErrorDetail != null ? msg.ErrorDetail.Message : string.Empty));
        }
        /// <summary>
        /// Add exception to log
        /// </summary>
        /// <param name="msg"></param>
        public void AddMessage(Exception msg)
        {
            if (msg == null)
                return;

            AddMessage(string.Format("EKF: {0} {1}",
                msg.Message, 
                msg.InnerException == null ? string.Empty : msg.InnerException.Message));
        }
    }
}
