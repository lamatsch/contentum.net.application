﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Data;

namespace Contentum.eUtility.EKF
{
    /// <summary>
    /// ElektronikusKuldemenyFeldolgozas
    /// </summary>
    public partial class EKFManager
    {
        #region SERVICES
        /// <summary>
        /// EUzenet szabalyok lekérése
        /// </summary>
        /// <returns></returns>
        private Result GetRules()
        {
            INT_AutoEUzenetSzabalyService ekfService = eAdminService.ServiceFactory.GetINT_AutoEUzenetSzabalyService();
            ExecParam execParam = MyExecParam;
            INT_AutoEUzenetSzabalySearch search = new INT_AutoEUzenetSzabalySearch();
            search.OrderBy = "Sorrend ASC";
            Result resultGetRules = ekfService.GetAll(execParam, search);

            if (!string.IsNullOrEmpty(resultGetRules.ErrorCode))
                throw new ResultException(resultGetRules);
            return resultGetRules;
        }
        /// <summary>
        /// EBeadvány csatolmány lekérése
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private Result GetEBeadvanyCsatolmany(Guid id)
        {
            ExecParam execParamGet = MyExecParam;
            EREC_eBeadvanyCsatolmanyokService eBeadvCsatolmanyokService = eRecordService.ServiceFactory.GetEREC_eBeadvanyCsatolmanyokService();
            EREC_eBeadvanyCsatolmanyokSearch searchEBeadvanyCsatolmanyok = new EREC_eBeadvanyCsatolmanyokSearch();
            searchEBeadvanyCsatolmanyok.eBeadvany_Id.Value = id.ToString();
            searchEBeadvanyCsatolmanyok.eBeadvany_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            Result eBeadvanyRCsatolanyokResultGet = eBeadvCsatolmanyokService.GetAll(execParamGet, searchEBeadvanyCsatolmanyok);

            if (!string.IsNullOrEmpty(eBeadvanyRCsatolanyokResultGet.ErrorCode))
                throw new ResultException(eBeadvanyRCsatolanyokResultGet);
            return eBeadvanyRCsatolanyokResultGet;
        }
        /// <summary>
        /// EBeadvány lekérése
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Result GetEBeadvany(Guid id)
        {
            ExecParam execParamGet = MyExecParam;
            EREC_eBeadvanyokService eBeadvService = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
            search.Id.Value = id.ToString();
            search.Id.Operator = Contentum.eQuery.Query.Operators.equals;
            Result eBeadvanyResult = eBeadvService.GetAll(execParamGet, search);

            if (!string.IsNullOrEmpty(eBeadvanyResult.ErrorCode))
                throw new ResultException(eBeadvanyResult);
            return eBeadvanyResult;
        }
        /// <summary>
        /// EBeadvány lekérése
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Result GetEBeadvanyById(string id)
        {
            ExecParam execParamGet = MyExecParam;
            EREC_eBeadvanyokService eBeadvService = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
            search.Id.Value = id;
            search.Id.Operator = Contentum.eQuery.Query.Operators.equals;
            Result eBeadvanyResult = eBeadvService.GetAll(execParamGet, search);

            if (!string.IsNullOrEmpty(eBeadvanyResult.ErrorCode))
                throw new ResultException(eBeadvanyResult);
            return eBeadvanyResult;
        }
        /// <summary>
        /// GetEBeadvanyByDataSet
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public EREC_eBeadvanyok GetEBeadvanyByDataSet(DataSet inputData)
        {
            string eBeadvanyId = GetDataSetValue(inputData, "Id");
            ExecParam execParamGet = MyExecParam;
            EREC_eBeadvanyokService eBeadvService = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            execParamGet.Record_Id = eBeadvanyId;
            Result eBeadvanyResult = eBeadvService.Get(execParamGet);

            if (!string.IsNullOrEmpty(eBeadvanyResult.ErrorCode))
                throw new ResultException(eBeadvanyResult);

            return (EREC_eBeadvanyok)eBeadvanyResult.Record;
        }
        /// <summary>
        /// UpdateEBeadvany
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="beadvany"></param>
        /// <returns></returns>
        public Result UpdateEBeadvany(EREC_eBeadvanyok beadvany)
        {
            EREC_eBeadvanyokService ugyIratokService = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            ExecParam xparamUpdate = MyExecParam;
            xparamUpdate.Record_Id = beadvany.Id;
            Result resultUpd = ugyIratokService.Update(xparamUpdate, beadvany);
            if (!string.IsNullOrEmpty(resultUpd.ErrorCode))
            {
                AddMessage(resultUpd);
                throw new ResultException(resultUpd);
            }
            return resultUpd;
        }

        /// <summary>
        /// GetIrat
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public EREC_IraIratok GetIrat(EKFRuleClass rule, string id)
        {
            EREC_IraIratokService serviceEbeadvany = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParamEBeadvGet = rule.GetExecParam();
            execParamEBeadvGet.Record_Id = id;
            Result resultGet = serviceEbeadvany.Get(execParamEBeadvGet);
            if (!string.IsNullOrEmpty(resultGet.ErrorCode))
            {
                AddMessage(resultGet);
                throw new ResultException(resultGet);
            }
            EREC_IraIratok irat = (EREC_IraIratok)resultGet.Record;
            return irat;
        }

        /// <summary>
        /// GetKuldemeny
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public EREC_KuldKuldemenyek GetKuldemeny(EKFRuleClass rule, string id)
        {
            EREC_KuldKuldemenyekService kuldService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();

            ExecParam xparamKuldEBeadvGet = rule.GetExecParam();
            xparamKuldEBeadvGet.Record_Id = id;
            Result kuldResultGet = kuldService.Get(xparamKuldEBeadvGet);

            if (!string.IsNullOrEmpty(kuldResultGet.ErrorCode))
            {
                AddMessage(kuldResultGet);
                throw new ResultException(kuldResultGet);
            }
            EREC_KuldKuldemenyek kuldemeny = (EREC_KuldKuldemenyek)kuldResultGet.Record;
            return kuldemeny;
        }

        /// <summary>
        /// GetUgyirat
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public EREC_UgyUgyiratok GetUgyirat(EKFRuleClass rule, string id)
        {
            #region GET UGYIRAT
            EREC_UgyUgyiratokService ugyIratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam xparamUgyGet = rule.GetExecParam();
            xparamUgyGet.Record_Id = id;
            Result ugyResultGet = ugyIratokService.Get(xparamUgyGet);

            if (!string.IsNullOrEmpty(ugyResultGet.ErrorCode))
            {
                AddMessage(ugyResultGet);
                throw new ResultException(ugyResultGet);
            }
            return (EREC_UgyUgyiratok)ugyResultGet.Record;
            #endregion  GET UGYIRAT
        }

        /// <summary>
        /// EUzenet nalo mentése
        /// </summary>
        /// <param name="naplo"></param>
        /// <returns></returns>
        private Result AddEUzenetNaplo(INT_AutoEUzenetSzabalyNaplo naplo)
        {
            ExecParam execParamGet = MyExecParam;
            INT_AutoEUzenetSzabalyNaploService eBeadvService = eAdminService.ServiceFactory.GetINT_AutoEUzenetSzabalyNaploService();
            INT_AutoEUzenetSzabalyNaploSearch search = new INT_AutoEUzenetSzabalyNaploSearch();

            Result eUzenetNaploResult = eBeadvService.Insert(execParamGet, naplo);

            if (!string.IsNullOrEmpty(eUzenetNaploResult.ErrorCode))
                throw new ResultException(eUzenetNaploResult);
            return eUzenetNaploResult;
        }

        public Result UpdateUgyirat(EKFRuleClass rule, EREC_UgyUgyiratok ugy)
        {
            EREC_UgyUgyiratokService ugyIratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam xparamUgyGet = rule.GetExecParam();
            xparamUgyGet.Record_Id = ugy.Id;
            Result resultUgyUpd = ugyIratokService.Update(xparamUgyGet, ugy);
            if (!string.IsNullOrEmpty(resultUgyUpd.ErrorCode))
            {
                AddMessage(resultUgyUpd);
                throw new ResultException(resultUgyUpd);
            }
            return resultUgyUpd;
        }
        #endregion
    }
}
