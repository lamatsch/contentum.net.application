﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility.EKF.FORM_XML;
using Contentum.eUtility.EKF.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.XPath;

namespace Contentum.eUtility.EKF
{
    public class EBeadvanyFormXmlHelper
    {
        public static readonly string XML_TAG_NAME_TYPE_ID = "//*/*[name()='variables']/*[name()='form.type_id']";

        public static readonly string XML_TAG_NAME_IRAT_TARGYA = "//*/*[name()='variables']/*[name()='iratTargya']";

        public static readonly string XML_TAG_NAME_FORM_ID = "//*/*[name()='variables']/*[name()='form.id']";
        public static readonly string XML_TAG_NAME_FORM_TYPE_NAME = "//*/*[name()='variables']/*[name()='form.type_name']";
        public static readonly string XML_TAG_NAME_TEXT001_INPUT = "//*/*[name()='variables']/*[name()='text001_input']";
        public static readonly string XML_TAG_NAME_FORM_USERID = "//*/*[name()='variables']/*[name()='form.userid']";
        public static readonly string XML_TAG_NAME_FORM_USERNAME = "//*/*[name()='variables']/*[name()='form.username']";
        public static readonly string XML_TAG_NAME_FORM_COMPANY_ID = "//*/*[name()='variables']/*[name()='form.company_id']";
        public static readonly string XML_TAG_NAME_FORM_HIR_ID = "//*/*[name()='variables']/*[name()='form.hir_id']";
        public static readonly string XML_TAG_NAME_FORM_COMPANY_NAME = "//*/*[name()='variables']/*[name()='form.company_name']";
        public static readonly string XML_TAG_NAME_FORM_COMPANY_ADDRESS_CITY = "//*/*[name()='variables']/*[name()='form.company_address_city']";
        public static readonly string XML_TAG_NAME_FORM_COMPANY_ADDRESS_ZIP = "//*/*[name()='variables']/*[name()='form.company_address_zip']";
        public static readonly string XML_TAG_NAME_FORM_COMPANY_ADDRESS_ID = "//*/*[name()='variables']/*[name()='form.company_address_id']";

        public static readonly string XML_TAG_NAME_CPF = "//*/*[name()='variables']/*[name()='cpf']";

        public static readonly string XML_TAG_NAME_CPF_SZERVEZET = "//*/*[name()='variables']/*[name()='cpf']/*[name()='Szervezet']";
        public static readonly string XML_TAG_NAME_CPF_KAPCSOLATTARTO = "//*/*[name()='variables']/*[name()='cpf']/*[name()='Kapcsolattarto']";
        public static readonly string XML_TAG_NAME_CPF_EMAILADDRESS = "//*/*[name()='variables']/*[name()='cpf']/*[name()='emailAddress']";

        public static readonly string XML_TAG_NAME_CPF_DRCIMJELZES = "//*/*[name()='variables']/*[name()='cpf']/*[name()='DrCimJelzes']";
        public static readonly string XML_TAG_NAME_CPF_CSALADINEV = "//*/*[name()='variables']/*[name()='cpf']/*[name()='CsaladiNev']";
        public static readonly string XML_TAG_NAME_CPF_UTONEV1 = "//*/*[name()='variables']/*[name()='cpf']/*[name()='UtoNev1']";
        public static readonly string XML_TAG_NAME_CPF_UTONEV2 = "//*/*[name()='variables']/*[name()='cpf']/*[name()='UtoNev2']";
        public static readonly string XML_TAG_NAME_CPF_BENYUJTO_LAKCIM_IRSZ = "//*/*[name()='variables']/*[name()='cpf']/*[name()='Benyujto_Lakcim_Irsz']";
        public static readonly string XML_TAG_NAME_CPF_BENYUJTO_LAKCIM_TELEPULES = "//*/*[name()='variables']/*[name()='cpf']/*[name()='Benyujto_Lakcim_Telepules']";
        public static readonly string XML_TAG_NAME_CPF_BENYUJTO_LAKCIM = "//*/*[name()='variables']/*[name()='cpf']/*[name()='Benyujto_Lakcim']";

        public static readonly string XML_TAG_VALUE_TYPE_ID = "NAF";
        public EKF_Form_XML_Model ExtractDataFromEBeadvanyFormXml(Guid id, ExecParam myExecParam)
        {
            List<byte[]> byteContents = GetEBeadvanyXmlDocuments(id, myExecParam);

            if (byteContents == null || byteContents.Count < 1)
            {
                return null;
            }

            EKF_Form_XML_Model modelFormXml = new EKF_Form_XML_Model();

            for (int i = 0; i < 1; i++)   //for (int i = 0; i < byteContents.Count; i++)
            {
                modelFormXml.TypeId = GetContentFromXml(byteContents[i], XML_TAG_NAME_TYPE_ID);
                if (!string.IsNullOrEmpty(modelFormXml.TypeId))
                {
                    modelFormXml.Type = 1;

                    modelFormXml.FormId = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_ID);
                    modelFormXml.FormTypeName = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_TYPE_NAME);
                    modelFormXml.Text001 = GetContentFromXml(byteContents[i], XML_TAG_NAME_TEXT001_INPUT);
                    modelFormXml.FormUserId = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_USERID);
                    modelFormXml.FormUserName = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_USERNAME);
                    modelFormXml.FormCompanyId = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_COMPANY_ID);
                    modelFormXml.FormHirId = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_HIR_ID);
                    modelFormXml.FormCompanyName = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_COMPANY_NAME);
                    modelFormXml.FormCompanyAddressCity = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_COMPANY_ADDRESS_CITY);
                    modelFormXml.FormCompanyAddressZip = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_COMPANY_ADDRESS_ZIP);
                    modelFormXml.FormCompanyAddressId = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_COMPANY_ADDRESS_ID);
                }
                else
                {
                    modelFormXml.CpfSzervezet = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_SZERVEZET);
                    if (!string.IsNullOrEmpty(modelFormXml.CpfSzervezet))
                    {
                        modelFormXml.Type = 2;

                        modelFormXml.CpfKapcsolattarto = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_KAPCSOLATTARTO);
                        modelFormXml.CpfEmailAddress = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_EMAILADDRESS);
                    }
                    else
                    {
                        modelFormXml.Type = 3;

                        modelFormXml.CpfDrCimJelzes = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_DRCIMJELZES);
                        modelFormXml.CpfCsaladiNev = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_CSALADINEV);
                        modelFormXml.CpfUtonev1 = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_UTONEV1);
                        modelFormXml.CpfUtonev2 = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_UTONEV2);
                    }

                    modelFormXml.CpfBenyujtoLakcimIrsz = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_BENYUJTO_LAKCIM_IRSZ);
                    modelFormXml.CpfBenyujtoLakcimTelepules = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_BENYUJTO_LAKCIM_TELEPULES);
                    modelFormXml.CpfBenyujtoLakcim = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_BENYUJTO_LAKCIM);
                }
            }
            return modelFormXml;
        }
        /// <summary>
        /// GetEBeadvanyXmlDocuments
        /// </summary>
        /// <param name="id"></param>
        /// <param name="myExecParam"></param>
        /// <returns></returns>
        internal static List<byte[]> GetEBeadvanyXmlDocuments(Guid id, ExecParam myExecParam)
        {
            Result resultEBeadvany = GetEBeadvany(id, myExecParam);
            resultEBeadvany.CheckError();
            if (resultEBeadvany.Ds.Tables[0].Rows.Count < 1)
            {
                return null;
            }

            string uzenetTipusa = EKFManager.GetDataSetValue(resultEBeadvany.Ds, "UzenetTipusa");

            if (string.IsNullOrEmpty(uzenetTipusa))
            {
                return null;
            }

            string searchXmlFileName = uzenetTipusa + EKFConstants.CONST_XML_FILE_EXTENSION;

            List<byte[]> byteContents = GetXmlDocuments(myExecParam, id.ToString(), searchXmlFileName, Query.Operators.equals);
            if (byteContents == null || byteContents.Count < 1)
            {
                searchXmlFileName = EKFConstants.CONST_XML_FILE_FORMXML;
                byteContents = GetXmlDocuments(myExecParam, id.ToString(), searchXmlFileName, Query.Operators.equals);
                if (byteContents == null || byteContents.Count < 1)
                {
                    return null;
                }
            }
            return byteContents;
        }
        /// <summary>
        /// GetEBeadvany
        /// </summary>
        /// <param name="id"></param>
        /// <param name="myExecParam"></param>
        /// <returns></returns>
        internal static Result GetEBeadvany(Guid id, ExecParam myExecParam)
        {
            ExecParam execParamGet = myExecParam;
            EREC_eBeadvanyokService eBeadvService = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
            search.Id.Value = id.ToString();
            search.Id.Operator = Query.Operators.equals;
            Result eBeadvanyResult = eBeadvService.GetAll(execParamGet, search);

            if (!string.IsNullOrEmpty(eBeadvanyResult.ErrorCode))
            {
                throw new ResultException(eBeadvanyResult);
            }

            return eBeadvanyResult;
        }
        /// <summary>
        /// GetXmlDocuments
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="eBeadvanyId"></param>
        /// <param name="searchFajlNev"></param>
        /// <param name="searchFajlNevOperator"></param>
        /// <returns></returns>
        internal static List<byte[]> GetXmlDocuments(ExecParam execParam, string eBeadvanyId, string searchFajlNev, string searchFajlNevOperator)
        {
            List<byte[]> xmlDocuments = new List<byte[]>();

            eDocument.Service.KRT_DokumentumokService dokumentumokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            KRT_DokumentumokSearch dokumentumokSearch = new KRT_DokumentumokSearch();
            dokumentumokSearch.Id.Value = string.Format("select Dokumentum_Id from EREC_eBeadvanyCsatolmanyok where eBeadvany_Id = '{0}' and GETDATE() between EREC_eBeadvanyCsatolmanyok.ErvKezd and EREC_eBeadvanyCsatolmanyok.ErvVege", eBeadvanyId);
            dokumentumokSearch.Id.Operator = Query.Operators.inner;

            dokumentumokSearch.FajlNev.Value = searchFajlNev;
            dokumentumokSearch.FajlNev.Operator = searchFajlNevOperator;

            Result dokumentumokResult = dokumentumokService.GetAll(execParam, dokumentumokSearch);

            if (dokumentumokResult.IsError)
            {
                throw new ResultException(dokumentumokResult);
            }

            if (dokumentumokResult.Ds.Tables[0].Rows.Count == 0)
            {
                return null;
            }

            string externalLink;
            byte[] docContent;
            for (int i = 0; i < dokumentumokResult.Ds.Tables[0].Rows.Count; i++)
            {
                externalLink = dokumentumokResult.Ds.Tables[0].Rows[i]["External_Link"].ToString();
                if (string.IsNullOrEmpty(externalLink))
                {
                    continue;
                }

                try
                {
                    docContent = GetDocContent(externalLink);
                    if (docContent == null)
                    {
                        continue;
                    }

                    xmlDocuments.Add(docContent);
                }
                catch (Exception exc)
                {
                    Logger.Error("EKFManagerTargyFormula.DownloadDocument External_Link:" + externalLink, exc);
                }
            }

            return xmlDocuments;
        }

        /// <summary>
        /// Get content from FormXml
        /// </summary>
        /// <param name="docContent"></param>
        /// <returns></returns>
        internal static string GetContentFromXml(byte[] docContent, string xPath)
        {
            MemoryStream ms = new MemoryStream(docContent);

            using (XmlReader reader = XmlReader.Create(ms))
            {
                XmlDocument doc = new XmlDocument();
                XmlNamespaceManager nsManager;
                try
                {
                    doc.Load(reader);
                }
                catch (XmlException exception)
                {
                    Logger.Error("EBeadvanyFormXmlHelper.GetContentFromXml.Load: " + exception.Message);
                    return null;
                }

                try
                {
                    nsManager = GetAllNamespaces(doc);
                }
                catch (XmlException exception)
                {
                    Logger.Error("EBeadvanyFormXmlHelper.GetContentFromXml.NSManager: " + exception.Message);
                    return null;
                }

                XmlNode node;
                try
                {
                    if (nsManager != null)
                    {
                        // V1A
                        node = XPathFinder.FindNode(xPath, doc, nsManager);

                        // V1B
                        //node = doc.SelectSingleNode(GetReFormattedXPath(xPath), nsManager);
                    }
                    else
                    {
                        node = doc.SelectSingleNode(xPath);
                    }
                }
                catch (XmlException exception)
                {
                    Logger.Error("EBeadvanyFormXmlHelper.GetContentFromXml.SelectNode: " + exception.Message);
                    return null;
                }
                return node?.InnerText;
            }
        }

        /// <summary>
        /// GetAllNamespaces
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        public static XmlNamespaceManager GetAllNamespaces(XmlDocument xDoc)
        {
            XmlNamespaceManager result = new XmlNamespaceManager(xDoc.NameTable);
            try
            {
                IDictionary<string, string> localNamespaces = null;
                XPathNavigator xNav = xDoc.CreateNavigator();
                while (xNav.MoveToFollowing(XPathNodeType.Element))
                {
                    localNamespaces = xNav.GetNamespacesInScope(XmlNamespaceScope.Local);
                    foreach (var localNamespace in localNamespaces)
                    {
                        string prefix = localNamespace.Key;
                        if (string.IsNullOrEmpty(prefix))
                        {
                            prefix = "DEFAULT";
                        }

                        result.AddNamespace(prefix, localNamespace.Value);
                    }
                }
            }
            catch (Exception)
            {

            }
            return result;
        }
        /// <summary>
        /// Reformat XPath query - namespace problem
        /// </summary>
        /// <param name="xPath"></param>
        /// <returns></returns>
        private static string GetReFormattedXPath(string xPath)
        {
            if (string.IsNullOrEmpty(xPath) || !xPath.Contains(":"))
            {
                return xPath;
            }

            string[] splittedEntries = xPath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (splittedEntries == null || splittedEntries.Length < 1)
            {
                return xPath;
            }
            splittedEntries[0] = string.Format("*[name()='" + splittedEntries[0] + "']");

            return string.Join("/ ", splittedEntries);
        }

        /// <summary>
        /// GetDocContent
        /// </summary>
        /// <param name="externalLink"></param>
        /// <returns></returns>
        internal static byte[] GetDocContent(string externalLink)
        {
            using (WebClient wc = new WebClient())
            {
                wc.UseDefaultCredentials = true;
                return wc.DownloadData(externalLink);
            }
        }
    }
}
