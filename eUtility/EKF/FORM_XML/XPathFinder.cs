﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace Contentum.eUtility.EKF.FORM_XML
{
    public class XPathFinder
    {
        private const string XPATH_SEPARATOR = "/";
        private const string XPATH_NAMESPACE_SEPARATOR = ":";

        public class Namespace
        {

            public string Prefix;
            public string Uri;

            public Namespace(string prefix, string uri)
            {
                Prefix = prefix;
                Uri = uri;
            }
        }

        private static XmlNode FindNodeNoException(string xPath,
                                                    XmlDocument document,
                                                    XmlNamespaceManager namespaceManager)
        {
            try
            {
                XmlNode result = document.SelectSingleNode(xPath, namespaceManager);
                return result;
            }
            catch
            {
                return null;
            }
        }

        private static string ConvertXPathPart(string xPathPart)
        {
            if (string.IsNullOrEmpty(xPathPart)) { return xPathPart; }
            if (xPathPart.IndexOf(XPATH_NAMESPACE_SEPARATOR) >= 0) { return xPathPart; }

            // Use name() operator
            var result = $@"*[name()='{xPathPart}']";
            return result;
        }
        /// <summary>
        /// FindNode
        /// </summary>
        /// <param name="xPath"></param>
        /// <param name="document"></param>
        /// <param name="namespaces"></param>
        /// <returns></returns>
        public static XmlNode FindNode(string xPath,
                                       XmlDocument document,
                                       IEnumerable<Namespace> namespaces = null)
        {

            if (document == null) { return null; }
            if (string.IsNullOrEmpty(xPath)) { return null; }

            XmlNamespaceManager namespaceManager = new XmlNamespaceManager(document.NameTable);

            if (namespaces != null)
            {
                foreach (Namespace @namespace in namespaces)
                {
                    namespaceManager.AddNamespace(@namespace.Prefix, @namespace.Uri);
                }
            }

            // Try xPath as it is
            XmlNode result = FindNodeNoException(xPath, document, namespaceManager);
            if (result != null) { return result; }

            // No success, try using name() operator
            // Split xPath and check parts if they have a namespace specified; if not, then use name() operator
            var xPathParts = xPath.Split(new[] { XPATH_SEPARATOR }, StringSplitOptions.None);
            var xPathPartsConverted = new List<string>();
            foreach (var xPathPart in xPathParts)
            {
                xPathPartsConverted.Add(ConvertXPathPart(xPathPart));
            }
            var xPathCorrected = string.Join(XPATH_SEPARATOR, xPathPartsConverted.ToArray());
            result = FindNodeNoException(xPathCorrected, document, namespaceManager);

            return result;
        }
        /// <summary>
        /// FindNode
        /// </summary>
        /// <param name="xPath"></param>
        /// <param name="document"></param>
        /// <param name="namespaceManager"></param>
        /// <returns></returns>
        public static XmlNode FindNode(string xPath,
                                   XmlDocument document,
                                   XmlNamespaceManager namespaceManager)
        {
            if (document == null) { return null; }
            if (string.IsNullOrEmpty(xPath)) { return null; }

            // Try xPath as it is
            XmlNode result = FindNodeNoException(xPath, document, namespaceManager);
            if (result != null) { return result; }

            // No success, try using name() operator
            // Split xPath and check parts if they have a namespace specified; if not, then use name() operator
            var xPathParts = xPath.Split(new[] { XPATH_SEPARATOR }, StringSplitOptions.None);
            var xPathPartsConverted = new List<string>();
            foreach (var xPathPart in xPathParts)
            {
                xPathPartsConverted.Add(ConvertXPathPart(xPathPart));
            }
            var xPathCorrected = string.Join(XPATH_SEPARATOR, xPathPartsConverted.ToArray());
            result = FindNodeNoException(xPathCorrected, document, namespaceManager);

            return result;
        }
    }

}
