﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility.EKF.Models;

namespace Contentum.eUtility.EKF.Helper
{
    public static partial class Converter_EKF
    {
        public static EREC_ObjektumTargyszavai Convert(EKF_ObjTargyszavakModel item)
        {
            EREC_ObjektumTargyszavai result = new EREC_ObjektumTargyszavai();

            result.Base.Note = item.Note;
            result.Base.Updated.Note = true;

            result.Ertek = item.Ertek;
            result.Updated.Ertek = true;

            result.ErvKezd = item.ErvKezd;
            result.Updated.ErvKezd = true;

            result.ErvVege = item.ErvVege;
            result.Updated.ErvVege = true;

            result.Forras = item.Forras;
            result.Updated.Forras = true;

            result.Id = item.Id;
            result.Updated.Id = true;

            result.ObjTip_Id = item.ObjTip_Id;
            result.Updated.ObjTip_Id = true;

            result.Obj_Id = item.Obj_Id;
            result.Updated.Obj_Id = true;

            result.Obj_Metaadatai_Id = item.Obj_Metaadatai_Id;
            result.Updated.Obj_Metaadatai_Id = true;

            result.Sorszam = item.Sorszam;
            result.Updated.Sorszam = true;

            result.SPSSzinkronizalt = item.SPSSzinkronizalt;
            result.Updated.SPSSzinkronizalt = true;

            result.Targyszo = item.Targyszo;
            result.Updated.Targyszo = true;

            result.Targyszo_Id = item.Targyszo_Id;
            result.Updated.Targyszo_Id = true;

            result.Targyszo_XML = item.Targyszo_XML;
            result.Updated.Targyszo_XML = true;

            result.Targyszo_XML_Ervenyes = item.Targyszo_XML_Ervenyes;
            result.Updated.Targyszo_XML_Ervenyes = true;

            return result;

        }
        public static EKF_ObjTargyszavakModel Convert(EREC_ObjektumTargyszavai item)
        {
            EKF_ObjTargyszavakModel result = new EKF_ObjTargyszavakModel();

            result.Note = item.Base.Note;
            result.Ertek = item.Ertek;
            result.ErvKezd = item.ErvKezd;
            result.ErvVege = item.ErvVege;
            result.Forras = item.Forras;
            result.Id = item.Id;
            result.ObjTip_Id = item.ObjTip_Id;
            result.Obj_Id = item.Obj_Id;
            result.Obj_Metaadatai_Id = item.Obj_Metaadatai_Id;
            result.Sorszam = item.Sorszam;
            result.SPSSzinkronizalt = item.SPSSzinkronizalt;
            result.Targyszo = item.Targyszo;
            result.Targyszo_Id = item.Targyszo_Id;
            result.Targyszo_XML = item.Targyszo_XML;
            result.Targyszo_XML_Ervenyes = item.Targyszo_XML_Ervenyes;
            return result;
        }

        public static void Add(ref EKF_ObjTargyszavakModel item, EREC_TargySzavak targyszo)
        {
            item.AlapertelmezettErtek = targyszo.AlapertelmezettErtek;
            item.BelsoAzonosito = targyszo.BelsoAzonosito;
            item.ControlTypeDataSource = targyszo.ControlTypeDataSource;
            item.ControlTypeSource = targyszo.ControlTypeSource;
            item.Csoport_Id_Tulaj = targyszo.Csoport_Id_Tulaj;
            item.Targyszo_Id = targyszo.Id;
            item.RegExp = targyszo.RegExp;
            item.Targyszo_SPSSzinkronizalt = targyszo.SPSSzinkronizalt;
            item.Targyszo_SPS_Field_Id = targyszo.SPS_Field_Id;
            item.TargySzo_Targyszavak = targyszo.TargySzavak;
            item.Targyszo_Id_Parent = targyszo.Targyszo_Id_Parent;
            item.Tipus = targyszo.Tipus;
            item.ToolTip = targyszo.ToolTip;
            item.Targyszo_XSD = targyszo.XSD;
        }
    }
}
