﻿using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Contentum.eUtility.EKF.Helper
{
    public class KRXHelper
    {
        public static void LoadKRXMetaAdatok(ExecParam xpm, string eBeadvanyokId, EREC_KuldKuldemenyek kuldKuldemeny)
        {
            try
            {
                string metaDokId = GetKRXMetaDokId(xpm, eBeadvanyokId);

                byte[] metaContent = GetDokContent(xpm, metaDokId);
                Contentum.eUtility.KRX.KULDEMENY_META.KULDEMENY kuldemeny = Contentum.eUtility.KRX.KRXFileManager.GetMetaAdatok(metaContent);

                if (kuldemeny != null)
                {
                    if (kuldemeny.FEJRESZ != null)
                    {
                        kuldKuldemeny.HivatkozasiSzam = kuldemeny.FEJRESZ.KULDEMENY_AZONOSITO;
                    }
                    if (kuldemeny.EXPEDIALASOK != null && kuldemeny.EXPEDIALASOK.EXPEDIALAS != null)
                    {
                        kuldKuldemeny.NevSTR_Bekuldo = kuldemeny.EXPEDIALASOK.EXPEDIALAS.KULDO_NEV;
                        kuldKuldemeny.CimSTR_Bekuldo = kuldemeny.EXPEDIALASOK.EXPEDIALAS.KULDO_CIM;
                        kuldKuldemeny.Targy = kuldemeny.EXPEDIALASOK.EXPEDIALAS.TARGY;

                        KRX.KuldesModjaConverter converter = new KRX.KuldesModjaConverter(xpm, System.Web.HttpRuntime.Cache);
                        string kod = converter.GetKod(kuldemeny.EXPEDIALASOK.EXPEDIALAS.KEZBESITES_MODJA);

                        if (!String.IsNullOrEmpty(kod))
                        {
                            kuldKuldemeny.KuldesMod = kod;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Result error = ResultException.GetResultFromException(ex);
                Logger.Error("LoadKRXMetaAdatok hiba!", xpm, error);
                throw;
            }
        }

        private static string GetKRXMetaDokId(ExecParam xpm, string eBeadvanyokId)
        {
            try
            {
                EREC_eBeadvanyCsatolmanyokService eBeadvanyCsatService = eRecordService.ServiceFactory.GetEREC_eBeadvanyCsatolmanyokService();
                EREC_eBeadvanyCsatolmanyokSearch eBeadvanyCsatSearch = new EREC_eBeadvanyCsatolmanyokSearch();
                eBeadvanyCsatSearch.eBeadvany_Id.Value = eBeadvanyokId;
                eBeadvanyCsatSearch.eBeadvany_Id.Operator = Query.Operators.equals;

                Result eBeadvanyCsatResult = eBeadvanyCsatService.GetAll(xpm, eBeadvanyCsatSearch);

                if (eBeadvanyCsatResult.IsError)
                {
                    throw new ResultException(eBeadvanyCsatResult);
                }
                string kuldemenyMetaDokId = String.Empty;

                foreach (DataRow row in eBeadvanyCsatResult.Ds.Tables[0].Rows)
                {
                    string nev = row["Nev"].ToString();

                    if ("KULDEMENY_META.xml".Equals(nev, StringComparison.CurrentCultureIgnoreCase))
                    {
                        kuldemenyMetaDokId = row["Dokumentum_Id"].ToString();
                        break;
                    }
                }

                if (String.IsNullOrEmpty("kuldemenyMetaDokId"))
                {
                    throw new Exception("KULDEMENY_META.xml nem található!");
                }

                return kuldemenyMetaDokId;
            }
            catch (Exception ex)
            {
                Result error = ResultException.GetResultFromException(ex);
                Logger.Error("GetKRXMetaDokId hiba!", xpm, error);
                throw;
            }
        }

        static byte[] GetDokContent(ExecParam xpm, string dokumentumId)
        {
            try
            {
                KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                xpm.Record_Id = dokumentumId;
                Result dokGetResult = dokService.Get(xpm);

                if (dokGetResult.IsError)
                {
                    throw new ResultException(dokGetResult);
                }

                string externalLink = (dokGetResult.Record as KRT_Dokumentumok).External_Link;

                using (System.Net.WebClient wc = new System.Net.WebClient())
                {
                    wc.Credentials = new System.Net.NetworkCredential(UI.GetAppSetting("eSharePointBusinessServiceUserName"), UI.GetAppSetting("eSharePointBusinessServicePassword"), UI.GetAppSetting("eSharePointBusinessServiceUserDomain"));
                    return wc.DownloadData(externalLink);
                }
            }
            catch (Exception ex)
            {
                Result error = ResultException.GetResultFromException(ex);
                Logger.Error("GetDokContent hiba!", xpm, error);
                throw;
            }
        }
    }
}
