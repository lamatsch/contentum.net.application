﻿using Contentum.eBusinessDocuments;
using System.Data;

namespace Contentum.eUtility.EKF
{
    /// <summary>
    /// Interface a művelet végrehajtásához
    /// </summary>
    public interface IEKFOperationExecute
    {
        bool Execute(EKFRuleClass rule, DataSet inputData, out Result result);
    }
}

