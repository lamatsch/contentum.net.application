﻿using System;
using System.Collections.Generic;

namespace Contentum.eUtility.EKF.Models
{
    /// <summary>
    /// Model a Targy mezo meghatarozasara
    /// </summary>
    public class EKF_ComplexTargyModel
    {
        public string Id { get; set; }
        public byte SorSzam { get; set; }

        public string Prefix { get; set; }
        public string Postfix { get; set; }

        public string Type { get; set; }

        public string Value_TXT { get; set; }
        public string Value_XML { get; set; }
        public string Value_DB { get; set; }
      
        public EKF_ComplexTargyModel()
        {
            Id = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Objektum érvényesen kitöltött ?
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            if (string.IsNullOrEmpty(Type))
                return false;

            if (string.IsNullOrEmpty(Value_DB) && string.IsNullOrEmpty(Value_TXT) && string.IsNullOrEmpty(Value_XML))
                return false;

            return true;
        }

        public override bool Equals(object obj)
        {
            var model = obj as EKF_ComplexTargyModel;
            return model != null &&
                   Id == model.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + EqualityComparer<string>.Default.GetHashCode(Id);
        }
    }
}

