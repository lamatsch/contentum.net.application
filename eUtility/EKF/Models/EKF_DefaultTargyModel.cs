﻿using System;
using System.Collections.Generic;

namespace Contentum.eUtility.EKF.Models
{
    /// <summary>
    /// Model a default Targy mezo meghatarozasara
    /// </summary>
    public class EKF_DefaultTargyModel
    {
        public string Id { get; set; }

        public string Type { get; set; }

        public string Value_TXT { get; set; }
        public string Value_XML { get; set; }
        public string Value_DB { get; set; }
      
        public EKF_DefaultTargyModel()
        {
            Id = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Objektum érvényesen kitöltött ?
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            if (string.IsNullOrEmpty(Type))
                return false;

            if (string.IsNullOrEmpty(Value_DB) && string.IsNullOrEmpty(Value_TXT) && string.IsNullOrEmpty(Value_XML))
                return false;

            return true;
        }

        public override bool Equals(object obj)
        {
            var model = obj as EKF_DefaultTargyModel;
            return model != null &&
                   Id == model.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + EqualityComparer<string>.Default.GetHashCode(Id);
        }
    }
}

