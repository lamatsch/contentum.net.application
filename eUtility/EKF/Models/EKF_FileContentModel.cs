﻿using System.Collections.Generic;

namespace Contentum.eUtility.EKF.Models
{
    /// <summary>
    /// EKF_FileContentModel
    /// </summary>
    public class EKF_FileContentModel
    {
        public EKF_FileContentModel(string fileName, byte[] content)
        {
            FileName = fileName;
            Content = content;
        }

        public string FileName { get; set; }
        public byte[] Content { get; set; }

        public override bool Equals(object obj)
        {
            var model = obj as EKF_FileContentModel;
            return model != null &&
                   FileName == model.FileName;
        }

        public override int GetHashCode()
        {
            return 901043656 + EqualityComparer<string>.Default.GetHashCode(FileName);
        }
    }
}

