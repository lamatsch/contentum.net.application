﻿namespace Contentum.eUtility.EKF.Models
{
    public class EKF_ObjTargyszavakModel
    {
        public string Id { get; set; }
        public string Targyszo_Id { get; set; }
        public string Obj_Metaadatai_Id { get; set; }
        public string Obj_Id { get; set; }
        public string ObjTip_Id { get; set; }
        public string Targyszo { get; set; }
        public string Forras { get; set; }
        public string SPSSzinkronizalt { get; set; }
        public string Ertek { get; set; }
        public string Sorszam { get; set; }
        public string Targyszo_XML { get; set; }
        public string Targyszo_XML_Ervenyes { get; set; }
        public string ErvKezd { get; set; }
        public string ErvVege { get; set; }
        public string Note { get; set; }
        public string Ismetlodo { get; set; }
        public string Opcionalis { get; set; }
        

        #region TARGYSZO
        public string Tipus { get; set; }
        public string TargySzo_Targyszavak { get; set; }
        public string Csoport_Id_Tulaj { get; set; }
        public string RegExp { get; set; }
        public string ToolTip { get; set; }
        public string ControlTypeSource { get; set; }
        public string ControlTypeDataSource { get; set; }
        public string Targyszo_Id_Parent { get; set; }
        public string Targyszo_XSD { get; set; }
        public string AlapertelmezettErtek { get; set; }
        public string BelsoAzonosito { get; set; }
        public string Targyszo_SPSSzinkronizalt { get; set; }
        public string Targyszo_SPS_Field_Id { get; set; }
        #endregion
    }
}

