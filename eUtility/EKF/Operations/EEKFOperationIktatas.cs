﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Data;
using System.Linq;

namespace Contentum.eUtility.EKF
{
    public class EKFMainOperationIktatas : EKFBaseOperation
    {
        private string TargyProperty;
        public EKFMainOperationIktatas()
        {
            OperationName = EnumEKFOperationName.IKTATAS;
            TypeFullName = this.GetType().FullName;
            OperationType = EnumEKFOperationType.MAIN;
        }
        public void InitParameters()
        {
            foreach (Enum_ParameterNames_Main_Iktatas name in Enum.GetValues(typeof(Enum_ParameterNames_Main_Iktatas)))
            {
                ParameterProperty.Parameters.Add(new EKFOperationParameterValue()
                {
                    IdProperty = name.ToString(),
                    NameProperty = name.ToString(),
                });
            }
        }
        public override string ToString()
        {
            return OperationName.ToString();
        }
        private string GetErkeztetoKonyvId(ExecParam param)
        {
            string erkeztetoKonyv_Id = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.ErkeztetoKonyv.ToString());
            string erkeztetoHely = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.ErkeztetoKonyvHely.ToString());
            if (!string.IsNullOrEmpty(erkeztetoKonyv_Id))
            {
                return erkeztetoKonyv_Id;
            }
            else if (!string.IsNullOrEmpty(erkeztetoHely))
            {
                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam xparamEBeadvGet = param;
                EREC_IraIktatoKonyvekSearch src = new EREC_IraIktatoKonyvekSearch();
                src.Ev.Value = DateTime.Now.Year.ToString();
                src.Ev.Operator = Query.Operators.equals;

                src.MegkulJelzes.Value = erkeztetoHely;
                src.MegkulJelzes.Operator = Query.Operators.equals;

                src.IktatoErkezteto.Value = "E";
                src.IktatoErkezteto.Operator = Query.Operators.equals;
                src.TopRow = 1;

                Result result = service.GetAll(param, src);
                if (!string.IsNullOrEmpty(result.ErrorCode))
                {
                    AddMessage(result);
                    throw new ResultException(result);
                }
                if (result.Ds.Tables[0].Rows.Count < 1)
                {
                    AddMessage("EKF: " + string.Format("Nem található érkeztetőkönvy {0}", erkeztetoHely));
                    throw new Exception(string.Format("Nem található érkeztetőkönvy {0}", erkeztetoHely));
                }
                return result.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            else
            {
                AddMessage("EKF: Érkeztetőkönyv nem található ");
                throw new Exception("Érkeztetőkönyv nem található !");
            }
        }
        private string GetIktatoKonyvId(ExecParam param)
        {
            string iktatoKonyvId = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.Iktatokonyv.ToString());
            string Iktatohely = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.IktatoKonyvHely.ToString());
            if (!string.IsNullOrEmpty(iktatoKonyvId))
            {
                return iktatoKonyvId;
            }
            else if (!string.IsNullOrEmpty(Iktatohely))
            {
                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam xparamEBeadvGet = param;
                EREC_IraIktatoKonyvekSearch src = new EREC_IraIktatoKonyvekSearch();
                src.Ev.Value = DateTime.Now.Year.ToString();
                src.Ev.Operator = Query.Operators.equals;

                src.Iktatohely.Value = Iktatohely;
                src.Iktatohely.Operator = Query.Operators.equals;

                src.IktatoErkezteto.Value = "I";
                src.IktatoErkezteto.Operator = Query.Operators.equals;
                src.TopRow = 1;

                Result result = service.GetAll(param, src);
                if (!string.IsNullOrEmpty(result.ErrorCode))
                {
                    AddMessage(result);
                    throw new ResultException(result);
                }
                if (result.Ds.Tables[0].Rows.Count < 1)
                {
                    AddMessage("EKF: " + string.Format("Nem található iktatókönvy {0}", Iktatohely));
                    throw new Exception(string.Format("Nem található iktatókönvy {0}", Iktatohely));
                }
                return result.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            return null;
        }

        public override bool Execute(EKFRuleClass rule, DataSet inputData, out Result result)
        {
            if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.IraIrat_Id.ToString())))
            {
                AddMessage("EKF: Iktatás nem lehetséges, mert már be van iktatva ");
                AddMessage(Environment.NewLine);
                throw new Exception("Iktatás nem lehetséges, mert már be van iktatva !");
            }

            result = null;

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam xparamIr = rule.GetExecParam();

            string eBeadvanyId = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Id.ToString());
            string iraIktatokonyv_Id = GetIktatoKonyvId(rule.GetExecParam());
            string erkeztetoKonyv_Id = GetErkeztetoKonyvId(rule.GetExecParam());

            #region EVAL TARGY
            AddMessage("EKF: Targy meghatarozas indul");
            AddMessage(Environment.NewLine);
            EKFManagerEvaluateTargyFormula targyEvaluation = new EKFManagerEvaluateTargyFormula(eBeadvanyId, rule, inputData);
            TargyProperty = targyEvaluation.EvaluateTargyFormula();
            if (TargyProperty.Length > 4000)
                TargyProperty = TargyProperty.Substring(0, 3999);

            if (!string.IsNullOrEmpty(TargyProperty))
            {
                AddMessage("EKF: Targy erteke:" + TargyProperty);
                AddMessage(Environment.NewLine);
            }
            #endregion

            #region ELOZMENY UGY
            string elozmenyIratid = null;
            string elozmenyUgyIratid = null;
            DataRow elozmenyUgyiratRow = null;
            Enum_Iktatas_Tipus tipus = CheckElozmenyUgyirat(rule, inputData, out elozmenyIratid, out elozmenyUgyIratid, out elozmenyUgyiratRow);

            if (elozmenyUgyiratRow != null && !string.IsNullOrEmpty(elozmenyUgyiratRow["IraIktatokonyv_Id"].ToString()))
                iraIktatokonyv_Id = elozmenyUgyiratRow["IraIktatokonyv_Id"].ToString();

            if (string.IsNullOrEmpty(iraIktatokonyv_Id))
            {
                AddMessage("EKF: Iktatókönyv nincs megadva !");
                AddMessage(Environment.NewLine);
                throw new Exception("Iktatókönyv nincs megadva !");
            }
            #endregion

            #region ELOZMENY KULDEMENY
            string elozmenykuldemenyId = null;
            string elozmenyErkeztetoKonyvId = null;
            DataRow elozmenyKuldemenyRow = null;
            Enum_Erkeztetes_Tipus tipusErk = CheckElozmenyKuldemeny(rule, inputData, out elozmenykuldemenyId, out elozmenyErkeztetoKonyvId, out elozmenyKuldemenyRow);
            if (!string.IsNullOrEmpty(elozmenyErkeztetoKonyvId))
                erkeztetoKonyv_Id = elozmenyErkeztetoKonyvId;

            if (string.IsNullOrEmpty(erkeztetoKonyv_Id))
            {
                AddMessage("EKF: Érkeztetőkönyv nincs megadva !");
                AddMessage(Environment.NewLine);
                throw new Exception("Érkeztetőkönyv nincs megadva !");
            }
            #endregion

            string iratId = null;
            string kuldemenyId = null;

            switch (tipus)
            {
                case Enum_Iktatas_Tipus.NINCS:
                case Enum_Iktatas_Tipus.Foszamra:
                    AddMessage("EKF: Iktatas FoSzamra indul");
                    AddMessage(Environment.NewLine);
                    IktatasFoSzamra(rule, inputData, out result, iraIratokService, xparamIr, eBeadvanyId, iraIktatokonyv_Id, erkeztetoKonyv_Id, out iratId, out kuldemenyId);
                    break;
                case Enum_Iktatas_Tipus.Alszamra:
                    AddMessage("EKF: Iktatas Alszamra indul");
                    AddMessage(Environment.NewLine);
                    IktatasAlszamra(rule, inputData, elozmenyUgyiratRow, erkeztetoKonyv_Id, iraIktatokonyv_Id, out result, out iratId, out kuldemenyId);
                    break;
                default:
                    AddMessage("EKF: Iktatas FoSzamra indul");
                    AddMessage(Environment.NewLine);
                    IktatasFoSzamra(rule, inputData, out result, iraIratokService, xparamIr, eBeadvanyId, iraIktatokonyv_Id, erkeztetoKonyv_Id, out iratId, out kuldemenyId);
                    break;
            }

            #region GET EBEADV ITEM
            EREC_eBeadvanyok eBeadvany = new EKFManager(xparamIr).GetEBeadvanyByDataSet(inputData);
            if (eBeadvany == null)
                return false;
            #endregion  GET EBEADV ITEM

            EREC_eBeadvanyokService eBeadvService = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();

            #region UPDATE EBEADV

            ExecParam xparamEBeadvUpdate = rule.GetExecParam();
            xparamEBeadvUpdate.Record_Id = eBeadvany.Id;
            if (eBeadvany.KuldKuldemeny_Id == null)
                eBeadvany.KuldKuldemeny_Id = kuldemenyId;
            eBeadvany.IraIrat_Id = iratId;
            Result resultEBeadvUpdate = eBeadvService.Update(xparamEBeadvUpdate, eBeadvany);
            #endregion UPDATE EBEADV

            if (!string.IsNullOrEmpty(resultEBeadvUpdate.ErrorCode))
            {
                AddMessage(resultEBeadvUpdate);
                throw new ResultException(resultEBeadvUpdate);
            }

            try
            {
                EnsureKezbesitesiTetelAtadasraKerult(rule);
            }
            catch (ResultException e)
            {
                AddMessage(e);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Előzmény ügy ellenőrzés
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="inputData"></param>
        /// <param name="iratId"></param>
        /// <param name="ugyiratId"></param>
        /// <param name="ugyiratRow"></param>
        /// <returns></returns>
        public Enum_Iktatas_Tipus CheckElozmenyUgyirat(EKFRuleClass rule, DataSet inputData, out string iratId, out string ugyiratId, out DataRow ugyiratRow)
        {
            Enum_Iktatas_Tipus iktatasTipus = Enum_Iktatas_Tipus.NINCS;
            ugyiratId = null;
            iratId = null;
            ugyiratRow = null;

            string elozmenyId = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Contentum_HivatkozasiSzam.ToString());
            if (string.IsNullOrEmpty(elozmenyId))
                return Enum_Iktatas_Tipus.NINCS;
            else
            {
                EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                ExecParam xparamIra = rule.GetExecParam();
                EREC_IraIratokSearch srcIra = new EREC_IraIratokSearch();
                srcIra.Id.Value = elozmenyId;
                srcIra.Id.Operator = Query.Operators.equals;
                srcIra.TopRow = 1;
                Result resultIra = iraIratokService.GetAll(xparamIra, srcIra);
                if (string.IsNullOrEmpty(resultIra.ErrorCode) && resultIra.Ds.Tables[0].Rows.Count > 0)
                {
                    iratId = resultIra.Ds.Tables[0].Rows[0]["Id"].ToString();
                    ugyiratId = resultIra.Ds.Tables[0].Rows[0]["Ugyirat_Id"].ToString();

                    ExecParam xparamUgyGet = rule.GetExecParam();
                    EREC_UgyUgyiratokSearch srcUgyGet = new EREC_UgyUgyiratokSearch();
                    srcUgyGet.Id.Value = ugyiratId;
                    srcUgyGet.Id.Operator = Query.Operators.equals;
                    srcUgyGet.TopRow = 1;
                    EREC_UgyUgyiratokService ugyIratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    Result resultUgyGet = ugyIratokService.GetAll(xparamUgyGet, srcUgyGet);
                    if (string.IsNullOrEmpty(resultUgyGet.ErrorCode) && resultUgyGet.Ds.Tables[0].Rows.Count > 0)
                    {
                        ugyiratRow = resultUgyGet.Ds.Tables[0].Rows[0];
                    }
                    return Enum_Iktatas_Tipus.Alszamra;
                }
            }
            return iktatasTipus;
        }
        /// <summary>
        /// CheckElozmenyKuldemeny
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="inputData"></param>
        /// <param name="kuldemenyId"></param>
        /// <param name="erkeztetoKonyvId"></param>
        /// <param name="kuldemenyRow"></param>
        /// <returns></returns>
        public Enum_Erkeztetes_Tipus CheckElozmenyKuldemeny(EKFRuleClass rule, DataSet inputData, out string kuldemenyId, out string erkeztetoKonyvId, out DataRow kuldemenyRow)
        {
            kuldemenyId = null;
            erkeztetoKonyvId = null;
            kuldemenyRow = null;

            string elozmenyId = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Contentum_HivatkozasiSzam.ToString());
            if (string.IsNullOrEmpty(elozmenyId))
                return Enum_Erkeztetes_Tipus.NINCS;
            else
            {
                EREC_KuldKuldemenyekService kuldService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                ExecParam xparamIra = rule.GetExecParam();
                EREC_KuldKuldemenyekSearch srcKuld = new EREC_KuldKuldemenyekSearch();
                srcKuld.Id.Value = elozmenyId;
                srcKuld.Id.Operator = Query.Operators.equals;
                srcKuld.TopRow = 1;
                Result resultKuld = kuldService.GetAll(xparamIra, srcKuld);
                if (string.IsNullOrEmpty(resultKuld.ErrorCode) && resultKuld.Ds.Tables[0].Rows.Count > 0)
                {
                    kuldemenyId = resultKuld.Ds.Tables[0].Rows[0]["Id"].ToString();
                    erkeztetoKonyvId = resultKuld.Ds.Tables[0].Rows[0]["IraIktatokonyv_Id"].ToString();
                    kuldemenyRow = resultKuld.Ds.Tables[0].Rows[0];

                    return Enum_Erkeztetes_Tipus.VAN;
                }
            }
            return Enum_Erkeztetes_Tipus.NINCS;
        }


        /// <summary>
        /// IktatasFoSzamra
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="inputData"></param>
        /// <param name="result"></param>
        /// <param name="iraIratokService"></param>
        /// <param name="xparamIr"></param>
        /// <param name="eBeadvanyId"></param>
        /// <param name="iraIktatokonyv_Id"></param>
        /// <param name="erkeztetoKonyv_Id"></param>
        /// <param name="iratId"></param>
        /// <param name="kuldemenyId"></param>
        private void IktatasFoSzamra(EKFRuleClass rule, DataSet inputData, out Result result, EREC_IraIratokService iraIratokService, ExecParam xparamIr, string eBeadvanyId, string iraIktatokonyv_Id, string erkeztetoKonyv_Id, out string iratId, out string kuldemenyId)
        {
            #region INIT
            Logger.Info("EKFMainOperationIktatas.IktatasFoSzamra.Init.Start");

            #region IDS
            string ugyiratId = Guid.NewGuid().ToString();
            iratId = Guid.NewGuid().ToString();
            kuldemenyId = Guid.NewGuid().ToString();
            #endregion IDS

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = GetNewKuldemeny(rule, inputData, erkeztetoKonyv_Id, kuldemenyId);
            #endregion EREC_KULDKULDEMENYEK

            #region EREC_UGYUGYIRATOK
            EREC_UgyUgyiratok ugyirat = GetNewUgyIrat(rule, inputData, ugyiratId);
            #endregion EREC_UGYUGYIRATOK

            #region EREC_IRAIRATOK
            EREC_IraIratok irat = GetNewIrat(rule, inputData, iratId);
            #endregion EREC_IRAIRATOK

            #region IKTATASIPARAMETEREK
            IktatasiParameterek iktatasParameterek = GetIktatasParameterek();
            #endregion IKTATASIPARAMETEREK

            ErkeztetesParameterek erkeztetesParameterek = new ErkeztetesParameterek();
            EREC_UgyUgyiratdarabok darabok = new EREC_UgyUgyiratdarabok();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            Logger.Info("EKFMainOperationIktatas.IktatasFoSzamra.Init.Stop");
            #endregion

            #region ERKEZTETES + IKTATAS
            Logger.Info("EKFMainOperationIktatas.IktatasFoSzamra.Start");
            string ugyintezo = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.Ugyirat_Ugyintezo.ToString());
            if (!string.IsNullOrEmpty(ugyintezo))
            {
                xparamIr.Felhasznalo_Id = ugyintezo;
            }
            else
            {
                xparamIr.Felhasznalo_Id = rule.ParametersProperty.UserIdProperty.ToString();
            }

            result = iraIratokService.HivataliKapusErkeztetesIktatas(
                   xparamIr,
                   kuldKuldemeny,
                   eBeadvanyId,
                   iraIktatokonyv_Id,
                   ugyirat,
                   darabok,
                   irat,
                   feladatok,
                   iktatasParameterek,
                   erkeztetesParameterek);

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage(result);
                throw new ResultException(result);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                AddMessage(string.Format("UGY-AZ: {0}, IRAT-AZ: {1}, KULD-AZ: {2}", erkeztetesIktatasResult.UgyiratAzonosito, erkeztetesIktatasResult.IratAzonosito, erkeztetesIktatasResult.KuldemenyAzonosito));
            }

            Logger.Info("EKFMainOperationIktatas.IktatasFoSzamra.Stop");
            #endregion ERKEZTETES + IKTATAS
        }
        /// <summary>
        /// IktatasAlszamra
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="inputData"></param>
        /// <param name="ugyIrat"></param>
        /// <param name="erkeztetoKonyvId"></param>
        /// <param name="iktatoKonyvId"></param>
        /// <param name="result"></param>
        private void IktatasAlszamra(EKFRuleClass rule, DataSet inputData, DataRow ugyIrat, string erkeztetoKonyvId, string iktatoKonyvId, out Result result, out string iratId, out string kuldemenyId)
        {
            #region INIT
            Logger.Info("EKFMainOperationIktatas.IktatasAlszamra.Init.Start");
            result = null;

            iratId = Guid.NewGuid().ToString();
            kuldemenyId = Guid.NewGuid().ToString();

            #region EREC_IRAIRATOK
            EREC_IraIratok newIrat = GetNewIrat(rule, inputData, iratId);
            #endregion EREC_IRAIRATOK

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = GetNewKuldemeny(rule, inputData, erkeztetoKonyvId, kuldemenyId);
            #endregion EREC_KULDKULDEMENYEK

            #region UGYIRAT
            EREC_UgyUgyiratok existingUgyirat = GetUgyIratById(rule, ugyIrat["Id"].ToString());
            #endregion

            Logger.Info("EKFMainOperationIktatas.IktatasAlszamra.Init.Stop");
            #endregion

            #region BELSO IRAT IKTATASA
            Logger.Info("EKFMainOperationIktatas.IktatasAlszamra.Start");
            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam xparam = rule.GetExecParam();
            result = iraIratokService.EgyszerusitettIktatasa_Alszamra(
                    xparam,
                    iktatoKonyvId,
                    existingUgyirat.Id,
                    new EREC_UgyUgyiratdarabok(),
                    newIrat,
                    new EREC_HataridosFeladatok(),
                    kuldKuldemeny,
                    GetIktatasParameterek(),
                    new ErkeztetesParameterek()
                    );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage("EKF: " + result.ErrorCode + " " + result.ErrorMessage);
                throw new ResultException(result);
            }
            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
            }

            Logger.Info("EKFMainOperationIktatas.IktatasAlszamra. Stop");
            #endregion BELSO IRAT IKTATASA
        }
        /// <summary>
        /// GetIktatasParameterek
        /// </summary>
        /// <returns></returns>
        private IktatasiParameterek GetIktatasParameterek()
        {
            IktatasiParameterek iktatasParameterek = new IktatasiParameterek();
            iktatasParameterek.IratpeldanyVonalkodGeneralasHaNincs = false;
            iktatasParameterek.Adoszam = null;
            //if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.IrattariTelel.ToString())))
            //    iktatasParameterek.UgykorId = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.IrattariTelel.ToString());

            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_Ugykor.ToString())))
                iktatasParameterek.UgykorId = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_Ugykor.ToString());

            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_UgyTipus.ToString())))
                iktatasParameterek.Ugytipus = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_UgyTipus.ToString());
            else
                iktatasParameterek.Ugytipus = EKFConstants.UgyTipusDefault;

            iktatasParameterek.UgyiratUjranyitasaHaLezart = false;
            iktatasParameterek.UgyiratPeldanySzukseges = false;
            iktatasParameterek.KeszitoPeldanyaSzukseges = false;
            iktatasParameterek.Atiktatas = false;
            iktatasParameterek.EmptyUgyiratSztorno = false;
            iktatasParameterek.MunkaPeldany = false;
            return iktatasParameterek;
        }
        /// <summary>
        /// GetNewUgyIrat
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="inputData"></param>
        /// <param name="ugyiratId"></param>
        /// <returns></returns>
        private EREC_UgyUgyiratok GetNewUgyIrat(EKFRuleClass rule, DataSet inputData, string ugyiratId)
        {
            EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();
            ugyirat.Id = ugyiratId;
            ugyirat.Targy = TargyProperty;

            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_UgyTipus.ToString())))
                ugyirat.UgyTipus = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_UgyTipus.ToString());
            else
                ugyirat.UgyTipus = EKFConstants.UgyTipusDefault;
            //rule.ParametersProperty.UserGroupProperty.ToString();

            string irattariTetelSzam = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.IrattariTelel.ToString());
            string irattariTetelId;
            if (GetIrattariTetelId(rule, irattariTetelSzam, out irattariTetelId))
            {
                if (!string.IsNullOrEmpty(irattariTetelId))
                    ugyirat.IraIrattariTetel_Id = irattariTetelId;
            }

            ugyirat.NevSTR_Ugyindito = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerNev.ToString());

            // BUG_11574
            ugyirat.Partner_Id_Ugyindito = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Partner_Id.ToString());
            ugyirat.Cim_Id_Ugyindito = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Cim_Id.ToString());

            bool isCimIdValid = !string.IsNullOrEmpty(ugyirat.Cim_Id_Ugyindito) && Utils.IsValidGuid(ugyirat.Cim_Id_Ugyindito);
            bool isPartnerIDValid = !string.IsNullOrEmpty(ugyirat.Partner_Id_Ugyindito) && Utils.IsValidGuid(ugyirat.Partner_Id_Ugyindito);
            //valid a cim_id
            if (isCimIdValid)
            {
                ugyirat.CimSTR_Ugyindito = CimUtility.GetCimSTRById(rule.GetExecParam(), ugyirat.Cim_Id_Ugyindito);
            }
            //ha nincs cim_id,de valid a partner akkor szedjuk ki a partner alapjan a cimet
            if (!isCimIdValid && isPartnerIDValid)
            {
                ugyirat.Cim_Id_Ugyindito = CimUtility.GetPartnerCimId(rule.GetExecParam(), ugyirat.Partner_Id_Ugyindito);
            }
            //ha a cim_id is invalid és a partner_id is invalid akkor a kuldemény adataiból olvassuk ki a cimstr-t.
            if (!isCimIdValid && !isPartnerIDValid)
            {
                ugyirat.CimSTR_Ugyindito = EKFManager.GetCimSTR_BekuldoFromKuldemenyData(inputData);
            }
           

            #region KEZELO
            string ugyFelelos = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.Ugyirat_Ugyintezo.ToString());
            if (string.IsNullOrEmpty(ugyFelelos))
            {
                ugyFelelos = rule.ParametersProperty.UserIdProperty.ToString();
            }
            // KEZELÕ:
            ugyirat.Csoport_Id_Felelos = ugyFelelos;
            #endregion

            #region UGYINTEZO
            //string ugyintezo = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.Ugyirat_Ugyintezo.ToString());
            //if (string.IsNullOrEmpty(ugyintezo))
            //{
            //    ugyintezo = rule.ParametersProperty.UserIdProperty.ToString();
            //}
            //ugyirat.FelhasznaloCsoport_Id_Ugyintez = ugyintezo;
            #endregion

            // KEZELÕ:
            //EREC_UGYUGYIRATOK.Csoport_Id_Felelos;
            //ÜGYINTÉZÕ
            //EREC_UGYUGYIRATOK.FelhasznaloCsoport_Id_Ugyintez;
            // ÜGYFELELÕS:
            //EREC_UGYUGYIRATOK.Csoport_Id_Ugyfelelos

            //rule.ParametersProperty.UserIdProperty.ToString();
            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_UgyFajta.ToString())))
                ugyirat.Ugy_Fajtaja = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_UgyFajta.ToString());
            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_UgyTipus.ToString())))
            {
                ugyirat.UgyTipus = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_UgyTipus.ToString());

                string ugykor = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_Ugykor.ToString());
                if (!string.IsNullOrEmpty(ugyirat.UgyTipus) && !string.IsNullOrEmpty(ugykor))
                {
                    string ugyfajta = EKFManager.GetUgyFajtaFromUgyTipus(rule, ugyirat.UgyTipus, ugykor);
                    if (!string.IsNullOrEmpty(ugyfajta))
                        ugyirat.Ugy_Fajtaja = ugyfajta;
                }
            }

            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.IntezesiIdo.ToString())))
                ugyirat.IntezesiIdo = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.IntezesiIdo.ToString());
            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.IntezesiIdoegyseg.ToString())))
                ugyirat.IntezesiIdoegyseg = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.IntezesiIdoegyseg.ToString());

            #region HATARIDO
            string errorMessageHatarido = null;
            EKFManager.SetUgyiratHatarido(rule.GetExecParam(), ref ugyirat, out errorMessageHatarido);
            if (!string.IsNullOrEmpty(errorMessageHatarido))
            {
                AddMessage("Ugyirat.Hatarido hiba:" + errorMessageHatarido);
                Logger.Error("ElektronikusKuldemenyFeldolgozas.Iktatas.Ugyirat.Hatarido hiba:" + errorMessageHatarido);
            }
            #endregion

            ugyirat.ElintezesMod = KodTarak.ELINTEZESMOD.Egyeb;
            ugyirat.UgyintezesModja = KodTarak.ELSODLEGES_ADATHORDOZO.Elektronikus_irat;
            ////ugyirat.Alkalmazas_Id = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.SzakrendszerKod.ToString());

            ugyirat.Csoport_Id_Ugyfelelos = GetCsoport_Id_Ugyfelelos(rule);

            return ugyirat;
        }
        /// <summary>
        /// GetNewKuldemeny
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="erkeztetoKonyv_Id"></param>
        /// <param name="kuldemenyId"></param>
        /// <returns></returns>
        private EREC_KuldKuldemenyek GetNewKuldemeny(EKFRuleClass rule, DataSet inputData, string erkeztetoKonyv_Id, string kuldemenyId)
        {
            EREC_KuldKuldemenyek kuldKuldemeny = new EREC_KuldKuldemenyek();
            kuldKuldemeny.Id = kuldemenyId;
            kuldKuldemeny.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            //kuldKuldemeny.ElsodlegesAdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;

            EKFManager.EBeadvanyKuldemenyBeerkezesIdoMeghatarozas(rule.GetExecParam(), inputData, ref kuldKuldemeny);

            kuldKuldemeny.HivatkozasiSzam = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.KR_ErkeztetesiSzam.ToString());
            kuldKuldemeny.Targy = TargyProperty;
            kuldKuldemeny.IraIktatokonyv_Id = erkeztetoKonyv_Id;
            kuldKuldemeny.NevSTR_Bekuldo = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerNev.ToString());
            kuldKuldemeny.CimSTR_Bekuldo = EKFManager.GetCimSTR_BekuldoFromKuldemenyData(inputData);

            EKFManager.SetKuldemenyKuldesModja(rule, ref kuldKuldemeny);

            kuldKuldemeny.Surgosseg = KodTarak.SURGOSSEG.Normal;
            kuldKuldemeny.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            kuldKuldemeny.Erkeztetes_Ev = DateTime.Now.Year.ToString();
            kuldKuldemeny.PeldanySzam = "1";
            kuldKuldemeny.Munkaallomas = EKFConstants.EnumEKFMunkaallomas.Automatikus_Erkezteto.ToString();
            kuldKuldemeny.FelhasznaloCsoport_Id_Bonto = rule.ParametersProperty.UserIdProperty.ToString();
            kuldKuldemeny.CimzesTipusa = KodTarak.KULD_CIMZES_TIPUS.nevre_szolo;
            EKFManager.SetKuldemenyKezbesitesModja(rule, ref kuldKuldemeny);

            kuldKuldemeny.Azonosito = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PR_ErkeztetesiSzam.ToString());
            kuldKuldemeny.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
            kuldKuldemeny.Tipus = KodTarak.KULDEMENY_TIPUS.Szerzodes;
            //kuldKuldemeny.KezbesitesModja = KodTarak.AdathordozoTipus.Egyeb;
            //kuldKuldemeny.Csoport_Id_Cimzett = EKFManager.GetEKFOperationParameterValue<EEKFOperationParameterIktatas>(ParameterProperty, EEKFOperationParameterIktatas.EnumParameterNames.Irat_Ugyintezo.ToString());
            //kuldKuldemeny.Csoport_Id_Felelos = EKFManager.GetEKFOperationParameterValue<EEKFOperationParameterIktatas>(ParameterProperty, EEKFOperationParameterIktatas.EnumParameterNames.Ugyirat_Ugyintezo.ToString());

            kuldKuldemeny.FelhasznaloCsoport_Id_Bonto = rule.ParametersProperty.UserIdProperty.ToString();
            kuldKuldemeny.FelbontasDatuma = kuldKuldemeny.BeerkezesIdeje;

            #region Feladasi ido
            string feladasiIdo = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.KR_ErkeztetesiDatum.ToString());
            if (!string.IsNullOrEmpty(feladasiIdo))
                kuldKuldemeny.BelyegzoDatuma = feladasiIdo;
            #endregion

            kuldKuldemeny.ElsodlegesAdathordozoTipusa = KodTarak.ELSODLEGES_ADATHORDOZO.Elektronikus_irat;

            string partnerId = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Partner_Id.ToString());
            if (!string.IsNullOrEmpty(partnerId) && Utils.IsValidGuid(partnerId))
            {
                kuldKuldemeny.Partner_Id_Bekuldo = partnerId;
            }

            string cimId = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Cim_Id.ToString());
            if (!string.IsNullOrEmpty(cimId) && Utils.IsValidGuid(cimId))
            {
                kuldKuldemeny.Cim_Id = cimId;
            }

            if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PR_ErkeztetesiSzam.ToString())))
                kuldKuldemeny.KulsoAzonosito = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PR_ErkeztetesiSzam.ToString());

            return kuldKuldemeny;
        }
        /// <summary>
        /// GetNewIrat
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="inputData"></param>
        /// <returns></returns>
        private EREC_IraIratok GetNewIrat(EKFRuleClass rule, DataSet inputData, string newIratId)
        {
            #region UJ IRAT
            EREC_IraIratok irat = new EREC_IraIratok();
            irat.Id = newIratId;
            irat.Targy = TargyProperty;
            irat.KiadmanyozniKell = "0";
            EKFManager.SetIratExpedialasModja(rule, ref irat);

            #region UGYINTEZO
            string ugyintezo = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.Irat_Ugyintezo.ToString());
            if (!string.IsNullOrEmpty(ugyintezo))
            {
                irat.FelhasznaloCsoport_Id_Ugyintez = ugyintezo;
                //irat.FelhasznaloCsoport_Id_Ugyintez = rule.ParametersProperty.UserIdProperty.ToString();
            }
            #endregion

            irat.Irattipus = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.IratTipus.ToString());
            irat.AdathordozoTipusa = KodTarak.AdathordozoTipus.Egyeb;
            irat.Jelleg = KodTarak.IRAT_JELLEG.Webes;

            #region UGY FAJTA
            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_UgyFajta.ToString())))
                irat.Ugy_Fajtaja = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_UgyFajta.ToString());
            else
            {
                string ugyTipus = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_UgyTipus.ToString());
                string ugykor = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.UT_Ugykor.ToString());
                if (!string.IsNullOrEmpty(ugyTipus) && !string.IsNullOrEmpty(ugykor))
                {
                    string ugyfajta = EKFManager.GetUgyFajtaFromUgyTipus(rule, ugyTipus, ugykor);
                    if (!string.IsNullOrEmpty(ugyfajta))
                        irat.Ugy_Fajtaja = ugyfajta;
                }
            }
            #endregion

            irat.Csoport_Id_Ugyfelelos = GetCsoport_Id_Ugyfelelos(rule);

            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.IntezesiIdo.ToString())))
                irat.IntezesiIdo = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.IntezesiIdo.ToString());
            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.IntezesiIdoegyseg.ToString())))
                irat.IntezesiIdoegyseg = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas.IntezesiIdoegyseg.ToString());

            #region HATARIDO
            string errorMessageHatarido = null;
            EKFManager.SetIratHatarido(rule.GetExecParam(), ref irat, out errorMessageHatarido);
            if (!string.IsNullOrEmpty(errorMessageHatarido))
            {
                AddMessage("Irat.Hatarido hiba:" + errorMessageHatarido);
                Logger.Error("ElektronikusKuldemenyFeldolgozas.Iktatas.Irat.Hatarido hiba:" + errorMessageHatarido);
            }
            #endregion

            return irat;
            #endregion
        }
        /// <summary>
        /// GetUgyIratById
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private EREC_UgyUgyiratok GetUgyIratById(EKFRuleClass rule, string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new Exception(string.Format("Nem található ügyirat"));

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam xParam = rule.GetExecParam();
            xParam.Record_Id = id;
            Result result = service.Get(xParam);
            if (string.IsNullOrEmpty(result.ErrorCode))
                return (EREC_UgyUgyiratok)result.Record;
            else
                return null;
        }
        /// <summary>
        /// GetIrattariTetelId
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="irattariTetelszam"></param>
        /// <param name="irattariTetelId"></param>
        /// <returns></returns>
        public bool GetIrattariTetelId(EKFRuleClass rule, string irattariTetelszam, out string irattariTetelId)
        {
            irattariTetelId = null;
            if (string.IsNullOrEmpty(irattariTetelszam))
                return false;

            Guid irattariTetelIdentity = Guid.Empty;
            if (EKFManager.TryParseGuid(irattariTetelszam, out irattariTetelIdentity))
            {
                irattariTetelId = irattariTetelIdentity.ToString();
                return true;
            }

            EREC_IraIrattariTetelekService irattarService = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            ExecParam xparamIrattarGet = rule.GetExecParam();
            EREC_IraIrattariTetelekSearch src_itsz = new EREC_IraIrattariTetelekSearch();
            src_itsz.IrattariTetelszam.Value = irattariTetelszam;
            src_itsz.IrattariTetelszam.Operator = Query.Operators.equals;
            Result resIrattar = irattarService.GetAllWithExtension(xparamIrattarGet, src_itsz);
            if (!string.IsNullOrEmpty(resIrattar.ErrorCode))
            {
                AddMessage(resIrattar);
                throw new ResultException(resIrattar);
            }
            if (resIrattar == null || resIrattar.Ds.Tables[0].Rows.Count < 1)
            {
                return false;
            }
            irattariTetelId = resIrattar.Ds.Tables[0].Rows[0]["Id"].ToString();
            return true;
        }

        /// <summary>
        /// Felelos szervezet beallitas
        /// Ha ugyintezore lesz szignalva, akkor beallitjuk szignalasnal megadott szervezetet felelos szervezetnek.
        /// </summary>
        /// <param name="rule"></param>
        /// <returns></returns>
        private string GetCsoport_Id_Ugyfelelos(EKFRuleClass rule)
        {
            EKFBaseOperation szignalas = rule.PostOperations.Where(p => p.OperationName == EnumEKFOperationName.SZIGNALAS && p.OperationType == EnumEKFOperationType.POST).FirstOrDefault();
            if (szignalas == null || szignalas.ParameterProperty == null || szignalas.ParameterProperty.Parameters == null)
                return rule.ParametersProperty.UserGroupProperty.ToString();

            string szervezetre = EKFManager.GetEKFOperationParameterValue(szignalas.ParameterProperty, Enum_ParameterNames_Post_Szignalas.Szervezet.ToString());
            string ugyintezore = EKFManager.GetEKFOperationParameterValue(szignalas.ParameterProperty, Enum_ParameterNames_Post_Szignalas.Szervezet_Ugyintezo.ToString());

            if (string.IsNullOrEmpty(ugyintezore) || string.IsNullOrEmpty(szervezetre))
                return rule.ParametersProperty.UserGroupProperty.ToString();

            return szervezetre;
        }
    }
}