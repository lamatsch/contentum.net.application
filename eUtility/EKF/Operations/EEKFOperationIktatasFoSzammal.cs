﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Data;
using System.Linq;

namespace Contentum.eUtility.EKF
{
    public class EKFOperationIktatasFoSzammal : EKFBaseOperation
    {
        private string TargyProperty;
        public EKFOperationIktatasFoSzammal()
        {
            OperationName = EnumEKFOperationName.IKTATAS_FOSZAMMAL;
            TypeFullName = this.GetType().FullName;
            OperationType = EnumEKFOperationType.MAIN;
        }
        public void InitParameters()
        {
            foreach (Enum_ParameterNames_Main_Iktatas_FoSzammal name in Enum.GetValues(typeof(Enum_ParameterNames_Main_Iktatas_FoSzammal)))
            {
                ParameterProperty.Parameters.Add(new EKFOperationParameterValue()
                {
                    IdProperty = name.ToString(),
                    NameProperty = name.ToString(),
                });
            }
        }
        public override string ToString()
        {
            return OperationName.ToString();
        }
        private EREC_UgyUgyiratok GetUgyiratByFoSzam(ExecParam param)
        {
            string foszam = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.FoSzam.ToString());

            if (string.IsNullOrEmpty(foszam))
                return null;

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam xparamEBeadvGet = param;
            EREC_UgyUgyiratokSearch src = new EREC_UgyUgyiratokSearch();
            src.Azonosito.Value = foszam;
            src.Azonosito.Operator = Query.Operators.equals;
            src.TopRow = 1;

            Result result = service.GetAll(param, src);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage(result);
                throw new ResultException(result);
            }
            if (result.Ds.Tables[0].Rows.Count < 1)
            {
                AddMessage("EKF: " + string.Format("Nem található ügyirat {0}", foszam));
                AddMessage(Environment.NewLine);
                throw new Exception(string.Format("Nem található ügyirat {0}", foszam));
            }

            return GetUgyIratById(param, result.Ds.Tables[0].Rows[0]["Id"].ToString());
        }

        public override bool Execute(EKFRuleClass rule, DataSet inputData, out Result result)
        {
            if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.IraIrat_Id.ToString())))
            {
                AddMessage("EKF: Iktatás nem lehetséges, mert már be van iktatva !");
                AddMessage(Environment.NewLine);
                throw new Exception("Iktatás nem lehetséges, mert már be van iktatva !");
            }

            result = null;
            string erkeztetoKonyv = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.ErkeztetoKonyv.ToString());

            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam xparamIr = rule.GetExecParam();

            string eBeadvanyId = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Id.ToString());
            EREC_UgyUgyiratok ugyirat = GetUgyiratByFoSzam(rule.GetExecParam());

            #region EVAL TARGY
            AddMessage("EKF: Targy meghatarozas indul");
            AddMessage(Environment.NewLine);

            EKFManagerEvaluateTargyFormula targyEvaluation = new EKFManagerEvaluateTargyFormula(eBeadvanyId, rule, inputData);
            TargyProperty = targyEvaluation.EvaluateTargyFormula();
            if (TargyProperty.Length > 4000)
                TargyProperty = TargyProperty.Substring(0, 3999);
            #endregion

            string iratId = null;
            string kuldemenyId = null;

            AddMessage("EKF: Iktatas Alszamra indul");
            AddMessage(Environment.NewLine);
            IktatasAlszamra(rule, inputData, ugyirat, erkeztetoKonyv, ugyirat.IraIktatokonyv_Id, out result, out iratId, out kuldemenyId);

            #region GET EBEADV ITEM
            EREC_eBeadvanyokService eBeadvService = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            ExecParam xparamEBeadvGet = rule.GetExecParam();
            xparamEBeadvGet.Record_Id = eBeadvanyId;
            Result eBeadvanyResultGet = eBeadvService.Get(xparamEBeadvGet);

            if (!string.IsNullOrEmpty(eBeadvanyResultGet.ErrorCode))
            {
                AddMessage(eBeadvanyResultGet);
                throw new ResultException(eBeadvanyResultGet);
            }
            EREC_eBeadvanyok eBeadvany = (EREC_eBeadvanyok)eBeadvanyResultGet.Record;
            #endregion  GET EBEADV ITEM

            #region UPDATE EBEADV
            ExecParam xparamEBeadvUpdate = rule.GetExecParam();
            xparamEBeadvUpdate.Record_Id = eBeadvany.Id;
            if (eBeadvany.KuldKuldemeny_Id == null)
                eBeadvany.KuldKuldemeny_Id = kuldemenyId;
            eBeadvany.IraIrat_Id = iratId;
            Result resultEBeadvUpdate = eBeadvService.Update(xparamEBeadvUpdate, eBeadvany);

            if (!string.IsNullOrEmpty(resultEBeadvUpdate.ErrorCode))
            {
                AddMessage(resultEBeadvUpdate);
                throw new ResultException(resultEBeadvUpdate);
            }
            #endregion UPDATE EBEADV

            return true;
        }

        /// <summary>
        /// IktatasAlszamra
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="inputData"></param>
        /// <param name="ugyIrat"></param>
        /// <param name="erkeztetoKonyvId"></param>
        /// <param name="iktatoKonyvId"></param>
        /// <param name="result"></param>
        private void IktatasAlszamra(EKFRuleClass rule, DataSet inputData, EREC_UgyUgyiratok ugyIrat, string erkeztetoKonyvId, string iktatoKonyvId, out Result result, out string iratId, out string kuldemenyId)
        {
            #region INIT
            Logger.Info("EKFMainOperationIktatasFoszammal.Init.Start");
            result = null;
            string eBeadvanyId = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Id.ToString());
            iratId = Guid.NewGuid().ToString();
            kuldemenyId = Guid.NewGuid().ToString();

            #region EREC_IRAIRATOK
            EREC_IraIratok newIrat = GetNewIrat(rule, inputData, iratId);

            if (!string.IsNullOrEmpty(ugyIrat.IntezesiIdo.ToString()))
                newIrat.IntezesiIdo = ugyIrat.IntezesiIdo.ToString();
            if (!string.IsNullOrEmpty(ugyIrat.IntezesiIdoegyseg.ToString()))
                newIrat.IntezesiIdoegyseg = ugyIrat.IntezesiIdoegyseg.ToString();

            #endregion EREC_IRAIRATOK

            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = GetNewKuldemeny(rule, inputData, erkeztetoKonyvId, kuldemenyId);
            #endregion EREC_KULDKULDEMENYEK

            Logger.Info("EKFMainOperationIktatasFoszammal.Init.Stop");
            #endregion

            #region BELSO IRAT IKTATASA
            Logger.Info("EKFMainOperationIktatasFoszammal.Start");
            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam xparam = rule.GetExecParam();
            //result = iraIratokService.EgyszerusitettIktatasa_Alszamra(
            //        xparam,
            //        iktatoKonyvId,
            //        ugyIrat.Id,
            //        new EREC_UgyUgyiratdarabok(),
            //        newIrat,
            //        new EREC_HataridosFeladatok(),
            //        kuldKuldemeny,
            //        GetIktatasParameterek(),
            //        new ErkeztetesParameterek()
            //        );
            result = iraIratokService.HivataliKapusErkeztetesIktatas_Alszamra(
                   xparam,
                   kuldKuldemeny,
                   eBeadvanyId,
                   iktatoKonyvId,
                   ugyIrat.Id,
                   new EREC_UgyUgyiratdarabok(),
                   newIrat,
                   new EREC_HataridosFeladatok(),
                   GetIktatasParameterek(),
                   new ErkeztetesParameterek()
                   );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage(result);
                throw new ResultException(result);
            }

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result.Record;
            if (erkeztetesIktatasResult != null)
            {
                iratId = string.IsNullOrEmpty(erkeztetesIktatasResult.IratId) ? iratId : erkeztetesIktatasResult.IratId;
                kuldemenyId = string.IsNullOrEmpty(erkeztetesIktatasResult.KuldemenyId) ? kuldemenyId : erkeztetesIktatasResult.KuldemenyId;
                AddMessage(string.Format("UGY-AZ: {0}, IRAT-AZ: {1}, KULD-AZ: {2}", erkeztetesIktatasResult.UgyiratAzonosito, erkeztetesIktatasResult.IratAzonosito, erkeztetesIktatasResult.KuldemenyAzonosito));
            }

            Logger.Info("EKFMainOperationIktatasFoszammal. Stop");
            #endregion BELSO IRAT IKTATASA
        }
        /// <summary>
        /// GetIktatasParameterek
        /// </summary>
        /// <returns></returns>
        private IktatasiParameterek GetIktatasParameterek()
        {
            IktatasiParameterek iktatasParameterek = new IktatasiParameterek();
            iktatasParameterek.IratpeldanyVonalkodGeneralasHaNincs = false;
            iktatasParameterek.Adoszam = null;

            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_Ugykor.ToString())))
                iktatasParameterek.UgykorId = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_Ugykor.ToString());

            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_UgyTipus.ToString())))
                iktatasParameterek.Ugytipus = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_UgyTipus.ToString());
            else
                iktatasParameterek.Ugytipus = EKFConstants.UgyTipusDefault;

            iktatasParameterek.UgyiratUjranyitasaHaLezart = false;
            iktatasParameterek.UgyiratPeldanySzukseges = false;
            iktatasParameterek.KeszitoPeldanyaSzukseges = false;
            iktatasParameterek.Atiktatas = false;
            iktatasParameterek.EmptyUgyiratSztorno = false;
            iktatasParameterek.MunkaPeldany = false;
            return iktatasParameterek;
        }
        /// <summary>
        /// GetNewUgyIrat
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="inputData"></param>
        /// <param name="ugyiratId"></param>
        /// <returns></returns>
        private EREC_UgyUgyiratok GetNewUgyIrat(EKFRuleClass rule, DataSet inputData, string ugyiratId)
        {
            EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();
            ugyirat.Id = ugyiratId;
            ugyirat.Targy = TargyProperty;

            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_UgyTipus.ToString())))
                ugyirat.UgyTipus = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_UgyTipus.ToString());
            else
                ugyirat.UgyTipus = EKFConstants.UgyTipusDefault;

            ugyirat.Csoport_Id_Felelos = rule.ParametersProperty.UserGroupProperty.ToString();

            string irattariTetelSzam = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.IrattariTelel.ToString());
            string irattariTetelId;
            if (new EKFMainOperationIktatas().GetIrattariTetelId(rule, irattariTetelSzam, out irattariTetelId))
            {
                if (!string.IsNullOrEmpty(irattariTetelId))
                    ugyirat.IraIrattariTetel_Id = irattariTetelId;
            }

            ugyirat.NevSTR_Ugyindito = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerNev.ToString());

            #region UGYINTEZO
            string ugyiratUgyintezo = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.Ugyirat_Ugyintezo.ToString());
            if (!string.IsNullOrEmpty(ugyiratUgyintezo))
            {
                ugyirat.FelhasznaloCsoport_Id_Ugyintez = ugyiratUgyintezo;
            }

            #endregion

            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_UgyFajta.ToString())))
                ugyirat.Ugy_Fajtaja = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_UgyFajta.ToString());

            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_UgyTipus.ToString())))
            {
                ugyirat.UgyTipus = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_UgyTipus.ToString());

                string ugykor = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_Ugykor.ToString());
                if (!string.IsNullOrEmpty(ugyirat.UgyTipus) && !string.IsNullOrEmpty(ugykor))
                {
                    string ugyfajta = EKFManager.GetUgyFajtaFromUgyTipus(rule, ugyirat.UgyTipus, ugykor);
                    if (!string.IsNullOrEmpty(ugyfajta))
                        ugyirat.Ugy_Fajtaja = ugyfajta;
                }
            }

            #region HATARIDO
            string errorMessageHatarido = null;
            EKFManager.SetUgyiratHatarido(rule.GetExecParam(), ref ugyirat, out errorMessageHatarido);
            if (!string.IsNullOrEmpty(errorMessageHatarido))
            {
                AddMessage("Ugyirat.Hatarido hiba:" + errorMessageHatarido);
                Logger.Error("ElektronikusKuldemenyFeldolgozas.Iktatas.Ugyirat.Hatarido hiba:" + errorMessageHatarido);
            }
            #endregion

            ugyirat.ElintezesMod = KodTarak.ELINTEZESMOD.Egyeb;
            ugyirat.UgyintezesModja = KodTarak.ELSODLEGES_ADATHORDOZO.Elektronikus_irat;
            ////ugyirat.Alkalmazas_Id = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.SzakrendszerKod.ToString());

            ugyirat.Csoport_Id_Ugyfelelos = GetCsoport_Id_Ugyfelelos(rule);

            return ugyirat;
        }
        /// <summary>
        /// GetNewKuldemeny
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="erkeztetoKonyv_Id"></param>
        /// <param name="kuldemenyId"></param>
        /// <returns></returns>
        private EREC_KuldKuldemenyek GetNewKuldemeny(EKFRuleClass rule, DataSet inputData, string erkeztetoKonyv_Id, string kuldemenyId)
        {
            EREC_KuldKuldemenyek kuldKuldemeny = new EREC_KuldKuldemenyek();
            kuldKuldemeny.Id = kuldemenyId;
            kuldKuldemeny.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            //kuldKuldemeny.ElsodlegesAdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;

            EKFManager.EBeadvanyKuldemenyBeerkezesIdoMeghatarozas(rule.GetExecParam(), inputData, ref kuldKuldemeny);

            kuldKuldemeny.HivatkozasiSzam = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.KR_ErkeztetesiSzam.ToString());
            kuldKuldemeny.Targy = TargyProperty;

            string paramErkKonyv = null;
            try
            {
                paramErkKonyv = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.ErkeztetoKonyv.ToString());
            }
            catch (Exception) { }

            if (!string.IsNullOrEmpty(paramErkKonyv))
                kuldKuldemeny.IraIktatokonyv_Id = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.ErkeztetoKonyv.ToString());
            else
                kuldKuldemeny.IraIktatokonyv_Id = erkeztetoKonyv_Id;

            kuldKuldemeny.NevSTR_Bekuldo = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerNev.ToString());

            EKFManager.SetKuldemenyKuldesModja(rule, ref kuldKuldemeny);

            kuldKuldemeny.Surgosseg = KodTarak.SURGOSSEG.Normal;
            kuldKuldemeny.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            kuldKuldemeny.Erkeztetes_Ev = DateTime.Now.Year.ToString();
            kuldKuldemeny.PeldanySzam = "1";
            kuldKuldemeny.Munkaallomas = EKFConstants.EnumEKFMunkaallomas.Automatikus_Erkezteto.ToString();
            kuldKuldemeny.FelhasznaloCsoport_Id_Bonto = rule.ParametersProperty.UserIdProperty.ToString();

            kuldKuldemeny.CimzesTipusa = KodTarak.KULD_CIMZES_TIPUS.nevre_szolo;
            EKFManager.SetKuldemenyKezbesitesModja(rule, ref kuldKuldemeny);

            kuldKuldemeny.Azonosito = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PR_ErkeztetesiSzam.ToString());
            kuldKuldemeny.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
            kuldKuldemeny.Tipus = KodTarak.KULDEMENY_TIPUS.Szerzodes;
            //kuldKuldemeny.KezbesitesModja = KodTarak.AdathordozoTipus.Egyeb;

            if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerKRID.ToString())))
                kuldKuldemeny.CimSTR_Bekuldo = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerKRID.ToString());
            else if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerKapcsolatiKod.ToString())))
                kuldKuldemeny.CimSTR_Bekuldo = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerKapcsolatiKod.ToString());
            else if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.KR_HivatkozasiSzam.ToString())))
                kuldKuldemeny.CimSTR_Bekuldo = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.KR_HivatkozasiSzam.ToString());
            else
                kuldKuldemeny.CimSTR_Bekuldo = EKFConstants.EnumEKFbekuldo.Hivatali_kapu.ToString();

            kuldKuldemeny.Csoport_Id_Cimzett = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.Erkeztetes_Cimzett.ToString());
            kuldKuldemeny.Csoport_Id_Felelos = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.Erkeztetes_Felelos.ToString());

            kuldKuldemeny.FelhasznaloCsoport_Id_Bonto = rule.ParametersProperty.UserIdProperty.ToString();
            kuldKuldemeny.FelbontasDatuma = kuldKuldemeny.BeerkezesIdeje;

            kuldKuldemeny.ElsodlegesAdathordozoTipusa = KodTarak.ELSODLEGES_ADATHORDOZO.Elektronikus_irat;

            #region Feladasi ido
            string feladasiIdo = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.KR_ErkeztetesiDatum.ToString());
            if (!string.IsNullOrEmpty(feladasiIdo))
                kuldKuldemeny.BelyegzoDatuma = feladasiIdo;
            #endregion

            string partnerId = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Partner_Id.ToString());
            if (!string.IsNullOrEmpty(partnerId) && Utils.IsValidGuid(partnerId))
            {
                kuldKuldemeny.Partner_Id_Bekuldo = partnerId;
            }

            string cimId = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Cim_Id.ToString());
            if (!string.IsNullOrEmpty(cimId) && Utils.IsValidGuid(cimId))
            {
                kuldKuldemeny.Cim_Id = cimId;
            }

            if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PR_ErkeztetesiSzam.ToString())))
                kuldKuldemeny.KulsoAzonosito = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PR_ErkeztetesiSzam.ToString());
            return kuldKuldemeny;
        }
        /// <summary>
        /// GetNewIrat
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="inputData"></param>
        /// <returns></returns>
        private EREC_IraIratok GetNewIrat(EKFRuleClass rule, DataSet inputData, string newIratId)
        {
            #region UJ IRAT
            EREC_IraIratok irat = new EREC_IraIratok();
            irat.Id = newIratId;
            irat.Targy = TargyProperty;
            irat.KiadmanyozniKell = "0";

            EKFManager.SetIratExpedialasModja(rule, ref irat);

            #region UGYINTEZO
            string ugyintezo = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.Irat_Ugyintezo.ToString());
            if (!string.IsNullOrEmpty(ugyintezo))
            {
                irat.FelhasznaloCsoport_Id_Ugyintez = ugyintezo;
                //irat.FelhasznaloCsoport_Id_Ugyintez = rule.ParametersProperty.UserIdProperty.ToString();
            }
            #endregion

            irat.Irattipus = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.IratTipus.ToString());
            irat.AdathordozoTipusa = KodTarak.AdathordozoTipus.Egyeb;
            irat.Jelleg = KodTarak.IRAT_JELLEG.Webes;

            #region UGY FAJTA
            if (!string.IsNullOrEmpty(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_UgyFajta.ToString())))
                irat.Ugy_Fajtaja = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_UgyFajta.ToString());
            else
            {
                string ugyTipus = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_UgyTipus.ToString());
                string ugykor = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Iktatas_FoSzammal.UT_Ugykor.ToString());
                if (!string.IsNullOrEmpty(ugyTipus) && !string.IsNullOrEmpty(ugykor))
                {
                    string ugyfajta = EKFManager.GetUgyFajtaFromUgyTipus(rule, ugyTipus, ugykor);
                    if (!string.IsNullOrEmpty(ugyfajta))
                        irat.Ugy_Fajtaja = ugyfajta;
                }
            }
            #endregion

            irat.Csoport_Id_Ugyfelelos = GetCsoport_Id_Ugyfelelos(rule);

            #region HATARIDO
            string errorMessageHatarido = null;
            EKFManager.SetIratHatarido(rule.GetExecParam(), ref irat, out errorMessageHatarido);
            if (!string.IsNullOrEmpty(errorMessageHatarido))
            {
                AddMessage("Irat.Hatarido hiba:" + errorMessageHatarido);
                Logger.Error("ElektronikusKuldemenyFeldolgozas.Iktatas.Irat.Hatarido hiba:" + errorMessageHatarido);
            }
            #endregion

            return irat;
            #endregion
        }
        /// <summary>
        /// GetUgyIratById
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private EREC_UgyUgyiratok GetUgyIratById(ExecParam xParam, string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                AddMessage("EKF: Nem található ügyirat");
                throw new Exception(string.Format("Nem található ügyirat"));
            }

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            xParam.Record_Id = id;
            Result result = service.Get(xParam);
            if (string.IsNullOrEmpty(result.ErrorCode))
                return (EREC_UgyUgyiratok)result.Record;
            else
            {
                AddMessage(string.Format("Nem található ügyirat"));
                throw new Exception(string.Format("Nem található ügyirat"));
            }
        }

        /// <summary>
        /// Felelos szervezet beallitas
        /// Ha ugyintezore lesz szignalva, akkor beallitjuk szignalasnal megadott szervezetet felelos szervezetnek.
        /// </summary>
        /// <param name="rule"></param>
        /// <returns></returns>
        private string GetCsoport_Id_Ugyfelelos(EKFRuleClass rule)
        {
            EKFBaseOperation szignalas = rule.PostOperations.Where(p => p.OperationName == EnumEKFOperationName.SZIGNALAS && p.OperationType == EnumEKFOperationType.POST).FirstOrDefault();
            if (szignalas == null || szignalas.ParameterProperty == null || szignalas.ParameterProperty.Parameters == null)
                return rule.ParametersProperty.UserGroupProperty.ToString();

            string szervezetre = EKFManager.GetEKFOperationParameterValue(szignalas.ParameterProperty, Enum_ParameterNames_Post_Szignalas.Szervezet.ToString());
            string ugyintezore = EKFManager.GetEKFOperationParameterValue(szignalas.ParameterProperty, Enum_ParameterNames_Post_Szignalas.Szervezet_Ugyintezo.ToString());

            if (string.IsNullOrEmpty(ugyintezore) || string.IsNullOrEmpty(szervezetre))
                return rule.ParametersProperty.UserGroupProperty.ToString();

            return szervezetre;
        }
    }
}