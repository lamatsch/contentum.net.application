﻿using Contentum.eDocument.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Data;
using System.Linq;

namespace Contentum.eUtility.EKF
{
    public class EKFMainOperationErkeztetes : EKFBaseOperation
    {
        private string TargyProperty;
        public EKFMainOperationErkeztetes()
        {
            OperationName = EnumEKFOperationName.ERKEZTETES;
            TypeFullName = this.GetType().FullName;
            OperationType = EnumEKFOperationType.MAIN;
        }
        public void InitParameters()
        {
            foreach (Enum_ParameterNames_Main_Erkeztetes name in Enum.GetValues(typeof(Enum_ParameterNames_Main_Erkeztetes)))
            {
                ParameterProperty.Parameters.Add(new EKFOperationParameterValue()
                {
                    IdProperty = name.ToString(),
                    NameProperty = name.ToString(),
                });
            }
        }
        public override string ToString()
        {
            return OperationName.ToString();
        }
        public override bool Execute(EKFRuleClass rule, DataSet inputData, out Result result)
        {
            if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.KuldKuldemeny_Id.ToString())))
            {
                AddMessage("EKF: Érkeztetés nem lehetséges, mert már be van érkeztetve !");
                throw new Exception("EKF: Érkeztetés nem lehetséges, mert már be van érkeztetve !");
            }

            EREC_KuldKuldemenyekService kuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam xpm = rule.GetExecParam();

            string eBeadvanyId = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Id.ToString());
            string kuldemenyId = Guid.NewGuid().ToString();
            string erkeztetoKonyv_Id = GetErkeztetoKonyvId(rule.GetExecParam());

            #region EVAL TARGY
            AddMessage("EKF: Targy meghatarozas indul");
            AddMessage(Environment.NewLine);
            EKFManagerEvaluateTargyFormula targyEvaluation = new EKFManagerEvaluateTargyFormula(eBeadvanyId, rule, inputData);
            TargyProperty = targyEvaluation.EvaluateTargyFormula();
            if (TargyProperty.Length > 4000)
                TargyProperty = TargyProperty.Substring(0, 3999);

            if (!string.IsNullOrEmpty(TargyProperty))
            {
                AddMessage("EKF: Targy erteke:" + TargyProperty);
                AddMessage(Environment.NewLine);
            }
            #endregion

            #region ELOZMENY KULDEMENY
            string elozmenykuldemenyId = null;
            string elozmenyErkeztetoKonyvId = null;
            DataRow elozmenyKuldemenyRow = null;
            Enum_Erkeztetes_Tipus tipusErk = new EKFMainOperationIktatas().CheckElozmenyKuldemeny(rule, inputData, out elozmenykuldemenyId, out elozmenyErkeztetoKonyvId, out elozmenyKuldemenyRow);
            if (!string.IsNullOrEmpty(elozmenyErkeztetoKonyvId))
                erkeztetoKonyv_Id = elozmenyErkeztetoKonyvId;

            if (string.IsNullOrEmpty(erkeztetoKonyv_Id))
            {
                AddMessage("EKF: Érkeztetőkönyv nincs megadva !");
                throw new Exception("Érkeztetőkönyv nincs megadva !");
            }
            #endregion


            #region EREC_KULDKULDEMENYEK
            EREC_KuldKuldemenyek kuldKuldemeny = new EREC_KuldKuldemenyek();
            kuldKuldemeny.Id = kuldemenyId;
            kuldKuldemeny.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            //kuldKuldemeny.ElsodlegesAdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            //kuldKuldemeny.Erkezteto_Szam = EKFManager.GetDataSetValue(inputData, "KR_ErkeztetesiSzam");
            EKFManager.EBeadvanyKuldemenyBeerkezesIdoMeghatarozas(rule.GetExecParam(), inputData, ref kuldKuldemeny);

            kuldKuldemeny.Targy = TargyProperty;
            kuldKuldemeny.IraIktatokonyv_Id = erkeztetoKonyv_Id;
            kuldKuldemeny.NevSTR_Bekuldo = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerNev.ToString());

            if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerKapcsolatiKod.ToString())))
                kuldKuldemeny.CimSTR_Bekuldo = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerKapcsolatiKod.ToString());
            else if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerKRID.ToString())))
                kuldKuldemeny.CimSTR_Bekuldo = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PartnerKRID.ToString());
            else if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.KR_HivatkozasiSzam.ToString())))
                kuldKuldemeny.CimSTR_Bekuldo = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.KR_HivatkozasiSzam.ToString());
            else
                kuldKuldemeny.CimSTR_Bekuldo = EKFConstants.EnumEKFbekuldo.Hivatali_kapu.ToString();

            kuldKuldemeny.FelhasznaloCsoport_Id_Bonto = rule.ParametersProperty.UserIdProperty.ToString();
            kuldKuldemeny.FelbontasDatuma = kuldKuldemeny.BeerkezesIdeje;

            EKFManager.SetKuldemenyKuldesModja(rule, ref kuldKuldemeny);

            kuldKuldemeny.Surgosseg = KodTarak.SURGOSSEG.Normal;
            kuldKuldemeny.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            kuldKuldemeny.Erkeztetes_Ev = DateTime.Now.Year.ToString();
            kuldKuldemeny.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
            //LZS - BUG_13589 
            // javítás kiszedve (rossz helyen javított) nekrisz
            kuldKuldemeny.IktatastNemIgenyel = "1";
            kuldKuldemeny.PeldanySzam = "1";
            kuldKuldemeny.Csoport_Id_Cimzett = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Erkeztetes.Cimzett.ToString());
            kuldKuldemeny.Csoport_Id_Felelos = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Erkeztetes.Felelos.ToString());
            kuldKuldemeny.Munkaallomas = EKFConstants.EnumEKFMunkaallomas.Automatikus_Erkezteto.ToString();

            kuldKuldemeny.CimzesTipusa = KodTarak.KULD_CIMZES_TIPUS.nevre_szolo;

            kuldKuldemeny.ElsodlegesAdathordozoTipusa = KodTarak.ELSODLEGES_ADATHORDOZO.Elektronikus_irat;

            #region Feladasi ido
            string feladasiIdo = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.KR_ErkeztetesiDatum.ToString());
            if (!string.IsNullOrEmpty(feladasiIdo))
                kuldKuldemeny.BelyegzoDatuma = feladasiIdo;
            #endregion

            EKFManager.SetKuldemenyKezbesitesModja(rule, ref kuldKuldemeny);

            if (string.IsNullOrEmpty(kuldKuldemeny.KezbesitesModja))
            {
                EKFManager.SetKuldemenyKezbesitesModja(rule, ref kuldKuldemeny);
            }

            kuldKuldemeny.Azonosito = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PR_ErkeztetesiSzam.ToString());

            if (!string.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PR_ErkeztetesiSzam.ToString())))
                kuldKuldemeny.KulsoAzonosito = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.PR_ErkeztetesiSzam.ToString());
            #endregion EREC_KULDKULDEMENYEK

            string fileNev = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.KR_FileNev.ToString());
            string ext = System.IO.Path.GetExtension(fileNev);

            if (".krx".Equals(ext, StringComparison.CurrentCultureIgnoreCase))
            {
                try
                {
                    AddMessage("LoadKRXMetaAdatok");
                    EKF.Helper.KRXHelper.LoadKRXMetaAdatok(xpm, eBeadvanyId, kuldKuldemeny);
                }
                catch (Exception ex)
                {
                    Result error = ResultException.GetResultFromException(ex);
                    AddMessage("LoadKRXMetaAdatok hiba!");
                    AddMessage(error);
                }
            }

            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            ErkeztetesParameterek erkParameterek = new ErkeztetesParameterek();

            string partnerId = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Partner_Id.ToString());
            if (!string.IsNullOrEmpty(partnerId) && Utils.IsValidGuid(partnerId))
            {
                kuldKuldemeny.Partner_Id_Bekuldo = partnerId;
            }

            string cimId = EKFManager.GetDataSetValue(inputData, EKFConstants.eBeadvanyOszlopok.Cim_Id.ToString());
            if (!string.IsNullOrEmpty(cimId) && Utils.IsValidGuid(cimId))
            {
                kuldKuldemeny.Cim_Id = cimId;
            }

            result = kuldemenyekService.ErkeztetesHivataliKapubol(xpm, kuldKuldemeny, eBeadvanyId, feladatok, erkParameterek);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage(result);
                throw new ResultException(result);
            }

            try
            {
                EnsureKezbesitesiTetelAtadasraKerult(rule);
            }
            catch (ResultException e)
            {
                AddMessage(e);
                return false;
            }

            return true;
        }

        private string GetErkeztetoKonyvId(ExecParam param)
        {
            string erkeztetoKonyv_Id = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Erkeztetes.ErkeztetoKonyv.ToString());
            string erkeztetoHely = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Main_Erkeztetes.ErkeztetoHely.ToString());
            if (!string.IsNullOrEmpty(erkeztetoKonyv_Id))
            {
                return erkeztetoKonyv_Id;
            }
            else if (!string.IsNullOrEmpty(erkeztetoHely))
            {
                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam xparamEBeadvGet = param;
                EREC_IraIktatoKonyvekSearch src = new EREC_IraIktatoKonyvekSearch();
                src.Ev.Value = DateTime.Now.Year.ToString();
                src.Ev.Operator = Query.Operators.equals;

                src.MegkulJelzes.Value = erkeztetoHely;
                src.MegkulJelzes.Operator = Query.Operators.equals;

                src.IktatoErkezteto.Value = "E";
                src.IktatoErkezteto.Operator = Query.Operators.equals;
                src.TopRow = 1;

                Result result = service.GetAll(param, src);
                if (!string.IsNullOrEmpty(result.ErrorCode))
                {
                    AddMessage(result);
                    throw new ResultException(result);
                }
                if (result.Ds.Tables[0].Rows.Count < 1)
                {
                    AddMessage("EKF: " + string.Format("Nem található érkeztetőkönvy {0}", erkeztetoHely));
                    throw new Exception(string.Format("Nem található érkeztetőkönvy {0}", erkeztetoHely));
                }
                return result.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            else
            {
                AddMessage("EKF: Érkeztetőkönyv nem található ");
                throw new Exception("Érkeztetőkönyv nem található !");
            }
        }

    }
}
