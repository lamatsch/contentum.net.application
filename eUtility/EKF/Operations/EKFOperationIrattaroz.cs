﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Data;

namespace Contentum.eUtility.ElektronikusKuldemenyFeldolgozas
{
    public class EKFMainOperationIrattaroz : EKFBaseOperation
    {
        public EKFMainOperationIrattaroz()
        {
            OperationName = EnumEKFOperationName.IRATTARAZAS;
            TypeFullName = this.GetType().FullName;
            OperationType = EnumEKFOperationType.MAIN;
        }
        public void InitParameters()
        {
            foreach (Enum_ParameterNames_Main_Irattaroz name in Enum.GetValues(typeof(Enum_ParameterNames_Main_Irattaroz)))
            {
                ParameterProperty.Parameters.Add(new EKFOperationParameterValue()
                {
                    IdProperty = name.ToString(),
                    NameProperty = name.ToString(),
                });
            }
        }
        public override string ToString()
        {
            return OperationName.ToString();
        }
        public override bool Execute(EKFRuleClass rule, DataSet inputData, out Result result)
        {
            ExecParam xparamIr = rule.GetExecParam();

            string eBeadvanyId = EKFManager.GetDataSetValue(inputData, "Id");
            string iratId = EKFManager.GetDataSetValue(inputData, "IraIrat_Id");
            if (string.IsNullOrEmpty(iratId))
                throw new Exception("Irat nem létezik !");

            #region GET IRAT
            EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam xparamIratGet = rule.GetExecParam();
            xparamIratGet.Record_Id = iratId;
            Result iratResultGet = iraIratokService.Get(xparamIratGet);

            if (!String.IsNullOrEmpty(iratResultGet.ErrorCode))
                throw new ResultException(iratResultGet);
            EREC_IraIratok irat = (EREC_IraIratok)iratResultGet.Record;
            #endregion  GET IRAT


            #region GET UGYIRAT
            EREC_UgyUgyiratokService ugyIratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam xparamUgyGet = rule.GetExecParam();
            xparamUgyGet.Record_Id = irat.Ugyirat_Id;
            Result ugyResultGet = ugyIratokService.Get(xparamUgyGet);

            if (!String.IsNullOrEmpty(ugyResultGet.ErrorCode))
                throw new ResultException(ugyResultGet);
            EREC_UgyUgyiratok ugy = (EREC_UgyUgyiratok)ugyResultGet.Record;
            #endregion  GET UGYIRAT

            ExecParam xparamElintez = rule.GetExecParam();
            xparamElintez.Record_Id = irat.Ugyirat_Id;
         
            result = ugyIratokService.ElintezetteNyilvanitas(
                xparamElintez,
                ugy.ElintezesDat,
                ugy.ElintezesMod,
                 new EREC_HataridosFeladatok()
                );

            if (!String.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);

            return true;
        }
    }
}
