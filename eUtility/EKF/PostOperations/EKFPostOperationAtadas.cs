﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Data;

namespace Contentum.eUtility.EKF
{
    [Obsolete]
    /// <summary>
    /// Elintézetté nyilvánítás - Elektronikus irattarba tetel
    /// </summary>
    public class EKFPostOperationAtadas : EKFBaseOperation
    {
        public EKFPostOperationAtadas()
        {
            OperationName = EnumEKFOperationName.ATADAS;
            TypeFullName = this.GetType().FullName;
            OperationType = EnumEKFOperationType.POST;
        }
        public void InitParameters()
        {
            foreach (Enum_ParameterNames_Post_Atadas name in Enum.GetValues(typeof(Enum_ParameterNames_Post_Atadas)))
            {
                ParameterProperty.Parameters.Add(new EKFOperationParameterValue()
                {
                    IdProperty = name.ToString(),
                    NameProperty = name.ToString(),
                });
            }
        }
        public override string ToString()
        {
            return OperationName.ToString();
        }
        public override bool Execute(EKFRuleClass rule, DataSet inputData, out Result result)
        {
            result = null;
            ExecParam xparamIr = rule.GetExecParam();

            AddMessage("EKF: Atadas indul");

            EREC_eBeadvanyok resultEBeadvany = new EKFManager(xparamIr).GetEBeadvanyByDataSet(inputData);
            if (resultEBeadvany == null)
                return false;

            string iratId = resultEBeadvany.IraIrat_Id;

            if (string.IsNullOrEmpty(iratId))
            {
                AddMessage("EKF: Irat nem található !");
                throw new Exception("Irat nem található !");
            }

            EREC_IraIratok irat = new EKFManager(xparamIr).GetIrat(rule, iratId);
            if (irat == null)
            {
                AddMessage("EKF: Irat nem található !");
                throw new Exception("Irat nem található !");
            }

            EREC_UgyUgyiratok ugy = new EKFManager(xparamIr).GetUgyirat(rule, irat.Ugyirat_Id);
            if (ugy == null)
            {
                AddMessage("EKF: Ugy nem található !");
                throw new Exception("Ugy nem található !");
            }
            EREC_UgyUgyiratokService ugyIratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            #region UGYINTEZES ALATT
            if (ugy.Allapot != KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt)
            {
                ExecParam xparamUgyintezesre = rule.GetExecParam();
                xparamUgyintezesre.Record_Id = ugy.Id;
                Result resultATV = ugyIratokService.AtvetelUgyintezesre(xparamUgyintezesre, ugy.Id);
                if (!string.IsNullOrEmpty(resultATV.ErrorCode))
                {
                    AddMessage(resultATV);
                    throw new ResultException(resultATV);
                }
            }
            #endregion

            #region ELINTEZ
            ExecParam xparamELINT = rule.GetExecParam();
            xparamELINT.Record_Id = ugy.Id;
            Result resultElintezett = ugyIratokService.ElintezetteNyilvanitas(
                              xparamELINT,
                              ugy.ElintezesDat ?? DateTime.Today.AddDays(1).ToString(),
                              ugy.ElintezesMod ?? string.Empty,
                              new EREC_HataridosFeladatok()
                              );

            if (!string.IsNullOrEmpty(resultElintezett.ErrorCode))
            {
                AddMessage(resultElintezett);
                throw new ResultException(resultElintezett);
            }
            #endregion
            AddMessage("EKF: Atadas vege");
            return true;
        }
    }
}
