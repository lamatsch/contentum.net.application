﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility.Netlock;
using Contentum.eUtility.NetLockSignAssist;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace Contentum.eUtility.EKF
{
    public class EKFPostOperationAutoValasz : EKFBaseOperation
    {
        public EKFPostOperationAutoValasz()
        {
            OperationName = EnumEKFOperationName.AUTO_VALASZ;
            TypeFullName = this.GetType().FullName;
            OperationType = EnumEKFOperationType.POST;
        }
        public void InitParameters()
        {
            foreach (Enum_ParameterNames_Post_AutoValasz name in Enum.GetValues(typeof(Enum_ParameterNames_Post_AutoValasz)))
            {
                ParameterProperty.Parameters.Add(new EKFOperationParameterValue()
                {
                    IdProperty = name.ToString(),
                    NameProperty = name.ToString(),
                });
            }
        }
        public override string ToString()
        {
            return OperationName.ToString();
        }
        public override bool Execute(EKFRuleClass rule, DataSet inputData, out Result result)
        {
            result = null;
            ExecParam xparam = rule.GetExecParam();

            AddMessage("EKF: AutoValasz indul");

            #region VARIABLES
            string eBeadvanyId = EKFManager.GetDataSetValue(inputData, "Id");
            #endregion

            System.Collections.Generic.Dictionary<string, string> ssrsParameters = new System.Collections.Generic.Dictionary<string, string>();
            ssrsParameters.Add("eBeadvanyId", eBeadvanyId);

            byte[] contentOriginalPdf =
                SSRSHelper.GetSSRSRiportPdf(
                    new Uri(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_AutoValasz.Riport_Szerver_Url.ToString())),
                    EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_AutoValasz.Valasz_Uzenet_Riport_Sablon.ToString()),
                    ssrsParameters
                    );

            if (contentOriginalPdf == null || contentOriginalPdf.Length < 1)
            {
                AddMessage("Hibás az SSRS dokumentum ! ");
                throw new Exception("Hibás az SSRS dokumentum ! ");
            }

            byte[] contentSignedPdf = null;
            string iktatniKell = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_AutoValasz.Iktatni_Kell.ToString());
            string irattarozniKell = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_AutoValasz.Irattarozni_Kell.ToString());
            string templateFileName = eBeadvanyId + "_" + DateTime.Now.ToFileTimeUtc() + ".PDF";

            #region E-ALAIRAS KELL?
            string eAlairasKell = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_AutoValasz.Elektronikus_Alairas_Kell.ToString());
            if (eAlairasKell == bool.TrueString)
            {

                string pki_vendor = Rendszerparameterek.Get(xparam, "PKI_VENDOR");
                if ("netlock".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase))
                {
                    var netlockService = new NetLockSignAssistService();
                    try
                    {
                        contentSignedPdf = netlockService.Sign(null, templateFileName, contentOriginalPdf);
                    }
                    catch (Exception x)
                    {
                        var errorMessage = x.Message;
                        Logger.Warn("NETLOCKSignatureService hiba: " + errorMessage);
                        AddMessage("EKF: " + string.Format("NETLOCKSignatureService hiba: " + errorMessage));
                        netlockService.ValidationReport.Dump();
                        throw new ResultException(string.Format("NETLOCKSignatureService hiba: " + errorMessage));
                    }
                }
            }
            #endregion E-ALAIRAS KELL?

            string newKuldemenyId = null;
            string newIratId = null;
            string newDokumentumId = null;
            string newUgyId = null;

            // BLG_1643
            // Kiemelve az iktatás alól
            #region CSATOLMANY LETREHOZAS
            Csatolmany csatolmany = new Csatolmany();
            if (contentSignedPdf != null)
                csatolmany.Tartalom = contentSignedPdf;
            else
                csatolmany.Tartalom = contentOriginalPdf;

            csatolmany.Nev = templateFileName;
            csatolmany.TartalomHash = Contentum.eUtility.FileFunctions.CalculateSHA1(csatolmany.Tartalom);
            csatolmany.Megnyithato = "1";
            csatolmany.Olvashato = "1";
            csatolmany.Titkositas = "0";
            #endregion CSATOLMANY LETREHOZAS

            // BLG_1643
            // Kiszedve Iktatás alól
            #region E BEADVANY GET
            EREC_eBeadvanyokService serviceEbeadvany = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            ExecParam execParamEBeadvGet = rule.GetExecParam();
            execParamEBeadvGet.Record_Id = eBeadvanyId;
            Result resultEBeadvanyGet = serviceEbeadvany.Get(execParamEBeadvGet);
            if (!string.IsNullOrEmpty(resultEBeadvanyGet.ErrorCode))
            {
                AddMessage(resultEBeadvanyGet);
                throw new ResultException(resultEBeadvanyGet);
            }
            EREC_eBeadvanyok eBeadvany = (EREC_eBeadvanyok)resultEBeadvanyGet.Record;
            #endregion E BEADVANY GET

            #region IKTATAS KELL?
            if (iktatniKell == bool.TrueString)
            {
                string prevIratId = EKFManager.GetDataSetValue(inputData, "IraIrat_Id");
                if (string.IsNullOrEmpty(prevIratId))
                {
                    Logger.Error("EKFPostOperationAutoValasz.Iktatas hiba: Nem létezik irat");
                }
                else
                {
                    newKuldemenyId = Guid.NewGuid().ToString();
                    newIratId = Guid.NewGuid().ToString();
                    newDokumentumId = Guid.NewGuid().ToString();

                    #region GET PREVIRAT
                    EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                    ExecParam xparamI = rule.GetExecParam();
                    xparamI.Record_Id = prevIratId;
                    Result iratResultGet = iraIratokService.Get(xparamI);

                    if (!string.IsNullOrEmpty(iratResultGet.ErrorCode))
                    {
                        AddMessage(iratResultGet);
                        throw new ResultException(iratResultGet);
                    }
                    EREC_IraIratok previousIrat = (EREC_IraIratok)iratResultGet.Record;
                    #endregion  GET PREVIRAT

                    #region GET UGYIRAT
                    EREC_UgyUgyiratokService ugyUgyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                    ExecParam xparamU = rule.GetExecParam();
                    xparamI.Record_Id = newIratId;
                    Result ugyResultGet = ugyUgyiratokService.Get(xparamU);

                    if (!string.IsNullOrEmpty(iratResultGet.ErrorCode))
                    {
                        AddMessage(iratResultGet);
                        throw new ResultException(iratResultGet);
                    }
                    EREC_UgyUgyiratok ugy = (EREC_UgyUgyiratok)ugyResultGet.Record;

                    if (ugy != null)
                        newUgyId = ugy.Id;
                    #endregion  GET UGYIRAT

                    #region UJ IRAT
                    EREC_IraIratok newIrat = new EREC_IraIratok();
                    newIrat.Id = newIratId;
                    newIrat.Targy = previousIrat.Targy;
                    newIrat.Irattipus = previousIrat.Irattipus;
                    newIrat.Jelleg = previousIrat.Jelleg;
                    newIrat.KiadmanyozniKell = "0";
                    newIrat.FelhasznaloCsoport_Id_Ugyintez = previousIrat.FelhasznaloCsoport_Id_Ugyintez;
                    newIrat.Irattipus = previousIrat.Irattipus;
                    newIrat.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
                    newIrat.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Kimeno;
                    newIrat.Csoport_Id_Felelos = previousIrat.Csoport_Id_Felelos;
                    newIrat.Csoport_Id_Ugyfelelos = previousIrat.Csoport_Id_Ugyfelelos;
                    newIrat.Hatarido = ugy.Hatarido;

                    EKFManager.SetIratExpedialasModja(rule, ref newIrat);
                    //string expedMod = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_AutoValasz.Valasz_Uzenet_Sablon_Expedialas_Modja.ToString());
                    //if (!string.IsNullOrEmpty(expedMod))
                    //    newIrat.ExpedialasModja = expedMod;
                    #endregion UJ IRAT

                    #region BELSO IRAT IKTATASA
                    Logger.Info("EKFPostOperationAutoValasz.Iktatas.BelsoIratIktatasa_Alszamra Start");
                    Result resultIratIktatas = iraIratokService.BelsoIratIktatasa_Alszamra(
                          xparam,
                          ugy.IraIktatokonyv_Id,
                          ugy.Id,
                          new EREC_UgyUgyiratdarabok(),
                          newIrat,
                          new EREC_HataridosFeladatok(),
                          new EREC_PldIratPeldanyok(),
                          new IktatasiParameterek()
                          );

                    if (!string.IsNullOrEmpty(resultIratIktatas.ErrorCode))
                    {
                        AddMessage(resultIratIktatas);
                        throw new ResultException(resultIratIktatas);
                    }
                    Logger.Info("EKFPostOperationAutoValasz.Iktatas.BelsoIratIktatasa_Alszamra Stop");
                    #endregion BELSO IRAT IKTATASA

                    // BLG_1643
                    //#region CSATOLMANY LETREHOZAS
                    //Csatolmany csatolmany = new Csatolmany();
                    //if (contentSignedPdf != null)
                    //    csatolmany.Tartalom = contentSignedPdf;
                    //else
                    //    csatolmany.Tartalom = contentOriginalPdf;

                    //csatolmany.Nev = templateFileName;
                    //csatolmany.TartalomHash = Contentum.eUtility.FileFunctions.CalculateSHA1(csatolmany.Tartalom);
                    //csatolmany.Megnyithato = "1";
                    //csatolmany.Olvashato = "1";
                    //csatolmany.Titkositas = "0";
                    #region CSATOLMANY KIEGESZITES, IRAT CSATOLMANY LETREHOZAS
                    string pki_vendor = Contentum.eUtility.Rendszerparameterek.Get(rule.GetExecParam(), "PKI_VENDOR");
                    if ("netlock".Equals(pki_vendor, StringComparison.InvariantCultureIgnoreCase))
                    {
                        csatolmany.ElektronikusAlairas = KodTarak.DOKUMENTUM_ALAIRAS_PKI.Ervenyes_alairas;
                    }
                    else
                    {
                        csatolmany.ElektronikusAlairas = "<null>";
                    }

                    EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
                    erec_Csatolmanyok.Dokumentum_Id = newDokumentumId;
                    erec_Csatolmanyok.IraIrat_Id = newIratId;
                    erec_Csatolmanyok.Leiras = null;
                    erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
                    #endregion IRAT CSATOLMANY LETREHOZAS

                    #region IRAT CSATOLMANY FELTOLTES
                    Logger.Info("EKFPostOperationAutoValasz.Iktatas.IraIratok.CsatolmanyUpload Start");

                    ExecParam xparamIratCsatolmany = rule.GetExecParam();
                    EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    xparamIratCsatolmany.Record_Id = newIratId;
                    Result resultIratCsatolmanyFeltoltes = erec_IraIratokService.CsatolmanyUpload(xparamIratCsatolmany, erec_Csatolmanyok, csatolmany);

                    if (!string.IsNullOrEmpty(resultIratCsatolmanyFeltoltes.ErrorCode))
                    {
                        AddMessage(resultIratCsatolmanyFeltoltes);
                        throw new ResultException(resultIratCsatolmanyFeltoltes);
                    }

                    Logger.Info("EKFPostOperationAutoValasz.Iktatas.IraIratok.CsatolmanyUpload Stop");
                    #endregion CSATOLMANY FELTOLTES

                    #region ALÁÍRÓK LETREHOZAS
                    EREC_IratAlairok erec_IratAlairok = new EREC_IratAlairok();
                    erec_IratAlairok.AlairasDatuma = DateTime.Now.ToString();
                    erec_IratAlairok.Obj_Id = newIratId;
                    erec_IratAlairok.FelhasznaloCsoport_Id_Alairo = newIrat.Csoport_Id_Felelos;
                    erec_IratAlairok.AlairoSzerep = KodTarak.ALAIRO_SZEREP.Kiadmanyozo;
                    erec_IratAlairok.AlairasSzabaly_Id = null;
                    erec_IratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
                    erec_IratAlairok.AlairasSorrend = null;
                    erec_IratAlairok.Leiras = null;
                    erec_IratAlairok.AlairasMod = "M_UTO";
                    #endregion ALÁÍRÓK LETREHOZAS

                    #region ALÁÍRÓK INSERT
                    Logger.Info("EKFPostOperationAutoValasz.Iktatas.Alairok.Insert Start");

                    EREC_IratAlairokService serviceAlairok = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
                    ExecParam execParamAlairok = rule.GetExecParam();
                    Result resultAlairok = serviceAlairok.Insert(execParamAlairok, erec_IratAlairok);
                    if (!string.IsNullOrEmpty(resultAlairok.ErrorCode))
                    {
                        AddMessage(resultAlairok);
                        throw new ResultException(resultAlairok);
                    }

                    Logger.Info("EKFPostOperationAutoValasz.Iktatas.Alairok.Insert Stop");
                    #endregion ALÁÍRÓK INSERT

                    // BLG_1643 
                    // Kiszedve előre
                    //#region E BEADVANY GET
                    //EREC_eBeadvanyokService serviceEbeadvany = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
                    //ExecParam execParamEBeadvGet = rule.GetExecParam();
                    //execParamEBeadvGet.Record_Id = eBeadvanyId;
                    //Result resultEBeadvanyGet = serviceEbeadvany.Get(execParamEBeadvGet);
                    //if (!string.IsNullOrEmpty(resultEBeadvanyGet.ErrorCode))
                    //{
                    //    AddMessage(resultEBeadvanyGet);
                    //    throw new ResultException(resultEBeadvanyGet);
                    //}
                    //EREC_eBeadvanyok eBeadvany = (EREC_eBeadvanyok)resultEBeadvanyGet.Record;
                    //#endregion E BEADVANY GET

                    #region IRAT PELDANY EXPEDIALAS
                    erec_IratAlairok = (EREC_IratAlairok)resultAlairok.Record;
                    if (erec_IratAlairok.PldIratPeldany_Id == null)
                    {
                        AddMessage("EKF: EKFPostOperationAutoValasz.Iktatas.Expedialas Nincs PldIratPeldany.");
                        throw new ResultException("EKFPostOperationAutoValasz.Iktatas.Expedialas Nincs PldIratPeldany.");
                    }

                    EREC_PldIratPeldanyokService servicePldIrat = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                    ExecParam execParamPldIratGet = rule.GetExecParam();
                    execParamPldIratGet.Record_Id = erec_IratAlairok.PldIratPeldany_Id;
                    Result resultPldIratGet = servicePldIrat.Get(execParamPldIratGet);
                    if (!string.IsNullOrEmpty(resultPldIratGet.ErrorCode))
                    {
                        AddMessage(resultPldIratGet);
                        throw new ResultException(resultPldIratGet);
                    }

                    Logger.Info("EKFPostOperationAutoValasz.Iktatas.PldIratPeldanyok.Expedialas_HivataliKapus Start");
                    EREC_PldIratPeldanyok pldIratPeldany = (EREC_PldIratPeldanyok)resultPldIratGet.Record;
                    ExecParam execParamPldIratExpedial = rule.GetExecParam();
                    Result resultPldIratExpedialas =
                        servicePldIrat.Expedialas_HivataliKapus(
                        execParamPldIratExpedial,
                        new string[] { pldIratPeldany.Id },
                        null,
                        pldIratPeldany,
                        eBeadvany,
                        null,
                        null);

                    Logger.Info("EKFPostOperationAutoValasz.Iktatas.PldIratPeldanyok.Expedialas_HivataliKapus Stop");
                    #endregion IRAT PELDANY EXPEDIALAS
                }
            }
            #endregion IKTATAS KELL?

            #region IRATTAROZNI KELL?
            if (irattarozniKell == bool.TrueString && !string.IsNullOrEmpty(newUgyId))
            {
                #region GET UGYIRAT
                EREC_UgyUgyiratokService ugyUgyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                ExecParam xparamUgy = rule.GetExecParam();
                xparamUgy.Record_Id = newUgyId;
                Result ugyResultGetUgyEIrattarba = ugyUgyiratokService.Get(xparamUgy);

                if (!string.IsNullOrEmpty(ugyResultGetUgyEIrattarba.ErrorCode))
                {
                    AddMessage(ugyResultGetUgyEIrattarba);
                    throw new ResultException(ugyResultGetUgyEIrattarba);
                }
                EREC_UgyUgyiratok ugyEIrattarba = (EREC_UgyUgyiratok)ugyResultGetUgyEIrattarba.Record;
                #endregion  GET UGYIRAT

                #region IRATTARBA
                ExecParam execParamUgyEIrattarba = rule.GetExecParam();

                string elektronikusIrattarId = KodTarak.SPEC_SZERVEK.GetElektronikusIrattar(execParamUgyEIrattarba);
                execParamUgyEIrattarba.Record_Id = ugyEIrattarba.Id;

                ugyEIrattarba.Csoport_Id_Felelos = elektronikusIrattarId;
                ugyEIrattarba.Updated.Csoport_Id_Felelos = true;

                ugyEIrattarba.FelhasznaloCsoport_Id_Orzo = elektronikusIrattarId;
                ugyEIrattarba.Updated.FelhasznaloCsoport_Id_Orzo = true;

                Result resultUgyEIrattarba = ugyUgyiratokService.Update(execParamUgyEIrattarba, ugyEIrattarba);

                if (resultUgyEIrattarba.IsError)
                {
                    AddMessage(resultUgyEIrattarba);
                    throw new ResultException(resultUgyEIrattarba);
                }
                #endregion IRATTARBA
            }
            #endregion IRATTAROZNI KELL?

            //BLG_1643
            #region UZENETKULDES

            #region Érkeztetési szám  Küldeményből
            EREC_KuldKuldemenyekService serviceKuldemenyek = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParamKuldemenyGet = rule.GetExecParam();
            execParamKuldemenyGet.Record_Id = eBeadvany.KuldKuldemeny_Id;
            Result resultKuldemenyGet = serviceKuldemenyek.Get(execParamKuldemenyGet);
            if (!string.IsNullOrEmpty(resultKuldemenyGet.ErrorCode))
            {
                AddMessage(resultKuldemenyGet);
                throw new ResultException(resultKuldemenyGet);
            }
            EREC_KuldKuldemenyek kuldemeny = (EREC_KuldKuldemenyek)resultKuldemenyGet.Record;
            #endregion Érkeztetési szám  Küldeményből

            // TODO IS HIVATALI KAPU
            ExecParam execParam = rule.GetExecParam();
            HivataliKapuFileSettings fileSettings = new HivataliKapuFileSettings();
            fileSettings.EncryptFiles = false; // cbEncrypt.Checked; titkosítás kell-e
            fileSettings.PackFiles = false;
            fileSettings.PackedFileName = null;
            fileSettings.FileFormat = HivataliKapuFileSettings.HivataliKapuFileFormat.Krx;


            //if (fileSettings != null && fileSettings.EncryptFiles)
            //{
            //    EncryptKuldemenyCsatolmanyai(execParam, kuldemenyId, _EREC_eBeadvanyok, fileSettings);
            //}

            #region hivatali kapu

            List<EREC_eBeadvanyok> eBeadvanyok = new List<EREC_eBeadvanyok>();
            List<Csatolmany> csatolmanyok = new List<Csatolmany>();


            EREC_eBeadvanyok _eBeadvany = new EREC_eBeadvanyok();
            _eBeadvany.Updated.SetValueAll(false);
            _eBeadvany.Base.Updated.SetValueAll(false);

            _eBeadvany.Irany = "1"; //Kimenő
            _eBeadvany.Updated.Irany = true;
            _eBeadvany.Allapot = "11"; //Hivatali kapunak elküldött
            _eBeadvany.Updated.Allapot = true;

            if (String.IsNullOrEmpty(EKFManager.GetDataSetValue(inputData, "KR_DokTipusHivatal")))
            {
                _eBeadvany.KR_DokTipusHivatal = GetDokTipusHivatal(execParam);
                _eBeadvany.Updated.KR_DokTipusHivatal = true;
            }
            else
            {
                _eBeadvany.KR_DokTipusHivatal = EKFManager.GetDataSetValue(inputData, "KR_DokTipusHivatal");
                _eBeadvany.Updated.KR_DokTipusHivatal = true;
            }

            _eBeadvany.KR_DokTipusAzonosito = "Értesítés " + kuldemeny.Azonosito; //"Valasz"+EKFManager.GetDataSetValue(inputData, "KR_DokTipusAzonosito");
            _eBeadvany.Updated.KR_DokTipusAzonosito = true;
            _eBeadvany.KR_DokTipusLeiras = "Válasz:" + EKFManager.GetDataSetValue(inputData, "KR_DokTipusLeiras");
            _eBeadvany.Updated.KR_DokTipusLeiras = true;

            _eBeadvany.KR_FileNev = csatolmany.Nev;
            _eBeadvany.Updated.KR_FileNev = true;
            // _eBeadvany.Base.Note = null; // Ha nincs iktatás null!!!; dokCsat.DokumentumId;
            //  _eBeadvany.KuldKuldemeny_Id = null; //Ha nincs iktatás null; feltoltesSeged.KuldemenyId;
            //  _eBeadvany.Updated.KuldKuldemeny_Id = true;
            _eBeadvany.KR_Valaszutvonal = "0"; // Közmű
            _eBeadvany.Updated.KR_Valaszutvonal = true;
            _eBeadvany.KR_Valasztitkositas = "0"; //nem
            _eBeadvany.Updated.KR_Valasztitkositas = true;
            _eBeadvany.KR_Rendszeruzenet = "1"; //igen
            _eBeadvany.Updated.KR_Rendszeruzenet = true;

            #region címzett adatok

            _eBeadvany.FeladoTipusa = EKFManager.GetDataSetValue(inputData, "FeladoTipusa");
            _eBeadvany.Updated.FeladoTipusa = true;
            _eBeadvany.PartnerKapcsolatiKod = EKFManager.GetDataSetValue(inputData, "PartnerKapcsolatiKod");
            _eBeadvany.Updated.PartnerKapcsolatiKod = true;
            _eBeadvany.PartnerNev = EKFManager.GetDataSetValue(inputData, "PartnerNev");
            _eBeadvany.Updated.PartnerNev = true;
            _eBeadvany.PartnerEmail = EKFManager.GetDataSetValue(inputData, "PartnerEmail");
            _eBeadvany.Updated.PartnerEmail = true;

            _eBeadvany.PartnerKRID = EKFManager.GetDataSetValue(inputData, "PartnerKRID");
            _eBeadvany.Updated.PartnerKRID = true;
            _eBeadvany.PartnerRovidNev = EKFManager.GetDataSetValue(inputData, "PartnerRovidNev");
            _eBeadvany.Updated.PartnerRovidNev = true;
            _eBeadvany.PartnerMAKKod = EKFManager.GetDataSetValue(inputData, "PartnerMAKKod");
            _eBeadvany.Updated.PartnerMAKKod = true;

            #endregion címzett adatok

            _eBeadvany.KR_ETertiveveny = EKFManager.GetDataSetValue(inputData, "KR_ETertiveveny");
            _eBeadvany.Updated.KR_ETertiveveny = true;

            _eBeadvany.KR_Megjegyzes = "Automatikus válaszüzenet";// EKFManager.GetDataSetValue(inputData, "KR_Megjegyzes");
            _eBeadvany.Updated.KR_Megjegyzes = true;

            // megjelöljük, hogy melyik üzenetre megy a válasz
            _eBeadvany.KR_HivatkozasiSzam = EKFManager.GetDataSetValue(inputData, "KR_ErkeztetesiSzam");
            _eBeadvany.Updated.KR_HivatkozasiSzam = true;

            #region kr file

            string ext = Path.GetExtension(csatolmany.Nev);

            //if (".kr".Equals(ext, StringComparison.InvariantCultureIgnoreCase))
            //{
            //    LoadKRFileMetaAdatok(_eBeadvany, csatolmany.Tartalom);
            //}

            #endregion

            _eBeadvany.KR_Fiok = EKFManager.GetDataSetValue(inputData, "KR_Fiok");
            _eBeadvany.Updated.KR_Fiok = true;

            _eBeadvany.Cel = EKFManager.GetDataSetValue(inputData, "Cel");
            _eBeadvany.Updated.Cel = true;

            eBeadvanyok.Add(_eBeadvany);
            csatolmanyok.Add(csatolmany);
            try
            {
                ExecParam execParameBeadvanyok = rule.GetExecParam();
                serviceEbeadvany.SendHKPAutomataValasz(EKFManager.GetDataSetValue(inputData, "KR_Fiok"), eBeadvanyok, csatolmanyok, execParameBeadvanyok);
            }
            catch (ResultException e)
            {
                AddMessage(e.GetResult());
                throw new ResultException(e.GetResult());
            }
            #endregion

            //#region üzenet eltárolása
            //EREC_eBeadvanyokService eBeadvanyokServcie = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            //EREC_eBeadvanyCsatolmanyokService eBeadvanyCsatolmanyokService = eRecordService.ServiceFactory.GetEREC_eBeadvanyCsatolmanyokService();

            //foreach (EREC_eBeadvanyok eBeadvanyInsert in eBeadvanyok)
            //{
            //    string dokumentumId = eBeadvanyInsert.Base.Note;
            //    eBeadvanyInsert.Base.Note = String.Empty;

            //    ExecParam execParameBeadvanyok = rule.GetExecParam();
            //    Result reseBeadvanyok = eBeadvanyokServcie.Insert(execParameBeadvanyok, eBeadvanyInsert);

            //    if (reseBeadvanyok.IsError)
            //    {
            //        throw new Contentum.eUtility.ResultException(reseBeadvanyok);
            //    }

            //    string neweBeadvanyId = reseBeadvanyok.Uid;

            //    EREC_eBeadvanyCsatolmanyok eBeadvanyCsatolmany = new EREC_eBeadvanyCsatolmanyok();
            //    eBeadvanyCsatolmany.eBeadvany_Id = neweBeadvanyId;
            //    eBeadvanyCsatolmany.Dokumentum_Id = dokumentumId;
            //    eBeadvanyCsatolmany.Nev = eBeadvanyInsert.KR_FileNev;


            //    Result reseBeadvanyokCsatolmany = eBeadvanyCsatolmanyokService.Insert(execParameBeadvanyok, eBeadvanyCsatolmany);

            //    if (reseBeadvanyokCsatolmany.IsError)
            //    {
            //        throw new Contentum.eUtility.ResultException(reseBeadvanyokCsatolmany);
            //    }
            // }

            //#endregion
            #endregion UZENETKULDES
            AddMessage("EKF: AutoValasz vege");
            return true;
        }

        // BLG_1643
        string GetDokTipusHivatal(ExecParam execParam)
        {
            string hivatal = "FPH";

            string orgId = execParam.Org_Id;

            if (String.IsNullOrEmpty(orgId))
                orgId = "450B510A-7CAA-46B0-83E3-18445C0C53A9"; //default org

            KRT_OrgokService service = eAdminService.ServiceFactory.GetKRT_OrgokService();
            ExecParam execParamGet = execParam.Clone();
            execParamGet.Record_Id = orgId;
            Result res = service.Get(execParamGet);

            if (!res.IsError)
            {
                KRT_Orgok org = res.Record as KRT_Orgok;
                hivatal = org.Kod;
            }

            return hivatal;
        }

        EREC_eBeadvanyok GetKRFileMataAdatok(byte[] fileContent)
        {
            EREC_eBeadvanyok adatok = new EREC_eBeadvanyok();

            try
            {
                MemoryStream ms = new MemoryStream(fileContent);
                XmlReader reader = XmlReader.Create(ms);
                XDocument doc = XDocument.Load(reader);

                var fejlec = from element in doc.Root.Elements()
                             where element.Name.LocalName == "Fejlec"
                             select element;

                adatok.PartnerRovidNev = (from element in fejlec.Elements()
                                          where element.Name.LocalName == "Cimzett"
                                          select element.Value).FirstOrDefault();

                adatok.KR_DokTipusAzonosito = (from element in fejlec.Elements()
                                               where element.Name.LocalName == "DokTipusAzonosito"
                                               select element.Value).FirstOrDefault();

                adatok.KR_DokTipusLeiras = (from element in fejlec.Elements()
                                            where element.Name.LocalName == "DokTipusLeiras"
                                            select element.Value).FirstOrDefault();

                adatok.KR_FileNev = (from element in fejlec.Elements()
                                     where element.Name.LocalName == "FileNev"
                                     select element.Value).FirstOrDefault();

                adatok.KR_Megjegyzes = (from element in fejlec.Elements()
                                        where element.Name.LocalName == "Megjegyzes"
                                        select element.Value).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logger.Error("GetKRFileMataAdatok.Error", ex);
                return null;
            }

            return adatok;
        }

        void LoadKRFileMetaAdatok(EREC_eBeadvanyok dok, byte[] fileContent)
        {
            EREC_eBeadvanyok metaAdatok = GetKRFileMataAdatok(fileContent);

            if (metaAdatok != null)
            {
                dok.KR_DokTipusAzonosito = metaAdatok.KR_DokTipusAzonosito;
                dok.KR_DokTipusLeiras = metaAdatok.KR_DokTipusLeiras;
                dok.KR_Megjegyzes = metaAdatok.KR_Megjegyzes;
            }
        }
    }
}
