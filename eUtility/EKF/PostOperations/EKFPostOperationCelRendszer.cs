﻿using Contentum.eBusinessDocuments;
using System;
using System.Data;
using System.Text;

namespace Contentum.eUtility.EKF
{
    public class EKFPostOperationCelRendszer : EKFBaseOperation
    {
        public EKFPostOperationCelRendszer()
        {
            OperationName = EnumEKFOperationName.CELRENDSZER;
            TypeFullName = this.GetType().FullName;
            OperationType = EnumEKFOperationType.POST;
        }
        public void InitParameters()
        {
            foreach (Enum_ParameterNames_Post_CelRendszer name in Enum.GetValues(typeof(Enum_ParameterNames_Post_CelRendszer)))
            {
                ParameterProperty.Parameters.Add(new EKFOperationParameterValue()
                {
                    IdProperty = name.ToString(),
                    NameProperty = name.ToString(),
                });
            }
        }
        public override string ToString()
        {
            return OperationName.ToString();
        }
        public override bool Execute(EKFRuleClass rule, DataSet inputData, out Result result)
        {
            result = null;
            AddMessage("EKF: CelRendszer indul");

            string targy = string.Empty;
            StringBuilder body = new StringBuilder();
            ExecParam xparamMail = rule.GetExecParam();

            string celErtekString = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_CelRendszer.CelRendszerErtek.ToString());
            if (string.IsNullOrEmpty(celErtekString))
            {
                AddMessage("EKF: Cél rendszer érték parameter nincs megadva !");
            }
            else
            {
                ExecParam xparamIr = rule.GetExecParam();
                EKFManager manager = new EKFManager(xparamIr);
                EREC_eBeadvanyok resultEBeadvany = manager.GetEBeadvanyByDataSet(inputData);
                if (resultEBeadvany == null)
                    return false;
                resultEBeadvany.Cel = celErtekString;
                resultEBeadvany.Updated.Cel = true;

                manager.UpdateEBeadvany(resultEBeadvany);
            }

            AddMessage("EKF: CelRendszer vege");
            return true;
        }
    }
}