﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Collections.Generic;
using System.Data;

namespace Contentum.eUtility.EKF
{
    public class EKFPostOperationElintezetteNyilvanitas : EKFBaseOperation
    {
        public const string TargyszoAzon_FT1 = "FT1";
        public const string TargyszoAzon_FT2 = "FT2";
        public const string TargyszoAzon_FT3 = "FT3";

        public EKFPostOperationElintezetteNyilvanitas()
        {
            OperationName = EnumEKFOperationName.ELINTEZETTE_NYILVANITAS;
            TypeFullName = this.GetType().FullName;
            OperationType = EnumEKFOperationType.POST;
        }
        public void InitParameters()
        {
            foreach (Enum_ParameterNames_Post_ElintezetteNyilvanitas name in Enum.GetValues(typeof(Enum_ParameterNames_Post_ElintezetteNyilvanitas)))
            {
                ParameterProperty.Parameters.Add(new EKFOperationParameterValue()
                {
                    IdProperty = name.ToString(),
                    NameProperty = name.ToString(),
                });
            }
        }
        public override string ToString()
        {
            return OperationName.ToString();
        }
        public override bool Execute(EKFRuleClass rule, DataSet inputData, out Result result)
        {
            ExecParam xparamIr = rule.GetExecParam();
            AddMessage("EKF: ElintezetteNyilvanitas indul");

            result = null;

            EREC_eBeadvanyok resultEBeadvany = new EKFManager(xparamIr).GetEBeadvanyByDataSet(inputData);
            if (resultEBeadvany == null)
                return false;

            string iratId = resultEBeadvany.IraIrat_Id;

            if (string.IsNullOrEmpty(iratId))
            {
                AddMessage("EKF: Irat nem található !");
                throw new Exception("Irat nem található !");
            }

            EREC_IraIratok irat = new EKFManager(xparamIr).GetIrat(rule, iratId);
            if (irat == null)
            {
                AddMessage("EKF: Irat nem található !");
                throw new Exception("Irat nem található !");
            }

            EREC_UgyUgyiratok ugy = new EKFManager(xparamIr).GetUgyirat(rule, irat.Ugyirat_Id);
            if (ugy == null)
            {
                AddMessage("EKF: Ugy nem található !");
                throw new Exception("Ugy nem található !");
            }

            #region IRATTARI TETEL
            string irattariTetelSzam = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_ElintezetteNyilvanitas.Irattari_Tetelszam.ToString());
            string irattariTetelId;
            if (new EKFMainOperationIktatas().GetIrattariTetelId(rule, irattariTetelSzam, out irattariTetelId))
            {
                if (!string.IsNullOrEmpty(irattariTetelId))
                {
                    ugy.IraIrattariTetel_Id = irattariTetelId;
                    ugy.Updated.IraIrattariTetel_Id = true;

                    #region SAVE UGY IRATTARI TETEL

                    ExecParam xparamUpdate = rule.GetExecParam();
                    xparamUpdate.Record_Id = ugy.Id;

                    Result resultUgyUpd = new EKFManager(xparamIr).UpdateUgyirat(rule, ugy);
                    #endregion
                }
            }

            #endregion IRATTARI TETEL

            #region TARGYSZO

            ExecParam xparamUgyTargySzo = rule.GetExecParam();
            string FT1 = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_ElintezetteNyilvanitas.FT1.ToString());
            string FT2 = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_ElintezetteNyilvanitas.FT2.ToString());
            string FT3 = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_ElintezetteNyilvanitas.FT3.ToString());
            SetObjektumTargyszo(rule, irat.Ugyirat_Id, TargyszoAzon_FT1, FT1);
            SetObjektumTargyszo(rule, irat.Ugyirat_Id, TargyszoAzon_FT2, FT2);
            SetObjektumTargyszo(rule, irat.Ugyirat_Id, TargyszoAzon_FT3, FT3);

            //this.SetObjektumTargyszo(new Guid(irat.Ugyirat_Id), eUtility.Constants.TableNames.EREC_UgyUgyiratok, TargyszoAzon_FT3, FT1, xparamUgyTargySzo);
            //this.SetObjektumTargyszo(new Guid(irat.Ugyirat_Id), eUtility.Constants.TableNames.EREC_UgyUgyiratok, TargyszoAzon_FT3, FT2, xparamUgyTargySzo);
            //this.SetObjektumTargyszo(new Guid(irat.Ugyirat_Id), eUtility.Constants.TableNames.EREC_UgyUgyiratok, TargyszoAzon_FT3, FT3, xparamUgyTargySzo);
            #endregion TARGYSZO

            #region GET UGYIRAT
            ugy = new EKFManager(xparamIr).GetUgyirat(rule, ugy.Id);
            if (ugy == null)
                return false;
            #endregion  GET UGYIRAT

            EREC_UgyUgyiratokService ugyIratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            #region UGYINTEZES ALATT
            if (ugy.Allapot != KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt)
            {
                ExecParam xparamUgyintezesre = rule.GetExecParam();
                xparamUgyintezesre.Record_Id = ugy.Id;

                Result resultATV = ugyIratokService.AtvetelUgyintezesre(xparamUgyintezesre, ugy.Id);
                if (!string.IsNullOrEmpty(resultATV.ErrorCode))
                {
                    AddMessage(resultATV);
                    throw new ResultException(resultATV);
                }
            }
            #endregion

            #region HATARIDOS FELADAT
            EREC_HataridosFeladatok erec_HataridosFeladatok = new EREC_HataridosFeladatok();
            erec_HataridosFeladatok.Obj_Id = ugy.Id;
            erec_HataridosFeladatok.Updated.Obj_Id = true;

            erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
            erec_HataridosFeladatok.Updated.Obj_type = true;

            erec_HataridosFeladatok.Azonosito = ugy.Azonosito.ToString();
            erec_HataridosFeladatok.Updated.Azonosito = true;

            erec_HataridosFeladatok.Tipus = eUtility.KodTarak.FELADAT_TIPUS.Megjegyzes;
            erec_HataridosFeladatok.Updated.Tipus = true;

            erec_HataridosFeladatok.Altipus = eUtility.KodTarak.FELADAT_ALTIPUS.Megjegyzes;
            erec_HataridosFeladatok.Updated.Tipus = true;

            #endregion

            #region ELINTEZ
            ExecParam xparamUgyElintez = rule.GetExecParam();
            xparamUgyElintez.Record_Id = ugy.Id;
            result = ugyIratokService.ElintezetteNyilvanitas(
                xparamUgyElintez,
                ugy.ElintezesDat,
                ugy.ElintezesMod,
                erec_HataridosFeladatok
                );

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage(result);
                throw new ResultException(result);
            }
            #endregion ELINTEZ
            AddMessage("EKF: ElintezetteNyilvanitas vege");
            return true;
        }
        /// <summary>
        /// GetTargySzo
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private Result GetTargySzo(EKFRuleClass rule, string key)
        {
            EREC_TargySzavakService tszService = eRecordService.ServiceFactory.GetEREC_TargySzavakService();
            ExecParam xparamTsz = rule.GetExecParam();
            EREC_TargySzavakSearch src = new EREC_TargySzavakSearch();
            src.BelsoAzonosito.Value = key;
            src.BelsoAzonosito.Operator = Query.Operators.equals;
            src.TopRow = 1;
            Result resultTsz = tszService.GetAll(xparamTsz, src);

            if (!string.IsNullOrEmpty(resultTsz.ErrorCode))
            {
                AddMessage(resultTsz);
                throw new ResultException(resultTsz);
            }
            return resultTsz;
        }
        private Result GetEREC_Obj_MetaAdatai(EKFRuleClass rule, string targyszoId)
        {
            EREC_Obj_MetaAdataiService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
            ExecParam xparamMA = rule.GetExecParam();
            EREC_Obj_MetaAdataiSearch src = new EREC_Obj_MetaAdataiSearch();
            src.Targyszavak_Id.Value = targyszoId;
            src.Targyszavak_Id.Operator = Query.Operators.equals;
            src.TopRow = 1;
            Result result = service.GetAll(xparamMA, src);

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage(result);
                throw new ResultException(result);
            }
            return result;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="ugyirat_Id"></param>
        /// <param name="targyszoAzon"></param>
        /// <param name="ertek"></param>
        private void SetObjektumTargyszo(EKFRuleClass rule, string ugyirat_Id, string targyszoAzon, string ertek)
        {
            Result resultTsz = GetTargySzo(rule, targyszoAzon);
            if (resultTsz.Ds.Tables[0].Rows.Count < 0)
                return;

            Result resultMa = GetEREC_Obj_MetaAdatai(rule, resultTsz.Ds.Tables[0].Rows[0]["Id"].ToString());
            if (resultMa.Ds.Tables[0].Rows.Count < 0)
                return;

            EKFPostOperationTargyszavak tsz = new EKFPostOperationTargyszavak();
            string resultTargySzoObjektumTipusId_Ugy = tsz.GetObjTipusId(rule, "EREC_UgyUgyiratok");

            #region UGYIRAT
            EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
            erec_ObjektumTargyszavai.Obj_Id = ugyirat_Id;
            erec_ObjektumTargyszavai.Updated.Obj_Id = true;

            erec_ObjektumTargyszavai.Forras = KodTarak.TARGYSZO_FORRAS.Automatikus;
            erec_ObjektumTargyszavai.Updated.Forras = true;

            erec_ObjektumTargyszavai.ObjTip_Id = resultTargySzoObjektumTipusId_Ugy;
            erec_ObjektumTargyszavai.Updated.ObjTip_Id = true;

            erec_ObjektumTargyszavai.Ertek = ertek;
            erec_ObjektumTargyszavai.Updated.Ertek = true;

            erec_ObjektumTargyszavai.Targyszo_Id = resultTsz.Ds.Tables[0].Rows[0]["Id"].ToString();
            erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;

            erec_ObjektumTargyszavai.Targyszo = targyszoAzon;
            erec_ObjektumTargyszavai.Updated.Targyszo = true;

            erec_ObjektumTargyszavai.Sorszam = "1";
            erec_ObjektumTargyszavai.Updated.Sorszam = true;

            erec_ObjektumTargyszavai.Obj_Metaadatai_Id = resultMa.Ds.Tables[0].Rows[0]["Id"].ToString();
            erec_ObjektumTargyszavai.Updated.Obj_Metaadatai_Id = true;

            tsz.AddEREC_ObjektumTargyszavai(rule, erec_ObjektumTargyszavai);
            #endregion
        }
        /// <summary>
        /// Megadott objektumhoz a megadott tárgyszó bejegyzése (Insert/Update)
        /// </summary>
        /// <param name="objId"></param>
        /// <param name="objTableName"></param>
        /// <param name="targyszoAzon"></param>
        /// <param name="ertek"></param>
        /// <param name="execParamUserId"></param>
        private void SetObjektumTargyszo(Guid objId, string objTableName, string targyszoAzon, string ertek, ExecParam execParamUserId)
        {
            this.SetObjektumTargyszavak(objId, objTableName, execParamUserId, new KeyValuePair<string, string>(targyszoAzon, ertek));
        }

        /// <summary>
        /// Megadott objektumhoz a megadott tárgyszó/érték párok bejegyzése (tömeges)
        /// </summary>
        /// <param name="objId"></param>
        /// <param name="objTableName"></param>
        /// <param name="execParamUserId"></param>
        /// <param name="targyszoErtekek">Tárgyszó azonosító - tárgyszó érték párok</param>
        private void SetObjektumTargyszavak(Guid objId, string objTableName, ExecParam execParamUserId, params KeyValuePair<string, string>[] targyszoErtekek)
        {
            #region Objektum tárgyszavak lekérése

            var serviceObjTargyszavai = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();

            EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

            Result resultGetAll = serviceObjTargyszavai.GetAllMetaByObjMetaDefinicio(execParamUserId, search, objId.ToString(), null
                , objTableName, null, null, false
                , null, false, true);  // KodTarak.OBJMETADEFINICIO_TIPUS.B1

            if (!string.IsNullOrEmpty(resultGetAll.ErrorCode))
            {
                AddMessage(resultGetAll);
                throw new ResultException(resultGetAll);
            }

            Dictionary<string, DataRow> objTargyszavakDict = new Dictionary<string, DataRow>();

            foreach (DataRow row in resultGetAll.Ds.Tables[0].Rows)
            {
                string belsoAzonosito = row["BelsoAzonosito"].ToString();
                objTargyszavakDict[belsoAzonosito] = row;
            }

            #endregion

            #region EREC_ObjektumTargyszavai objektumok feltöltése

            List<EREC_ObjektumTargyszavai> modositandoObjTargyszavak = new List<EREC_ObjektumTargyszavai>();

            foreach (KeyValuePair<string, string> modositandoTargyszo in targyszoErtekek)
            {
                // Ha a lekérdezés nem adott vissza ilyen tárgyszót, akkor hiba:
                if (!objTargyszavakDict.ContainsKey(modositandoTargyszo.Key))
                {
                    AddMessage("EKF: Hiányzó tárgyszó: " + modositandoTargyszo.Key);
                    throw new ResultException("Hiányzó tárgyszó: " + modositandoTargyszo.Key);
                }

                DataRow rowObjTargyszo = objTargyszavakDict[modositandoTargyszo.Key];

                // Tárgyszavas objektum feltöltése:
                EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
                erec_ObjektumTargyszavai.Updated.SetValueAll(false);
                erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);

                erec_ObjektumTargyszavai.Id = rowObjTargyszo["Id"].ToString();
                erec_ObjektumTargyszavai.Updated.Id = true;

                erec_ObjektumTargyszavai.Obj_Metaadatai_Id = rowObjTargyszo["Obj_Metaadatai_Id"].ToString();
                erec_ObjektumTargyszavai.Updated.Obj_Metaadatai_Id = true;

                erec_ObjektumTargyszavai.Targyszo_Id = rowObjTargyszo["Targyszo_Id"].ToString();
                erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;

                erec_ObjektumTargyszavai.Targyszo = rowObjTargyszo["Targyszo"].ToString();
                erec_ObjektumTargyszavai.Updated.Targyszo = true;

                erec_ObjektumTargyszavai.Ertek = modositandoTargyszo.Value;
                erec_ObjektumTargyszavai.Updated.Ertek = true;

                modositandoObjTargyszavak.Add(erec_ObjektumTargyszavai);
            }

            #endregion

            #region Objektum tárgyszó insert/update

            // Insert/update webservice hívása:
            var resultInsertUpdate = serviceObjTargyszavai.InsertOrUpdateValuesByObjMetaDefinicio(
                            execParamUserId
                            , modositandoObjTargyszavak.ToArray()
                            , objId.ToString()
                            , null
                            , objTableName, "*", null, false, null, false, false);

            if (!string.IsNullOrEmpty(resultInsertUpdate.ErrorCode))
            {
                AddMessage(resultInsertUpdate);
                throw new ResultException(resultInsertUpdate);
            }

            #endregion
        }
    }
}
