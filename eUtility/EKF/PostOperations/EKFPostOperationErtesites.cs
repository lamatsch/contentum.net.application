﻿using Contentum.eBusinessDocuments;
using System;
using System.Data;
using System.Text;

namespace Contentum.eUtility.EKF
{
    public class EKFPostOperationErtesites : EKFBaseOperation
    {
        public EKFPostOperationErtesites()
        {
            OperationName = EnumEKFOperationName.ERTESITES;
            TypeFullName = this.GetType().FullName;
            OperationType = EnumEKFOperationType.POST;
        }

        public void InitParameters()
        {
            foreach (Enum_ParameterNames_Post_Ertesites name in Enum.GetValues(typeof(Enum_ParameterNames_Post_Ertesites)))
            {
                ParameterProperty.Parameters.Add(new EKFOperationParameterValue()
                {
                    IdProperty = name.ToString(),
                    NameProperty = name.ToString(),
                });
            }
        }

        public override string ToString()
        {
            return OperationName.ToString();
        }

        public override bool Execute(EKFRuleClass rule, DataSet inputData, out Result result)
        {
            result = null;
            AddMessage("EKF: Ertesites indul");

            StringBuilder body = new StringBuilder();
            ExecParam xparamMail = rule.GetExecParam();

            string cimzettString = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_Ertesites.Email_Cimzettek.ToString());
            if (string.IsNullOrEmpty(cimzettString))
            {
                AddMessage("EKF: Cimzett parameter nincs megadva !");
                throw new ResultException("Cimzett parameter nincs megadva !");
            }

            if (!cimzettString.Contains("@"))
            {
                AddMessage("EKF: Érvénytelen email cím(ek) !");
                throw new ResultException("Érvénytelen email cím(ek) !");
            }

            string[] cimzettek = cimzettString.Split(new char[] { ',', ';', '|', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string targyTemp = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_Ertesites.Email_Targy.ToString());
            StringBuilder targy = new StringBuilder(targyTemp);

            body.Append(EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_Ertesites.Email_Szovege.ToString()));

            EREC_eBeadvanyok beadvany = new EKFManager(xparamMail).GetEBeadvanyByDataSet(inputData);
            if (beadvany == null)
                return false;

            EREC_IraIratok irat = null;
            EREC_KuldKuldemenyek kuldemeny = null;
            if (!string.IsNullOrEmpty(beadvany.IraIrat_Id))
            {
                irat = new EKFManager(xparamMail).GetIrat(rule, beadvany.IraIrat_Id);
                if (irat == null)
                    return false;
            }

            string kuldoRendszerNev = GetKuldoRendszer(rule, inputData);

            if (!string.IsNullOrEmpty(beadvany.KuldKuldemeny_Id))
            {
                kuldemeny = new EKFManager(xparamMail).GetKuldemeny(rule, beadvany.KuldKuldemeny_Id);
                if (kuldemeny == null)
                    return false;
            }

            eAdmin.Service.EmailService emailService = eAdminService.ServiceFactory.GetEmailService();
            string messageFrom = Rendszerparameterek.Get(xparamMail, Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM);

            FillBodyTemplate(ref body, rule, irat, kuldemeny, inputData, kuldoRendszerNev);

            FillSubjectTemplate(ref targy, irat, kuldemeny, inputData, kuldoRendszerNev);

            bool ret = emailService.SendEmail(xparamMail, messageFrom, cimzettek, targy.ToString(), null, null, true, body.ToString());

            if (!ret)
            {
                AddMessage("EKF: Sikertelen email küldés !");
                throw new Exception("Sikertelen email küldés !");
            }
            AddMessage("EKF: Ertesites vege");
            return true;
        }

        private static string GetKuldoRendszer(EKFRuleClass rule, DataSet inputData)
        {
            #region KULDO RENDSZER

            string kuldoRendszer = EKFManager.GetDataSetValue(inputData, "KuldoRendszer");
            string kuldoRendszerNev = string.Empty;
            if (!string.IsNullOrEmpty(kuldoRendszer))
            {
                kuldoRendszerNev = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.KULD_KEZB_MODJA.KCS, kuldoRendszer, rule.GetExecParam(), System.Web.HttpContext.Current.Cache);
                if (string.IsNullOrEmpty(kuldoRendszerNev))
                    kuldoRendszerNev = kuldoRendszer;
            }

            #endregion KULDO RENDSZER

            return kuldoRendszerNev;
        }

        /// <summary>
        /// GetKuldemenyContentumPageLink
        /// </summary>
        /// <param name="kuldemeny"></param>
        /// <returns></returns>
        private string GetKuldemenyContentumPageLink(EREC_KuldKuldemenyek kuldemeny)
        {
            if (kuldemeny == null)
                return string.Empty;

            string eRecordWsUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(EKFConstants.AppKey_eRecordBusinessServiceUrl);

            if (string.IsNullOrEmpty(eRecordWsUrl))
                return string.Empty;

            return string.Format("{0}{1}{2}", eRecordWsUrl.Replace(EKFConstants.eRecordWebServiceSiteName, EKFConstants.eRecordWebSiteName), EKFConstants.ContentumKuldemenyFormPageLink, kuldemeny.Id);
        }

        /// <summary>
        /// GetIratContentumPageLink
        /// </summary>
        /// <param name="irat"></param>
        /// <returns></returns>
        private string GetIratContentumPageLink(EREC_IraIratok irat)
        {
            if (irat == null)
                return string.Empty;

            string eRecordWsUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(EKFConstants.AppKey_eRecordBusinessServiceUrl);

            if (string.IsNullOrEmpty(eRecordWsUrl))
                return string.Empty;
            return string.Format("{0}{1}{2}", eRecordWsUrl.Replace(EKFConstants.eRecordWebServiceSiteName, EKFConstants.eRecordWebSiteName), EKFConstants.ContentumIratPageFormLink, irat.Id);
        }

        /// <summary>
        /// FillBodyTemplate
        /// </summary>
        /// <param name="body"></param>
        /// <param name="rule"></param>
        /// <param name="irat"></param>
        /// <param name="kuldemeny"></param>
        /// <param name="inputData"></param>
        /// <param name="kuldoRendszerNev"></param>
        private void FillBodyTemplate(ref StringBuilder body, EKFRuleClass rule, EREC_IraIratok irat, EREC_KuldKuldemenyek kuldemeny, DataSet inputData, string kuldoRendszerNev)
        {
            if (string.IsNullOrEmpty(body.ToString()))
            {
                if (rule.Operation.TypeFullName.Contains(new EKFMainOperationErkeztetes().GetType().FullName))
                {
                    body.Append(string.Format("{1}{0}{0} KuldoRendszer:{2}{0} PartnerNev:{3}{0} PR_ErkeztetesiSzam:{4}{0} Azonosito:{5}{0} {0}{6}",
                            "</br>",// Environment.NewLine,
                            "Érkeztetés végrehajtva !",
                            kuldoRendszerNev,
                            EKFManager.GetDataSetValue(inputData, "PartnerNev"),
                            EKFManager.GetDataSetValue(inputData, "PR_ErkeztetesiSzam"),
                            kuldemeny != null ? kuldemeny.Azonosito : string.Empty,
                            GetKuldemenyContentumPageLink(kuldemeny)
                            ));
                }
                else if (rule.Operation.TypeFullName.Contains(new EKFMainOperationIktatas().GetType().FullName))
                {
                    if (irat != null)
                        body.Append(string.Format("{1}{0}{0} KuldoRendszer:{2}{0} PartnerNev:{3}{0} PR_ErkeztetesiSzam:{4}{0} Azonosito:{5}{0} {0}{6}",
                            "</br>",// Environment.NewLine,
                            "Iktatás végrehajtva !",
                            kuldoRendszerNev,
                            EKFManager.GetDataSetValue(inputData, "PartnerNev"),
                            EKFManager.GetDataSetValue(inputData, "PR_ErkeztetesiSzam"),
                            irat != null ? irat.Azonosito : string.Empty,
                            GetIratContentumPageLink(irat)));
                    else
                        body.Append(string.Format("{1}{0}{0} KuldoRendszer:{2}{0} PartnerNev:{3}{0} PR_ErkeztetesiSzam:{4}{0} Azonosito:{5}{0} {0}{6}",
                            "</br>",// Environment.NewLine,
                            "Iktatás végrehajtva !",
                            kuldoRendszerNev,
                            EKFManager.GetDataSetValue(inputData, "PartnerNev"),
                            EKFManager.GetDataSetValue(inputData, "PR_ErkeztetesiSzam"),
                            kuldemeny != null ? kuldemeny.Azonosito : string.Empty,
                            GetKuldemenyContentumPageLink(kuldemeny)
                            ));
                }
                else if (rule.Operation.TypeFullName.Contains(new EKFOperationIktatasFoSzammal().GetType().FullName))
                {
                    if (irat != null)
                        body.Append(string.Format("{1}{0}{0} KuldoRendszer:{2}{0} PartnerNev:{3}{0} PR_ErkeztetesiSzam:{4}{0} Azonosito:{5}{0} {0}{6}",
                             "</br>",// Environment.NewLine,
                            "Iktatás végrehajtva !",
                            kuldoRendszerNev,
                            EKFManager.GetDataSetValue(inputData, "PartnerNev"),
                            EKFManager.GetDataSetValue(inputData, "PR_ErkeztetesiSzam"),
                            irat != null ? irat.Azonosito : string.Empty,
                            GetIratContentumPageLink(irat)
                             ));
                    else
                        body.Append(string.Format("{1}{0}{0} KuldoRendszer:{2}{0} PartnerNev:{3}{0} PR_ErkeztetesiSzam:{4}{0} Azonosito:{5}{0} {0}{6}",
                            "</br>",// Environment.NewLine,
                            "Iktatás végrehajtva !",
                            kuldoRendszerNev,
                            EKFManager.GetDataSetValue(inputData, "PartnerNev"),
                            EKFManager.GetDataSetValue(inputData, "PR_ErkeztetesiSzam"),
                            kuldemeny != null ? kuldemeny.Azonosito : string.Empty,
                            GetKuldemenyContentumPageLink(kuldemeny)
                            ));
                }
                else
                {
                    body.Append(string.Format("{1}{0}{0} KuldoRendszer:{2}{0} PartnerNev:{3}{0} PR_ErkeztetesiSzam:{4}{0} ",
                        "</br>",// Environment.NewLine,
                        "Művelet végrehajtva !",
                        kuldoRendszerNev,
                        EKFManager.GetDataSetValue(inputData, "PartnerNev"),
                        EKFManager.GetDataSetValue(inputData, "PR_ErkeztetesiSzam")
                        ));
                }
            }
            else
            {
                FillTemplateVariables(ref body, irat, kuldemeny, inputData, kuldoRendszerNev);
            }

            body = body.Replace(Environment.NewLine, "</br>");
        }

        /// <summary>
        /// FillSubjectTemplate
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="irat"></param>
        /// <param name="kuldemeny"></param>
        /// <param name="inputData"></param>
        /// <param name="kuldoRendszerNev"></param>
        private void FillSubjectTemplate(ref StringBuilder subject, EREC_IraIratok irat, EREC_KuldKuldemenyek kuldemeny, DataSet inputData, string kuldoRendszerNev)
        {
            if (subject == null || string.IsNullOrEmpty(subject.ToString()))
                return;

            FillTemplateVariables(ref subject, irat, kuldemeny, inputData, kuldoRendszerNev);
        }

        /// <summary>
        /// FillTemplateVariables
        /// </summary>
        /// <param name="input"></param>
        /// <param name="irat"></param>
        /// <param name="kuldemeny"></param>
        /// <param name="inputData"></param>
        /// <param name="kuldoRendszerNev"></param>
        private void FillTemplateVariables(ref StringBuilder input, EREC_IraIratok irat, EREC_KuldKuldemenyek kuldemeny, DataSet inputData, string kuldoRendszerNev)
        {
            if (input == null || string.IsNullOrEmpty(input.ToString()))
                return;

            input = input.Replace("$KR_HIVATKOZASISZAM$", EKFManager.GetDataSetValue(inputData, "KR_HivatkozasiSzam"));
            input = input.Replace("$KR_ERKEZTETESISZAM$", EKFManager.GetDataSetValue(inputData, "KR_ErkeztetesiSzam"));
            input = input.Replace("$KULDORENDSZER$", kuldoRendszerNev);
            input = input.Replace("$PARTNERNEV$", EKFManager.GetDataSetValue(inputData, "PartnerNev"));
            input = input.Replace("$PARTNEREMAIL$", EKFManager.GetDataSetValue(inputData, "PartnerEmail"));
            input = input.Replace("$PR_ERKEZTETESISZAM$", EKFManager.GetDataSetValue(inputData, "PR_ErkeztetesiSzam"));

            if (irat != null)
                input = input.Replace("$IRAT_ID$", irat.Id);
            if (kuldemeny != null)
                input = input.Replace("$KULDEMENY_ID$", kuldemeny.Id);

            if (irat != null)
            {
                input = input.Replace("$AZONOSITO$", irat.Azonosito);
                input = input.Replace("$TARGY$", irat.Targy);
            }
            else if (kuldemeny != null)
            {
                input = input.Replace("$AZONOSITO$", kuldemeny.Azonosito);
                input = input.Replace("$TARGY$", kuldemeny.Targy);
            }
        }
    }
}