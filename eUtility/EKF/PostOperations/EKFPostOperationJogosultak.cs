﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Data;

namespace Contentum.eUtility.EKF
{
    public class EKFPostOperationJogosultak : EKFBaseOperation
    {
        public EKFPostOperationJogosultak()
        {
            OperationName = EnumEKFOperationName.JOGOSULTAK;
            TypeFullName = this.GetType().FullName;
            OperationType = EnumEKFOperationType.POST;
        }
        public void InitParameters()
        {
            foreach (Enum_ParameterNames_Post_Jogosultak name in Enum.GetValues(typeof(Enum_ParameterNames_Post_Jogosultak)))
            {
                ParameterProperty.Parameters.Add(new EKFOperationParameterValue()
                {
                    IdProperty = name.ToString(),
                    NameProperty = name.ToString(),
                });
            }
        }
        public override string ToString()
        {
            return OperationName.ToString();
        }
        public override bool Execute(EKFRuleClass rule, DataSet inputData, out Result result)
        {
            result = null;
            ExecParam xparamIr = rule.GetExecParam();

            AddMessage("EKF: Kuldemeny jogosultak felvetele indul");

            EREC_eBeadvanyok resultEBeadvany = new EKFManager(xparamIr).GetEBeadvanyByDataSet(inputData);
            if (resultEBeadvany == null)
                return false;

            string iratId = resultEBeadvany.IraIrat_Id;
            string KuldemenyId = resultEBeadvany.KuldKuldemeny_Id;

            try
            {
                if (!string.IsNullOrEmpty(KuldemenyId))
                {
                    EREC_KuldKuldemenyek kuldemeny = new EKFManager(xparamIr).GetKuldemeny(rule, KuldemenyId);
                    if (kuldemeny == null)
                        return false;

                    RightsService service = eAdminService.ServiceFactory.getRightsService();
                    ExecParam execParam = rule.GetExecParam();

                    if (kuldemeny != null && ParameterProperty != null && ParameterProperty.Parameters != null && ParameterProperty.Parameters.Count > 0)
                    {
                        foreach (var item in ParameterProperty.Parameters)
                        {
                            if (item.IdProperty == Enum_ParameterNames_Post_Jogosultak.Jogosultak.ToString())
                            {
                                string[] paramA = item.ValueProperty.Split('|');
                                if (paramA != null)
                                {
                                    foreach (var itemA in paramA)
                                    {
                                        string[] paramB = itemA.Split('#');
                                        if (paramB.Length == 2)
                                        {
                                            #region CHECK EXIST
                                            if (IsJogosultAlreadyExist(execParam, kuldemeny.Id, paramB[1], "I"))
                                            {
                                                AddMessage(string.Format("Jogosultsag mar letezik: Kuldemeny {0} {1}", kuldemeny.Id, paramB[1]));
                                                continue;
                                            }
                                            #endregion

                                            #region KULDEMENY JOGOSULTAK
                                            result = service.AddCsoportToJogtargyWithJogszint(execParam, kuldemeny.Id, paramB[1], 'I', 'I', '0');
                                            if (!string.IsNullOrEmpty(result.ErrorCode))
                                            {
                                                AddMessage(result);
                                                throw new ResultException(result);
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                        AddMessage("EKF: Jogosultak felvéve !");
                    }
                    else
                    {
                        AddMessage("EKF: Nincsenek megadva jogosultak !");
                    }
                }
            }
            catch (Exception exc)
            {
                AddMessage("EKF: " + exc.Message);
                throw;
            }

            try
            {
                if (!string.IsNullOrEmpty(iratId))
                {
                    AddMessage("EKF: Irat, ugyirat jogosultak felvetele");

                    EREC_IraIratok irat = new EKFManager(xparamIr).GetIrat(rule, iratId);
                    if (irat == null)
                        return false;

                    RightsService service = eAdminService.ServiceFactory.getRightsService();
                    ExecParam execParam = rule.GetExecParam();

                    if (irat != null && ParameterProperty != null && ParameterProperty.Parameters != null && ParameterProperty.Parameters.Count > 0)
                    {
                        foreach (var item in ParameterProperty.Parameters)
                        {
                            if (item.IdProperty == Enum_ParameterNames_Post_Jogosultak.Jogosultak.ToString())
                            {
                                string[] paramA = item.ValueProperty.Split('|');
                                if (paramA != null)
                                {
                                    foreach (var itemA in paramA)
                                    {
                                        string[] paramB = itemA.Split('#');
                                        if (paramB.Length == 2)
                                        {
                                            #region CHECK EXIST
                                            if (!IsJogosultAlreadyExist(execParam, irat.Id, paramB[1], "I"))
                                            {
                                                #region IRAT JOGOSULTAK
                                                result = service.AddCsoportToJogtargyWithJogszint(execParam, irat.Id, paramB[1], 'I', 'I', '0');
                                                if (!string.IsNullOrEmpty(result.ErrorCode))
                                                {
                                                    AddMessage(result);
                                                    throw new ResultException(result);
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                AddMessage(string.Format("Jogosultsag mar letezik: Irat {0} {1}", irat.Id, paramB[1]));
                                            }
                                            #endregion

                                            if (!string.IsNullOrEmpty(irat.Ugyirat_Id))
                                            {
                                                #region CHECK EXIST
                                                if (!IsJogosultAlreadyExist(execParam, irat.Ugyirat_Id, paramB[1], "I"))
                                                {
                                                    #region UGY JOGOSULTAK
                                                    result = service.AddCsoportToJogtargyWithJogszint(execParam, irat.Ugyirat_Id, paramB[1], 'I', 'I', '0');
                                                    if (!string.IsNullOrEmpty(result.ErrorCode))
                                                    {
                                                        AddMessage(result);
                                                        throw new ResultException(result);
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    AddMessage(string.Format("Jogosultsag mar letezik: Ugyirat {0} {1}", irat.Ugyirat_Id, paramB[1]));
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        AddMessage("EKF: Jogosultak felvéve!");
                    }
                    else
                    {
                        AddMessage("EKF: Nincsenek megadva jogosultak !");
                    }
                }
            }
            catch (Exception exc)
            {
                AddMessage("EKF: " + exc.Message);
                throw;
            }

            AddMessage("EKF: Kuldemeny jogosultak felvetele vege");
            return true;
        }
        /// <summary>
        /// IsJogosultAlreadyExist
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="jogTargyId"></param>
        /// <param name="jogAlanyCsoportId"></param>
        /// <param name="jogSzint"></param>
        /// <returns></returns>
        public bool IsJogosultAlreadyExist(ExecParam execParam, string jogTargyId, string jogAlanyCsoportId, string jogSzint)
        {
            KRT_JogosultakService service = eAdminService.ServiceFactory.GetKRT_JogosultakService();
            KRT_JogosultakSearch src = new KRT_JogosultakSearch();

            src.Obj_Id.Value = jogTargyId;
            src.Obj_Id.Operator = Query.Operators.equals;

            src.Csoport_Id_Jogalany.Value = jogAlanyCsoportId;
            src.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            src.Tipus.Value = jogSzint;
            src.Tipus.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, src);
            result.CheckError();

            return result.Ds.Tables[0].Rows.Count > 0 ? true : false;
        }
        /// <summary>
        /// Add irat jogosult
        /// </summary>
        /// <param name="irat"></param>
        /// <param name="execParam"></param>
        /// <param name="alanyId"></param>
        public void AddIratJogosult(ExecParam execParam, EREC_IraIratok irat, string alanyId)
        {
            if (irat == null)
                return;

            RightsService service = eAdminService.ServiceFactory.getRightsService();

            #region CHECK EXIST
            if (!IsJogosultAlreadyExist(execParam, irat.Id, alanyId, "I"))
            {
                #region IRAT JOGOSULTAK
                Result result = service.AddCsoportToJogtargyWithJogszint(execParam, irat.Id, alanyId, 'I', 'I', '0');
                if (!string.IsNullOrEmpty(result.ErrorCode))
                {
                    AddMessage(result);
                    throw new ResultException(result);
                }
                #endregion
            }
            //else
            //{
            //    AddMessage(string.Format("Jogosultsag mar letezik: Irat {0} {1}", irat.Id, alanyId));
            //}
            #endregion
        }
        /// <summary>
        /// Add ugy jogosult
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ugy"></param>
        /// <param name="alanyId"></param>
        public void AddUgyJogosult(ExecParam execParam, EREC_UgyUgyiratok ugy, string alanyId)
        {
            if (ugy == null)
                return;

            RightsService service = eAdminService.ServiceFactory.getRightsService();

            #region CHECK EXIST
            if (!IsJogosultAlreadyExist(execParam, ugy.Id, alanyId, "I"))
            {
                #region IRAT JOGOSULTAK
                Result result = service.AddCsoportToJogtargyWithJogszint(execParam, ugy.Id, alanyId, 'I', 'I', '0');
                if (!string.IsNullOrEmpty(result.ErrorCode))
                {
                    AddMessage(result);
                    throw new ResultException(result);
                }
                #endregion
            }
            //else
            //{
            //    AddMessage(string.Format("Jogosultsag mar letezik: Irat {0} {1}", irat.Id, alanyId));
            //}
            #endregion
        }
    }
}