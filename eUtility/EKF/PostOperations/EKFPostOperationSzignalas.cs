﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Data;

namespace Contentum.eUtility.EKF
{
    public class EKFPostOperationSzignalas : EKFBaseOperation
    {
        public EKFPostOperationSzignalas()
        {
            OperationName = EnumEKFOperationName.SZIGNALAS;
            TypeFullName = this.GetType().FullName;
            OperationType = EnumEKFOperationType.POST;
        }
        public void InitParameters()
        {
            foreach (Enum_ParameterNames_Post_Szignalas name in Enum.GetValues(typeof(Enum_ParameterNames_Post_Szignalas)))
            {
                ParameterProperty.Parameters.Add(new EKFOperationParameterValue()
                {
                    IdProperty = name.ToString(),
                    NameProperty = name.ToString(),
                });
            }
        }
        public override string ToString()
        {
            return OperationName.ToString();
        }
        public override bool Execute(EKFRuleClass rule, DataSet inputData, out Result result)
        {
            result = null;
            AddMessage("EKF: Szignalas indul");
            ExecParam xparamIr = rule.GetExecParam();

            var resultEBeadvany = new EKFManager(xparamIr).GetEBeadvanyByDataSet(inputData);
            if (resultEBeadvany == null)
                return false;

            if (rule.Operation.TypeFullName.Contains(new EKFMainOperationErkeztetes().GetType().FullName))
            {
                AddMessage("EKF: Szignalas kuldemenyre");
                KuldemenySzignalas(resultEBeadvany,rule);
            }
            else
            {
                AddMessage("EKF: Szignalas iratra");
                IratSzignalas(resultEBeadvany, rule);
            }
            AddMessage("EKF: Szignalas vege");
            return true;
        }

        private bool IratSzignalas(EREC_eBeadvanyok resultEBeadvany, EKFRuleClass rule)
        {
            ExecParam xparamIr = rule.GetExecParam();
            string iratId = resultEBeadvany.IraIrat_Id;

            if (string.IsNullOrEmpty(iratId))
            {
                AddMessage("EKF: Irat nem található !");
                throw new Exception("Irat nem található !");
            }

            EREC_IraIratok irat = new EKFManager(xparamIr).GetIrat(rule, iratId);
            if (irat == null)
            {
                AddMessage("EKF: Irat nem található !");
                throw new Exception("Irat nem található !");
            }

            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            feladatok.Obj_Id = irat.Ugyirat_Id;
            feladatok.Obj_type = Constants.TableNames.EREC_UgyUgyiratok;
            //feladatok.Altipus = KodTarak.FELADAT_ALTIPUS.Megjegyzes;
            feladatok.Tipus = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_Szignalas.Feladat_Megjegyzes_Tipus.ToString());
            feladatok.Leiras = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_Szignalas.Feladat_Szoveges_Kifejtese.ToString());
            string szervezetre = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_Szignalas.Szervezet.ToString());
            string ugyintezore = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_Szignalas.Szervezet_Ugyintezo.ToString());

            EREC_UgyUgyiratokService ugyiratService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            if (string.IsNullOrEmpty(szervezetre) && string.IsNullOrEmpty(ugyintezore))
            {
                AddMessage("Szignalas parameter hiba !");
                throw new Exception("Szignalas parameter hiba !");
            }
            if (!string.IsNullOrEmpty(ugyintezore))
            {
                AddMessage("EKF: Szignalas ugyintezore !");
                Result resultSzignalUgyintezore = ugyiratService.Szignalas(
                      xparamIr,
                      irat.Ugyirat_Id,
                      ugyintezore,
                      feladatok);
                if (resultSzignalUgyintezore.IsError)
                {
                    AddMessage(resultSzignalUgyintezore);
                    throw new ResultException(resultSzignalUgyintezore);
                }
                //EKFPostOperationJogosultak jogosultak = new EKFPostOperationJogosultak();
                //jogosultak.AddIratJogosult(xparamIr, irat, ugyintezore);
            }
            else if (!string.IsNullOrEmpty(szervezetre))
            {
                AddMessage("EKF: Szignalas szervezetre !");
                Result resultSzignalSzervezetre = ugyiratService.SzignalasSzervezetre(
                    xparamIr,
                    irat.Ugyirat_Id,
                    szervezetre,
                    feladatok);
                if (resultSzignalSzervezetre.IsError)
                {
                    AddMessage(resultSzignalSzervezetre);
                    throw new ResultException(resultSzignalSzervezetre);
                }

                // BUG_8371: szervezetre szignáláskor az Iraton a Felelős szervezet és Vezetője legyen üres 
                var iratService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                xparamIr.Record_Id = irat.Id;

                var resultIrat = iratService.Get(xparamIr);
                if (!resultIrat.IsError)
                {
                    irat = resultIrat.Record as EREC_IraIratok;
                    irat.Csoport_Id_Ugyfelelos = "";
                    irat.Updated.Csoport_Id_Ugyfelelos = true;
                    resultIrat = iratService.Update(xparamIr, irat);
                }

                if (resultIrat.IsError)
                {
                    AddMessage(resultIrat);
                    throw new ResultException(resultIrat);
                }
            }
            return true;
        }

        private bool KuldemenySzignalas(EREC_eBeadvanyok resultEBeadvany, EKFRuleClass rule)
        {
            ExecParam xparamIr = rule.GetExecParam();
            string kuldemenyId = resultEBeadvany.KuldKuldemeny_Id;

            if (string.IsNullOrEmpty(kuldemenyId))
            {
                AddMessage("EKF: Kuldemeny nem található !");
                throw new Exception("Kuldemeny nem található !");
            }

            var kuldemeny = new EKFManager(xparamIr).GetKuldemeny(rule, kuldemenyId);
            if (kuldemeny == null)
            {
                AddMessage("EKF: Kuldemeny nem található !");
                throw new Exception("Kuldemeny nem található !");
            }

            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            feladatok.Obj_Id = kuldemeny.Id;
            feladatok.Obj_type = Constants.TableNames.EREC_KuldKuldemenyek;
            feladatok.Tipus = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_Szignalas.Feladat_Megjegyzes_Tipus.ToString());
            feladatok.Leiras = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_Szignalas.Feladat_Szoveges_Kifejtese.ToString());

            string szervezetre = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_Szignalas.Szervezet.ToString());
            string ugyintezore = EKFManager.GetEKFOperationParameterValue(ParameterProperty, Enum_ParameterNames_Post_Szignalas.Szervezet_Ugyintezo.ToString());

            var kuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();

            if (string.IsNullOrEmpty(szervezetre) && string.IsNullOrEmpty(ugyintezore))
            {
                AddMessage("Szignalas parameter hiba !");
                throw new Exception("Szignalas parameter hiba !");
            }
            if (!string.IsNullOrEmpty(ugyintezore))
            {
                AddMessage("EKF: Szignalas ugyintezore !");
                Result resultSzignalUgyintezore = kuldemenyekService.SzignalasUgyintezore(
                      xparamIr,
                      kuldemenyId,
                      ugyintezore,
                      feladatok,
                      kuldemeny.FelhasznaloCsoport_Id_Orzo //MOPE: Ide mi kellene?
                      );
                if (resultSzignalUgyintezore.IsError)
                {
                    AddMessage(resultSzignalUgyintezore);
                    throw new ResultException(resultSzignalUgyintezore);
                }
            }
            else if (!string.IsNullOrEmpty(szervezetre))
            {
                AddMessage("EKF: Szignalas szervezetre !");
                Result resultSzignalSzervezetre = kuldemenyekService.SzignalasSzervezetre(
                    xparamIr,
                    kuldemenyId,
                    szervezetre,
                    feladatok);
                if (resultSzignalSzervezetre.IsError)
                {
                    AddMessage(resultSzignalSzervezetre);
                    throw new ResultException(resultSzignalSzervezetre);
                }
            }
            return true;
        }
    }
}