﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility.EKF.Helper;
using Contentum.eUtility.EKF.Models;
using System;
using System.Collections.Generic;
using System.Data;

namespace Contentum.eUtility.EKF
{
    public class EKFPostOperationTargyszavak : EKFBaseOperation
    {
        public EKFPostOperationTargyszavak()
        {
            OperationName = EnumEKFOperationName.TARGYSZAVAK;
            TypeFullName = this.GetType().FullName;
            OperationType = EnumEKFOperationType.POST;
        }
        public void InitParameters()
        {
            foreach (Enum_ParameterNames_Post_Targyszavak name in Enum.GetValues(typeof(Enum_ParameterNames_Post_Targyszavak)))
            {
                ParameterProperty.Parameters.Add(new EKFOperationParameterValue()
                {
                    IdProperty = name.ToString(),
                    NameProperty = name.ToString(),
                });
            }
        }
        public override string ToString()
        {
            return OperationName.ToString();
        }
        public override bool Execute(EKFRuleClass rule, DataSet inputData, out Result result)
        {
            result = null;
            ExecParam xparamIr = rule.GetExecParam();

            AddMessage("EKF: Targyszavak felvetele indul");

            string eBeadvanyId = EKFManager.GetDataSetValue(inputData, "Id");

            EREC_eBeadvanyok resultEBeadvany = new EKFManager(xparamIr).GetEBeadvanyByDataSet(inputData);
            if (resultEBeadvany == null)
                return false;

            string iratId = resultEBeadvany.IraIrat_Id;

            if (string.IsNullOrEmpty(iratId))
            {
                AddMessage("EKF: Irat nem található !");
                throw new Exception("Irat nem található !");
            }

            EREC_IraIratok irat = new EKFManager(xparamIr).GetIrat(rule, iratId);
            if (irat == null)
            {
                AddMessage("EKF: Irat nem található !");
                throw new Exception("Irat nem található !");
            }

            RightsService service = eAdminService.ServiceFactory.getRightsService();
            ExecParam execParam = rule.GetExecParam();

            if (ParameterProperty != null && ParameterProperty.Parameters != null && ParameterProperty.Parameters.Count > 0)
            {
                List<EKF_ObjTargyszavakModel> resultJsonTargyszavak = null;

                foreach (var item in ParameterProperty.Parameters)
                {
                    if (item.IdProperty == Enum_ParameterNames_Post_Targyszavak.TargySzoObjektumLista.ToString())
                    {
                        resultJsonTargyszavak = JSONUtilityFunctions.DeSerializeEKFTargyszavakList(item.ValueProperty);
                    }
                }

                if (resultJsonTargyszavak == null || resultJsonTargyszavak.Count < 1)
                {
                    AddMessage("EKF: Nincsenek targyszavak !");
                    return true;
                }

                string resultTargySzoObjektumTipusKod_Ugy = "EREC_UgyUgyiratok";
                string resultTargySzoObjektumTipusId_Ugy = GetObjTipusId(rule, resultTargySzoObjektumTipusKod_Ugy);

                string resultTargySzoObjektumTipusKod_Irat = "EREC_IraIratok";
                string resultTargySzoObjektumTipusId_Irat = GetObjTipusId(rule, resultTargySzoObjektumTipusKod_Irat);

                EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = null;
                foreach (EKF_ObjTargyszavakModel targySzoSablon in resultJsonTargyszavak)
                {
                    //if (string.IsNullOrEmpty(targySzoSablon.Targyszo_Id))
                    //    continue;

                    #region IRATHOZ
                    erec_ObjektumTargyszavai = Converter_EKF.Convert(targySzoSablon);
                    erec_ObjektumTargyszavai.Obj_Id = irat.Id;
                    erec_ObjektumTargyszavai.Updated.Obj_Id = true;
                    erec_ObjektumTargyszavai.Forras = KodTarak.TARGYSZO_FORRAS.Automatikus;
                    erec_ObjektumTargyszavai.Updated.Forras = true;
                    erec_ObjektumTargyszavai.ObjTip_Id = resultTargySzoObjektumTipusId_Irat;
                    erec_ObjektumTargyszavai.Updated.ObjTip_Id = true;
                    erec_ObjektumTargyszavai.Ertek = targySzoSablon.Ertek;
                    erec_ObjektumTargyszavai.Updated.Ertek = true;

                    string existIrathozId;
                    bool existIrathoz = CheckExits_EREC_ObjektumTargyszavai(rule, erec_ObjektumTargyszavai, out existIrathozId);
                    if (existIrathoz)
                    {
                        EREC_ObjektumTargyszavai itemIrat = Get_EREC_ObjektumTargyszavai(rule, existIrathozId);
                        if (itemIrat != null)
                        {
                            itemIrat.Ertek = erec_ObjektumTargyszavai.Ertek;
                            UpdateEREC_ObjektumTargyszavai(rule, itemIrat);
                        }
                    }
                    else
                    {
                        AddEREC_ObjektumTargyszavai(rule, erec_ObjektumTargyszavai);
                    }
                    #endregion

                    #region UGYIRATHOZ
                    erec_ObjektumTargyszavai = Converter_EKF.Convert(targySzoSablon);
                    erec_ObjektumTargyszavai.Obj_Id = irat.Ugyirat_Id;
                    erec_ObjektumTargyszavai.Updated.Obj_Id = true;
                    erec_ObjektumTargyszavai.Forras = KodTarak.TARGYSZO_FORRAS.Automatikus;
                    erec_ObjektumTargyszavai.Updated.Forras = true;
                    erec_ObjektumTargyszavai.ObjTip_Id = resultTargySzoObjektumTipusId_Ugy;
                    erec_ObjektumTargyszavai.Updated.ObjTip_Id = true;
                    erec_ObjektumTargyszavai.Ertek = targySzoSablon.Ertek;
                    erec_ObjektumTargyszavai.Updated.Ertek = true;

                    List<string> validTargyszoMetaAdataiIds = null;
                    if (HasUgyiratMetaDefTargyszo(rule, erec_ObjektumTargyszavai.Targyszo_Id, out validTargyszoMetaAdataiIds))
                    {
                        for (int i = 0; i < validTargyszoMetaAdataiIds.Count; i++)
                        {
                            erec_ObjektumTargyszavai.Obj_Metaadatai_Id = validTargyszoMetaAdataiIds[i];
                            erec_ObjektumTargyszavai.Updated.Obj_Metaadatai_Id = true;

                            AddEREC_ObjektumTargyszavai(rule, erec_ObjektumTargyszavai);
                        }
                    }
                    else
                    {
                        AddEREC_ObjektumTargyszavai(rule, erec_ObjektumTargyszavai);
                    }

                    #endregion
                }
            }
            AddMessage("EKF: Targyszavak felvetele vege");
            return true;
        }

        #region HELPER
        /// <summary>
        /// Get obj tipus from kod
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="objTipusKod"></param>
        /// <returns></returns>
        public string GetObjTipusId(EKFRuleClass rule, string objTipusKod)
        {
            ExecParam ExecParam = rule.GetExecParam();
            KRT_ObjTipusokSearch search = new KRT_ObjTipusokSearch();
            search.Kod.Value = objTipusKod;
            search.Kod.Operator = eQuery.Query.Operators.equals;
            search.TopRow = 1;
            KRT_ObjTipusokService service = eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
            Result result = service.GetAll(ExecParam, search);

            if (string.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    return result.Ds.Tables[0].Rows[0]["Id"].ToString();
                }
                else
                {
                    return "00000000-0000-0000-0000-000000000000";
                }
            }
            else
                return "00000000-0000-0000-0000-000000000000";
        }
        /// <summary>
        /// Save ObjektumTargyszavai
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="item"></param>
        public void AddEREC_ObjektumTargyszavai(EKFRuleClass rule, EREC_ObjektumTargyszavai item)
        {
            EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
            ExecParam execParam = rule.GetExecParam();

            Result result = service.Insert(execParam, item);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage(result);
                throw new ResultException(result);
            }
        }
        /// <summary>
        /// UpdateEREC_ObjektumTargyszavai
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="item"></param>
        public void UpdateEREC_ObjektumTargyszavai(EKFRuleClass rule, EREC_ObjektumTargyszavai item)
        {
            EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
            ExecParam execParam = rule.GetExecParam();

            Result result = service.Update(execParam, item);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage(result);
                throw new ResultException(result);
            }
        }
        /// <summary>
        /// CheckExits_EREC_ObjektumTargyszavai
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="item"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CheckExits_EREC_ObjektumTargyszavai(EKFRuleClass rule, EREC_ObjektumTargyszavai item, out string id)
        {
            id = null;

            EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
            ExecParam execParam = rule.GetExecParam();

            EREC_ObjektumTargyszavaiSearch src = new EREC_ObjektumTargyszavaiSearch();

            if (!string.IsNullOrEmpty(item.Targyszo_Id))
            {
                src.Targyszo_Id.Value = item.Targyszo_Id;
                src.Targyszo_Id.Operator = Query.Operators.equals;
            }

            if (!string.IsNullOrEmpty(item.Targyszo))
            {
                src.Targyszo.Value = item.Targyszo;
                src.Targyszo.Operator = Query.Operators.equals;
            }

            src.ObjTip_Id.Value = item.ObjTip_Id;
            src.ObjTip_Id.Operator = Query.Operators.equals;

            src.Obj_Id.Value = item.Obj_Id;
            src.Obj_Id.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, src);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage(result);
                throw new ResultException(result);
            }

            if (result.Ds.Tables[0].Rows.Count < 1)
                return false;

            id = result.Ds.Tables[0].Rows[0]["Id"].ToString();
            return true;
        }
        /// <summary>
        /// Get_EREC_ObjektumTargyszavai
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public EREC_ObjektumTargyszavai Get_EREC_ObjektumTargyszavai(EKFRuleClass rule, string id)
        {
            if (string.IsNullOrEmpty(id))
                return null;

            EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
            ExecParam execParam = rule.GetExecParam();
            execParam.Record_Id = id;

            Result result = service.Get(execParam);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage(result);
                throw new ResultException(result);
            }
            return (EREC_ObjektumTargyszavai)result.Record;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="targyszo_id"></param>
        /// <param name="validTargyszoMetDefIds"></param>
        /// <returns></returns>
        private bool HasUgyiratMetaDefTargyszo(EKFRuleClass rule, string targyszo_id, out List<string> validTargyszoMetaAdataiIds)
        {
            validTargyszoMetaAdataiIds = new List<string>();

            if (string.IsNullOrEmpty(targyszo_id))
                return false;

            string objTipId_EREC_UgyUgyiratok = null;
            if (!GetKRT_ObjTipusIdByKod(rule, Constants.TableNames.EREC_UgyUgyiratok, out objTipId_EREC_UgyUgyiratok))
                return false;

            if (string.IsNullOrEmpty(objTipId_EREC_UgyUgyiratok))
                return false;

            List<string> metaDefIds = new List<string>();
            if (!GetEREC_Obj_MetaDefiniciok(rule, objTipId_EREC_UgyUgyiratok, out metaDefIds))
                return false;

            if (!GetEREC_Obj_MetaAdatai(rule, targyszo_id, metaDefIds, out validTargyszoMetaAdataiIds))
                return false;

            return true;
        }
        /// <summary>
        /// GetKRT_ObjTipusIdByKod
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="kod"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool GetKRT_ObjTipusIdByKod(EKFRuleClass rule, string kod, out string id)
        {
            id = null;
            KRT_ObjTipusokService service = eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
            ExecParam execParam = rule.GetExecParam();

            KRT_ObjTipusokSearch src = new KRT_ObjTipusokSearch();
            src.TopRow = 1;
            src.Kod.Value = kod; ////Constants.TableNames.EREC_UgyUgyiratok;
            src.Kod.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, src);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage(result);
                throw new ResultException(result);
            }

            if (result.Ds.Tables[0].Rows.Count < 1)
                return false;

            id = result.Ds.Tables[0].Rows[0]["Id"].ToString();
            return true;
        }

        /// <summary>
        /// GetEREC_Obj_MetaDefiniciok
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="obj_tip_id"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        private bool GetEREC_Obj_MetaDefiniciok(EKFRuleClass rule, string obj_tip_id, out List<string> ids)
        {
            ids = new List<string>();
            EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
            ExecParam execParam = rule.GetExecParam();

            EREC_Obj_MetaDefinicioSearch src = new EREC_Obj_MetaDefinicioSearch();
            src.Objtip_Id.Value = obj_tip_id;
            src.Objtip_Id.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, src);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage(result);
                throw new ResultException(result);
            }

            if (result.Ds.Tables[0].Rows.Count < 1)
                return false;

            for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
            {
                ids.Add(result.Ds.Tables[0].Rows[i]["Id"].ToString());
            }
            return true;
        }

        /// <summary>
        /// GetEREC_Obj_MetaAdatai
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="targyszoId"></param>
        /// <param name="metDefIds"></param>
        /// <param name="validTargyszoMetaAdataiIds"></param>
        /// <returns></returns>
        private bool GetEREC_Obj_MetaAdatai(EKFRuleClass rule, string targyszoId, List<string> metDefIds, out List<string> validTargyszoMetaAdataiIds)
        {
            validTargyszoMetaAdataiIds = new List<string>();
            EREC_Obj_MetaAdataiService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
            ExecParam execParam = rule.GetExecParam();

            EREC_Obj_MetaAdataiSearch src = new EREC_Obj_MetaAdataiSearch();
            src.Targyszavak_Id.Value = targyszoId;
            src.Targyszavak_Id.Operator = Query.Operators.equals;

            src.Obj_MetaDefinicio_Id.Value = Search.GetSqlInnerString(metDefIds.ToArray());
            src.Obj_MetaDefinicio_Id.Operator = Query.Operators.inner;

            Result result = service.GetAll(execParam, src);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                AddMessage(result);
                throw new ResultException(result);
            }

            if (result.Ds.Tables[0].Rows.Count < 1)
                return false;

            for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
            {
                validTargyszoMetaAdataiIds.Add(result.Ds.Tables[0].Rows[i]["Id"].ToString());
            }
            return true;
        }
        #endregion

    }
}