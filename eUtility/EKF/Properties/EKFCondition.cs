﻿namespace Contentum.eUtility.EKF
{
    /// <summary>
    /// Feltétel
    /// </summary>
    public partial class EKFCondition
    {
        public EKFCondition()
        {

        }
        public EKFCondition(string fieldProperty, EnumEKFConditionOperator operatorProperty, string conditionProperty)
        {
            FieldProperty = fieldProperty;
            OperatorProperty = operatorProperty;
            ConditionProperty = conditionProperty;
        }
        public override string ToString()
        {
            if (string.IsNullOrEmpty(FieldProperty))
                return string.Empty;

            return string.Format("{0} {1} {2}", FieldProperty, OperatorProperty.ToString(), ConditionProperty);
        }
        /// <summary>
        /// Vizsgált mező
        /// </summary>
        public string FieldProperty { get; set; }

        /// <summary>
        /// Xml attribútomot kell kiértékelni
        /// </summary>
        public bool IsXmlAttributeProperty { get; set; }

        /// <summary>
        /// Xml elemet kell kiértékelni
        /// </summary>
        public bool IsXmlElementProperty { get; set; }

        /// <summary>
        /// Operátor
        /// </summary>
        public EnumEKFConditionOperator OperatorProperty { get; set; }

        /// <summary>
        /// Feltétel
        /// </summary>
        public string ConditionProperty { get; set; }
    }
}
