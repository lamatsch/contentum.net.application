﻿namespace Contentum.eUtility.EKF
{
    /// <summary>
    /// Feltétel a csatolmány tartalmában
    /// </summary>
    public partial class EKFConditionForXML
    {
        public EKFConditionForXML()
        {
        }
        public EKFConditionForXML(string fileNameProperty, string xPathProperty, EnumEKFConditionOperatorForXML operatorProperty, string conditionProperty)
        {
            FileNameProperty = fileNameProperty;
            XPathProperty = xPathProperty;
            OperatorProperty = operatorProperty;
            ValueProperty = conditionProperty;
        }
        public override string ToString()
        {
            if (string.IsNullOrEmpty(ValueProperty))
                return string.Empty;

            return string.Format("{0} {1} {2} {3}", FileNameProperty, XPathProperty, OperatorProperty.ToString(), ValueProperty);
        }
        /// <summary>
        /// Azonosito
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// SorSzam
        /// </summary>
        public byte SorSzam { get; set; }
        /// <summary>
        /// Vizsgált file
        /// </summary>
        public string FileNameProperty { get; set; }

        /// <summary>
        /// Vizsgált mező
        /// </summary>
        public string XPathProperty { get; set; }

        /// <summary>
        /// Operátor
        /// </summary>
        public EnumEKFConditionOperatorForXML OperatorProperty { get; set; }

        /// <summary>
        /// Feltétel
        /// </summary>
        public string ValueProperty { get; set; }

        /// <summary>
        /// Objektum érvényesen kitöltött ?
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            if (string.IsNullOrEmpty(XPathProperty))
                return false;

            if (string.IsNullOrEmpty(ValueProperty))
                return false;

            return true;
        }
    }
}
