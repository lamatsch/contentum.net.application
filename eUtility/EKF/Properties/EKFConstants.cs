﻿namespace Contentum.eUtility.EKF
{
    /// <summary>
    /// Konstansok
    /// </summary>
    public static class EKFConstants
    {
        public static string[] ConditionFieldSplitSeparator = new string[] { "." };

        public static string ConditionValueSplitSeparator = "|";
        public static string[] ConditionValueSplitSeparatorArray = new string[] { ConditionValueSplitSeparator };

        public static string ConditionValueStarSplitSeparator = "*";
        public static string[] ConditionValueStarSplitSeparatorArray = new string[] { ConditionValueStarSplitSeparator };

        public static string UgyTipusDefault = "001";
        public static string CONST_LIKE_FORMULA_XML_FILES = "%.xml";
        public static string CONST_XML_FILE_EXTENSION = ".xml";
        public static string CONST_XML_FILE_FORMXML = "form.xml";

        public enum EnumDataTableNames
        {
            EREC_eBeadvanyok,
            EREC_eBeadvanyCsatolmanyok
        }

        public enum EnumEKFMunkaallomas
        {
            Automatikus_Erkezteto,
        }
        public enum EnumEKFbekuldo
        {
            Hivatali_kapu,
        }
        #region TARGY

        /// <summary>
        /// EnumTargyForrasTipus
        /// </summary>
        public enum EnumTargyForrasTipus
        {
            FROM_FORM_XML_TAG_IRATTARGYA,
            FROM_DATA,
            FROM_CONSTANT
        }
        /// <summary>
        /// Form xml irat targya xpath
        /// </summary>
        public static string XPATH_FORM_XML_IRATTARGYA = "//*/*[name()='variables']/*[name()='iratTargya']";

        #endregion

        public enum eBeadvanyOszlopok
        {
            Id,
            Irany,
            Allapot,
            KuldoRendszer,
            UzenetTipusa,
            FeladoTipusa,
            PartnerKapcsolatiKod,
            PartnerNev,
            PartnerEmail,
            PartnerRovidNev,
            PartnerMAKKod,
            PartnerKRID,
            Partner_Id,
            Cim_Id,
            KR_HivatkozasiSzam,
            KR_ErkeztetesiSzam,
            Contentum_HivatkozasiSzam,
            PR_HivatkozasiSzam,
            PR_ErkeztetesiSzam,
            KR_DokTipusHivatal,
            KR_DokTipusAzonosito,
            KR_DokTipusLeiras,
            KR_Megjegyzes,
            KR_ErvenyessegiDatum,
            KR_ErkeztetesiDatum,
            KR_FileNev,
            KR_Kezbesitettseg,
            KR_Idopecset,
            KR_Valasztitkositas,
            KR_Valaszutvonal,
            KR_Rendszeruzenet,
            KR_Tarterulet,
            KR_ETertiveveny,
            KR_Lenyomat,
            KuldKuldemeny_Id,
            IraIrat_Id,
            IratPeldany_Id,
            Cel,
            PR_Parameterek,
            Ver,
            Note,
            Stat_id,
            ErvKezd,
            ErvVege,
            Letrehozo_id,
            LetrehozasIdo,
            Modosito_id,
            ModositasIdo,
            Zarolo_id,
            ZarolasIdo,
            Tranz_id,
            UIAccessLog_id,
            KR_Fiok,
        }
        public static string AppKey_eRecordBusinessServiceUrl = "eRecordBusinessServiceUrl";
        public static string eRecordWebServiceSiteName = "eRecordWebService";
        public static string eRecordWebSiteName = "eRecord";

        public static string ContentumKuldemenyFormPageLink = "/KuldKuldemenyekForm.aspx?Command=View&Id=";
        public static string ContentumIratPageFormLink = "/IraIratokForm.aspx?Command=View&Id=";

        public static string IranyBejovo = "0";
        public static string IranyKimeno = "1";
    }
}
