﻿namespace Contentum.eUtility.EKF
{
    /// <summary>
    /// Szabály típusa
    /// </summary>
    public enum EnumEKFRuleType
    {
        ElektronikusBeadvany,
        //Email,
    }
    public enum EnumEKFConditionOperator
    {
        EGYENLO,//==
        NEM_EGYENLO,//!=  <>

        NULL_VAGY_URES,// NULL ""
        KISEBB, //<
        NAGYOBB, //>
        KISEBB_EGYENLO,//=<
        NAGYOBB_EGYENLO,//>=
      
        KEZDODIK, //XYZ*
        VEGZODIK,//*XYZ

        TARTALMAZZA,//XY*Z
        CSILLAGOS, // XA*A*AAAAA
        BENNE_VAN,//IN
        REGULARIS_KIFEJEZES,
    }

    public enum EnumEKFConditionOperatorForXML
    {
        EGYENLO,//==
        NEM_EGYENLO,//!=  <>
        KISEBB, //<
        NAGYOBB, //>
        KISEBB_EGYENLO,//=<
        NAGYOBB_EGYENLO,//>=
    }

    /// <summary>
    /// Művelet típusa
    /// </summary>
    public enum EnumEKFOperationType
    {
        UNKNOWN,
        MAIN,
        POST
    }
    /// <summary>
    /// Művelethez tartozó araméter típusa
    /// </summary>
    public enum EnumEKFOperationParameterType
    {
        UNKNOWN,
        MAIN,
        POST
    }
    public enum EnumEKFOperationName
    {
        ISMERETLEN,
        ERKEZTETES,
        IKTATAS,
        IKTATAS_FOSZAMMAL,
        IKTATAS_ALSZAMRA_HIVATKOZASI_SZAMMAL,
        IRATTARAZAS,
        SZIGNALAS,
        ATADAS,
        AUTO_VALASZ,
        ERTESITES,
        ELINTEZETTE_NYILVANITAS,
        JOGOSULTAK,
        TARGYSZAVAK,
        CELRENDSZER,
    }

    public enum Enum_ParameterNames_Main_Iktatas
    {
        Iktatokonyv,
        IktatoKonyvHely,
        ErkeztetoKonyv,
        ErkeztetoKonyvHely,
        Ugyirat_Ugyintezo,
        Irat_Ugyintezo,
        IratTipus,
        ////Targy,
        Iktatas_Tipus,
       // Ugyirat_Jellege,
        //Irat_Cime,
        SzakrendszerKod,
        //Iratot_Olvashatjak,
        IrattariTelel,
        //Ugyirat_Fajtaja,

        Erkeztetes_Cimzett,
        Erkeztetes_Felelos,

        IntezesiIdo,
        IntezesiIdoegyseg,

        UT_UgyFajta,
        UT_AgazatiJel,
        UT_Ugykor,
        UT_UgyTipus
    }

    public enum Enum_ParameterNames_Main_Iktatas_FoSzammal
    {
        FoSzam,
        //FoSzamEv,
        //Iktatokonyv,
        //IktatoKonyvHely,
        ErkeztetoKonyv,
        //ErkeztetoKonyvHely,
        Ugyirat_Ugyintezo,
        Irat_Ugyintezo,
        IratTipus,
        ////Targy,
        Iktatas_Tipus,
        //Ugyirat_Jellege,
        //Irat_Cime,
        SzakrendszerKod,
        //Iratot_Olvashatjak,
        IrattariTelel,
        //Ugyirat_Fajtaja,
        Erkeztetes_Cimzett,
        Erkeztetes_Felelos,

        //IntezesiIdo,
        //IntezesiIdoegyseg,

        UT_UgyFajta,
        UT_AgazatiJel,
        UT_Ugykor,
        UT_UgyTipus
    }

    public enum Enum_ParameterNames_Main_Erkeztetes
    {
        ErkeztetoHely,
        ErkeztetoKonyv,
        ErkeztetoKonyvMegkulonboztetoJel,
        Cimzett,
        Felelos,
        KezbesitesModja
    }

    public enum Enum_ParameterNames_Main_Irattaroz
    {
        ElintezetteNyilvanitas
       // Irattari_Tetelszam,
    }


    public enum Enum_ParameterNames_Post_ElintezetteNyilvanitas
    {
        FT1,
        FT2,
        FT3,
        Irattari_Tetelszam,
    }

    public enum Enum_ParameterNames_Post_Atadas
    {
        Szervezet_Ugyintezo,
        Feladat_Megjegyzes_Tipus,
        Kezelesi_Utasitas,
        Feladat_Szoveges_Kifejtese,
        HKP_DokTipusHivatal,
        HKP_DokTipusAzonosito,
        IrattariTetel,
    }

    public enum Enum_ParameterNames_Post_AutoValasz
    {
        Riport_Szerver_Url,
        Valasz_Uzenet_Riport_Sablon,
        Elektronikus_Alairas_Kell,
        Iktatni_Kell,
        Irattarozni_Kell,
        //Email_Kuldo,
        //Email_CC,
        //Email_BCC,
        //Email_Targy,
        //Valasz_Uzenet_Sablon_Expedialas_Modja,
    }

    public enum Enum_ParameterNames_Post_Ertesites
    {
        Email_Cimzettek,
        Email_Targy,
        Email_Szovege,
    }

    public enum Enum_ParameterNames_Post_Szignalas
    {
        Szervezet,
        Szervezet_Ugyintezo,
        Feladat_Megjegyzes_Tipus,
        Kezelesi_Utasitas,
        Feladat_Szoveges_Kifejtese
    }

    public enum Enum_Iktatas_Tipus
    {
        NINCS,
        Foszamra,
        Alszamra,
    }
    public enum Enum_Erkeztetes_Tipus
    {
        NINCS,
        VAN
    }
    public enum Enum_ParameterNames_Post_Jogosultak
    {
        Jogosultak,
    }
    public enum Enum_ParameterNames_Post_Targyszavak
    {
        TargySzoObjektumTipusKod,
        TargySzoObjektumLista,
    }

    public enum Enum_ParameterNames_Post_CelRendszer
    {
        CelRendszerErtek,
    }
}
