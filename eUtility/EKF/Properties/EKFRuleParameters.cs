﻿using System;

namespace Contentum.eUtility.EKF
{
    /// <summary>
    /// Szabály paraméterei
    /// </summary>
    public partial class EKFRuleParameters
    {
        public EKFRuleParameters()
        {
            NameProperty = "NA";
            DescriptionProperty = string.Empty;
            StopRuleProperty = false;
            TypeProperty = EnumEKFRuleType.ElektronikusBeadvany;
        }
        public EKFRuleParameters( string nameProperty, string descriptionProperty,  bool? stopRuleProperty, EnumEKFRuleType typeProperty, Guid userIdProperty, Guid userGroupProperty)
        {
            NameProperty = nameProperty;
            DescriptionProperty = descriptionProperty;
            StopRuleProperty = stopRuleProperty;
            TypeProperty = typeProperty;
            UserIdProperty = userIdProperty;
            UserGroupProperty = userGroupProperty;
        }

        /// <summary>
        /// Elnevezés
        /// </summary>
        public string NameProperty { get; set; }

        /// <summary>
        /// Leírás
        /// </summary>
        public string DescriptionProperty { get; set; }

        ///// <summary>
        ///// Lefutási sorrend
        ///// </summary>
        //public int OrderProperty { get; set; }

        /// <summary>
        ///  A szabály teljesülése leállítja-e a sorrendben hátrébb lévő szabályokra való ellenőrzést
        /// </summary>
        public bool? StopRuleProperty { get; set; }

        /// <summary>
        /// Üzenet típusa
        /// </summary>
        public EnumEKFRuleType TypeProperty { get; set; }

        /// <summary>
        /// Felhasznalo
        /// </summary>
        public Guid UserIdProperty { get; set; }

        /// <summary>
        /// Felhasznalo szervezeti egysege
        /// </summary>
        public Guid UserGroupProperty { get; set; }
    }
}
