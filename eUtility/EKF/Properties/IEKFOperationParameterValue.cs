﻿using System.Collections.Generic;

namespace Contentum.eUtility.EKF
{
    /// <summary>
    /// Művelet paraméter értékei
    /// </summary>
    public partial class EKFOperationParameterValue
    {
        /// <summary>
        /// Azonosító
        /// </summary>
        public string IdProperty { get; set; }

        /// <summary>
        /// Elnevezés
        /// </summary>
        public string NameProperty { get; set; }

        /// <summary>
        /// Érték
        /// </summary>
        public string ValueProperty { get; set; }

        public override bool Equals(object obj)
        {
            var value = obj as EKFOperationParameterValue;
            return value != null &&
                   IdProperty == value.IdProperty;
        }

        public override int GetHashCode()
        {
            return 197972145 + EqualityComparer<string>.Default.GetHashCode(IdProperty);
        }
        public EKFOperationParameterValue()
        {
            IdProperty = System.Guid.NewGuid().ToString();
        }
    }
}
