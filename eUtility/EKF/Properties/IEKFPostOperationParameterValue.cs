﻿using System.Collections.Generic;

namespace Contentum.eUtility.EKF
{
    /// <summary>
    /// Utó Művelet paraméter értékei
    /// </summary>
    public partial class IEKFPostOperationParameterValue
    {
        public IEKFPostOperationParameterValue()
        {
        }

        public IEKFPostOperationParameterValue(string idProperty, string nameProperty, string descriptionProperty, string valueProperty)
        {
            IdProperty = idProperty;
            NameProperty = nameProperty;
            DescriptionProperty = descriptionProperty;
            ValueProperty = valueProperty;
        }

        /// <summary>
        /// Azonosító
        /// </summary>
        public string IdProperty { get; set; }

        /// <summary>
        /// Elnevezés
        /// </summary>
        public string NameProperty { get; set; }

        /// <summary>
        /// Leírás
        /// </summary>
        public string DescriptionProperty { get; set; }

        /// <summary>
        /// Érték
        /// </summary>
        public string ValueProperty { get; set; }

        public override bool Equals(object obj)
        {
            var value = obj as IEKFPostOperationParameterValue;
            return value != null &&
                   IdProperty == value.IdProperty;
        }

        public override int GetHashCode()
        {
            return 197972145 + EqualityComparer<string>.Default.GetHashCode(IdProperty);
        }
    }
}
