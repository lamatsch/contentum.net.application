﻿using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Contentum.eUtility.EKF
{
    public partial class EKFRuleClass
    {
        public EKFRuleClass()
        {
            ParametersProperty = new EKFRuleParameters();
            Conditions = new List<EKFCondition>();
            Operation = null;
            PostOperations = new List<EKFBaseOperation>();
        }
        public EKFRuleClass(Guid id, EKFRuleParameters parameters, List<EKFCondition> conditions, EKFBaseOperation operation, List<EKFBaseOperation> postOperations)
        {
            IdProperty = id;
            ParametersProperty = parameters;
            Conditions = conditions;

            Operation = operation;
            PostOperations = postOperations;
        }

        public EKFRuleClass(Guid id, int order, EKFRuleParameters parameters, List<EKFCondition> conditions, EKFBaseOperation operation, List<EKFBaseOperation> postOperations)
        {
            IdProperty = id;
            OrderProperty = order;
            ParametersProperty = parameters;
            Conditions = conditions;

            Operation = operation;
            PostOperations = postOperations;
        }
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            if (Conditions != null && Conditions.Count > 0)
            {
                result.Append(string.Format("Feltétel: "));
                bool first = true;
                foreach (EKFCondition item in Conditions.Where(c => !string.IsNullOrEmpty(c.FieldProperty)))
                {
                    if (first)
                    {
                        result.Append(item.ToString());
                        first = false;
                    }
                    else
                    {
                        result.Append(string.Format(" && {0}", item.ToString()));
                    }
                }
            }
            result.Append(Environment.NewLine);

            if (ConditionsForXML != null && ConditionsForXML.Count > 0)
            {
                result.Append(string.Format("Feltétel: "));
                bool first = true;
                foreach (EKFConditionForXML item in ConditionsForXML.Where(c => !string.IsNullOrEmpty(c.XPathProperty)))
                {
                    if (first)
                    {
                        result.Append(item.ToString());
                        first = false;
                    }
                    else
                    {
                        result.Append(string.Format(" && {0}", item.ToString()));
                    }
                }
            }
            result.Append(Environment.NewLine);

            if (Operation != null)
                result.Append(string.Format("Művelet: {0}", Operation.ToString()));
            result.Append(Environment.NewLine);

            if (PostOperations != null)
            {
                result.Append(string.Format("UtóMűvelet: "));
                bool first = true;
                foreach (var item in PostOperations)
                {
                    if (first)
                    {
                        result.Append(item.ToString());
                        first = false;
                    }
                    else
                    {
                        result.Append(string.Format(" -> {0}", item.ToString()));
                    }
                }
            }
            return result.ToString();
        }
        /// <summary>
        /// Azonosító
        /// </summary>
        public string VersionProperty = "1.0.0";

        /// <summary>
        /// Azonosító
        /// </summary>
        public Guid IdProperty { get; set; }

        /// <summary>
        /// Nev
        /// </summary>
        public string NameProperty { get; set; }

        /// <summary>
        /// Sorrend
        /// </summary>
        public int OrderProperty { get; set; }
        ///// <summary>
        ///// TargyProperty
        ///// </summary>
        //[Obsolete]
        //public string TargyProperty { get; set; }
        /// <summary>
        /// Elnevezés
        /// </summary>
        public EKFRuleParameters ParametersProperty { get; set; }

        /// <summary>
        /// Feltételek
        /// </summary>
        public List<EKFCondition> Conditions { get; set; }

        /// <summary>
        /// Feltételek csatolmányban
        /// </summary>
        public List<EKFConditionForXML> ConditionsForXML { get; set; }

        /// <summary>
        /// Művelet(ek)
        /// EKFBaseOperation
        /// </summary>
        public EKFBaseOperation Operation { get; set; }

        /// <summary>
        /// Utó művelet(ek)
        /// </summary>
        public List<EKFBaseOperation> PostOperations { get; set; }

        #region OVERRIDE

        #region TARGY
        /// <summary>
        /// TargyForrasTipus
        /// </summary>
        public string TargyForrasTipus { get; set; }
        /// <summary>
        /// Targy formula
        /// </summary>
        public string TargyFormula { get; set; }

        /// <summary>
        /// TargyAlapertelmezettFormula
        /// </summary>
        public string TargyAlapertelmezettFormula { get; set; }
        #endregion

        public override bool Equals(object obj)
        {
            var rule = obj as EKFRuleClass;
            return rule != null &&
                   IdProperty.Equals(rule.IdProperty);
        }

        public override int GetHashCode()
        {
            var hashCode = -895134899;
            hashCode = hashCode * -1521134295 + EqualityComparer<Guid>.Default.GetHashCode(IdProperty);
            return hashCode;
        }
        #endregion

        /// <summary>
        /// Visszaadja az ExecParam objektumot
        /// </summary>
        public ExecParam GetExecParam()
        {
            ExecParam xpm = new ExecParam();
            xpm.Felhasznalo_Id = ParametersProperty.UserIdProperty.ToString();
            xpm.FelhasznaloSzervezet_Id = ParametersProperty.UserGroupProperty.ToString();
            return xpm;
        }
    }
}
