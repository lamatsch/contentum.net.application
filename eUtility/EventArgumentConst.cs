using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUtility
{
    public class EventArgumentConst 
    {
        public const string refresh = "refresh";
        public const string refreshMasterList = "refreshMasterList";
        public const string refreshDetailList = "refreshDetailList";
        public const string Delete = "Delete";
        public const string refreshLovListByDetailSearch = "refreshLovListByDetailSearch";
        public const string refreshKuldemenyekPanel = "refreshKuldemenyekPanel";
        public const string refreshUgyiratokPanel = "refreshUgyiratokPanel";
        public const string refreshFileUploadComponent = "refreshFileUploadComponent";
        public const string refreshValuesFromParent = "refreshValuesFromParent";

        // TreeView
        public const string refreshSelectedNode = "refreshSelectedNode";
        public const string refreshSelectedNodeChildren = "refreshSelectedNodeChildren";
    }
}
