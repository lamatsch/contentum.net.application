using System;
using System.Text;
using System.Data;
using System.Web;
using Contentum.eBusinessDocuments;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace Contentum.eUtility
{
    public class CSVExport
    {

        private bool _errorFlag;
        public bool ErrorFlag
        {
            get { return _errorFlag; }
            set { _errorFlag = value; }
        }

        private string _classMsg;
        public string ClassMsg
        {
            get { return _classMsg; }
            set { _classMsg = value; }
        }

        private bool isProcedure;
        public bool IsProcedure
        {
            get { return isProcedure; }
            set { isProcedure = value; }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="CSVExport"/> class.
        /// </summary>
        /// <param name="isProcedure">if set to <c>true</c> [is isProcedure].</param>
        public CSVExport(bool isProcedure)
        {
            ErrorFlag = false;
            ClassMsg = String.Empty;
            this.isProcedure = isProcedure;
        }

        #region export from DataGridView - checked rows  
        /// <summary>
        /// Exports to CSV from data grid view.
        /// </summary>
        /// <param name="execParam">The execute parameter.</param>
        /// <param name="dataGridView">The data grid view.</param>
        public void ExportToCSVFromDataGridViewWithProcedure(ExecParam execParam, GridView dataGridView, string ConnectionString, string procedure, string sqlTable)
        {
            StringBuilder sb = new StringBuilder();
            //Get the data from database into datatable
            isProcedure = true;
            
            DataTable checkedRows = getDataTable_fromGridView(dataGridView);
            string[] ids = getIdListFromDataTable(checkedRows);
            DataTable dt = GetDataFromDb(execParam, procedure, ids, sqlTable, ConnectionString);

            for (int k = 0; k < dt.Columns.Count; k++)
            {
                //add separator
                sb.Append(dt.Columns[k].ColumnName + ';');
            }
            //append new line
            sb.Append(Environment.NewLine);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(dt.Rows[i][k].ToString() + ';');
                }
                //append new line
                sb.Append(Environment.NewLine);
            }
            HttpResponse Response = HttpContext.Current.Response;
            Response.BufferOutput = true;
            Response.Clear();
            Response.Buffer = true;


            Response.ContentEncoding = Encoding.GetEncoding(Constants.CSVParams.CSVEncoding);
            Response.ContentType = Constants.CSVParams.CSVContentType;
            //Response.Charset = Constants.CSVParams.CharSet;
            Response.AddHeader(Constants.CSVParams.ContentDisposition,
                 Constants.CSVParams.Attachment+sqlTable+DateTime.Now.ToString("yyyyMMdd")+".csv");
            Response.Output.Write((sb as StringBuilder).ToString());
            Response.Flush();
            Response.End();
        }
        /// <summary>
        /// Exports to CSV from data grid view.
        /// </summary>
        /// <param name="execParam">The execute parameter.</param>
        /// <param name="dataGridView">The data grid view.</param>
        public void ExportToCSVFromDataGridView(ExecParam execParam, GridView dataGridView)
        {   
            StringBuilder sb = new StringBuilder();
            //Get the data from database into datatable
            
            DataTable dt = getDataTable_fromGridView(dataGridView);
            for (int k = 0; k < dt.Columns.Count; k++)
            {
                //add separator
                sb.Append(dt.Columns[k].ColumnName + ';');
            }
            //append new line
            sb.Append(Environment.NewLine);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(dt.Rows[i][k].ToString() + ';');
                }
                //append new line
                sb.Append(Environment.NewLine);
            }
            
            
            HttpResponse Response = HttpContext.Current.Response;
            Response.BufferOutput = true;
            Response.Clear();
            Response.Buffer = true;


            Response.ContentEncoding = Encoding.GetEncoding(Constants.CSVParams.CSVEncoding);
            Response.ContentType = Constants.CSVParams.CSVContentType;
            //Response.Charset = Constants.CSVParams.CharSet;
            Response.AddHeader(Constants.CSVParams.ContentDisposition,
                 Constants.CSVParams.Attachment + DateTime.Now.ToString("yyyyMMdd") + ".csv");
            Response.Output.Write((sb as StringBuilder).ToString());
            Response.Flush();
            Response.End();
        }
        /// <summary>
        /// Gets an identifier list from data table's all ids.  
        /// </summary>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        private string[] getIdListFromDataTable(DataTable dt)
        {
            System.Collections.Generic.List<string> list = new System.Collections.Generic.List<string>();
            foreach (DataRow item in dt.Rows)
            {
                list.Add(item.ItemArray[0].ToString());
            }
            return list.ToArray();
        }
        /// <summary>
        /// Gets the data table from grid view. Where row is checked
        /// </summary>
        /// <param name="_dtGridView">The dt grid view.</param>
        /// <returns></returns>
        private DataTable getDataTable_fromGridView(GridView _dtGridView)
        {
            DataTable _resultTable = new DataTable();
            switch (_dtGridView.HeaderRow.Cells.Count)
            {
                case 0:
                    ErrorFlag = true;
                    ClassMsg = "Error! GridView is empty! (Column number: 0)";
                    return _resultTable;
                default:
                    int cellIndex = 0;
                    foreach (DataControlFieldCell cell in _dtGridView.HeaderRow.Cells)
                    {
                        if (cell.ContainingField.HeaderStyle.CssClass == "GridViewInvisibleColumnStyle") { continue; }
                        if (cell.ContainingField.HeaderStyle.CssClass == "GridViewCheckBoxTemplateHeaderStyle")
                        {
                            if (cellIndex == 0 && isProcedure)
                                _resultTable.Columns.Add("Id");
                            ++cellIndex;
                            continue;
                        }
                        if (cell.ContainingField is BoundField)
                        {
                            if (cell.ContainingField.Visible)
                            {
                                _resultTable.Columns.Add(cell.ContainingField.HeaderText);
                            }
                        }
                        else
                            if (cell.ContainingField is TemplateField && cell.ContainingField.Visible)
                            {
                                if (!String.IsNullOrEmpty(cell.ContainingField.HeaderText))
                                {
                                    _resultTable.Columns.Add(cell.ContainingField.HeaderText);
                                }
                            }
                        cellIndex++;
                    }
                    break;
            }

            switch (_dtGridView.Rows.Count)
            {
                case 0:
                    //nincs sor, nincs error, de nem dolgozunk fel adatokat
                    return _resultTable;
                default:
                    foreach (GridViewRow rowItem in _dtGridView.Rows)
                    {
                        DataRow _resultRowItem = _resultTable.NewRow();
                        if (rowItem.Cells[0].Controls[1].GetType() == typeof(CheckBox) && isProcedure)
                        {
                            if ((rowItem.Cells[0].Controls[1] as CheckBox).Checked)
                            {
                                _resultRowItem["Id"] = (rowItem.Cells[0].Controls[1] as CheckBox).Text;
                                
                            }
                            else continue;
                        }
                        foreach (DataControlFieldCell cell in rowItem.Cells)
                        {
                            if (cell.ContainingField.HeaderStyle.CssClass == "GridViewInvisibleColumnStyle") { continue; }
                            if (cell.ContainingField is BoundField)
                            {
                                if (cell.ContainingField.Visible && _resultTable.Columns.Contains(cell.ContainingField.HeaderText))
                                {
                                    _resultRowItem[cell.ContainingField.HeaderText] = HttpUtility.HtmlDecode(cell.Text);
                                }
                            }
                            else
                                if (cell.ContainingField is TemplateField && cell.ContainingField.Visible && cell.Controls.Count != 0)
                                {
                                    string controlString = "";
                                    for (int controlIndex = 0; controlIndex < cell.Controls.Count; controlIndex++)
                                    {
                                        if (cell.Controls[controlIndex].GetType() == typeof(Label))
                                        {
                                            controlString += Contentum.eUtility.TextUtils.getTextFromHTML(HttpUtility.HtmlDecode(((Label)cell.Controls[controlIndex]).Text));
                                        }
                                        if (cell.Controls[controlIndex].GetType() == typeof(System.Web.UI.WebControls.Image) || cell.Controls[controlIndex].GetType().Name == "CustomFunctionImageButton")
                                        {
                                            if (cell.Controls[controlIndex].Visible == true)
                                            {
                                                if (!String.IsNullOrEmpty(cell.ContainingField.HeaderText))
                                                {
                                                    controlString = cell.ContainingField.HeaderText;
                                                }
                                            }
                                            else
                                            {
                                                controlString = "empty";
                                            }
                                        }
                                    }
                                    if (!String.IsNullOrEmpty(controlString))
                                    {
                                        if (controlString != "empty" && _resultTable.Columns.Contains(cell.ContainingField.HeaderText))
                                        {
                                            _resultRowItem[cell.ContainingField.HeaderText] = controlString;
                                        }
                                    }
                                }
                        }
                        _resultTable.Rows.Add(_resultRowItem);
                    }
                    break;
            }

            if (_resultTable.Rows.Count != 0)
            {
                dropEmptyRows(_resultTable);
            }
            return _resultTable;
        }

        private void dropEmptyRows(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (AreAllColumnsEmpty(dt.Rows[i]))
                {
                    dt.Rows.RemoveAt(i);
                }
            }
        }

        private bool AreAllColumnsEmpty(DataRow dr)
        {
            if (dr == null)
            {
                return true;
            }
            else
            {
                for (int i = 0; i < dr.ItemArray.Length; i++)
                {
                    if (!(String.IsNullOrEmpty(Regex.Replace(dr.ItemArray[i].ToString(), @"\s+", ""))))
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        #endregion

        #region export From Database                
        /// <summary>
        /// Exports to CSV from database.
        /// </summary>
        /// <param name="execParam">The execute parameter.</param>
        /// <param name="strQuery">SQl select vagy a t�rolt elj�r�s neve.</param>
        /// <param name="sqlTable">SQL t�bla t�rolt elj�r�shoz.</param>
        public void ExportToCSVFromDb(ExecParam execParam, string strQuery, string sqlTable, string ConnectionString)
        {
            //Get the data from database into datatable
            DataTable dt = GetDataFromDb(execParam, strQuery, null, sqlTable, ConnectionString); 
            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < dt.Columns.Count; k++)
            {
                //add separator
                sb.Append(dt.Columns[k].ColumnName + ';');
            }
            //append new line
            sb.Append(Environment.NewLine);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(dt.Rows[i][k].ToString() + ';');
                }
                //append new line
                sb.Append(Environment.NewLine);
            }

            

            HttpResponse Response = HttpContext.Current.Response;
            //Response.ClearHeaders();
            Response.BufferOutput = true;
            Response.Clear();
            Response.Buffer = true;
           
            Response.AddHeader( Constants.CSVParams.ContentDisposition,
                 Constants.CSVParams.Attachment + sqlTable + DateTime.Now.ToString("yyyyMMdd") + ".csv");
            Response.ContentEncoding = Encoding.GetEncoding(Constants.CSVParams.CSVEncoding);
            //Response.Charset = Constants.CSVParams.CharSet;
            Response.ContentType = Constants.CSVParams.CSVContentType;
           
            Response.Output.Write((sb as StringBuilder).ToString());
            Response.Flush();
            Response.End();
        }
        /// <summary>
        /// Gets the data from database.
        /// </summary>
        /// <param name="execParam">The execute parameter.</param>
        /// <param name="cmd">A t�rolt elj�r�s neve vagy az sql select.</param>
        /// <param name="ids">T�rolt elj�r�shoz a sor azonos�t�k.</param>
        /// <param name="sqlTable">T�rolt elj�r�shoz az sql t�bla.</param>
        /// <returns></returns>
        private DataTable GetDataFromDb(ExecParam execParam, string cmd, string[] ids, string sqlTable, string ConnectionString)
        {
            DataTable dt = new DataTable();
            if (String.IsNullOrEmpty(ConnectionString)) throw new ResultException("nincs inicializ�lva a connectionstring");
            using (System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(ConnectionString))
            {

                using (System.Data.SqlClient.SqlCommand sqlCommand = new System.Data.SqlClient.SqlCommand())
                {
                    if (isProcedure)
                    {
                        sqlCommand.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
                        //sqlCommand.Parameters["@Where"].Size = 4000;
                        string inWhere = "'";
                        if (ids.Length > 0)
                        {
                            for (int i = 0; i < ids.Length; ++i)
                            {
                                if (i == 0)
                                    inWhere += ids[i];
                                else if (i == ids.Length - 1)
                                    inWhere += "', '" + ids[i] ;
                                else
                                    inWhere += "', '" + ids[i];
                                if (i == ids.Length - 1)
                                    inWhere += "'";
                            }
                            sqlCommand.Parameters["@Where"].Value = " " + sqlTable + @".Id in (" + inWhere + ")";
                            
                        }
                        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
                        sqlCommand.Parameters["@ExecutorUserId"].Value = new System.Data.SqlTypes.SqlGuid(execParam.Felhasznalo_Id);
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                    }
                    else
                        sqlCommand.CommandType = CommandType.Text;
                    System.Data.SqlClient.SqlDataAdapter sda = new System.Data.SqlClient.SqlDataAdapter();

                    sqlCommand.Connection = sqlConnection;
                    sqlCommand.CommandText = cmd;
                    try
                    {
                        sqlConnection.Open();
                        sda.SelectCommand = sqlCommand;
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        sda.Dispose();
                    }
                }
            }
        }
        #endregion
    }
}
