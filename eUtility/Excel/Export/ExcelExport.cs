using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

namespace Contentum.eUtility
{
    public class ExcelExport
    {
        private static string nyomtatasHeader = "";
        private bool _errorFlag;
        public bool ErrorFlag
        {
            get { return _errorFlag; }
            set { _errorFlag = value; }
        }

        private string _classMsg;
        public string ClassMsg
        {
            get { return _classMsg; }
            set { _classMsg = value; }
        }

        public ExcelExport()
        {
            ErrorFlag = false;
            ClassMsg = String.Empty;
        }

        public void SaveGridView_ToExcel(ExecParam execParam, GridView dataGridView, string aspxHeaderListName, string headerType, string browser)
        {
            SaveGridView_ToExcel(execParam, dataGridView, aspxHeaderListName, headerType, new List<double>(), double.NaN, browser);
        }

        public void SaveGridView_ToExcel(ExecParam execParam, GridView dataGridView, string aspxHeaderListName, string headerType, List<double> columnSizeList, string browser)
        {
            SaveGridView_ToExcel(execParam, dataGridView, aspxHeaderListName, headerType, columnSizeList, double.NaN, browser);
        }

        //LZS - BLG_2936
        //widhFactor �tad�sa
        public void SaveGridView_ToExcel(ExecParam execParam, GridView dataGridView, string aspxHeaderListName, string headerType, double widthFactor, string browser)
        {
            SaveGridView_ToExcel(execParam, dataGridView, aspxHeaderListName, headerType, new List<double>(), widthFactor, browser);
        }

        public void SaveDataTable_ToExcel(ExecParam execParam, DataTable dataTableSzervezetek, DataTable dataTableSzemelyek, string aspxHeaderListName, string headerType, double widthFactor, string browser)
        {
            SaveDataTable_ToExcel(execParam, dataTableSzervezetek, dataTableSzemelyek, aspxHeaderListName, headerType, new List<double>(), widthFactor, browser);
        }

        public void SaveDataTable_ToExcel(ExecParam execParam, DataTable dataTableSzervezetek, DataTable dataTableSzemelyek, string aspxHeaderListName, string headerType, List<double> columnSizeList, double widthFactor, string browser)
        {
            string sheetName = String.Empty;
            string fileName = String.Empty;
            string userName = String.Empty;
            string groupName = String.Empty;

            #region felhaszn�l� neve
            KRT_FelhasznalokService felhService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            execParam.Record_Id = execParam.LoginUser_Id;
            Result felhResult = felhService.Get(execParam);
            if (!felhResult.IsError)
            {
                KRT_Felhasznalok felhRecord = (KRT_Felhasznalok)felhResult.Record;
                userName = felhRecord.Nev;
            }
            //TODO  - sz�r�si paramok
            #endregion
            #region CR3187 - Nyomtat�s header
            nyomtatasHeader = Rendszerparameterek.Get(execParam, Rendszerparameterek.NYOMTATAS_HEADER);
            #endregion
            #region szervezet neve
            KRT_CsoportokService csopService = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            execParam.Record_Id = execParam.FelhasznaloSzervezet_Id;
            Result csopResult = csopService.Get(execParam);
            if (!csopResult.IsError)
            {
                KRT_Csoportok csopRecord = (KRT_Csoportok)csopResult.Record;
                groupName = csopRecord.Nev;
            }
            #endregion

            if (!string.IsNullOrEmpty(aspxHeaderListName))
            {
                sheetName = HttpUtility.HtmlDecode(aspxHeaderListName);

                switch (browser)
                {
                    case "FF":
                        // BUG_6452
                        //fileName = HttpUtility.HtmlDecode(aspxHeaderListName) + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xls";
                        fileName = sheetName + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xlsx";

                        break;

                    case "IE":
                        // BUG_6452
                        //fileName = EncodeFileName(aspxHeaderListName) + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xls";
                        fileName = EncodeFileName(sheetName) + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xlsx";
                        break;
                }

            }

            XSSFWorkbook workbook = new XSSFWorkbook();
            ISheet worksheet_BodySzemelyek = BuildWorkSheet(dataTableSzemelyek, "Szemely_tipus_adatlista", workbook, headerType, columnSizeList, userName, groupName, widthFactor);
            ISheet worksheet_BodySzervezetek = BuildWorkSheet(dataTableSzervezetek, "Szervezet_tipus_adatlista", workbook, headerType, columnSizeList, userName, groupName, widthFactor);

            MemoryStream memoryStream = new MemoryStream();
            using (MemoryStream tempStream = new MemoryStream())
            {
                workbook.Write(tempStream);
                var byteArray = tempStream.ToArray();
                memoryStream.Write(byteArray, 0, byteArray.Length);
                HttpResponse response = HttpContext.Current.Response;
                response.BufferOutput = true;
                response.Clear();
                response.Buffer = true;
                response.AddHeader("Accept-Header", "0");
                response.AddHeader("Content-Length", memoryStream.Length.ToString());
                string contentDispositon = "inline" + (!String.IsNullOrEmpty(fileName) ? "; filename=" + fileName : "");
                response.AddHeader("Content-Disposition", contentDispositon);
                response.AddHeader("Expires", "0");
                response.AddHeader("Pragma", "cache");
                response.AddHeader("Cache-Control", "private");
                response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                response.AddHeader("Accept-Ranges", "bytes");
                response.BinaryWrite(memoryStream.GetBuffer());
                response.Flush();
            }
            /*try
			{
				 response.End();
			}
			catch
			{
			}*/
        }

        /// <summary>
        /// Itt lehet a HeaderTypeokat be�ll�tani (ami t�rolva van).
        /// <param name="execParam"></param>
        /// <param name="dataGridView">A lista gridViewja</param>
        /// <param name="aspxHeaderListName">A lista neve</param>
        /// <param name="headerType">A fejl�c t�rolt t�pusa (Haszn�ld a Constants.ExcelHeaderType-ot)</param>
        /// </summary>
        public void SaveGridView_ToExcel(ExecParam execParam, GridView dataGridView, string aspxHeaderListName, string headerType, List<double> columnSizeList, double widthFactor, string browser)
        {
            string sheetName = String.Empty;
            string fileName = String.Empty;
            string userName = String.Empty;
            string groupName = String.Empty;

            #region felhaszn�l� neve
            KRT_FelhasznalokService felhService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            execParam.Record_Id = execParam.LoginUser_Id;
            Result felhResult = felhService.Get(execParam);
            if (!felhResult.IsError)
            {
                KRT_Felhasznalok felhRecord = (KRT_Felhasznalok)felhResult.Record;
                userName = felhRecord.Nev;
            }
            //TODO  - sz�r�si paramok
            #endregion
            #region CR3187 - Nyomtat�s header
            nyomtatasHeader = Rendszerparameterek.Get(execParam, Rendszerparameterek.NYOMTATAS_HEADER);
            #endregion
            #region szervezet neve
            KRT_CsoportokService csopService = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            execParam.Record_Id = execParam.FelhasznaloSzervezet_Id;
            Result csopResult = csopService.Get(execParam);
            if (!csopResult.IsError)
            {
                KRT_Csoportok csopRecord = (KRT_Csoportok)csopResult.Record;
                groupName = csopRecord.Nev;
            }
            #endregion

            if (!string.IsNullOrEmpty(aspxHeaderListName))
            {
                sheetName = HttpUtility.HtmlDecode(aspxHeaderListName);

                switch (browser)
                {
                    case "FF":
                        // BUG_6452
                        //fileName = HttpUtility.HtmlDecode(aspxHeaderListName) + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xls";
                        fileName = sheetName + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xlsx";

                        break;

                    case "IE":
                        // BUG_6452
                        //fileName = EncodeFileName(aspxHeaderListName) + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xls";
                        fileName = EncodeFileName(sheetName) + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xlsx";
                        break;
                }

            }

            DataTable toXLSExport = getDataTable_fromGridView(dataGridView);

            XSSFWorkbook workbook = new XSSFWorkbook();
            ISheet worksheet_Body = BuildWorkSheet(toXLSExport, sheetName, workbook, headerType, columnSizeList, userName, groupName, widthFactor);

            MemoryStream memoryStream = new MemoryStream();
            using (MemoryStream tempStream = new MemoryStream())
            {
                workbook.Write(tempStream);
                var byteArray = tempStream.ToArray();
                memoryStream.Write(byteArray, 0, byteArray.Length);
                HttpResponse response = HttpContext.Current.Response;
                response.BufferOutput = true;
                response.Clear();
                response.Buffer = true;
                response.AddHeader("Accept-Header", "0");
                response.AddHeader("Content-Length", memoryStream.Length.ToString());
                string contentDispositon = "inline" + (!String.IsNullOrEmpty(fileName) ? "; filename=" + fileName : "");
                response.AddHeader("Content-Disposition", contentDispositon);
                response.AddHeader("Expires", "0");
                response.AddHeader("Pragma", "cache");
                response.AddHeader("Cache-Control", "private");
                response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                response.AddHeader("Accept-Ranges", "bytes");
                response.BinaryWrite(memoryStream.GetBuffer());
                response.Flush();
            }
            /*try
			{
				 response.End();
			}
			catch
			{
			}*/
        }

        #region Saj�t bels� elj�r�sok

        private DataTable getDataTable_fromGridView(GridView _dtGridView)
        {
            DataTable _resultTable = new DataTable();

            if (_dtGridView.HeaderRow == null || _dtGridView.HeaderRow.Cells.Count == 0 || _dtGridView.Rows.Count == 0)
            {
                return _resultTable;
            }

            int cellIndex = 0;
            foreach (DataControlFieldCell cell in _dtGridView.HeaderRow.Cells)
            {
                if (cell.ContainingField.HeaderStyle.CssClass == "GridViewInvisibleColumnStyle") { continue; }
                if (cell.ContainingField.HeaderStyle.CssClass == "GridViewCheckBoxTemplateHeaderStyle") { continue; }
                if (cell.ContainingField is BoundField)
                {
                    if (cell.ContainingField.Visible)
                    {
                        _resultTable.Columns.Add(cell.ContainingField.HeaderText);
                    }
                }
                else
                     if (cell.ContainingField is TemplateField && cell.ContainingField.Visible)//&& cell.Controls.Count != 0)
                {
                    if (!String.IsNullOrEmpty(cell.ContainingField.HeaderText))
                    {
                        _resultTable.Columns.Add(cell.ContainingField.HeaderText);
                    }
                }
                cellIndex++;
            }

            foreach (GridViewRow rowItem in _dtGridView.Rows)
            {
                DataRow _resultRowItem = _resultTable.NewRow();
                foreach (DataControlFieldCell cell in rowItem.Cells)
                {
                    if (cell.ContainingField.HeaderStyle.CssClass == "GridViewInvisibleColumnStyle") { continue; }
                    if (cell.ContainingField is BoundField)
                    {
                        if (cell.ContainingField.Visible && _resultTable.Columns.Contains(cell.ContainingField.HeaderText))
                        {
                            _resultRowItem[cell.ContainingField.HeaderText] = HttpUtility.HtmlDecode(cell.Text);
                        }
                    }
                    else
                         if (cell.ContainingField is TemplateField && cell.ContainingField.Visible && cell.Controls.Count != 0)
                    {
                        string controlString = "";
                        for (int controlIndex = 0; controlIndex < cell.Controls.Count; controlIndex++)
                        {
                            if (cell.Controls[controlIndex].GetType() == typeof(Label))
                            {
                                controlString += TextUtils.getTextFromHTML(HttpUtility.HtmlDecode(((Label)cell.Controls[controlIndex]).Text));
                            }
                            if (cell.Controls[controlIndex].GetType() == typeof(Image) || cell.Controls[controlIndex].GetType().Name == "CustomFunctionImageButton")
                            {
                                if (cell.Controls[controlIndex].Visible == true)
                                {
                                    if (!String.IsNullOrEmpty(cell.ContainingField.HeaderText))
                                    {
                                        controlString = cell.ContainingField.HeaderText;
                                    }
                                }
                                else
                                {
                                    controlString = "empty";
                                }
                            }
                        }
                        if (!String.IsNullOrEmpty(controlString))
                        {
                            if (controlString != "empty" && _resultTable.Columns.Contains(cell.ContainingField.HeaderText))
                            {
                                _resultRowItem[cell.ContainingField.HeaderText] = controlString;
                            }
                        }
                    }
                }
                _resultTable.Rows.Add(_resultRowItem);
            }

            if (_resultTable.Rows.Count != 0)
            {
                dropEmptyRows(_resultTable);
            }
            return _resultTable;
        }

        private ISheet BuildWorkSheet(DataTable table, string sheetName, XSSFWorkbook mainBook, string headerType, List<double> columnSizeList)
        {
            return BuildWorkSheet(table, sheetName, mainBook, headerType, columnSizeList, String.Empty, String.Empty, double.NaN);
        }

        private ISheet BuildWorkSheet(DataTable table, string sheetName, XSSFWorkbook mainBook, string headerType, double widthFactor)
        {
            return BuildWorkSheet(table, sheetName, mainBook, headerType, null, String.Empty, String.Empty, widthFactor);
        }

        private ISheet BuildWorkSheet(DataTable table, string sheetName, XSSFWorkbook mainBook, string headerType, List<double> columnSizeList, string userName, string groupName, double widthFactor)
        {
            ISheet _tempWorksheet = mainBook.CreateSheet(sheetName);

            #region Start koordin�t�k default �rt�ke
            int col_StartNumber = 0;
            int row_StartNumber = 0;
            #endregion

            #region Cellast�lusok be�ll�t�sa
            ICellStyle HeaderCell_Style = mainBook.CreateCellStyle();
            HeaderCell_Style.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style.FillPattern = FillPattern.SolidForeground;
            HeaderCell_Style.Alignment = HorizontalAlignment.Center;
            HeaderCell_Style.VerticalAlignment = VerticalAlignment.Center;
            IFont HeaderFont = mainBook.CreateFont();
            HeaderFont.FontName = "Arial";
            HeaderFont.FontHeightInPoints = 9;
            HeaderFont.Boldweight = (short)FontBoldWeight.Bold;
            HeaderCell_Style.SetFont(HeaderFont);
            HeaderCell_Style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            HeaderCell_Style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            HeaderCell_Style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            HeaderCell_Style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            HeaderCell_Style.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            HeaderCell_Style.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;

            IFont minorFont = mainBook.CreateFont();
            minorFont.FontHeightInPoints = 8;
            minorFont.Boldweight = (short)FontBoldWeight.Bold;
            minorFont.FontName = "Arial";
            ICellStyle minorStyle = mainBook.CreateCellStyle();
            minorStyle.Alignment = HorizontalAlignment.Left;
            minorStyle.VerticalAlignment = VerticalAlignment.Center;
            minorStyle.SetFont(minorFont);
            minorStyle.WrapText = true;

            IFont tableFont = mainBook.CreateFont();
            tableFont.FontName = "Arial";
            tableFont.FontHeightInPoints = 9;

            #region P�ratlan form�z�sok

            #region P�ratlan DateTime form�z�s
            ICellStyle BodyCell_StyleOdd_DateTime = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleOdd_DateTime = mainBook.CreateDataFormat();
            BodyCell_StyleOdd_DateTime.DataFormat = dataFormat_BodyCell_StyleOdd_DateTime.GetFormat("yyyy.MM.dd HH:mm:ss");
            BodyCell_StyleOdd_DateTime.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LemonChiffon.Index;
            BodyCell_StyleOdd_DateTime.FillPattern = FillPattern.SolidForeground;
            BodyCell_StyleOdd_DateTime.WrapText = true;
            BodyCell_StyleOdd_DateTime.VerticalAlignment = VerticalAlignment.Top;
            BodyCell_StyleOdd_DateTime.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_DateTime.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_DateTime.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_DateTime.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_DateTime.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_DateTime.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_DateTime.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_DateTime.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_DateTime.SetFont(tableFont);
            #endregion

            #region P�ratlan DateTime form�z�s (Ha nincs id�)
            ICellStyle BodyCell_StyleOdd_DateTime_WithoutTime = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleOdd_DateTime_WithoutTime = mainBook.CreateDataFormat();
            BodyCell_StyleOdd_DateTime_WithoutTime.DataFormat = dataFormat_BodyCell_StyleOdd_DateTime_WithoutTime.GetFormat("yyyy.MM.dd");
            BodyCell_StyleOdd_DateTime_WithoutTime.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LemonChiffon.Index;
            BodyCell_StyleOdd_DateTime_WithoutTime.FillPattern = FillPattern.SolidForeground;
            BodyCell_StyleOdd_DateTime_WithoutTime.WrapText = true;
            BodyCell_StyleOdd_DateTime_WithoutTime.VerticalAlignment = VerticalAlignment.Top;
            BodyCell_StyleOdd_DateTime_WithoutTime.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_DateTime_WithoutTime.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_DateTime_WithoutTime.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_DateTime_WithoutTime.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_DateTime_WithoutTime.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_DateTime_WithoutTime.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_DateTime_WithoutTime.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_DateTime_WithoutTime.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_DateTime_WithoutTime.SetFont(tableFont);
            #endregion

            #region P�ratlan Integer form�z�s
            ICellStyle BodyCell_StyleOdd_Number = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleOdd_Number = mainBook.CreateDataFormat();
            BodyCell_StyleOdd_Number.DataFormat = dataFormat_BodyCell_StyleOdd_Number.GetFormat("0");
            BodyCell_StyleOdd_Number.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LemonChiffon.Index;
            BodyCell_StyleOdd_Number.FillPattern = FillPattern.SolidForeground;
            BodyCell_StyleOdd_Number.WrapText = true;
            BodyCell_StyleOdd_Number.VerticalAlignment = VerticalAlignment.Top;
            BodyCell_StyleOdd_Number.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_Number.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_Number.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_Number.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_Number.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_Number.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_Number.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_Number.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_Number.SetFont(tableFont);
            #endregion

            #region P�ratlan Double form�z�s
            ICellStyle BodyCell_StyleOdd_Double = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleOdd_Double = mainBook.CreateDataFormat();
            BodyCell_StyleOdd_Double.DataFormat = dataFormat_BodyCell_StyleOdd_Double.GetFormat("#,##0.###");
            BodyCell_StyleOdd_Double.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LemonChiffon.Index;
            BodyCell_StyleOdd_Double.FillPattern = FillPattern.SolidForeground;
            BodyCell_StyleOdd_Double.WrapText = true;
            BodyCell_StyleOdd_Double.VerticalAlignment = VerticalAlignment.Top;
            BodyCell_StyleOdd_Double.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_Double.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_Double.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_Double.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_Double.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_Double.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_Double.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_Double.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_Double.SetFont(tableFont);
            #endregion

            #region P�ratlan String form�z�s
            ICellStyle BodyCell_StyleOdd_String = mainBook.CreateCellStyle();
            BodyCell_StyleOdd_String.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LemonChiffon.Index;
            BodyCell_StyleOdd_String.FillPattern = FillPattern.SolidForeground;
            BodyCell_StyleOdd_String.WrapText = true;
            BodyCell_StyleOdd_String.VerticalAlignment = VerticalAlignment.Top;
            BodyCell_StyleOdd_String.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_String.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_String.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_String.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleOdd_String.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_String.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_String.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_String.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleOdd_String.SetFont(tableFont);
            #endregion

            #endregion

            #region P�ros form�z�sok

            #region P�ros DateTime form�z�s
            ICellStyle BodyCell_StyleDuo_DateTime = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleDuo_DateTime = mainBook.CreateDataFormat();
            BodyCell_StyleDuo_DateTime.DataFormat = dataFormat_BodyCell_StyleDuo_DateTime.GetFormat("yyyy.MM.dd HH:mm:ss");
            BodyCell_StyleDuo_DateTime.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            BodyCell_StyleDuo_DateTime.FillPattern = FillPattern.SolidForeground;
            BodyCell_StyleDuo_DateTime.WrapText = true;
            BodyCell_StyleDuo_DateTime.VerticalAlignment = VerticalAlignment.Top;
            BodyCell_StyleDuo_DateTime.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_DateTime.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_DateTime.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_DateTime.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_DateTime.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_DateTime.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_DateTime.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_DateTime.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_DateTime.SetFont(tableFont);
            #endregion

            #region P�ros DateTime form�z�s (Ha nincs id�)
            ICellStyle BodyCell_StyleDuo_DateTime_WithoutTime = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleDuo_DateTime_WithoutTime = mainBook.CreateDataFormat();
            BodyCell_StyleDuo_DateTime_WithoutTime.DataFormat = dataFormat_BodyCell_StyleDuo_DateTime_WithoutTime.GetFormat("yyyy.MM.dd");
            BodyCell_StyleDuo_DateTime_WithoutTime.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            BodyCell_StyleDuo_DateTime_WithoutTime.FillPattern = FillPattern.SolidForeground;
            BodyCell_StyleDuo_DateTime_WithoutTime.WrapText = true;
            BodyCell_StyleDuo_DateTime_WithoutTime.VerticalAlignment = VerticalAlignment.Top;
            BodyCell_StyleDuo_DateTime_WithoutTime.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_DateTime_WithoutTime.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_DateTime_WithoutTime.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_DateTime_WithoutTime.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_DateTime_WithoutTime.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_DateTime_WithoutTime.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_DateTime_WithoutTime.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_DateTime_WithoutTime.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_DateTime_WithoutTime.SetFont(tableFont);
            #endregion

            #region P�ros Integer form�z�s
            ICellStyle BodyCell_StyleDuo_Number = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleDuo_Number = mainBook.CreateDataFormat();
            BodyCell_StyleDuo_Number.DataFormat = dataFormat_BodyCell_StyleDuo_Number.GetFormat("0");
            BodyCell_StyleDuo_Number.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            BodyCell_StyleDuo_Number.FillPattern = FillPattern.SolidForeground;
            BodyCell_StyleDuo_Number.WrapText = true;
            BodyCell_StyleDuo_Number.VerticalAlignment = VerticalAlignment.Top;
            BodyCell_StyleDuo_Number.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_Number.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_Number.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_Number.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_Number.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_Number.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_Number.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_Number.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_Number.SetFont(tableFont);
            #endregion

            #region P�ros Double form�z�s
            ICellStyle BodyCell_StyleDuo_Double = mainBook.CreateCellStyle();
            IDataFormat dataFormat_BodyCell_StyleDuo_Double = mainBook.CreateDataFormat();
            BodyCell_StyleDuo_Double.DataFormat = dataFormat_BodyCell_StyleDuo_Double.GetFormat("#,##0.###");
            BodyCell_StyleDuo_Double.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            BodyCell_StyleDuo_Double.FillPattern = FillPattern.SolidForeground;
            BodyCell_StyleDuo_Double.WrapText = true;
            BodyCell_StyleDuo_Double.VerticalAlignment = VerticalAlignment.Top;
            BodyCell_StyleDuo_Double.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_Double.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_Double.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_Double.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_Double.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_Double.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_Double.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_Double.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_Double.SetFont(tableFont);
            #endregion

            #region P�ros String form�z�s
            ICellStyle BodyCell_StyleDuo_String = mainBook.CreateCellStyle();
            BodyCell_StyleDuo_String.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.White.Index;
            BodyCell_StyleDuo_String.FillPattern = FillPattern.SolidForeground;
            BodyCell_StyleDuo_String.WrapText = true;
            BodyCell_StyleDuo_String.VerticalAlignment = VerticalAlignment.Top;
            BodyCell_StyleDuo_String.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_String.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_String.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_String.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            BodyCell_StyleDuo_String.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_String.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_String.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_String.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            BodyCell_StyleDuo_String.SetFont(tableFont);
            #endregion

            #endregion
            #endregion

            #region Worksheet fejl�c�nek be�ll�t�sa + start koordin�t�k SET
            switch (headerType)
            {
                case Constants.ExcelHeaderType.Standard:
                    #region CR3187 - Nyomtat�s header
                    _tempWorksheet.Header.Center = HSSFHeader.Font("Arial", "Bold") + HSSFHeader.FontSize((short)14) + nyomtatasHeader + "\r\n " + sheetName;
                    #endregion

                    _tempWorksheet.SetMargin(MarginType.TopMargin, 1.4);
                    #region els� sor a f�c�m ut�n

                    IRow firstMinorRow = _tempWorksheet.CreateRow(row_StartNumber);
                    if (!String.IsNullOrEmpty(userName))
                    {
                        ICell userName_Cell = firstMinorRow.CreateCell(col_StartNumber);
                        userName_Cell.CellStyle = minorStyle;
                        userName_Cell.SetCellValue(userName);
                        NPOI.SS.Util.CellRangeAddress userNameCellRange = new NPOI.SS.Util.CellRangeAddress(row_StartNumber, row_StartNumber, col_StartNumber, col_StartNumber + 5);
                        _tempWorksheet.AddMergedRegion(userNameCellRange);
                    }

                    row_StartNumber++;
                    #endregion

                    #region m�sodik sor a f�c�m ut�n
                    IRow secondMinorRow = _tempWorksheet.CreateRow(row_StartNumber);
                    if (!String.IsNullOrEmpty(groupName))
                    {
                        ICell groupName_Cell = secondMinorRow.CreateCell(col_StartNumber);
                        groupName_Cell.CellStyle = minorStyle;
                        groupName_Cell.SetCellValue(groupName);
                        NPOI.SS.Util.CellRangeAddress groupCellRange = new NPOI.SS.Util.CellRangeAddress(row_StartNumber, row_StartNumber, col_StartNumber, col_StartNumber + 5);
                        _tempWorksheet.AddMergedRegion(groupCellRange);
                    }
                    ICell dataCount_Cell = secondMinorRow.CreateCell(col_StartNumber + 6);
                    dataCount_Cell.SetCellValue("T�telek sz�ma: " + table.Rows.Count);
                    dataCount_Cell.CellStyle = minorStyle;
                    NPOI.SS.Util.CellRangeAddress dataCountCellRange = new NPOI.SS.Util.CellRangeAddress(row_StartNumber, row_StartNumber, col_StartNumber + 6, col_StartNumber + 12);
                    _tempWorksheet.AddMergedRegion(dataCountCellRange);
                    row_StartNumber += 2;
                    #endregion
                    break;
                case Constants.ExcelHeaderType.None:
                default:
                    col_StartNumber = 1;
                    row_StartNumber = 1;
                    break;
            }
            #endregion

            #region T�bla fejl�c�nek bet�lt�se a WorkSheetbe
            IRow new_Row_Header = _tempWorksheet.CreateRow(row_StartNumber);
            new_Row_Header.HeightInPoints = 24;
            foreach (DataColumn colItem in table.Columns)
            {
                ICell new_Cell_Header = new_Row_Header.CreateCell(colItem.Ordinal + col_StartNumber);
                new_Cell_Header.SetCellValue(colItem.ColumnName);
                new_Cell_Header.CellStyle = HeaderCell_Style;
                if (colItem.Ordinal > columnSizeList.Count - 1)
                {
                    _tempWorksheet.AutoSizeColumn(colItem.Ordinal + col_StartNumber);
                }
                else
                {
                    _tempWorksheet.SetColumnWidth(colItem.Ordinal + col_StartNumber, (int)(columnSizeList[colItem.Ordinal] * 300));
                }
            }
            _tempWorksheet.RepeatingRows = new NPOI.SS.Util.CellRangeAddress(row_StartNumber, row_StartNumber, 0, 0);
            row_StartNumber++;
            #endregion

            #region T�bla adatainak bet�lt�se a WorkSheetbe
            int rowIndex_Incremental = 0;
            foreach (DataRow rowItem in table.Rows)
            {
                IRow new_Row_Body = _tempWorksheet.CreateRow(rowIndex_Incremental + row_StartNumber);
                foreach (DataColumn colItem in table.Columns)
                {
                    ICell new_Cell_Body = new_Row_Body.CreateCell(colItem.Ordinal + col_StartNumber);
                    TypeCode temporaryCode = TypeCode.String;
                    switch (excel_ObjectParser(rowItem[colItem.ColumnName].ToString()))
                    {
                        case TypeCode.DateTime:
                            new_Cell_Body.SetCellValue(DateTime.Parse(rowItem[colItem.ColumnName].ToString()));
                            temporaryCode = TypeCode.DateTime;
                            break;
                        case TypeCode.Char:
                            if (rowItem[colItem.ColumnName].ToString().Equals('1') || rowItem[colItem.ColumnName].ToString().Equals('0'))
                            {
                                new_Cell_Body.SetCellValue(bool.Parse(rowItem[colItem.ColumnName].ToString()).ToString());
                            }
                            else
                            {
                                new_Cell_Body.SetCellValue(rowItem[colItem.ColumnName].ToString());
                            }
                            break;
                        case TypeCode.Double:
                            new_Cell_Body.SetCellValue(Double.Parse(rowItem[colItem.ColumnName].ToString()));
                            temporaryCode = TypeCode.Double;
                            break;
                        case TypeCode.String:
                            new_Cell_Body.SetCellValue(rowItem[colItem.ColumnName].ToString());
                            break;
                        case TypeCode.Int64:
                            new_Cell_Body.SetCellValue(Int64.Parse(rowItem[colItem.ColumnName].ToString()));
                            temporaryCode = TypeCode.Int64;
                            break;
                    }
                    switch (rowIndex_Incremental % 2 != 0)
                    {
                        case true:
                            switch (temporaryCode)
                            {
                                case TypeCode.String:
                                    new_Cell_Body.CellStyle = BodyCell_StyleOdd_String;
                                    break;
                                case TypeCode.Int64:
                                    new_Cell_Body.CellStyle = BodyCell_StyleOdd_Number;
                                    break;
                                case TypeCode.Double:
                                    new_Cell_Body.CellStyle = BodyCell_StyleOdd_Double;
                                    break;
                                case TypeCode.DateTime:
                                    DateTime dateTime_Checker = DateTime.Parse(new_Cell_Body.DateCellValue.ToString());
                                    if (dateTime_Checker.Hour == 0 && dateTime_Checker.Minute == 0 && dateTime_Checker.Second == 0)
                                    {
                                        new_Cell_Body.CellStyle = BodyCell_StyleOdd_DateTime_WithoutTime;
                                    }
                                    else
                                    {
                                        new_Cell_Body.CellStyle = BodyCell_StyleOdd_DateTime;
                                    }
                                    break;
                            }
                            break;
                        case false:
                            switch (temporaryCode)
                            {
                                case TypeCode.String:
                                    new_Cell_Body.CellStyle = BodyCell_StyleDuo_String;
                                    break;
                                case TypeCode.Int64:
                                    new_Cell_Body.CellStyle = BodyCell_StyleDuo_Number;
                                    break;
                                case TypeCode.Double:
                                    new_Cell_Body.CellStyle = BodyCell_StyleDuo_Double;
                                    break;
                                case TypeCode.DateTime:
                                    DateTime dateTime_Checker = DateTime.Parse(new_Cell_Body.DateCellValue.ToString());
                                    if (dateTime_Checker.Hour == 0 && dateTime_Checker.Minute == 0 && dateTime_Checker.Second == 0)
                                    {
                                        new_Cell_Body.CellStyle = BodyCell_StyleDuo_DateTime_WithoutTime;
                                    }
                                    else
                                    {
                                        new_Cell_Body.CellStyle = BodyCell_StyleDuo_DateTime;
                                    }
                                    break;
                            }
                            break;
                    }
                }
                rowIndex_Incremental++;
            }
            #endregion
            /* NPOI Oldalbe�ll�t�s
		* 
		* worksheet_Body.FitToPage = false; <- ezzel lehet teljesen kikapcsolni azt, hogy az oldal magass�g�hoz vagy hossz�s�g�hoz m�retezze az excelt.
		* 
		* worksheet_Body.PrintSetup.FitHeight = 1 <- ez a default, a sz�m az oldalak sz�m�t jel�li, ahova el kell f�rnie hossz�ban.
		* worksheet_Body.PrintSetup.FitWidth = 1 <- ugyanaz, mint felette, csak ez a sz�less�ghez k�t�dik.
	  */
            #region automatikus oszlopsz�less�gek be�ll�t�sa
            if (double.IsNaN(widthFactor))
                widthFactor = 1.06;

            foreach (DataColumn colItem in table.Columns)
            {
                if (colItem.Ordinal > columnSizeList.Count - 1)
                {
                    _tempWorksheet.AutoSizeColumn(colItem.Ordinal + col_StartNumber);
                    int width = _tempWorksheet.GetColumnWidth(colItem.Ordinal + col_StartNumber);
                    width = (int)(width * widthFactor);
                    _tempWorksheet.SetColumnWidth(colItem.Ordinal + col_StartNumber, width);
                }
            }
            #endregion
            _tempWorksheet.Footer.Left = "Oldal: " + HeaderFooter.Page + "/" + HeaderFooter.NumPages;
            _tempWorksheet.PrintSetup.PaperSize = (short)PaperSize.A4 + 1;
            _tempWorksheet.PrintSetup.Landscape = true;
            _tempWorksheet.FitToPage = true;
            _tempWorksheet.PrintSetup.FitWidth = (short)1;
            _tempWorksheet.PrintSetup.FitHeight = 0;
            _tempWorksheet.HorizontallyCenter = true;
            _tempWorksheet.Autobreaks = true;
            return _tempWorksheet;
        }

        private TypeCode excel_ObjectParser(string needToParse)
        {
            DateTime parser_DateTime = new DateTime();
            Double parser_Double = new Double();
            Char parser_Char = new Char();
            Int64 parser_Int = new Int64();

            bool isAzonosito = regex_UsualDataStructures(needToParse);


            if (DateTime.TryParse(needToParse, out parser_DateTime) && !isAzonosito)
            {
                if (parser_DateTime.Year >= 1900)
                {
                    return Type.GetTypeCode(parser_DateTime.GetType());
                }
            }
            if (Char.TryParse(needToParse, out parser_Char))
            {
                return Type.GetTypeCode(parser_Char.GetType());
            }
            if (Int64.TryParse(needToParse, out parser_Int))
            {
                return Type.GetTypeCode(parser_Int.GetType());
            }
            if (Double.TryParse(needToParse, out parser_Double))
            {
                if (needToParse.Length < 17)
                {
                    return Type.GetTypeCode(parser_Double.GetType());
                }
            }

            return Type.GetTypeCode(needToParse.GetType());
        }

        private bool regex_UsualDataStructures(string input)
        {
            bool _returnBool = false;
            _returnBool = Regex.IsMatch(input.Replace(" ", ""), @"\d{1,4}\/\d{1,4}\/\d{1,4}");
            _returnBool = Regex.IsMatch(input.Replace(" ", ""), @"\d{1,2}\/\d{1,2}");
            return _returnBool;
        }

        private bool AreAllColumnsEmpty(DataRow dr)
        {
            if (dr == null)
            {
                return true;
            }
            else
            {
                for (int i = 0; i < dr.ItemArray.Length; i++)
                {
                    if (!(String.IsNullOrEmpty(Regex.Replace(dr.ItemArray[i].ToString(), @"\s+", ""))))
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        private void dropEmptyRows(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (AreAllColumnsEmpty(dt.Rows[i]))
                {
                    dt.Rows.RemoveAt(i);
                }
            }
        }

        private List<IdValue> propertyLister_eQuery(object _eQueryObject, bool isErvenyessegNeeded)
        {
            List<IdValue> _tempList = new List<IdValue>();
            if (_eQueryObject == null)
            {
                return new List<IdValue>();
            }
            foreach (PropertyInfo _property in _eQueryObject.GetType().GetProperties())
            {
                if (_property.PropertyType.Name.ToLower() != "field") { continue; }
                IdValue _temp = new IdValue();
                string eQueryObjectName = getObjectType_eQuery(_eQueryObject);
                switch (eQueryObjectName.ToLower())
                {
                    case "erec_kuldkuldemenyeksearch":
                    case "erec_ugyugyiratoksearch":
                    case "erec_irairatoksearch":
                    case "erec_pldiratpeldanyoksearch":
                    case "krt_dokumentumoksearch":
                        if (_property.Name.ToLower().Contains("manual_"))
                        {
                            _temp.Id = Regex.Replace(_property.Name, "Manual_", "");
                        }
                        else
                        { _temp.Id = _property.Name; }
                        break;
                    default:
                        if (_property.Name.ToLower().Contains("manual_"))
                        { continue; }
                        _temp.Id = _property.Name;
                        break;
                }
                if (!isErvenyessegNeeded)
                {
                    if (_property.Name.ToLower() == "ervkezd" || _property.Name.ToLower() == "ervvege")
                    { continue; }
                }

                object _temporary = _property.GetValue(_eQueryObject, null);
                foreach (PropertyInfo _tempProperty in _temporary.GetType().GetProperties())
                {
                    if (_tempProperty.Name.ToLower() == "value")
                    {
                        _temp.Value = SQL_dataFunction_Converter(_tempProperty.GetValue(_temporary, null).ToString());
                    }
                    if (_tempProperty.Name.ToLower() == "operator")
                    {
                        _temp.Operator = _tempProperty.GetValue(_temporary, null).ToString();
                        if (string.IsNullOrEmpty(_temp.Operator))
                        {
                            _temp.Operator = "equal";
                        }
                    }
                    if (_tempProperty.Name.ToLower() == "valueto")
                    {
                        _temp.ValueTo = _tempProperty.GetValue(_temporary, null).ToString();
                    }
                }
                _tempList.Add(_temp);
            }
            List<IdValue> deleteList = new List<IdValue>();
            for (int i = 0; i < _tempList.Count; i++)
            {
                if (String.IsNullOrEmpty(_tempList[i].Value))
                {
                    deleteList.Add(_tempList[i]);
                }
            }
            foreach (IdValue delete in deleteList)
            {
                _tempList.Remove(delete);
            }
            return _tempList;
        }

        private string getObjectType_eQuery(object _TestingObject)
        {
            if (_TestingObject == null)
                return "";
            string _objectTypeName = _TestingObject.GetType().Name;
            /*string _objectTypeName = "";
			foreach (String _listItem in eQuery_ClassnameList)
			{
				 if (_listItem == _TestingObject.GetType().Name)
				 {
					  _objectTypeName = _listItem;
					  break;
				 }
			}*/

            return _objectTypeName;
        }

        private string SQL_dataFunction_Converter(string _rawString)
        {
            switch (_rawString.ToLower())
            {
                case "getdate()":
                    return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                default:
                    return _rawString;
            }
        }

        private string EncodeFileName(string fileName)
        {
            if (String.IsNullOrEmpty(fileName))
                return String.Empty;

            fileName = HttpUtility.UrlEncode(fileName);
            fileName = fileName.Replace("+", "%20");

            return fileName;
        }
        #endregion
    }
}
