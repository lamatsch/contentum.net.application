﻿using System;
using System.Collections.Generic;

namespace Contentum.eUtility.ExcelImport
{
    [Serializable]
    /// <summary>
    /// ExcelClassCimzettListaIktatas
    /// </summary>
    public class ExcelClassCimzettListaIktatas
    {
        public string Id { get; set; }
        public string Allapot { get; set; }
        //public string Peldany_helye { get; set; }
        public string Expedialas_Modja { get; set; }
        public string Cimzett_neve { get; set; }
        public string Cim_Tipus { get; set; }
        public string Hivatali_Kapus_Azonosito { get; set; }
        public string Cimzett_Orszag { get; set; }
        public string Cimzett_IRSZ { get; set; }
        public string Cimzett_Telepules { get; set; }
        public string Cimzett_Kozterulet_Neve { get; set; }
        public string Cimzett_Kozterulet_Tipusa { get; set; }
        public string Cimzett_Hazszam1 { get; set; }
        public string Cimzett_Hazszam2 { get; set; }
        public string Cimzett_Hazszam_Betujele { get; set; }
        public string Cimzett_Lepcsohaz { get; set; }
        public string Cimzett_Emelet { get; set; }
        public string Cimzett_Ajto { get; set; }
        public string Cimzett_Ajto_Betujele { get; set; }
        public ExcelClassCimzettListaIktatas()
        {
            Id = System.Guid.NewGuid().ToString();
        }
        public override bool Equals(object obj)
        {
            var iktatas = obj as ExcelClassCimzettListaIktatas;
            return iktatas != null &&
                   Id == iktatas.Id;
        }
        public bool IsValid()
        {
            if (string.IsNullOrEmpty(Expedialas_Modja))
                return false;
            if (string.IsNullOrEmpty(Cimzett_neve))
                return false;
            if (string.IsNullOrEmpty(Cim_Tipus))
                return false;
            if (Cim_Tipus == KodTarak.Cim_Tipus.Postai)
            {
                if (string.IsNullOrEmpty(Cimzett_IRSZ))
                    return false;
                if (string.IsNullOrEmpty(Cimzett_Telepules))
                    return false;
                if (string.IsNullOrEmpty(Cimzett_Kozterulet_Neve))
                    return false;
                if (string.IsNullOrEmpty(Cimzett_Kozterulet_Tipusa))
                    return false;
            }

            if (Expedialas_Modja == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu || Expedialas_Modja == KodTarak.KULDEMENY_KULDES_MODJA.Elhisz)
            {
                if (string.IsNullOrEmpty(Hivatali_Kapus_Azonosito))
                    return false;
            }
            return true;
        }
        public string GetAsXml()
        {
            return XmlFunction.ObjectToXml(this, false);
        }

        public override int GetHashCode()
        {
            return 2108858624 + EqualityComparer<string>.Default.GetHashCode(Id);
        }

        public string ToStringCimzett()
        {
            return string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}",
                string.IsNullOrEmpty(Cimzett_Orszag) ? string.Empty : Cimzett_Orszag + ", ",
                string.IsNullOrEmpty(Cimzett_IRSZ) ? string.Empty : Cimzett_IRSZ + ", ",
                string.IsNullOrEmpty(Cimzett_Telepules) ? string.Empty : Cimzett_Telepules + ", ",
                string.IsNullOrEmpty(Cimzett_Kozterulet_Neve) ? string.Empty : Cimzett_Kozterulet_Neve + " ",
                string.IsNullOrEmpty(Cimzett_Kozterulet_Tipusa) ? string.Empty : Cimzett_Kozterulet_Tipusa + " ",
                string.IsNullOrEmpty(Cimzett_Hazszam1) ? string.Empty : Cimzett_Hazszam1 + " ",
                string.IsNullOrEmpty(Cimzett_Hazszam2) ? string.Empty : Cimzett_Hazszam2 + " ",
                string.IsNullOrEmpty(Cimzett_Hazszam_Betujele) ? string.Empty : Cimzett_Hazszam_Betujele + " ",
                string.IsNullOrEmpty(Cimzett_Lepcsohaz) ? string.Empty : Cimzett_Lepcsohaz + " ",
                string.IsNullOrEmpty(Cimzett_Emelet) ? string.Empty : Cimzett_Emelet + " ",
                string.IsNullOrEmpty(Cimzett_Ajto) ? string.Empty : Cimzett_Ajto + " ",
                string.IsNullOrEmpty(Cimzett_Ajto_Betujele) ? string.Empty : Cimzett_Ajto_Betujele + " "
                );
        }
    }
}
