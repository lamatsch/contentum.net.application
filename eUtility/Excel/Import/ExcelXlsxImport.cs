﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;

namespace Contentum.eUtility.ExcelImport
{
    /// <summary>
    /// XLSX import to DataTable
    /// </summary>
    public static class ExcelXlsxImport
    {
        /// <summary>
        /// Excel file stream to DataTable
        /// </summary>
        /// <param name="type"></param>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        public static DataTable GetDataTableFromExcel(EnumExcelTypes type, System.IO.Stream fileStream)
        {
            DataTable table = null;
            DataRow row = null;
            try
            {
                table = InitDataTable(type);
                using (ExcelPackage xls = new ExcelPackage(fileStream))
                {
                    ExcelWorksheet ws = xls.Workbook.Worksheets[1];
                    int maxRows = ws.Dimension.End.Row;

                    for (int currentRow = 2; currentRow <= maxRows; currentRow++)
                    {
                        row = table.NewRow();
                        row[0] = Guid.NewGuid().ToString();
                        for (int currentColumn = 1; currentColumn < table.Columns.Count; currentColumn++)
                        {
                            row[currentColumn] = ws.GetValue<string>(currentRow, currentColumn);
                        }
                        if (!string.IsNullOrEmpty(row[2].ToString()) && !string.IsNullOrEmpty(row[3].ToString()) && !string.IsNullOrEmpty(row[4].ToString()))
                            table.Rows.Add(row);
                    }
                }
            }
            catch (Exception exc)
            {
                Logger.Error("ExcelXlsxImport.GetDataTableFromExcel hiba:", exc);
                return null;
            }
            return table;
        }
        /// <summary>
        /// Initialize DataTable from type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static DataTable InitDataTable(EnumExcelTypes type)
        {
            DataTable table = new DataTable();
            switch (type)
            {
                case EnumExcelTypes.CimzettListaIktatas:
                    List<string> columns = Reflection.GetAttributeNames(typeof(ExcelClassCimzettListaIktatas));
                    foreach (string val in columns)
                    {
                        table.Columns.Add(val.ToString(), typeof(string));
                    }
                    break;
                default:
                    break;
            }
            return table;
        }
    }
}
