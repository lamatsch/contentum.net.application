﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Contentum.eUtility.ExcelImport
{
    public static class HelperCimzettLista
    {
        /// <summary>
        /// Excel file stream to DataTable
        /// </summary>
        /// <param name="type"></param>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        public static List<ExcelClassCimzettListaIktatas> GetCimzettListaIktatasFromDataTable(DataTable table)
        {
            if (table == null || table.Rows.Count < 1)
                return null;

            List<ExcelClassCimzettListaIktatas> result = new List<ExcelClassCimzettListaIktatas>();
            ExcelClassCimzettListaIktatas tmp = null;
            //List<string> classColumns = Reflection.GetAttributeNames(typeof(ExcelClassCimzettListaIktatas));

            try
            {
                foreach (DataRow row in table.Rows)
                {
                    tmp = new ExcelClassCimzettListaIktatas();
                    tmp.Id = row["Id"].ToString();
                    tmp.Allapot = row["Allapot"].ToString();
                    tmp.Cimzett_Ajto = row["Cimzett_Ajto"].ToString();
                    tmp.Cimzett_Ajto_Betujele = row["Cimzett_Ajto_Betujele"].ToString();
                    tmp.Cimzett_Emelet = row["Cimzett_Emelet"].ToString();
                    tmp.Cimzett_Hazszam1 = row["Cimzett_Hazszam1"].ToString();
                    tmp.Cimzett_Hazszam2 = row["Cimzett_Hazszam2"].ToString();
                    tmp.Cimzett_Hazszam_Betujele = row["Cimzett_Hazszam_Betujele"].ToString();
                    tmp.Cimzett_IRSZ = row["Cimzett_IRSZ"].ToString();
                    tmp.Cimzett_Kozterulet_Neve = row["Cimzett_Kozterulet_Neve"].ToString();
                    tmp.Cimzett_Kozterulet_Tipusa = row["Cimzett_Kozterulet_Tipusa"].ToString();
                    tmp.Cimzett_Lepcsohaz = row["Cimzett_Lepcsohaz"].ToString();
                    tmp.Cimzett_neve = row["Cimzett_neve"].ToString();
                    tmp.Cimzett_Orszag = row["Cimzett_Orszag"].ToString();
                    tmp.Cimzett_Telepules = row["Cimzett_Telepules"].ToString();
                    tmp.Cim_Tipus = row["Cim_Tipus"].ToString();
                    tmp.Expedialas_Modja = row["Expedialas_Modja"].ToString();
                    tmp.Hivatali_Kapus_Azonosito = row["Hivatali_Kapus_Azonosito"].ToString();

                    result.Add(tmp);
                }
            }
            catch (Exception exc)
            {
                Logger.Error("ExcelXlsxImport.GetCimzettListaIktatasFromExcel hiba:", exc);
                return null;
            }
            return result;
        }
    }
}
