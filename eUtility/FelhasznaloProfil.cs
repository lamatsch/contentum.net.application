using System;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Web.UI;

namespace Contentum.eUtility
{
    public class FelhasznaloProfil
    {
        public FelhasznaloClass Felhasznalo = new FelhasznaloClass();
        public CsoportTagsagClass FelhasznaloCsoportTagsag = new CsoportTagsagClass();
        public FelhasznaloClass LoginUser = new FelhasznaloClass();
        public HelyettesitesClass Helyettesites = new HelyettesitesClass();

        //public FelhasznaloClass Helyettesitett = new FelhasznaloClass();

        //public static string FelhasznaloIdFromCache(Page page)
        //{
        //    FelhasznaloProfil f = (FelhasznaloProfil)page.Cache[Constants.FelhasznaloProfil];
        //    if (f == null)
        //    {
        //        FunctionRights.FelhasznaloProfilLoadToCache(page);
        //    }
        //    return f.Felhasznalo.Id;
        //}

        // Ha helyettes�t�s van, ez a helyettes�tett id-ja, egy�bk�nt a beloggolt felhaszn�l� id-ja
        public static string FelhasznaloId(Page page)
        {
            return UI.GetSession(page, Constants.FelhasznaloId);
        }

        public static string FelhasznaloId(HttpSessionState Session)
        {
            return UI.GetSession(Session, Constants.FelhasznaloId);
        }

        public static string LoginUserId(Page page)
        {
            return UI.GetSession(page, Constants.LoginUserId);
        }

        public static string LoginUserId(HttpSessionState Session)
        {
            return UI.GetSession(Session, Constants.LoginUserId);
        }

        public static string FelhasznaloCsoportTagId(Page page)
        {
            return UI.GetSession(page, Constants.FelhasznaloCsoportTagId);
        }

        public static string FelhasznaloCsoportTagId(HttpSessionState Session)
        {
            return UI.GetSession(Session, Constants.FelhasznaloCsoportTagId);
        }

        public static string HelyettesitesId(Page page)
        {
            return UI.GetSession(page, Constants.HelyettesitesId);
        }

        public static string HelyettesitesId(HttpSessionState Session)
        {
            return UI.GetSession(Session, Constants.HelyettesitesId);
        }
        
        public static string FelhasznaloSzerverzetId(Page page)
        {
            return UI.GetSession(page, Constants.FelhasznaloSzervezetId);
        }

        public static string FelhasznaloSzerverzetId(HttpSessionState Session)
        {
            return UI.GetSession(Session, Constants.FelhasznaloSzervezetId);
        }

        public static bool IsAdminInSzervezet(Page page)
        {
            return UI.GetSession(page, Constants.IsAdminInSzervezet) == "1";
        }

        //public static string CsoportId(Page page)
        //{
        //    // Jelenleg a CsoportId = FelhasznaloId !!
        //    return UI.GetSession(page, Constants.FelhasznaloId);
        //}

        //public static string HelyettesitettId(Page page)
        //{
        //    return UI.GetSession(page, Constants.HelyettesitettId);
        //}   

        /// <summary>
        /// Az aktu�lis felhaszn�l� vezet� vagy dolgoz� a csoportj�ban?
        /// Az aktu�lis pgae.Sessionben t�rolt felhaszn�l�profil alapj�n (alap�rtelmez�s: dolgoz�)
        /// </summary>
        /// <param name="page">KodTarak.CsoprttagsagTipus �rt�k</param>
        /// <returns></returns>
        public static string GetCsoportTagsagTipus_CurrentUser(Page page)
        {
            if (page.Session[Constants.FelhasznaloProfil] != null)
            {
                FelhasznaloProfil f = (FelhasznaloProfil)page.Session[Constants.FelhasznaloProfil];
                return f.FelhasznaloCsoportTagsag.Tipus;

            }
            else
            {
                return KodTarak.CsoprttagsagTipus.dolgozo;
            }
        }

        public static string ApplicationId(Page page)
        {
            if (page.Session[Constants.FelhasznaloProfil] == null) return "";

            return (page.Session[Constants.FelhasznaloProfil] as FelhasznaloProfil).Application.Id;
        }

        public static string ApplicationId(HttpSessionState Session)
        {
            if (Session[Constants.FelhasznaloProfil] == null) return "";

            return (Session[Constants.FelhasznaloProfil] as FelhasznaloProfil).Application.Id;
        }

        public static string OrgId(HttpSessionState Session)
        {
            FelhasznaloProfil f = FelhasznaloProfil.GetCurrent(Session);
            if (f != null)
            {
                if (f.Felhasznalo != null)
                {
                    if (f.Felhasznalo.Org != null)
                    {
                        return f.Felhasznalo.Org.Id;
                    }
                }
            }

            return String.Empty;
        }

        public static string OrgId(Page page)
        {
            return OrgId(page.Session);
        }

        // Org.Kod lek�rdez�se
        // Org-t�l f�gg� megjelen�t�si, m�k�d�si elemek elk�l�n�t�s�re
        // CR 3054 
        public static string OrgKod(HttpSessionState Session)
        {
            FelhasznaloProfil f = FelhasznaloProfil.GetCurrent(Session);
            if (f != null)
            {
                if (f.Felhasznalo != null)
                {
                    if (f.Felhasznalo.Org != null)
                    {
                        return f.Felhasznalo.Org.Kod;
                    }
                }
            }

            return String.Empty;
        }

        // Org.Kod lek�rdez�se
        // Org-t�l f�gg� megjelen�t�si, m�k�d�si elemek elk�l�n�t�s�re
        // CR 3054 
        public static string OrgKod(Page page)
        {
            return OrgKod(page.Session);
        }

        /// <summary>
        /// Felhaszn�l� utols� bejelentkez�s�nek id�pontja ha le lett k�rve, egy�bk�nt DateTime.Now.ToString()
        /// </summary>        
        public static string GetLastLoginTime(Page page)
        {
            if (page.Session[Constants.SessionNames.LastLoginTime] != null
                && !string.IsNullOrEmpty(page.Session[Constants.SessionNames.LastLoginTime].ToString()))
            {
                return page.Session[Constants.SessionNames.LastLoginTime].ToString();
            }
            else
            {
                return DateTime.Now.ToString();
            }
        }

        public class FelhasznaloClass
        {
            public _Org Org = new _Org();
            public class _Org
            {
                private string _Id = null;
                public string Id
                {
                    get { return _Id; }
                    set { _Id = value; }
                }

                private string _Nev = null;
                public string Nev
                {
                    get { return _Nev; }
                    set { _Nev = value; }
                }

                // CR#2013 (EB 2009.10.12): a felhaszn�l�i fel�let elt�r� elemeinek kezel�s�hez
                private string _Kod = null;
                public string Kod
                {
                    get { return _Kod; }
                    set { _Kod = value; }
                }
            }

            private string _Id = null;
            public string Id
            {
                get { return _Id; }
                set { _Id = value; }
            }

            private string _Nev = null;
            public string Nev
            {
                get { return _Nev; }
                set { _Nev = value; }
            }

        }

        public class CsoportTagsagClass
        {
            private string _Id = null;
            public string Id
            {
                get { return _Id; }
                set { _Id = value; }
            }

            private string _Csoport_Id = null;
            public string Csoport_Id
            {
                get { return _Csoport_Id; }
                set { _Csoport_Id = value; }
            }

            private string _Csoport_Nev = null;
            public string Csoport_Nev
            {
                get { return _Csoport_Nev; }
                set { _Csoport_Nev = value; }
            }

            private string _Tipus = null;
            public string Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }
            }
        }

        public class HelyettesitesClass
        {
            private string _Id = null;
            public string Id
            {
                get { return _Id; }
                set { _Id = value; }
            }

            private string _HelyettesitesMod = null;
            public string HelyettesitesMod
            {
                get { return _HelyettesitesMod; }
                set { _HelyettesitesMod = value; }
            }

            private string _Helyettesitett_Id = null;
            public string Helyettesitett_Id
            {
                get { return _Helyettesitett_Id; }
                set { _Helyettesitett_Id = value; }
            }

            private string _CsoportTag_ID_helyettesitett = null;
            public string CsoportTag_ID_helyettesitett
            {
                get { return _CsoportTag_ID_helyettesitett; }
                set { _CsoportTag_ID_helyettesitett = value; }
            }

            private string _Helyettesito_Id = null;
            public string Helyettesito_Id
            {
                get { return _Helyettesito_Id; }
                set { _Helyettesito_Id = value; }
            }
        }

        public _Application Application = new _Application();

        public class _Application
        {
            private string _Id = null;
            public string Id
            {
                get { return _Id; }
                set { _Id = value; }
            }

            private string _Nev = null;
            public string Nev
            {
                get { return _Nev; }
                set { _Nev = value; }
            }
        }

        public Dictionary<String, Szerepkor> Szerepkorok = new Dictionary<String, Szerepkor>();

        public class Szerepkor
        {
            public Szerepkor()
            {
            }

            public Szerepkor(string Id, string Nev)
            {
                this._Id = Id;
                this._Nev = Nev;
            }

            private string _Id = null;
            public string Id
            {
                get { return _Id; }
                set { _Id = value; }
            }

            private string _Nev = null;
            public string Nev
            {
                get { return _Nev; }
                set { _Nev = value; }
            }
        }

        public Dictionary<String, Funkcio> Funkciok = new Dictionary<String, Funkcio>();

        private DateTime timeStamp_FunkciokRefresh;
        /// <summary>
        /// Id�b�lyeg: mikor lett friss�tve az el�rhet� funkci�k list�ja
        /// </summary>
        public DateTime TimeStamp_FunkciokRefresh
        {
            get { return timeStamp_FunkciokRefresh; }
            set { timeStamp_FunkciokRefresh = value; }
        }
	

        public class Funkcio
        {
            public Funkcio()
            {
            }

            public Funkcio(string Id, string Nev)
            {
                this._Id = Id;
                this._Nev = Nev;
            }

            private string _Id = null;
            public string Id
            {
                get { return _Id; }
                set { _Id = value; }
            }

            private string _Nev = null;
            public string Nev
            {
                get { return _Nev; }
                set { _Nev = value; }
            }
        }


        private string lastLoginTime;
        public string  LastLoginTime 
        {
            get
            {
                return lastLoginTime;
            }
            set
            {
                lastLoginTime = value;
            }
        }

        private string loginType;

        public string LoginType
        {
            get { return loginType; }
            set { loginType = value; }
        }


        public static FelhasznaloProfil GetCurrent(HttpSessionState Session)
        {
            if (Session[Constants.FelhasznaloProfil] != null)
                return Session[Constants.FelhasznaloProfil] as FelhasznaloProfil;

            return null;
        }
        public static FelhasznaloProfil GetCurrent(Page page)
        {
            return GetCurrent(page.Session);
        }

        public static string ConvertToQueryString(Page page)
        {
            string query = String.Empty;
            if (page != null)
            {
                string loginType = UI.GetSession(page, Constants.LoginType);
                if (loginType != "Windows")
                {
                    query = QueryStringVars.LoggedLoginType + "=" + loginType;
                }
                else
                {
                    if (!Authentication.IsSimpleLogin(page))
                    {
                        query = QueryStringVars.LoggedFelhasznaloId + "=" + FelhasznaloProfil.FelhasznaloId(page);
                        if (!String.IsNullOrEmpty(FelhasznaloProfil.FelhasznaloCsoportTagId(page)))
                            query += "&" + QueryStringVars.LoggedCsoporttagId + "=" + FelhasznaloProfil.FelhasznaloCsoportTagId(page);
                        if (!String.IsNullOrEmpty(FelhasznaloProfil.HelyettesitesId(page)))
                            query += "&" + QueryStringVars.LoggedHelyettesitesId + "=" + FelhasznaloProfil.HelyettesitesId(page);
                    }
                }
            }

            return query;
        }

        public static FelhasznaloProfil GetLastLoginData(Page page)
        {
            string felhasznaloId = String.Empty;
            string szervezetId = String.Empty;
            string helyettesitesId = String.Empty;
            string csoporttagId = String.Empty;
            string loginType = String.Empty;

            if (!page.IsPostBack)
            {
                felhasznaloId = page.Request.QueryString.Get(QueryStringVars.LoggedFelhasznaloId);
                szervezetId = page.Request.QueryString.Get(QueryStringVars.LoggedSzervezetId);
                helyettesitesId = page.Request.QueryString.Get(QueryStringVars.HelyettesitesId);
                csoporttagId = page.Request.QueryString.Get(QueryStringVars.LoggedCsoporttagId);
                loginType = page.Request.QueryString.Get(QueryStringVars.LoggedLoginType);
            }
            else
            {
                if (page.Master != null && page.Master is ILastLoginData)
                {
                    ILastLoginData loginData = page.Master as ILastLoginData;
                    felhasznaloId = page.Request.Params[loginData.FelhasznaloHiddenFieldUniqueId];
                    szervezetId = page.Request.Params[loginData.SzervezetHiddenFieldUniqueId];
                    helyettesitesId = page.Request.Params[loginData.HelyettesitesHiddenFieldUniqueId];
                    csoporttagId = page.Request.Params[loginData.FelhasznaloCsoporttagsagHiddenFieldUniqueId];
                    loginType = page.Request.Params[loginData.LoginTypeHiddenFieldUniqueId];
                }
            }

            FelhasznaloProfil fp = new FelhasznaloProfil();
            fp.Felhasznalo.Id = felhasznaloId;
            Logger.Debug("El�z� login FelhasznaloId-ja: " + felhasznaloId);
            fp.FelhasznaloCsoportTagsag.Csoport_Id = szervezetId;
            Logger.Debug("El�z� login SzervezetId-ja: " + szervezetId);
            fp.Helyettesites.Id = helyettesitesId;
            Logger.Debug("El�z� login HelyettesitesId-ja: " + helyettesitesId);
            fp.FelhasznaloCsoportTagsag.Id = csoporttagId;
            Logger.Debug("El�z� login CsoporttagId-ja: " + csoporttagId);
            fp.LoginType = loginType;
            Logger.Debug("El�z� login t�pusa: " + loginType);
            return fp;
        }

    }

}
