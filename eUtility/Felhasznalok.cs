﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Data;
using Contentum.eBusinessDocuments;
using System.Collections;
using System.Collections.Specialized;
using Contentum.eAdmin.Service;

namespace Contentum.eUtility
{
    public class FelhasznaloNevek_Cache
    {
        private static readonly object _sync = new object();

        public static string GetFelhasznaloNevFromCache(string Felhasznalo_Id, Page page)
        {
            return GetFelhasznaloNevFromCache(Felhasznalo_Id, UI.SetExecParamDefault(page), page.Cache);
        }

        public static string GetFelhasznaloNevFromCache(string Felhasznalo_Id, ExecParam execParam, System.Web.Caching.Cache Cache)
        {
            Logger.Debug("GetFelhasznaloNevFromCache kezdete");

            if (String.IsNullOrEmpty(Felhasznalo_Id)) { Logger.Debug("String.IsNullOrEmpty(Felhasznalo_Id)"); return String.Empty; }

            if (String.IsNullOrEmpty(execParam.Org_Id))
            {
                Logger.Debug("String.IsNullOrEmpty(execParam.Org_Id)");
                string orgId = FelhasznaloNevek_Cache.GetOrgByFelhasznalo(execParam.Felhasznalo_Id, Cache);
                Logger.Debug("execParam.Org_Id=" + orgId ?? "NULL");
                execParam.Org_Id = orgId;
            }

            OrgSeperatedDictionary<string, string> FelhasznaloNevDictionaryCache =
                (OrgSeperatedDictionary<string, string>)Cache[Constants.FelhasznaloNevek];


            if (FelhasznaloNevDictionaryCache == null)
            {
                // Cache időtartamra ugyanazt használhatjuk, mint a csoportoknál
                int CsoportNevekCache_RefreshPeriod_Minute =
                    Rendszerparameterek.GetInt(execParam, Rendszerparameterek.CsoportNevekCache_RefreshPeriod_Minute);
                if (CsoportNevekCache_RefreshPeriod_Minute == 0)
                {
                    CsoportNevekCache_RefreshPeriod_Minute = 600;
                }
                lock (_sync)
                {
                    FelhasznaloNevDictionaryCache = (OrgSeperatedDictionary<string, string>)Cache[Constants.FelhasznaloNevek];
                    if (FelhasznaloNevDictionaryCache == null)
                    {
                        // Dictionary felvétele a Cache-be:
                        FelhasznaloNevDictionaryCache = new OrgSeperatedDictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

                        Cache.Insert(Constants.FelhasznaloNevek, FelhasznaloNevDictionaryCache, null
                                    , DateTime.Now.AddMinutes(CsoportNevekCache_RefreshPeriod_Minute), System.Web.Caching.Cache.NoSlidingExpiration
                                    , System.Web.Caching.CacheItemPriority.NotRemovable, null);

                    }
                }

            }

            //ExecParam execParam = UI.SetExecParamDefault(parentPage);
            Dictionary<string, string> FelhasznaloNevDictionary = FelhasznaloNevDictionaryCache.Get(execParam.Org_Id);

            if (FelhasznaloNevDictionary.Count == 0)
            {
                lock (_sync)
                {
                    if (FelhasznaloNevDictionary.Count == 0)
                    {
                        // Felhasználónevek lekérése az adatbázisból (Felhasználók GETALL)
                        Contentum.eAdmin.Service.KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

                        Contentum.eQuery.BusinessDocuments.KRT_FelhasznalokSearch search = new Contentum.eQuery.BusinessDocuments.KRT_FelhasznalokSearch();

                        // eredményhalmazt azért korlátozzuk (ugyanaz az érték mehet, mint a csoport cache-nél)
                        int CsoportNevekCache_MaxRowsFromDBToCache =
                            Rendszerparameterek.GetInt(execParam, Rendszerparameterek.CsoportNevekCache_MaxRowsFromDBToCache);
                        // ha valamiért nem kapjuk meg, default érték:
                        if (CsoportNevekCache_MaxRowsFromDBToCache == 0)
                        {
                            CsoportNevekCache_MaxRowsFromDBToCache = 10000;
                        }

                        search.TopRow = CsoportNevekCache_MaxRowsFromDBToCache;

                        Contentum.eBusinessDocuments.Result result = service.GetAll(execParam, search);
                        if (result.IsError)
                        {
                            // hiba:
                            Logger.Error("GetFelhasznaloNevFromCache hiba: KRT_FelhasznalokService.GetAll", execParam, result);
                            return String.Empty;
                        }
                        else
                        {
                            // Dictionary feltöltése:
                            try
                            {
                                foreach (DataRow row in result.Ds.Tables[0].Rows)
                                {
                                    string row_Id = row["Id"].ToString();
                                    string row_Nev = row["Nev"].ToString();

                                    if (!FelhasznaloNevDictionary.ContainsKey(row_Id))
                                    {
                                        FelhasznaloNevDictionary.Add(row_Id, row_Nev);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                // hiba:
                                Logger.Error("GetFelhasznaloNevFromCache Dictionary feltoltése hiba", ex);
                                return String.Empty;
                            }
                        }
                    }
                }
            }

            // Kért név visszaadása:
            if (FelhasznaloNevDictionary.ContainsKey(Felhasznalo_Id))
            {
                // Dictionary-ból visszaadjuk a felhasználónevet:
                return FelhasznaloNevDictionary[Felhasznalo_Id];
            }
            else
            {

                // nincs benne a Dictionary-ben, ki kell venni adatbázisból:

                Contentum.eAdmin.Service.KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                execParam.Record_Id = Felhasznalo_Id;

                Contentum.eBusinessDocuments.Result result = service.Get(execParam);

                if (!result.IsError && result.Record != null)
                {
                    Contentum.eBusinessDocuments.KRT_Felhasznalok krt_felhasznalok = (Contentum.eBusinessDocuments.KRT_Felhasznalok)result.Record;
                    string felhasznaloNev = krt_felhasznalok.Nev;
                    // betesszük a Dictionary-be:
                    try
                    {
                        if (!FelhasznaloNevDictionary.ContainsKey(Felhasznalo_Id))
                        {
                            lock (_sync)
                            {
                                if (!FelhasznaloNevDictionary.ContainsKey(Felhasznalo_Id))
                                {
                                    FelhasznaloNevDictionary.Add(Felhasznalo_Id, felhasznaloNev);
                                }
                            }
                        }
                    }
                    catch (ArgumentException aex)
                    {
                        // már benne van a Dictionary-ben (esetleg valaki közben betette)
                        Logger.Error("FelhasznaloNevDictionary.Add hiba", aex);
                    }
                    return felhasznaloNev;
                }
                else
                {
                    // hiba:
                    Logger.Error(String.Format("GetFelhasznaloNevFromCache: KRT_FelhasznalokService.Get hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
                    return String.Empty;
                }
            }

        }

        public static string GetOrgByFelhasznalo(string Felhasznalo_Id, System.Web.Caching.Cache Cache)
        {
            Logger.Debug("GetOrgByFelhasznalo kezdete: " + Felhasznalo_Id ?? "NULL");

            if (String.IsNullOrEmpty(Felhasznalo_Id)) { Logger.Debug("String.IsNullOrEmpty(Felhasznalo_Id)"); return String.Empty; }

            OrgSeperatedDictionary<string, string> FelhasznaloNevDictionaryCache =
                (OrgSeperatedDictionary<string, string>)Cache[Constants.FelhasznaloNevek];

            string org_id = String.Empty;

            if (FelhasznaloNevDictionaryCache != null)
            {
                org_id = FelhasznaloNevDictionaryCache.GetOrg_Id(Felhasznalo_Id);
            }

            if (String.IsNullOrEmpty(org_id))
            {
                Logger.Debug("GetOrgByFelhasznalo not in cache");
                KRT_Felhasznalok felhasznalo = Get(Felhasznalo_Id);

                if (felhasznalo == null)
                {
                    Logger.Error("felhasznalo == null");
                }
                else if (String.IsNullOrEmpty(felhasznalo.Org))
                {
                    Logger.Error("String.IsNullOrEmpty(felhasznalo.Org)");
                }
                else
                {

                    org_id = felhasznalo.Org;

                    //elmentés cache-be
                    ExecParam xpm = new ExecParam();
                    xpm.Felhasznalo_Id = Felhasznalo_Id;
                    xpm.Org_Id = org_id;
                    string felhasznaloNev = GetFelhasznaloNevFromCache(Felhasznalo_Id, xpm, Cache);
                }

            }

            Logger.Debug("GetOrgByFelhasznalo vege: " + org_id);
            return org_id;
        }

        private static KRT_Felhasznalok Get(string Felhasznalo_Id)
        {
            Logger.Debug("FelhasznaloNevek_Cache.Get: Felhasznalo_Id:" + Felhasznalo_Id ?? "NULL");

            Arguments args = new Arguments();
            args.Add(new Argument("Felhasználó azonosító", Felhasznalo_Id, ArgumentTypes.Guid));
            args.ValidateArguments();

            ExecParam xpm = new ExecParam();
            xpm.Felhasznalo_Id = Felhasznalo_Id;
            xpm.Record_Id = Felhasznalo_Id;

            KRT_FelhasznalokService svcFelhasznalok = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            Result res = svcFelhasznalok.Get(xpm);

            if (res.IsError)
            {
                Logger.Error(String.Format("KRT_FelhasznalokService.Get hiba: {0},{1}", res.ErrorCode, res.ErrorMessage));
                throw new ResultException(res);
            }

            return res.Record as KRT_Felhasznalok;
        }
    }

    public class Felhasznalok
    {
        public static string GetCsoportNev(ExecParam execParam, KRT_Felhasznalok krtFelhasznalo)
        {
            bool emailDisabled = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.CSOPORT_NEV_WITH_EMAIL_DISABLED, false);

            if (emailDisabled)
            {
                return krtFelhasznalo.Nev;
            }
            else
            {
                return GetFelhasznaloNevWithEmail(krtFelhasznalo);
            }
        }
        public static string GetFelhasznaloNevWithEmail(DataRow rowFelhasznalo)
        {
            KRT_Felhasznalok krtFelhasznalo = new KRT_Felhasznalok();
            krtFelhasznalo.Nev = rowFelhasznalo["Nev"].ToString();
            krtFelhasznalo.EMail = rowFelhasznalo["EMail"].ToString();
            return GetFelhasznaloNevWithEmail(krtFelhasznalo);
        }
        public static string GetFelhasznaloNevWithEmail(KRT_Felhasznalok krtFelhasznalo)
        {
            if (String.IsNullOrEmpty(krtFelhasznalo.EMail))
                return krtFelhasznalo.Nev;

            return String.Format(krtFelhasznalo.Nev + " ({0})", krtFelhasznalo.EMail);
        }
    }
}
