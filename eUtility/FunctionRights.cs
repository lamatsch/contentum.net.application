using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Contentum.eBusinessDocuments;

namespace Contentum.eUtility
{
    public class FunctionRights
    {
        public static DateTime GetFunkciokLastUpdatedTimeFromCache(Page page)
        {
            if (page == null || page.Cache["FunkciokLastUpdatedTime"] == null)
            {
                DateTime newTimeStamp = DateTime.Now;
                page.Cache.Insert("FunkciokLastUpdatedTime", newTimeStamp, null
                    , DateTime.Now.AddHours(2), System.Web.Caching.Cache.NoSlidingExpiration
                    , System.Web.Caching.CacheItemPriority.NotRemovable, null);
                return newTimeStamp;
            }
            else
            {
                DateTime timeStamp = (DateTime)page.Cache["FunkciokLastUpdatedTime"];
                return timeStamp;
            }
        }

        public static void RefreshFunkciokLastUpdatedTimeInCache(Page page)
        {
            if (page == null) return;
            DateTime newTimeStamp = DateTime.Now;
            if (page.Cache["FunkciokLastUpdatedTime"] != null)
            {
                page.Cache["FunkciokLastUpdatedTime"] = newTimeStamp;
            }
            else
            {
                page.Cache.Insert("FunkciokLastUpdatedTime", newTimeStamp, null
                    , DateTime.Now.AddHours(2), System.Web.Caching.Cache.NoSlidingExpiration
                    , System.Web.Caching.CacheItemPriority.NotRemovable, null);
            }
        }

        public static bool FelhasznaloProfilLoadToCache(Page ParentPage)
        {
            if (ParentPage == null) return false;
            try
            {
                FelhasznaloProfil FelhasznaloProfil = new FelhasznaloProfil();
                // ide jonne a service, ami visszater a feltoltott felhasznaloi profillal.

                string felhasznaloId = UI.GetSession(ParentPage, Constants.FelhasznaloId);
                string loginUserId = UI.GetSession(ParentPage, Constants.LoginUserId);

                if (String.IsNullOrEmpty(felhasznaloId))
                {
                    Authentication.CheckLogin(ParentPage);
                    felhasznaloId = UI.GetSession(ParentPage, Constants.FelhasznaloId);
                }
                else
                {

                    //string helyettesitettId = UI.GetSession(ParentPage, Constants.HelyettesitettId);
                    ////ParentPage.Session.Remove(Constants.HelyettesitettId);

                    //FelhasznaloProfil = SetUserProfil(ParentPage, felhasznaloId, loginUserId  /*, helyettesitettId*/);

                    string helyettesitesId = UI.GetSession(ParentPage, Constants.HelyettesitesId);
                    string csoporttagId = UI.GetSession(ParentPage, Constants.FelhasznaloCsoportTagId);

                    if (String.IsNullOrEmpty(csoporttagId))
                    {
                        Logger.Error("FelhasznaloProfilLoadToCache: FelhasznaloCsoportTagId not set.");
                        return false;
                    }

                    FelhasznaloProfil = SetUserProfil(ParentPage, felhasznaloId, loginUserId, csoporttagId, helyettesitesId);
                    ParentPage.Session[Constants.FelhasznaloProfil] = FelhasznaloProfil;

                }
            }
            catch (Exception e)
            {
                throw (e);
                //return false;
            }
            return true;
        }

        public static bool FelhasznaloProfilIsCached(Page ParentPage)
        {
            return FelhasznaloProfilIsCached(ParentPage, true);
        }

        public static bool FelhasznaloProfilIsCached(Page ParentPage, bool LoadToCache)
        {
            if (ParentPage == null) return false;

            if (ParentPage.Session[Constants.FelhasznaloProfil] == null)
            {
                if (LoadToCache)
                {
                    return FelhasznaloProfilLoadToCache(ParentPage);
                }

                return false;
            }

            return true;
        }
        
        private static FelhasznaloProfil SetUserProfil(Page ParentPage,String felhasznaloId, String loginUserId, String csoporttagId, String helyettesitesId)
        {
            if (String.IsNullOrEmpty(felhasznaloId)) return null;
            if (String.IsNullOrEmpty(csoporttagId)) return null;
            FelhasznaloProfil _FelhasznaloProfil = new FelhasznaloProfil();
            ParentPage.Session[Constants.FelhasznaloProfil] = _FelhasznaloProfil;

            _FelhasznaloProfil.LoginType = UI.GetSession(ParentPage, Constants.LoginType);

            #region Felhasznalo GET

            Contentum.eAdmin.Service.KRT_FelhasznalokService _KRT_FelhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = UI.SetExecParamDefault(ParentPage,new ExecParam());
            ExecParam.Felhasznalo_Id = felhasznaloId;
            ExecParam.Record_Id = felhasznaloId;

            Contentum.eBusinessDocuments.Result _ret = _KRT_FelhasznalokService.Get(ExecParam);
            if (_ret.Record == null) return null;
            Contentum.eBusinessDocuments.KRT_Felhasznalok _KRT_Felhasznalok = (Contentum.eBusinessDocuments.KRT_Felhasznalok)_ret.Record;

            #endregion

            _FelhasznaloProfil.Felhasznalo.Id = _KRT_Felhasznalok.Id;
            _FelhasznaloProfil.Felhasznalo.Nev = _KRT_Felhasznalok.Nev;

            if (loginUserId == felhasznaloId)
            {
                _FelhasznaloProfil.LoginUser.Id = _FelhasznaloProfil.Felhasznalo.Id;
                _FelhasznaloProfil.LoginUser.Nev = _FelhasznaloProfil.Felhasznalo.Nev;
            }
            else
            {
                #region LoginUser GET

                ExecParam execParam_loginUserGet = UI.SetExecParamDefault(ParentPage, new ExecParam());
                execParam_loginUserGet.Felhasznalo_Id = felhasznaloId;
                execParam_loginUserGet.Record_Id = loginUserId;

                Result result_loginUserGet = _KRT_FelhasznalokService.Get(execParam_loginUserGet);
                if (result_loginUserGet.Record == null) return null;
                KRT_Felhasznalok krt_Felhasznalo_loginUser = (KRT_Felhasznalok) result_loginUserGet.Record;

                _FelhasznaloProfil.LoginUser.Id = loginUserId;
                _FelhasznaloProfil.LoginUser.Nev = krt_Felhasznalo_loginUser.Nev;

                #endregion
            }

            _FelhasznaloProfil.Felhasznalo.Org.Id = _KRT_Felhasznalok.Org;
            if (!String.IsNullOrEmpty(_FelhasznaloProfil.Felhasznalo.Org.Id))
            {
                #region Org GET (Kell?)

                Contentum.eAdmin.Service.KRT_OrgokService _KRT_OrgokService = eAdminService.ServiceFactory.GetKRT_OrgokService();
                //ExecParam.Felhasznalo_Id = LoginId;

                ExecParam.Record_Id = _FelhasznaloProfil.Felhasznalo.Org.Id;
                _ret = _KRT_OrgokService.Get(ExecParam);
                Contentum.eBusinessDocuments.KRT_Orgok _KRT_Orgok = (Contentum.eBusinessDocuments.KRT_Orgok)_ret.Record;
                _FelhasznaloProfil.Felhasznalo.Org.Nev = _KRT_Orgok.Nev;

                // CR#2013 (EB 2009.10.12): a felhaszn�l�i fel�let elt�r� elemeinek kezel�s�hez
                _FelhasznaloProfil.Felhasznalo.Org.Kod = _KRT_Orgok.Kod;
                #endregion
            }

            try
            {
                #region Application GET (Kell?)

                string ApplicationName = UI.GetAppSetting(Constants.ApplicationName);

                ExecParam = UI.SetExecParamDefault(ParentPage,new ExecParam());
                ExecParam.Felhasznalo_Id = felhasznaloId;

                Contentum.eAdmin.Service.KRT_AlkalmazasokService alkalmazasokservice = eAdminService.ServiceFactory.GetKRT_AlkalmazasokService();

                //Contentum.eBusinessDocuments.KRT_Alkalmazasok = new Contentum.eBusinessDocuments.KRT_Alkalmazasok();

                Contentum.eQuery.BusinessDocuments.KRT_AlkalmazasokSearch alkalmazasoksearch = new Contentum.eQuery.BusinessDocuments.KRT_AlkalmazasokSearch();
                alkalmazasoksearch.Kod.Value = ApplicationName;
                alkalmazasoksearch.Kod.Operator = Contentum.eQuery.Query.Operators.equals;

                Contentum.eBusinessDocuments.Result _res = alkalmazasokservice.GetAll(ExecParam, alkalmazasoksearch);

                if (String.IsNullOrEmpty(_res.ErrorCode) && _res.Ds != null && _res.Ds.Tables[0].Rows.Count > 0)
                {
                    _FelhasznaloProfil.Application.Id = _res.Ds.Tables[0].Rows[0]["Id"].ToString();
                    _FelhasznaloProfil.Application.Nev = _res.Ds.Tables[0].Rows[0]["Nev"].ToString();
                }
                else
                {
                }

                #endregion
            }
            catch (Exception e)
            {
                throw (e);
            }

            //Helyettesites kezelese:
            if (!String.IsNullOrEmpty(helyettesitesId))
            {
                ExecParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
                ExecParam.Felhasznalo_Id = felhasznaloId;
                ExecParam.Record_Id = helyettesitesId;
                Contentum.eAdmin.Service.KRT_HelyettesitesekService helyettesitesekservice = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();

                //Contentum.eQuery.BusinessDocuments.KRT_HelyettesitesekSearch helyettesiteseksearch = new Contentum.eQuery.BusinessDocuments.KRT_HelyettesitesekSearch();
                Logger.Debug("SetUserProfil helyettes�t�s id: " + helyettesitesId);
                _ret = helyettesitesekservice.Get(ExecParam);
                if (_ret.Record == null)
                {
                    Logger.Debug("Hiba a helyettes�t�s lek�r�se sor�n!");
                    return null;
                }

                Contentum.eBusinessDocuments.KRT_Helyettesitesek _KRT_Helyettesitesek = (Contentum.eBusinessDocuments.KRT_Helyettesitesek)_ret.Record;

                _FelhasznaloProfil.Helyettesites.Id = _KRT_Helyettesitesek.Id;
                string helyettesitesmod = _KRT_Helyettesitesek.HelyettesitesMod;
                // r�gi rekordok miatt
                if (String.IsNullOrEmpty(helyettesitesmod))
                {
                    helyettesitesmod = KodTarak.HELYETTESITES_MOD.Helyettesites;
                }
                _FelhasznaloProfil.Helyettesites.HelyettesitesMod = helyettesitesmod;
                _FelhasznaloProfil.Helyettesites.Helyettesitett_Id = _KRT_Helyettesitesek.Felhasznalo_ID_helyettesitett;
                _FelhasznaloProfil.Helyettesites.CsoportTag_ID_helyettesitett = _KRT_Helyettesitesek.CsoportTag_ID_helyettesitett;
                _FelhasznaloProfil.Helyettesites.Helyettesito_Id = _KRT_Helyettesitesek.Felhasznalo_ID_helyettesito;
            }

            if (!String.IsNullOrEmpty(csoporttagId))
            {
                Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoporttagok = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                ExecParam = UI.SetExecParamDefault(ParentPage, new ExecParam());

                Contentum.eQuery.BusinessDocuments.KRT_CsoportTagokSearch search_csoporttagok = new Contentum.eQuery.BusinessDocuments.KRT_CsoportTagokSearch();

                search_csoporttagok.Id.Value = csoporttagId;
                search_csoporttagok.Id.Operator = Contentum.eQuery.Query.Operators.equals;

                _ret = service_csoporttagok.GetAll(ExecParam, search_csoporttagok);
                if (!String.IsNullOrEmpty(_ret.ErrorCode) || _ret.Ds.Tables[0].Rows.Count == 0) return null;

                _FelhasznaloProfil.FelhasznaloCsoportTagsag.Id = csoporttagId;
                string csoport_id = _ret.Ds.Tables[0].Rows[0]["Csoport_Id"].ToString();
                _FelhasznaloProfil.FelhasznaloCsoportTagsag.Csoport_Id = csoport_id;
                _FelhasznaloProfil.FelhasznaloCsoportTagsag.Csoport_Nev = CsoportNevek_Cache.GetCsoportNevFromCache(csoport_id, ParentPage);
                _FelhasznaloProfil.FelhasznaloCsoportTagsag.Tipus = _ret.Ds.Tables[0].Rows[0]["Tipus"].ToString();

            }

            FunctionRights.UpdateSzerepkorFunkcioOnFelhasznaloProfil(ParentPage,_FelhasznaloProfil);
            
            return _FelhasznaloProfil;
        }


        public static void UpdateSzerepkorFunkcioOnFelhasznaloProfil(Page ParentPage, FelhasznaloProfil felhasznaloProfil)
        {
            if (felhasznaloProfil != null)
            {
                felhasznaloProfil.Szerepkorok.Clear();
                felhasznaloProfil.Funkciok.Clear();

                // ha nincs megadva a CsoportTag_Id, akkor val�sz�n�leg a Login-oldalt�l visszafel�
                // lapozott a (t�bb csoportba is tartoz�) felhaszn�l�, mik�zben nem volt m�g bel�ptetve
                if (String.IsNullOrEmpty(felhasznaloProfil.FelhasznaloCsoportTagsag.Id))
                {
                    Logger.Warn("A felhaszn�l� aktu�lis csoporttags�ga nem azonos�that�. A felhaszn�l�hoz nem hat�rozhat�k meg a szerepk�r�k �s funkci�jogok.");
                    return;
                }

                Contentum.eBusinessDocuments.KRT_Felhasznalok _KRT_Felhasznalok = new Contentum.eBusinessDocuments.KRT_Felhasznalok();
                Contentum.eBusinessDocuments.ExecParam ExecParam = UI.SetExecParamDefault(ParentPage, new ExecParam());

                Contentum.eAdmin.Service.KRT_SzerepkorokService _KRT_SzerepkorokService = eAdminService.ServiceFactory.GetKRT_SzerepkorokService();
                Contentum.eQuery.BusinessDocuments.KRT_Felhasznalo_SzerepkorSearch _KRT_Felhasznalo_SzerepkorSearch =
                    new Contentum.eQuery.BusinessDocuments.KRT_Felhasznalo_SzerepkorSearch();
                Contentum.eBusinessDocuments.Result _ret = null;
                // Mindig a Felhasznalo -ra k�rj�k le a funkci�jogosults�gokat
                // (Helyettes�t�sn�l is a Felhaszn�l� lesz a helyettes�tett)

                // kiv�ve a megb�z�sn�l
                if (felhasznaloProfil.Helyettesites.HelyettesitesMod == KodTarak.HELYETTESITES_MOD.Megbizas)
                {
                    _KRT_Felhasznalok.Id = felhasznaloProfil.LoginUser.Id;

                    ExecParam.Felhasznalo_Id = felhasznaloProfil.LoginUser.Id;

                    _KRT_Felhasznalo_SzerepkorSearch.Helyettesites_Id.Value = felhasznaloProfil.Helyettesites.Id;
                    _KRT_Felhasznalo_SzerepkorSearch.Helyettesites_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                    _ret = _KRT_SzerepkorokService.GetAllByFelhasznalo(ExecParam, _KRT_Felhasznalok, _KRT_Felhasznalo_SzerepkorSearch);

                    if (_ret.Ds != null)
                    {
                        for (int i = 0; i < _ret.Ds.Tables[0].Rows.Count; i++)
                        {
                            string szerepkorNev = _ret.Ds.Tables[0].Rows[i]["Szerepkor_Nev"].ToString();
                            if (felhasznaloProfil.Szerepkorok.ContainsKey(szerepkorNev) == false)
                            {
                                felhasznaloProfil.Szerepkorok.Add(szerepkorNev, new FelhasznaloProfil.Szerepkor(_ret.Ds.Tables[0].Rows[i]["Id"].ToString(), szerepkorNev));
                            }
                        }
                    }
                }
                else
                {
                    _KRT_Felhasznalok.Id = felhasznaloProfil.Felhasznalo.Id;

                    //if (String.IsNullOrEmpty(felhasznaloProfil.Helyettesitett.Id))
                    //{
                    //    _KRT_Felhasznalok.Id = felhasznaloProfil.Felhasznalo.Id;
                    //}
                    //else
                    //{
                    //    _KRT_Felhasznalok.Id = felhasznaloProfil.Helyettesitett.Id;                
                    //}

                    ExecParam.Felhasznalo_Id = felhasznaloProfil.Felhasznalo.Id;

                    // itt csak a saj�t jogon szerzettek
                    _KRT_Felhasznalo_SzerepkorSearch.Helyettesites_Id.Value = "";
                    _KRT_Felhasznalo_SzerepkorSearch.Helyettesites_Id.Operator = Contentum.eQuery.Query.Operators.isnull;

                    // aktu�lis csoporttags�gt�l is f�gg...
                    _KRT_Felhasznalo_SzerepkorSearch.CsoportTag_Id.Value = felhasznaloProfil.FelhasznaloCsoportTagsag.Id;
                    _KRT_Felhasznalo_SzerepkorSearch.CsoportTag_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                    _ret =
                        _KRT_SzerepkorokService.GetAllByFelhasznalo(ExecParam, _KRT_Felhasznalok, _KRT_Felhasznalo_SzerepkorSearch);

                    if (_ret.Ds != null)
                    {
                        for (int i = 0; i < _ret.Ds.Tables[0].Rows.Count; i++)
                        {
                            string szerepkorNev = _ret.Ds.Tables[0].Rows[i]["Szerepkor_Nev"].ToString();
                            if (felhasznaloProfil.Szerepkorok.ContainsKey(szerepkorNev) == false)
                            {
                                felhasznaloProfil.Szerepkorok.Add(szerepkorNev, new FelhasznaloProfil.Szerepkor(_ret.Ds.Tables[0].Rows[i]["Id"].ToString(), szerepkorNev));
                            }
                        }
                    }

                    // ...vagy nincs megadva a csoporttags�g
                    _KRT_Felhasznalo_SzerepkorSearch.CsoportTag_Id.Value = "";
                    _KRT_Felhasznalo_SzerepkorSearch.CsoportTag_Id.Operator = Contentum.eQuery.Query.Operators.isnull;

                    _ret =
                        _KRT_SzerepkorokService.GetAllByFelhasznalo(ExecParam, _KRT_Felhasznalok, _KRT_Felhasznalo_SzerepkorSearch);

                    if (_ret.Ds != null)
                    {
                        for (int i = 0; i < _ret.Ds.Tables[0].Rows.Count; i++)
                        {
                            string szerepkorNev = _ret.Ds.Tables[0].Rows[i]["Szerepkor_Nev"].ToString();
                            if (felhasznaloProfil.Szerepkorok.ContainsKey(szerepkorNev) == false)
                            {
                                felhasznaloProfil.Szerepkorok.Add(szerepkorNev, new FelhasznaloProfil.Szerepkor(_ret.Ds.Tables[0].Rows[i]["Id"].ToString(), szerepkorNev));
                            }
                        }
                    }
                }

                Contentum.eAdmin.Service.KRT_FunkciokService _KRT_FunkciokService = eAdminService.ServiceFactory.GetKRT_FunkciokService();
                if (felhasznaloProfil.Helyettesites.HelyettesitesMod == KodTarak.HELYETTESITES_MOD.Megbizas)
                {
                    ExecParam.Record_Id = felhasznaloProfil.Helyettesites.Id;
                    _ret = _KRT_FunkciokService.GetAllByMegbizas(ExecParam);
                }
                else
                {
                    ExecParam.Record_Id = felhasznaloProfil.FelhasznaloCsoportTagsag.Id;
                    _ret = _KRT_FunkciokService.GetAllByCsoporttagSajatJogu(ExecParam);
                }

                if (_ret.Ds != null)
                {
                    for (int i = 0; i < _ret.Ds.Tables[0].Rows.Count; i++)
                    {
                        string funkcioKod = _ret.Ds.Tables[0].Rows[i]["Kod"].ToString();
                        if (felhasznaloProfil.Funkciok.ContainsKey(funkcioKod) == false)
                        {
                            felhasznaloProfil.Funkciok.Add(funkcioKod, new FelhasznaloProfil.Funkcio(_ret.Ds.Tables[0].Rows[i]["Id"].ToString(), funkcioKod));
                        }
                    }
                }

                felhasznaloProfil.TimeStamp_FunkciokRefresh = DateTime.Now;

            }
        }


        public static void GetFunkcioJogRedirectErrorPage(Page ParentPage, string FunkcioNev)
        {
            if (ParentPage == null || String.IsNullOrEmpty(FunkcioNev)) return;
            if (ParentPage is Contentum.eUtility.UI.PageBase) (ParentPage as Contentum.eUtility.UI.PageBase).FunkcioKod = FunkcioNev;
            bool _ret = GetFunkcioJog(ParentPage, FunkcioNev);
            if (!_ret)
            {
                RedirectErrorPage(ParentPage,FunkcioNev);
            }
        }

        public static void RedirectErrorPage(Page ParentPage)
        {
            RedirectErrorPage(ParentPage, null);
        }
        public static void RedirectErrorPage(Page ParentPage, string FunkcioNev)
        {
            if (ParentPage == null) return;

            ParentPage.Session[Constants.PageNameBeforeLogin] = ParentPage.Request.Url.AbsolutePath;

            if (!String.IsNullOrEmpty(FunkcioNev))
            {
                Log.Warning.Page(ParentPage, "Acces Denied: " + FunkcioNev);
                UI.RedirectPage(ParentPage, "Error.aspx?errorcode=UIAccessDeniedPage&FunkcioKod=" + FunkcioNev);
            }
            else
            {
                Log.Warning.Page(ParentPage, "Acces Denied");
                UI.RedirectPage(ParentPage, "Error.aspx?errorcode=UIAccessDeniedPage");
            }
        }

        public static bool GetFunkcioJog(Page ParentPage, string FunkcioNev)
        {
            if (ParentPage == null || String.IsNullOrEmpty(FunkcioNev)) return false;
            bool _ret = false;

            if (FelhasznaloProfilIsCached(ParentPage))
            {
                FelhasznaloProfil _FelhasznaloProfil =
                    (FelhasznaloProfil)ParentPage.Session[Constants.FelhasznaloProfil];

                DateTime userTimeStamp = _FelhasznaloProfil.TimeStamp_FunkciokRefresh;
                DateTime serverTimeStamp = FunctionRights.GetFunkciokLastUpdatedTimeFromCache(ParentPage);

                if (userTimeStamp != null)
                {
                    if (userTimeStamp.CompareTo(serverTimeStamp) >= 0)
                    {
                        _ret = _FelhasznaloProfil.Funkciok.ContainsKey(FunkcioNev);
                    }
                    else
                    {
                        FunctionRights.UpdateSzerepkorFunkcioOnFelhasznaloProfil(ParentPage,_FelhasznaloProfil);
                        ParentPage.Session[Constants.FelhasznaloProfil] = _FelhasznaloProfil;
                        _ret = _FelhasznaloProfil.Funkciok.ContainsKey(FunkcioNev);
                    }
                }
            }
            return _ret;
        }

        /// <summary>
        /// Lek�rdezi az execParamban szerepl� felhaszn�l�i adatok alapj�n, hogy a felhaszn�l� (esetleg megb�zott)
        /// rendelkezik-e a function param�terben megadott k�d� funkci�joggal. Ha a lek�r�s sor�n hiba l�p fel, false-t ad vissza!
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="function">A funkci� k�dja, melynek megl�t�t vizsg�ljuk</param>
        /// <returns></returns>
        public static bool HasFunctionRight(ExecParam execParam, string function)
        {
            Logger.DebugStart("HasFunctionRight - Start");
            Logger.Debug("Felhasznalo_Id: " + (execParam.Felhasznalo_Id ?? "NULL") + " LoginUser_Id: " + (execParam.LoginUser_Id ?? "NULL"));
            Logger.Debug("CsoportTag_Id: " + (execParam.CsoportTag_Id ?? "NULL") + " FelhasznaloSzervezet_Id: " + (execParam.FelhasznaloSzervezet_Id ?? "NULL"));

            bool HasFunctionRight = false;
            bool isMegbizas = false;
            // ha hiba volt a lek�r�s sor�n, akkor a v�g�n false-t adunk vissza
            if (!String.IsNullOrEmpty(execParam.Helyettesites_Id))
            {
                Logger.Debug("Helyettesites_Id: " + execParam.Helyettesites_Id);
                Contentum.eAdmin.Service.KRT_HelyettesitesekService service_helyettesitesek = eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();
                ExecParam execParam_helyettesitesek = execParam.Clone();
                execParam_helyettesitesek.Record_Id = execParam.Helyettesites_Id;
                Result result_helyettesitesGet = service_helyettesitesek.Get(execParam_helyettesitesek);

                if (String.IsNullOrEmpty(result_helyettesitesGet.ErrorCode))
                {
                    KRT_Helyettesitesek krt_Helyettesitesek = (KRT_Helyettesitesek)result_helyettesitesGet.Record;
                    if (krt_Helyettesitesek != null)
                    {
                        if (krt_Helyettesitesek.HelyettesitesMod == KodTarak.HELYETTESITES_MOD.Megbizas)
                        {
                            isMegbizas = true;
                            Logger.Debug("isMegbizas = true");
                        }
                    }
                    else
                    {
                        // hiba
                        Logger.Debug("EKF: krt_Helyettesitesek == null");
                        Logger.DebugEnd("HasFunctionRight - End");
                        return false;
                    }
                }
                else
                {
                    // hiba
                    Logger.Debug("EKF: !String.IsNullOrEmpty(result_helyettesitesGet.ErrorCode)");
                    Logger.DebugEnd("HasFunctionRight - End");
                    return false;
                }
            }

            // Mindig a Felhasznalo -ra k�rj�k le a funkci�jogosults�gokat
            // (Helyettes�t�sn�l is a Felhaszn�l� lesz a helyettes�tett) - kiv�ve megb�z�s
            Result _ret;

            ExecParam execParam_funkciok = execParam.Clone();

            Contentum.eAdmin.Service.KRT_FunkciokService _KRT_FunkciokService = eAdminService.ServiceFactory.GetKRT_FunkciokService();
            if (isMegbizas)
            {
                execParam_funkciok.Record_Id = execParam.Helyettesites_Id;
                Logger.Debug("HasFunctionRight: GetAllByMegbizas", execParam_funkciok);
                _ret = _KRT_FunkciokService.GetAllByMegbizas(execParam_funkciok);
            }
            else
            {
                // szervezet �s felhaszn�l� adott, de sz�ks�g van a csoporttags�gra
                execParam_funkciok.Record_Id = execParam.CsoportTag_Id;
                Logger.Debug("HasFunctionRight: GetAllByCsoporttagSajatJogu", execParam_funkciok);
                _ret = _KRT_FunkciokService.GetAllByCsoporttagSajatJogu(execParam_funkciok);
            }

            if (_ret.Ds != null)
            {
                System.Data.DataRow[] rows_funkcio = _ret.Ds.Tables[0].Select("Kod='" + function + "'");
                if (rows_funkcio.Length > 0)
                {
                    HasFunctionRight = true;
                }
            }
            else
            {
                // hiba
                Logger.Debug("EKF: _ret.Ds == null");
                Logger.DebugEnd("HasFunctionRight - End");
                return false;
            }

            Logger.Debug("HasFunctionRight = " + HasFunctionRight.ToString());
            Logger.DebugEnd("HasFunctionRight - End");
            return HasFunctionRight;
        }

        /// <summary>
        /// Lek�rdezi az �sszes, a krt_FunkciokSearchben meghat�rozott felt�telnek megfelel� funkci�k�dot
        /// �s visszaadja egy string list�ban. Hiba eset�n null-t ad vissza!
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="krt_FunkciokSearch">Keres�si felt�telek a lek�rt funkci�k sz�k�t�s�hez</param>
        /// <returns></returns>
        public static List<string> GetAllFunkcioKod(ExecParam execParam, Contentum.eQuery.BusinessDocuments.KRT_FunkciokSearch krt_FunkciokSearch)
        {
            Logger.DebugStart("GetAllFunkcioKod - Start");
            Contentum.eAdmin.Service.KRT_FunkciokService _KRT_FunkciokService = eAdminService.ServiceFactory.GetKRT_FunkciokService();
            Logger.Debug("GetAllFunkcioKod: GetAll", execParam);
            Result result = _KRT_FunkciokService.GetAll(execParam, krt_FunkciokSearch);

            if (result.IsError)
            {
                // hiba
                Logger.Error("GetAllFunkcioKod hiba!", execParam, result);
                Logger.DebugEnd("GetAllFunkcioKod - End");
                return null;
            }

            List<string> lstFunkciok = new List<string>();

            foreach (System.Data.DataRow row in result.Ds.Tables[0].Rows)
            {
                string FunkcioKod = row["Kod"].ToString();
                if (!String.IsNullOrEmpty(FunkcioKod) && !lstFunkciok.Contains(FunkcioKod))
                {
                    lstFunkciok.Add(FunkcioKod);
                }
            }

            Logger.DebugEnd(String.Format("GetAllFunkcioKod Result Count: {0}", lstFunkciok.Count));
            Logger.DebugEnd("GetAllFunkcioKod - End");
            return lstFunkciok;
        }

        /// <summary>
        /// Lek�rdezi az �sszes, a krt_FunkciokSearchben meghat�rozott felt�telnek megfelel� funkci�k�dot
        /// �s visszaadja egy string list�ban. Hiba eset�n null-t ad vissza!
        /// </summary>
        /// <param name="page"></param>
        /// <param name="krt_FunkciokSearch">Keres�si felt�telek a lek�rt funkci�k sz�k�t�s�hez</param>
        /// <returns></returns>
        public static List<string> GetAllFunkcioKod(Page page, Contentum.eQuery.BusinessDocuments.KRT_FunkciokSearch krt_FunkciokSearch)
        {
            ExecParam execParam = UI.SetExecParamDefault(page);
            return GetAllFunkcioKod(execParam, krt_FunkciokSearch);
        }
    
    }
  
}
