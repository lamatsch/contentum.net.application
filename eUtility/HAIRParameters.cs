﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
namespace Contentum.eUtility
{
    public class HAIRParameters
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string URL { get; set; }
        protected HAIRParameters() { }
        protected string GetParameter(ExecParam execParam, string key)
        {
            var parameterService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_ParameterekService();
            var search = new KRT_ParameterekSearch();
            search.Nev.Value = key;
            search.Nev.Operator = Contentum.eQuery.Query.Operators.equals;

            Result searchResult = parameterService.GetAll(execParam, search);
            if (searchResult.IsError || searchResult.Ds.Tables[0].Rows.Count == 0) { return string.Empty; }
            string ertek = searchResult.Ds.Tables[0].Rows[0]["Ertek"].ToString();
            return ertek;
        }
    }
    public class HAIRPartnerModositasParameters : HAIRParameters
    {
        public HAIRPartnerModositasParameters(ExecParam execParam) : base()
        {
            User = GetParameter(execParam, Constants.USER_KEY);
            Password = GetParameter(execParam, Constants.PASSWORD_KEY);
            URL = GetParameter(execParam, Constants.URL_KEY);
        }

        private static class Constants
        {           
            public const string USER_KEY = "HAIR.PARTNER.USER";
            public const string PASSWORD_KEY = "HAIR.PARTNER.PWD";
            public const string URL_KEY = "HAIR.PARTNER.URL";
        }
    }

    public class HAIRCsatolmanyFeltoltesParameters : HAIRParameters
    {
       
        public HAIRCsatolmanyFeltoltesParameters(ExecParam execParam) : base()
        {
            User = GetParameter(execParam, Constants.USER_KEY);
            Password = GetParameter(execParam, Constants.PASSWORD_KEY);
            URL = GetParameter(execParam, Constants.URL_KEY);
        }

        public static class Constants
        {
            public const string HAIR_FILE_SOURCE = "HAIR";
            public const string USER_KEY = "HAIR.CSATOLMANY.USER";
            public const string PASSWORD_KEY = "HAIR.CSATOLMANY.PWD";
            public const string URL_KEY = "HAIR.CSATOLMANY.URL";
        }
    }
}