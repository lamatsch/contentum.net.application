﻿using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eRecord.Service;
using System;
using System.Data.SqlTypes;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Web.UI;
using System.Xml;

namespace Contentum.eUtility
{
    public static class AlairasokUtility
    {
        private static X509Certificate2Collection _Alairasok;
        private static X509Certificate2 _XmlAlairas;

        /// <summary>
        /// A Local Machine tároló aláírásra használható tanúsítványai.
        /// </summary>
        public static X509Certificate2Collection AlairasLista
        {
            get
            {

                return _Alairasok;
            }
            set
            {
                _Alairasok = value;
            }
        }

        public static X509Certificate2 XmlAlairas
        {
            get
            {

                return _XmlAlairas;
            }
            set
            {
                _XmlAlairas = value;
            }
        }

        /// <summary>
        /// Lekérdezi a Local Machine tároló digitális aláírásra használható tanúsítványait az eredményhalmaz _Alairasok tagba kerül.
        /// </summary>
        public static string InitAlairasokStore()
        {
            string msg = String.Empty;
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            try
            {
                store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);
                _Alairasok = store.Certificates.Find(X509FindType.FindByKeyUsage, X509KeyUsageFlags.DigitalSignature, true);
                if (_Alairasok.Count < 1)
                {
                    msg = "Nem található aláírókulcs";
                }
                //_Alairasok = store.Certificates;
            }
            catch (Exception e)
            {
                return e.Message;
            }
            finally
            {
                store.Close();
            }
            return msg;
        }

        public static string InitXmlAlairas()
        {
            string msg = String.Empty;
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            try
            {
                string serial = UI.GetAppSetting("AlairasCertificateSerial");

                store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);
                X509Certificate2Collection alairasok = store.Certificates.Find(X509FindType.FindBySerialNumber, serial, true);
                if (alairasok.Count < 1)
                {
                    msg = "Nem található aláírókulcs";
                }
                else
                {
                    XmlAlairas = alairasok[0];
                }
                //_Alairasok = store.Certificates;
            }
            catch (Exception e)
            {
                return e.Message;
            }
            finally
            {
                store.Close();
            }
            return msg;
        }

        /// <summary>
        /// Aláírja a paraméterként megadott tanúsítvánnyal a paraméterként megadott XML fájlt.
        /// </summary>
        /// <param name="cert">Az aláírásra használt tanúsítvány.</param>
        /// <param name="filestream">Az aláirandó XML fájl</param>
        /// <param name="outFileName">Az aláírt XML fájlneve</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static Stream SignXml(X509Certificate2 cert, Stream filestream)
        {
            if (cert == null || filestream == null)
            {
                throw new ArgumentNullException();
            }
            XmlDocument XmlDoc = new XmlDocument();

            XmlDoc.PreserveWhitespace = true;
            XmlDoc.Load(filestream);
            SignedXml sxml = new SignedXml(XmlDoc);
            Reference reference = new Reference();
            reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
            reference.Uri = "";
            sxml.AddReference(reference);
            sxml.KeyInfo = new KeyInfo();
            KeyInfoX509Data keydata = new KeyInfoX509Data(cert);
            keydata.AddSubjectName(cert.Subject);
            keydata.AddIssuerSerial(cert.Issuer, cert.GetSerialNumberString());
            sxml.KeyInfo.AddClause(keydata);
            if (cert.HasPrivateKey)
            {
                sxml.SigningKey = cert.PrivateKey;
            }
            sxml.ComputeSignature();
            XmlDoc.DocumentElement.AppendChild(XmlDoc.ImportNode(sxml.GetXml(), true));
            MemoryStream ms = new MemoryStream(100);
            XmlDoc.Save(ms);
            return ms;
        }

        public static Result FileUpload(Page page, ExecParam exec, Stream stream, string fileName, bool isSigned, string existingFileId)
        {
            #region Feltöltött file beolvasása byte[]-ba

            stream.Position = 0;
            System.IO.StreamReader sr = new System.IO.StreamReader(stream);
            string s = sr.ReadToEnd();
            byte[] buf = System.Text.ASCIIEncoding.ASCII.GetBytes(s);
            //byte[] buf = fileUpload.FileBytes;

            #endregion Feltöltött file beolvasása byte[]-ba

            ExecParam execParam = null;
            if (page != null)
                execParam = UI.SetExecParamDefault(page);
            else
                execParam = exec;

            DocumentService documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();

            #region FileUpload
            KRT_Dokumentumok krt_Dokumentumok = GetBusinessObjectFromComponents(fileName, isSigned);
            //string checkinmode = "1";
            //if (TextBoxFileName.Text == "-")
            //{
            //    checkinmode = "2";
            //    krt_Dokumentumok.Id = Request.QueryString.Get(QueryStringVars.Id);
            //}
            //Result result = documentService.DirectUploadAndDocumentInsert(execParam, Contentum.eUtility.Constants.DocumentStoreType.UCM
            //    , SITE_PATH, DOC_LIB_PATH, FOLDER_PATH, fileName, buf, checkinmode, krt_Dokumentumok);

            string ujBejegyzes = String.IsNullOrEmpty(existingFileId) ? "IGEN" : "NEM";

            String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                       "<iktatokonyv></iktatokonyv>" +
                                                       "<sourceSharePath></sourceSharePath>" +
                                                       "<foszam>{0}</foszam>" +
                                                       "<kivalasztottIratId></kivalasztottIratId>" +
                                                       "<docmetaDokumentumId>{1}</docmetaDokumentumId>" +
                                                       "<megjegyzes></megjegyzes>" +
                                                       "<munkaanyag>{2}</munkaanyag>" +
                                                       "<ujirat>{3}</ujirat>" +
                                                       "<vonalkod></vonalkod>" +
                                                       "<docmetaIratId></docmetaIratId>" +
                                                       "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                       "<ucmiktatokonyv>{5}</ucmiktatokonyv>" +
                                                   "</uploadparameterek>"
                                                   , String.Empty
                                                   , existingFileId
                                                   , "NEM"
                                                   , "IGEN"
                                                   , ujBejegyzes
                                                   , "HK"
                                                   , fileName
                                                   );

            Result result_upload = documentService.UploadFromeRecordWithCTTCheckin
                (
                   execParam
                   , Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
                   , fileName
                   , buf
                   , krt_Dokumentumok
                   , uploadXmlStrParams
                );

            return result_upload;


            #endregion FileUpload
        }

        public static KRT_Dokumentumok GetBusinessObjectFromComponents(string fileName, bool isSigned)
        {
            KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();
            // összes mező update-elhetőségét kezdetben letiltani:
            krt_Dokumentumok.Updated.SetValueAll(false);
            krt_Dokumentumok.Base.Updated.SetValueAll(false);

            krt_Dokumentumok.FajlNev = fileName;
            krt_Dokumentumok.Updated.FajlNev = true;

            krt_Dokumentumok.Megnyithato = "1";
            krt_Dokumentumok.Updated.Megnyithato = true;

            krt_Dokumentumok.Olvashato = "1";
            krt_Dokumentumok.Updated.Olvashato = true;

            krt_Dokumentumok.Titkositas = "0";
            krt_Dokumentumok.Updated.Titkositas = true;

            krt_Dokumentumok.ElektronikusAlairas = isSigned ? KodTarak.DOKUMENTUM_ALAIRAS_PKI.Ervenyes_alairas : String.Empty;
            krt_Dokumentumok.Updated.ElektronikusAlairas = true;

            krt_Dokumentumok.Tipus = "Alairas (Xml)";
            krt_Dokumentumok.Updated.Tipus = true;

            krt_Dokumentumok.ErvKezd = DateTime.Today.ToString();
            krt_Dokumentumok.Updated.ErvKezd = true;

            krt_Dokumentumok.Base.Ver = "1";
            krt_Dokumentumok.Base.Updated.Ver = true;

            return krt_Dokumentumok;
        }

        public static Result SignAndSavetoDocStore(Page page, ExecParam exec, string[] IraIktatoKonyvekIds, out string alairasStoreInitMessage, bool lezaras)
        {
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();

            alairasStoreInitMessage = String.Empty;

            Result result = new Result();
            if (IraIktatoKonyvekIds != null)
            {
                alairasStoreInitMessage = InitXmlAlairas();

                foreach (string IktatokonyvId in IraIktatoKonyvekIds)
                {
                    try
                    {
                        ExecParam execParam = null;

                        if (page != null)
                            execParam = UI.SetExecParamDefault(page, new ExecParam());
                        else
                            execParam = exec;

                        execParam.Record_Id = IktatokonyvId;
                        Result res = service.Get(execParam);
                        res.CheckError();

                        EREC_IraIktatoKonyvek konyv = (EREC_IraIktatoKonyvek)res.Record;

                        if (konyv.Statusz == "1" && !lezaras)
                        {
                            continue;
                        }

                        if (!String.IsNullOrEmpty(konyv.HitelesExport_Dok_ID))
                        {
                            Contentum.eDocument.Service.KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                            ExecParam dokExecParam = null;

                            if (page != null)
                                dokExecParam = UI.SetExecParamDefault(page, new ExecParam());
                            else
                                dokExecParam = exec;

                            dokExecParam.Record_Id = konyv.HitelesExport_Dok_ID;
                            Result dokResult = dokService.Get(dokExecParam);
                            if (String.IsNullOrEmpty(dokResult.ErrorCode))
                            {
                                KRT_Dokumentumok krt_Dokumentumok = (KRT_Dokumentumok)dokResult.Record;
                                if (krt_Dokumentumok.ElektronikusAlairas == KodTarak.DOKUMENTUM_ALAIRAS_PKI.Ervenyes_alairas)
                                {
                                    continue;
                                }
                            }
                            else
                            {
                                throw new ResultException(dokResult);
                            }
                        }

                        string iktatoKonyvXML = GetIktatoKonyvXML(konyv);

                        Stream ms = new MemoryStream(Encoding.UTF8.GetBytes(iktatoKonyvXML));
                        bool isSigned = false;

                        if (String.IsNullOrEmpty(alairasStoreInitMessage) && XmlAlairas != null)
                        {
                            try
                            {
                                Stream signed = SignXml(XmlAlairas, ms);
                                ms = signed;
                                isSigned = true;
                            }
                            catch (Exception)
                            {
                                isSigned = false;
                            }
                        }

                        Result uploadResult;
                        Result updateResult;

                        if (page != null)
                        {
                            uploadResult = FileUpload(page, null, ms, IktatokonyvId, isSigned, konyv.HitelesExport_Dok_ID);
                            updateResult = UpdateHitelesDocId(page, null, konyv, IktatokonyvId, uploadResult.Uid);

                        }
                        else
                        {
                            uploadResult = FileUpload(null, exec, ms, IktatokonyvId, isSigned, konyv.HitelesExport_Dok_ID);
                            updateResult = UpdateHitelesDocId(null, exec, konyv, IktatokonyvId, uploadResult.Uid);
                        }

                        uploadResult.CheckError();
                        updateResult.CheckError();
                    }
                    catch (Exception ex)
                    {
                        result = ResultException.GetResultFromException(ex);
                    }
                }
            }

            return result;
        }

        private static Result UpdateHitelesDocId(Page page, ExecParam exec, EREC_IraIktatoKonyvek konyv, string iktatokonyvId, string dokumentumId)
        {
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam execParam = null;

            if (page != null)
                execParam = UI.SetExecParamDefault(page, new ExecParam());
            else
                execParam = exec;

            execParam.Record_Id = iktatokonyvId;

            konyv.Updated.SetValueAll(false);
            konyv.Base.Updated.SetValueAll(false);
            konyv.Base.Updated.Ver = true;
            konyv.HitelesExport_Dok_ID = dokumentumId;
            konyv.Updated.HitelesExport_Dok_ID = true;

            Result updateResult = service.Update(execParam, konyv);

            return updateResult;

        }

        public static string GetIktatoKonyvXML(EREC_IraIktatoKonyvek iktatoKonyv)
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(EREC_IraIktatoKonyvek));
            string xml;
            using (Utf8StringWriter writer = new Utf8StringWriter())
            {
                serializer.Serialize(writer, iktatoKonyv);
                xml = writer.ToString();
            }

            return xml;
        }

        public class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get { return Encoding.UTF8; }
            }
        }

        private static bool IsOKForTomeges(EREC_IratAlairok record)
        {
            if (record == null) { return false; }
            if (record.Base == null) { return false; }
            return true;
        }

        public static bool IsTomeges(EREC_IratAlairok record)
        {
            if (!IsOKForTomeges(record)) { return false; }
            if (record.Base.Note != Constants.NoteSpecialValues.Tomeges)
            {
                return false;
            }
            return true;
        }

        public static void SetNoteTomeges(EREC_IratAlairok record)
        {
            if (!IsOKForTomeges(record)) { return; }
            record.Base.Note += Constants.NoteSpecialValues.Tomeges;
            record.AlairasDatuma = string.Empty;
        }

        public static void UnSetNoteTomeges(EREC_IratAlairok record)
        {
            if (!IsOKForTomeges(record)) { return; }
            if (!record.Base.Note.Contains(Constants.NoteSpecialValues.Tomeges)) { return; }
            record.Base.Note = record.Base.Note.Replace(Constants.NoteSpecialValues.Tomeges, string.Empty);
        }
    }
}
