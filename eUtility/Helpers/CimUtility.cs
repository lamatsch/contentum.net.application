﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;

namespace Contentum.eUtility
{
    public static class CimUtility
    {
        public static string GetPartnerCimId(ExecParam execParam, string partnerId)
        {
            var cimekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CimekService();

            var p = new KRT_Partnerek();
            p.Typed.Id = new Guid(partnerId);
            var pcSearch = new KRT_PartnerCimekSearch();
            pcSearch.WhereByManual = " and (KRT_Cimek.Tipus ='01RNY' or KRT_Cimek.Tipus ='01') ";
            pcSearch.OrderBy = " KRT_PartnerCimek.LetrehozasIdo DESC ";
            pcSearch.TopRow = 1;

            pcSearch.ErvKezd.LessOrEqual(Query.SQLFunction.getdate);
            pcSearch.ErvKezd.AndGroup("100");

            pcSearch.ErvVege.GreaterOrEqual(Query.SQLFunction.getdate);
            pcSearch.ErvVege.AndGroup("100");

            var execParamCim = execParam.Clone();
            Result partnerCimekResult = cimekService.GetAllByPartner(execParamCim, p, null, pcSearch);

            if (!partnerCimekResult.IsError && partnerCimekResult.GetCount > 0)
            {
                return partnerCimekResult.Ds.Tables[0].Rows[0]["Cim_Id"] == DBNull.Value ? string.Empty : partnerCimekResult.Ds.Tables[0].Rows[0]["Cim_Id"].ToString();
            }
            return "";
        }

        public static string GetCimIdByStr(ExecParam execParam, string cimSTR)
        {
            var cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();

            KRT_CimekSearch cimSearch = new KRT_CimekSearch();

            string where = String.Format(@"dbo.fn_MergeCim
			  (KRT_Cimek.Tipus,
			   KRT_Cimek.OrszagNev,
			   KRT_Cimek.IRSZ,
			   KRT_Cimek.TelepulesNev,
			   KRT_Cimek.KozteruletNev,
			   KRT_Cimek.KozteruletTipusNev,
			   KRT_Cimek.Hazszam,
			   KRT_Cimek.Hazszamig,
			   KRT_Cimek.HazszamBetujel,
			   KRT_Cimek.Lepcsohaz,
			   KRT_Cimek.Szint,
			   KRT_Cimek.Ajto,
			   KRT_Cimek.AjtoBetujel,
			   -- BLG_1347
			   KRT_Cimek.HRSZ,
			   KRT_Cimek.CimTobbi) = '{0}'", cimSTR);

            cimSearch.WhereByManual = " and " + where;

            cimSearch.ErvKezd.LessOrEqual(Query.SQLFunction.getdate);
            cimSearch.ErvKezd.AndGroup("100");

            cimSearch.ErvVege.GreaterOrEqual(Query.SQLFunction.getdate);
            cimSearch.ErvVege.AndGroup("100");

            Result res = cimekService.GetAll(execParam.Clone(), cimSearch);

            if (!res.IsError && res.GetCount > 0)
            {
                return res.Ds.Tables[0].Rows[0]["Id"].ToString();
            }

            return String.Empty;
        }

        private static KRT_Cimek GetCim(ExecParam execParam, string cimId)
        {
            var cimekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CimekService();
            execParam.Record_Id = cimId;
            var result = cimekService.Get(execParam);
            if (result.IsError)
            {
                Logger.Error(result.ErrorMessage);
                return null;
            }
            return result.Record as KRT_Cimek;
        }

        public static string GetCimSTRById(ExecParam execParam, string cimId)
        {
            var cim = GetCim(execParam, cimId);
            if (cim == null)
            {
                return "";
            }
            return String.IsNullOrEmpty(cim.Nev) ? MergeCim(cim, true) : cim.Nev;
        }

        public static string MergeCim(ExecParam execParam, string cimId)
        {
            var cim = GetCim(execParam, cimId);
            return MergeCim(cim, true);
        }

        private static string Delimiter = " ";

        private static string Str(object s)
        {
            return s == null || s.ToString() == "" || s == DBNull.Value ? "" : s.ToString();
        }

        private static string Delim(object s)
        {
            var str = Str(s);
            return str.Length == 0 ? "" : str + Delimiter;
        }
        private static string Space(object s)
        {
            var str = Str(s);
            return str.Length == 0 ? "" : str + " ";
        }

        private static bool Has(string s)
        {
            return s != null && s != Delimiter && s.Trim().Length > 0 && s.Trim() != Delimiter;
        }

        // based on fn_MergeCim2
        public static string MergeCim(KRT_Cimek cim, bool isFull)
        {
            if (cim == null)
            {
                return "";
            }

            var res = "";

            if (cim.Tipus != KodTarak.Cim_Tipus.Postai)
            {
                res = Str(cim.CimTobbi);
                return res;
            }

            if (isFull)
            {
                if (string.IsNullOrEmpty(cim.OrszagNev))
                    res = Delim(cim.IRSZ) + Delim(cim.TelepulesNev);
                else
                    res = cim.OrszagNev + ", " + cim.IRSZ + ", " + cim.TelepulesNev + ", ";
            }
            else
                res = "";

            var kt = Delim(cim.KozteruletNev);
            var hrsz = Space(cim.HRSZ);
            if (Has(hrsz) && (kt == "" || kt == Delimiter))
            {
                res += " HRSZ. " + hrsz;
            }
            else
            {
                res += kt;
            }

            var hazszam = Space(cim.Hazszam);
            res += Space(cim.KozteruletTipusNev) + hazszam;

            var hazszamig = Space(cim.Hazszamig);
            if (Has(hazszamig))
            {
                res += "-" + hazszamig;
            }

            var betujel = Delim(cim.HazszamBetujel);
            if (Has(betujel))
            {
                res += "/" + betujel;
            }

            var lepcsohaz = Str(cim.Lepcsohaz);
            if (Has(lepcsohaz))
            {
                res += lepcsohaz + " lépcsőház" + Delimiter;
            }

            var szint = Str(cim.Szint);
            if (Has(szint))
            {
                res += lepcsohaz + ". emelet" + Delimiter;
            }

            var ajto = Str(cim.Ajto);
            if (Has(ajto))
            {
                res += ajto;
                var ajtoBetujel = Str(cim.AjtoBetujel);
                if (Has(ajtoBetujel))
                {
                    res += "/" + ajtoBetujel;
                }
                res += " ajtó";
            }
            res = res.Trim();
            return res;
        }
    }
}
