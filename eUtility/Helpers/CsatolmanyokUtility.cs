﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.UI;
using System.Linq;

namespace Contentum.eUtility
{
    public static class CsatolmanyokUtility
    {
        #region BLG_577 CSATOLMANY JOGOSULTAK
        /// <summary>
        /// Csatolmany jogosultjai
        /// </summary>
        /// <param name="ParentPage"></param>
        /// <param name="erec_KuldKuldemenyek"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetUserRights_Csatolmanyok(Page ParentPage, EREC_KuldKuldemenyek erec_KuldKuldemenyek)
        {
            if (erec_KuldKuldemenyek != null)
            {
                string id = null;
                if (erec_KuldKuldemenyek.Allapot != KodTarak.KULDEMENY_ALLAPOT.Iktatva) //IKTATOTT
                {
                    id = erec_KuldKuldemenyek.Id;
                }
                else  // NEM IKTATOTT KÜLDEMÉNY
                {
                    EREC_IraIratok irat = GetIratFromKuldemeny(ParentPage, erec_KuldKuldemenyek.Id);
                    if (irat == null)
                        return null;
                    id = irat.Id;
                }

                Result kuldemenyJogosultak = GetKuldemenyJogosultak(ParentPage, id);
                if (kuldemenyJogosultak == null || kuldemenyJogosultak.Ds.Tables[0].Rows.Count < 1)
                    return null;

                Dictionary<string, string> listJogalanyok = new Dictionary<string, string>();
                foreach (DataRow itemRow in kuldemenyJogosultak.Ds.Tables[0].Rows)
                {
                    if (!listJogalanyok.ContainsKey(itemRow["Csoport_Id_Jogalany"].ToString()))
                        listJogalanyok.Add(itemRow["Csoport_Id_Jogalany"].ToString(), itemRow["Jogszint"].ToString());
                }

                Dictionary<string, string> listJogosultCsoportok = new Dictionary<string, string>();
                listJogosultCsoportok = GetKuldemenyJogosultCsoportok(ParentPage, listJogalanyok);
                if (listJogosultCsoportok == null || listJogosultCsoportok.Count < 1)
                    return null;

                Dictionary<string, string> listJogosultCsoportTagok = new Dictionary<string, string>();
                listJogosultCsoportTagok = GetCsoportTagok(ParentPage, listJogosultCsoportok);

                return listJogosultCsoportTagok;
            }
            return null;
        }

        #region PRIVATE
        /// <summary>
        /// Kuldemeny jogosultjai
        /// </summary>
        /// <param name="page"></param>
        /// <param name="objId"></param>
        /// <returns></returns>
        private static Result GetKuldemenyJogosultak(Page page, string objId)
        {
            // objektumon keresztüli jogosultság ellenőrzése
            Contentum.eAdmin.Service.KRT_JogosultakService service_jogosultak = eAdminService.ServiceFactory.GetKRT_JogosultakService();
            ExecParam execParam_jogosultak = UI.SetExecParamDefault(page);
            KRT_JogosultakSearch search_jogosultak = new KRT_JogosultakSearch();
            search_jogosultak.Obj_Id.Value = objId;
            search_jogosultak.Obj_Id.Operator = Query.Operators.equals;

            Result result_jogosultak = service_jogosultak.GetAll(execParam_jogosultak, search_jogosultak);
            if (result_jogosultak.IsError || result_jogosultak.Ds.Tables[0].Rows.Count < 1)
                return null;
            return result_jogosultak;
        }
        /// <summary>
        /// Kuldemeny jogosult csoportjai
        /// </summary>
        /// <param name="page"></param>
        /// <param name="csoportJogalanyok"></param>
        /// <returns></returns>
        private static Dictionary<string, string> GetKuldemenyJogosultCsoportok(Page page, Dictionary<string, string> csoportJogalanyok)
        {
            Dictionary<string, string> listCsoportok = new Dictionary<string, string>();
            ExecParam execParam_csoport = UI.SetExecParamDefault(page);
            Contentum.eAdmin.Service.KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            KRT_CsoportokSearch krt_CsoportokSearch = new KRT_CsoportokSearch();

            List<string> keyList = new List<string>(csoportJogalanyok.Keys);

            krt_CsoportokSearch.Id.Value = GetIdsString(keyList);
            krt_CsoportokSearch.Id.Operator = Query.Operators.inner;

            Result result = service.GetAll(execParam_csoport, krt_CsoportokSearch);
            if (result.IsError && result.Ds.Tables[0].Rows.Count < 1)
                return null;

            foreach (DataRow itemRow in result.Ds.Tables[0].Rows)
            {
                if (itemRow["Tipus"].ToString() == KodTarak.CSOPORTTIPUS.Dolgozo)
                {
                    if (!listCsoportok.ContainsKey(itemRow["Id"].ToString()))
                        listCsoportok.Add(itemRow["Id"].ToString(), csoportJogalanyok[itemRow["Id"].ToString()]);
                }
                else if (itemRow["Tipus"].ToString() == KodTarak.CSOPORTTIPUS.Szervezet)
                {
                    if (!listCsoportok.ContainsKey(itemRow["Id"].ToString()))
                        listCsoportok.Add(itemRow["Id"].ToString(), csoportJogalanyok[itemRow["Id"].ToString()]);

                    Result subCsoport = GetCsoportSubCsoport(page, itemRow["Id"].ToString());
                    if (subCsoport.IsError || subCsoport.Ds.Tables[0].Rows.Count < 1)
                        continue;
                    foreach (DataRow subItem in subCsoport.Ds.Tables[0].Rows)
                    {
                        if (!listCsoportok.ContainsKey(itemRow["Id"].ToString()))
                            listCsoportok.Add(subItem["Id"].ToString(), csoportJogalanyok[itemRow["Id"].ToString()]);
                    }
                }
            }
            return listCsoportok;
        }
        /// <summary>
        /// Csoport al csoportjai
        /// </summary>
        /// <param name="page"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private static Result GetCsoportSubCsoport(Page page, string id)
        {
            ExecParam execParam_csoport = UI.SetExecParamDefault(page);
            Contentum.eAdmin.Service.KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            KRT_CsoportokSearch krt_CsoportokSearch = new KRT_CsoportokSearch();
            krt_CsoportokSearch.Tipus.Value = KodTarak.CSOPORTTIPUS.Dolgozo;
            krt_CsoportokSearch.Tipus.Operator = Query.Operators.equals;

            //krt_CsoportokSearch.Id.Value = id;
            //krt_CsoportokSearch.Id.Operator = Query.Operators.equals;

            string FelhasznaloSzervezet_Id = FelhasznaloProfil.FelhasznaloSzerverzetId(page);
            Result result_csoport = service.GetAllSubCsoport(execParam_csoport, id, true, krt_CsoportokSearch);

            if (result_csoport.IsError || result_csoport.Ds.Tables[0].Rows.Count < 1)
                return null;
            return result_csoport;
        }
        /// <summary>
        /// Csoport tagok
        /// </summary>
        /// <param name="page"></param>
        /// <param name="csoportok"></param>
        /// <returns></returns>
        private static Dictionary<string, string> GetCsoportTagok(Page page, Dictionary<string, string> csoportok)
        {
            Dictionary<string, string> listCsoportTagok = new Dictionary<string, string>();
            ExecParam execParam = UI.SetExecParamDefault(page);
            Contentum.eAdmin.Service.KRT_CsoportTagokService csoptag_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

            List<string> keyList = new List<string>(csoportok.Keys);

            krt_CsoportTagokSearch.Csoport_Id.Value = GetIdsString(keyList);
            krt_CsoportTagokSearch.Csoport_Id.Operator = Query.Operators.inner;

            krt_CsoportTagokSearch.Tipus.Value = "1";
            krt_CsoportTagokSearch.Tipus.Operator = Query.Operators.notequals;

            Result csoptag_result = csoptag_service.GetAll(execParam, krt_CsoportTagokSearch);
            if (csoptag_result.IsError || csoptag_result.Ds.Tables[0].Rows.Count < 1)
                return null;

            foreach (DataRow itemRow in csoptag_result.Ds.Tables[0].Rows)
            {
                if (!listCsoportTagok.ContainsKey(itemRow["Csoport_Id_Jogalany"].ToString()))
                    listCsoportTagok.Add(itemRow["Csoport_Id_Jogalany"].ToString(), csoportok[itemRow["Id"].ToString()]);
            }
            return listCsoportTagok;
        }
        /// <summary>
        /// Irat a kuldemenybol
        /// </summary>
        /// <param name="page"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private static EREC_IraIratok GetIratFromKuldemeny(Page page, string id)
        {
            EREC_IraIratok erec_IraIratok = new EREC_IraIratok();

            ExecParam execParam_irat = UI.SetExecParamDefault(page);
            EREC_IraIratokService service_irat = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            EREC_IraIratokSearch search_irat = new EREC_IraIratokSearch();
            search_irat.KuldKuldemenyek_Id.Value = id;
            search_irat.KuldKuldemenyek_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result_irat = service_irat.GetAllWithExtensionAndJogosultak(execParam_irat, search_irat, true);

            if (result_irat.IsError || result_irat.Ds.Tables[0].Rows.Count < 1)
                return null;

            DataRow row_irat = result_irat.Ds.Tables[0].Rows[0];

            // objektum feltöltése a DataRow alapján
            System.Reflection.PropertyInfo[] Properties = erec_IraIratok.GetType().GetProperties();
            foreach (System.Reflection.PropertyInfo P in Properties)
            {
                if (row_irat.Table.Columns.Contains(P.Name))
                {
                    if (!(row_irat.IsNull(P.Name)))
                    {
                        P.SetValue(erec_IraIratok, row_irat[P.Name].ToString(), null);
                    }
                }
            }
            return erec_IraIratok;
        }
        /// <summary>
        /// id listából inner lekérdezés formátum
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        private static string GetIdsString(List<string> ids)
        {
            return Search.GetSqlInnerString(ids.ToArray());
        }
        #endregion

        #endregion BLG_577 CSATOLMANY JOGOSULTSAG

        #region BLG_3217 TITKOSITOTT CSATOLMANYOK

        private const string ENCRYPT_KITITKOSITANDO_FAJL_TIPUSOKPKI = "ENCRYPT_KITITKOSITANDO_FAJL_TIPUSOK";
        private const string ENCRYPT_SAJAT_PRIVAT_KULCS_PATHPKI = "ENCRYPT_SAJAT_PRIVAT_KULCS_PATH";
        private const string ENCRYPT_SAJAT_PRIVAT_KULCS_PASSWORDKI = "ENCRYPT_SAJAT_PRIVAT_KULCS_PASSWORD";

        private static List<string> _kititkositandoFileExtensions = null;
        private static string privateKey;
        private static string privatePassword;

        static string batchFilePath;

        public static string BatchFilePath
        {
            get
            {
                return batchFilePath;
            }
        }

        static CsatolmanyokUtility()
        {
            batchFilePath = ConfigurationManager.AppSettings.Get("GPG_Titok_FilePath");
        }

        static string GetTempPath()
        {
            return Path.GetTempPath();
        }

        static string GetTempPath(string erkeztetoSzam)
        {
            return Path.Combine(GetTempPath(), erkeztetoSzam);
        }
        static string GetInputDir(string erkeztetoSzam)
        {
            return Path.Combine(GetTempPath(erkeztetoSzam), "In");
        }

        static string GetOutputDir(string erkeztetoSzam)
        {
            return Path.Combine(GetTempPath(erkeztetoSzam), "Out");
        }


        static void CreateDirectories(string name)
        {
            string inputDirectory = GetInputDir(name);
            CreateDirectory(inputDirectory);
            string outputDirectory = GetOutputDir(name);
            CreateDirectory(outputDirectory);
        }

        static void CreateDirectory(string directoryPath)
        {
            Logger.Debug(String.Format("CreateDirectory: {0}", directoryPath));
            Directory.CreateDirectory(directoryPath);
        }

        public static void Kititkosit(string filePath, string outPutDir, string fileName, string passwordFilePath, out string error)
        {
            Logger.Info(String.Format("CsatolmanyokUtility.Kititkosit indul: {0}, {1}", fileName, passwordFilePath));
            error = null;

            if (!File.Exists(batchFilePath))
            {
                error = String.Format("CsatolmanyokUtility.Kititkosit hiba, batch file nem talalhato: {0}", batchFilePath);
                Logger.Error(error);
                return;
            }

            try
            {
                string args = String.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\"", filePath, outPutDir, fileName, privateKey, passwordFilePath);

                Logger.Debug(String.Format("Process: {0} {1}", batchFilePath, args));

                ProcessStartInfo process = new ProcessStartInfo
                {
                    CreateNoWindow = false,
                    UseShellExecute = false,
                    //WorkingDirectory = msPath,
                    RedirectStandardOutput = true,
                    FileName = batchFilePath,
                    Arguments = args
                };

               
                //Process p = Process.Start(batchFilePath, args);
                //Process p = Process.Start(process);

                Process p = new Process();
                p.StartInfo = process;

                StringBuilder errorBuilder = new StringBuilder();
                p.ErrorDataReceived += delegate (object sender, DataReceivedEventArgs e)
                {
                    errorBuilder.Append(e.Data);
                };
                //call this before process start
                p.StartInfo.RedirectStandardError = true;
                
                p.Start();

                //call this after process start
                p.BeginErrorReadLine();


                string output = p.StandardOutput.ReadToEnd();
                Logger.Debug(String.Format("Process output: {0}", output));

                p.WaitForExit();
                int result = p.ExitCode;

                Logger.Debug(String.Format("ExitCode: {0}", result));
                Logger.Debug(String.Format("errorBuilder: {0}", errorBuilder));

                if (result == 0)
                {
                    Logger.Info(String.Format("CsatolmanyokUtility.Kititkosit vege"));
                }
                else
                {
                    error = String.Format("CsatolmanyokUtility.Kititkosit hiba, batch exit code: {0}", result);
                    Logger.Error(error);
                }
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                Logger.Error("CsatolmanyokUtility.Kititkosit hiba", ex);
            }
        }

        public static Stream ProcessFile(string fileName, byte[] fileContent, out string error)
        {
            Logger.Info(String.Format("CsatolmanyokUtility.ProcessFile indul: {0}", fileName));
            error = null;

            try
            {
                Logger.Debug("CreateDirectories");

                string folder = "Temp_" + fileName;

                CreateDirectories(folder);

                string inputDir = GetInputDir(folder);

                string tempFile = Path.Combine(GetInputDir(folder), fileName);

                Logger.Debug(String.Format("File mentese indul: {0}", tempFile));

                System.IO.DirectoryInfo di = new DirectoryInfo(inputDir);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }

                File.WriteAllBytes(tempFile, fileContent);

                Kititkosit(tempFile, inputDir, fileName, privatePassword, out error);

                FileStream stream = null;

                if (error == null)
                {
                    if (di.Exists)
                    {
                        FileInfo[] files = di.GetFiles("*.*", SearchOption.TopDirectoryOnly);
                        FileInfo info = files.FirstOrDefault(x => x.Extension != "enc");

                        if (info != null)
                        {
                            try
                            {
                                stream = info.Open(FileMode.Open);
                            }
                            catch (Exception ex)
                            {
                                error = ex.ToString();
                                Logger.Error("CsatolmanyokUtility.ProcessFile hiba", ex);
                            }
                        }
                    }
                    return stream;
                }
                else
                {
                    Logger.Error(String.Format("CsatolmanyokUtility.ProcessFile hiba: {0}", error));
                }
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                Logger.Error("CsatolmanyokUtility.ProcessFile hiba", ex);
            }

            return null;
        }


        public static List<string> GetKititkositandoFileExtensions(ExecParam execParam)
        {
            if (_kititkositandoFileExtensions == null)
            {
                _kititkositandoFileExtensions = new List<string>();
                string fileExtensions = Rendszerparameterek.Get(execParam, ENCRYPT_KITITKOSITANDO_FAJL_TIPUSOKPKI);
                if (!string.IsNullOrEmpty(fileExtensions))
                {
                    _kititkositandoFileExtensions.AddRange(fileExtensions.Split('|'));
                }
            }
            return _kititkositandoFileExtensions;
        }

        public static bool CanDecrypt(string fileExtension, Contentum.eBusinessDocuments.ExecParam execParam)
        {
            privateKey = Rendszerparameterek.Get(execParam, ENCRYPT_SAJAT_PRIVAT_KULCS_PATHPKI);
            privatePassword = Rendszerparameterek.Get(execParam, ENCRYPT_SAJAT_PRIVAT_KULCS_PASSWORDKI);

            if (GetKititkositandoFileExtensions(execParam).Contains(fileExtension) && !string.IsNullOrEmpty(privateKey) && !String.IsNullOrEmpty(privatePassword))
            {
                return true;
            }
            return false;
        }

        #endregion

    }
}
