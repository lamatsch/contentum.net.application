﻿using Contentum.eUtility.Models;
using System;
using System.IO;

namespace Contentum.eUtility
{
    /// <summary>
    /// Helper manual kezelő
    /// </summary>
    public class HelperManualUtility
    {
        public static string HelpCacheKey = "HelperManualKey";
        private const string HelpFolderName = "Help/Osszerendeles";
        private const string HelpBinderFileName = "HelperManualOsszerendelo.json";
        public HelperManualModelContainerClass ItemsContainer { get; set; }
        public string RootPath { get; set; }

        public HelperManualUtility()
        {

        }
        public HelperManualUtility(string rootPath)
        {
            this.RootPath = rootPath;
            LoadBinderFile();
        }
        /// <summary>
        /// Összerendelő file beolvasása
        /// </summary>
        public void LoadBinderFile()
        {
#warning Cache-elni kell !
            string helpFolderPath = Path.Combine(RootPath, HelpFolderName);
            if (!Directory.Exists(helpFolderPath))
                throw new FileNotFoundException("Help mappa nem található:" + helpFolderPath);

            string filePath = Path.Combine(helpFolderPath, HelpBinderFileName);
            if (!File.Exists(filePath))
                throw new FileNotFoundException("Help összerendelő file nem található:" + filePath);

            string helpBinderFileContent = File.ReadAllText(filePath);
            if (string.IsNullOrEmpty(helpBinderFileContent))
                throw new FileNotFoundException("Help összerendelő file tartalma üres:" + filePath);

            ItemsContainer = JSONUtilityFunctions.DeSerializeHelperManualModelObj(helpBinderFileContent);
            if (ItemsContainer == null)
                throw new FormatException("Help file tartalma nem volt értelmezhető !");
        }
        /// <summary>
        /// Visszaadja a topic id-t a paraméterek alapján
        /// </summary>
        /// <param name="form"></param>
        /// <param name="mode"></param>
        /// <param name="command"></param>
        /// <param name="startup"></param>
        /// <returns></returns>
        public string GetTopicId(string form, string mode, string command, string startup)
        {
            if (ItemsContainer == null || ItemsContainer.Collection == null || ItemsContainer.Collection.Count < 1)
                return null;
            HelperManualModelClass model = new HelperManualModelClass()
            {
                Form = form != null ? form.Trim() : form,
                Mode = mode != null ? mode.Trim() : mode,
                Command = command != null ? command.Trim() : command,
                Startup = startup != null ? startup.Trim() : startup,
            };
            int index = ItemsContainer.Collection.IndexOf(model);

            if (index < 0)
                return null;

            return ItemsContainer.Collection[index].TopicId;
        }
        /// <summary>
        /// GetTopicId
        /// </summary>
        /// <param name="itemsContainer"></param>
        /// <param name="form"></param>
        /// <param name="mode"></param>
        /// <param name="command"></param>
        /// <param name="startup"></param>
        /// <returns></returns>
        public static string GetTopicId(HelperManualModelContainerClass itemsContainer, string form, string mode, string command, string startup)
        {
            if (itemsContainer == null || itemsContainer.Collection == null || itemsContainer.Collection.Count < 1)
                return null;
            HelperManualModelClass model = new HelperManualModelClass()
            {
                Form = form != null ? form.Trim() : form,
                Mode = mode != null ? mode.Trim() : mode,
                Command = command != null ? command.Trim() : command,
                Startup = startup != null ? startup.Trim() : startup,
            };
            int index = itemsContainer.Collection.IndexOf(model);

            if (index < 0)
                return null;

            return itemsContainer.Collection[index].TopicId;
        }
    }
}
