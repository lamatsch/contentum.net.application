﻿using Contentum.eUtility.EKF.Models;
using Contentum.eUtility.EKF;
using Contentum.eUtility.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Contentum.eUtility
{
    public class JSONUtilityFunctions
    {
        /// <summary>
        /// SerializeSakkoraModelObj
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string SerializeSakkoraModelObj(SakkoraModel item)
        {
            string result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.SerializeObject(item, settings);
            }
            catch (Exception)
            {

            }
            return result;
        }
        /// <summary>
        /// DeSerializeSakkoraModelObj
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static SakkoraModel DeSerializeSakkoraModelObj(string json)
        {
            SakkoraModel result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.DeserializeObject<SakkoraModel>(json, settings);
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.Message);
            }
            return result;
        }

        /// <summary>
        /// SerializeHelperManualModelObj
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string SerializeHelperManualModelObj(HelperManualModelContainerClass item)
        {
            string result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.SerializeObject(item, settings);
            }
            catch (Exception)
            {

            }
            return result;
        }
        /// <summary>
        /// DeSerializeHelpermanualModelObj
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static HelperManualModelContainerClass DeSerializeHelperManualModelObj(string json)
        {
            HelperManualModelContainerClass result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.DeserializeObject<HelperManualModelContainerClass>(json, settings);
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.Message);
            }
            return result;
        }

        /// <summary>
        /// SerializeEKFTargyszavakList
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string SerializeEKFTargyszavakList(List<EKF_ObjTargyszavakModel> item)
        {
            string result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.SerializeObject(item, settings);
            }
            catch (Exception)
            {

            }
            return result;
        }
        /// <summary>
        /// DeSerializeEKFTargyszavakList
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static List<EKF_ObjTargyszavakModel> DeSerializeEKFTargyszavakList(string json)
        {
            List<EKF_ObjTargyszavakModel> result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.DeserializeObject<List<EKF_ObjTargyszavakModel>>(json, settings);
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.Message);
            }
            return result;
        }

        /// <summary>
        /// SerializeEKFTargyModelObj
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string SerializeEKFTargyModelObj(List<EKF_ComplexTargyModel> item)
        {
            string result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.SerializeObject(item, settings);
            }
            catch (Exception)
            {

            }
            return result;
        }
        /// <summary>
        /// DeSerializeEKFTargyModelObj
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static List<EKF_ComplexTargyModel> DeSerializeEKFTargyModelObj(string json)
        {
            List<EKF_ComplexTargyModel> result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.DeserializeObject<List<EKF_ComplexTargyModel>>(json, settings);
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.Message);
            }
            return result;
        }

        /// <summary>
        /// SerializeEKFDefaultTargyModelObj
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string SerializeEKFDefaultTargyModelObj(EKF_DefaultTargyModel item)
        {
            string result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.SerializeObject(item, settings);
            }
            catch (Exception)
            {

            }
            return result;
        }
        /// <summary>
        /// DeSerializeEKFDefaultTargyModelObj
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static EKF_DefaultTargyModel DeSerializeEKFDefaultTargyModelObj(string json)
        {
            EKF_DefaultTargyModel result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.DeserializeObject<EKF_DefaultTargyModel>(json, settings);
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.Message);
            }
            return result;
        }




        #region EKFConditionOperatorInXML
        /// <summary>
        /// SerializeEKFConditionOperatorInXMLObj
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string SerializeEKFConditionOperatorInXMLObj(List<EKFConditionForXML> item)
        {
            string result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.SerializeObject(item, settings);
            }
            catch (Exception)
            {

            }
            return result;
        }
        /// <summary>
        /// DeSerializeEKFConditionOperatorInXMLObj
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static List<EKFConditionForXML> DeSerializeEKFConditionOperatorInXMLObj(string json)
        {
            List<EKFConditionForXML> result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.DeserializeObject<List<EKFConditionForXML>>(json, settings);
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.Message);
            }
            return result;
        }
        #endregion
    }
}
