using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using System.Configuration;
using System.Web;
using System.Threading;

namespace Contentum.eUtility
{
    public class Notify
    {
        public static string GetAssemblies()
        {
            bool IsInWebConfig = false;

            System.Web.Configuration.CompilationSection compilationSection = (System.Web.Configuration.CompilationSection)ConfigurationManager.GetSection("system.web/compilation");
            System.Web.Configuration.AssemblyCollection assemblies = compilationSection.Assemblies;

            string list = "<ul>";

            foreach (System.Web.Configuration.AssemblyInfo assemblie in assemblies)
            {
                if (IsInWebConfig)
                    list += "<li>" + assemblie.Assembly + "</li>";

                if (assemblie.Assembly == "*")
                    IsInWebConfig = true;
            }

            list += "</ul>";

            return list;
        }

        public static bool SendSelectedItemsByEmail(Page ParentPage, List<string> ItemsList, List<string> KRT_FelhasznalokList, String Message, String TableName)
        {
            return SendSelectedItemsByEmail(ParentPage, ItemsList, KRT_FelhasznalokList, Message, TableName, "");
        }

        public static bool SendSelectedItemsByEmail(Page ParentPage, List<string> ItemsList, List<string> EmailAddressList, String Message, String TableName, String QueryString)
        {
            Contentum.eBusinessDocuments.ExecParam ExecParam = UI.SetExecParamDefault(ParentPage, new Contentum.eBusinessDocuments.ExecParam());
            Boolean _ret = false;
            if (ItemsList == null || ItemsList.Count <= 0)
            {
                return false;
            }
            if (EmailAddressList == null || EmailAddressList.Count <= 0)
            {
                return false;
            }

            try
            {
                Contentum.eAdmin.Service.EmailService service = eAdminService.ServiceFactory.GetEmailService();
                Contentum.eBusinessDocuments.ExecParam[] ExecParams = new Contentum.eBusinessDocuments.ExecParam[ItemsList.Count];
                
                for (int i = 0; i < ItemsList.Count; i++)
                {
                    ExecParams[i] = UI.SetExecParamDefault(ParentPage, new Contentum.eBusinessDocuments.ExecParam());
                    ExecParams[i].Record_Id = ItemsList[i];
                }
                Contentum.eAdmin.Service.KRT_FelhasznalokService KRT_FelhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                
                Contentum.eBusinessDocuments.KRT_Felhasznalok[] KRT_Felhasznalok = new Contentum.eBusinessDocuments.KRT_Felhasznalok[EmailAddressList.Count];                
                
                
                for (int i = 0; i < EmailAddressList.Count; i++)
                {
                    string email = EmailAddressList[i];
                    email = email.Replace("(", "<");
                    email = email.Replace(")", ">");                
                    
                    string xemail = email.Split('<')[1];
                    xemail = xemail.Substring(0, xemail.Length - 1);

                    Contentum.eBusinessDocuments.Result _res = KRT_FelhasznalokService.GetByEmail(ExecParam, xemail);
                    if (_res.IsError)
                    {
                        Logger.Error("SendEmail hiba", ExecParam, _res);
                        return false;
                    }   
                    KRT_Felhasznalok[i] = (Contentum.eBusinessDocuments.KRT_Felhasznalok)_res.Record;
                    KRT_Felhasznalok[i].EMail = email;
                    
                }

                // teszt email sajat magunknak: ide egy for ciklus kell az osszes cimzett eloszedesehez
                _ret = service.SendObjectsLinkToEmail(ExecParams, KRT_Felhasznalok, Message, TableName, QueryString);
            }
            catch(Exception e)
            {
                Result resError = ResultException.GetResultFromException(e);
                
                return false;
            }
            if (!_ret)
            {
                ParentPage.ClientScript.RegisterStartupScript(ParentPage.GetType(), "EmailError", "alert('Hiba!\nNem siker�lt elk�ldeni minden c�mzettnek a levelet!');", true);
            }
            return _ret;
        }

        public static bool SendErrorInEmail(Page page, string ErrorCode, string ErrorMessage, string CurrentLogEntry)
        {
            if (page == null) return false;

            ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
            string notifiedAddress = Rendszerparameterek.Get(execParam, Rendszerparameterek.HIBABEJELENTES_EMAIL_CIM);
            string url = page.Request.Url.ToString();
            string errorCode = String.IsNullOrEmpty(ErrorCode) ? Resources.Error.GetString("EmptyErrorCode") : ErrorCode;
            string errorMessage = String.IsNullOrEmpty(ErrorMessage) ? Resources.Error.GetString("EmptyErrorMessage") : ErrorMessage;
            string currentLogEntry = CurrentLogEntry;
            string desciption = "A rendszer �ltal haszn�lt szerelv�nyek list�ja: <br/> " + GetAssemblies();

            EmailService service = eAdminService.ServiceFactory.GetEmailService();
            return service.SendErrorInMail(execParam, notifiedAddress, url, errorCode, errorMessage,currentLogEntry, desciption);
        }

        public static bool SendAnswerEmail(ExecParam execParam, string kuldemeny_id, string ugyirat_id)
        {
            bool rv = false;
            Contentum.eAdmin.Service.EmailService service = eAdminService.ServiceFactory.GetEmailService();
            rv = service.SendAnswerEmail(execParam, kuldemeny_id, ugyirat_id);
            return rv;
        }
        
        public static Result SendAnswerDeclinedEmail(ExecParam execParam, EREC_eMailBoritekok erec_eMailBoritekok, String FullErkeztetoSzam, String MegtagadasIndoka)
        {
            Contentum.eAdmin.Service.EmailService service = eAdminService.ServiceFactory.GetEmailService();
            Result result = service.SendAnswerDeclinedEmail(execParam, erec_eMailBoritekok, FullErkeztetoSzam, MegtagadasIndoka);
            return result;
        }

        public static bool SendHataridosErtesites(ExecParam execParam, EREC_HataridosErtesitesek erec_HataridosErtesitesek)
        {
            List<EREC_HataridosErtesitesek> erec_HataridosErtesitesekList = new List<EREC_HataridosErtesitesek>(1);
            erec_HataridosErtesitesekList.Add(erec_HataridosErtesitesek);
            return SendHataridosErtesites(execParam, erec_HataridosErtesitesekList);
        }

        public static bool SendHataridosErtesites(ExecParam execParam, List<EREC_HataridosErtesitesek> erec_HataridosErtesitesekList)
        {
            try
            {
                List<EREC_HataridosErtesitesek> erec_HataridosErtesitesekListKozepes = new List<EREC_HataridosErtesitesek>();
                List<EREC_HataridosErtesitesek> erec_HataridosErtesitesekListMagas = new List<EREC_HataridosErtesitesek>();

                int magas = Int32.Parse(KodTarak.FELADAT_Prioritas.Magas);
                int kozepes = Int32.Parse(KodTarak.FELADAT_Prioritas.Kozepes);

                foreach (EREC_HataridosErtesitesek erec_HataridosErtesitesek in erec_HataridosErtesitesekList)
                {
                    if (!String.IsNullOrEmpty(erec_HataridosErtesitesek.Prioritas))
                    {
                        int prioritas;
                        if (Int32.TryParse(erec_HataridosErtesitesek.Prioritas, out prioritas))
                        {
                            if (prioritas >= magas)
                            {
                                erec_HataridosErtesitesekListMagas.Add(erec_HataridosErtesitesek);
                                continue;
                            }

                            if (prioritas >= kozepes)
                            {
                                erec_HataridosErtesitesekListKozepes.Add(erec_HataridosErtesitesek);
                                continue;
                            }
                        }
                    }
                }

                if (erec_HataridosErtesitesekListMagas.Count > 0)
                {
                    //E-mail kuldes
                    ExecParam xpmEmail = execParam.Clone();
                    xpmEmail.CallingChain = new CallingChain();
                    if (execParam.CallingChain.Count > 0)
                    {
                        xpmEmail.CallingChain.Push(execParam.CallingChain.Peek());
                    }
                    Contentum.eAdmin.Service.EmailService emailService = eAdminService.ServiceFactory.GetEmailService();
                    emailService.BeginSendHataridosErtesitesEmailTomeges(xpmEmail, erec_HataridosErtesitesekListMagas.ToArray(), null, null);
                }

                if (erec_HataridosErtesitesekListKozepes.Count > 0)
                {
                    //�zenet k�ld�s
                    ExecParam xpmMessage = execParam.Clone();
                    xpmMessage.CallingChain = new CallingChain();
                    if (xpmMessage.CallingChain.Count > 0)
                    {
                        xpmMessage.CallingChain.Push(execParam.CallingChain.Peek());
                    }
                    Contentum.eAdmin.Service.MessageService messageService = eAdminService.ServiceFactory.GetMessageService();
                    messageService.BeginSendHataridosErtesitesMessageTomeges(xpmMessage, erec_HataridosErtesitesekListKozepes.ToArray(), null, null);
                }
            }
            catch(Exception ex)
            {
                Result resError = ResultException.GetResultFromException(ex);
                Logger.Error("SendHataridosErtesites hiba", execParam, resError);
                return false;
            }

            return true;
        }
    }

}
