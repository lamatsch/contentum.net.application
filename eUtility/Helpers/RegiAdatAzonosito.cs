﻿using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eQuery;
using System;

namespace Contentum.eUtility
{

    /// <summary>
    /// RegiAdatAzonosito
    /// </summary>
    public class RegiAdatAzonosito
    {
        public const string delimeter = "/";
        public const char PREFIX_SAV = '(';
        public const char POSTFIX_SAV = ')';

        public static string GetAzonosito(string sav, string ev, string foszam)
        {
            string azonosito;
            azonosito = String.Format("({0}) {1}{2}{3}{4}", new string[5] { sav, delimeter, foszam, delimeter, ev });
            return azonosito;
        }

        /// <summary>
        /// Get Azonosito Darabok
        /// </summary>
        /// <param name="azonosito"></param>
        /// <returns></returns>
        public static string[] GetAzonositoDarabok(string azonosito)
        {
            if (string.IsNullOrEmpty(azonosito))
                throw new FormatException("Az Azonosító üres !");

            string[] azonositoDarabok = azonosito.Split(delimeter.ToCharArray());

            if (azonositoDarabok.Length < 3)
                throw new FormatException("Az Azonosító nem megfelelo formatumu: " + azonosito);

            if (azonositoDarabok.Length == 3)
            {
                return azonositoDarabok;
            }
            else
            {
                string[] result = new string[3];
                result[2] = azonositoDarabok[azonositoDarabok.Length - 1];
                result[1] = azonositoDarabok[azonositoDarabok.Length - 2];
                result[0] = string.Join(delimeter, azonositoDarabok, 0, azonositoDarabok.Length - 2).Trim();
                return result;
            }
        }

        /// <summary>
        /// Get Sav
        /// </summary>
        /// <param name="azonosito"></param>
        /// <returns></returns>
        public static string GetSav(string azonosito)
        {
            string[] azonositoDarabok = GetAzonositoDarabok(azonosito);

            string sav = azonositoDarabok[0].Trim();
            sav = sav.TrimEnd(POSTFIX_SAV);
            sav = sav.TrimStart(PREFIX_SAV);

            return sav;
        }

        /// <summary>
        /// Get Ev
        /// </summary>
        /// <param name="azonosito"></param>
        /// <returns></returns>
        public static string GetEv(string azonosito)
        {
            string[] azonositoDarabok = GetAzonositoDarabok(azonosito);

            return azonositoDarabok[2];
        }

        /// <summary>
        /// Get Foszam
        /// </summary>
        /// <param name="azonosito"></param>
        /// <returns></returns>
        public static string GetFoszam(string azonosito)
        {
            string[] azonositoDarabok = GetAzonositoDarabok(azonosito);

            return azonositoDarabok[1];
        }

        public static void SetSerchObject(string azonosito, MIG_FoszamSearch search)
        {
            Logger.Debug("Az azonosito felbontasa");
            string sav = GetSav(azonosito);
            Logger.Debug(String.Format("A kapott sav: {0}", sav));
            string ev = GetEv(azonosito);
            Logger.Debug(String.Format("A kapott ev: {0}", ev));
            string foszam = GetFoszam(azonosito);
            Logger.Debug(String.Format("A kapott foszam: {0}", foszam));

            search.EdokSav.Filter(sav);
            search.UI_YEAR.Filter(ev);
            search.UI_NUM.Filter(foszam);
        }

        public static void SetSearchObjectSav(MIG_FoszamSearch search)
        {
            string sav = search.UI_SAV.Value.Trim();
            string savOperator = search.UI_SAV.Operator;
            Logger.Debug("Sav value: " + sav);
            Logger.Debug("Sav operator: " + savOperator);
            search.UI_SAV.Clear();
            if (String.IsNullOrEmpty(sav) || savOperator != Query.Operators.equals)
            {
                Logger.Debug("SetSearchObjectSav return");
                return;
            }

            search.EdokSav.Value = sav;
            search.EdokSav.Operator = savOperator;
        }
    }
}