﻿using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eUtility.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eUtility
{
    public class SharePointUploadDTO
    {
        public SharePointUploadDTO(ePostaTertivevenyDataPdfClass pdf)
        {
            Name = pdf.PdfFileName;
            Content = pdf.PdfFileContent;
        }
        public string Name { get; set; }
        public byte[] Content { get; set; }
    }

    public class SharePointUtil
    {

        public static Result UploadToSharePoint(ExecParam execParam, SharePointUploadDTO csatolmany)
        {

            // KRT_Dokumentumok objektum létrehozása
            KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();

            DocumentService documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();
            documentService.Timeout = (int)TimeSpan.FromMinutes(20).TotalMilliseconds;


            String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                    "<iktatokonyv></iktatokonyv>" +
                                                    "<sourceSharePath></sourceSharePath>" +
                                                    "<foszam>{0}</foszam>" +
                                                    "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                    "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                    "<megjegyzes></megjegyzes>" +
                                                    "<munkaanyag>{2}</munkaanyag>" +
                                                    "<ujirat>{3}</ujirat>" +
                                                    "<vonalkod></vonalkod>" +
                                                    "<docmetaIratId></docmetaIratId>" +
                                                    "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                    "<ucmiktatokonyv>{5}</ucmiktatokonyv>" +
                                                "</uploadparameterek>"
                                                , String.Empty
                                                , String.Empty
                                                , "NEM"
                                                , "IGEN"
                                                , "IGEN"
                                                , "HK"
                                                );

            Result result_upload = documentService.UploadFromeRecordWithCTTCheckin
                (
                   execParam
                   , Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
                   , csatolmany.Name
                   , csatolmany.Content
                   , krt_Dokumentumok
                   , uploadXmlStrParams
                );
            
            return result_upload;
        }
    }
}
