﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Contentum.eUtility
{
  public class TextUtils
    {

        public static string JoinWith(char joinChar,string string1, string string2)
        {
            if(string.IsNullOrEmpty(string1) && string.IsNullOrEmpty(string2))
            {
                return string.Empty;
            }
            if(string.IsNullOrEmpty(string1))
            {
                return string2;
            }
            if(string.IsNullOrEmpty(string2))
            {
                return string1;
            }
            return string1 + joinChar + string2;
        }

        public static string getTextFromHTML(string input)
        {
            List<string> findedElements = new List<string>();

            if (!input.Contains("<"))
            {
                return input;
            }
            else
            {
                if (input.IndexOf("<") > 0)
                {
                    findedElements.Add(input.Substring(0, input.IndexOf("<")));
                    input = input.Substring(input.IndexOf("<"), input.Length - input.IndexOf("<"));
                }
            }

            Regex img_alt_Regex = new Regex(@"(\balt="")[^""]*");
            Match img_alt_RegexMatch = img_alt_Regex.Match(input);
            if (!String.IsNullOrEmpty(img_alt_RegexMatch.Value))
            { findedElements.Add(img_alt_RegexMatch.Value.Substring(img_alt_RegexMatch.Value.IndexOf("=") + 2)); }

            //rossz link a küldemények lsitán: <a> <a />
            Regex link_Regex = new Regex(@"(<a.*>)(.*)(<a \/>)");
            MatchCollection link_Regex_MatchCollection = link_Regex.Matches(input);
            if (link_Regex_MatchCollection.Count > 0)
            {
                foreach (Match _item in link_Regex_MatchCollection)
                {
                    findedElements.Add(_item.Groups[2].Value);
                }
            }

            link_Regex = new Regex(@"(<a.*>)(.*)(<\/a>)");
            link_Regex_MatchCollection = link_Regex.Matches(input);
            if (link_Regex_MatchCollection.Count > 0)
            {
                foreach (Match _item in link_Regex_MatchCollection)
                {
                    findedElements.Add(_item.Groups[2].Value);
                }
            }

            if (findedElements.Count > 1)
            {
                return string.Join("; ", findedElements.ToArray());
            }
            else
            {
                return findedElements[0];
            }
        }
    }
}
