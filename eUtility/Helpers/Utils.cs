﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Xml;
using Parameter = Contentum.eBusinessDocuments.Utility.Parameter;

namespace Contentum.eUtility
{
    public class Utils
    {
        public static string GetStringFromArray(string[] array, string Delimeter)
        {
            if (array.Length == 0)
                return String.Empty;

            if (array.Length == 1)
                return array[0];

            int capacity = (2 * array.Length) - 1;
            StringBuilder sb = new StringBuilder(capacity);

            for (int i = 0; i < array.Length; i++)
            {
                if (i > 0)
                {
                    sb.Append(Delimeter);
                }

                sb.Append(array[i]);
            }

            return sb.ToString();
        }

        public static string GetQueryStringOriginalParts(Page page, string[] ignoreKeys)
        {
            StringBuilder sb = new StringBuilder();
            foreach (String key in page.Request.QueryString.AllKeys)
            {
                if (ignoreKeys == null || (Array.IndexOf(ignoreKeys, key) < 0 && !String.IsNullOrEmpty(page.Request.QueryString.Get(key))))
                {
                    sb.Append("&");
                    sb.Append(key);
                    sb.Append("=");
                    sb.Append(page.Request.QueryString.Get(key));
                }
            }

            if (sb.Length > 1)
            {
                return sb.ToString().Substring(1);
            }
            else
            {
                return "";
            }
        }

        public static string GetViewRedirectQs(Page page, string[] ignoreKeys)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(QueryStringVars.Command + "=" + CommandName.View);

            string[] ignoreKeysWithCommand = null;

            if (ignoreKeys == null)
            {
                ignoreKeysWithCommand = new string[] { QueryStringVars.Command };
            }
            else if (Array.IndexOf(ignoreKeys, QueryStringVars.Command) < 0)
            {
                ignoreKeysWithCommand = new string[ignoreKeys.Length + 1];
                ignoreKeysWithCommand[0] = QueryStringVars.Command;
                Array.Copy(ignoreKeys, 0, ignoreKeysWithCommand, 1, ignoreKeys.Length);
            }
            sb.Append("&");
            sb.Append(GetQueryStringOriginalParts(page, ignoreKeysWithCommand));
            return sb.ToString();
        }

        /// <summary>
        /// GuidTryParse
        /// </summary>
        /// <param name="s"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool IsValidGuid(string s)
        {
            if (s == null)
                return false;

            System.Text.RegularExpressions.Regex format = new System.Text.RegularExpressions.Regex(
                "^[A-Fa-f0-9]{32}$|" +
                "^({|\\()?[A-Fa-f0-9]{8}-([A-Fa-f0-9]{4}-){3}[A-Fa-f0-9]{12}(}|\\))?$|" +
                "^({)?[0xA-Fa-f0-9]{3,10}(, {0,1}[0xA-Fa-f0-9]{3,6}){2}, {0,1}({)([0xA-Fa-f0-9]{3,4}, {0,1}){7}[0xA-Fa-f0-9]{3,4}(}})$");
            System.Text.RegularExpressions.Match match = format.Match(s);
            if (match.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #region refactored from Utility.cs files in App_Code folders:

        public static void AddExtendedSearchObjectToGetAllStoredProcedure(SqlCommand SqlComm, string extendedWhereParamterName, object searchObject, string searchObjectWhereByManual)
        {
            if (searchObject != null && !String.IsNullOrEmpty(extendedWhereParamterName))
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(searchObject);
                query.Where = Search.ConcatWhereExpressions(query.Where, searchObjectWhereByManual);

                if (!String.IsNullOrEmpty(query.Where))
                {
                    SqlComm.Parameters.Add(extendedWhereParamterName, SqlDbType.NVarChar).Value = query.Where;
                }
            }
        }

        public static void LoadBusinessDocumentFromDataAdapter(object BusinessDocument, SqlCommand sqlcommand)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = sqlcommand;
            da.Fill(ds);

            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    PropertyInfo[] Properties = BusinessDocument.GetType().GetProperties();
                    foreach (PropertyInfo P in Properties)
                    {
                        //dt.Rows[0][GetOrdinal(P.Name)];
                        if (!(ds.Tables[0].Rows[0].IsNull(P.Name)))
                        {
                            P.SetValue(BusinessDocument, ds.Tables[0].Rows[0][P.Name].ToString(), null);
                        }
                    }
                    FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
                    object BaseObject = BaseField.GetValue(BusinessDocument);

                    PropertyInfo[] BaseProperties = BaseObject.GetType().GetProperties();

                    foreach (PropertyInfo P in BaseProperties)
                    {
                        if (!(ds.Tables[0].Rows[0].IsNull(P.Name)))
                        {
                            P.SetValue(BaseObject, ds.Tables[0].Rows[0][P.Name].ToString(), null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                da.Dispose();
            }
        }

        public static void LoadBusinessDocumentFromDataRow(object BusinessDocument, DataRow row)
        {
            try
            {
                if (row != null)
                {
                    PropertyInfo[] Properties = BusinessDocument.GetType().GetProperties();
                    foreach (PropertyInfo P in Properties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BusinessDocument, row[P.Name].ToString(), null);
                            }
                        }
                    }
                    FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
                    object BaseObject = BaseField.GetValue(BusinessDocument);

                    PropertyInfo[] BaseProperties = BaseObject.GetType().GetProperties();

                    foreach (PropertyInfo P in BaseProperties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BaseObject, row[P.Name].ToString(), null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void LoadUpdateBusinessDocumentFromDataRow(object BusinessDocument, DataRow row)
        {
            try
            {
                if (row != null)
                {
                    PropertyInfo[] Properties = BusinessDocument.GetType().GetProperties();
                    FieldInfo UpdatedField = BusinessDocument.GetType().GetField("Updated");
                    object UpdatedObject = UpdatedField.GetValue(BusinessDocument);
                    foreach (PropertyInfo P in Properties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BusinessDocument, row[P.Name].ToString(), null);
                                PropertyInfo UpdatedProperty = UpdatedObject.GetType().GetProperty(P.Name);
                                if (UpdatedProperty != null)
                                {
                                    UpdatedProperty.SetValue(UpdatedObject, true, null);
                                }
                            }
                        }
                    }
                    FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
                    object BaseObject = BaseField.GetValue(BusinessDocument);

                    PropertyInfo[] BaseProperties = BaseObject.GetType().GetProperties();

                    foreach (PropertyInfo P in BaseProperties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BaseObject, row[P.Name].ToString(), null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool LoadBusinessDocumentToParameter(object BusinessDocument, SqlCommand SqlComm, Dictionary<String, Parameter> Parameters)
        {
            if (BusinessDocument != null && SqlComm != null)
            {
                SqlComm.CommandType = CommandType.StoredProcedure;
                try
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    XmlElement rootNode = xmlDocument.CreateElement("root");
                    xmlDocument.AppendChild(rootNode);


                    FieldInfo TypedField = BusinessDocument.GetType().GetField("Typed");
                    object TypedObject = TypedField.GetValue(BusinessDocument);
                    PropertyInfo[] TypedProperties = TypedObject.GetType().GetProperties();


                    FieldInfo UpdatedField = BusinessDocument.GetType().GetField("Updated");
                    object UpdatedObject = UpdatedField.GetValue(BusinessDocument);

                    foreach (PropertyInfo P in TypedProperties)
                    {
                        Parameter _Par = new Parameter();
                        Parameters.TryGetValue(P.Name, out _Par);

                        if (_Par != null)
                        {
                            PropertyInfo UpdatedProperty = UpdatedObject.GetType().GetProperty(P.Name);
                            bool CanUpdate = (bool)UpdatedProperty.GetValue(UpdatedObject, null);

                            if (CanUpdate)
                            {
                                XmlElement childNode = xmlDocument.CreateElement(P.Name);
                                rootNode.AppendChild(childNode);

                                SqlComm.Parameters.Add(new SqlParameter(_Par.Name, GetSqlType(_Par.Type)));
                                SqlComm.Parameters[_Par.Name].Size = _Par.Length;
                                SqlComm.Parameters[_Par.Name].Value = P.GetValue(TypedObject, null);
                            }
                        }
                    }

                    FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
                    object BaseObject = BaseField.GetValue(BusinessDocument);
                    FieldInfo BaseTypedField = BaseObject.GetType().GetField("Typed");
                    object BaseTypedObject = BaseTypedField.GetValue(BaseObject);
                    PropertyInfo[] BaseTypedProperties = BaseTypedObject.GetType().GetProperties();


                    FieldInfo BaseUpdatedField = BaseObject.GetType().GetField("Updated");
                    object BaseUpdatedObject = BaseUpdatedField.GetValue(BaseObject);

                    foreach (PropertyInfo P in BaseTypedProperties)
                    {
                        Parameter _Par = new Parameter();
                        Parameters.TryGetValue(P.Name, out _Par);

                        if (_Par != null && !rootNode.InnerXml.Contains("<" + P.Name + " />"))
                        {
                            PropertyInfo BaseUpdatedProperty = BaseUpdatedObject.GetType().GetProperty(P.Name);
                            bool CanUpdate = (bool)BaseUpdatedProperty.GetValue(BaseUpdatedObject, null);

                            if (CanUpdate)
                            {
                                XmlElement childNode = xmlDocument.CreateElement(P.Name);
                                rootNode.AppendChild(childNode);

                                SqlComm.Parameters.Add(new SqlParameter(_Par.Name, GetSqlType(_Par.Type)));
                                SqlComm.Parameters[_Par.Name].Size = _Par.Length;
                                SqlComm.Parameters[_Par.Name].Value = P.GetValue(BaseTypedObject, null);
                            }
                        }
                    }

                    SqlComm.Parameters.Add(new SqlParameter("@UpdatedColumns", SqlDbType.Xml));
                    SqlComm.Parameters["@UpdatedColumns"].Value = xmlDocument.InnerXml.ToString();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return true;
        }

        public static bool IsNeedUpdateBusinessDocument(object BusinessDocumentCurrent, object BusinessDocumentUpdate)
        {
            if (BusinessDocumentCurrent.GetType() != BusinessDocumentUpdate.GetType())
            {
                throw new ResultException("Üzleti objektumok típusa nem azonos!");
            }
            Type businessDocumentType = BusinessDocumentCurrent.GetType();
            PropertyInfo[] Properties = businessDocumentType.GetProperties();
            FieldInfo UpdatedField = businessDocumentType.GetField("Updated");
            object UpdatedObject = UpdatedField.GetValue(BusinessDocumentUpdate);

            foreach (PropertyInfo P in Properties)
            {
                PropertyInfo UpdatedProperty = UpdatedObject.GetType().GetProperty(P.Name);
                bool needUpdate = (bool)UpdatedProperty.GetValue(UpdatedObject, null);

                if (needUpdate)
                {
                    string currentValue = (string)P.GetValue(BusinessDocumentCurrent, null);
                    string updateValue = (string)P.GetValue(BusinessDocumentUpdate, null);
                    if (currentValue != updateValue)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static Result ProcessEsmenyekTriggerResult(DataContext dataContext, ExecParam p_ExecParam, Result p_result)
        {
            Result res = new Result();

            if (p_result.IsError)
            {
                Logger.Error("result.IsError");
                throw new ResultException(p_result);
            }

            bool isResult = true;
            if (p_result.Ds == null || p_result.Ds.Tables.Count == 0 || p_result.Ds.Tables[0].Rows.Count == 0)
            {
                Logger.Debug("isResult = false");
                isResult = false;
            }

            if (isResult)
            {
                Logger.Debug("isResult");
                List<EREC_HataridosErtesitesek> erec_HataridosErtesitesekList = new List<EREC_HataridosErtesitesek>();
                foreach (DataTable table in p_result.Ds.Tables)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        //DataRow row = p_result.Ds.Tables[0].Rows[0];

                        string FuttatandoFunkcio = row["Funkcio_Futtatando"].ToString();
                        Logger.Debug("FuttatandoFunkcio: " + FuttatandoFunkcio);

                        switch (FuttatandoFunkcio)
                        {
                            case "FeladatNew":
                                {
                                    Logger.Debug("case FeladatNew");
                                    Contentum.eRecord.Service.EREC_HataridosFeladatokService service = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                                    ExecParam xpm = p_ExecParam.Clone();
                                    Logger.Debug("LoadBusinessDocumentFromDataRow EREC_HataridosFeladatok");
                                    EREC_HataridosFeladatok erec_hatartidosFeladat = new EREC_HataridosFeladatok();
                                    Utility.LoadBusinessDocumentFromDataRow(erec_hatartidosFeladat, row);

                                    //Logger.Debug("LoadBusinessDocumentFromDataRow EREC_Hatarid_Objektumok");
                                    //EREC_Hatarid_Objektumok erec_hataridosObjektum = new EREC_Hatarid_Objektumok();
                                    //Utility.LoadBusinessDocumentFromDataRow(erec_hataridosObjektum, row);

                                    //List<EREC_Hatarid_Objektumok> erec_hataridosObjektumokList = new List<EREC_Hatarid_Objektumok>(1);
                                    //erec_hataridosObjektumokList.Add(erec_hataridosObjektum);

                                    res = service.Insert(xpm, erec_hatartidosFeladat);
                                    if (res.IsError)
                                    {
                                        Logger.Error("EREC_HataridosFeladatokService Insert hiba", xpm, res);
                                        throw new ResultException(res);
                                    }
                                }
                                break;
                            case "FeladatModify":
                                {
                                    Logger.Debug("case FeladatModify");
                                    string feladatID = row["Feladat_Id"].ToString();
                                    Logger.Debug("FeleadatID" + feladatID);
                                    if (!String.IsNullOrEmpty(feladatID))
                                    {
                                        Contentum.eRecord.Service.EREC_HataridosFeladatokService service = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                                        ExecParam xpm = p_ExecParam.Clone();
                                        xpm.Record_Id = feladatID;

                                        res = service.Get(xpm);

                                        if (res.IsError)
                                        {
                                            Logger.Error("EREC_HataridosFeladatokService hiba", xpm, res);
                                            throw new ResultException(res);
                                        }

                                        EREC_HataridosFeladatok erec_hatartidosFeladat = (EREC_HataridosFeladatok)res.Record;
                                        erec_hatartidosFeladat.Updated.SetValueAll(false);
                                        erec_hatartidosFeladat.Base.Updated.SetValueAll(false);
                                        erec_hatartidosFeladat.Base.Updated.Ver = true;

                                        Logger.Debug("LoadUpdateBusinessDocumentFromDataRow EREC_HataridosFeladatok");
                                        Utility.LoadUpdateBusinessDocumentFromDataRow(erec_hatartidosFeladat, row);

                                        res = service.Update(xpm, erec_hatartidosFeladat);
                                        if (res.IsError)
                                        {
                                            Logger.Error("EREC_HataridosFeladatokService Update hiba", xpm, res);
                                            throw new ResultException(res);
                                        }
                                    }
                                }
                                break;
                            case "HataridosErtesites":
                            case "EsemenyErtesites":
                                {
                                    Logger.Debug("case HataridosErtesites, EsemenyErtesites");
                                    Logger.Debug("LoadBusinessDocumentFromDataRow EREC_HataridosErtesitesek");
                                    EREC_HataridosErtesitesek erec_hatartidosErtesitesek = new EREC_HataridosErtesitesek();
                                    Utility.LoadBusinessDocumentFromDataRow(erec_hatartidosErtesitesek, row);
                                    erec_HataridosErtesitesekList.Add(erec_hatartidosErtesitesek);
                                }
                                break;
                        }
                    }
                }

                if (erec_HataridosErtesitesekList.Count > 0)
                {
                    ExecParam xpm = p_ExecParam.Clone();
                    bool ret = Notify.SendHataridosErtesites(xpm, erec_HataridosErtesitesekList);
                    if (!ret)
                    {
                        Logger.Error("Notify.SendHataridosErtesites hiba", xpm);
                        res.ErrorCode = res.ErrorMessage = "Notify.SendHataridosErtesites hiba";
                    }
                }
            }

            return res;
        }

        public static SqlDbType GetSqlType(String type)
        {
            SqlDbType _ret = new SqlDbType();
            _ret = SqlDbType.NVarChar; // default ertek!
            switch (type)
            {
                case "uniqueidentifier":
                    _ret = SqlDbType.UniqueIdentifier;
                    break;
                case "nvarchar":
                    _ret = SqlDbType.NVarChar;
                    break;
                case "int":
                    _ret = SqlDbType.Int;
                    break;
                case "float":
                    _ret = SqlDbType.Float;
                    break;
                case "char":
                    _ret = SqlDbType.Char;
                    break;
                case "datetime":
                    _ret = SqlDbType.DateTime;
                    break;
                case "smalldatetime":
                    _ret = SqlDbType.SmallDateTime;
                    break;
                case "image":
                    _ret = SqlDbType.Image;
                    break;
                case "varbinary":
                    _ret = SqlDbType.VarBinary;
                    break;
            }
            return _ret;
        }
        #endregion
    }
}
