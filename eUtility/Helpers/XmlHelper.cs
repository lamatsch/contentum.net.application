﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Schema;

/// TODO: Kidolgozandók az XSD feldolgozásra épülő (rekurzív) eljárások,
/// ezek egyelőre csak részlegesen adnak helyes eredményt és használatuk kerülendő
/// helyettük ld. EFeladoJegyzek névtér osztályai és metódusai (jelenleg beégetett
/// értékekkel), melyeket később erre az osztályra kell irányítani
namespace Contentum.eUtility
{
    /// <summary>
    /// Summary description for XmlHelper
    /// </summary>
    public static class XmlHelper
    {
        public class ValidationException : System.Xml.XmlException
        {
            public ValidationException() : base() { }
            public ValidationException(String message) : base(message) { }
            public ValidationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
            public ValidationException(String message, Exception innerException) : base(message, innerException) { }
            public ValidationException(String message, Exception innerException, Int32 lineNumber, Int32 linePosition) : base(message, innerException, lineNumber, linePosition) { }
        }


        #region Restrictions

        /// <summary>
        /// XSD Restrictions/Facets for Datatypes
        /// -----------------------------------------------------------------------------------------------------
        /// Constraint          Description 
        /// -----------------------------------------------------------------------------------------------------
        /// enumeration         Defines a list of acceptable values 
        /// fractionDigits      Specifies the maximum number of decimal places allowed. Must be equal to or greater than zero 
        /// length              Specifies the exact number of characters or list items allowed. Must be equal to or greater than zero 
        /// maxExclusive        Specifies the upper bounds for numeric values (the value must be less than this value) 
        /// maxInclusive        Specifies the upper bounds for numeric values (the value must be less than or equal to this value) 
        /// maxLength           Specifies the maximum number of characters or list items allowed. Must be equal to or greater than zero 
        /// minExclusive        Specifies the lower bounds for numeric values (the value must be greater than this value) 
        /// minInclusive        Specifies the lower bounds for numeric values (the value must be greater than or equal to this value) 
        /// minLength           Specifies the minimum number of characters or list items allowed. Must be equal to or greater than zero 
        /// pattern             Defines the exact sequence of characters that are acceptable  
        /// totalDigits         Specifies the exact number of digits allowed. Must be greater than zero 
        /// whiteSpace          Specifies how white space (line feeds, tabs, spaces, and carriage returns) is handled 
        /// </summary>
        public class SimpleTypeRestriction
        {
            #region Properties
            private bool mandatory = false; // true if element or element's any parent has the setting minOccurs == 0; false otherwise
            public bool Mandatory
            {
                get { return mandatory; }
                set { mandatory = value; }
            }

            private string simpleType = null; // xs:string, xs:integer
            public string SimpleType
            {
                get { return simpleType; }
                set { simpleType = value; }
            }

            private Type baseType = null; // typeof(string), typeof(integer)
            public Type BaseType
            {
                get { return baseType; }
                set { baseType = value; }
            }

            private UInt64? minOccurs = null; // not defined
            public UInt64? MinOccurs
            {
                get { return minOccurs; }
                set { minOccurs = value; }
            }

            private UInt64? maxOccurs = null; // -> unbound
            public UInt64? MaxOccurs
            {
                get { return maxOccurs; }
                set { maxOccurs = value; }
            }

            private List<string> enumeration = null;
            public List<string> Enumeration
            {
                get { return enumeration; }
                set { enumeration = value; }
            }

            private UInt64? fractionDigits = null; // not defined
            public UInt64? FractionDigits
            {
                get { return fractionDigits; }
                set { fractionDigits = value; }
            }

            private UInt64? length = null; // not defined
            public UInt64? Length
            {
                get { return length; }
                set { length = value; }
            }

            private UInt64? maxExclusive = null; // not defined
            public UInt64? MaxExclusive
            {
                get { return maxExclusive; }
                set { maxExclusive = value; }
            }

            private UInt64? maxInclusive = null; // not defined
            public UInt64? MaxInclusive
            {
                get { return maxInclusive; }
                set { maxInclusive = value; }
            }

            private UInt64? maxLength = null; // not defined
            public UInt64? MaxLength
            {
                get { return maxLength; }
                set { maxLength = value; }
            }

            private UInt64? minExclusive = null; // not defined
            public UInt64? MinExclusive
            {
                get { return minExclusive; }
                set { minExclusive = value; }
            }

            private UInt64? minInclusive = null; // not defined
            public UInt64? MinInclusive
            {
                get { return minInclusive; }
                set { minInclusive = value; }
            }

            private UInt64? minLength = null; // not defined
            public UInt64? MinLength
            {
                get { return minLength; }
                set { minLength = value; }
            }

            private string pattern = null;
            public string Pattern
            {
                get { return pattern; }
                set { pattern = value; }
            }

            private UInt64? totalDigits = null; // not defined
            public UInt64? TotalDigits
            {
                get { return totalDigits; }
                set { totalDigits = value; }
            }

            private string whiteSpace = null;
            public string WhiteSpace
            {
                get { return whiteSpace; }
                set { whiteSpace = value; }
            }
            #endregion Properties

            #region Private Utility
            private static Dictionary<string, List<string>> AddToErrorsDictionary(Dictionary<string, List<string>> errors, string colname, List<string> facetnames)
            {
                if (errors != null)
                {
                    if (!String.IsNullOrEmpty(colname) && facetnames != null && facetnames.Count > 0)
                    {
                        if (!errors.ContainsKey(colname))
                        {
                            errors.Add(colname, new List<string>());
                        }

                        // nem közvetlenül másoljuk, hanem elemenként adjuk hozzá, ha nem üres
                        // hatékonyságnöveléshez csak egyszerűen a kapott listát kell betenni
                        foreach (string facetname in facetnames)
                        {
                            AddToErrorsList(errors[colname], facetname);
                        }
                    }
                }
                return errors;
            }

            private static Dictionary<string, List<string>> AddToErrorsDictionary(Dictionary<string, List<string>> errors, string colname, string facetname)
            {
                if (errors != null)
                {
                    if (!String.IsNullOrEmpty(colname) && !String.IsNullOrEmpty(facetname))
                    {
                        if (!errors.ContainsKey(colname))
                        {
                            errors.Add(colname, new List<string>());
                        }
                        AddToErrorsList(errors[colname], facetname);
                    }
                }
                return errors;
            }

            private static List<string> AddToErrorsList(List<string> errors, string facetname)
            {
                if (errors != null)
                {
                    if (!String.IsNullOrEmpty(facetname) && !errors.Contains(facetname))
                    {
                        errors.Add(facetname);
                    }
                }
                return errors;
            }
            #endregion Private Utility

            #region Validation
            public bool Validate(object value, out List<string> errors)
            {
                bool isValid = true;

                errors = new List<string>();

                if (this.Mandatory && (value == null || value is DBNull))
                {
                    AddToErrorsList(errors, "mandatory");
                }
                else
                {
                    if (value != null && !(value is DBNull))
                    {
                        if (this.SimpleType == "integer")
                        {
                            UInt64 uintValue;
                            if (UInt64.TryParse(value.ToString(), out uintValue))
                            {
                                if (this.MinInclusive.HasValue && uintValue < this.MinInclusive)
                                {
                                    AddToErrorsList(errors, "minInclusive");
                                }
                                if (this.MaxInclusive.HasValue && uintValue > this.MaxInclusive)
                                {
                                    AddToErrorsList(errors, "maxInclusive");
                                }

                                if (this.MinExclusive.HasValue && uintValue <= this.MinExclusive)
                                {
                                    AddToErrorsList(errors, "minExclusive");
                                }
                                if (this.MaxExclusive.HasValue && uintValue >= this.MaxExclusive)
                                {
                                    AddToErrorsList(errors, "maxExclusive");
                                }
                            }
                            else
                            {
                                AddToErrorsList(errors, "baseType");
                            }
                        }
                        else if (this.SimpleType == "string")
                        {
                            String strValue = value as String;
                            if (strValue != null)
                            {
                                if (this.Length.HasValue && (UInt64)strValue.Length != this.Length)
                                {
                                    AddToErrorsList(errors, "length");
                                }
                                if (this.MaxLength.HasValue && (UInt64)strValue.Length > this.MaxLength)
                                {
                                    AddToErrorsList(errors, "maxLength");
                                }
                                if (this.MinLength.HasValue && (UInt64)strValue.Length < this.MinLength)
                                {
                                    AddToErrorsList(errors, "minLength");
                                }

                                if (!String.IsNullOrEmpty(this.Pattern))
                                {
                                    System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(this.Pattern);

                                    if (!re.IsMatch(strValue))
                                    {
                                        AddToErrorsList(errors, "pattern");
                                    }
                                }

                                if (this.Enumeration != null && this.Enumeration.Count > 0)
                                {
                                    if (!this.Enumeration.Contains(strValue))
                                    {
                                        AddToErrorsList(errors, "enumeration");
                                    }
                                }
                            }
                            else
                            {
                                AddToErrorsList(errors, "baseType");
                            }
                        }
                    }
                }
                if (errors.Count > 0)
                {
                    isValid = false;
                }
                return isValid;
            }

            public bool Validate(System.Data.DataRowView drv, string colname, out List<string> errors)
            {
                bool isValid = true;

                object value = null;
                if (drv != null && !String.IsNullOrEmpty(colname))
                {
                    if (drv.Row.Table.Columns.Contains(colname))
                    {
                        value = drv.Row[colname];
                    }
                }

                isValid = Validate(value, out errors);
                return isValid;
            }

            public static bool Validate(Dictionary<string, Contentum.eUtility.XmlHelper.SimpleTypeRestriction> restrictions, System.Data.DataRowView drv, out Dictionary<string, List<string>> errors)
            {
                bool isValid = true;
                errors = new Dictionary<string, List<string>>();
                if (restrictions != null || drv != null)
                {
                    foreach (string dictKey in restrictions.Keys)
                    {
                        string[] keyElements = dictKey.Split(new char[] { '/' });
                        string key = keyElements[0];

                        if (!String.IsNullOrEmpty(key) && drv.Row.Table.Columns.Contains(key))
                        {
                            Contentum.eUtility.XmlHelper.SimpleTypeRestriction restriction = restrictions[dictKey];
                            if (restriction != null)
                            {
                                List<string> lstErrors = new List<string>();
                                isValid &= restriction.Validate(drv, key, out lstErrors);
                                AddToErrorsDictionary(errors, key, lstErrors);
                            }
                        }
                    }
                }

                return isValid;
            }
            #endregion Validation

            #region Fill From Xsd
            public void FillFacets(XmlSchemaSimpleType simpleType)
            {
                if (simpleType != null)
                {
                    XmlSchemaSimpleTypeRestriction r = simpleType.Content as XmlSchemaSimpleTypeRestriction;

                    if (r != null)
                    {
                        this.SimpleType = r.BaseTypeName.Name;

                        #region Facets
                        List<string> enumFacets = null;
                        foreach (XmlSchemaFacet facet in r.Facets)
                        {
                            if (facet is XmlSchemaEnumerationFacet)
                            {
                                if (enumFacets == null)
                                {
                                    enumFacets = new List<string>();
                                }
                                enumFacets.Add((facet as XmlSchemaEnumerationFacet).Value);
                            }
                            else if (facet is XmlSchemaFractionDigitsFacet)
                            {
                                this.FractionDigits = System.Convert.ToUInt64((facet as XmlSchemaFractionDigitsFacet).Value);
                            }
                            else if (facet is XmlSchemaLengthFacet)
                            {
                                this.Length = System.Convert.ToUInt64((facet as XmlSchemaLengthFacet).Value);
                            }
                            else if (facet is XmlSchemaMaxExclusiveFacet)
                            {
                                this.MaxExclusive = System.Convert.ToUInt64((facet as XmlSchemaMaxExclusiveFacet).Value);
                            }
                            else if (facet is XmlSchemaMaxInclusiveFacet)
                            {
                                this.MaxInclusive = System.Convert.ToUInt64((facet as XmlSchemaMaxInclusiveFacet).Value);
                            }
                            else if (facet is XmlSchemaMaxLengthFacet)
                            {
                                this.MaxLength = System.Convert.ToUInt64((facet as XmlSchemaMaxLengthFacet).Value);
                            }
                            else if (facet is XmlSchemaMinExclusiveFacet)
                            {
                                this.MinExclusive = System.Convert.ToUInt64((facet as XmlSchemaMinExclusiveFacet).Value);
                            }
                            else if (facet is XmlSchemaMinInclusiveFacet)
                            {
                                this.MinInclusive = System.Convert.ToUInt64((facet as XmlSchemaMinInclusiveFacet).Value);
                            }
                            else if (facet is XmlSchemaMinLengthFacet)
                            {
                                this.MinLength = System.Convert.ToUInt64((facet as XmlSchemaMinLengthFacet).Value);
                            }
                            else if (facet is XmlSchemaPatternFacet)
                            {
                                this.Pattern = (facet as XmlSchemaPatternFacet).Value;
                            }
                            else if (facet is XmlSchemaTotalDigitsFacet)
                            {
                                this.TotalDigits = System.Convert.ToUInt64((facet as XmlSchemaTotalDigitsFacet).Value);
                            }
                            else if (facet is XmlSchemaWhiteSpaceFacet)
                            {
                                this.WhiteSpace = (facet as XmlSchemaWhiteSpaceFacet).Value;
                            }
                        }
                        this.Enumeration = enumFacets;
                        #endregion Facets
                    }
                }
            }
            #endregion Fill From Xsd
        } // class

        public class ElementOccurence
        {
            #region Properties
            private bool mandatory = false; // true if element or element's any parent has the setting minOccurs == 0; false otherwise
            public bool Mandatory
            {
                get { return mandatory; }
                set { mandatory = value; }
            }

            private string simpleType = null; // xs:string, xs:integer
            public string SimpleType
            {
                get { return simpleType; }
                set { simpleType = value; }
            }

            private Type baseType = null; // typeof(string), typeof(integer)
            public Type BaseType
            {
                get { return baseType; }
                set { baseType = value; }
            }

            private UInt64? minOccurs = null; // not defined
            public UInt64? MinOccurs
            {
                get { return minOccurs; }
                set { minOccurs = value; }
            }

            private UInt64? maxOccurs = null; // -> unbound
            public UInt64? MaxOccurs
            {
                get { return maxOccurs; }
                set { maxOccurs = value; }
            }
            #endregion Properties

            #region Private Utility
            private static Dictionary<string, List<string>> AddToErrorsDictionary(Dictionary<string, List<string>> errors, string colname, List<string> facetnames)
            {
                if (errors != null)
                {
                    if (!String.IsNullOrEmpty(colname) && facetnames != null && facetnames.Count > 0)
                    {
                        if (!errors.ContainsKey(colname))
                        {
                            errors.Add(colname, new List<string>());
                        }

                        // nem közvetlenül másoljuk, hanem elemenként adjuk hozzá, ha nem üres
                        // hatékonyságnöveléshez csak egyszerűen a kapott listát kell betenni
                        foreach (string facetname in facetnames)
                        {
                            AddToErrorsList(errors[colname], facetname);
                        }
                    }
                }
                return errors;
            }

            private static Dictionary<string, List<string>> AddToErrorsDictionary(Dictionary<string, List<string>> errors, string colname, string facetname)
            {
                if (errors != null)
                {
                    if (!String.IsNullOrEmpty(colname) && !String.IsNullOrEmpty(facetname))
                    {
                        if (!errors.ContainsKey(colname))
                        {
                            errors.Add(colname, new List<string>());
                        }
                        AddToErrorsList(errors[colname], facetname);
                    }
                }
                return errors;
            }

            private static List<string> AddToErrorsList(List<string> errors, string facetname)
            {
                if (errors != null)
                {
                    if (!String.IsNullOrEmpty(facetname) && !errors.Contains(facetname))
                    {
                        errors.Add(facetname);
                    }
                }
                return errors;
            }
            #endregion Private Utility

            #region Validation
            public bool Validate(object value, out List<string> errors)
            {
                bool isValid = true;

                errors = new List<string>();

                if (this.Mandatory && value == null)
                {
                    AddToErrorsList(errors, "mandatory");
                }
                else
                {
                    if (this.SimpleType == "integer")
                    {
                        UInt64? uintValue = System.Convert.ToUInt64(value);
                        if (uintValue == null)
                        {
                            AddToErrorsList(errors, "baseType");
                        }
                    }
                    else if (this.SimpleType == "string")
                    {
                        String strValue = System.Convert.ToString(value);
                        if (strValue == null)
                        {
                            AddToErrorsList(errors, "baseType");
                        }
                    }
                }
                if (errors.Count > 0)
                {
                    isValid = false;
                }
                return isValid;
            }

            public bool Validate(System.Data.DataRowView drv, string colname, out List<string> errors)
            {
                bool isValid = true;

                object value = null;
                if (drv != null && !String.IsNullOrEmpty(colname))
                {
                    if (drv.Row.Table.Columns.Contains(colname))
                    {
                        value = drv.Row[colname];
                    }
                }

                isValid = Validate(value, out errors);
                return isValid;
            }

            public static bool Validate(Dictionary<string, Contentum.eUtility.XmlHelper.ElementOccurence> occurences, System.Data.DataRowView drv, out Dictionary<string, List<string>> errors)
            {
                bool isValid = true;
                errors = new Dictionary<string, List<string>>();
                if (occurences != null || drv != null)
                {
                    foreach (string dictKey in occurences.Keys)
                    {
                        string[] keyElements = dictKey.Split(new char[] { '/' });
                        string key = keyElements[0];

                        if (!String.IsNullOrEmpty(key) && drv.Row.Table.Columns.Contains(key))
                        {
                            Contentum.eUtility.XmlHelper.ElementOccurence occurence = occurences[dictKey];
                            if (occurence != null)
                            {
                                List<string> lstErrors = new List<string>();
                                isValid &= occurence.Validate(drv, key, out lstErrors);
                                AddToErrorsDictionary(errors, key, lstErrors);
                            }
                        }
                    }
                }

                return isValid;
            }
            #endregion Validation

            #region Fill From Xsd
            public void FillFacets(XmlSchemaElement schemaElement)
            {
                if (schemaElement != null)
                {
                    this.MinOccurs = System.Convert.ToUInt64(schemaElement.MinOccurs);
                    this.MaxOccurs = System.Convert.ToUInt64(schemaElement.MaxOccurs);
                    this.SimpleType = schemaElement.ElementSchemaType.Datatype.ValueType.Name;
                    this.BaseType = schemaElement.ElementSchemaType.Datatype.ValueType;
                }
            }
            #endregion Fill From Xsd
        } // class
        #endregion Restrictions

        #region Read/Write XML/XSD

        public static System.Xml.Schema.XmlSchema GetXsdSchemaFromText(string xsd)
        {
            System.Xml.Schema.ValidationEventHandler validationEventHandler = new System.Xml.Schema.ValidationEventHandler(ShowCompileErrors);
            return GetXsdSchemaFromText(xsd, validationEventHandler);
        }

        public static System.Xml.Schema.XmlSchema GetXsdSchemaFromText(string xsd, System.Xml.Schema.ValidationEventHandler validationEventHandler)
        {
            System.Xml.Schema.XmlSchema xmlSchema = null;
            System.Xml.XmlReader reader = null;

            try
            {
                reader = System.Xml.XmlReader.Create(new System.IO.StringReader(xsd));
                xmlSchema = System.Xml.Schema.XmlSchema.Read(reader, validationEventHandler);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return xmlSchema;
        }

        public static System.Xml.Schema.XmlSchema GetXsdSchemaFromFile(string path)
        {
            System.Xml.Schema.ValidationEventHandler validationEventHandler = new System.Xml.Schema.ValidationEventHandler(ShowCompileErrors);
            return GetXsdSchemaFromFile(path, validationEventHandler);
        }

        public static System.Xml.Schema.XmlSchema GetXsdSchemaFromFile(string path, System.Xml.Schema.ValidationEventHandler validationEventHandler)
        {
            System.Xml.Schema.XmlSchema xmlSchema = null;
            System.Xml.XmlTextReader reader = null;

            try
            {
                reader = new System.Xml.XmlTextReader(path);
                xmlSchema = System.Xml.Schema.XmlSchema.Read(reader, validationEventHandler);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return xmlSchema;
        }

        public static System.Xml.XmlDocument GetXmlDocumentFromText(string xml)
        {
            System.Xml.XmlDocument xmlDocument = null;

            xmlDocument = new System.Xml.XmlDocument();
            xmlDocument.LoadXml(xml);

            return xmlDocument;
        }

        public static System.Xml.XmlDocument GetXmlDocumentFromFile(string path)
        {
            System.Xml.XmlDocument xmlDocument = null;

            xmlDocument = new System.Xml.XmlDocument();
            xmlDocument.Load(path);

            return xmlDocument;
        }

        public static void WriteXmlDocumentToFile(System.Xml.XmlTextWriter writer, System.Xml.XmlDocument xmlDocument, bool bIndent)
        {
            try
            {
                if (bIndent)
                {
                    // output formatted
                    writer.Formatting = System.Xml.Formatting.Indented;
                }

                xmlDocument.Save(writer);
            }
            finally
            {
                //clean up even if error
                if (writer != null)
                    writer.Close();
            }
        }

        public static void WriteXmlDocumentToFile(string path, System.Xml.XmlDocument xmlDocument, int codePage, bool bIndent)
        {
            System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(path, System.Text.Encoding.GetEncoding(codePage));
            WriteXmlDocumentToFile(writer, xmlDocument, bIndent);
        }

        public static void WriteXmlDocumentToFile(string path, System.Xml.XmlDocument xmlDocument, System.Text.Encoding encoding, bool bIndent)
        {
            System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(path, encoding);
            WriteXmlDocumentToFile(writer, xmlDocument, bIndent);
        }

        #endregion Read/Write XML/XSD

        #region Queries on XML/XSD
        public static Dictionary<string, List<string>> GetMandatoryFieldsFromXmlSchema(string xsd)
        {
            if (xsd == null) return null;

            XmlSchema xmlSchema = GetXsdSchemaFromText(xsd);

            return GetMandatoryFieldsFromXmlSchema(xmlSchema);
        }

        public static Dictionary<string, List<string>> GetMandatoryFieldsFromXmlSchema(System.Xml.Schema.XmlSchema xmlSchema)
        {
            if (xmlSchema == null) return null;

            Dictionary<string, List<string>> mandatoryElements = new Dictionary<string, List<string>>();

            Dictionary<XmlSchemaElement, List<XmlSchemaElement>> elements = GetXmlSchemaElementReferenceDictionary(xmlSchema);

            // debug
            //int kk = 0;
            //foreach(XmlSchemaElement elem in elements.Keys)
            //{
            //    if (elements[elem] != null && elements[elem].Count > 0)
            //    {
            //        int j = 0;
            //        foreach (XmlSchemaElement refelem in elements[elem])
            //        {
            //            System.Diagnostics.Debug.WriteLine(String.Format("{0}/{5}. Name: {1}; RefName: {2}; MinOccurs: {3} (str: {4}); Referenced by: {6}; RefName: {7}; MinOccurs: {8} (str: {9});", kk, elem.QualifiedName, elem.RefName, elem.MinOccurs, elem.MinOccursString, j++, refelem.QualifiedName, refelem.RefName, refelem.MinOccurs, refelem.MinOccursString));
            //        }
            //    }
            //    else
            //    {
            //        System.Diagnostics.Debug.WriteLine(String.Format("{0}. Name: {1}; RefName: {2}; MinOccurs: {3} (str: {4});", kk, elem.QualifiedName, elem.RefName, elem.MinOccurs, elem.MinOccursString));
            //    }
            
            //    kk++;
            //}

            foreach (XmlSchemaElement elem in elements.Keys)
            {
                if (elements[elem] != null && elements[elem].Count > 0)
                {
                    foreach (XmlSchemaElement refelem in elements[elem])
                    {
                        if (refelem.MinOccurs > 0)
                        {
                            List<List<XmlSchemaObject>> parents = new List<List<XmlSchemaObject>>();
                            parents = GetElementParentObjectList(elements, refelem, parents);

                            if (parents != null && parents.Count > 0)
                            {
                                foreach (List<XmlSchemaObject> parentList in parents)
                                {
                                    int i = 0;
                                    bool isMandatoryRelativeToParentElement = true;
                                    while (isMandatoryRelativeToParentElement && i < parentList.Count && (parentList[i] is XmlSchemaElement) == false)
                                    {
                                        if (parentList[i] is XmlSchemaGroupBase && ((XmlSchemaGroupBase)parentList[i]).MinOccurs == 0)
                                        {
                                            isMandatoryRelativeToParentElement = false;
                                        }
                                        else
                                        {
                                            i++;
                                        }
                                    }
                                    if (isMandatoryRelativeToParentElement && i < parentList.Count && parentList[i] is XmlSchemaElement)
                                    {
                                        // egyelőre csak a közvetlen szülőhöz viszonyítva nézzük
                                        string path = ((XmlSchemaElement)parentList[i]).QualifiedName.ToString();
                                        if (!mandatoryElements.ContainsKey(path))
                                        {
                                            mandatoryElements.Add(path, new List<string>());
                                        }
                                        if (!mandatoryElements[path].Contains(elem.QualifiedName.ToString()))
                                        {
                                            mandatoryElements[path].Add(elem.QualifiedName.ToString());
                                        }
                                    }
                                }
                            }
                            else
                            {
                                string path = "";
                                if (!mandatoryElements.ContainsKey(path))
                                {
                                    mandatoryElements.Add(path, new List<string>());
                                }
                                if (!mandatoryElements[path].Contains(elem.QualifiedName.ToString()))
                                {
                                    mandatoryElements[path].Add(elem.QualifiedName.ToString());
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (elem.MinOccurs > 0)
                    {
                        List<List<XmlSchemaObject>> parents = new List<List<XmlSchemaObject>>();
                        parents = GetElementParentObjectList(elements, elem, parents);

                        if (parents != null && parents.Count > 0)
                        {
                            foreach (List<XmlSchemaObject> parentList in parents)
                            {
                                int i = 0;
                                bool isMandatoryRelativeToParentElement = true;
                                while (isMandatoryRelativeToParentElement && i < parentList.Count && (parentList[i] is XmlSchemaElement) == false)
                                {
                                    if (parentList[i] is XmlSchemaGroupBase && ((XmlSchemaGroupBase)parentList[i]).MinOccurs == 0)
                                    {
                                        isMandatoryRelativeToParentElement = false;
                                    }
                                    else
                                    {
                                        i++;
                                    }
                                }
                                if (isMandatoryRelativeToParentElement && i < parentList.Count && parentList[i] is XmlSchemaElement)
                                {
                                    // egyelőre csak a közvetlen szülőhöz viszonyítva nézzük
                                    string path = ((XmlSchemaElement)parentList[i]).QualifiedName.ToString();
                                    if (!mandatoryElements.ContainsKey(path))
                                    {
                                        mandatoryElements.Add(path, new List<string>());
                                    }
                                    if (!mandatoryElements[path].Contains(elem.QualifiedName.ToString()))
                                    {
                                        mandatoryElements[path].Add(elem.QualifiedName.ToString());
                                    }
                                }
                            }
                        }
                        else
                        {
                            string path = "";
                            if (!mandatoryElements.ContainsKey(path))
                            {
                                mandatoryElements.Add(path, new List<string>());
                            }
                            if (!mandatoryElements[path].Contains(elem.QualifiedName.ToString()))
                            {
                                mandatoryElements[path].Add(elem.QualifiedName.ToString());
                            }
                        }
                    }
                }
            }

            return mandatoryElements;
        }


        private static void AddToRestrictionsDictionary(Dictionary<string, SimpleTypeRestriction> dictionary, KeyValuePair<string, SimpleTypeRestriction> restrictionPair)
        {
            if (!String.IsNullOrEmpty(restrictionPair.Key) && restrictionPair.Value != null)
            {
                if (!dictionary.ContainsKey(restrictionPair.Key))
                {
                    dictionary.Add(restrictionPair.Key, restrictionPair.Value);
                }
            }
        }

        public static Dictionary<string, SimpleTypeRestriction> GetSimpleTypeRestrictionsFromXmlSchema(XmlSchema xs)
        {
            if (xs == null) return null;

            Dictionary<XmlSchemaElement, List<XmlSchemaElement>> elements = GetXmlSchemaElementReferenceDictionary(xs);

            Dictionary<string, SimpleTypeRestriction> restrictionDictionary = new Dictionary<string, SimpleTypeRestriction>();

            XmlSchemaSet schemaSet = new XmlSchemaSet();
            schemaSet.Add(xs);
            if (xs.IsCompiled == false)
            {
                schemaSet.ValidationEventHandler += new ValidationEventHandler(ShowCompileErrors);
                schemaSet.Compile();
            }

            // types: collect only when referenced
            //XmlSchemaComplexType complexType;
            //foreach (XmlSchemaType type in xs.SchemaTypes.Values)
            //{
            //    complexType = type as XmlSchemaComplexType;
            //    if (complexType != null)
            //    {
            //        AddToRestrictionsDictionary(restrictionDictionary, GetElementRestrictionsFromSchemaParticle(complexType.ContentTypeParticle, schemaSet));
            //    }
            //}
            Dictionary<string, SimpleTypeRestriction> restricitonDictionary = new Dictionary<string, SimpleTypeRestriction>();
            foreach (XmlSchemaElement element in xs.Elements.Values)
            {
               restricitonDictionary = GetElementRestrictionsFromSchemaParticle(elements, element, restrictionDictionary);
            }

            return restrictionDictionary;
        }

        public static Dictionary<string, SimpleTypeRestriction> GetElementRestrictionsFromSchemaParticle(Dictionary<XmlSchemaElement, List<XmlSchemaElement>> elements, XmlSchemaObject particle, Dictionary<string, SimpleTypeRestriction> simpleTypeRestrictions)
        {
            if (simpleTypeRestrictions == null)
            {
                simpleTypeRestrictions = new Dictionary<string, SimpleTypeRestriction>();
            }
            if (particle != null)
            {
                if (particle is XmlSchemaElement)
                {
                    XmlSchemaElement elem = (XmlSchemaElement)particle;
                    XmlSchemaElement parentElement = null;

                    XmlSchemaSimpleType simpleType = null;
                    XmlSchemaType type = (XmlSchemaType)elem.ElementSchemaType;
                    XmlSchemaComplexType complexType = type as XmlSchemaComplexType;
                    if (complexType != null && complexType.Name == null)
                    {
                        //return GetElementRestrictionsFromSchemaParticle(complexType.ContentTypeParticle, schemaSet);
                        simpleTypeRestrictions = GetElementRestrictionsFromSchemaParticle(elements, complexType.ContentTypeParticle, simpleTypeRestrictions);
                    }
                    else
                    {
                        simpleType = type as XmlSchemaSimpleType;
                    }

                    if (elements.ContainsKey(elem) && elements[elem] != null && elements[elem].Count > 0)
                    {
                        foreach (XmlSchemaElement refelem in elements[elem])
                        {
                            XmlSchemaObject obj = refelem.Parent;
                            while (obj != null && (obj is XmlSchemaElement) == false)
                            {
                                obj = obj.Parent;
                            }

                            parentElement = obj as XmlSchemaElement;

                            SimpleTypeRestriction simpleTypeRestriction = new SimpleTypeRestriction();
                            if (simpleType != null)
                            {
                                simpleTypeRestriction.FillFacets(simpleType);
                            }
                            simpleTypeRestriction.MinOccurs = System.Convert.ToUInt64(refelem.MinOccurs);
                            if ("unbounded" != refelem.MaxOccursString)
                            {
                                simpleTypeRestriction.MaxOccurs = System.Convert.ToUInt64(refelem.MaxOccurs);
                            }
                            if (refelem.ElementSchemaType.Datatype != null)
                            {
                                simpleTypeRestriction.BaseType = refelem.ElementSchemaType.Datatype.ValueType;
                            }

                            string key = null;
                            if (parentElement != null)
                            {
                                key = String.Format("{0}/{1}", elem.QualifiedName.ToString(), parentElement.QualifiedName.ToString());
                            }
                            else
                            {
                                key = elem.QualifiedName.ToString();

                            }
                            if (!simpleTypeRestrictions.ContainsKey(key))
                            {
                                simpleTypeRestrictions.Add(key, simpleTypeRestriction);
                            }
                        }
                    }
                    else
                    {
                        XmlSchemaObject obj = elem.Parent;
                        while (obj != null && (obj is XmlSchemaElement) == false)
                        {
                            obj = obj.Parent;
                        }

                        parentElement = obj as XmlSchemaElement;


                        SimpleTypeRestriction simpleTypeRestriction = new SimpleTypeRestriction();
                        if (simpleType != null)
                        {
                            simpleTypeRestriction.FillFacets(simpleType);
                        }
                        simpleTypeRestriction.MinOccurs = System.Convert.ToUInt64(elem.MinOccurs);
                        if ("unbounded" != elem.MaxOccursString)
                        {
                            simpleTypeRestriction.MaxOccurs = System.Convert.ToUInt64(elem.MaxOccurs);
                        }
                        if (elem.ElementSchemaType.Datatype != null)
                        {
                            simpleTypeRestriction.BaseType = elem.ElementSchemaType.Datatype.ValueType;
                        }

                        string key = null;
                        if (parentElement != null)
                        {
                            key = key = String.Format("{0}/{1}", elem.QualifiedName.ToString(), parentElement.QualifiedName.ToString());
                        }
                        else
                        {
                            key = elem.QualifiedName.ToString();
                            
                        }
                        if (!simpleTypeRestrictions.ContainsKey(key))
                        {
                            simpleTypeRestrictions.Add(key, simpleTypeRestriction);
                        }
                    }
                } // XmlSchemaElement
                else if (particle is XmlSchemaGroupBase)
                {
                    //xs:all, xs:choice, xs:sequence
                    XmlSchemaGroupBase baseParticle = particle as XmlSchemaGroupBase;
                    foreach (XmlSchemaParticle subParticle in baseParticle.Items)
                    {
                        simpleTypeRestrictions = GetElementRestrictionsFromSchemaParticle(elements, subParticle, simpleTypeRestrictions);
                    }
                }
            }
            return simpleTypeRestrictions;
        }

        //public static Dictionary<string, SimpleTypeRestriction> GetSimpleTypeRestrictionsFromXmlSchema(XmlSchema xs)
        //{
        //    if (xs == null) return null;

        //    Dictionary<string, SimpleTypeRestriction> restrictionDictionary = new Dictionary<string, SimpleTypeRestriction>();
            
        //    XmlSchemaSet schemaSet = new XmlSchemaSet();
        //    schemaSet.Add(xs);
        //    if (xs.IsCompiled == false)
        //    {
        //        schemaSet.ValidationEventHandler += new ValidationEventHandler(ShowCompileErrors);
        //        schemaSet.Compile();
        //    }

        //    // types: collect only when referenced
        //    //XmlSchemaComplexType complexType;
        //    //foreach (XmlSchemaType type in xs.SchemaTypes.Values)
        //    //{
        //    //    complexType = type as XmlSchemaComplexType;
        //    //    if (complexType != null)
        //    //    {
        //    //        AddToRestrictionsDictionary(restrictionDictionary, GetElementRestrictionsFromSchemaParticle(complexType.ContentTypeParticle, schemaSet));
        //    //    }
        //    //}

        //    foreach (XmlSchemaElement element in xs.Elements.Values)
        //    {
        //        AddToRestrictionsDictionary(restrictionDictionary, GetElementRestrictionsFromSchemaParticle(element, schemaSet));
        //    }

        //    return restrictionDictionary;
        //}

        //public static KeyValuePair<string, SimpleTypeRestriction> GetElementRestrictionsFromSchemaParticle(XmlSchemaParticle particle, XmlSchemaSet schemaSet)
        //{
        //    KeyValuePair<string, SimpleTypeRestriction> restrictionPair = new KeyValuePair<string, SimpleTypeRestriction>();
        //    if (particle is XmlSchemaElement)
        //    {
        //        XmlSchemaElement elem = particle as XmlSchemaElement;

        //        if (elem.RefName.IsEmpty)
        //        {
        //            XmlSchemaType type = (XmlSchemaType)elem.ElementSchemaType;
        //            XmlSchemaComplexType complexType = type as XmlSchemaComplexType;
        //            if (complexType != null && complexType.Name == null)
        //            {
        //                return GetElementRestrictionsFromSchemaParticle(complexType.ContentTypeParticle, schemaSet);
        //            }
        //            else
        //            {
        //                XmlSchemaSimpleType simpleType = type as XmlSchemaSimpleType;
        //                SimpleTypeRestriction restriction = new SimpleTypeRestriction();

        //                restriction.Mandatory = true;

        //                XmlSchemaObject p = elem;
        //                while (p != null && restriction.Mandatory)
        //                {
        //                    if (p is XmlSchemaElement)
        //                    {
        //                        UInt64? minOccurs = System.Convert.ToUInt64((p as XmlSchemaElement).MinOccurs);
        //                        if (minOccurs == 0)
        //                        {
        //                            restriction.Mandatory = false;
        //                        }
        //                    }

        //                    p = p.Parent;
        //                }

        //                if (simpleType != null)
        //                {

        //                    restriction.MinOccurs = System.Convert.ToUInt64(elem.MinOccurs);
        //                    restriction.MaxOccurs = System.Convert.ToUInt64(elem.MaxOccurs);

        //                    restriction.FillFacets(simpleType);
        //                    restriction.BaseType = type.Datatype.ValueType; //r.BaseType.Datatype.ValueType;

        //                    XmlSchemaObject e = elem;
        //                    List<string> elementHierarchy = new List<string>();
        //                    while (e != null)
        //                    {
        //                        if (e is XmlSchemaElement)
        //                        {
        //                            elementHierarchy.Add((e as XmlSchemaElement).Name);
        //                        }

        //                        e = e.Parent;
        //                    }
        //                    //elementHierarchy.Reverse();
        //                    return new KeyValuePair<string, SimpleTypeRestriction>(String.Join("/", elementHierarchy.ToArray()), restriction);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            foreach (XmlSchemaElement so in schemaSet.GlobalElements.Values)
        //            {
        //                if (so.QualifiedName.Name == elem.RefName.Name)
        //                {
        //                    restrictionPair = GetElementRestrictionsFromSchemaParticle(so, schemaSet);
        //                    restrictionPair.Value.MinOccurs = System.Convert.ToUInt64(so.MinOccurs);
        //                    restrictionPair.Value.MaxOccurs = System.Convert.ToUInt64(so.MaxOccurs);
        //                    return restrictionPair;
        //                }
        //            }
        //        }
        //    }
        //    else if (particle is XmlSchemaGroupBase)
        //    { //xs:all, xs:choice, xs:sequence
        //        XmlSchemaGroupBase baseParticle = particle as XmlSchemaGroupBase;
        //        foreach (XmlSchemaParticle subParticle in baseParticle.Items)
        //        {
        //            restrictionPair = GetElementRestrictionsFromSchemaParticle(subParticle, schemaSet);
        //            if (subParticle is XmlSchemaElement)
        //            {
        //                XmlSchemaElement e = (subParticle as XmlSchemaElement);
        //                if (restrictionPair.Key == null)
        //                {
        //                    restrictionPair = new KeyValuePair<string, SimpleTypeRestriction>(e.QualifiedName.Name, new SimpleTypeRestriction());
        //                }

        //                restrictionPair.Value.MinOccurs = System.Convert.ToUInt64(e.MinOccurs);
        //                restrictionPair.Value.MaxOccurs = System.Convert.ToUInt64(e.MaxOccurs);
        //            }
        //            return restrictionPair;
        //        }
        //    }

        //    return restrictionPair;
        //}



        #endregion Queries on XML/XSD


        #region Validation
        public static void ShowCompileErrors(object sender, System.Xml.Schema.ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
            {
                Logger.Warn(args.Message);
            }
            else if (args.Severity == XmlSeverityType.Error)
            {
                throw new ValidationException(args.Message, args.Exception);
            }
        }

        public static void ValidateXmlAgainstXmlSchema(System.Xml.XmlDocument xmlDocument, System.Xml.Schema.XmlSchema xmlSchema)
        {
            System.Xml.Schema.ValidationEventHandler validationEventHandler = new System.Xml.Schema.ValidationEventHandler(ShowCompileErrors);
            ValidateXmlAgainstXmlSchema(xmlDocument, xmlSchema, validationEventHandler);
        }

        public static void ValidateXmlAgainstXmlSchema(System.Xml.XmlDocument xmlDocument, System.Xml.Schema.XmlSchema xmlSchema, System.Xml.Schema.ValidationEventHandler validationEventHandler)
        {
            xmlDocument.Schemas.Add(xmlSchema);
            xmlDocument.Validate(validationEventHandler);
        }


        #region XmlSchema Validation of Fragment
        public static void ValidateXmlFragmentAgainstXmlSchema(string xmlFrag, System.Xml.Schema.XmlSchema xmlSchema)
        {
            System.Xml.Schema.ValidationEventHandler validationEventHandler = new System.Xml.Schema.ValidationEventHandler(ShowCompileErrors);
            ValidateXmlFragmentAgainstXmlSchema(xmlFrag, xmlSchema, validationEventHandler);
        }

        public static void ValidateXmlFragmentAgainstXmlSchema(string xmlFrag, System.Xml.Schema.XmlSchema xmlSchema, System.Xml.Schema.ValidationEventHandler validationEventHandler)
        {
            if (String.IsNullOrEmpty(xmlFrag) || xmlSchema == null) return;

            System.Xml.XmlReader reader = null;
            System.Xml.Schema.XmlSchemaSet myschemaset = new System.Xml.Schema.XmlSchemaSet();

            try
            {
                //Create the XmlParserContext.
                System.Xml.XmlParserContext context = new System.Xml.XmlParserContext(null, null, "", System.Xml.XmlSpace.None);

                //Add the schema.
                myschemaset.Add(xmlSchema);

                //Implement the reader. 
                System.Xml.XmlReaderSettings settings = new System.Xml.XmlReaderSettings();
                settings.ValidationType = System.Xml.ValidationType.Schema;
                settings.Schemas = myschemaset;
                if (validationEventHandler != null)
                {
                    settings.ValidationEventHandler += validationEventHandler;
                }

                reader = System.Xml.XmlReader.Create(new System.IO.StringReader(xmlFrag), settings, context);

                while (reader.Read())
                {
                }

                System.Diagnostics.Debug.WriteLine("Completed validating xmlfragment");
            }
            //catch (System.Xml.XmlException XmlExp)
            //{
            //    System.Diagnostics.Debug.WriteLine(XmlExp.Message);
            //}
            //catch (System.Xml.Schema.XmlSchemaException XmlSchExp)
            //{
            //    System.Diagnostics.Debug.WriteLine(XmlSchExp.Message);
            //}
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }
        #endregion XmlSchema Validation of Fragment

        #endregion Validation

        #region XmlDocument Manipulation

        public static void RemoveEmptyNodes(System.Xml.XmlDocument xmlDocument)
        {
            System.Xml.XmlNodeList emptyElements = xmlDocument.SelectNodes(@"//*[not(node())]");

            for (int i = emptyElements.Count - 1; i >= 0; i--)
            {
                emptyElements[i].ParentNode.RemoveChild(emptyElements[i]);
            }
        }

        #endregion XmlDocument Manipulation

        #region XmlSchema Traversing
        private static List<string> GetElementPath(Dictionary<XmlSchemaElement, List<XmlSchemaElement>> elements, XmlSchemaElement elem, List<string> paths)
        {
            List<string> pathList = new List<string>();
            if (paths == null)
            {
                paths = new List<string>();
            }

            if (elem != null)
            {
                string parentName = "";
                XmlSchemaObject obj = elem.Parent;
                XmlSchemaElement lastElement = elem;
                while (obj != null)
                {
                    if (obj is XmlSchemaElement)
                    {
                        lastElement = (XmlSchemaElement)obj;
                        parentName += "/" + lastElement.QualifiedName.ToString();
                    }

                    obj = obj.Parent;
                }

                List<string> newpaths = new List<string>();
                if (elements.ContainsKey(lastElement))
                {
                    if (elements[lastElement] != null && elements[lastElement].Count > 0)
                    {
                        foreach (XmlSchemaElement el in elements[lastElement])
                        {
                            List<string> refpaths = GetElementPath(elements, el, paths);
                            newpaths.AddRange(refpaths);
                        }
                    }
                }

                if (newpaths.Count > 0)
                {
                    if (paths.Count > 0)
                    {
                        for (int i = 0; i < paths.Count; i++)
                        {
                            string i_item = paths[i];
                            for (int j = 0; j < newpaths.Count; j++)
                            {
                                pathList.Add(i_item + newpaths[j]);
                            }
                        }
                    }
                    else
                    {
                        for (int j = 0; j < newpaths.Count; j++)
                        {
                            pathList.Add(parentName + newpaths[j]);
                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(parentName))
                    {
                        if (paths.Count == 0)
                        {
                            pathList.Add(parentName);
                        }
                        else
                        {
                            for (int i = 0; i < paths.Count; i++)
                            {
                                pathList.Add(paths[i] + parentName);
                            }
                        }
                    }
                }

            }

            return pathList;
        }

        private static List<List<XmlSchemaElement>> GetElementParentList(Dictionary<XmlSchemaElement, List<XmlSchemaElement>> elements, XmlSchemaElement elem, List<List<XmlSchemaElement>> parents)
        {
            List<List<XmlSchemaElement>> parentElementList = new List<List<XmlSchemaElement>>();
            if (parents == null)
            {
                parents = new List<List<XmlSchemaElement>>();
            }

            if (elem != null)
            {
                List<XmlSchemaElement> parentElements = new List<XmlSchemaElement>();
                XmlSchemaObject obj = elem.Parent;
                XmlSchemaElement lastElement = elem;
                while (obj != null)
                {
                    if (obj is XmlSchemaElement)
                    {
                        lastElement = (XmlSchemaElement)obj;
                        parentElements.Add(lastElement);
                    }

                    obj = obj.Parent;
                }

                List<List<XmlSchemaElement>> newparents = new List<List<XmlSchemaElement>>();
                if (elements.ContainsKey(lastElement))
                {
                    if (elements[lastElement] != null && elements[lastElement].Count > 0)
                    {
                        foreach (XmlSchemaElement el in elements[lastElement])
                        {
                            List<List<XmlSchemaElement>> refparents = GetElementParentList(elements, el, parents);
                            newparents.AddRange(refparents);
                        }
                    }
                }

                if (newparents.Count > 0)
                {
                    if (parents.Count > 0)
                    {
                        for (int i = 0; i < parents.Count; i++)
                        {
                            List<XmlSchemaElement> i_item = parents[i];
                            for (int j = 0; j < newparents.Count; j++)
                            {
                                newparents[j].InsertRange(0, i_item);
                                parentElementList.Add(newparents[j]);
                            }
                        }
                    }
                    else
                    {
                        for (int j = 0; j < newparents.Count; j++)
                        {
                            newparents[j].InsertRange(0, parentElements);
                            parentElementList.Add(newparents[j]);
                        }
                    }
                }
                else
                {
                    if (parentElements != null && parentElements.Count > 0)
                    {
                        if (parents.Count == 0)
                        {
                            parentElementList.Add(parentElements);
                        }
                        else
                        {
                            for (int i = 0; i < parents.Count; i++)
                            {
                                parentElements.InsertRange(0, parents[i]);
                                parentElementList.Add(parents[i]);
                            }
                        }
                    }
                }

            }

            return parentElementList;
        }

        private static List<List<XmlSchemaObject>> GetElementParentObjectList(Dictionary<XmlSchemaElement, List<XmlSchemaElement>> elements, XmlSchemaElement elem, List<List<XmlSchemaObject>> parents)
        {
            List<List<XmlSchemaObject>> parentObjectList = new List<List<XmlSchemaObject>>();
            if (parents == null)
            {
                parents = new List<List<XmlSchemaObject>>();
            }

            if (elem != null && elem is XmlSchemaElement)
            {
                List<XmlSchemaObject> parentObjects = new List<XmlSchemaObject>();
                XmlSchemaObject obj = elem.Parent;
                XmlSchemaElement lastElement = elem;
                while (obj != null)
                {
                    if (obj is XmlSchemaElement)
                    {
                        lastElement = (XmlSchemaElement)obj;
                        parentObjects.Add(lastElement);
                    }
                    else if (obj is XmlSchemaGroupBase)
                    {
                        parentObjects.Add((XmlSchemaGroupBase)obj);
                    }

                    obj = obj.Parent;
                }

                List<List<XmlSchemaObject>> newparents = new List<List<XmlSchemaObject>>();
                if (elements.ContainsKey(lastElement))
                {
                    if (elements[lastElement] != null && elements[lastElement].Count > 0)
                    {
                        foreach (XmlSchemaElement el in elements[lastElement])
                        {
                            List<List<XmlSchemaObject>> refparents = GetElementParentObjectList(elements, el, parents);
                            newparents.AddRange(refparents);
                        }
                    }
                }

                if (newparents.Count > 0)
                {
                    if (parents.Count > 0)
                    {
                        for (int i = 0; i < parents.Count; i++)
                        {
                            List<XmlSchemaObject> i_item = parents[i];
                            for (int j = 0; j < newparents.Count; j++)
                            {
                                newparents[j].InsertRange(0, i_item);
                                parentObjectList.Add(newparents[j]);
                            }
                        }
                    }
                    else
                    {
                        for (int j = 0; j < newparents.Count; j++)
                        {
                            newparents[j].InsertRange(0, parentObjects);
                            parentObjectList.Add(newparents[j]);
                        }
                    }
                }
                else
                {
                    if (parentObjects != null && parentObjects.Count > 0)
                    {
                        if (parents.Count == 0)
                        {
                            parentObjectList.Add(parentObjects);
                        }
                        else
                        {
                            for (int i = 0; i < parents.Count; i++)
                            {
                                parentObjects.InsertRange(0, parents[i]);
                                parentObjectList.Add(parents[i]);
                            }
                        }
                    }
                }

            }

            return parentObjectList;
        }

        private static Dictionary<XmlSchemaElement, List<XmlSchemaElement>> GetXmlSchemaElementReferenceDictionary(XmlSchema xmlSchema)
        {
            XmlSchemaSet schemaSet = new XmlSchemaSet();
            schemaSet.Add(xmlSchema);
            if (xmlSchema.IsCompiled == false)
            {
                schemaSet.ValidationEventHandler += new ValidationEventHandler(ShowCompileErrors);
                schemaSet.Compile();
            }

            Dictionary<XmlSchemaElement, List<XmlSchemaElement>> elements = new Dictionary<XmlSchemaElement, List<XmlSchemaElement>>();

            foreach (XmlSchemaElement el in xmlSchema.Elements.Values)
            {
                elements = GetXmlSchemaElementReferenceDictionary(xmlSchema, el, elements);
            }

            #region Debug Print
            //int i = 0;
            //foreach (XmlSchemaElement el in elements.Keys)
            //{
            //    List<string> paths = new List<string>();
            //    paths = GetElementPath(elements, el, paths);

            //    int j = 0;
            //    foreach (string str in paths)
            //    {
            //        System.Diagnostics.Debug.WriteLine(String.Format("\t{0}: ElementName: {1};\t{2}. Path: {3};", i, el.QualifiedName.ToString(), j++, str));
            //    }
            //    i++;
            //}

            //i = 0;
            //foreach (XmlSchemaElement el in elements.Keys)
            //{
            //    List<List<XmlSchemaElement>> parents = new List<List<XmlSchemaElement>>();
            //    parents = GetElementParentList(elements, el, parents);

            //    int j = 0;
            //    foreach (List<XmlSchemaElement> ls in parents)
            //    {
            //        string str = "";
            //        foreach (XmlSchemaElement p in ls)
            //        {
            //            if (elements.ContainsKey(p) && elements[p] != null && elements[p].Count > 0)
            //            {
            //                foreach (XmlSchemaElement pref in elements[p])
            //                {
            //                    str += String.Format("; {0} (PRef: {1} - MinOccurs: {2} - SchemaTypeName: {3})", pref.QualifiedName.ToString(), pref.RefName.ToString(), pref.MinOccursString, pref.SchemaTypeName);
            //                }
            //            }
            //            else
            //            {
            //                str += String.Format("; {0} (Ref: {1} - MinOccurs: {2} - SchemaTypeName: {3})", p.QualifiedName.ToString(), p.RefName.ToString(), p.MinOccursString, p.SchemaTypeName);
            //            }
            //        }
            //        System.Diagnostics.Debug.WriteLine(String.Format("\t{0}: ElementName: {1};\t{2}. Parents: {3};", i, el.QualifiedName.ToString(), j++, str));
            //    }
            //    i++;
            //}
            #endregion Debug Print

            return elements;
        }

        private static Dictionary<XmlSchemaElement, List<XmlSchemaElement>> GetXmlSchemaElementReferenceDictionary(XmlSchema xs, XmlSchemaParticle particle, Dictionary<XmlSchemaElement, List<XmlSchemaElement>> elements)
        {
            if (xs == null) return null;

            if (elements == null)
            {
                elements = new Dictionary<XmlSchemaElement, List<XmlSchemaElement>>();
            }

            if (particle is XmlSchemaElement)
            {
                XmlSchemaElement elem = (XmlSchemaElement)particle;
                if (!elem.RefName.IsEmpty)
                {
                    foreach (XmlSchemaElement refelem in xs.Elements.Values)
                    {
                        if (refelem.QualifiedName == elem.RefName)
                        {
                            if (!elements.ContainsKey(refelem))
                            {
                                List<XmlSchemaElement> ls = new List<XmlSchemaElement>();
                                ls.Add(elem);
                                elements.Add(refelem, ls);
                            }
                            else
                            {
                                // BLG_1938
                                if (elements[refelem] != null)
                                {
                                    if (!elements[refelem].Contains(elem))
                                    {
                                        elements[refelem].Add(elem);
                                    }
                                }
                            }
                            //System.Diagnostics.Debug.WriteLine(String.Format("refName: {0};QualifiedName: {1};Name: {2};ElementSchemaType: {3};Parent: {4}", elem.RefName.ToString(), elem.QualifiedName.ToString(), elem.Name, elem.ElementSchemaType.TypeCode, (elem.Parent as XmlSchemaElement) == null ? ((elem.Parent as XmlSchemaComplexType) == null ? ((elem.Parent as XmlSchemaGroupBase) == null ? "<null>" : ((XmlSchemaGroupBase)elem.Parent).ToString()) : ((XmlSchemaComplexType)elem.Parent).QualifiedName.ToString()) : ((XmlSchemaElement)elem.Parent).QualifiedName.ToString()));
                            elements = GetXmlSchemaElementReferenceDictionary(xs, refelem, elements);
                        }
                    }
                }
                else
                {
                    // nem volt (még) rá hivatkozás
                    if (!elements.ContainsKey(elem))
                    {
                        elements.Add(elem, null);
                    }

                    XmlSchemaType type = (XmlSchemaType)elem.ElementSchemaType;
                    XmlSchemaComplexType complexType = type as XmlSchemaComplexType;
                    if (complexType != null && complexType.Name == null)
                    {
                        elements = GetXmlSchemaElementReferenceDictionary(xs, complexType.ContentTypeParticle, elements);
                    }

                    //System.Diagnostics.Debug.WriteLine(String.Format("QualifiedName: {0};Name: {1};ElementSchemaType: {2};Parent: {3}", elem.QualifiedName.ToString(), elem.Name, elem.ElementSchemaType.TypeCode, (elem.Parent as XmlSchemaElement) == null ? ((elem.Parent as XmlSchemaComplexType) == null ? ((elem.Parent as XmlSchemaGroupBase) == null ? "<null>" : ((XmlSchemaGroupBase)elem.Parent).ToString()) : ((XmlSchemaComplexType)elem.Parent).QualifiedName.ToString()) : ((XmlSchemaElement)elem.Parent).QualifiedName.ToString()));
                }
            }
            else if (particle is XmlSchemaGroupBase)
            { //xs:all, xs:choice, xs:sequence
                XmlSchemaGroupBase baseParticle = particle as XmlSchemaGroupBase;
                foreach (XmlSchemaParticle subParticle in baseParticle.Items)
                    elements = GetXmlSchemaElementReferenceDictionary(xs, subParticle, elements);
            }

            return elements;
        }


        #region Traversing - alapkód - KOMMENT
        //public static void StartTraverseXmlSchema(XmlSchema xs)
        //{
        //    if (xs == null) return;

        //    if (xs.IsCompiled == false)
        //    {
        //        XmlSchemaSet schemaSet = new XmlSchemaSet();
        //        schemaSet.Add(xs);
        //        schemaSet.ValidationEventHandler += new ValidationEventHandler(ShowCompileErrors);
        //        schemaSet.Compile();
        //    }

        //    XmlSchemaComplexType complexType;
        //    foreach (XmlSchemaType type in xs.SchemaTypes.Values)
        //    {
        //        complexType = type as XmlSchemaComplexType;
        //        if (complexType != null)
        //            TraverseParticle(complexType.ContentTypeParticle);
        //    }

        //    foreach (XmlSchemaElement el in xs.Elements.Values)
        //        TraverseParticle(el);

        //}

        //public static void TraverseParticle(XmlSchemaParticle particle)
        //{
        //    if (particle is XmlSchemaElement)
        //    {
        //        XmlSchemaElement elem = particle as XmlSchemaElement;

        //        if (elem.RefName.IsEmpty)
        //        {
        //            XmlSchemaType type = (XmlSchemaType)elem.ElementSchemaType;
        //            XmlSchemaComplexType complexType = type as XmlSchemaComplexType;
        //            if (complexType != null && complexType.Name == null)
        //                TraverseParticle(complexType.ContentTypeParticle);

        //            //System.Diagnostics.Debug.WriteLine(String.Format("QualifiedName: {0};Name: {1};ElementSchemaType: {2};Parent: {3}", elem.QualifiedName.ToString(), elem.Name, elem.ElementSchemaType.TypeCode, (elem.Parent as XmlSchemaElement) == null ? "<null>" : ((XmlSchemaElement)elem.Parent).QualifiedName.ToString()));
        //        }
        //    }
        //    else if (particle is XmlSchemaGroupBase)
        //    { //xs:all, xs:choice, xs:sequence
        //        XmlSchemaGroupBase baseParticle = particle as XmlSchemaGroupBase;
        //        foreach (XmlSchemaParticle subParticle in baseParticle.Items)
        //            TraverseParticle(subParticle);
        //    }
        //}
        #endregion Traversing - alapkód - KOMMENT

        #endregion XmlSchema Traversing

    } // class
} // namespace