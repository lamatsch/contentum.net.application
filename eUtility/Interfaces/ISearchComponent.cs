﻿namespace Contentum.eUtility
{
    public interface ISearchComponent
    {
        string GetSearchText();
    }
}
