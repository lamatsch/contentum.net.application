using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contentum.eUtility
{
    public interface ISelectableUserComponent
    {
        List<WebControl> GetComponentList();

        bool ViewEnabled
        {
            get;
            set;
        }

        bool ViewVisible
        {
            get;
            set;
        }

        string ClientID
        {
            get;
        }

        string UniqueID
        {
            get;
        }
    }

    public interface ISelectableUserComponentContainer
    {
        List<Control> GetComponentList();
    }

    public interface IComponentSelectControl
    {
        bool Enabled { get; set; }
        void Add_ComponentOnClick(System.Web.UI.WebControls.WebControl webControl);
        void Add_ComponentOnClick(System.Web.UI.HtmlControls.HtmlControl htmlControl);
        void Add_ComponentOnClick(ISelectableUserComponent userComponent);
        void Add_ComponentOnClick(ISelectableUserComponentContainer componentContainer);
        void Add_ComponentOnClick(List<Control> componentList);
        void Add_ComponentChildsOnClick(Control component);
        void Add_ComponentChildsOnClick(Control component, List<Control> excludedComponents);
    }
}
