using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eBusinessDocuments;

namespace Contentum.eUtility
{
    public class JavaScripts
    {
        /// <summary>
        /// RegisterCloseWindowClientScript(Page, true);   --> default window_returnValue �rt�k: true
        /// </summary>
        /// <param name="ParentPage"></param>
        public static void RegisterCloseWindowClientScriptWithScripManager(Page ParentPage)
        {
            if (ParentPage == null) return;
            RegisterCloseWindowClientScriptWithScripManager(ParentPage, true);
        }

        public static void RegisterCloseWindowClientScriptWithScripManager(Page ParentPage, bool window_returnValue)
        {
            if (ParentPage == null) return;

            string returnValue = window_returnValue ? "true" : "false";

            string script = "window.returnValue=" + returnValue + ";";
            //firefox eset�n is legyen friss�t�s, b�r az aszinkron nem m�k�dik, ez�rt nincs �tadva postbackcontrol
            script += "if(window.opener && window.returnValue && window.opener.__doPostBack){window.opener.__doPostBack('',window.opener.postBackArgument);}";
            script += "window.close();";

            //ScriptManager.RegisterStartupScript(ParentPage, ParentPage.GetType(), "WindowClose", "window.returnValue=" + returnValue + "; window.close();", true);
            ScriptManager.RegisterStartupScript(ParentPage, ParentPage.GetType(), "WindowClose", script, true);
        }

        /// <summary>
        /// RegisterCloseWindowClientScript(Page, true);   --> default window_returnValue �rt�k: true
        /// </summary>
        /// <param name="ParentPage"></param>
        public static void RegisterCloseWindowClientScript(Page ParentPage)
        {
            if (ParentPage == null) return;
            RegisterCloseWindowClientScript(ParentPage, true);
        }

        public static void RegisterCloseWindowClientScript(Page ParentPage, bool window_returnValue)
        {
            if (ParentPage == null) return;

            string returnValue = window_returnValue ? "true" : "false";
            string script = "window.returnValue=" + returnValue + ";";
            //firefox eset�n is legyen friss�t�s, b�r az aszinkron nem m�k�dik, ez�rt nincs �tadva postbackcontrol
            script += "if(window.opener && window.returnValue && window.opener.__doPostBack){window.opener.__doPostBack('',window.opener.postBackArgument);}";
            script += "window.close();";
            ParentPage.ClientScript.RegisterStartupScript(ParentPage.GetType(), "WindowClose", script, true);            
        }

        public static void RegisterCloseWindowClientScriptforChrome(Page ParentPage)
        {
            if (ParentPage == null) return;
            RegisterCloseWindowClientScriptforChrome(ParentPage, true);
        }

        public static void RegisterCloseWindowClientScriptforChrome(Page ParentPage, bool window_returnValue)
        {
            if (ParentPage == null) return;

            string returnValue = window_returnValue ? "true" : "false";
            string script = "window.returnValue=" + returnValue + ";";
            //firefox eset�n is legyen friss�t�s, b�r az aszinkron nem m�k�dik, ez�rt nincs �tadva postbackcontrol
            script += "if(window.opener && window.returnValue && window.opener.__doPostBack){window.opener.__doPostBack('',window.opener.postBackArgument);}";
            script += "window.close();";
            ScriptManager.RegisterClientScriptBlock(ParentPage, ParentPage.GetType(), "close", script, true);
        }

        public static void RegisterReturnValuesToParentWindowClientScript(Page page, Dictionary<string, string> ReturnComponentsAndValues)
        {
            if (page == null || ReturnComponentsAndValues == null) return;
            RegisterReturnValuesToParentWindowClientScript(page, ReturnComponentsAndValues, "ReturnValuesToParentWindow");
        }


        public static void RegisterReturnValuesToParentWindowClientScript(Page page, Dictionary<string, string> ReturnComponentsAndValues, string scriptId)
        {
            RegisterReturnValuesToParentWindowClientScript(page, ReturnComponentsAndValues, scriptId, false, false);
        }

        public static void RegisterReturnValuesToParentWindowClientScript(Page page, Dictionary<string, string> ReturnComponentsAndValues
            , string scriptId, bool registerWithScriptManager)
        {
            RegisterReturnValuesToParentWindowClientScript(page, ReturnComponentsAndValues, scriptId, registerWithScriptManager, false);
        }

        public static void RegisterReturnValuesToParentWindowClientScript(Page page, Dictionary<string, string> ReturnComponentsAndValues
            , string scriptId, bool registerWithScriptManager, bool tryFireChangeEvent)
        {
            RegisterReturnValuesToParentWindowClientScript(page, ReturnComponentsAndValues, scriptId, registerWithScriptManager, tryFireChangeEvent, false);
        }

        public static void RegisterReturnValuesToParentWindowClientScript(Page page, Dictionary<string, string> ReturnComponentsAndValues
            , string scriptId, bool registerWithScriptManager, bool tryFireChangeEvent, bool forceFireChangeEventIfDisabled)
        {
            if (page == null || ReturnComponentsAndValues == null) return;
            string script = "";
            foreach (KeyValuePair<string, string> kvp in ReturnComponentsAndValues)
            {


                string componentId = kvp.Key;
                string lovValue = kvp.Value;


                if (!String.IsNullOrEmpty(componentId))
                {

                    // spec. karakterek kicser�l�se (pl. ' (aposztr�f))
                    lovValue = ChangeSpecJavascriptChars(lovValue);

                    script += " if (window.dialogArguments) { ";
                    //BUG 5197
                    if (componentId.Contains("DropDown") && lovValue.Contains(";"))
                    {
                        string dropDownScript = "";
                        try
                        {
                            //BUG 5197 Ez els� el�fordul� ';' karakter hat�rozza meg, meddig tart az id
                            int firstSeparator = lovValue.IndexOf(";");
                            string optionId = lovValue.Substring(0, firstSeparator);
                            string optionValue = lovValue.Length > firstSeparator ? lovValue.Substring(firstSeparator + 1) : "";

                            dropDownScript += " var dropDownElement = window.dialogArguments.parentWindow.document.getElementById('" + componentId + "'); \n";
                            dropDownScript += " var dropDownNewOption = window.dialogArguments.parentWindow.document.createElement('option'); \n";

                            dropDownScript += " dropDownNewOption.value = '" + optionId + "'; \n";
                            dropDownScript += " dropDownNewOption.innerHTML = '" + optionValue + "'; \n";
                            dropDownScript += " dropDownNewOption.selected = true; \n";
                            dropDownScript += " dropDownElement.appendChild(dropDownNewOption); \n";
                        }catch(Exception)
                        {
                            dropDownScript = "";
                        }
                        script += dropDownScript;
                    }
                    else
                    {
                        script += " window.dialogArguments.parentWindow.document.getElementById('" + componentId + "').value = '" + lovValue + "'; \n";
                    }
                    //BLG_2011
                    if (componentId.Contains("ViewImageButton") && lovValue == "1")
                    {
                        script += " window.dialogArguments.parentWindow.document.getElementById('" + componentId + "').disabled = true;";
                        script += " window.dialogArguments.parentWindow.document.getElementById('" + componentId + "').style.opacity = \"0.3\";";
                    }
                    else if (componentId.Contains("ViewImageButton") && lovValue == "0")
                    {
                        script += " window.dialogArguments.parentWindow.document.getElementById('" + componentId + "').disabled = false;";
                        script += " window.dialogArguments.parentWindow.document.getElementById('" + componentId + "').style.opacity = \"1.0\";";
                    }

                    if (tryFireChangeEvent)
                    {
                        if (forceFireChangeEventIfDisabled)
                        {
                            script += " var isDisabled = window.dialogArguments.parentWindow.document.getElementById('" + componentId + "').disabled; window.dialogArguments.parentWindow.document.getElementById('" + componentId + "').disabled = false; ";
                        }
                        script += " window.dialogArguments.parentWindow.$common.tryFireEvent(window.dialogArguments.parentWindow.document.getElementById('" + componentId + "'), 'change',{lovlist:1}); ";
                        if (forceFireChangeEventIfDisabled)
                        {
                            script += " window.dialogArguments.parentWindow.document.getElementById('" + componentId + "').disabled = isDisabled; ";
                        }
                    }
                    script += " } else { "
                        + " window.opener.document.getElementById('" + componentId + "').value = '" + lovValue + "'; \n";
                    if (tryFireChangeEvent)
                    {
                        if (forceFireChangeEventIfDisabled)
                        {
                            script += " var isDisabled = window.opener.document.getElementById('" + componentId + "').disabled; window.opener.document.getElementById('" + componentId + "').disabled = false; ";
                        }
                        script += " window.opener.$common.tryFireEvent(window.opener.document.getElementById('" + componentId + "'), 'change',{lovlist:1});";
                        if (forceFireChangeEventIfDisabled)
                        {
                            script += " window.opener.document.getElementById('" + componentId + "').disabled = isDisabled; ";
                        }
                    }


                    script += " }";
                }
            }
            if (registerWithScriptManager)
            {
                ScriptManager.RegisterStartupScript(page, page.GetType(), scriptId, script, true);
            }
            else
            {
                page.ClientScript.RegisterStartupScript(page.GetType(), scriptId
                    , script, true);
            }

        }

        public static string ChangeSpecJavascriptChars(string js_string)
        {
            if (js_string != null)
            {
                // ' (aposztr�f) kicser�l�se \' -re (hogy a javascriptes string �sszef�z�sn�l ne zavarjon be)
                js_string = js_string.Replace("'", "\\'");
                return js_string;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Visszak�ldi a h�v� oldal egy HiddenField-j�be a megadott Id-t �s egy TextBox�ba a megadott sz�veget
        /// Visszat�r�si �rt�k true, ha van HiddenFieldId nev� param�ter a parentPage.Request.QueryString -ben
        /// </summary>
        public static bool SendBackResultToCallingWindow(Page parentPage, string newRecordId, string textBoxText)
        {
            return SendBackResultToCallingWindow(parentPage, newRecordId, textBoxText, false, false);
        }

        /// <summary>
        /// Visszak�ldi a h�v� oldal egy HiddenField-j�be a megadott Id-t �s egy TextBox�ba a megadott sz�veget
        /// Visszat�r�si �rt�k true, ha van HiddenFieldId nev� param�ter a parentPage.Request.QueryString -ben
        /// </summary>
        public static bool SendBackResultToCallingWindow(Page parentPage, string newRecordId, string textBoxText
            , bool registerWithScriptManager)
        {
            return SendBackResultToCallingWindow(parentPage, newRecordId, textBoxText, registerWithScriptManager, false);
        }


        /// <summary>
        /// Visszak�ldi a h�v� oldal egy HiddenField-j�be a megadott Id-t �s egy TextBox�ba a megadott sz�veget
        /// Visszat�r�si �rt�k true, ha van HiddenFieldId nev� param�ter a parentPage.Request.QueryString -ben
        /// </summary>
        public static bool SendBackResultToCallingWindow(Page parentPage, string newRecordId, string textBoxText
            , bool registerWithScriptManager, bool tryFireChangeEvent)
        {
            return SendBackResultToCallingWindow(parentPage, newRecordId, textBoxText, registerWithScriptManager, tryFireChangeEvent, false);
        }



        /// <summary>
        /// Visszak�ldi a h�v� oldal egy HiddenField-j�be a megadott Id-t �s egy TextBox�ba a megadott sz�veget
        /// Visszat�r�si �rt�k true, ha van HiddenFieldId nev� param�ter a parentPage.Request.QueryString -ben
        /// </summary>
        public static bool SendBackResultToCallingWindow(Page parentPage, string newRecordId, string textBoxText
           , bool registerWithScriptManager, bool tryFireChangeEvent, bool elosztoIvek = false)
        {
            if (parentPage == null) return false;

            Dictionary<string, string> returnValues = new Dictionary<string, string>();

            #region BLG_335
            //LZS - BLG_335 - Ha vannak �tadott kl�nozand� control id-k, akkor azok �rt�k�t kiszedj�k, �s �tadjuk a returnValues dictionary-nek.
            string cloneHiddenfieldId = parentPage.Request.QueryString.Get(QueryStringVars.CloneHiddenFieldId);
            string cloneTextBoxId = parentPage.Request.QueryString.Get(QueryStringVars.CloneTextBoxId);

            if (!String.IsNullOrEmpty(cloneHiddenfieldId) && !String.IsNullOrEmpty(cloneTextBoxId))
            {
                returnValues.Add(cloneHiddenfieldId, newRecordId);
                returnValues.Add(cloneTextBoxId, textBoxText);
            }

            #endregion

            #region BUG 5197 kapcsolat partner dropdown id
            string kapcsoltPartnerDropDownId = parentPage.Request.QueryString.Get(QueryStringVars.KapcsoltPartnerDropDownId);
            if (!String.IsNullOrEmpty(kapcsoltPartnerDropDownId))
            {
                returnValues.Add(kapcsoltPartnerDropDownId, newRecordId + ";" + textBoxText);
            }
            #endregion

            string hiddenFieldId = parentPage.Request.QueryString.Get(QueryStringVars.HiddenFieldId);
            if (!String.IsNullOrEmpty(hiddenFieldId))
            {
                returnValues.Add(hiddenFieldId, newRecordId);


                string textBoxId = parentPage.Request.QueryString.Get(QueryStringVars.TextBoxId);
                if (textBoxId != null && textBoxText != null)
                {
                    returnValues.Add(textBoxId, textBoxText);
                }

                var cloneFieldId = parentPage.Request.QueryString.Get(QueryStringVars.CloneFieldId);
                if (cloneFieldId != null && textBoxText != null)
                {
                    returnValues.Add(cloneFieldId, textBoxText);
                }

                string viewImageButtonId = parentPage.Request.QueryString.Get(QueryStringVars.ViewImageButtonId);
                if (viewImageButtonId != null)
                {
                    returnValues.Add(viewImageButtonId, elosztoIvek ? "1" : "0");
                }

                JavaScripts.RegisterReturnValuesToParentWindowClientScript(parentPage, returnValues
                    , "ReturnValuesToParentWindow", registerWithScriptManager, tryFireChangeEvent);
                return true;

            }
            return false;
        }



        public static string SetOnCLientClick_NoPostBack(string PageUrl, string queryString, int Width, int Height)
        {
            string pref = "";
            if (!String.IsNullOrEmpty(queryString))
                pref = "?";

            string javascript = "popup('" + PageUrl + pref + queryString + "'," + Width.ToString() + "  , " + Height.ToString() + "); return false;";

            return javascript;
        }

        public static string SetOnClientClick(string PageUrl, string queryString, int Width, int Height, string PostBackControlClientName, string PostBackArgument, bool ForwardRequestToServer)
        {
            return SetOnClientClick(PageUrl, queryString, Width, Height, PostBackControlClientName, PostBackArgument, ForwardRequestToServer, false);
        }

        public static string SetOnClientClick(string PageUrl, string queryString, int Width, int Height, string PostBackControlClientName, string PostBackArgument, bool ForwardRequestToServer, bool noDialog)
        {
            string javascript = "";
            string pref = "";
            string strEndRequest = " return false;";
            if (!String.IsNullOrEmpty(queryString))
                pref = "?";
            if (ForwardRequestToServer)
                strEndRequest = "";
            javascript = "if (typeof(event) != 'undefined' && event) { window.savedEvent = event; } window.postBackArgument = '" + PostBackArgument + "'; window.postBackControl = '" + PostBackControlClientName + "';" +
                       "if (popup('" + PageUrl + pref + queryString + "'," + Width.ToString() + "  , " + Height.ToString() + " , " + noDialog.ToString().ToLower() + ")) {__doPostBack('" + PostBackControlClientName + "','" + PostBackArgument + "');}" + strEndRequest;
            return javascript;
        }
        /// <summary>
        /// Set Partner url OnClientClick
        /// </summary>
        /// <param name="PageUrl"></param>
        /// <param name="hiddenFieldClientId"></param>
        /// <param name="queryString"></param>
        /// <param name="Width"></param>
        /// <param name="Height"></param>
        /// <param name="PostBackControlClientName"></param>
        /// <param name="PostBackArgument"></param>
        /// <param name="ForwardRequestToServer"></param>
        /// <returns></returns>
        public static string SetPartnerOnClientClick(string PageUrl, string hiddenFieldClientId, string queryString, int Width, int Height, string PostBackControlClientName, string PostBackArgument, bool ForwardRequestToServer)
        {
            string javascript = "";
            string pref = "";
            string strEndRequest = " return false;";
            if (!string.IsNullOrEmpty(queryString))
                pref = "?";
            if (ForwardRequestToServer)
                strEndRequest = "";

            javascript ="var hidden = document.getElementById('" + hiddenFieldClientId + "').value;  " +
                        "if (typeof(event) != 'undefined' && event) " +
                        "{ window.savedEvent = event; } " +
                        "window.postBackArgument = '" + PostBackArgument + "';" +
                        " window.postBackControl = '" + PostBackControlClientName + "';" +
                        "if (popup('" + PageUrl + pref
                        + QueryStringVars.PartnerId + "='+hidden" + " + '&"
                        + queryString + "',"
                        + Width.ToString() + "  , "
                        + Height.ToString() + ")) " +
                        "{__doPostBack('" + PostBackControlClientName + "','" + PostBackArgument + "');}"
                        + strEndRequest;
            return javascript;
        }

        /// <summary>
        /// Az updateProgress-t mindenk�ppen elind�tjuk
        /// </summary>        
        public static string SetOnClientClick_DisplayUpdateProgress(string PageUrl, string queryString, int Width, int Height, string PostBackControlClientName, string PostBackArgument, string UpdateProgressClientId)
        {
            string javascript = "";
            string pref = "";
            string strEndRequest = " return false;";
            if (!String.IsNullOrEmpty(queryString))
                pref = "?";
            javascript = "if (typeof(event) != 'undefined' && event) { window.savedEvent = event; } window.postBackArgument = '" + PostBackArgument + "'; window.postBackControl = '" + PostBackControlClientName + "';" +
                       "if (popup('" + PageUrl + pref + queryString + "'," + Width.ToString() + "  , "
                       + Height.ToString() + ")) { $get('" + UpdateProgressClientId + "').style.display = 'block'; "
                       + " __doPostBack('" + PostBackControlClientName + "','" + PostBackArgument + "');}" + strEndRequest;
            return javascript;
        }

        public static string SetOnClientClick(string PageUrl, string queryString, int Width, int Height, string PostBackControlClientName, string PostBackArgument)
        {
            return SetOnClientClick(PageUrl, queryString, Width, Height, PostBackControlClientName, PostBackArgument, false);
        }

        public static string SetOnClientClick(string PageUrl, string queryString, int Width, int Height, string PostBackControlClientName)
        {
            return SetOnClientClick(PageUrl, queryString, Width, Height, PostBackControlClientName, "");
        }


        public static void RegisterActiveTabChangedClientScript(Page ParentPage, Control TabContainer)
        {
            if (ParentPage == null || TabContainer == null) return;

            string js = @"<script language='JavaScript' type='text/javascript'>function ActiveTabChanged(sender, e)
                            {
                              var activeTab = sender.get_activeTab().get_tabIndex();
                              __doPostBack('" + TabContainer.UniqueID + @"', 'activeTabChanged:' + activeTab);
                            } </script>";

            ParentPage.ClientScript.RegisterClientScriptBlock(ParentPage.GetType(), "ActiveTabChanged", js);
        }

        /// <summary>
        /// Javascript file regisztr�l�sa az oldalhoz
        /// </summary>
        /// <param name="page"></param>
        /// <param name="scriptFile"></param>
        public static void registerOuterScriptFile(Page page, string key, string scriptFile)
        {
            if (page == null || key == null || scriptFile == null) return;

            if (!page.ClientScript.IsClientScriptBlockRegistered(scriptFile))
            {
                page.ClientScript.RegisterClientScriptBlock(page.GetType(),
                    key,
                    "<script language='javascript' type='text/javascript' src='" + scriptFile
                    + "'></script>");
            }
        }



        public static void RegisterPopupWindowClientScript(Page ParentPage)
        {
            if (ParentPage == null) return;

            //if (!ParentPage.ClientScript.IsClientScriptBlockRegistered("PopupWindow"))
            //{
            //var height = 350;
            //var width = 450;
            string query = UI.GetLoginDataQueryString(ParentPage);
            string js = @"function popupwindow(url, width, height, noDialog)
                                {		
                                    var left = (screen.availWidth - width)/2;
                                    var top = (screen.availHeight - height)/2;
                                    if (window.showModelessDialog && !noDialog)
                                    {
                                        var dialogArguments = new Object();
                                        dialogArguments.parentWindow = window.self;
                                        if(url.indexOf('?')  > -1)
                                        {
                                            url += '&';
                                        }
                                        else
                                        {
                                            url += '?';
                                        }
                                        url += '" + QueryStringVars.WindowType + "=" + Constants.WindowType.Popup + @"'
                                        url += '" + (String.IsNullOrEmpty(query) ? String.Empty : ("&" + query)) + @"';
                                        var _R = window.showModalDialog(url, dialogArguments, 'dialogWidth='+width+'px;dialogHeight='+height+'px;scroll=yes;status=yes;');
                                        if ('undefined' != typeof(_R))
                                        {
                                            //SetName(_R.strName);	
                                        }		                            
                                        return _R;
                                    }		
                                    else	//NS			
                                    {  	
                                        var left = (screen.width-width)/2;
                                        var top = (screen.height-height)/2;
                                        winHandle = window.open(url, '', 'modal=yes,toolbar=false,location=false,directories=false,status=false,menubar=false,scrollbars=yes,resizable=no,left='+left+',top='+top+',width='+width+',height='+height);
                                        winHandle.focus();
                                    }

                                    return false;
                                    }

                                    function popup(pagename, width, height, noDialog)
                                    {
                                            var mystr = popupwindow(pagename, width, height, noDialog);
                                            if (mystr == null || mystr=='undefined')
                                            {                                            
                                                return false;
                                            }
                                            return mystr;
                                    }";

            //    ParentPage.ClientScript.RegisterClientScriptBlock(ParentPage.GetType(), "PopupWindow", js);
            //}

            ScriptManager.RegisterClientScriptBlock(ParentPage, ParentPage.GetType(), "PopupWindow", js, true);
        }

        /// <summary>
        /// Felugr� ablak a megadott URL oldallal; Id param�ter�be beteszi a megadott HiddenField �rt�k�t
        /// </summary>
        /// <param name="PageUrl">Lap URL </param>
        /// <param name="QueryString">Egy�b URL param�terek ('Commmand=View&Id=...'-n k�v�l)</param>
        /// <param name="HiddenFieldClientId">A HiddenField kliensoldali id-ja, amelynek �rt�ke lesz az 'Id' param. �rt�ke</param>        
        /// <returns>Az el��ll�tott javascript parancs, ami az onClient attr.-ba ker�l</returns>
        public static string SetOnClientClick_FormViewByHiddenField(string PageUrl, string QueryString, string HiddenFieldClientId)
        {
            // default m�ret� popup-ablak
            return SetOnClientClick_FormViewByHiddenField(PageUrl, QueryString, HiddenFieldClientId, "", Defaults.PopupWidth, Defaults.PopupHeight);
        }

        /// <summary>
        /// Felugr� ablak a megadott URL oldallal; Id param�ter�be beteszi a megadott HiddenField �rt�k�t
        /// </summary>
        /// <param name="PageUrl">Lap URL </param>
        /// <param name="QueryString">Egy�b URL param�terek ('Commmand=View&Id=...'-n k�v�l)</param>
        /// <param name="HiddenFieldClientId">A HiddenField kliensoldali id-ja, amelynek �rt�ke lesz az 'Id' param. �rt�ke</param>
        /// <param name="extenderBehaviorID">Az extender kliens oldali id-je, amiben az alternat�v id �rt�ke tal�lhat�. Elvileg ua mint a HiddenFiled �rt�ke (FF miall)</param>
        /// <returns>Az el��ll�tott javascript parancs, ami az onClient attr.-ba ker�l</returns>
        public static string SetOnClientClick_FormViewByHiddenField(string PageUrl, string QueryString, string HiddenFieldClientId, string extenderBehaviorID)
        {
            // default m�ret� popup-ablak
            return SetOnClientClick_FormViewByHiddenField(PageUrl, QueryString, HiddenFieldClientId, extenderBehaviorID, Defaults.PopupWidth, Defaults.PopupHeight);
        }

        public static string SetOnClientClick_FormViewByHiddenField(string PageUrl, string QueryString, string HiddenFieldClientId, int PopupWidth, int PopupHeight)
        {
            return SetOnClientClick_FormViewByHiddenField(PageUrl, QueryString, HiddenFieldClientId, "", PopupWidth, PopupHeight);
        }

        public static string SetOnClientClick_FormViewByHiddenField(string PageUrl, string QueryString, string HiddenFieldClientId, string extenderBehaviorID, int PopupWidth, int PopupHeight)
        {
            string javascript = "";

            javascript =
                " var hidden = document.getElementById('" + HiddenFieldClientId + "'); "
                + " var ext = $find('" + extenderBehaviorID + "'); "
                + " if ((hidden.value==null || hidden.value=='') && ext!=null) { hidden.value=ext.hiddenValue; }"
                + " if (hidden.value!=null && hidden.value!='') "
                + " { popup('" + PageUrl + "?" + QueryStringVars.Id + "='+hidden.value"
                + " + '&" + CommandName.Command + "=" + CommandName.View + "'";

            if (!String.IsNullOrEmpty(QueryString))
            {
                javascript += " + '&" + QueryString + "'";
            }

            javascript += ", " + PopupWidth.ToString()
                + ", " + PopupHeight.ToString()
                + " ); } else { alert('" + Contentum.eUtility.Resources.Error.GetString("UINoSelectedItem") + "'); } "
                + " return false;";

            return javascript;
        }

        public static string SetOnClientClick_FormModifyByHiddenField(string PageUrl, string QueryString, string HiddenFieldClientId
           , int PopupWidth, int PopupHeight)
        {
            string javascript = "";

            javascript = " var hidden = document.getElementById('" + HiddenFieldClientId + "'); "
                + " if (hidden.value!=null && hidden.value!='') "
                + " { popup('" + PageUrl + "?" + QueryStringVars.Id + "='+hidden.value"
                + "+'&" + CommandName.Command + "=" + CommandName.Modify + "'";

            if (!String.IsNullOrEmpty(QueryString))
            {
                javascript += "+'&" + QueryString + "'";
            }

            javascript += ", " + PopupWidth.ToString()
                + ", " + PopupHeight.ToString()
                + " ); } else { alert('" + Contentum.eUtility.Resources.Error.GetString("UINoSelectedItem") + "'); } "
                + " return false;";

            return javascript;
        }

        /*
         --------------- SSRS �j oldal megnyit�s --------------
         */

        public static string SetSSRSPrintOnClientClick(string PageUrl, string QueryString)
        {
            string javascript = String.Format("javascript:window.open('{0}{1}');return false;"
                , PageUrl, String.IsNullOrEmpty(QueryString) ? "" : String.Concat("?", QueryString));

            return javascript;
        }

        /*
         --------------- Kuldemeny Alert-ek -------------------
         */
        public static string SetOnClientClickNemIktathatoKuldemenyAlert()
        {
            string javascript = "";
            javascript = "alert('" + Contentum.eUtility.Resources.Error.GetString("UINemIktathatoKuldemeny") + "'); return false;";
            return javascript;
        }

        public static string SetOnClientClickIktatasNemTagadhatoMegKuldemenyAlert()
        {
            string javascript = "";
            javascript = "alert('" + Contentum.eUtility.Resources.Error.GetString("UIIktatasNemTagadhatoMegKuldemeny") + "'); return false;";
            return javascript;
        }

        public static string SetOnClientClickNemLezarhatoKuldemenyAlert()
        {
            string javascript = "";
            javascript = "alert('" + Contentum.eUtility.Resources.Error.GetString("UINemLezarhatoKuldemeny") + "'); return false;";
            return javascript;
        }

        public static string SetOnClientClickNemSztornirozhatoKuldemenyAlert()
        {
            string javascript = "";
            javascript = "alert('" + Contentum.eUtility.Resources.Error.GetString("UINemSztornirozhatoKuldemeny") + "'); return false;";
            return javascript;
        }

        /*
         --------------- /Kuldemeny Alert-ek -------------------
         */

        /*
         --------------- IrattariKikero Alert-ek -------------------
         */
        public static string SetOnClientClickNemJovahagyhatoKolcsonzesAlert()
        {
            string javascript = "";
            javascript = "alert('" + Contentum.eUtility.Resources.Error.GetString("UINemJovahagyhatoKolcsonzes") + "'); return false;";
            return javascript;
        }

        public static string SetOnClientClickNemVisszautasithatoKolcsonzesAlert()
        {
            string javascript = "";
            javascript = "alert('" + Contentum.eUtility.Resources.Error.GetString("UINemVisszautasithatoKolcsonzes") + "'); return false;";
            return javascript;
        }

        public static string SetOnClientClickNemSztornozhatoKolcsonzesAlert()
        {
            string javascript = "";
            javascript = "alert('" + Contentum.eUtility.Resources.Error.GetString("UINemSztornozhatoKolcsonzes") + "'); return false;";
            return javascript;
        }

        public static string SetOnClientClickNemKiadhatoKolcsonzesAlert()
        {
            string javascript = "";
            javascript = "alert('" + Contentum.eUtility.Resources.Error.GetString("UINemKiadhatoKolcsonzes") + "'); return false;";
            return javascript;
        }

        public static string SetOnClientClickNemVisszavehetoKolcsonzesAlert()
        {
            string javascript = "";
            javascript = "alert('" + Contentum.eUtility.Resources.Error.GetString("UINemVisszavehetoKolcsonzes") + "'); return false;";
            return javascript;
        }

        /*
         --------------- /Kuldemeny Alert-ek -------------------
         */


        public static string SetOnClientClickNoSelectedRow()
        {
            string javascript = "";
            javascript = "alert('" + Contentum.eUtility.Resources.Error.GetString("UINoSelectedRow") + "'); return false;";
            return javascript;
        }


        private static string SetOnClientClickConfirm(string GridViewClientId, string PostBackArgumen, string CheckBoxID, string confirmHeader, string confirmQuestion)
        {
            string javascript = "";

            if (GridViewClientId == null || CheckBoxID == null) return javascript;

            javascript =
            "var count = getSelectedCheckBoxesCount('" +
            GridViewClientId + "','" + CheckBoxID + "'); " +
            " if (count==0) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem")
            + "'); return false;} "
            + "else if (confirm('" + confirmHeader + " \\n \\n"
            + confirmQuestion
            + "\\n("
            + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount")
            + " '+count+')\\n"
             + "')) {  } else return false;";
            return javascript;
        }

        #region SetOnClientClickDeleteConfirm
        public static string SetOnClientClickDeleteConfirm(string GridViewClientId, string PostBackArgumen, string CheckBoxID)
        {
            //string javascript = "";

            //if (GridViewClientId == null || CheckBoxID == null) return javascript;

            //javascript =
            //"var count = getSelectedCheckBoxesCount('" +
            //GridViewClientId + "','" + CheckBoxID + "'); " +
            //" if (count==0) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem")
            //+ "'); return false;} "
            //+ "else if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Delete") + " \\n \\n"
            //+ Contentum.eUtility.Resources.Question.GetString("UIisDelete")
            //+ "\\n("
            //+ Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount")
            //+ " '+count+')\\n"
            // + "')) {  } else return false;";
            //return javascript;

            return SetOnClientClickConfirm(GridViewClientId, PostBackArgumen, CheckBoxID
                , Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Delete")
                , Contentum.eUtility.Resources.Question.GetString("UIisDelete"));
        }

        public static string SetOnClientClickDeleteConfirm(string GridViewClientId)
        {
            return SetOnClientClickDeleteConfirm(GridViewClientId, "", "");
        }

        public static string SetOnClientClickDeleteConfirm(string PostBackControlClientName, string PostBackArgumen)
        {
            return SetOnClientClickDeleteConfirm(PostBackControlClientName, PostBackArgumen, false);
        }

        public static string SetOnClientClickDeleteConfirm(string PostBackControlClientName, string PostBackArgumen, bool ForwardRequestToServer)
        {
            //string javascript = "";
            //string strEndRequest = " return false;";
            //if (ForwardRequestToServer)
            //    strEndRequest = "";
            //javascript = "if (confirm('" + Contentum.eUtility.Resources.Question.UIisDelete + "')) { __doPostBack('" + PostBackControlClientName + "','" + PostBackArgumen + "'); }" + strEndRequest;
            //return javascript;

            //string javascript = "";
            //string strEndRequest = " return false;";
            //if (ForwardRequestToServer)
            //    strEndRequest = "";
            //javascript =
            //    "var count = getSelectedCheckBoxesCount('" +
            //    PostBackControlClientName + "'); " +
            //    " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem
            //    + "');} "
            //    + "else if (confirm('" + Resources.List.UI_SelectedItemsCount
            //    + " '+count+'\\n \\n"
            //    + Contentum.eUtility.Resources.Question.UIisDelete + "')) { __doPostBack('" + PostBackControlClientName + "','" + PostBackArgumen + "'); }" + strEndRequest;
            //return javascript;

            return SetOnClientClickDeleteConfirm(PostBackControlClientName);
        }
        #endregion SetOnClientClickDeleteConfirm

        #region SetOnClientClickHierarchicalDeleteConfirm
        public static string SetOnClientClickHierarchicalDeleteConfirm(string GridViewClientId, string PostBackArgumen, string CheckBoxID)
        {
            return SetOnClientClickConfirm(GridViewClientId, PostBackArgumen, CheckBoxID
                , Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_HierarchicalDelete")
                , Contentum.eUtility.Resources.Question.GetString("UIConfirm_HierarchicalDelete"));
        }

        public static string SetOnClientClickHierarchicalDeleteConfirm(string GridViewClientId)
        {
            return SetOnClientClickHierarchicalDeleteConfirm(GridViewClientId, "", "");
        }

        public static string SetOnClientClickHierarchicalDeleteConfirm(string PostBackControlClientName, string PostBackArgumen)
        {
            return SetOnClientClickHierarchicalDeleteConfirm(PostBackControlClientName, PostBackArgumen, false);
        }

        public static string SetOnClientClickHierarchicalDeleteConfirm(string PostBackControlClientName, string PostBackArgumen, bool ForwardRequestToServer)
        {
            return SetOnClientClickHierarchicalDeleteConfirm(PostBackControlClientName);
        }

        #endregion SetOnClientClickHierarchicalDeleteConfirm

        public static string SetOnClientClickRefreshMasterList(string PostBackControlClientName)
        {
            string javascript = "__doPostBack('" + PostBackControlClientName + "','" + EventArgumentConst.refreshMasterList + "');";
            return javascript;
        }
        #region Fel�lvizsg�lat szerelt vagy csatolt ellen�rz�s
        //bernat.laszlo added
        public static string SetOnClientClickFitterOrAttachedConfirm(string GridViewCliendId)
        {
            string javascript = "";
            javascript = "var count = getSelectedCheckBoxesCount('" + GridViewCliendId + "','check'); "
            + "if (count==0) {alert('" + Resources.List.GetString("UI_NoSelectedItem") + "'); return false;}"
            + " else {"
            //+ " var count = getSelectedAndLocked('" + GridViewCliendId + "','check','LockedImage');"
            + " var countAttached = getSelectedAndLocked('" + GridViewCliendId + "','check','SzereltImage');"
            + " var countFitted = getSelectedAndLocked('" + GridViewCliendId + "','check','CsatoltImage');"
            + " if (countFitted.locked == 0 && countAttached.locked == 0) { } else if (confirm('Fel�lvizsg�lat \\n \\n Figyelem! A fel�lvizsg�lt �gyirathoz szerelve/csatolva van m�sik �gyirat. Folytatja?')) { } else return false;"
            + "}";

            return javascript;
            /*string javascript = "";
            javascript =
                "var count = getSelectedCheckBoxesCount('" +
                GridViewCliendId + "','check'); " +
                " if (count==0) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem")
                + "'); return false;} ";
            return javascript;*/
        }
        #endregion 

        public static string SetOnClientClickLockConfirm(string GridViewClientId)
        {
            string javascript = "";
            javascript =
                "var count = getSelectedAndLocked('" +
                GridViewClientId + "','check','LockedImage');"
                + " if (count.selected==0) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem")
                + "'); return false;} "
                + "else if (!(count.selected > count.locked)) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoLockableRecord") + "'); return false;} "
                + "else if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Lock") + " \\n \\n"
                + Contentum.eUtility.Resources.Question.GetString("UILockQuestion")
                + "\\n("
                + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount")
                + " '+ count.selected +((count.locked > 0) ? '\\n" + Contentum.eUtility.Resources.List.GetString("UI_LockedItemsCount")
                    + " '+ count.locked : '') + ')\\n"
                + "')) {  } else return false;";
            return javascript;
        }

        public static string SetOnClientClickLockConfirm(string GridViewClientId, string modosithatoCheckboxID)
        {
            string javascript = "";
            javascript =
                "var count = getSelectedAndLockedAndNotModifiable('" +
                GridViewClientId + "','check','" + modosithatoCheckboxID + "','LockedImage');"
                + " if (count.selected==0) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem")
                + "'); return false;} "
                + "else if (!(count.selected > count.locked) || !(count.selected > count.notModifiable) || !(count.selected > count.notModifiable + count.locked)) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoLockableRecord") + "'); return false;} "
                + "if (count.notModifiable>0) { if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Lock") + " \\n \\n"
                + Contentum.eUtility.Resources.List.GetString("UI_NotExecutable") + "\\n"
                + "(" + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount")
                + " '+ count.selected +((count.locked > 0) ? '\\n" + Contentum.eUtility.Resources.List.GetString("UI_LockedItemsCount")
                    + " '+ count.locked : '') + ')\\n"
                 + "(" + Contentum.eUtility.Resources.List.GetString("UI_NotExecutableItemsCount")
                 + " '+ count.notModifiable+')\\n\\n"
                + Contentum.eUtility.Resources.Question.GetString("UILockQuestion") //("UI_ContinueQuestion")
                + "\\n ' )) { } else {return false;} }"
                + "else if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Lock") + " \\n \\n"
                + Contentum.eUtility.Resources.Question.GetString("UILockQuestion")
                + "\\n("
                + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount")
                + " '+ count.selected +((count.locked > 0) ? '\\n" + Contentum.eUtility.Resources.List.GetString("UI_LockedItemsCount")
                    + " '+ count.locked : '') + ')\\n"
                + "')) {  } else return false;";
            return javascript;
        }

        public static string SetOnClientClickLockConfirm()
        {
            string javascript = "";
            javascript =
                "var count = 1; " +
                " if (count==0) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem")
                + "'); return false;} "
                + "else if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Lock") + " \\n \\n"
                + Contentum.eUtility.Resources.Question.GetString("UILockQuestion")
                + "\\n("
                + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount")
                + " '+count+')\\n"
                + "')) {  } else return false;";
            return javascript;
        }

        public static string SetOnClientClickUnlockConfirm(string GridViewClientId)
        {
            string javascript = "";
            javascript =
                "var count = getSelectedAndLocked('" +
                GridViewClientId + "','check','LockedImage');"
                + " if (count.selected==0) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem")
                + "'); return false;} "
                + "else if (count.locked == 0) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoUnlockableRecord") + "'); return false;} "
                + "else if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Unlock") + " \\n \\n"
                + Contentum.eUtility.Resources.Question.GetString("UIUnlockQuestion")
                + "\\n("
                + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount")
                + " '+ count.selected + ((count.selected > count.locked) ? '\\n" + Contentum.eUtility.Resources.List.GetString("UI_NotLockedItemsCount")
                + " '+ (count.selected - count.locked) : '') + ')\\n"
                + "')) {  } else return false;";
            return javascript;
        }

        public static string SetOnClientClickUnlockConfirm()
        {
            string javascript = "";
            javascript =
                "var count = 1; " +
                " if (count==0) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem")
                + "'); return false;} "
                + "else if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Unlock") + " \\n \\n"
                + Contentum.eUtility.Resources.Question.GetString("UIUnlockQuestion")
                + "\\n("
                + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount")
                + " '+count+')\\n"
                + "')) {  } else return false;";
            return javascript;
        }

        public static string SetOnClientClickLovListSelectItem(string ListBoxClientId)
        {
            string javascript = "if (document.forms[0]." + ListBoxClientId + ".value == '') "
                + " { alert('" + Contentum.eUtility.Resources.Error.GetString("UINoSelectedItem") + "'); return false; } ";
            return javascript;
        }

        public static string SetOnClientClick_Check_HiddenfieldIsNotEmpty(string HiddenfieldClientId)
        {
            string javascript = "if ($get('" + HiddenfieldClientId + "').value == '') "
                + " { alert('" + Contentum.eUtility.Resources.Error.GetString("UINincsMegadvaKuldemeny") + "'); return false; } ";
            return javascript;
        }

        /// <summary>
        /// A returnValue lesz a JavaScript visszat�r�si �rt�ke azon esetekben, amikor nem minden rekordon, vagy egyen sem hajthat� v�gre egy m�velet.
        /// Ha true, tov�bbi feldolgoz�s (pl. sorok megjel�l�se) lehets�ges postback ut�n.
        /// False-t ad vissza t�l sok kijel�lt rekordn�l, vagy kijel�l�s hi�ny�ban, illetve ha a felhaszn�l�
        /// a m�gse gombot v�lasztja a Confirmn�l
        /// </summary>
        /// <param name="gridViewClientID"></param>
        /// <param name="selectCheckboxID"></param>
        /// <param name="executableCheckboxID"></param>
        /// <param name="checkedIsExecutable"></param>
        /// <param name="iMaxTetelSzam"></param>
        /// <param name="headerText"></param>
        /// <param name="questionText"></param>
        /// <param name="returnValue"></param>
        /// <returns></returns>
        public static string SetOnClientClickOperationAllowedConfirm(string gridViewClientID, string selectCheckboxID, string executableCheckboxID, bool checkedIsExecutable, int iMaxTetelSzam, string headerText, string questionText, bool returnValue)
        {
            string maxTetelszam = iMaxTetelSzam.ToString();
            string strReturnValue = (returnValue == true ? "true" : "false");

            StringBuilder sb = new StringBuilder();

            // vigy�zat, AppendFormat eset�n a { �s } helyett {{ �s }} haszn�land�, Appendben viszont csak { �s }

            sb.AppendFormat(" var count = getSelectedAndNotModifiable('{0}','{1}','{2}');", gridViewClientID, selectCheckboxID, executableCheckboxID);
            sb.AppendFormat("if (count.selected>0) {{if(!Boolean.parse('{0}')) count.notModifiable = count.selected - count.notModifiable;", checkedIsExecutable.ToString());
            sb.Append("if( count.notModifiable>0) {");
            sb.Append("if (count.selected == count.notModifiable) {");
            sb.AppendFormat("alert('{0}'); return {1} ;}}", Contentum.eUtility.Resources.List.GetString("UI_NoExecutableItem"), strReturnValue);
            sb.Append("else {");
            sb.AppendFormat("if ((count.selected-count.notModifiable)>{0}) {{alert('{1}{0}\\n{2}' + (count.selected-count.notModifiable)); return false; }} ", maxTetelszam
                , Contentum.eUtility.Resources.List.GetString("UI_MaximumSelectedItemNumber"), Contentum.eUtility.Resources.List.GetString("UI_CurrentSelectedItemNumber"));
            sb.AppendFormat("else if (confirm('{0} \\n \\n{1}\\n({2} '+ count.selected +')\\n", headerText
                , Contentum.eUtility.Resources.List.GetString("UI_NotExecutable"), Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount"));
            sb.AppendFormat("({0} '+ count.notModifiable+')\\n\\n{1}\\n ' )) {{ }}else return false; }}}}"
                , Contentum.eUtility.Resources.List.GetString("UI_NotExecutableItemsCount"), Contentum.eUtility.Resources.Question.GetString("UI_ContinueQuestion"));
            sb.AppendFormat("else {{ if (confirm('{0} \\n \\n{1}\\n({2} '+ count.selected +')\\n' )) {{ }}else return false; }}}}"
                , headerText, questionText, Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount"));
            sb.AppendFormat("else {{alert('{0}'); return false;}}", Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem"));

            return sb.ToString();
        }

        public static string SetOnClientClickOperationAllowedConfirm(string gridViewClientID, string selectCheckboxID, string executableCheckboxID, bool checkedIsExecutable, int iMaxTetelSzam, string headerText, string questionText)
        {
            //string maxTetelszam = iMaxTetelSzam.ToString();

            //string javascript = "";

            //javascript =
            //    " var count = getSelectedAndNotModifiable('"
            //    + gridViewClientID + "','" + selectCheckboxID + "','" + executableCheckboxID + "');" +
            //    "if (count.selected>0) {" + "if(!Boolean.parse('" + checkedIsExecutable.ToString() + "')) count.notModifiable = count.selected - count.notModifiable;" +
            //    "if( count.notModifiable>0) {" +
            //    "if (count.selected == count.notModifiable) {" +
            //    "alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoExecutableItem") +
            //    "'); return false;}" +
            //    "else {"
            //    + "if ((count.selected-count.notModifiable)>" + maxTetelszam + ") {alert('" + Contentum.eUtility.Resources.List.GetString("UI_MaximumSelectedItemNumber")
            //    + maxTetelszam + "\\n" + Contentum.eUtility.Resources.List.GetString("UI_CurrentSelectedItemNumber") + "' + (count.selected-count.notModifiable)); return false; } " +
            //    "else if (confirm('" + headerText + " \\n \\n" +
            //    Contentum.eUtility.Resources.List.GetString("UI_NotExecutable") + "\\n" +
            //    "(" + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount") +
            //    " '+ count.selected +')\\n" +
            //    "(" + Contentum.eUtility.Resources.List.GetString("UI_NotExecutableItemsCount") +
            //    " '+ count.notModifiable+')\\n\\n" +
            //    Contentum.eUtility.Resources.Question.GetString("UI_ContinueQuestion") +
            //    "\\n ' )) { }" +
            //    "else return false; }}" +
            //    "else {" +
            //    " if (confirm('" + headerText + " \\n \\n" +
            //    questionText +
            //    "\\n(" + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount") +
            //    " '+ count.selected +')\\n' )) { }" +
            //    "else return false; }" +
            //    "}" +
            //    "else {" +
            //    "alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem") +
            //    "'); return false;}";

            //return javascript;

            return SetOnClientClickOperationAllowedConfirm(gridViewClientID, selectCheckboxID, executableCheckboxID, checkedIsExecutable, iMaxTetelSzam, headerText, questionText, false);
        }

        public static string SetOnClientClickOperationAllowedConfirm(string gridViewClientID, string selectCheckboxID, string executableCheckboxID, bool checkedIsExecutable, string headerText, string questionText)
        {
            string javascript = "";

            javascript =
                " var count = getSelectedAndNotModifiable('"
                + gridViewClientID + "','" + selectCheckboxID + "','" + executableCheckboxID + "');" +
                "if (count.selected>0) {" + "if(!Boolean.parse('" + checkedIsExecutable.ToString() + "')) count.notModifiable = count.selected - count.notModifiable;" +
                "if( count.notModifiable>0) {" +
                "if (count.selected == count.notModifiable) {" +
                "alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoExecutableItem") +
                "'); return false;}" +
                "else {" +
                "if (confirm('" + headerText + " \\n \\n" +
                Contentum.eUtility.Resources.List.GetString("UI_NotExecutable") + "\\n" +
                "(" + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount") +
                " '+ count.selected +')\\n" +
                "(" + Contentum.eUtility.Resources.List.GetString("UI_NotExecutableItemsCount") +
                " '+ count.notModifiable+')\\n\\n" +
                Contentum.eUtility.Resources.Question.GetString("UI_ContinueQuestion") +
                "\\n ' )) { }" +
                "else return false; }}" +
                "else {" +
                " if (confirm('" + headerText + " \\n \\n" +
                questionText +
                "\\n(" + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount") +
                " '+ count.selected +')\\n' )) { }" +
                "else return false; }" +
                "}" +
                "else {" +
                "alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem") +
                "'); return false;}";

            return javascript;
        }

        public static string SetOnClientClickDeletableConfirm(string gridViewClientID, string selectCheckboxID, string modosthatoCheckboxID, bool checkedModifiabel)
        {
            string javascript = "";

            javascript =
                " var count = getSelectedAndNotModifiable('"
                + gridViewClientID + "','" + selectCheckboxID + "','" + modosthatoCheckboxID + "');" +
                "if (count.selected>0) {" + "if(!Boolean.parse('" + checkedModifiabel.ToString() + "')) count.notModifiable = count.selected - count.notModifiable;" +
                "if( count.notModifiable>0) {" +
                "if (count.selected == count.notModifiable) {" +
                "alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoExecutableItem") +
                "'); return false;}" +
                "else {" +
                "if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Delete") + " \\n \\n" +
                Contentum.eUtility.Resources.List.GetString("UI_NotExecutable") + "\\n" +
                "(" + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount") +
                " '+ count.selected +')\\n" +
                "(" + Contentum.eUtility.Resources.List.GetString("UI_NotModifiableItemsCount") +
                " '+ count.notModifiable+')\\n\\n" +
                Contentum.eUtility.Resources.Question.GetString("UI_ContinueQuestion") +
                "\\n ' )) { }" +
                "else return false; }}" +
                "else {" +
                " if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Delete") + " \\n \\n" +
                Contentum.eUtility.Resources.Question.GetString("UIisDelete") +
                "\\n(" + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount") +
                " '+ count.selected +')\\n' )) { }" +
                "else return false; }" +
                "}" +
                "else {" +
                "alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem") +
                "'); return false;}";

            return javascript;
        }

        public static string SetOnClientClickDeletableConfirm(string gridViewClientID, string selectCheckboxID, string modosthatoCheckboxID)
        {
            return SetOnClientClickDeletableConfirm(gridViewClientID, selectCheckboxID, modosthatoCheckboxID, true);
        }

        public static string SetOnClientClickDeletableConfirm(string gridViewClientID, string modosthatoCheckboxID)
        {
            return SetOnClientClickDeletableConfirm(gridViewClientID, "check", modosthatoCheckboxID);
        }

        //        public static void RegisterActiveTabChangedClientScript(Page ParentPage)
        //        {
        //            if (ParentPage == null) return;

        //            string js = @"<script language='JavaScript' type='text/javascript'>function ActiveTabChanged(sender, e)
        //                            {
        //                              var activeTab = sender.get_activeTab().get_tabIndex();
        //                              __doPostBack(sender.get_id(), activeTab);                          
        //                            } </script>";

        //            ParentPage.ClientScript.RegisterClientScriptBlock(ParentPage.GetType(), "ActiveTabChanged", js);
        //        }

        public static void RegisterCloseWindowClientScript(Page ParentPage, int returnValue)
        {
            if (ParentPage == null) return;

            ParentPage.ClientScript.RegisterStartupScript(ParentPage.GetType(), "WindowClose", "window.returnValue=" + returnValue + "; window.close();", true);
        }

        public static string SetOnClientClickWithActivePanelSet(string PageUrl, string queryString, int Width, int Height, string PostBackControlClientName, string PostBackArgument, string TabContainerClientName)
        {
            string javascript = "";
            string pref = "";

            if (!String.IsNullOrEmpty(queryString))
            {
                pref = "?";
                if (!String.IsNullOrEmpty(TabContainerClientName))
                {
                    queryString += "&TabContainer=" + TabContainerClientName;
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(TabContainerClientName))
                {
                    pref = "?";
                    queryString = "TabContainer=" + TabContainerClientName;
                }
            }

            javascript = "var returnValue = popup('" + PageUrl + pref + queryString + "'," + Width.ToString() + " , " + Height.ToString() + ");var tabContainer = $find('" + TabContainerClientName + "');" +
                "if(!isNaN(parseInt(returnValue)) && tabContainer && returnValue >=0 && returnValue < tabContainer.get_tabs().length) tabContainer.set_activeTabIndex(returnValue);if(!isNaN(parseInt(returnValue)) || returnValue!=false) __doPostBack('" + PostBackControlClientName + "','" + PostBackArgument + "');";

            return javascript;
        }

        public static void RegisterOnValidatorOverClientScript(Page ParentPage)
        {
            if (ParentPage == null) return;

            if (!ParentPage.ClientScript.IsClientScriptBlockRegistered("OnValidatorOver"))
            {

                string js = @"
                            function OnValidatorOver(e)
                             {
                                if(AjaxControlToolkit.ValidatorCalloutBehavior._currentCallout)
                                AjaxControlToolkit.ValidatorCalloutBehavior._currentCallout.hide();
                             }";

                js += @"
                      function ValidatorOverScript(target)
                        {
                                 
                            if(arguments.length > 0 && target)
                            {
                                try
                                {            
                                    $removeHandler(target,'mouseover',OnValidatorOver);
                                }
                                catch(ex)
                                {}
                                var id = target.id;
                                var index = id.indexOf('_popupTable');
                                if(index> -1)
                                {
                                    var calloutId = id.substring(0,index);
                                    var callout = $find(calloutId);
                                    if(callout)
                                    {
                                        callout.cleanPopupTable = target;
                                    }
                                }
                                $addHandler(target,'mouseover',OnValidatorOver);
                            }
                        }";

                ParentPage.ClientScript.RegisterClientScriptBlock(ParentPage.GetType(), "OnValidatorOver", js, true);
            }
        }

        public static void SetAutoCompleteContextKey(Page page, System.Web.UI.WebControls.TextBox ParentTextBox, System.Web.UI.WebControls.TextBox CurrentTextBox, string AutoCompleteClientID)
        {
            if (page == null || ParentTextBox == null || CurrentTextBox == null || String.IsNullOrEmpty(AutoCompleteClientID))
                return;

            string felh_id = Contentum.eUtility.FelhasznaloProfil.FelhasznaloId(page);

            string name = CurrentTextBox.Parent.ID + "_OnFocus";

            string js = "function " + name + "(sender,args){ var ExtenderBehavior = $find('" + AutoCompleteClientID + "'); var text = $get('" + ParentTextBox.ClientID + "');" +
                        "if(ExtenderBehavior && text){ExtenderBehavior.set_contextKey('" + felh_id + ";'+ text.value);};};" +
                        "if($get('" + CurrentTextBox.ClientID + "')) $addHandler($get('" + CurrentTextBox.ClientID + "'),'focus'," + name + ");";


            ScriptManager.RegisterStartupScript(page, page.GetType(), name, js, true);
        }

        public static void SetTextWithFirstResult(System.Web.UI.WebControls.TextBox ParentTextBox, System.Web.UI.WebControls.TextBox ChildTextBox, string ChildAutoCompleteClientID)
        {
            if (ParentTextBox == null || ChildTextBox == null || String.IsNullOrEmpty(ChildAutoCompleteClientID))
                return;

            string js = "function CancelShowing(sender,eventArgs){sender.remove_showing(CancelShowing);eventArgs.set_cancel(true);}" +
                        "function OnPopulated(sender,eventArgs){sender.remove_populated(OnPopulated);sender.add_showing(CancelShowing);var state = sender.state;" +
                        "sender.set_minimumPrefixLength(state.prefixLength); sender.set_completionSetCount(state.completionCount);sender.set_completionInterval(state.completionInterval);" +
                        "sender._setText(sender.get_completionList().firstChild);}" +
                        "var telepules = $get('" + ParentTextBox.ClientID + "');" +
                        "if (telepules.value.length > 0 && this.value.length == 0) {var extender = $find('" + ChildAutoCompleteClientID + "');if(extender){" +
                        "var state = new Object(); state.prefixLength = extender.get_minimumPrefixLength(); state.completionCount = extender.get_completionSetCount();" +
                        "state.completionInterval = extender.get_completionInterval(); extender.state = state; extender.add_populated(OnPopulated); " +
                        "extender.set_minimumPrefixLength(0); extender.set_completionSetCount(1);extender.set_completionInterval(0);" +
                        "}}";

            ChildTextBox.Attributes.Add("onfocus", js);
        }

        public static void SetCompletionListszIndex(Page page)
        {
            if (page == null)
                return;

            string js;
            js = @"Sys.Application.add_load(SetAutoCompletes);
                 function SetAutoCompletes()
                 {
                    var elements = Sys.Application.getComponents();
                    for(i in elements)
                    {
                        var element = elements[i];
                        if(Object.getType(element).getName() == 'AjaxControlToolkit.AutoCompleteBehavior')
                        {
                            element.get_completionList().style.zIndex = 1000;
                        }
                    }
                }";

            page.ClientScript.RegisterStartupScript(page.GetType(), "SetAutoCompleteszIndex", js, true);

        }

        public static void FixAccordionPane(Page page, string AccordionPaneClientID)
        {
            if (page == null || String.IsNullOrEmpty(AccordionPaneClientID))
                return;

            string scriptName = "Fix_" + AccordionPaneClientID;

            string js;
            js = "Sys.Application.add_load(" + scriptName + ");" +
                  "function " + scriptName + "(){" +
                  "var accordion = $find('" + AccordionPaneClientID + "_AccordionExtender');" +
                 @"if(!accordion) return;
                   var selectedPane = accordion.get_Pane(accordion.get_SelectedIndex());
                   for(i in accordion._panes)
                   {
                     var pane = accordion.get_Pane(i);
                     if(pane !== selectedPane)
                     {
                        pane.header.className = accordion.get_HeaderCssClass();
                     }
                   }" +
                   "}";

            page.ClientScript.RegisterStartupScript(page.GetType(), scriptName, js, true);

        }

        public static string SetOnClientClickSendObjectsConfirm(string GridViewClientId)
        {
            string javascript = "";
            javascript =
                "var count = getSelectedCheckBoxesCount('" +
                GridViewClientId + "','check'); " +
                " if (count==0) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem")
                + "'); return false;} ";
            return javascript;
        }

        public static string SetOnClientClickSendObjectsConfirm(string gridViewClientId, string selectCheckboxID, string modosithatoCheckboxID)
        {
            string javascript = "";
            javascript =
                " var count = getSelectedAndNotModifiable('"
                + gridViewClientId + "','" + selectCheckboxID + "','" + modosithatoCheckboxID + "');" +
                "if (count.selected>0) {" +
                "if( count.notModifiable>0) {" +
                "if (count.selected == count.notModifiable) {" +
                "alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoExecutableItem") +
                "'); return false;}" +
                "else {" +
                "if (confirm('" +
                Contentum.eUtility.Resources.List.GetString("UI_NotExecutable") + "\\n" +
                "(" + Contentum.eUtility.Resources.List.GetString("UI_SelectedItemsCount") +
                " '+ count.selected +')\\n" +
                "(" + Contentum.eUtility.Resources.List.GetString("UI_NotExecutableItemsCount") +
                " '+ count.notModifiable+')\\n\\n" +
                Contentum.eUtility.Resources.Question.GetString("UI_ContinueQuestion") +
                "\\n ' )) { }" +
                "else {return false;} }}}" +
                "else {" +
                "alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem") +
                "'); return false;}";
            return javascript;
        }

        public static void SendBackResultIdsListToCallingWindow(Page parentPage, String RecordsId, String Message)
        {
            if (parentPage == null || RecordsId == null) return;

            string hiddenFieldId = parentPage.Request.QueryString.Get(QueryStringVars.HiddenFieldId);
            string MessageHiddenFieldId = parentPage.Request.QueryString.Get(QueryStringVars.MessageHiddenFieldId);
            if (!String.IsNullOrEmpty(hiddenFieldId))
            {

                String script = "";
                script += " if (window.dialogArguments) { ";
                script += " window.dialogArguments.parentWindow.document.getElementById('" + hiddenFieldId + "').value = '" + RecordsId + "'; \n";
                script += " window.dialogArguments.parentWindow.document.getElementById('" + MessageHiddenFieldId + "').value = '" + Message + "'; \n";
                script += " } else { "
                    + " window.opener.document.getElementById('" + hiddenFieldId + "').value = '" + RecordsId + "'; \n"
                    + " window.opener.document.getElementById('" + MessageHiddenFieldId + "').value = '" + Message + "'; \n"
                    + " } window.returnValue=true;";

                parentPage.ClientScript.RegisterStartupScript(parentPage.GetType(), "ReturnValuesToParentWindow"
                    , script, true);

            }
        }

        public static void RegisterSelectedRecordIdToParent(Page page, string selectedRecordId)
        {
            if (page == null || selectedRecordId == null)
                return;

            string script = "";
            script += " if (window.dialogArguments) { ";
            script += "var parent = window.dialogArguments.parentWindow; if(parent && parent.event && parent.event.srcElement && parent.event.srcElement.id && parent.event.srcElement.id != '')";
            script += "{var srcElement = parent.event.srcElement.id;";
            script += "var lastIndex = srcElement.lastIndexOf('_'); var componentId = srcElement.substring(0,lastIndex);";
            script += "componentId += '_selectedRecordId'; var component = parent.$get(componentId);";
            script += " if(component)component.value = '" + selectedRecordId + "'; \n";
            script += " }} else { ";
            script += "var parent = window.opener; if(parent && parent.savedEvent) {var target = parent.savedEvent.target.id;";
            script += "var lastIndex = target.lastIndexOf('_'); var componentId = target.substring(0,lastIndex);";
            script += "componentId += '_selectedRecordId'; var component = parent.$get(componentId);";
            script += "if(component)component.value = '" + selectedRecordId + "';\n";
            script += " };}";

            ScriptManager.RegisterStartupScript(page, page.GetType(), "ReturnSelectedRecotdIdToParentWindow"
                , script, true);
        }

        public static void RegisterScrollManagerScript(Page page)
        {
            ScriptManager sm = ScriptManager.GetCurrent(page);
            if (sm == null)
            {
                throw new Exception("ScriptManager not found.");
            }
            ScriptReference sr = new ScriptReference();
            sr.Path = "~/JavaScripts/Scrolling.js";
            sm.Scripts.Add(sr);
            page.ClientScript.RegisterStartupScript(page.GetType(), "ScrollManagerScript", "new Utility.ScrollManager();", true);
        }

        public static void ResetScroll(Page page, AjaxControlToolkit.CollapsiblePanelExtender collapsibleExtender)
        {
            ScriptManager sm = ScriptManager.GetCurrent(page);
            if (sm == null)
            {
                throw new Exception("ScriptManager not found.");
            }
            if (sm.IsInAsyncPostBack)
            {
                try
                {
                    sm.RegisterDataItem(collapsibleExtender, true.ToString());
                }
                catch (ArgumentException)
                {
                }
            }
        }

        public static void RegisterGetValuesFromParentWindowClientScript(Page page, Dictionary<string, string> ParentChildControlPairs, string postBackControlClientId, string postBackArgument)
        {
            if (page == null || ParentChildControlPairs == null || postBackControlClientId == null || postBackArgument == null) return;
            string script = "";
            foreach (KeyValuePair<string, string> kvp in ParentChildControlPairs)
            {
                string parentControlId = kvp.Key;
                string childControlId = kvp.Value;
                if (!String.IsNullOrEmpty(parentControlId))
                {
                    script += " if (window.dialogArguments) { ";
                    script += "$get('" + childControlId + "').value = window.dialogArguments.parentWindow.document.getElementById('" + parentControlId + "').value; \n";
                    script += " } else { "
                        + "$get('" + childControlId + "').value = window.opener.document.getElementById('" + parentControlId + "').value;\n"
                        + " };";
                }
            }
            script += "__doPostBack('" + postBackControlClientId + "','" + postBackArgument + "');";
            page.ClientScript.RegisterStartupScript(page.GetType(), "GetValuesFromParentWindow"
                , script, true);
        }

        public static string SetDisableButtonOnClientClick(Page page, System.Web.UI.WebControls.ImageButton button)
        {
            if (page == null || button == null) return String.Empty;

            string js = "if(typeof(Page_ClientValidate) != 'undefined') Page_ClientValidate('" + button.ValidationGroup + "'); if(typeof(Page_ClientValidate) == 'undefined' || Page_IsValid){this.disabled = true;this.className = 'disableditem';"
                        + page.ClientScript.GetPostBackEventReference(new PostBackOptions(button)) + ";};return false;";

            return js;
        }

        public static string SetDisableButtonOnClientClick(Page page, System.Web.UI.WebControls.ImageButton button, List<System.Web.UI.WebControls.ImageButton> disabledButtonList)
        {
            if (page == null || disabledButtonList == null || disabledButtonList.Count == 0) return String.Empty;

            string jsDisable = "";

            foreach (System.Web.UI.WebControls.ImageButton _button in disabledButtonList)
            {
                jsDisable += "if($get('" + _button.ClientID + "')){$get('" + _button.ClientID + "').disabled = true;" +
                             "$get('" + _button.ClientID + "').className = 'disableditem';}";

            }

            string js = "if(typeof(Page_ClientValidate) != 'undefined') Page_ClientValidate('" + button.ValidationGroup + "'); if(typeof(Page_ClientValidate) == 'undefined' || Page_IsValid){" + jsDisable
                        + page.ClientScript.GetPostBackEventReference(new PostBackOptions(button)) + ";};return false;";

            return js;
        }

        public static void RegisterEmailFeedbackMessage(Control control, bool IsSuccess)
        {
            string js;
            if (IsSuccess)
            {
                js = "alert('" + Resources.Error.GetString("UI_EmailSuccess") + "');";
            }
            else
            {
                js = "alert('" + Resources.Error.GetString("UI_EmailNotSuccess") + "');";
            }

            ScriptManager.RegisterStartupScript(control, control.GetType(), "EmailFeedbackAlert", js, true);
        }


        #region SetLovListGridViewRowScripts f�ggv�nyei


        public static void SetLovListGridViewRowScripts(GridViewRow row, HiddenField GridViewSelectedIndex, HiddenField GridViewSelectedId)
        {
            SetLovListGridViewRowScripts(row, false, GridViewSelectedIndex, GridViewSelectedId);
        }

        private static void SetLovListGridViewRowScripts(GridViewRow row, bool disabledRow, HiddenField GridViewSelectedIndex, HiddenField GridViewSelectedId)
        {
            const string selectedRowStyle = "GridViewLovListSelectedRowStyle";
            const string disabledRowStyle = "GridViewLovListDisabledRowStyle";

            if (!disabledRow)
            {
                row.Attributes.Add("onmouseover", "this.oldClass = this.className;" + " this.className = 'EntryLineHover';");

                row.Attributes.Add("onmouseout", "if(this.className != '" + selectedRowStyle + "') this.className = this.oldClass;");

                string js = "if(this.className != '" + selectedRowStyle + "'){ var rowIndex = $get('" + GridViewSelectedIndex.ClientID + "'); if(rowIndex) rowIndex.value = " + row.RowIndex.ToString() + ";" +
                " var selectedId = $get('" + GridViewSelectedId.ClientID + "');if(selectedId) selectedId.value = '" + row.Cells[0].Text + "';" +
                " if(window.selectedRow && window.oldClass) window.selectedRow.className = window.oldClass; window.selectedRow = this; if(this.oldClass) window.oldClass = this.oldClass; this.className = '" + selectedRowStyle + "';}";

                row.Attributes.Add("onclick", js);
            }
            else
            {
                row.CssClass = disabledRowStyle;
            }
        }


        public static void SetLovListGridViewRowScripts_disableIfValueIsInList(GridViewRow row, HiddenField GridViewSelectedIndex, HiddenField GridViewSelectedId
           , string filterLabelId, List<string> disabledValueList)
        {
            bool disabledRow = false;

            if (!String.IsNullOrEmpty(filterLabelId) && disabledValueList != null && disabledValueList.Count > 0)
            {
                Label filterLabel = row.FindControl(filterLabelId) as Label;
                if (filterLabel != null)
                {
                    // ha a Label �rt�ke benne van a list�ban, disabled sor lesz
                    if (disabledValueList.Contains(filterLabel.Text))
                    {
                        disabledRow = true;
                    }
                }
            }

            SetLovListGridViewRowScripts(row, disabledRow, GridViewSelectedIndex, GridViewSelectedId);
        }


        /// <summary>
        /// A megadott Labelek �rt�kei k�z�l valamelyik nem �res --> kisz�rk�tj�k
        /// </summary>
        public static void SetLovListGridViewRowScripts_disableIfOneIsNotEmpty(GridViewRow row, HiddenField GridViewSelectedIndex, HiddenField GridViewSelectedId
            , List<string> filterLabelIdList)
        {
            bool disabledRow = false;

            if (filterLabelIdList != null && filterLabelIdList.Count > 0)
            {
                foreach (string filterLabelId in filterLabelIdList)
                {
                    Label filterLabel = row.FindControl(filterLabelId) as Label;
                    if (filterLabel != null)
                    {
                        // ha a Label �rt�ke nem �res, disabled sor lesz
                        if (!String.IsNullOrEmpty(filterLabel.Text))
                        {
                            disabledRow = true;
                            // nem kell tov�bb n�zni a list�t:
                            break;
                        }
                    }
                }
            }

            SetLovListGridViewRowScripts(row, disabledRow, GridViewSelectedIndex, GridViewSelectedId);
        }

        #endregion


        public static string SetOnClientClickNoSelectedRow(HiddenField SelectedIndex)
        {
            string js = "if ($get('" + SelectedIndex.ClientID + "').value.trim() == '') "
               + " { alert('" + Contentum.eUtility.Resources.Error.GetString("UINoSelectedRow") + "'); return false; } ";

            return js;
        }

        public static void SetFocus(Control control)
        {
            SetFocus(control, 100);
        }

        public static void SetFocus(Control control, int timeOut)
        {
            string js = "var fControl = $get('" + control.ClientID + "');"
                     + "if(fControl) window.setTimeout(\"fControl.focus()\"," + timeOut.ToString() + ");";

            ScriptManager.RegisterStartupScript(control, control.GetType(), "SetFocus", js, true);
        }

        public static void SetFocusOnEnd(Control control)
        {
            string js = "window.setTimeout(\"SetFocusOnEnd()\",100);";

            js += @"function SetFocusOnEnd(){
                    var fControl = $get('" + control.ClientID + @"');
                    if(fControl)
                    { try
                      {
                         fControl.focus();
                         Utility.Selection.textboxSelect(fControl,fControl.value.length);
                      } 
                      catch(err){}
                    }
                    }";

            ScriptManager.RegisterStartupScript(control, control.GetType(), "SetFocus", js, true);
        }

        public static string SetOnClientClickWithTimeout(string PageUrl, string queryString, int Width, int Height, string PostBackControlClientName, string PostBackArgument)
        {
            return SetOnClientClickWithTimeout(PageUrl, queryString, Width, Height, PostBackControlClientName, PostBackArgument, 10);
        }

        public static string SetOnClientClickWithTimeout(string PageUrl, string queryString, int Width, int Height, string PostBackControlClientName, string PostBackArgument, int TimeOut)
        {
            return SetOnClientClickWithTimeout(PageUrl, queryString, Width, Height, PostBackControlClientName, PostBackArgument, TimeOut, false);
        }

        public static string SetOnClientClickWithTimeout(string PageUrl, string queryString, int Width, int Height, string PostBackControlClientName, string PostBackArgument, int TimeOut, bool noDialog)
        {
            string javascript = "";
            string pref = "";
            string strEndRequest = " return false;";
            if (!String.IsNullOrEmpty(queryString))
                pref = "?";
            javascript = "if (typeof(event) != 'undefined' && event) { window.savedEvent = event; } window.postBackArgument = '" + PostBackArgument + "'; window.postBackControl = '" + PostBackControlClientName + "';" +
                       "if (popup('" + PageUrl + pref + queryString + "'," + Width.ToString() + "  , " + Height.ToString() + " , " + noDialog.ToString().ToLower() + ")) { window.setTimeout(\"__doPostBack('" + PostBackControlClientName + "','" + PostBackArgument + "')\"," + TimeOut.ToString() + ");}" + strEndRequest;
            return javascript;
        }

        public static void SetAutoCompleteContextKeyByWatermarkedTextBox(Page page, System.Web.UI.WebControls.TextBox ParentTextBox, System.Web.UI.WebControls.TextBox CurrentTextBox, string AutoCompleteClientID)
        {
            if (page == null || ParentTextBox == null || CurrentTextBox == null || String.IsNullOrEmpty(AutoCompleteClientID))
                return;

            string felh_id = Contentum.eUtility.FelhasznaloProfil.FelhasznaloId(page);

            string name = CurrentTextBox.Parent.ID + "_OnFocus";

            string js = "function " + name + "(sender,args){ var ExtenderBehavior = $find('" + AutoCompleteClientID + "'); var text = $get('" + ParentTextBox.ClientID + "');" +
                        "if(ExtenderBehavior && text){ExtenderBehavior.set_contextKey('" + felh_id + ";'+ AjaxControlToolkit.TextBoxWrapper.get_Wrapper(text).get_Value());};};" +
                        "if($get('" + CurrentTextBox.ClientID + "')) $addHandler($get('" + CurrentTextBox.ClientID + "'),'focus'," + name + ");";


            ScriptManager.RegisterStartupScript(page, page.GetType(), name, js, true);
        }

        public static void RegisterAjaxLoggingScript(Page page)
        {
            ScriptManager sm = ScriptManager.GetCurrent(page);
            if (sm == null)
            {
                throw new Exception("ScriptManager not found.");
            }
            ScriptReference sr = new ScriptReference();
            sr.Path = "~/JavaScripts/AjaxLogging.js";
            if (!sm.Scripts.Contains(sr))
                sm.Scripts.Add(sr);
        }

        public static void RegisterCommonScripts(Page page)
        {
            ScriptManager sm = ScriptManager.GetCurrent(page);
            if (sm == null)
            {
                throw new Exception("ScriptManager not found.");
            }
            ScriptReference sr = new ScriptReference();
            sr.Path = "~/JavaScripts/Common.js";
            if (!sm.Scripts.Contains(sr))
                sm.Scripts.Add(sr);
        }

        //Oldal �t�r�ny�t�sa kliens oldali javascript k�ddal
        public static string GetRedirectPageScript(string Url)
        {
            if (String.IsNullOrEmpty(Url))
                return String.Empty;

            string js = "if(typeof(dialogArguments) == 'undefined'){ window.location.assign('" + Url + "');}";
            js += @"else {if(window.name==''){window.name = 'winDialog';};
                          var oLink = document.createElement('A');document.body.insertAdjacentElement('beforeEnd', oLink);
                          with(oLink){ href = '" + Url + "'; target = window.name; click();}}";

            return js;
        }


        public static void RegisterParentWindowCallbackFunction(Page page, string ParentWindowFunctionName)
        {
            RegisterParentWindowCallbackFunction(page, ParentWindowFunctionName, "ParentWindowCallbackFunction");
        }

        public static void RegisterParentWindowCallbackFunction(Page page, string ParentWindowFunctionName, string scriptId)
        {
            string js = @"var parentWindow = Utility.Popup.getParentWindow();
                if(parentWindow && typeof(parentWindow." + ParentWindowFunctionName + @") == 'function')
                  {parentWindow." + ParentWindowFunctionName + @"();};";

            ScriptManager.RegisterStartupScript(page, page.GetType(), scriptId, js, true);
        }

        public static void SetOnClientEnterPressed(WebControl webControl, string javascriptCode)
        {
            string jsOnKeyDown = @"
                var k = event.keyCode ? event.keyCode : event.rawEvent.keyCode;
                if (k === Sys.UI.Key.enter)
                {
                    " + javascriptCode + @"
                    return false;
                }";

            webControl.Attributes.Add("onkeydown", jsOnKeyDown);
        }

        public static string SetOnClientClickIsSelectedRow(string GridViewClientID)
        {
            string js = "var count = getSelectedCheckBoxesCount('" + GridViewClientID + "','check');"
                      + "if (count==0) {alert('" + Resources.List.GetString("UI_NoSelectedItem") + "'); return false;}";

            return js;
        }

        #region Date Control JavaScripts
        /// <summary>
        /// A kezd�d�tum nem lehet a v�ge d�tum ut�ni, m�sk�pp figyelmeztet�s
        /// </summary>
        /// <param name="ccIntervallumKezdClientID">A kezd�d�tumot tartalmaz� TextBox ClientID</param>
        /// <param name="ccIntervallumVegeClientID">A v�ge d�tumot tartalmaz� TextBox ClientID</param>
        /// <returns>javascript</returns>
        public static string SetCheckDateIntervallJavaScript(string ccIntervallumKezdClientID, string ccIntervallumVegeClientID)
        {
            string js = "";
            if (!String.IsNullOrEmpty(ccIntervallumKezdClientID) && !String.IsNullOrEmpty(ccIntervallumVegeClientID))
            {
                js = "var ccKezd = $get('" + ccIntervallumKezdClientID + @"');
                  var ccVege = $get('" + ccIntervallumVegeClientID + @"');
                   if (ccKezd && ccVege)
                   {
                         var yearKezd  = parseInt(ccKezd.value.substring(0,4), 10);
                         var monthKezd = parseInt(ccKezd.value.substring(5,7), 10);
                         var dayKezd   = parseInt(ccKezd.value.substring(8,10), 10);
                         var ccKezdDate = new Date(yearKezd, monthKezd-1, dayKezd);

                         var yearVege  = parseInt(ccVege.value.substring(0,4), 10);
                         var monthVege = parseInt(ccVege.value.substring(5,7), 10);
                         var dayVege   = parseInt(ccVege.value.substring(8,10), 10);
                         var ccVegeDate = new Date(yearVege, monthVege-1, dayVege);

                         if (ccKezdDate > ccVegeDate)
                         {
                           alert('" + Resources.Error.GetString("UISpecStartDateAfterEndDate") + @"');
                           return false;
                         }
                   }
                 ";
            }

            return js;
        }

        /// <summary>
        /// A d�tum nem lehet m�ltbeli, m�sk�pp figyelmeztet�s
        /// </summary>
        /// <param name="ccDateControlClientID">A d�tumot tartalmaz� TextBox ClientID</param>
        /// <param name="fieldName">A figyelmeztet� sz�vegbe illesztend�, a textboxhoz kapcsol�d� mez�n�v</param>
        /// <returns>javascript</returns>
        public static string SetDateIsInThePastJavaScript(string ccDateControlClientID, string fieldName)
        {
            string js = "";
            if (!String.IsNullOrEmpty(ccDateControlClientID))
            {
                js = "var ccDateControl = $get('" + ccDateControlClientID + @"');
                   if (ccDateControl)
                   {
                         var yearDate  = parseInt(ccDateControl.value.substring(0,4), 10);
                         var monthDate = parseInt(ccDateControl.value.substring(5,7), 10);
                         var dayDate   = parseInt(ccDateControl.value.substring(8,10), 10);
                         var ccDate = new Date(yearDate, monthDate-1, dayDate);

                         var currentTime = new Date();
                         var yearToday  = currentTime.getFullYear();
                         var monthToday = currentTime.getMonth();
                         var dayToday   = currentTime.getDate();
                         var today = new Date(yearToday, monthToday, dayToday);

                         if (ccDate < today)
                         {
                           alert('" + UI.GetTimeIsLesserThenNowMessage(fieldName) + @"');
                           return false;
                         }
                   }
                 ";
            }

            return js;
        }
        #endregion Date Control JavaScripts

        public static string SetOnClientClickErrorDetailsAlert(Page page, int errorCode, ErrorDetails errorDetails)
        {
            string errorMessage = ResultError.GetErrorMessageByErrorCode(errorCode);
            return SetOnClientClickErrorDetailsAlert(page, errorMessage, errorDetails);
        }

        public static string SetOnClientClickErrorDetailsAlert(Page page, string errorMessage, ErrorDetails errorDetails)
        {
            string javascript = "";
            javascript = "alert('" + errorMessage + ResultError.GetErrorDetailMsgForAlert(errorDetails, page) + "'); return false;";
            return javascript;
        }

        public static string SetOnClientClickConfirm(string confirmHeader, string confirmQuestion)
        {
            string javascript = "";
            javascript = "if (confirm('" + confirmHeader + " \\n \\n"
                + confirmQuestion
                + "')) {  } else return false;";
            return javascript;
        }


        public static void RegisterSetLinkOnForm(Page page, bool isVisible, String NavigateUrl, HyperLink link)
        {
            // az url f�ggv�nyt is tartalmazhat, amit csak a h�v�skor szabad ki�rt�kelni, ez�rt az onclick-re tessz�k
            // (k�zvetlen�l be�rva a href-be azonnal �s egyszeri alkalommal ki�rt�kel�dne a fv., �s az eredm�ny ker�lne a linkbe)
            string js_url = @"var link = $get('" + link.ClientID + @"'); if (link) { link.href = '" + NavigateUrl + @"'; }";
            link.Attributes.Add("onclick", js_url);

            // a l�that�s�got azonnal be kell �ll�tani, amikor az oldal megjelenik
            string js_visibility = @"var link = $get('" + link.ClientID + @"'); if (link) { link.style.display = '" + (isVisible ? "inline" : "none") + @"'; }";
            ScriptManager.RegisterStartupScript(page, page.GetType(), "SetLinkOnForm", js_visibility, true);

        }

        public static void RegisterGetActiveTabClientScript(Page page)
        {
            string js = @"function getActiveTab(tabcontainer_id) {
    var tabcontainer = $find(tabcontainer_id);
    if (tabcontainer) {
        var owner_id = tabcontainer.get_activeTab().get_owner().get_id();
        var tab_id = tabcontainer.get_activeTab().get_id();
        return tab_id.substring(owner_id.length + 1);
    }
    else {
        return '';
    }
}";
            ScriptManager.RegisterStartupScript(page, page.GetType(), "GetActiveTabClientScript", js, true);
        }

        public static void RegisterRefreshOpenerClientScript(Page page)
        {
            string script =
                @"if (window.dialogArguments && window.dialogArguments.parentWindow.__doPostBack) {
                    window.dialogArguments.parentWindow.__doPostBack('', window.dialogArguments.parentWindow.postBackArgument);
                } else if (window.opener && window.opener.__doPostBack) {
                    window.opener.__doPostBack('', window.opener.postBackArgument);
                }";
            ScriptManager.RegisterStartupScript(page, page.GetType(), "RefreshOpenerClientScript", script, true);
        }

        public static string GetOnCloseClientClickScript()
        {
            string result = @"
            window.returnValue=true;
            if(window.opener){
                 window.opener.__doPostBack(window.opener.postBackControl, window.opener.postBackArgument);            
            }else{
                  window.__doPostBack(window.postBackControl, window.postBackArgument);   
            }
             window.close();
             return false;";
            return result;
        }
    }
}
