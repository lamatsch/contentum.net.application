﻿using Contentum.eUtility.KRX.KULDEMENY_META;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Contentum.eUtility.KRX
{
    public class KRXFileManager
    {
        class DirectoryStrucure
        {
            public DirectoryInfo outputDirectory { get; private set; }
            public DirectoryInfo tempDirectory { get; private set; }
            public DirectoryInfo workDirectory { get; private set; }
            public DirectoryInfo KRX { get; private set; }
            public DirectoryInfo OCD { get; private set; }
            public DirectoryInfo Metalayer { get; private set; }
            public DirectoryInfo Payload { get; private set; }

            public DirectoryStrucure(string outputDirectoryPath)
            {
                outputDirectory = new DirectoryInfo(outputDirectoryPath);
                tempDirectory = new DirectoryInfo(Path.Combine(outputDirectory.FullName, Guid.NewGuid().ToString()));
                workDirectory = new DirectoryInfo(Path.Combine(tempDirectory.FullName, "work"));
                KRX = new DirectoryInfo(Path.Combine(workDirectory.FullName, "KRX"));
                OCD = new DirectoryInfo(Path.Combine(KRX.FullName, "OCD"));
                Metalayer = new DirectoryInfo(Path.Combine(OCD.FullName, "Metalayer"));
                Payload = new DirectoryInfo(Path.Combine(OCD.FullName, "Payload"));
            }

            public void CreateDirectories()
            {
                if (!outputDirectory.Exists)
                    throw new Exception(String.Format("A könyvtár nem létezik: {0}", outputDirectory));

                CreateDirectory(workDirectory);
                CreateDirectory(KRX);
                CreateDirectory(OCD);
                CreateDirectory(Metalayer);
                CreateDirectory(Payload);
            }

            public DirectoryInfo CreateMellekletDirectory(int index)
            {
                string mellekletDirectoryName = String.Format("{0}{1}", mellekletDirectoryPrefix, index);
                DirectoryInfo mellekletDirectory = new DirectoryInfo(Path.Combine(Payload.FullName, mellekletDirectoryName));
                CreateDirectory(mellekletDirectory);
                return mellekletDirectory;
            }

            public string GetRelativePath(string fullPath)
            {
                Uri baseUri = new Uri(KRX.FullName);
                Uri uri = new Uri(fullPath);
                Uri relativeUri = baseUri.MakeRelativeUri(uri);
                return relativeUri.ToString();
            }

            public string GetFullPath(string relativePath)
            {
                Uri baseUri = new Uri(KRX.FullName);
                Uri uri = new Uri(baseUri, relativePath);
                return uri.LocalPath;
            }

            void CreateDirectory(DirectoryInfo di)
            {
                Logger.Debug(String.Format("CreateDirectory: {0}", di.FullName));
                di.Create();
            }
        }

        DirectoryStrucure Dirs;
        readonly Encoding DefaultEncoding = new UTF8Encoding(false);
        const string mimeTypeFileName = "mimetype";
        const string mimeTypeFileContent = "application/OCD+ZIP";
        KULDEMENY Kuldemeny;
        const string metaFileName = "KULDEMENY_META.xml";
        List<Contentum.eBusinessDocuments.Csatolmany> Csatolmanyok;
        const string mellekletDirectoryPrefix = "ID-";
        Contentum.eBusinessDocuments.Csatolmany metaFile;

        public KRXFileManager()
        {
        }

        public byte[] CreateKRXFile(KULDEMENY kuldemeny, List<Contentum.eBusinessDocuments.Csatolmany> csatolmanyok, List<Contentum.eBusinessDocuments.Csatolmany> metaFiles = null)
        {
            Logger.Info("KRXFileManager.CreateKRXFile kezdete");

            this.Dirs = new DirectoryStrucure(Path.GetTempPath());
            this.Kuldemeny = kuldemeny;
            this.Csatolmanyok = csatolmanyok;

            Dirs.CreateDirectories();

            CreateMimeType();

            CreateMellekletek();

            CreateKuldemenyMetaFile();

            if (metaFiles != null)
            {
                AddMetaFiles(metaFiles);
            }

            byte[] krxContent = CreateZip();

            Logger.Debug(String.Format("KRXFileManager.DeleteDirectory: {0}", Dirs.tempDirectory.FullName));

            Dirs.tempDirectory.Delete(true);

            Logger.Info("KRXFileManager.CreateKRXFile vege");

            return krxContent;
        }

        byte[] CreateZip()
        {
            Logger.Debug("KRXFileManager.CreateZip");
            string zipFileName = String.Format("{0}.krx", Path.GetFileNameWithoutExtension(Path.GetRandomFileName()));
            string zipFilePath = Path.Combine(Dirs.tempDirectory.FullName, zipFileName);
            FastZip zip = new FastZip();
            Logger.Debug(String.Format("zipFileName={0},sourecDirectory={1}", zipFilePath, Dirs.workDirectory.FullName));
            zip.CreateZip(zipFilePath, Dirs.workDirectory.FullName, true, null);
            Logger.Debug(String.Format("ReadAllBytes: {0}", zipFilePath));
            return File.ReadAllBytes(zipFilePath);
        }

        void CreateMimeType()
        {
            Logger.Debug("KRXFileManager.CreateMimeType");
            string mimeTypeFilePath = Path.Combine(Dirs.KRX.FullName, mimeTypeFileName);
            Logger.Debug(String.Format("WriteAllText: {0}", mimeTypeFilePath));
            File.WriteAllText(mimeTypeFilePath, mimeTypeFileContent, DefaultEncoding);
        }

        void CreateMellekletek()
        {
            Logger.Debug("KRXFileManager.CreateMellekletek");

            List<KULDEMENYEXPEDIALASOKEXPEDIALASMELLEKLET> mellekletek = new List<KULDEMENYEXPEDIALASOKEXPEDIALASMELLEKLET>();

            int index = 1;

            foreach (Contentum.eBusinessDocuments.Csatolmany csatolmany in this.Csatolmanyok)
            {
                DirectoryInfo mellekletDirectory = Dirs.CreateMellekletDirectory(index);
                string mellekletFilePath = Path.Combine(mellekletDirectory.FullName, csatolmany.Nev);
                Logger.Debug(String.Format("WriteAllBytes: {0}", mellekletFilePath));
                File.WriteAllBytes(mellekletFilePath, csatolmany.Tartalom);

                KULDEMENYEXPEDIALASOKEXPEDIALASMELLEKLET melleklet = new KULDEMENYEXPEDIALASOKEXPEDIALASMELLEKLET();
                melleklet.MELLEKLET_LEIRASA = "Csatolmány";
                melleklet.CSATOLMANY_SZAMA = index;
                melleklet.FAJL_NEV = csatolmany.Nev;
                melleklet.MERET = Math.Round((double)csatolmany.Tartalom.Length / 1024);
                melleklet.ELHELYEZKEDES = Dirs.GetRelativePath(mellekletDirectory.FullName);


                mellekletek.Add(melleklet);

                index++;
            }


            this.Kuldemeny.EXPEDIALASOK.EXPEDIALAS.MELLEKLETEK = mellekletek.ToArray();
            this.Kuldemeny.EXPEDIALASOK.EXPEDIALAS.MELLEKLETEK_SZAMA = mellekletek.Count;
        }

        void CreateKuldemenyMetaFile()
        {
            Logger.Debug("KRXFileManager.CreateKuldemenyMetaFile");

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("ns2", "http://xsd.orfk.hu/rzs/ker/kuldemeny");
            XmlSerializer serializer = new XmlSerializer(typeof(KULDEMENY));

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = DefaultEncoding;
            settings.Indent = true;

            string filePath = Path.Combine(Dirs.Metalayer.FullName, metaFileName);
            Logger.Debug(String.Format("XmlWriter.Create: {0}", filePath));
            using (XmlWriter writer = XmlWriter.Create(filePath, settings))
            {
                serializer.Serialize(writer, this.Kuldemeny, ns);
            }
        }

        void AddMetaFiles(List<Contentum.eBusinessDocuments.Csatolmany> metaFiles)
        {
            Logger.Debug("KRXFileManager.AddMetaFiles");

            foreach (Contentum.eBusinessDocuments.Csatolmany metaFile in metaFiles)
            {
                string filePath = Path.Combine(Dirs.Metalayer.FullName, metaFile.Nev);
                Logger.Debug(String.Format("WriteAllBytes: {0}", filePath));
                File.WriteAllBytes(filePath, metaFile.Tartalom);
            }
        }

        public List<Contentum.eBusinessDocuments.Csatolmany> ProcessKRXFile(Contentum.eBusinessDocuments.Csatolmany krxFile)
        {
            Logger.Info("KRXFileManager.ProcessKRXFile kezdete");

            List<Contentum.eBusinessDocuments.Csatolmany> csatolmanyok = new List<Contentum.eBusinessDocuments.Csatolmany>();

            this.Dirs = new DirectoryStrucure(Path.GetTempPath());
            Dirs.CreateDirectories();

            string krFilePath = SaveFile(this.Dirs.tempDirectory.FullName, krxFile.Nev, krxFile.Tartalom);

            Unzip(krFilePath);

            LoadMetaFile();

            csatolmanyok.Add(metaFile);

            csatolmanyok.AddRange(GetMellekletek());

            this.Dirs.tempDirectory.Delete(true);

            Logger.Info("KRXFileManager.ProcessKRXFile vege");

            return csatolmanyok;
        }

        string SaveFile(string path, string fileName, byte[] content)
        {
            string filePath = Path.Combine(path, fileName);
            Logger.Debug(String.Format("File.WritwAllBytes: {0}", filePath));
            File.WriteAllBytes(filePath, content);
            return filePath;
        }


        void Unzip(string filePath)
        {
            Logger.Debug(String.Format("Unzip: {0}", filePath));
            FastZip zip = new FastZip();
            Logger.Debug(String.Format("zipFileName={0},targetDirectory={1}", filePath, Dirs.workDirectory.FullName));
            zip.ExtractZip(filePath, this.Dirs.workDirectory.FullName, null);
        }

        void LoadMetaFile()
        {
            Logger.Debug("KRXFileManager.LoadMetaFile");

            Contentum.eBusinessDocuments.Csatolmany metaFile = new Contentum.eBusinessDocuments.Csatolmany();
            metaFile.Nev = metaFileName;
            metaFile.Tartalom = ReadFile(Dirs.Metalayer.FullName, metaFileName);
            this.metaFile = metaFile;


            LoadMetaAdatok();
        }

        void LoadMetaAdatok()
        {
            Logger.Debug("KRXFileManager.LoadMetaAdatok");

            XmlSerializer serializer = new XmlSerializer(typeof(KULDEMENY));

            MemoryStream ms = new MemoryStream(this.metaFile.Tartalom);
            using (XmlReader reader = XmlReader.Create(ms))
            {
                object o = serializer.Deserialize(reader);
                this.Kuldemeny = o as KULDEMENY;
            }
        }

        List<Contentum.eBusinessDocuments.Csatolmany> GetMellekletek()
        {
            Logger.Debug("KRXFileManager.GetMellekletek");

            List<Contentum.eBusinessDocuments.Csatolmany> csatolmanyok = new List<Contentum.eBusinessDocuments.Csatolmany>();

            foreach (KULDEMENYEXPEDIALASOKEXPEDIALASMELLEKLET melleklet in this.Kuldemeny.EXPEDIALASOK.EXPEDIALAS.MELLEKLETEK)
            {
                string filePath = Path.Combine(this.Dirs.GetFullPath(melleklet.ELHELYEZKEDES), melleklet.FAJL_NEV);

                if (File.Exists(filePath))
                {
                    Contentum.eBusinessDocuments.Csatolmany csatolmany = new Contentum.eBusinessDocuments.Csatolmany();
                    csatolmany.Nev = melleklet.FAJL_NEV;
                    csatolmany.Tartalom = ReadFile(this.Dirs.GetFullPath(melleklet.ELHELYEZKEDES), melleklet.FAJL_NEV);

                    csatolmanyok.Add(csatolmany);
                }
                else
                {
                    Logger.Error(String.Format("A melléklet nem található: {0}", filePath));
                }
            }

            return csatolmanyok;
        }

        byte[] ReadFile(string path, string fileName)
        {
            string filePath = Path.Combine(path, fileName);
            Logger.Debug(String.Format("File.ReadAllBytes: {0}", filePath));
            return File.ReadAllBytes(filePath);
        }

        public static KULDEMENY GetMetaAdatok(byte[] fileContent)
        {
            Logger.Debug("KRXFileManager.GetMetaAdatok");

            XmlSerializer serializer = new XmlSerializer(typeof(KULDEMENY));

            MemoryStream ms = new MemoryStream(fileContent);
            using (XmlReader reader = XmlReader.Create(ms))
            {
                object o = serializer.Deserialize(reader);
                return o as KULDEMENY;
            }
        }
    }
}
