﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eUtility.KRX.KULDEMENY_META
{
     /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://xsd.orfk.hu/rzs/ker/kuldemeny")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://xsd.orfk.hu/rzs/ker/kuldemeny", IsNullable = false)]
    public partial class KULDEMENY
    {

        private KULDEMENYFEJRESZ fEJRESZField;

        private KULDEMENYEXPEDIALASOK eXPEDIALASOKField;

        /// <remarks/>
        public KULDEMENYFEJRESZ FEJRESZ
        {
            get
            {
                return this.fEJRESZField;
            }
            set
            {
                this.fEJRESZField = value;
            }
        }

        /// <remarks/>
        public KULDEMENYEXPEDIALASOK EXPEDIALASOK
        {
            get
            {
                return this.eXPEDIALASOKField;
            }
            set
            {
                this.eXPEDIALASOKField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://xsd.orfk.hu/rzs/ker/kuldemeny")]
    public partial class KULDEMENYFEJRESZ
    {

        private string kRX_VERZIOSZAMField;

        private string fORRASRENDSZER_AZONOSITOField;

        private string kULDEMENY_AZONOSITOField;

        private System.DateTime kULDEMENY_LETREHOZASANAK_IDEJEField;

        private string kULDEMENY_TIPUSField;

        private bool tESZTField;

        /// <remarks/>
        public string KRX_VERZIOSZAM
        {
            get
            {
                return this.kRX_VERZIOSZAMField;
            }
            set
            {
                this.kRX_VERZIOSZAMField = value;
            }
        }

        /// <remarks/>
        public string FORRASRENDSZER_AZONOSITO
        {
            get
            {
                return this.fORRASRENDSZER_AZONOSITOField;
            }
            set
            {
                this.fORRASRENDSZER_AZONOSITOField = value;
            }
        }

        /// <remarks/>
        public string KULDEMENY_AZONOSITO
        {
            get
            {
                return this.kULDEMENY_AZONOSITOField;
            }
            set
            {
                this.kULDEMENY_AZONOSITOField = value;
            }
        }

        /// <remarks/>
        public System.DateTime KULDEMENY_LETREHOZASANAK_IDEJE
        {
            get
            {
                return this.kULDEMENY_LETREHOZASANAK_IDEJEField;
            }
            set
            {
                this.kULDEMENY_LETREHOZASANAK_IDEJEField = value;
            }
        }

        /// <remarks/>
        public string KULDEMENY_TIPUS
        {
            get
            {
                return this.kULDEMENY_TIPUSField;
            }
            set
            {
                this.kULDEMENY_TIPUSField = value;
            }
        }

        /// <remarks/>
        public bool TESZT
        {
            get
            {
                return this.tESZTField;
            }
            set
            {
                this.tESZTField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://xsd.orfk.hu/rzs/ker/kuldemeny")]
    public partial class KULDEMENYEXPEDIALASOK
    {

        private KULDEMENYEXPEDIALASOKEXPEDIALAS eXPEDIALASField;

        /// <remarks/>
        public KULDEMENYEXPEDIALASOKEXPEDIALAS EXPEDIALAS
        {
            get
            {
                return this.eXPEDIALASField;
            }
            set
            {
                this.eXPEDIALASField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://xsd.orfk.hu/rzs/ker/kuldemeny")]
    public partial class KULDEMENYEXPEDIALASOKEXPEDIALAS
    {

        private string eXPEDIALASI_AZONOSITOField;

        private string kEZBESITES_MODJAField;

        private string kEZBESITES_PRIORITASAField;

        private string cIMZETT_NEVField;

        private string cIMZETT_CIMField;

        private KULDEMENYEXPEDIALASOKEXPEDIALASCIMZETT cIMZETTField;

        private string kULDO_NEVField;

        private string kULDO_CIMField;

        private KULDEMENYEXPEDIALASOKEXPEDIALASKULDO kULDOField;

        private string kULDO_TELEFONSZAMField;

        private string hIVATKOZASI_SZAMField;

        private KULDEMENYEXPEDIALASOKEXPEDIALASUGYKEZELES uGYKEZELESField;

        private string tARGYField;

        private System.DateTime eXPEDIALAS_DATUMField;

        private int mELLEKLETEK_SZAMAField;

        private KULDEMENYEXPEDIALASOKEXPEDIALASMELLEKLET[] mELLEKLETEKField;

        private bool tERTIVEVENYESField;

        /// <remarks/>
        public string EXPEDIALASI_AZONOSITO
        {
            get
            {
                return this.eXPEDIALASI_AZONOSITOField;
            }
            set
            {
                this.eXPEDIALASI_AZONOSITOField = value;
            }
        }

        /// <remarks/>
        public string KEZBESITES_MODJA
        {
            get
            {
                return this.kEZBESITES_MODJAField;
            }
            set
            {
                this.kEZBESITES_MODJAField = value;
            }
        }

        /// <remarks/>
        public string KEZBESITES_PRIORITASA
        {
            get
            {
                return this.kEZBESITES_PRIORITASAField;
            }
            set
            {
                this.kEZBESITES_PRIORITASAField = value;
            }
        }

        /// <remarks/>
        public string CIMZETT_NEV
        {
            get
            {
                return this.cIMZETT_NEVField;
            }
            set
            {
                this.cIMZETT_NEVField = value;
            }
        }

        /// <remarks/>
        public string CIMZETT_CIM
        {
            get
            {
                return this.cIMZETT_CIMField;
            }
            set
            {
                this.cIMZETT_CIMField = value;
            }
        }

        /// <remarks/>
        public KULDEMENYEXPEDIALASOKEXPEDIALASCIMZETT CIMZETT
        {
            get
            {
                return this.cIMZETTField;
            }
            set
            {
                this.cIMZETTField = value;
            }
        }

        /// <remarks/>
        public string KULDO_NEV
        {
            get
            {
                return this.kULDO_NEVField;
            }
            set
            {
                this.kULDO_NEVField = value;
            }
        }

        /// <remarks/>
        public string KULDO_CIM
        {
            get
            {
                return this.kULDO_CIMField;
            }
            set
            {
                this.kULDO_CIMField = value;
            }
        }

        /// <remarks/>
        public KULDEMENYEXPEDIALASOKEXPEDIALASKULDO KULDO
        {
            get
            {
                return this.kULDOField;
            }
            set
            {
                this.kULDOField = value;
            }
        }

        /// <remarks/>
        public string KULDO_TELEFONSZAM
        {
            get
            {
                return this.kULDO_TELEFONSZAMField;
            }
            set
            {
                this.kULDO_TELEFONSZAMField = value;
            }
        }

        /// <remarks/>
        public string HIVATKOZASI_SZAM
        {
            get
            {
                return this.hIVATKOZASI_SZAMField;
            }
            set
            {
                this.hIVATKOZASI_SZAMField = value;
            }
        }

        /// <remarks/>
        public KULDEMENYEXPEDIALASOKEXPEDIALASUGYKEZELES UGYKEZELES
        {
            get
            {
                return this.uGYKEZELESField;
            }
            set
            {
                this.uGYKEZELESField = value;
            }
        }

        /// <remarks/>
        public string TARGY
        {
            get
            {
                return this.tARGYField;
            }
            set
            {
                this.tARGYField = value;
            }
        }

        /// <remarks/>
        public System.DateTime EXPEDIALAS_DATUM
        {
            get
            {
                return this.eXPEDIALAS_DATUMField;
            }
            set
            {
                this.eXPEDIALAS_DATUMField = value;
            }
        }

        /// <remarks/>
        public int MELLEKLETEK_SZAMA
        {
            get
            {
                return this.mELLEKLETEK_SZAMAField;
            }
            set
            {
                this.mELLEKLETEK_SZAMAField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("MELLEKLET", IsNullable = false)]
        public KULDEMENYEXPEDIALASOKEXPEDIALASMELLEKLET[] MELLEKLETEK
        {
            get
            {
                return this.mELLEKLETEKField;
            }
            set
            {
                this.mELLEKLETEKField = value;
            }
        }

        /// <remarks/>
        public bool TERTIVEVENYES
        {
            get
            {
                return this.tERTIVEVENYESField;
            }
            set
            {
                this.tERTIVEVENYESField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://xsd.orfk.hu/rzs/ker/kuldemeny")]
    public partial class KULDEMENYEXPEDIALASOKEXPEDIALASCIMZETT
    {

        private KULDEMENYEXPEDIALASOKEXPEDIALASCIMZETTPARTNER pARTNERField;

        /// <remarks/>
        public KULDEMENYEXPEDIALASOKEXPEDIALASCIMZETTPARTNER PARTNER
        {
            get
            {
                return this.pARTNERField;
            }
            set
            {
                this.pARTNERField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://xsd.orfk.hu/rzs/ker/kuldemeny")]
    public partial class KULDEMENYEXPEDIALASOKEXPEDIALASCIMZETTPARTNER
    {

        private string kER_AZONField;

        private string tELJES_NEVField;

        private string oRSZAGKODField;

        private string iRANYITOSZAMField;

        private string tELEPULESField;

        private string kOZTERULET_NEVField;

        private string kOZTERULET_JELLEGEField;

        private string hAZSZAMField;

        /// <remarks/>
        public string KER_AZON
        {
            get
            {
                return this.kER_AZONField;
            }
            set
            {
                this.kER_AZONField = value;
            }
        }

        /// <remarks/>
        public string TELJES_NEV
        {
            get
            {
                return this.tELJES_NEVField;
            }
            set
            {
                this.tELJES_NEVField = value;
            }
        }

        /// <remarks/>
        public string ORSZAGKOD
        {
            get
            {
                return this.oRSZAGKODField;
            }
            set
            {
                this.oRSZAGKODField = value;
            }
        }

        /// <remarks/>
        public string IRANYITOSZAM
        {
            get
            {
                return this.iRANYITOSZAMField;
            }
            set
            {
                this.iRANYITOSZAMField = value;
            }
        }

        /// <remarks/>
        public string TELEPULES
        {
            get
            {
                return this.tELEPULESField;
            }
            set
            {
                this.tELEPULESField = value;
            }
        }

        /// <remarks/>
        public string KOZTERULET_NEV
        {
            get
            {
                return this.kOZTERULET_NEVField;
            }
            set
            {
                this.kOZTERULET_NEVField = value;
            }
        }

        /// <remarks/>
        public string KOZTERULET_JELLEGE
        {
            get
            {
                return this.kOZTERULET_JELLEGEField;
            }
            set
            {
                this.kOZTERULET_JELLEGEField = value;
            }
        }

        /// <remarks/>
        public string HAZSZAM
        {
            get
            {
                return this.hAZSZAMField;
            }
            set
            {
                this.hAZSZAMField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://xsd.orfk.hu/rzs/ker/kuldemeny")]
    public partial class KULDEMENYEXPEDIALASOKEXPEDIALASKULDO
    {

        private KULDEMENYEXPEDIALASOKEXPEDIALASKULDOPARTNER pARTNERField;

        /// <remarks/>
        public KULDEMENYEXPEDIALASOKEXPEDIALASKULDOPARTNER PARTNER
        {
            get
            {
                return this.pARTNERField;
            }
            set
            {
                this.pARTNERField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://xsd.orfk.hu/rzs/ker/kuldemeny")]
    public partial class KULDEMENYEXPEDIALASOKEXPEDIALASKULDOPARTNER
    {

        private string kER_AZONField;

        private string tELJES_NEVField;

        private string oRSZAGKODField;

        private string iRANYITOSZAMField;

        private string tELEPULESField;

        private string kOZTERULET_NEVField;

        private string kOZTERULET_JELLEGEField;

        private string hAZSZAMField;

        /// <remarks/>
        public string KER_AZON
        {
            get
            {
                return this.kER_AZONField;
            }
            set
            {
                this.kER_AZONField = value;
            }
        }

        /// <remarks/>
        public string TELJES_NEV
        {
            get
            {
                return this.tELJES_NEVField;
            }
            set
            {
                this.tELJES_NEVField = value;
            }
        }

        /// <remarks/>
        public string ORSZAGKOD
        {
            get
            {
                return this.oRSZAGKODField;
            }
            set
            {
                this.oRSZAGKODField = value;
            }
        }

        /// <remarks/>
        public string IRANYITOSZAM
        {
            get
            {
                return this.iRANYITOSZAMField;
            }
            set
            {
                this.iRANYITOSZAMField = value;
            }
        }

        /// <remarks/>
        public string TELEPULES
        {
            get
            {
                return this.tELEPULESField;
            }
            set
            {
                this.tELEPULESField = value;
            }
        }

        /// <remarks/>
        public string KOZTERULET_NEV
        {
            get
            {
                return this.kOZTERULET_NEVField;
            }
            set
            {
                this.kOZTERULET_NEVField = value;
            }
        }

        /// <remarks/>
        public string KOZTERULET_JELLEGE
        {
            get
            {
                return this.kOZTERULET_JELLEGEField;
            }
            set
            {
                this.kOZTERULET_JELLEGEField = value;
            }
        }

        /// <remarks/>
        public string HAZSZAM
        {
            get
            {
                return this.hAZSZAMField;
            }
            set
            {
                this.hAZSZAMField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://xsd.orfk.hu/rzs/ker/kuldemeny")]
    public partial class KULDEMENYEXPEDIALASOKEXPEDIALASUGYKEZELES
    {

        private string iKTATOSZAMField;

        /// <remarks/>
        public string IKTATOSZAM
        {
            get
            {
                return this.iKTATOSZAMField;
            }
            set
            {
                this.iKTATOSZAMField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://xsd.orfk.hu/rzs/ker/kuldemeny")]
    public partial class KULDEMENYEXPEDIALASOKEXPEDIALASMELLEKLET
    {

        private string mELLEKLET_LEIRASAField;

        private int cSATOLMANY_SZAMAField;

        private string fAJL_NEVField;

        private double mERETField;

        private string eLHELYEZKEDESField;

        /// <remarks/>
        public string MELLEKLET_LEIRASA
        {
            get
            {
                return this.mELLEKLET_LEIRASAField;
            }
            set
            {
                this.mELLEKLET_LEIRASAField = value;
            }
        }

        /// <remarks/>
        public int CSATOLMANY_SZAMA
        {
            get
            {
                return this.cSATOLMANY_SZAMAField;
            }
            set
            {
                this.cSATOLMANY_SZAMAField = value;
            }
        }

        /// <remarks/>
        public string FAJL_NEV
        {
            get
            {
                return this.fAJL_NEVField;
            }
            set
            {
                this.fAJL_NEVField = value;
            }
        }

        /// <remarks/>
        public double MERET
        {
            get
            {
                return this.mERETField;
            }
            set
            {
                this.mERETField = value;
            }
        }

        /// <remarks/>
        public string ELHELYEZKEDES
        {
            get
            {
                return this.eLHELYEZKEDESField;
            }
            set
            {
                this.eLHELYEZKEDESField = value;
            }
        }
    }
}
