﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Contentum.eUtility.KRX
{
    /// <summary>
    /// Summary description for KuldesModjaConverter
    /// </summary>
    public class KuldesModjaConverter
    {
        const string KRX_BEERKEZES_MODJA = "KRX_BEERKEZES_MODJA";
        const string KULDEMENY_KULDES_MODJA = "KULDEMENY_KULDES_MODJA";

        ExecParam execParam;
        System.Web.Caching.Cache cache;

        public KuldesModjaConverter(Page page)
        {
            this.execParam = UI.SetExecParamDefault(page);
            this.cache = page.Cache;
        }

        public KuldesModjaConverter(ExecParam execParam, System.Web.Caching.Cache cache)
        {
            this.execParam = execParam;
            this.cache = cache;
        }

        string GetIdFromNev(string nev)
        {
            var ktList = KodTar_Cache.GetKodtarakByKodCsoportListAll(KRX_BEERKEZES_MODJA, execParam, cache);
            var filterList = ktList.Where(kt => kt.Nev.Equals(nev, StringComparison.CurrentCultureIgnoreCase));

            if (filterList.Count() == 1)
            {
                return filterList.First().Id;
            }

            return String.Empty;
        }

        string GetKodFormId(string id)
        {
            var ktList = KodTar_Cache.GetKodtarakByKodCsoportListAll(KULDEMENY_KULDES_MODJA, execParam, cache);
            var filterList = ktList.Where(kt => kt.Id.Equals(id, StringComparison.CurrentCultureIgnoreCase));

            if (filterList.Count() == 1)
            {
                return filterList.First().Kod;
            }

            return String.Empty;
        }

        string GetFuggoId(string vezerloId)
        {
            Contentum.eAdmin.Service.KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();

            KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
            search.Vezerlo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", KRX_BEERKEZES_MODJA);
            search.Vezerlo_KodCsoport_Id.Operator = Query.Operators.inner;
            search.Fuggo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", KULDEMENY_KULDES_MODJA);
            search.Fuggo_KodCsoport_Id.Operator = Query.Operators.inner;

            Result res = service.GetAll(execParam, search);

            if (!res.IsError)
            {

                if (res.Ds.Tables[0].Rows.Count == 1)
                {
                    string adat = res.Ds.Tables[0].Rows[0]["Adat"].ToString();
                    KodtarFuggosegDataClass fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(adat);

                    List<string> fuggoKodtarak = new List<string>();

                    foreach (KodtarFuggosegDataItemClass item in fuggosegek.Items)
                    {
                        if (item.VezerloKodTarId.Equals(vezerloId, StringComparison.InvariantCultureIgnoreCase))
                        {
                            fuggoKodtarak.Add(item.FuggoKodtarId);
                        }
                    }

                    if (fuggoKodtarak.Count == 1)
                    {
                        return fuggoKodtarak[0];
                    }
                }
            }

            return String.Empty;
        }

        public string GetKod(string input)
        {
            string vezerloId = GetIdFromNev(input);

            if (String.IsNullOrEmpty(vezerloId))
            {
                return String.Empty;
            }

            string fuggoId = GetFuggoId(vezerloId);

            if (String.IsNullOrEmpty(fuggoId))
            {
                return String.Empty;
            }

            return GetKodFormId(fuggoId);
        }
    }
}