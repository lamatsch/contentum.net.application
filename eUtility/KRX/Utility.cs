﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eUtility.KRX
{
    public class KRXUtility
    {
        public static KULDEMENY_META.KULDEMENY CreateKuldemeny()
        {
            Contentum.eUtility.KRX.KULDEMENY_META.KULDEMENY kuldemeny = new Contentum.eUtility.KRX.KULDEMENY_META.KULDEMENY();
            kuldemeny.FEJRESZ = new Contentum.eUtility.KRX.KULDEMENY_META.KULDEMENYFEJRESZ();
            kuldemeny.FEJRESZ.KRX_VERZIOSZAM = "v0.9";
            kuldemeny.FEJRESZ.FORRASRENDSZER_AZONOSITO = "IMAP";
            kuldemeny.FEJRESZ.KULDEMENY_TIPUS = "EXPEDIALAS";
            kuldemeny.FEJRESZ.TESZT = false;

            kuldemeny.EXPEDIALASOK = new Contentum.eUtility.KRX.KULDEMENY_META.KULDEMENYEXPEDIALASOK();
            kuldemeny.EXPEDIALASOK.EXPEDIALAS = new Contentum.eUtility.KRX.KULDEMENY_META.KULDEMENYEXPEDIALASOKEXPEDIALAS();
            kuldemeny.EXPEDIALASOK.EXPEDIALAS.KEZBESITES_MODJA = "hivatali kapu (elektronikus küldés hivatalnak)";

            kuldemeny.EXPEDIALASOK.EXPEDIALAS.UGYKEZELES = new Contentum.eUtility.KRX.KULDEMENY_META.KULDEMENYEXPEDIALASOKEXPEDIALASUGYKEZELES();

            return kuldemeny;
        }
    }
}
