﻿using System;
using System.Data;

namespace Contentum.eAdmin.BaseUtility.KodtarFuggoseg
{
    /// <summary>
    /// Summary description for DataSetHelper
    /// </summary>
    public class DataSetHelper
    {
        public static void LoadUpdateBusinessDocumentFromDataRow(object BusinessDocument, DataRow row)
        {
            try
            {
                if (row != null)
                {
                    System.Reflection.PropertyInfo[] Properties = BusinessDocument.GetType().GetProperties();
                    System.Reflection.FieldInfo UpdatedField = BusinessDocument.GetType().GetField("Updated");
                    object UpdatedObject = UpdatedField.GetValue(BusinessDocument);
                    foreach (System.Reflection.PropertyInfo P in Properties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BusinessDocument, row[P.Name].ToString(), null);
                                System.Reflection.PropertyInfo UpdatedProperty = UpdatedObject.GetType().GetProperty(P.Name);
                                if (UpdatedProperty != null)
                                {
                                    UpdatedProperty.SetValue(UpdatedObject, true, null);
                                }
                            }
                        }
                    }
                    System.Reflection.FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
                    object BaseObject = BaseField.GetValue(BusinessDocument);

                    System.Reflection.PropertyInfo[] BaseProperties = BaseObject.GetType().GetProperties();

                    foreach (System.Reflection.PropertyInfo P in BaseProperties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BaseObject, row[P.Name].ToString(), null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}