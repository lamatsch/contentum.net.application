﻿using Contentum;
using Newtonsoft.Json;
using System;

namespace Contentum.eAdmin.BaseUtility.KodtarFuggoseg
{
    /// <summary>
    /// Summary description for JSONFunctions
    /// </summary>
    public class JSONFunctions
    {
        public static KodtarFuggosegDataClass DeSerialize(string json)
        {
            if (string.IsNullOrEmpty(json))
                return null;
            KodtarFuggosegDataClass result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.DeserializeObject<KodtarFuggosegDataClass>(json, settings);
            }
            catch (Exception)
            {

            }
            return result;
        }

        public static string Serialize(KodtarFuggosegDataClass item)
        {
            if (item == null)
                return null;

            string result = null;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            try
            {
                result = JsonConvert.SerializeObject(item, settings);
            }
            catch (Exception)
            {

            }
            return result;
        }
    }
}