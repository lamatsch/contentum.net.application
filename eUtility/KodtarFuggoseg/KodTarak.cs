﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eUtility.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;

namespace Contentum.eUtility
{
    public class KodTar_Cache
    {
        public class KodTarElem
        {
            private string _Id;

            public string Id
            {
                get { return _Id; }
                set { _Id = value; }
            }

            private string _Kod;

            public string Kod
            {
                get { return _Kod; }
                set { _Kod = value; }
            }

            private string _Nev;

            public string Nev
            {
                get { return _Nev; }
                set { _Nev = value; }
            }

            private string _RovidNev;

            public string RovidNev
            {
                get { return _RovidNev; }
                set { _RovidNev = value; }
            }

            public string Egyeb { get; set; }


            private DateTime? _ErvKezd;

            public DateTime? ErvKezd
            {
                get { return _ErvKezd; }
                set { _ErvKezd = value; }
            }

            private DateTime? _ErvVege;

            public DateTime? ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }
            }

            private string _Sorrend;

            public string Sorrend
            {
                get { return _Sorrend; }
                set { _Sorrend = value; }
            }

            public KodTarElem()
            {
            }

            public KodTarElem(DataRow row)
            {
                this.Id = row["Id"].ToString();
                this.Kod = row["Kod"].ToString();
                this.Nev = row["Nev"].ToString();
                this.RovidNev = row["RovidNev"].ToString();
                this.Egyeb = row["Egyeb"].ToString();
                this.ErvKezd = row["ErvKezd"] as DateTime?;
                this.ErvVege = row["ErvVege"] as DateTime?;
                this.Sorrend = row["Sorrend"] as string;
            }

            public bool IsErvenyes(DateTime Ervenyesseg)
            {
                return (ErvKezd == null || ErvKezd < Ervenyesseg)
                    && (ErvVege == null || ErvVege > Ervenyesseg);
            }
        }
        //Kodtarak lecache-elve az alábbi formában:
        //Dictionary<String Kodcsoport_Kod,Dictionary<String Kodtar_Kod,String Kodtar_Nev>>

        private const String cache_key_kodcsoportok = "KodcsoportokDictionary";

        private static readonly object _sync = new object();

        // hány percenként ürítse a Cache-ből a kódcsoportokat 
        //private const int KodCsoportokCache_RefreshPeriod_Minute = 30;
        public static Dictionary<string, string> GetKodtarakByKodCsoport(String kodcsoport_kod, System.Web.UI.Page page)
        {
            return GetKodtarakByKodCsoport(kodcsoport_kod, page, null);
        }

        public static Dictionary<string, string> GetKodtarakByKodCsoport(String kodcsoport_kod, System.Web.UI.Page page, DateTime? Ervenyesseg)
        {
            ExecParam execParam = UI.SetExecParamDefault(page);
            return GetKodtarakByKodCsoport(kodcsoport_kod, execParam, page.Cache, Ervenyesseg);
        }

        public static Dictionary<string, string> GetKodtarakByKodCsoport(String kodcsoport_kod, ExecParam execParam, System.Web.Caching.Cache cache)
        {
            return GetKodtarakByKodCsoport(kodcsoport_kod, execParam, cache, null);
        }

        public static Dictionary<string, string> GetKodtarakByKodCsoport(String kodcsoport_kod, ExecParam execParam, System.Web.Caching.Cache cache, DateTime? Ervenyesseg)
        {
            if (Ervenyesseg == null) Ervenyesseg = DateTime.Now;

            List<KodTarElem> kodtarakListAll = GetKodtarakByKodCsoportListAll(kodcsoport_kod, execParam, cache);

            if (kodtarakListAll != null)
            {
                Dictionary<string, string> kodtarakDictionary = new Dictionary<string, string>();
                foreach (KodTarElem kte in kodtarakListAll)
                {
                    if (kte.IsErvenyes(Ervenyesseg.Value))
                    {
                        if (!kodtarakDictionary.ContainsKey(kte.Kod))
                        {
                            kodtarakDictionary.Add(kte.Kod, kte.Nev);
                        }
                    }
                }
                return kodtarakDictionary;
            }

            return null;
        }

        public static List<KodTarElem> GetKodtarakByKodCsoportList(String kodcsoport_kod, System.Web.UI.Page page, DateTime? Ervenyesseg)
        {
            ExecParam execParam = UI.SetExecParamDefault(page);
            return GetKodtarakByKodCsoportList(kodcsoport_kod, execParam, page.Cache, Ervenyesseg);
        }

        public static List<KodTarElem> GetKodtarakByKodCsoportList(String kodcsoport_kod, ExecParam execParam, System.Web.Caching.Cache cache, DateTime? Ervenyesseg)
        {
            if (Ervenyesseg == null) Ervenyesseg = DateTime.Now;

            List<KodTarElem> kodtarakListAll = GetKodtarakByKodCsoportListAll(kodcsoport_kod, execParam, cache);

            if (kodtarakListAll != null)
            {
                List<KodTarElem> kodtarakList = new List<KodTarElem>();
                foreach (KodTarElem kte in kodtarakListAll)
                {
                    if (kte.IsErvenyes(Ervenyesseg.Value))
                    {
                        kodtarakList.Add(kte);
                    }
                }
                return kodtarakList;
            }

            return null;
        }

        //LZS - 11346
        public static List<KodTarElem> GetErvenytelenKodtarakByKodCsoportList(String kodcsoport_kod, System.Web.UI.Page page, DateTime? Ervenyesseg)
        {
            if (Ervenyesseg == null) Ervenyesseg = DateTime.Now;

            ExecParam execParam = UI.SetExecParamDefault(page);
            List<KodTarElem> kodtarakListAll = GetKodtarakByKodCsoportListAll(kodcsoport_kod, execParam, page.Cache);

            if (kodtarakListAll != null)
            {
                List<KodTarElem> kodtarakList = new List<KodTarElem>();
                foreach (KodTarElem kte in kodtarakListAll)
                {
                    if (!kte.IsErvenyes(Ervenyesseg.Value))
                    {
                        kodtarakList.Add(kte);
                    }
                }
                return kodtarakList;
            }

            return null;
        }

        public static List<KodTarElem> GetKodtarakByKodCsoportListAll(String kodcsoport_kod, ExecParam execParam, System.Web.Caching.Cache cache)
        {
            OrgSeperatedDictionary<String, List<KodTarElem>> kodcsoport_DictionaryCache = null;

            if (String.IsNullOrEmpty(execParam.Org_Id))
            {
                Logger.Debug("String.IsNullOrEmpty(execParam.Org_Id)");
                string orgId = FelhasznaloNevek_Cache.GetOrgByFelhasznalo(execParam.Felhasznalo_Id, cache);
                Logger.Debug("execParam.Org_Id=" + orgId ?? "NULL");
                execParam.Org_Id = orgId;
            }

            if (cache[cache_key_kodcsoportok] == null)
            {
                int KodCsoportokCache_RefreshPeriod_Minute =
                    Rendszerparameterek.GetInt(execParam, Rendszerparameterek.KodCsoportokCache_RefreshPeriod_Minute);
                if (KodCsoportokCache_RefreshPeriod_Minute == 0)
                {
                    KodCsoportokCache_RefreshPeriod_Minute = 600;
                }
                lock (_sync)
                {
                    if (cache[cache_key_kodcsoportok] == null)
                    {
                        kodcsoport_DictionaryCache = new OrgSeperatedDictionary<string, List<KodTarElem>>();
                        // felvétel a Cache-be:
                        cache.Insert(cache_key_kodcsoportok, kodcsoport_DictionaryCache, null
                            , DateTime.Now.AddMinutes(KodCsoportokCache_RefreshPeriod_Minute), System.Web.Caching.Cache.NoSlidingExpiration
                            , System.Web.Caching.CacheItemPriority.NotRemovable, null);
                    }
                }

            }

            kodcsoport_DictionaryCache = (OrgSeperatedDictionary<String, List<KodTarElem>>)cache[cache_key_kodcsoportok];

            Dictionary<String, List<KodTarElem>> kodcsoport_Dictionary = kodcsoport_DictionaryCache.Get(execParam.Org_Id);

            List<KodTarElem> kodtarakDictionary = null;


            if (kodcsoport_Dictionary.ContainsKey(kodcsoport_kod))
            {
                kodtarakDictionary = kodcsoport_Dictionary[kodcsoport_kod];
                return kodtarakDictionary;
            }
            else
            {
                kodtarakDictionary = new List<KodTarElem>();

                // Nincs benn a kódcsoport a Cache-ben, le kell kérni:


                KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                Result result = service.GetAllByKodcsoportKod(execParam, kodcsoport_kod);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        KodTarElem kte = new KodTarElem(row);
                        kodtarakDictionary.Add(kte);
                    }

                    // hozzáadás a kódcsoport dictionary-hez:
                    if (kodcsoport_Dictionary.ContainsKey(kodcsoport_kod) == false)
                    {
                        lock (_sync)
                        {
                            if (!kodcsoport_Dictionary.ContainsKey(kodcsoport_kod))
                            {
                                kodcsoport_Dictionary.Add(kodcsoport_kod, kodtarakDictionary);
                            }
                        }
                    }

                    return kodtarakDictionary;
                }
                else
                {
                    Logger.Error(String.Format("GetKodtarakByKodCsoport: KRT_KodTarakService.GetAllByKodcsoportKod hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
                    return null;
                }
            }
        }

        public static string GetKodtarErtekByKodCsoportKodTar(string kodcsoport_kod, string kodtar_kod, System.Web.UI.Page page)
        {
            return GetKodtarErtekByKodCsoportKodTar(kodcsoport_kod, kodtar_kod, page, null);
        }

        public static string GetKodtarErtekByKodCsoportKodTar(string kodcsoport_kod, string kodtar_kod, System.Web.UI.Page page, DateTime? Ervenyesseg)
        {
            ExecParam execParam = UI.SetExecParamDefault(page);
            return GetKodtarErtekByKodCsoportKodTar(kodcsoport_kod, kodtar_kod, execParam, page.Cache, Ervenyesseg);
        }

        public static string GetKodtarErtekByKodCsoportKodTar(string kodcsoport_kod, string kodtar_kod, ExecParam execParam, System.Web.Caching.Cache cache)
        {
            return GetKodtarErtekByKodCsoportKodTar(kodcsoport_kod, kodtar_kod, execParam, cache, null);
        }

        public static string GetKodtarErtekByKodCsoportKodTar(string kodcsoport_kod, string kodtar_kod, ExecParam execParam, System.Web.Caching.Cache cache, DateTime? Ervenyesseg)
        {
            Dictionary<string, string> kodtarakDictionary = GetKodtarakByKodCsoport(kodcsoport_kod, execParam, cache, Ervenyesseg);

            // ha van ilyen kódtárérték:
            if (kodtarakDictionary != null && kodtarakDictionary.ContainsKey(kodtar_kod))
            {
                return kodtarakDictionary[kodtar_kod];
            }
            else
            {
                return "";
            }
        }

        public static void RefreshKodCsoportokCacheByKodcsoportKod(ExecParam execParam, String kodcsoport_kod, System.Web.Caching.Cache cache /*System.Web.UI.Page page*/)
        {
            OrgSeperatedDictionary<String, List<KodTarElem>> kodcsoport_DictionaryCache = null;

            if (cache[cache_key_kodcsoportok] != null)
            {
                kodcsoport_DictionaryCache = (OrgSeperatedDictionary<String, List<KodTarElem>>)cache[cache_key_kodcsoportok];

                Dictionary<String, List<KodTarElem>> kodcsoport_Dictionary = kodcsoport_DictionaryCache.Get(execParam.Org_Id);
                // a megadott kódcsoport kivétele a Dictionary-ből;
                // a kódcsoport majd a következő kéréskor kerül be újra a cache-be az adatbázisból

                // TODO: esetleg le lehetne rögtön kérni az adatbázisból

                if (kodcsoport_Dictionary.ContainsKey(kodcsoport_kod))
                {
                    lock (_sync)
                    {
                        if (kodcsoport_Dictionary.ContainsKey(kodcsoport_kod))
                        {
                            kodcsoport_Dictionary.Remove(kodcsoport_kod);
                        }
                    }
                }
            }
        }

    }


    public class KodTarak
    {
        public static class ALAIRO_SZEREP
        {
            public const string Alairo = "0";
            public const string Kiadmanyozo = "1";
            public const string Jovahagyo = "2";
            public const string Lattamozo = "3";
            //public const string Szignalo = "3";
            public const string EgyebAlairo = "4";
            public const string TeljesitesIgazolo = "TIG";
            public const string Utalvanyozo = "UT";
        }

        public static class Alkalmazas
        {
            public const string eAdmin = "eAdmin";
            public const string eRecord = "eRecord";
        }

        public static class Cim_Tipus
        {
            public const string Postai = "01";
            public const string Postafiok = "02";
            public const string Email = "03";
            public const string Telefon = "04";
            public const string Fax = "05";
            public const string Web = "06";
            public const string Egyeb = "Egyeb";
            public const string EgyebDefault = Email;
            public const string Default = Postai;

            public const string TitkosKapcsolatiKod = "07";
            public const string Hivatali_Kapu = TitkosKapcsolatiKod;

            public const string Postai_Cim_RNY = "01RNY";
            public const string Email_Cim_RNY = "03RNY";
            public const string Telefonszam_RNY = "04RNY";

            public const string UGYFELKAPU = "UGYFELKAPU";
            public const string CEGKAPU = "CEGKAPU";

            public const string MAK_KOD = "MAK_KOD";
        }

        //LZS
        public static class Cim_Fajtaja
        {
            public const string Szekhely = "01";
            public const string Telephely = "02";
            public const string AllandoLakcim = "03";
            public const string IdeiglenesLakcim = "04";
            public const string LevelezesiCim = "05";
            public const string ErtesitesiCim = "06";
            public const string Default = Szekhely;
        }

        public static class Cim_Kategoria
        {
            public const string K = "K";
        }
        public static class Cim_Forras
        {
            public const string HIR = "H";
            public const string FMS = "F";
            public const string AK = "K";
            public const string HKP = "E";
            public const string RNY = "R";
            public const string ETDR = "É";
        }
        public static class Partner_Forras
        {
            public const string ADATKAPU = "U";
            public const string CIMTAR = "1";
            public const string TORZSADAT = "2";
            public const string MIGRALT = "3";
            public const string MANUALIS = "4";
            public const string ADAJK = "A";
            public const string CIVIL_TORZS = "C";
            public const string HAIR = "H";
            public const string INTEZMENYEK = "I";
            public const string MINOSITETT_BESZALLITOK = "M";
            public const string ONKORMANYZATOK = "O";
            public const string KUK = "P";
            public const string ONKORMANYZATI_KUK = "Q";
            public const string BESZALLITO_KUK = "R";
            public const string ISKOLAK_KUK = "S";
            public const string SZOCIALIS_OTTHON_KUK = "T";
            public const string EXCEL = "X";
            public const string HKP = "E";
            public const string ETDR = "É";
        }
        public static class CONTROLTYPE_SOURCE
        {
            public const string CustomTextBox = "Contentum.eUIControls.CustomTextBox;Contentum.eUIControls";
            public const string RequiredTextBox = "~/Component/RequiredTextBox.ascx";
            public const string CheckBox = "System.Web.UI.WebControls.CheckBox;System.Web";
            public const string CalendarControl = "~/Component/CalendarControl.ascx";
            public const string KodTarakDropDownList = "~/Component/KodTarakDropDownList.ascx";
            public const string EditablePartnerTextBox = "~/Component/EditablePartnerTextBox.ascx";
            public const string FelhasznaloCsoportTextBox = "~/Component/FelhasznaloCsoportTextBox.ascx";
            public const string CsoportTextBox = "~/Component/CsoportTextBox.ascx";
            public const string eDropDownList = "Contentum.eUIControls.eDropDownList;Contentum.eUIControls";
            public const string RequiredNumberBox = "~/Component/RequiredNumberBox.ascx";
            public const string RequiredDecimalNumberBox = "~/Component/RequiredDecimalNumberBox.ascx";
            public const string Label = "System.Web.UI.WebControls.Label;System.Web";
            public const string AdoszamTextBox = "~/Component/AdoszamTextBox.ascx";
            public const string BizottsagTextBox = "~/Component/BizottsagTextBox.ascx";
            public const string FuggoKodTarakDropDownList = "~/Component/FuggoKodtarakDropDownList.ascx";
            public const string KodTarakListBox = "~/Component/KodTarakListBox.ascx";
            public const string KodTarakCheckBoxList = "~/Component/KodTarakCheckBoxList.ascx";

            // nem szerepel az adatbázisbeli kódcsoportban, de felületen a MapForGrid visszatérési értékeként használjuk
            public const string BoundField = "System.Web.UI.WebControls.BoundField;System.Web";
            public const string CheckBoxField = "System.Web.UI.WebControls.CheckBoxField;System.Web";

            // nem szerepel az adatbázisbeli kódcsoportban, de felületen a MapForSeachForm visszatérési értékeként használjuk
            public const string YesNoNotSetSearchFormControl = "~/Component/YesNoNotSetSearchFormControl.ascx";

            public static String MapForGrid(String ControlTypeSource)
            {
                switch (ControlTypeSource)
                {
                    case CONTROLTYPE_SOURCE.CheckBox:
                        return CheckBoxField;
                    case CONTROLTYPE_SOURCE.CustomTextBox:
                    case CONTROLTYPE_SOURCE.RequiredTextBox:
                    case CONTROLTYPE_SOURCE.CalendarControl:
                    case CONTROLTYPE_SOURCE.KodTarakDropDownList:
                    case CONTROLTYPE_SOURCE.EditablePartnerTextBox:
                    case CONTROLTYPE_SOURCE.FelhasznaloCsoportTextBox:
                    case CONTROLTYPE_SOURCE.CsoportTextBox:
                    case CONTROLTYPE_SOURCE.eDropDownList:
                    case CONTROLTYPE_SOURCE.RequiredNumberBox:
                    case CONTROLTYPE_SOURCE.RequiredDecimalNumberBox:
                    case CONTROLTYPE_SOURCE.Label:
                    case CONTROLTYPE_SOURCE.AdoszamTextBox:
                    case CONTROLTYPE_SOURCE.BizottsagTextBox:
                    case CONTROLTYPE_SOURCE.KodTarakListBox:
                    case CONTROLTYPE_SOURCE.KodTarakCheckBoxList:
                        return BoundField;
                    default:
                        return BoundField;
                }
            }

            public static String MapForSearchForm(String ControlTypeSource)
            {
                switch (ControlTypeSource)
                {
                    case CONTROLTYPE_SOURCE.CheckBox:
                        return YesNoNotSetSearchFormControl;
                    case CONTROLTYPE_SOURCE.CustomTextBox:
                    case CONTROLTYPE_SOURCE.RequiredTextBox:
                    case CONTROLTYPE_SOURCE.CalendarControl:
                    case CONTROLTYPE_SOURCE.KodTarakDropDownList:
                    case CONTROLTYPE_SOURCE.EditablePartnerTextBox:
                    case CONTROLTYPE_SOURCE.FelhasznaloCsoportTextBox:
                    case CONTROLTYPE_SOURCE.CsoportTextBox:
                    case CONTROLTYPE_SOURCE.eDropDownList:
                    case CONTROLTYPE_SOURCE.RequiredNumberBox:
                    case CONTROLTYPE_SOURCE.RequiredDecimalNumberBox:
                    case CONTROLTYPE_SOURCE.Label:
                    case CONTROLTYPE_SOURCE.AdoszamTextBox:
                    case CONTROLTYPE_SOURCE.BizottsagTextBox:
                    case CONTROLTYPE_SOURCE.KodTarakListBox:
                    case CONTROLTYPE_SOURCE.KodTarakCheckBoxList:
                        return ControlTypeSource;
                    default:
                        return ControlTypeSource;
                }
            }
        }

        public static class CsoprttagsagTipus
        {
            public const string SzervezetAsszisztense = "4";
            public const string vezeto = "3";
            public const string dolgozo = "2";
            public const string alszervezet = "1";

        }

        public static class CSOPORTTIPUS
        {
            public const string Szervezet = "0";
            public const string Dolgozo = "1";
            public const string Projekt = "4";
            public const string Testulet_Bizottsag = "5";
            public const string HivatalVezetes = "6";

        }

        public static class DOKUKAPCSOLAT_TIPUS
        {
            public const string Dokumentum_hierarchia = "1";
            public const string Elektronikus_alairas = "2";
        }

        public static class DOKUMENTUM_ALAIRAS_PKI
        {
            public const string Nincs_Vagy_Nem_Hiteles = "0";
            public const string Minositett_Alairas = "1";
            public const string Fokozott_Biztonsagu_Alairas = "2";
            public const string Hivatali_Belso_Alairas = "3";
            public const string Ervenyes_alairas = "8";     //nekrisz CR 3033
            public const string Veglegesen_Nem_Hiteles_Alairas = "9";
        }

        public static class DOKUMENTUM_ALLAPOT
        {
            public const string Iktatorendszeren_kivuli = "0";
            public const string Iktatorendszerben_nyilvantartott = "1";
        }

        public static class DOKUMENTUM_SZEREP
        {
            public const string Fodokumentum = "01";
            public const string DokumentumMelleklet = "02";
            public const string Segeddokumentum = "03";
            public const string KikuldottDokumentum = "04";
            public const string Tervezet = "05";

        }

        public static class DOKUMENTUMTIPUS
        {
            public const string Szamla = "01";
            public const string Szerzodes = "02";
            public const string Beszamolo = "11";
            public const string ElemzesEllenorzesEloterjesztes = "12";
            public const string Feljegyzes = "13";
            public const string Jogszabaly = "14";
            public const string Palyazat = "15";
            public const string Prezentacio = "16";
            public const string Tanulmany = "17";
            public const string DigitalizaltDokumentum = "18";
            public const string BontasiJegyzokonyv = "19";
            public const string AtveteliElismerveny = "20";
            public const string Egyeb = "21";
            public const string OktatasiAnyag = "40";

        }

        public static class ELJARASI_SZAKASZ
        {
            public const string Osztatlan = "00";
            public const string Altalanos = "01";
            public const string Specialis = "02";
            public const string Kozbenso = "03";

        }

        public static class IKTATASI_KOTELEZETTSEG
        {
            public const string Iktatando = "1";
            public const string Kezzel_Iktatott = "2";
            public const string Nem_Iktatando = "0";
            public const string Teves_iktatas = "3";
        }

        public static class IRAT_ALLAPOT
        {
            public static string KodcsoportKod
            {
                get { return "IRAT_ALLAPOT"; }
            }

            public const string Iktatott = "04";
            public const string Szignalt = "05";
            public const string Atiktatott = "06"; // régebben ez volt a Felszabaditott
            public const string Felszabaditva = "08"; // csak hogy ne a régi Felszabaditott nevet kapja
            public const string Kiadmanyozott = "30";
            public const string Sztornozott = "90";
            public const string Megnyitott_Munkairat = "0";
            public const string Iktatando_Munkairat = "1";
            public const string Befagyasztott_Munkairat = "2";
            public const string JegyzekreHelyezett = "31";
            public const string LezartJegyzekben = "32";
            public const string Selejtezett = "33";
            public const string LeveltarbaAdott = "34";
            public const string EgyebSzervezetnekAtadott = "35";
            public const string Foglalt = "20";
            public const string IntezkedesAlatt = "07";
            public const string Elintezett = "09";
        }

        public static class IRAT_MINOSITES
        {
            public const string DontesElokeszitesNemNyilvanos = "20"; //Döntés-előkészítéssel összefüggő adatot tartalmaz, nem nyilvános
            public const string BelsoNemNyilvanos = "10"; // Belső használatra készült, nem nyilvános	
            public const string Mind = "40"; // Iart minosites bizalmas: Mind
        }

        public static class IRATTIPUS
        {
            public static string KodcsoportKod = "IRATTIPUS";

            public const string Maganlevel = "03";
            public const string Beadvany = "04";
            public const string Hatarozat = "07";
            public const string Fellebbezes = "17";
            public const string Eloterjesztes = "08";
            public const string Hivatalbol = "11";
            public const string AjalnlatKeres = "A";
            public const string Ajanlat = "B";
            public const string Szerzodes = "C";
            public const string EUDokumentum = "EU";
            public const string Tajekoztato = "ik";
            public const string Utasitas = "LE";
            public const string Level = "LEV";
            public const string KozgyulesiEloterjesztes = "KOZGY_ELOTERJ";
            public const string BizottsagiEloterjesztes = "BE";
        }

        public static class IRATALAIRAS_ALLAPOT
        {
            public const string Alairando = "1";
            public const string Alairt = "2";
            public const string Visszautasitott = "3";
        }

        public static class IRATPELDANY_ALLAPOT
        {
            public static string KodcsoportKod
            {
                get { return "IRATPELDANY_ALLAPOT"; }
            }

            public const string Munkaallapotban = "0";
            public const string Iktatott = "04";
            public const string Felszabaditott = "06";
            public const string Irattarban_orzott = "10";
            public const string Kiadmanyozott = "30";
            public const string Lezart_jegyzeken = "31";
            public const string Jegyzekre_helyezett = "32";
            public const string Cimzett_atvette = "40";
            public const string Cimzett_nem_vette_at = "41";
            public const string Postazott = "45";
            public const string Tovabbitas_alatt = "50";
            public const string Jovahagyas_alatt = "60";
            public const string Ujrakuldendo = "61";
            //public const string Azonositott = "62";
            public const string Kimeno_kuldemenyben = "63";
            public const string Expedialt = "64";
            public const string Sztornozott = "90";
            public const string Selejtezett = "33";
            public const string LeveltarbaAdott = "34";
            public const string EgyebSzervezetnekAtadott = "35";
        }

        public static class IRATPELDANY_POSTAZAS_ALLAPOT
        {
            public const string Elso_postazas = "01";
            public const string Ujrapostazas = "02";
            public const string Elokeszites = "03";
        }


        public static class IRAT_FAJTA
        {
            public const string Eredeti = "1";
            public const string Szignalt_irattari_peldany = "2";
            public const string Masodlat = "3";
            public const string Masolat = "4";
            public const string Hitelesitett_masolat = "5";
        }

        public static class IRAT_JELLEG
        {
            public const string Webes = "1";
            public const string E_mail_uzenet = "2";
            public const string Word_dokumentum = "3";
            public const string Egyeb = "4";
        }

        // A kódtár neve tartalmazza az árat
        public static class KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS
        {
            public const string Ajanlott = "01";
            public const string Tertiveveny = "02";
            public const string Sajat_kezbe = "03";
            public const string E_ertesites = "04";
            public const string E_elorejelzes = "05";
            public const string Postai_lezaro_szolgalat = "06";
        }

        public static class KIMENO_KULDEMENY_FAJTA
        {
            public const string kcsNev = "KIMENO_KULDEMENY_FAJTA";

            public const string Hivatalos_irat = "19";
            public const string Hivatalos_irat_sajat_kezbe = "20";

            public const string Postai_Ajanlott_Kuldemeny = "40";
            public const string Postai_Erteknyilvanitasos_Level_Kuldemeny = "41";
            public const string Postai_Hivatalos_Irat = "42";
            public const string Postai_Erteknyilvanitasos_Hivatalos_Irat = "43";
            public const string Postai_Cimzett_Kezebe_Level = "44";

            public static bool IsBelfoldi(string kodtarKod)
            {
                return !IsEuropai(kodtarKod) && !IsEgyebKulfoldi(kodtarKod);
            }

            public static bool IsEuropai(string kodtarKod)
            {
                if (String.IsNullOrEmpty(kodtarKod)) return false;
                return kodtarKod.StartsWith(Constants.OrszagViszonylatKod.Europai);
            }

            public static bool IsEgyebKulfoldi(string kodtarKod)
            {
                if (String.IsNullOrEmpty(kodtarKod)) return false;
                return kodtarKod.StartsWith(Constants.OrszagViszonylatKod.EgyebKulfoldi);
            }

            public static bool IsPostaiSpecifikaioAlapjan(string kodtarKod)
            {
                if (String.IsNullOrEmpty(kodtarKod)) return false;
                return kodtarKod == Postai_Ajanlott_Kuldemeny ||
                        kodtarKod == Postai_Erteknyilvanitasos_Level_Kuldemeny ||
                        kodtarKod == Postai_Hivatalos_Irat ||
                        kodtarKod == Postai_Erteknyilvanitasos_Hivatalos_Irat ||
                        kodtarKod == Postai_Cimzett_Kezebe_Level;

            }
        }

        public static class KULDEMENY_ALLAPOT
        {
            public const string Atveve = "00";
            public const string Erkeztetve = "01";
            public const string Tovabbitas_alatt = "03";
            public const string Iktatva = "04";
            public const string Szignalva = "05";
            //public const string Felszabaditott = "06";
            //public const string Postazott = "53"; (átírva: 2008.02.11. Nem tudom honnan jött az 53, mindig is 06 volt az xml-ben)
            public const string Postazott = "06";
            public const string Kimeno_osszeallitas_alatt = "51";
            public const string Expedialt = "52";

            public const string Sztorno = "90";
            public const string Lezarva = "99";
            public const string Iktatas_Megtagadva = "10";
            public const string Foglalt = "20";

            public static List<string> GetKimenoKuldemenyAllapotFilterList()
            {
                List<string> list = new List<string>();
                list.Add(KULDEMENY_ALLAPOT.Expedialt);
                list.Add(KULDEMENY_ALLAPOT.Postazott);
                list.Add(KULDEMENY_ALLAPOT.Kimeno_osszeallitas_alatt);
                list.Add(KULDEMENY_ALLAPOT.Tovabbitas_alatt);
                list.Add(KULDEMENY_ALLAPOT.Sztorno);

                return list;
            }
        }

        public static class KULDEMENY_IKTATAS_STATUSZA
        {
            public const string Nem_kell_iktatni = "0";
            public const string Nincs_iktatva = "1";
            public const string Iktatva = "2";
            public const string Teves_iktatas = "3";
        }

        public static class KULDEMENY_KULDES_MODJA
        {
            public static string KodcsoportKod = "KULDEMENY_KULDES_MODJA";

            public const string Postai_sima = "01";
            public const string Postai_tertivevenyes = "02";
            public const string Postai_ajanlott = "03";
            public const string Ugyfel = "04";
            public const string Allami_futarszolgalat = "05";
            public const string Diplomaciai_futarszolgalat = "07";
            public const string Legiposta = "08";
            public const string Kezbesoto_utjan = "09";
            public const string Fax = "10";
            public const string E_mail = "11";
            //public const string X400 = "12";
            public const string Helyben = "13";
            /// <summary>
            /// Az NMHH-s (és Csepel) adatbázisokban 290-es kódú lett valamiért a "helyben marad" kódtárkód (13-as helyett)
            /// </summary>
            public const string HelybenMarad_NMHH = "290";
            public const string Postai_tavirat = "14";
            public const string Portal_beadvany = "15";
            //public const string eGovPortal = "16";
            public const string Postai_elsobbsegi = "17";
            public const string Futarszolgalat = "18";
            //public const string Belso_egyetemi = "19";

            public const string HivataliKapu = "19";
            public const string Elhisz = "190";
            public const string PostaHibrid = "191";
            public const string HAIR = "192";

            #region BLG 452
            public const string TUK_Postai_uton = "01";
            public const string TUK_Szemelyes_kezbesites = "98";
            public const string TUK_Futarszolgalat = "99";
            #endregion BLG 452

            // BUG_7098,7085
            public const string TUK_Helyben_marado = "110";
            public const string Helyben_marad = "290";

            public static List<string> GetElektronikusFilterList()
            {
                List<string> list = new List<string>();
                list.Add(KULDEMENY_KULDES_MODJA.E_mail);
                list.Add(KULDEMENY_KULDES_MODJA.Portal_beadvany);
                list.Add(KULDEMENY_KULDES_MODJA.HivataliKapu);
                list.Add(KULDEMENY_KULDES_MODJA.Elhisz);

                return list;
            }

            public static List<string> GetPapirAlapuFilterList()
            {
                List<string> list = new List<string>();
                list.Add(KULDEMENY_KULDES_MODJA.Postai_sima);
                list.Add(KULDEMENY_KULDES_MODJA.Postai_tertivevenyes);
                list.Add(KULDEMENY_KULDES_MODJA.Postai_ajanlott);
                list.Add(KULDEMENY_KULDES_MODJA.Ugyfel);
                list.Add(KULDEMENY_KULDES_MODJA.Allami_futarszolgalat);
                list.Add(KULDEMENY_KULDES_MODJA.Diplomaciai_futarszolgalat);
                list.Add(KULDEMENY_KULDES_MODJA.Legiposta);
                list.Add(KULDEMENY_KULDES_MODJA.Kezbesoto_utjan);
                list.Add(KULDEMENY_KULDES_MODJA.Fax);
                list.Add(KULDEMENY_KULDES_MODJA.Helyben);
                list.Add(KULDEMENY_KULDES_MODJA.Postai_tavirat);
                list.Add(KULDEMENY_KULDES_MODJA.Postai_elsobbsegi);
                list.Add(KULDEMENY_KULDES_MODJA.Futarszolgalat);

                return list;
            }
        }

        public static class KULDEMENY_TIPUS
        {
            public const string Szamla = "01";
            public const string Szerzodes = "02";
        }

        public static class MAPPA_TIPUS
        {
            public const string Fizikai = "01";
            public const string Virtualis = "02";
        }

        public static class Partner_Tipus
        {
            public const string Szervezet = "10";
            public const string Szemely = "20";
            public const string Alkalmazas = "30";
            public const string Projekt = "14";
            public const string Bizottsag = "15";
            public const string HivatalVezetes = "16";
        }

        public static class Partner_Allapot
        {
            public const string Jovahagyando = "JOVAHAGYANDO";
            public const string Jovahagyott = "JOVAHAGYOTT";
        }

        public static class PartnerKapcsolatTipus
        {
            public const string KCS_PARTNERKAPCSOLAT_TIPUSA = "PARTNERKAPCSOLAT_TIPUSA";
            //Partner kapcsolat típusai
            //1-Szervezete, felettes szervezete,
            //2- KIR1 - felhasználó szervezete,
            //3- KIR1 - Szervezeti egység érkeztetohelye,
            //4-Szervezeti egység iktatóhelye,
            //5-Szervezeti egység irattára
            //7-Szakértoi csoportja
            //8-Szakértoi csoport vezetoje
            //9-Szakértoi csoport fogadója
            //A-Szakértoi csoport tagja

            //több típus megadása: "'1','2','3','4','5'"
            public const string alarendelt = "'1','11','2','V','SZA'";

            public const string alarendeltszemely = "'2','V','SZA'";
            public const string alarendeltszervezet = "'11','1','2','6','SZA','V'";
            public const string normalszemely = "'M'";
            public const string normalszervezet = "'3','4','5'";

            public const string normalszervezetbelso = "'3','4','5'";
            public const string normalszervezetkulso = "'6'";
            public const string normalszemelykulso = "'6'";

            public const string alarendeltszervezetbelso = "'1','11','2','SZA','V'";
            public const string alarendeltszervezetbelsoTUK = "'1','11','2','SZA','V','M'";
            public const string alarendeltszervezetkulso = "'11','1'";
            public const string alarendeltszervezetkulsoTUK = "'11','1','M'";

            public const string alarendeltszemelybelso = "'SZA','2','V'";
            public const string alarendeltszemelybelsoTUK = "'SZA','2','V'";

            public const string alarendeltszemelykulso = "M";

            public const string konszolidalt = "B";

            public static string nemnormal = alarendelt + "," + "'" + konszolidalt + "'";

            public const string normal = "normal";

            public const string irattara = "5";

            public const string vezetoje = "V";
            public const string felettesSzervezete = "1";
            public const string felhasznaloSzervezete = "2";
            public const string alarendeltPartnere = "11";

            public const string Minosito = "M";
            public const string SzervezetAsszisztense = "SZA";
            public const string SzervezetDolgozoja = "SZD";
            public const string Szervezet_Kapcsolattartoja = "6";

            //egyéb szervezet típúsú partnerek kezlése
            public static string GetPartnerTipus(string tipus)
            {
                if (!String.IsNullOrEmpty(tipus))
                {
                    if (tipus == KodTarak.Partner_Tipus.Bizottsag
                        || tipus == KodTarak.Partner_Tipus.HivatalVezetes
                        || tipus == KodTarak.Partner_Tipus.Projekt)
                        return KodTarak.Partner_Tipus.Szervezet;
                }

                return tipus;
            }

            public class KapcsolatMapping
            {
                public string PartnerTipus;
                public string Belso;
                public bool IsNormal;
                public string KapcsolatTipus;
                public string ListazandoPartnerTipus;
                public string ListazandoPartnerBelso;

                public static string[] GetListazandoPartnerTipusok(string partnerTipus)
                {
                    if (partnerTipus == KodTarak.Partner_Tipus.Szervezet)
                    {
                        return new string[] { KodTarak.Partner_Tipus.Szervezet, KodTarak.Partner_Tipus.Bizottsag, KodTarak.Partner_Tipus.HivatalVezetes, KodTarak.Partner_Tipus.Projekt };
                    }

                    return new string[] { partnerTipus };
                }

                private KapcsolatMapping()
                { }

                public KapcsolatMapping(string partnerTipus, string belso, bool isNormal, string kapcsolatTipus, string listazandoPartnerTipus, string listazandoPartnerBelso)
                {
                    this.PartnerTipus = partnerTipus;
                    this.Belso = belso;
                    this.IsNormal = isNormal;
                    this.KapcsolatTipus = kapcsolatTipus;
                    this.ListazandoPartnerTipus = listazandoPartnerTipus;
                    this.ListazandoPartnerBelso = listazandoPartnerBelso;
                }

            }

            public class KapcsolatMappingList : List<KapcsolatMapping>
            {
                public IEnumerable<KapcsolatMapping> FilterByPartnerTipus(string partnerTipus)
                {
                    partnerTipus = GetPartnerTipus(partnerTipus);
                    return this.Where(x => x.PartnerTipus == partnerTipus);
                }
            }

            public static KapcsolatMappingList KapcsolatMappings = new KapcsolatMappingList();

            static PartnerKapcsolatTipus()
            {
                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szervezet, "1", true, "3", "10", "1"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szervezet, "1", true, "4", "10", "1"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szervezet, "1", true, "5", "10", "1"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szervezet, "0", true, "6", "20", "0"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szemely, "1", true, "fake", "fake", "0"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szemely, "0", true, "6", "10", "0"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szervezet, "1", false, "1", "10", "1"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szervezet, "1", false, "11", "10", "1"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szervezet, "1", false, "2", "20", "1"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szervezet, "1", false, "M", "20", "1"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szervezet, "1", false, "SZA", "20", "1"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szervezet, "1", false, "V", "20", "1"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szervezet, "0", false, "11", "10", "0"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szervezet, "0", false, "1", "10", "0"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szervezet, "0", false, "M", "20", "0"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szemely, "1", false, "SZA", "10", "1"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szemely, "1", false, "2", "10", "1"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szemely, "1", false, "V", "10", "1"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szemely, "1", false, "M", "10", "1"));

                KapcsolatMappings.Add(new KapcsolatMapping(KodTarak.Partner_Tipus.Szemely, "0", false, "M", "10", "0"));
            }

            public static string GetKapcsolatTipusForNormal(KRT_Partnerek selectedPartnerObj, bool isTUK)
            {
                string tipus = "";
                if (selectedPartnerObj.Tipus == KodTarak.Partner_Tipus.Szervezet)
                {
                    if (selectedPartnerObj.Belso == "1")
                    {
                        tipus = KodTarak.PartnerKapcsolatTipus.normalszervezetbelso;
                    }
                    else
                    {
                        tipus = KodTarak.PartnerKapcsolatTipus.normalszervezetkulso;
                    }
                }
                else
                {
                    if (selectedPartnerObj.Belso == "1")
                    {
                        tipus = "fake";
                    }
                    else
                    {
                        tipus = KodTarak.PartnerKapcsolatTipus.normalszemelykulso;
                    }
                }
                return tipus;
            }

            public static string GetKapcsolatTipusForAlarendelt(KRT_Partnerek selectedPartnerObj, bool isTUK)
            {
                string tipus = "";
                if (selectedPartnerObj.Tipus == KodTarak.Partner_Tipus.Szervezet)
                {
                    if (selectedPartnerObj.Belso == "1")
                    {
                        tipus = KodTarak.PartnerKapcsolatTipus.alarendeltszervezetbelso;
                        if (isTUK)
                        {
                            tipus = KodTarak.PartnerKapcsolatTipus.alarendeltszervezetbelsoTUK;
                        }
                    }
                    else
                    {
                        tipus = KodTarak.PartnerKapcsolatTipus.alarendeltszervezetkulso;
                        if (isTUK)
                        {
                            tipus = KodTarak.PartnerKapcsolatTipus.alarendeltszervezetkulsoTUK;
                        }
                    }
                }
                else
                {
                    if (selectedPartnerObj.Belso == "1")
                    {
                        tipus = KodTarak.PartnerKapcsolatTipus.alarendeltszemelybelso;
                        if (isTUK)
                        {
                            tipus = KodTarak.PartnerKapcsolatTipus.alarendeltszemelybelsoTUK;
                        }
                    }
                    else
                    {
                        if (isTUK)
                        {
                            tipus = KodTarak.PartnerKapcsolatTipus.normalszemelykulso;
                        }
                        else
                        {
                            tipus = "fake";
                        }
                    }
                }
                return tipus;
            }
        }



        public class PIR_ALLAPOT
        {
            public const string Fogadott = "F";
            public const string Ellenorzott = "E";
            public const string Sztornozott = "S";
            public const string Rontott = "R";
            public const string Veglegesitett = "V";
            public const string Elutasitott = "U";
        }


        public static class POSTAZAS_IRANYA
        {
            public const string Belso = "0";
            public const string Bejovo = "1";
            public const string Kimeno = "2";
            //public const string Helyben = "3";
        }

        public static class CSATOLMANY_VIZSGALAT
        {
            public const string NINCS = "1";
            public const string VAN = "2";
            public const string MIND = "0";
        }

        public static class SURGOSSEG
        {
            public const string Azonnali = "Azonnali";
            public const string IratSzerint = "IratSzerint";
            public const string Surgos_5_nap = "5";
            public const string Nap_15 = "15";
            public const string Normal = "30";
            public const string Nap_60 = "60";
        }

        public static class SZIGNALAS_TIPUSA
        {
            public const string _1_Szervezetre_Ugyintezore_Iktatas = "1";
            public const string _2_Szervezetre_Iktatas_Ugyintezore = "2";
            public const string _3_Iktatas_Szervezetre_Ugyintezore = "3";
            public const string _4_NincsSzervezetreSzignalas = "4";
            public const string _5_AutomatikusSzignalasUgyintezore = "5";
            public const string _6_IktatoSzabadonValaszthat = "6";
        }

        public static class TERTIVEVENY_ALLAPOT
        {
            public const string Elokeszitett = "0";
            public const string Feltoltott = "1";
            public const string Regisztralt = "2";
            public const string Feldolgozott = "9";
        }

        public static class TERTIVEVENY_VISSZA_KOD
        {
            public const string Kezbesitve_Cimzettnek = "1";
            public const string Kezbesitve_kozvetett_kezbesitonek = "2";
            public const string Kezbesitve_meghatalmazottnak = "3";
            public const string Kezbesitve_helyettes_atvevonek = "4";
            public const string Visszakuldes_oka_cim_nem_azonosithato = "5";
            public const string Visszakuldes_oka_nem_kereste = "6";
            public const string Visszakuldes_oka_elkoltozott = "7";
            public const string Visszakuldes_oka_bejelentve_meghalt_megszunt = "8";
            public const string Visszakuldes_oka_cimzett_ismeretlen = "9";
            public const string Visszakuldes_oka_atvetelt_megtagadta = "10";
            public const string Visszakuldés_oka_kezbesites_akadalyozott = "11";
            
        }

        public static class ETERTI_VISSZAKULDES_OKA
        {
            public const string C = "Cím nem azonosítható";
            public const string I = "Címzett ismeretlen";
            public const string A = "Kézbesítés akadályozott";
            public const string N = "Nem kereste";
            public const string E = "Elköltözött";
            public const string M = "Átvételt megtagadta";
            public const string B = "Bejelentve: meghalt, megszunt";  
        }

        public static class ETERTI_ATVETELJOGCIM
        {
            public const string C = "Címzett";
            public const string M = "Meghatalmazott";
            public const string H = "Helyettes átvevo";
            public const string K = "Közvetett kézbesíto";
        }

        //LZS - BUG_8536
        public static class TERTI_VISSZA_KOD_POSTA
        {
            public const string Nem_kereste = "1";
            public const string Meghatalmazottnak = "2";
            public const string Helyettes_atvevonek = "3";
            public const string Kozvetett_kezbesitonek = "4";
            public const string Cimzettnek = "5";

            public const string Kezbesitve_Cimzettnek = "1C";
            public const string Kezbesitve_Helyettes_Atvevonek = "1H";
            public const string Kezbesitve_Kozvetett_kezbesitonek = "1K";
            public const string Kezbesitve_Meghatalmazottnak = "1M";

            public const string Visszakuldott_Atvetelt_megtagadta = "0M";
            public const string Visszakuldott_Bejelentve_Meghalt_megszunt = "0B";
            public const string Visszakuldott_Cim_nem_azonosithato = "0C";
            public const string Visszakuldott_Cimzett_ismeretlen = "0I";
            public const string Visszakuldott_Elkoltozott = "0E";
            public const string Visszakuldott_Kezbesites_akadalyozott = "0A";
            public const string Visszakuldott_Nem_kereste = "0N";
        }

        public static class TOVABBITO_SZERVEZET
        {
            public const string Allami_futarszolgalat = "ÁLLFUT";
            public const string DHL_futarszolgalat = "DHL";
            public const string eGovPortal = "EGOV";
            public const string Egyeb = "STB";
            public const string E_mail = "MAIL";
            public const string Kezbesito = "Kezbesito";
            public const string Magyar_Posta = "POSTA";
            public const string Nem_hasznalando = "NH";
            public const string Telefax = "FAX";
        }

        public static class UGY_FAJTAJA
        {
            public const string KodcsoportKod = "UGY_FAJTAJA";
            [Obsolete]
            public const string NemHatosagiUgy = "0";
            public const string AllamigazgatasiUgy = "1";
            public const string OnkormanyzatiUgy = "2";
            public const string Nem_Hatosagi_Ugy = "15";
            public const string Egyeb_Hatosagi_Ugy = "5";
        }

        public static class UGYIRAT_ALLAPOT
        {
            public const string KodcsoportKod = "UGYIRAT_ALLAPOT";
            public const string Megnyitott_Munkaanyag = "0";
            public const string Szignalt = "03";
            public const string Iktatott = "04";
            public const string Ugyintezes_alatt = "06";
            public const string Skontroban = "07";
            public const string A_a_ban_Lezart = "09"; // A/a-ban
            public const string Irattarban_orzott = "10";
            public const string Irattarba_kuldott = "11";
            //public const string Megsemmisitesre_kijelolt = "12";
            public const string Kolcsonzott = "13";
            //public const string Megsemmisitett = "14";
            //public const string Leveltari_atadasra_kijelolt = "16";
            public const string Jegyzekre_helyezett = "30";
            public const string Lezart_jegyzekben_levo = "31";
            public const string Selejtezett = "32";
            public const string LeveltarbaAdott = "33";
            //public const string Leveltari_jegyzeken = "32";
            public const string EgyebSzervezetnekAtadott = "34";
            public const string Tovabbitas_alatt = "50";
            public const string Atmeneti_irattarbol_kikolcsonzott = "54";
            public const string Irattarbol_elkert = "55";
            public const string Skontrobol_elkert = "57";
            public const string Szerelt = "60";
            public const string Sztornozott = "90";
            //public const string Selejtezesi_jegyzeken = "95";
            //public const string Atmeneti_irattarban = "97";
            public const string ElintezesJovahagyasa = "98";
            public const string Elintezett = "99";
            public const string IrattarozasJovahagyasa = "52";
            public const string EngedelyezettKikeronLevo = "56";
            public const string SkontrobaHelyezesJovahagyasa = "70";
            public const string Felfuggesztett = "62";
            public const string Foglalt = "20";
        }

        public static class UGYIRAT_KAPCSOLATTIPUS
        {
            public const string Csatolt = "01";
        }

        public static class UGYIRAT_JELLEG
        {
            public const string KodcsoportKod = "UGYIRAT_JELLEG";

            public const string Elektronikus = "01";
            public const string Papir = "02";
            public const string Vegyes = "03";
        }

        public static class UGYIRATDARAB_ALLAPOT
        {
            public const string Iktatott = "04";
            public const string Lezart = "09";
            public const string Szerelt = "60";
            public const string Elintezette_nyilvanitott = "99";
            public const string Foglalt = "20";
        }



        public static class UGYINTEZES_ALAPJA
        {
            public const string Elektronikus_nincs_papir = "1";
            public const string Hagyomanyos_papir = "0";
            public const string Vegyes = "V";
        }



        public static class IRATTARIKIKEROALLAPOT
        {
            public const string Nyitott = "01";
            public const string Engedelyezheto = "02";
            public const string Engedelyzett = "03";
            public const string Kiadott = "04";
            public const string Visszaadott = "05";
            public const string Szerelt = "06";
            public const string Visszautasitott = "07";
            public const string Sztornozott = "08";
        }

        public class KEZBESITESITETEL_ALLAPOT
        {
            public static string KodcsoportKod
            {
                get { return "KEZBESITESITETEL_ALLAPOT"; }
            }
            public const string Atadasra_kijelolt = "1";
            public const string Atadott = "2";
            public const string Atvett = "3";
            public const string Visszakuldott = "4";
        }

        public static class BoritoTipus
        {
            public const string Boritek = "01";
            public const string Irat = "02";
            public const string EldobhatoBoritek = "03";
        }

        public static class AdathordozoTipus
        {
            public const string PapirAlapu = "01";
            public const string Fax = "02";
            public const string Email = "03";
            public const string Floppy = "04";
            public const string Cd = "05";
            public const string Dvd = "06";
            public const string Video = "07";
            public const string Kazetta = "08";
            public const string Egyeb = "09";
            public const string Csomag = "10";
            public const string Pendrive = "11";
            public const string Boritek = "12";
            public const string EgyebPapir = "13";
            public const string Tertiveveny = "14";
            public const string KapuRendszerVeveny = "16";
            public const string PostaHibridVisszaigazolas = "17";
            public const string AutomatikusVisszaigazolas = "18";
        }

        public static class MennyisegiEgyseg
        {
            public const string Lap = "01";
            public const string Darab = "02";
        }

        public static class KezelesiFeljegyzes
        {
            public static string KodcsoportKod
            {
                get { return "KEZELESI_FELJEGYZESEK_TIPUSA"; }
            }
            public const string Megjegyzes = "09";
            public const string Irattarozasra = "11";
            public const string Skontrora = "14";
            public const string Elutasitasra = "15";
            public const string Jovahagyasra = "05";
        }

        public static class SPEC_SZERVEK
        {
            public const string _kodcsoportKod = "SPEC_SZERVEK";

            public const string KOZPONTIIRATTAR = "SPEC_SZERVEK.KOZPONTIIRATTAR";
            public const string KOZPONTIIKTATO = "SPEC_SZERVEK.KOZPONTIIKTATO";
            public const string SKONTROIRATTAROS = "SPEC_SZERVEK.SKONTROIRATTAROS";
            public const string ELEKTRONIKUSIRATTAR = "SPEC_SZERVEK.ELEKTRONIKUSIRATTAR";
            public const string EGYEBSZERVEZETIRATTAR = "SPEC_SZERVEK.EGYEBSZERVEZETIRATTAR";

            public static KRT_KodTarak GetKozpontiIrattar(ExecParam execParam)
            {
                KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                string kodtarKod = KOZPONTIIRATTAR;
                string kodcsoportKod = _kodcsoportKod;
                Result res = service.GetByKodcsoportKod(execParam, kodcsoportKod, kodtarKod);

                if (res.ErrorMessage == "[50101]")
                {
                    throw new ResultException("Központi irattár not found");
                }

                if (String.IsNullOrEmpty(res.ErrorCode))
                {
                    KRT_KodTarak ktKozpontiIrattar = (KRT_KodTarak)res.Record;
                    return ktKozpontiIrattar;
                }
                else
                {
                    throw new ResultException(res);
                }
            }

            public static KRT_KodTarak GetKozpontiIrattar(Page page)
            {
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
                return GetKozpontiIrattar(execParam);
            }

            public static KRT_KodTarak GetSkontroIrattaros(ExecParam execParam)
            {
                KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                string kodtarKod = SKONTROIRATTAROS;
                string kodcsoportKod = _kodcsoportKod;
                Result res = service.GetByKodcsoportKod(execParam, kodcsoportKod, kodtarKod);

                if (res.ErrorMessage == "[50101]")
                {
                    throw new ResultException("Skontró irattáros not found");
                }

                if (String.IsNullOrEmpty(res.ErrorCode))
                {
                    KRT_KodTarak ktSkontroIrattaror = (KRT_KodTarak)res.Record;
                    return ktSkontroIrattaror;
                }
                else
                {
                    throw new ResultException(res);
                }
            }

            public static KRT_KodTarak GetSkontroIrattaros(Page page)
            {
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
                return GetSkontroIrattaros(execParam);
            }

            public static KRT_KodTarak GetKozpontiIktato(ExecParam execParam)
            {
                KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                string kodtarKod = KOZPONTIIKTATO;
                string kodcsoportKod = _kodcsoportKod;
                Result res = service.GetByKodcsoportKod(execParam, kodcsoportKod, kodtarKod);

                if (res.ErrorMessage == "[50101]")
                {
                    throw new ResultException("Központi iktató not found");
                }

                if (String.IsNullOrEmpty(res.ErrorCode))
                {
                    KRT_KodTarak ktKozpontiIktato = (KRT_KodTarak)res.Record;
                    return ktKozpontiIktato;
                }
                else
                {
                    throw new ResultException(res);
                }
            }

            public static string GetElektronikusIrattar(ExecParam execParam)
            {
                KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                string kodtarKod = ELEKTRONIKUSIRATTAR;
                string kodcsoportKod = _kodcsoportKod;
                Result res = service.GetByKodcsoportKod(execParam, kodcsoportKod, kodtarKod);

                if (res.IsError)
                {
                    Logger.Error(String.Format("GetElektronikusIrattar {0} kódcsoport {1} kódtár lekérés hiba", kodcsoportKod, kodtarKod), execParam, res);
                    //Az elektronikus irattár lekérése sikertelen
                    throw new ResultException(52244);
                }

                KRT_KodTarak ktElektronikusIrattar = (KRT_KodTarak)res.Record;
                string elektronikusIrattarId = ktElektronikusIrattar.Obj_Id;

                if (String.IsNullOrEmpty(elektronikusIrattarId))
                {
                    Logger.Error(String.Format("GetElektronikusIrattar elektronikus irattár id üres"), execParam);
                    //Az elektronikus irattár lekérése sikertelen
                    throw new ResultException(52244);
                }

                return elektronikusIrattarId;
            }

            public static string GetElektronikusIrattar(Page page)
            {
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
                return GetElektronikusIrattar(execParam);
            }

            public static string GetEgyebSzervezetIrattar(ExecParam execParam)
            {
                KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                string kodtarKod = EGYEBSZERVEZETIRATTAR;
                string kodcsoportKod = _kodcsoportKod;
                Result res = service.GetByKodcsoportKod(execParam, kodcsoportKod, kodtarKod);

                if (res.IsError)
                {
                    Logger.Error(String.Format("GetEgyebSzervezetIrattar {0} kódcsoport {1} kódtár lekérés hiba", kodcsoportKod, kodtarKod), execParam, res);
                    //Az egyéb szervezet irattár lekérése sikertelen
                    throw new ResultException(52244);
                }

                KRT_KodTarak ktEgyebSzervezetIrattar = (KRT_KodTarak)res.Record;
                string egyebSzervezetIrattarId = ktEgyebSzervezetIrattar.Obj_Id;

                if (String.IsNullOrEmpty(egyebSzervezetIrattarId))
                {
                    Logger.Error(String.Format("GetEgyebSzervezetIrattar egyéb szervezet irattár id üres"), execParam);
                    //Az egyéb szervezet irattár lekérése sikertelen
                    throw new ResultException(52244);
                }

                return egyebSzervezetIrattarId;
            }

            public static string GetEgyebSzervezetIrattar(Page page)
            {
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
                return GetEgyebSzervezetIrattar(execParam);
            }
        }

        public static class VISSZAVAROLAG
        {
            public const string Tajekoztatasra = "0";
            public const string Visszavarolag = "1";
            public const string ValaszadasiKotelezettseg = "2";
            public const string Visszavarolag_ValaszadasiKotelezettseg = "4";
            public const string Postazasra = "5";
            public const string Visszavarolag_Velemenyezni = "6";
            public const string Megkereses = "7";
        }

        /// <summary>
        /// EREC_UgyiratObjKapcsolatok tábla KapcsolatTipus oszlopa
        /// </summary>
        public static class UGYIRAT_OBJ_KAPCSOLATTIPUS
        {
            public const string KodCsoportKod = "UGYIRAT_OBJ_KAPCSOLATTIPUS";
            /// <summary>
            /// Csatolt ügyiratok
            /// </summary>
            public const string Csatolt = "01";
            /// <summary>
            /// Irat-Irat kapcsolat
            /// </summary>
            public const string Iratfordulat = "02";
            /// <summary>
            /// Egy ügyön belüli iratpéldány és irat közötti kapcsolat
            /// </summary>
            public const string MegkeresesValasz = "03";
            /// <summary>
            /// Az ügy e példány alapján indult
            /// </summary>
            public const string UgyinditoPeldany = "04";
            /// <summary>
            /// Egyéb kapcsolatok leírására fenntartva
            /// </summary>
            public const string Egyeb = "05";
            /// <summary>
            /// Itiktatás során létrehozott kapcsolat a két irat között
            /// </summary>
            public const string Atiktatas = "06";
            /// <summary>
            /// Egy ügyirat és a bele szerelt régi (2008 előtti) ügyirat közötti kapcsolat reprezentálása
            /// </summary>
            public const string MigraltSzereles = "07";
            /// <summary>
            /// Egy küldemény és az azt eredetileg tartalmazó küldemény (boríték) közötti kapcsolat reprezentálása
            /// Irány: előzmény borítékja kapcsoltnak
            /// </summary>
            public const string KuldemenyBoritekTartalom = "11";
            /// <summary>
            /// Iktatott irathoz kapcsolódó nem iktatott küldemény (pl. szerződéshez tartozó számla)
            /// Irány: előzmény az irat, kapcsolt a küldemény
            /// </summary>
            public const string IratKuldemeny = "12";
            /// <summary>
            /// Ügyirathoz kapcsolódó nem iktatott küldemény (pl. szerződéshez tartozó számla)
            /// Irány: előzmény az ügyirat, kapcsolt a küldemény
            /// </summary>
            public const string UgyiratKuldemeny = "13";
        }

        public static class OBJMETADEFINICIO_TIPUS
        {
            /// <summary>
            /// Nincs objektumtípus
            /// </summary>
            public const string A0 = "A0";
            /// <summary>
            /// Objektum kiegészítés
            /// </summary>
            public const string B1 = "B1";
            /// <summary>
            /// Típusos objektum kiegészítés
            /// </summary>
            public const string B2 = "B2";
            /// <summary>
            /// Objektum statisztika
            /// </summary>
            public const string B3 = "B3";
            /// <summary>
            /// Fizetési információk
            /// </summary>
            public const string B4 = "B4";
            /// <summary>
            /// Típusos kiegészítés ügyiratnyilvántartásho
            /// </summary>
            public const string C2 = "C2";
            /// <summary>
            /// HatosagiAdatok
            /// </summary>
            public const string HatosagiAdatok = "HA";
        }

        public static class TARGYSZO_FORRAS
        {
            public const string Automatikus = "01";
            public const string Listas = "02";
            public const string Kezi = "03";
        }

        public static class KEZELESI_FELJEGYZESEK_TIPUSA
        {
            public const string EloadoiMunkanaploMegjegyzes = "13";

            // küldeményből átvett
            public const string Iktatasra = "12";
            public const string TevesSzignalasMiattVissza = "1";
            public const string Tajekoztatasra = "03";
            public const string Lezarasra = "5";
            public const string Lassa = "7";
            public const string Latta = "8";
            public const string Megjegyzes = "09";

            public static List<string> GetKuldemenyFilterList()
            {
                List<string> list = new List<string>();
                list.Add(KEZELESI_FELJEGYZESEK_TIPUSA.Iktatasra);
                list.Add(KEZELESI_FELJEGYZESEK_TIPUSA.TevesSzignalasMiattVissza);
                list.Add(KEZELESI_FELJEGYZESEK_TIPUSA.Tajekoztatasra);
                list.Add(KEZELESI_FELJEGYZESEK_TIPUSA.Lezarasra);
                list.Add(KEZELESI_FELJEGYZESEK_TIPUSA.Lassa);
                list.Add(KEZELESI_FELJEGYZESEK_TIPUSA.Latta);
                list.Add(KEZELESI_FELJEGYZESEK_TIPUSA.Megjegyzes);
                return list;
            }
        }

        public static class FELADAT_TIPUS
        {
            public const string kcsNev = "FELADAT_TIPUS";
            public const string Megjegyzes = "M";
            public const string Feladat = "F";

            public const string ElintezesMegjegyzes = "E";
        }

        public static class FELADAT_ALTIPUS
        {
            public const string kcsNev = "FELADAT_ALTIPUS";
            public const string Intezkedesre = "01";
            public const string Velemenyezesre = "02";
            public const string Tajekoztatasra = "03";
            public const string Targyalasra = "04";
            public const string Latta = "07";
            public const string Lassa = "08";
            public const string Megjegyzes = "09";
            public const string TevesSzignalas = "10";
            public const string EloadoiMunkanaploMegjegyzes = "13";
        }

        public static class FELADAT_ALLAPOT
        {
            public const string kcsNev = "FELADAT_ALLAPOT";
            public const string Uj = "0";
            public const string Nyitott = "1";
            public const string Folyamatban = "2";
            public const string Lezart = "3";   // Megoldott
                                                //public const string Sztornozott = "4";
            public const string Delegalt = "7";

            public const string KeruloutonMegoldott = "4"; // Kerülőúton megoldott
            public const string NemMegoldhato = "5"; // Nem megoldható
            public const string Sztornozott = "6"; // Sztornózott
            public const string Lezart_Megoldott = "13"; // Lezárt - megoldott
            public const string Lezart_KeruloUtonMegoldott = "14"; // Lezárt - kerülőúton megoldott
            public const string Lezart_NemMegoldhato = "15"; // Lezárt – nem megoldható
        }

        public static class IDOEGYSEG
        {
            public const string Perc = "1";
            public const string Ora = "60";
            public const string Nap = "1440";
            public const string Munkanap = "-1440";
            // BLG_2156
            public const string Het = "10080";
            public const string Honap = "28";
            public const string Ev = "365";

            public const string DEFAULT = Munkanap;

        }

        // BLG_2156
        public static class IdoegysegFelhasznalas
        {
            public const string Selejtezes = "S";
            public const string Hatarido = "H";
        }

        public static class HELYETTESITES_MOD
        {
            public const string Helyettesites = "1";
            public const string Megbizas = "2";
        }

        public static class UGYTIPUS
        {
            public const string KozgyulesiEloterjesztes = "KOZGY_ELOTERJ";
            public const string BizottsagiEloterjesztes = "BIZ_ELOTERJ";
        }

        public static class IKT_SZAM_OSZTAS
        {
            public const string Normal = "0";
            public const string Folyosorszamos = "1";

            // Jelenleg a Folyósorszámos kiosztás mögött nincs kód,
            // ezért csak Normál-t adjuk vissza
            public static List<string> GetUsedValuesFilterList()
            {
                List<string> listUsedValues = new List<string>();
                listUsedValues.Add(IKT_SZAM_OSZTAS.Normal);

                return listUsedValues;
            }
        }

        public static class FELADAT_Prioritas
        {
            public const string kcsNev = "FELADAT_PRIORITAS";
            public const string Alacsony = "10";
            public const string Kozepes = "20";
            public const string Magas = "30";
            public const string DefaultValue = "21";
        }

        //bernat.laszlo added
        public static class KULD_KEZB_MODJA
        {
            public const string KCS = "KULD_KEZB_MODJA";
            public const string posta = "01";
            public const string futar = "02";
            public const string szemelyes_benyujtas = "03";
            public const string email = "04";
            public const string fax = "05";
            public const string e_ugyintezes = "06";
            public const string szeusz_szolgaltatas = "07";
            public const string elektronikus_uton_HKP = "10";
            public const string elektronikus_uton_Ugyfelkapu = "11";
            public const string elektronikus_uton_Adatkapu = "30";
            public const string hivataliKapun = "16";
            public const string elektronikus_uton_e_nmhh_nem_azonositott_felhasznalo = "31";
            public const string elektronikus_uton_e_nmhh_azonositott_felhasznalo = "32";
            public const string elektronikus_uton_cegkapu = "33";
            public const string elektronikus_uton = "34";
        }

        public static class IRAT_UGYINT_MODJA
        {
            public const string Szemelyes = "1";
            public const string Adat2 = "2";
            public const string Adat3 = "3";
        }

        public static class KULD_CIMZES_TIPUS
        {
            public const string nevre_szolo = "01";
            public const string sajat_kezu = "02";
        }

        public static class KodCsoportokKodok
        {
            public const string IRAT_HATASA_UGYINTEZESRE = "IRAT_HATASA_UGYINTEZESRE";
            public const string UGYIRAT_FELFUGGESZTES_OKA = "UGYIRAT_FELFUGGESZTES_OKA";
            public const string UGYIRAT_LEZARAS_OKA = "UGYIRAT_LEZARAS_OKA";
        }

        //tomeges_alairas_statusok
        public static class TOMEGES_ALAIRAS_FOLYAMAT_STATUS
        {
            public const string folyamat_rogzitve = "00";
            public const string alairas_folyamatban = "01";
            public const string sikeres_alairas = "02";
            public const string sikertelen_alairas = "03";
        }

        #region BLG 2947
        //Aláírás ellenőrzés státuszok
        public static class TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS
        {
            public const string folyamat_rogzitve = "10";
            public const string ellenorzes_folyamatban = "11";
            public const string sikeres_ellenorzes = "12";
            public const string sikertelen_ellenorzes = "13";
        }

        //Felülvizsgalat eredmények
        public static class TOMEGES_ALAIRAS_FELULVIZSGALAT_EREDMENY
        {
            public const string sikeres_felulvizsgalat = "Sikeres felülvizsgálat.";
            public const string sikertelen_felulvizsgalat = "Sikertelen felülvizsgálat, dokumentum nem rendelkezik érvényes elektronikus aláírással, vagy ellenőrzés során hiba lépett fel.";
        }
        #endregion

        public static class ELhiszUrlapSablon
        {
            public const string Adatkapu = "ADATKAPU";
            public const string Mediaszabalyozas = "MEDIASZABALYOZAS";
            public const string ElektronikusBeadvany = "ELEKTRONIKUSBEADVANY";
        }

        public static class eBeadvany_Irany
        {
            public const string Bejovo = "0";
            public const string Kimeno = "1";
            public const string KuldesiAdatok = "2";
        }

        public static class KULDEMENY_INPUT_FORMAT
        {
            public const string UNSIGNED_DOCUMENT = "UNSIGNED_DOCUMENT";
            public const string UNSIGNED_PDF = "UNSIGNED_PDF";
            public const string SIGNED_PDF = "SIGNED_PDF";
            public const string SIGNED_XADES_XML = "SIGNED_XADES_XML";
            public const string SIGNED_ASIC_E = "SIGNED_ASIC_E";
            public const string KR = "KR";
            public const string SIGNED_ES3 = "SIGNED_ES3";
        }

        public static class KULDEMENY_OUTPUT_FORMAT
        {
            public const string SIGNED_PDF = "SIGNED_PDF";
            public const string SIGNED_XADES_XML = "SIGNED_XADES_XML";
            public const string SIGNED_ASIC_E = "SIGNED_ASIC_E";
            public const string KR = "KR";
            public const string SIGNED_ES3 = "SIGNED_ES3";
        }

        public static class ELOSZTOIV_FAJTA
        {
            public const string KodCsoportKod = "ELOSZTOIV_FAJTA";

            public const string Egyedi = "1";
            public const string Titkosito = "4";
            public const string Cimzettlista = "10";
        }

        public static class VISSZAFIZETES_JOGCIME
        {
            public const string Dij = "01";
            public const string Illetek = "02";
        }

        public static class SPEC_KONYVEK
        {
            public const string _kodcsoportKod = "SPEC_KONYVEK";

            public const string MEGSEMMISITESINAPLO = "SPEC_KONYVEK.MEGSEMMISITESINAPLO";

            public static KRT_KodTarak GetMegsemmisitesiNaplo(ExecParam execParam)
            {
                KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                string kodtarKod = MEGSEMMISITESINAPLO;
                string kodcsoportKod = _kodcsoportKod;
                Result res = service.GetByKodcsoportKod(execParam, kodcsoportKod, kodtarKod);

                if (res.ErrorMessage == "[50101]")
                {
                    throw new ResultException("A megsemmisítési napló nem található!");
                }

                if (String.IsNullOrEmpty(res.ErrorCode))
                {
                    KRT_KodTarak kt = (KRT_KodTarak)res.Record;
                    return kt;
                }
                else
                {
                    throw new ResultException(res);
                }
            }

        }

        public static class UGYIRAT_ELTELT_IDO_ALLAPOT
        {
            /// <summary>
            /// ÚJ
            /// </summary>
            public const string NEW = "0";
            /// <summary>
            /// INDUL
            /// </summary>
            public const string START = "1";
            /// <summary>
            /// TELJESEN MEGÁLL
            /// </summary>
            public const string STOP = "2";
            /// <summary>
            /// FELFÜGGESZT
            /// </summary>
            public const string PAUSE = "3";
        }

        /// <summary>
        /// Eljárás felfüggesztésének, szünetelésének oka
        /// </summary>
        public static class IRAT_HATASA_UGYINTEZESRE
        {
            public const string KCS = "IRAT_HATASA_UGYINTEZESRE";

            public const string A_hatosagi_ugyintezesi_idore_nincs_hatassal_NONE = "0_NONE";
            public const string A_hatosagi_ugyintezesi_idore_nincs_hatassal_ON_PROGRESS = "0_ON_PROGRESS";
            public const string A_hatosagi_ugyintezesi_idore_nincs_hatassal_STOP = "0_STOP";
            public const string A_hatosagi_ugyintezesi_idore_nincs_hatassal_PAUSE = "0_PAUSE";

            public const string A_hatosagi_ugyintezesi_ido_indul_folytatodik = "1";
            public const string A_hatosagi_ugyintezesi_ido_megall = "2";
            public const string Hatosagi_ugyintezesi_ido_vege = "3";
            public const string Az_elsofok_fellebbezesi_kerelem_alapjan_modosít_visszavon = "4";
            public const string Masodfoku_hatosagi_ugyintezesi_ido_indul_folytatodik = "5";
            public const string Masodfoku_hatosagi_ugyintezesi_ido_megall = "6";
            public const string Masodfoku_hatosagi_ugyintezesi_ido_vege = "7";
            public const string Hatosagi_ugyintezesi_ido_indul_ujraindul_es_vege = "8";
            public const string A_hatosagi_ugyintezesi_ido_indul_ujraindul_es_megall = "9";
            public const string Masodfoku_hatosagi_ugyintezesi_ido_indul_ujraindul_es_vege = "10";
            public const string Masodfoku_hatosagi_ugyintezesi_ido_indul_ujraindul_es_megall = "11";
        }
        public static class IRAT_HATASA_UGYINTEZESRE_KOD
        {
            public const string NULLA_NONE = "0_NONE";
            public const string NULLA_STOP = "0_STOP";
            public const string NULLA_PAUSE = "0_PAUSE";
            public const string NULLA_ONPROGRESS = "0_ON_PROGRESS";

            public const string Egy = "1";
            public const string Ketto = "2";
            public const string Harom = "3";
            public const string Negy = "4";
            public const string Ot = "5";
            public const string Hat = "6";
            public const string Het = "7";
            public const string Nyolc = "8";
            public const string Kilenc = "9";
            public const string Tiz = "10";
            public const string Tizenegy = "11";
        }
        public static class UGY_JELLEG_FAJTAJA_HATOSAGI
        {
            public const string KCS_UGY_FAJTAJA = "UGY_FAJTAJA";

            public const string HATOSAGI = "HATOSAGI";
            public const string NEM_HATOSAGI = "NEM_HATOSAGI";
        }

        public static class ELJARASI_SZAKASZ_FOK
        {
            public const string KCS = "ELJARASI_SZAKASZ_FOK";
            public const string I = "1";
            public const string II = "2";
        }
        public static class UGYIRAT_LEZARAS_OKA
        {
            public const string KCS = "UGYIRAT_LEZARAS_OKA";

            public const string ATTETEL = "ATTETEL";
            public const string VIZSGALAT_NELKULI_ELUTASITO_VEGZES = "VIZSGALAT_NELKULI_ELUTASITO_VEGZES";

            public const string ELJARAST_MEGSZUNTETO_VEGZES = "ELJARAST_MEGSZUNTETO_VEGZES";
            public const string ELSO_FOKU_HATAROZAT = "ELSO_FOKU_HATAROZAT";
            public const string ELSO_FOKU_HATAROZAT_MODOSITASA = "ELSO_FOKU_HATAROZAT_MODOSITASA";
            public const string ELSO_FOKU_HATAROZAT_VISSZAVONASA = "ELSO_FOKU_HATAROZAT_VISSZAVONASA";

            public const string MASODFOKU_ELJARAST_MEGSZUNTETO_VEGZES = "MASODFOKU_ELJARAST_MEGSZUNTETO_VEGZES";
            public const string MASODFOKU_HATAROZAT = "MASODFOKU_HATAROZAT";
            public const string MASODFOKU_HATAROZAT_MODOSITASA = "MASODFOKU_HATAROZAT_MODOSITASA";
            public const string MASODFOKU_HATAROZAT_VISSZAVONASA = "ELSO_FOKU_HATAROZAT_VISSZAVONASA";
        }


        public static class UGYIRAT_FELFUGGESZTES_OKA
        {
            public const string KCS = "UGYIRAT_FELFUGGESZTES_OKA";

            public const string ADATSZOLGALTATAS = "ADATSZOLGALTATAS";
            public const string BIZONYITASI_ELJARASBAN = "BIZONYITASI_ELJARASBAN";
            public const string EGYEB_OK = "EGYEB_OK";
            public const string ELJARAS_FELFUGGESZTESE = "ELJARAS_FELFUGGESZTESE";
            public const string HATOSAGI_KOZVETITES = "HATOSAGI_KOZVETITES";
            public const string IRAT_FORDITASA = "IRAT_FORDITASA";
            public const string JOGSEGELYELJARAS = "JOGSEGELYELJARAS";
            public const string KEZBESITESI_KOZLESI_IDOTARTAM = "KEZBESITESI_KOZLESI_IDOTARTAM";
            public const string SZAKERTOI_VELEMENY_ELKESZITESE = "SZAKERTOI_VELEMENY_ELKESZITESE";
            public const string SZAKHATOSAGI_ELJARAS = "SZAKHATOSAGI_ELJARAS";
            public const string HATÁSKORI_VAGY_ILLETEKESSÉGI_VITA = "HATÁSKORI_VAGY_ILLETEKESSÉGI_VITA";
        }

        public enum SakkoraUgyFajtaTipus
        {
            NH,
            //NH1,
            //NH2,
            H1,
            H2,
        }
        public static class TITULUSOK
        {
            public const string DR = "DR.";
            public const string IFJ = "IFJ.";
            public const string OZV = "ÖZV.";
            public const string II = "II.";
        }

        public static class OCR_Allapot
        {
            public const string KCS_OCR_Allapot = "OCR_Allapot";
            public const string Nem_OCR_Ezendo_Dokumentum = "";
            public const string OCR_Ezendo_Dokumentum = "1";
            public const string OCR_Ezes_Alatt_Levo_Dokumentum = "2";
            public const string OCR_Ezett_Dokumentum = "3";
        }

        public static class JEGYZEK_ALLAPOT
        {
            public const string Nyitott = "1";
            public const string Lezart = "2";
            public const string VegrehajtasFolyamatban = "3";
            public const string Vegrahajtott = "4";
            public const string Sztornozott = "9";
        }
        public static class KAPCSOLT_UGYEK_ERTESITES_TIPUS
        {
            public const string FelhasznaloTorles = "1";
            public const string FelhasznaloCsoportTagsagTorles = "2";
        }
        /// <summary>
        /// Partner_Tipusok_Filter
        /// </summary>
        public static class Partner_Tipusok_Filter
        {
            public static NameDescriptionValueModel Mind = new NameDescriptionValueModel("0", "All", "Minden partner");
            public static NameDescriptionValueModel Szemely = new NameDescriptionValueModel("1", "Szemely", "Természetes személy");
            public static NameDescriptionValueModel Szervezet = new NameDescriptionValueModel("2", "Szervezet", "Szervezet");
            public static NameDescriptionValueModel SzervezetKapcsolattartoja = new NameDescriptionValueModel("3", "SzervezetKapcsolattartoja", "Szervezet kapcsolattartója");
        }

        public static class eBeadvany_CelRendszer
        {
            public const string KODCSOPORT = "eBeadvany_CelRendszer";
            public const string WEB = "WEB";
            public const string HAIR = "HAIR";
            public const string FILESHARE = "FILESHARE";
        }


        /// <summary>
        /// ALAIRAS_KOD
        /// </summary>
        public static class ALAIRAS_KOD
        {
            public const string Egyszeru_Szemelyes_Alairas = "A1";
            public const string Utolagosan_Adminisztralt_Alairas = "A2";
            public const string KET_Minositett_Alairas = "M1";
        }
        /// <summary>
        /// ALAIRAS_MOD
        /// </summary>
        public static class ALAIRAS_MOD
        {
            public const string M_ADM = "M_ADM";
            public const string E_EMB = "E_EMB";
            public const string M_UTO = "M_UTO";
        }

        public static class TOMEGES_IKTATAS_TETEL_ALLAPOT
        {
            public const string Inicializalt = "1";
            public const string Iktatott = "2";
            public const string Hibas = "9";
        }

        /// <summary>
        /// Paraméter forrás: E-ÜzenetSzabály tárgy mező
        /// </summary>
        public static class PARAMETER_FORRAS_EUZENETSZABALY_TARGY
        {
            public const string KODCSOPORT = "PARAMETER_FORRAS_EUZENETSZABALY_TARGY";
            public const string TXT = "TXT";
            public const string XML = "XML";
            public const string DB = "DB";
        }

        //LZS - BUG_7062
        public static class EXTRANAPOK
        {
            public const string MunkaszunetiNap = "-1";
            public const string RendkivuliMunkanap = "1";
            public const string Ervenytelen = "Érvénytelen érték";
            public const string Empty = "X";
        }

        public static class EXTRANAPOK_NEV
        {
            public const string MunkaszunetiNap = "Munkaszüneti nap";
            public const string RendkivuliMunkanap = "Rendkívüli munkanap";
            public const string Ervenytelen = "Érvénytelen érték";
            public const string Empty = "X";
        }

        public static class ELINTEZESMOD
        {
            public const string Egyeb = "90";
            public const string Szereles = "9";
        }

        public static class ELSODLEGES_ADATHORDOZO
        {
            public const string PapirAlapu_Irat = "01";
            public const string Elektronikus_irat = "09";
        }
        /// <summary>
        /// INT_MODULOK
        /// </summary>
        public static class INT_MODULOK
        {
            public const string AdatKapuIntegracio = "AdatKapuIntegracio";
            public const string ASPDWH = "ASPDWH";
            public const string PostaHibrid = "PostaHibrid";
            public const string eUzenet = "eUzenet";
        }
        /// <summary>
        /// INT_PARAMETEREK
        /// </summary>
        public static class INT_PARAMETEREK
        {
            public const string ADATKAPU_PARTNER_SYNC_VIEW_SCHEMA = "ADATKAPU_PARTNER_SYNC_VIEW_SCHEMA";
            public const string FEJLESZTO_AZONOSITO = "FEJLESZTO_AZONOSITO";
            public const string Posta_PartnerNev = "Posta_PartnerNev";
            public const string Posta_KRID = "Posta_KRID";
            public const string eUzenetSchema = "eUzenetSchema";
            public const string eUzenetForras = "eUzenetForras";
            public const string Posta_RovidNev = "Posta_RovidNev";
        }

        public static class TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT
        {
            public const string InicializalasFolyamatban = "01";
            public const string InicializalasiHiba = "02";
            public const string Inicializalt = "03";
            public const string VegrehajtasFolyamatban = "04";
            public const string VegrehajtasiHiba = "05";
            public const string Vegrehajtott = "06";
        }
        /// <summary>
        /// EBEADVANY_FELADO_TIPUS
        /// </summary>
        public static class EBEADVANY_FELADO_TIPUS
        {
            public const string KCS = "EBEADVANYFELADOTIPUS";
            public const string Elektronikus_Uton_Ugyfelkapu = "0";
            public const string Allampolgar = Elektronikus_Uton_Ugyfelkapu;

            public const string Elektronikus_Uton_HivataliKapu = "1";
            public const string NemTermeszetesSzemely_HivataliKapu_Cegkapu_Perkapu = Elektronikus_Uton_HivataliKapu;

            public const string Rendszer = "2";
            public const string Elektronikus_Uton_Cegkapu = "3";
        }

        public class EBEADVANY_KODCSOPORTOK
        {
            public const string KEZBETITETTSEG = "HKP_KEZBESITETTSEG";
            public const string VALASZUTVONAL = "HKP_VALASZUTVONAL";
            public const string TARTERULET = "HKP_TARTERULET";
            public const string ALLAPOT = "HKP_ALLAPOT";
            public const string FELADO_TIPUSA = "HKP_FELADO_TIPUSA";
            public const string POSTAZAS_IRANYA = "POSTAZAS_IRANYA";
            public const string eBeadvany_CelRendszer = "eBeadvany_CelRendszer";
        }
        public static class HAIR_ADAT_TIPUSOK
        {
            public const string KezbesitErtesites = "01";
            public const string PartnerAdatSzinkronizacio = "02";
        }

        public static class HIVATALOS_LEVEL_KODCSOPORT
        {
            public const string KODCSOPORT_KOD = "HIVATALOS_LEVEL";
            public const string IGEN = "1";
            public const string NEM = "0";
        }
    }
}

