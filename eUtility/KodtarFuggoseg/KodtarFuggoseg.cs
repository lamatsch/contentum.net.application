﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;

namespace Contentum.eUtility
{
    public static class KodtarFuggoseg
    {
        public static Result GetFuggoKodtarak(ExecParam execParam, string vezerloKodcsoportKod, string fuggoKodcsoportKod, string vezerloKodtarId)
        {
            Contentum.eAdmin.Service.KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();

            KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
            search.Vezerlo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", vezerloKodcsoportKod);
            search.Vezerlo_KodCsoport_Id.Operator = Query.Operators.inner;
            search.Fuggo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", fuggoKodcsoportKod);
            search.Fuggo_KodCsoport_Id.Operator = Query.Operators.inner;

            return service.GetAll(execParam, search);
        }
        public static Result GetFuggoKodtarById(ExecParam execParam, string guidString)
        {
            Contentum.eAdmin.Service.KRT_KodtarFuggosegService svc = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
            ExecParam kfExecParam = execParam;
            kfExecParam.Record_Id = guidString;
            Result result = svc.Get(kfExecParam);
            return result;
        }

        public static List<string> GetFuggoKodtarakKodList(ExecParam execParam, string vezerloKodcsoportKod, string fuggoKodcsoportKod, string vezerloKodtarKod, out bool hasNincsItem)
        {
            string vezerloKodtarId = GetKodtarId(execParam, vezerloKodcsoportKod, vezerloKodtarKod);

            Contentum.eAdmin.Service.KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();

            KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
            search.Vezerlo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", vezerloKodcsoportKod);
            search.Vezerlo_KodCsoport_Id.Operator = Query.Operators.inner;
            search.Fuggo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", fuggoKodcsoportKod);
            search.Fuggo_KodCsoport_Id.Operator = Query.Operators.inner;

            Result res = service.GetAll(execParam, search);

            res.CheckError();

            hasNincsItem = false;

            if (!res.IsError)
            {

                if (res.Ds.Tables[0].Rows.Count > 0)
                {
                    string adat = res.Ds.Tables[0].Rows[0]["Adat"].ToString();

                    if (!String.IsNullOrEmpty(adat))
                    {
                        Contentum.eAdmin.BaseUtility.KodtarFuggoseg.KodtarFuggosegDataClass fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(adat);

                        List<string> fuggoKodtarak = new List<string>();

                        foreach (Contentum.eAdmin.BaseUtility.KodtarFuggoseg.KodtarFuggosegDataItemClass item in fuggosegek.Items)
                        {
                            if (item.VezerloKodTarId.Equals(vezerloKodtarId, StringComparison.InvariantCultureIgnoreCase))
                            {
                                string fuggoKodtarKod = GetKodtarKod(execParam, fuggoKodcsoportKod, item.FuggoKodtarId);
                                if (fuggoKodtarKod != null)
                                    fuggoKodtarak.Add(fuggoKodtarKod);
                            }
                        }

                        foreach (Contentum.eAdmin.BaseUtility.KodtarFuggoseg.KodtarFuggosegDataNincsClass item in fuggosegek.ItemsNincs)
                        {
                            if (item.VezerloKodTarId.Equals(vezerloKodtarId, StringComparison.InvariantCultureIgnoreCase))
                            {
                                hasNincsItem = true;
                                break;
                            }
                        }

                        return fuggoKodtarak;
                    }
                }
            }

            return null;
        }

        static string GetKodtarId(ExecParam execParam, string kodcsoportKod, string kodtarKod)
        {
            List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarList = KodTar_Cache.GetKodtarakByKodCsoportList(kodcsoportKod, execParam, GetCache(), null);

            foreach (Contentum.eUtility.KodTar_Cache.KodTarElem elem in kodtarList)
            {
                if (elem.Kod == kodtarKod)
                    return elem.Id;
            }

            return null;
        }

        static string GetKodtarKod(ExecParam execParam, string kodcsoportKod, string kodTarId)
        {
            List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarList = KodTar_Cache.GetKodtarakByKodCsoportList(kodcsoportKod, execParam, GetCache(), null);

            foreach (Contentum.eUtility.KodTar_Cache.KodTarElem elem in kodtarList)
            {
                if (elem.Id == kodTarId)
                    return elem.Kod;
            }

            return null;
        }

        private static Cache GetCache()
        {
            if (HttpContext.Current == null)
            {
                return System.Web.HttpRuntime.Cache;
            }
            return HttpContext.Current.Cache;
        }
    }
}
