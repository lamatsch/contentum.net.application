﻿using System.Collections.Generic;

namespace Contentum.eAdmin.BaseUtility.KodtarFuggoseg
{
    /// <summary>
    /// Summary description for KodtarFuggosegDataClass
    /// </summary>
    public class KodtarFuggosegDataClass
    {
        public string VezerloKodCsoportId { get; set; }
        public string FuggoKodCsoportId { get; set; }

        public List<KodtarFuggosegDataItemClass> Items { get; set; }
        public List<KodtarFuggosegDataNincsClass> ItemsNincs { get; set; }

        public KodtarFuggosegDataClass()
        {
            Items = new List<KodtarFuggosegDataItemClass>();
            ItemsNincs = new List<KodtarFuggosegDataNincsClass>();
        }
    }
}