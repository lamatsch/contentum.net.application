﻿using System.Collections.Generic;

namespace Contentum.eAdmin.BaseUtility.KodtarFuggoseg
{
    /// <summary>
    /// Summary description for KodtarFuggosegDataClass
    /// </summary>
    public class KodtarFuggosegDataItemClass
    {
        public string VezerloKodTarId { get; set; }
        public string FuggoKodtarId { get; set; }
        public bool? Aktiv { get; set; }

        public override bool Equals(object obj)
        {
            var @class = obj as KodtarFuggosegDataItemClass;
            return @class != null
                    &&
                   VezerloKodTarId == @class.VezerloKodTarId
                    &&
                   FuggoKodtarId == @class.FuggoKodtarId;
        }

        public override int GetHashCode()
        {
            var hashCode = -1467904237;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(VezerloKodTarId);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(FuggoKodtarId);
            return hashCode;
        }
    }
}