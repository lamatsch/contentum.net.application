﻿using System.Collections.Generic;

namespace Contentum.eAdmin.BaseUtility.KodtarFuggoseg
{
    /// <summary>
    /// Summary description for KodtarFuggosegDataClass
    /// </summary>
    public class KodtarFuggosegDataNincsClass
    {
        public string VezerloKodTarId { get; set; }

        public bool? Nincs { get; set; }

        public override bool Equals(object obj)
        {
            var @class = obj as KodtarFuggosegDataNincsClass;
            return @class != null &&
                   VezerloKodTarId == @class.VezerloKodTarId;
        }

        public override int GetHashCode()
        {
            var hashCode = -1839797616;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(VezerloKodTarId);
            return hashCode;
        }
    }
}