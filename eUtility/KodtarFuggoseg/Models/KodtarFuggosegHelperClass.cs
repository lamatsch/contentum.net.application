﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contentum.eAdmin.BaseUtility.KodtarFuggoseg
{
    /// <summary>
    /// Summary description for KodtarFuggosegHelperClass
    /// </summary>
    public class KodtarFuggosegHelperClass
    {
        public string Id { get; set; }
        public string KodCsoportId { get; set; }
        public string Nev { get; set; }

        public override bool Equals(object obj)
        {
            var @class = obj as KodtarFuggosegHelperClass;
            return @class != null && Id == @class.Id;
        }

        public override int GetHashCode()
        {
            var hashCode = -697703164;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Id);
            return hashCode;
        }
    }
}