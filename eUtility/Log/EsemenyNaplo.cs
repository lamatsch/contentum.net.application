﻿using System;
using System.Collections.Generic;
using System.Text;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Reflection;
using System.Web.UI;
using System.Threading;

namespace Contentum.eUtility
{
    public class EsemenyNaplo
    {

        private class EsemenyInfo
        {
            private ExecParam _ExecParam;

            public ExecParam ExecParam
            {
                get { return _ExecParam; }
                set { _ExecParam = value; }
            }

            private string _TranzId;

            public string TranzId
            {
                get { return _TranzId; }
                set { _TranzId = value; }
            }

            private string _Obj_Id;

            public string Obj_Id
            {
                get { return _Obj_Id; }
                set { _Obj_Id = value; }
            }

            private string _TablaNev;

            public string TablaNev
            {
                get { return _TablaNev; }
                set { _TablaNev = value; }
            }

            private string _FunkcioKod;

            public string FunkcioKod
            {
                get { return _FunkcioKod; }
                set { _FunkcioKod = value; }
            }

            private string _KeresesiFeltetel;

            public string KeresesiFeltetel
            {
                get { return _KeresesiFeltetel; }
                set { _KeresesiFeltetel = value; }
            }

            private string _TalalatokSzama;

            public string TalalatokSzama
            {
                get { return _TalalatokSzama; }
                set { _TalalatokSzama = value; }
            }
        }

        public static void Insert(ExecParam execParam, string TablaNev, string FunkcioKod, BaseSearchObject SearchObject, Result result)
        {
            if (!result.IsError)
            {
                try
                {
                    string KeresesiFeltetel = String.Empty;

                    if (SearchObject != null)
                    {
                        Query query = new Query();
                        query.BuildFromBusinessDocument(SearchObject);
                        string whereByManual = String.Empty;
                        FieldInfo fi = SearchObject.GetType().GetField("WhereByManual");
                        if (fi != null)
                        {
                            whereByManual = fi.GetValue(SearchObject).ToString();
                        }
                        KeresesiFeltetel = Search.ConcatWhereExpressions(query.Where, whereByManual);
                    }

                    string TalalatokSzama = String.Empty;

                    if (result.Ds.Tables.Count > 1)
                    {
                        TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }
                    else
                    {
                        TalalatokSzama = result.Ds.Tables[0].Rows.Count.ToString();
                    }

                    EsemenyInfo ei = new EsemenyInfo();
                    ei.ExecParam = execParam;
                    ei.TranzId = execParam.Page_Id;
                    ei.TablaNev = TablaNev;
                    ei.FunkcioKod = FunkcioKod;
                    ei.KeresesiFeltetel = KeresesiFeltetel;
                    ei.TalalatokSzama = TalalatokSzama;
                    ThreadPool.QueueUserWorkItem(new WaitCallback(InsertEsemenyProc),ei);
                }
                catch (Exception ex)
                {
                    Logger.Error("EsemenyNaploInsert hiba", ex);
                }
            }
        }

        public static void Insert(ExecParam execParam, string TablaNev, string FunkcioKod, string Obj_Id, Result result)
        {
            if (!result.IsError)
            {
                try
                {
                    EsemenyInfo ei = new EsemenyInfo();
                    ei.ExecParam = execParam;
                    ei.TranzId = execParam.Page_Id;
                    ei.Obj_Id = Obj_Id;
                    ei.TablaNev = TablaNev;
                    ei.FunkcioKod = FunkcioKod;
                    ThreadPool.QueueUserWorkItem(new WaitCallback(InsertEsemenyProc), ei);
                }
                catch (Exception ex)
                {
                    Logger.Error("EsemenyNaploInsert hiba", ex);
                }
            }
        }

        private static void InsertEsemenyProc(object stateInfo)
        {
            EsemenyInfo ei = (EsemenyInfo)stateInfo;
            KRT_EsemenyekService service = eAdminService.ServiceFactory.GetKRT_EsemenyekService();
            Result res = service.InsertByFunkcioKod(ei.ExecParam,ei.TranzId, ei.Obj_Id, ei.TablaNev, ei.FunkcioKod, ei.KeresesiFeltetel, ei.TalalatokSzama);
            if (res.IsError)
            {
                Logger.Error("EsemenyNaploInsert hiba", null, res);
            }
        }
    }
}
