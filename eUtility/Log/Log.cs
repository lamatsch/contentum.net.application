using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using log4net;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Data.SqlClient;
using Parameter = Contentum.eBusinessDocuments.Utility.Parameter;


namespace Contentum.eUtility.LogStoredProcedures
{
    /// <summary>
    /// WebService loggol�s�hoz sz�ks�ges adatb�zism�veletek
    /// </summary>
    //[Transaction(TransactionOption.Disabled)]
    public partial class KRT_Log_WebServiceStoredProcedure
    {
        private DataContext dataContext;

        internal static Dictionary<String, Parameter> sp_KRT_Log_WebServiceInsertParameters = null;
        internal static Dictionary<String, Parameter> sp_KRT_Log_WebServiceUpdateParameters = null;

        public KRT_Log_WebServiceStoredProcedure(DataContext _dataContext)
        {
            this.dataContext = _dataContext;
        }

        public Result Insert(String Method, ExecParam ExecParam, KRT_Log_WebService Record)
        {
            return Insert(Method, ExecParam, Record, DateTime.Now);
        }

        public Result Insert(String Method, ExecParam ExecParam, KRT_Log_WebService Record, DateTime ExecutionTime)
        {
            //Contentum.eUtility.Log log = new Contentum.eUtility.Log();

            Result _ret = new Result();

            //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
            //{
            //sqlConnection.Open();

            SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
            if (Method == "") Method = "Insert";
            switch (Method)
            {
                case Constants.Insert:
                    //log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_WebServiceInsert");
                    Record.Base.Updated.LetrehozasIdo = true;
                    Record.Base.Updated.Letrehozo_id = true;
                    Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                    Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;

                    // TranzId-ba betessz�k a Page_Id-t
                    Record.Base.Tranz_id = ExecParam.Page_Id;

                    SqlComm = new SqlCommand("[sp_KRT_Log_WebServiceInsert]");
                    if (sp_KRT_Log_WebServiceInsertParameters == null)
                    {
                        //sp_KRT_Log_WebServiceInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_Log_WebServiceInsert]");
                        //sp_KRT_Log_WebServiceInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_Log_WebServiceInsert]");
                        sp_KRT_Log_WebServiceInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Log_WebServiceInsert]");
                    }

                    Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_Log_WebServiceInsertParameters);

                    Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

                    break;
                case Constants.Update:
                    //log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_WebServiceUpdate");
                    Record.Base.Updated.ModositasIdo = true;
                    Record.Base.Updated.Modosito_id = true;
                    Record.Base.ModositasIdo = ExecutionTime.ToString();
                    Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                    SqlComm = new SqlCommand("[sp_KRT_Log_WebServiceUpdate]");
                    if (sp_KRT_Log_WebServiceUpdateParameters == null)
                    {
                        //sp_KRT_Log_WebServiceUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_Log_WebServiceUpdate]");
                        //sp_KRT_Log_WebServiceUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_Log_WebServiceUpdate]");
                        sp_KRT_Log_WebServiceUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Log_WebServiceUpdate]");
                    }

                    Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_Log_WebServiceUpdateParameters);

                    Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                    break;
            }

            SqlComm.CommandType = CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            try
            {
                SqlComm.ExecuteNonQuery();
                if (Method == Constants.Insert)
                {
                    _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
                }
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            //sqlConnection.Close();
            //}        

            //log.SpEnd(ExecParam, _ret);

            return _ret;
        }

        public Result Get(ExecParam ExecParam)
        {
            //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam,"sp_KRT_Log_WebServiceGet");

            Result _ret = new Result();

            //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
            //{
            //    sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_WebServiceGet]");
                SqlComm.CommandType = CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

                KRT_Log_WebService _KRT_Log_WebService = new KRT_Log_WebService();
                Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Log_WebService, SqlComm);
                _ret.Record = _KRT_Log_WebService;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            //sqlConnection.Close();
            //}

            //log.SpEnd(ExecParam, _ret);
            return _ret;

        }

        public Result GetAll(ExecParam ExecParam, KRT_Log_WebServiceSearch _KRT_Log_WebServiceSearch)
        {
            //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_WebServiceGetAll");

            Result _ret = new Result();

            //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
            //{
            //sqlConnection.Open();

            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_KRT_Log_WebServiceSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _KRT_Log_WebServiceSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_WebServiceGetAll]");
                SqlComm.CommandType = CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_Log_WebServiceSearch.OrderBy, _KRT_Log_WebServiceSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            //sqlConnection.Close();
            //}

            //log.SpEnd(ExecParam, _ret);
            return _ret;

        }

        public Result Delete(ExecParam ExecParam)
        {
            return Delete(ExecParam, DateTime.Now);
        }

        public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
        {
            //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_WebServiceDelete");

            Result _ret = new Result();

            //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
            //{
            //sqlConnection.Open();

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_WebServiceDelete]");

            try
            {
                SqlComm.CommandType = CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                SqlComm.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            //sqlConnection.Close();
            //}

            //log.SpEnd(ExecParam, _ret);
            return _ret;
        }

    }

    //[Transaction(TransactionOption.Disabled)]
    public partial class KRT_Log_StoredProcedureStoredProcedure
    {
        private DataContext dataContext;

        internal static Dictionary<String, Parameter> sp_KRT_Log_StoredProcedureInsertParameters = null;
        internal static Dictionary<String, Parameter> sp_KRT_Log_StoredProcedureUpdateParameters = null;

        public KRT_Log_StoredProcedureStoredProcedure(DataContext _dataContext)
        {
            this.dataContext = _dataContext;
        }

        public Result Insert(String Method, ExecParam ExecParam, KRT_Log_StoredProcedure Record)
        {
            return Insert(Method, ExecParam, Record, DateTime.Now);
        }

        public Result Insert(String Method, ExecParam ExecParam, KRT_Log_StoredProcedure Record, DateTime ExecutionTime)
        {
            //Contentum.eUtility.Log log = new Contentum.eUtility.Log();

            Result _ret = new Result();

            //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
            //{
            //sqlConnection.Open();

            SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
            if (Method == "") Method = "Insert";
            switch (Method)
            {
                case Constants.Insert:
                    //log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_StoredProcedureInsert");
                    Record.Base.Updated.LetrehozasIdo = true;
                    Record.Base.Updated.Letrehozo_id = true;
                    Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                    Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;

                    // TranzId-ba betessz�k a Page_Id-t
                    Record.Base.Tranz_id = ExecParam.Page_Id;

                    SqlComm = new SqlCommand("[sp_KRT_Log_StoredProcedureInsert]");
                    if (sp_KRT_Log_StoredProcedureInsertParameters == null)
                    {
                        //sp_KRT_Log_StoredProcedureInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_Log_StoredProcedureInsert]");
                        //sp_KRT_Log_StoredProcedureInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_Log_StoredProcedureInsert]");
                        sp_KRT_Log_StoredProcedureInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Log_StoredProcedureInsert]");
                    }

                    Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_Log_StoredProcedureInsertParameters);

                    Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

                    break;
                case Constants.Update:
                    //log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_StoredProcedureUpdate");
                    Record.Base.Updated.ModositasIdo = true;
                    Record.Base.Updated.Modosito_id = true;
                    Record.Base.ModositasIdo = ExecutionTime.ToString();
                    Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                    SqlComm = new SqlCommand("[sp_KRT_Log_StoredProcedureUpdate]");
                    if (sp_KRT_Log_StoredProcedureUpdateParameters == null)
                    {
                        //sp_KRT_Log_StoredProcedureUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_Log_StoredProcedureUpdate]");
                        //sp_KRT_Log_StoredProcedureUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_Log_StoredProcedureUpdate]");
                        sp_KRT_Log_StoredProcedureUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Log_StoredProcedureUpdate]");
                    }

                    Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_Log_StoredProcedureUpdateParameters);

                    Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                    break;
            }

            SqlComm.CommandType = CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            try
            {
                SqlComm.ExecuteNonQuery();
                if (Method == Constants.Insert)
                {
                    _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
                }
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            //sqlConnection.Close();
            //}        

            //log.SpEnd(ExecParam, _ret);

            return _ret;
        }

        public Result Get(ExecParam ExecParam)
        {
            //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam,"sp_KRT_Log_StoredProcedureGet");

            Result _ret = new Result();

            //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
            //{
            //    sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_StoredProcedureGet]");
                SqlComm.CommandType = CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

                KRT_Log_StoredProcedure _KRT_Log_StoredProcedure = new KRT_Log_StoredProcedure();
                Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Log_StoredProcedure, SqlComm);
                _ret.Record = _KRT_Log_StoredProcedure;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            //sqlConnection.Close();
            //}

            //log.SpEnd(ExecParam, _ret);
            return _ret;

        }

        public Result GetAll(ExecParam ExecParam, KRT_Log_StoredProcedureSearch _KRT_Log_StoredProcedureSearch)
        {
            //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_StoredProcedureGetAll");

            Result _ret = new Result();

            //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
            //{
            //sqlConnection.Open();

            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_KRT_Log_StoredProcedureSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _KRT_Log_StoredProcedureSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_StoredProcedureGetAll]");
                SqlComm.CommandType = CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_Log_StoredProcedureSearch.OrderBy, _KRT_Log_StoredProcedureSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            //sqlConnection.Close();
            //}

            //log.SpEnd(ExecParam, _ret);
            return _ret;

        }

        public Result Delete(ExecParam ExecParam)
        {
            return Delete(ExecParam, DateTime.Now);
        }

        public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
        {
            //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_StoredProcedureDelete");

            Result _ret = new Result();

            //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
            //{
            //sqlConnection.Open();

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_StoredProcedureDelete]");

            try
            {
                SqlComm.CommandType = CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                SqlComm.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            //sqlConnection.Close();
            //}

            //log.SpEnd(ExecParam, _ret);
            return _ret;
        }

    }

    /// <summary>
    /// Page loggol�sa
    /// </summary>
    public partial class KRT_Log_PageStoredProcedure
    {
        private DataContext dataContext;

        internal static Dictionary<String, Parameter> sp_KRT_Log_PageInsertParameters = null;
        internal static Dictionary<String, Parameter> sp_KRT_Log_PageUpdateParameters = null;

        public KRT_Log_PageStoredProcedure(DataContext _dataContext)
        {
            this.dataContext = _dataContext;

            //ConnectionString = Database.GetConnectionString(App);        
            //try
            //{
            //Connection = Database.Connect(App);
            //}
            //catch (Exception e)
            //{
            //throw e;
            //}
        }

        public Result Insert(String Method, ExecParam ExecParam, KRT_Log_Page Record)
        {
            return Insert(Method, ExecParam, Record, DateTime.Now);
        }

        public Result Insert(String Method, ExecParam ExecParam, KRT_Log_Page Record, DateTime ExecutionTime)
        {
            //Contentum.eUtility.Log log = new Contentum.eUtility.Log();

            Result _ret = new Result();

            //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
            //{
            //sqlConnection.Open();

            SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
            if (Method == "") Method = "Insert";
            switch (Method)
            {
                case Constants.Insert:
                    //log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_PageInsert");
                    Record.Base.Updated.LetrehozasIdo = true;
                    Record.Base.Updated.Letrehozo_id = true;
                    Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                    Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                    SqlComm = new SqlCommand("[sp_KRT_Log_PageInsert]");
                    if (sp_KRT_Log_PageInsertParameters == null)
                    {
                        //sp_KRT_Log_PageInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_Log_PageInsert]");
                        //sp_KRT_Log_PageInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_Log_PageInsert]");
                        sp_KRT_Log_PageInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Log_PageInsert]");
                    }

                    Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_Log_PageInsertParameters);

                    Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

                    break;
                case Constants.Update:
                    //log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_PageUpdate");
                    Record.Base.Updated.ModositasIdo = true;
                    Record.Base.Updated.Modosito_id = true;
                    Record.Base.ModositasIdo = ExecutionTime.ToString();
                    Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;

                    // TranzId-ba betessz�k a Page_Id-t
                    Record.Base.Tranz_id = ExecParam.Page_Id;

                    SqlComm = new SqlCommand("[sp_KRT_Log_PageUpdate]");
                    if (sp_KRT_Log_PageUpdateParameters == null)
                    {
                        //sp_KRT_Log_PageUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_KRT_Log_PageUpdate]");
                        //sp_KRT_Log_PageUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_KRT_Log_PageUpdate]");
                        sp_KRT_Log_PageUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KRT_Log_PageUpdate]");
                    }

                    Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KRT_Log_PageUpdateParameters);

                    Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                    break;
            }

            SqlComm.CommandType = CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            try
            {
                SqlComm.ExecuteNonQuery();
                if (Method == Constants.Insert)
                {
                    _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
                }
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            //sqlConnection.Close();
            //}        

            //log.SpEnd(ExecParam, _ret);

            return _ret;
        }

        public Result Get(ExecParam ExecParam)
        {
            //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam,"sp_KRT_Log_PageGet");

            Result _ret = new Result();

            //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
            //{
            //    sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_PageGet]");
                SqlComm.CommandType = CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

                KRT_Log_Page _KRT_Log_Page = new KRT_Log_Page();
                Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Log_Page, SqlComm);
                _ret.Record = _KRT_Log_Page;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            //sqlConnection.Close();
            //}

            //log.SpEnd(ExecParam, _ret);
            return _ret;

        }

        public Result GetAll(ExecParam ExecParam, KRT_Log_PageSearch _KRT_Log_PageSearch)
        {
            //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_PageGetAll");

            Result _ret = new Result();

            //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
            //{
            //sqlConnection.Open();

            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_KRT_Log_PageSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _KRT_Log_PageSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_PageGetAll]");
                SqlComm.CommandType = CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_Log_PageSearch.OrderBy, _KRT_Log_PageSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            //sqlConnection.Close();
            //}

            //log.SpEnd(ExecParam, _ret);
            return _ret;

        }

        public Result Delete(ExecParam ExecParam)
        {
            return Delete(ExecParam, DateTime.Now);
        }

        public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
        {
            //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Log_PageDelete");

            Result _ret = new Result();

            //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
            //{
            //sqlConnection.Open();

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Log_PageDelete]");

            try
            {
                SqlComm.CommandType = CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                SqlComm.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            //sqlConnection.Close();
            //}

            //log.SpEnd(ExecParam, _ret);
            return _ret;
        }
    }


}

namespace Contentum.eUtility
{
    public class Log
    {
        #region konstansok

        public const string AjaxCall = "AJAX Client Side WebService Calling";

        #endregion

        #region priv�t �s statikus v�ltoz�k

        //webservice
        protected string currentId = String.Empty;
        protected int currentLevel = -1;
        
        //webservice
        KRT_Log_WebService wsData;

        //storedprocedure
        KRT_Log_StoredProcedure spData;

        //page
        protected Page page;
        protected HttpRequest request;

        //loggol�s
        //protected static int logginglevel = levelSettings.Null;

        #endregion

        #region konstruktor

        public Log()
        {
        }

        public Log(Page page)
        {
            if (IsLogEnabled)
            {
                if (page != null)
                {
                    this.page = page;
                    PageStart();
                    this.page.Unload += new EventHandler(page_Unload);
                }
            }
        }

        #endregion

        #region level

        //protected static void InitlevelSetting()
        //{
        //    if (logginglevel == levelSettings.Null)
        //    {
        //        string s = UI.GetAppSetting("LoggingThreshold");
        //        //ha nics a webconfigban be�ll�tva a loggol�s, alapb�l ki lesz kapcsolva
        //        if (String.IsNullOrEmpty(s))
        //        {
        //            logginglevel = levelSettings.Off;
        //        }
        //        else
        //        {
        //            int levelNum;
        //            if (Int32.TryParse(s, out levelNum))
        //            {
        //                if (-1 < levelNum)
        //                {
        //                    logginglevel = levelNum;
        //                }
        //                else
        //                {
        //                    logginglevel = levelSettings.Off;
        //                }
        //            }
        //            else
        //            {
        //                logginglevel = levelSettings.Off;
        //            }
        //        }
        //    }
        //}

        //protected static Boolean IsRecordLoggable(Level level)
        //{
        //    InitlevelSetting();

        //    if (logginglevel == levelSettings.Off)
        //    {
        //        return false;
        //    }

        //    int levelNum;

        //    if (Int32.TryParse(level, out levelNum))
        //    {
        //        if (levelNum >= logginglevel)
        //        {
        //            return true;
        //        }
        //    }

        //    return false;


        //}

        protected static bool IsLogEnabled
        {
            get
            {
                if (LogManager.GetRepository().Threshold == Level.Off)
                    return false;
                return true;
            }
        }

        protected static bool IsLoginLogEnabled()
        {
            return true;
        }

        protected static bool IsPageLogEnabled()
        {
            return true;
        }

        protected static bool IsWsLogEnabled()
        {
            return true;
        }

        protected static bool IsSpLogEnabled()
        {
            return true;
        }

        public static class Severity
        {
            public const string Information = "10";
            public const string Warning = "20";
            public const string Error = "30";

            public static string GetSeverity(string value)
            {
                if (!String.IsNullOrEmpty(value))
                {
                    if (value == Information || value == Warning || value == Error)
                    {
                        return value;
                    }
                    else
                    {
                        return Information;
                    }
                }

                return Information;

            }

        }

        //protected static class levelSettings
        //{
        //    public const int Null = -100;
        //    public const int Off = -1;
        //    public const int Information = 10;
        //    public const int Warning = 20;
        //    public const int Error = 30;
        //}

        public static bool TryParseStringAsLevel(string value, out Level level)
        {
            level = null;
            if (String.IsNullOrEmpty(value))
                return false;

            if (String.Compare(value, Level.Info.Name, true) == 0)
            {
                level = Level.Info;
                return true;
            }
            if (String.Compare(value, Level.Warn.Name, true) == 0)
            {
                level = Level.Warn;
                return true;
            }
            if (String.Compare(value, Level.Error.Name, true) == 0)
            {
                level = Level.Error;
                return true;
            }
            if (String.Compare(value, Level.Severe.Name, true) == 0)
            {
                level = Level.Severe;
                return true;
            }
            if (String.Compare(value, Level.Debug.Name, true) == 0)
            {
                level = Level.Debug;
                return true;
            }
            if (String.Compare(value, Level.Fatal.Name, true) == 0)
            {
                level = Level.Fatal;
                return true;
            }
            if (String.Compare(value, Level.Alert.Name, true) == 0)
            {
                level = Level.Alert;
                return true;
            }
            if (String.Compare(value, Level.All.Name, true) == 0)
            {
                level = Level.All;
                return true;
            }
            if (String.Compare(value, Level.Critical.Name, true) == 0)
            {
                level = Level.Critical;
                return true;
            }
            if (String.Compare(value, Level.Emergency.Name, true) == 0)
            {
                level = Level.Emergency;
                return true;
            }
            if (String.Compare(value, Level.Fine.Name, true) == 0)
            {
                level = Level.Fine;
                return true;
            }
            if (String.Compare(value, Level.Finer.Name, true) == 0)
            {
                level = Level.Finer;
                return true;
            }
            if (String.Compare(value, Level.Finest.Name, true) == 0)
            {
                level = Level.Finest;
                return true;
            }
            if (String.Compare(value, Level.Notice.Name, true) == 0)
            {
                level = Level.Notice;
                return true;
            }
            if (String.Compare(value, Level.Off.Name, true) == 0)
            {
                level = Level.Off;
                return true;
            }
            if (String.Compare(value, Level.Trace.Name, true) == 0)
            {
                level = Level.Trace;
                return true;
            }
            if (String.Compare(value, Level.Verbose.Name, true) == 0)
            {
                level = Level.Verbose;
                return true;
            }

            return false;

        }

        #endregion

        #region Status

        public static class Status
        {
            public const string Elindult = "0";
            public const string Fut = "1";
            public const string KliensElindult = "4";
            public const string KliensFut = "5";
            public const string KliensLefutott = "6";
            public const string Lefutott = "9";
        }

        #endregion

        #region Time

        //pillanatnyi id� lek�r�se
        public static string Time
        {
            get
            {
                return DateTimeToString(DateTime.Now);
            }
        }

        private static string DateTimeToString(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH:mm:ss.fff");
        }

        public static string GetStaticDateID()
        {
            int randomMillisec = GetRandom();
            return DateTimeToString(DateTime.Now.Add(new TimeSpan(0, 0, 0, 0, randomMillisec)));
        }

        private static int GetRandom()
        {
            Random r = new Random((int)DateTime.Now.Ticks + System.Threading.Thread.CurrentThread.ManagedThreadId);
            int random = r.Next(0,10);
            return random;
        }

        //private int randomMillisec = -1;
        //private DateTimePrecise dateTimePrecise = new DateTimePrecise(10);

        public string GetDateID()
        {
            if (HttpContext.Current.Items["DateTimePrecise"] == null)
            {
                HttpContext.Current.Items["DateTimePrecise"] = new DateTimePrecise(10);
            }
            DateTimePrecise dateTimePrecise = (DateTimePrecise)HttpContext.Current.Items["DateTimePrecise"];
            if (HttpContext.Current.Items["RandomMillisec"] == null)
            {
                HttpContext.Current.Items["RandomMillisec"] = GetRandom();
            }
            int randomMillisec = (int)HttpContext.Current.Items["RandomMillisec"];
            return DateTimeToString(dateTimePrecise.Now.Add(new TimeSpan(0, 0, 0, 0, randomMillisec)));
        }

        #endregion

        //loggol�si bejegyz�sek adatb�zisba ir�s�ra szolg�l� r�teg
        private static class Add
        {
            private static void SetLevelFromResult(Result result, ref Level level)
            {
                if (!String.IsNullOrEmpty(result.ErrorMessage) && !String.IsNullOrEmpty(result.ErrorCode))
                {
                    int errorNumber = ResultError.GetErrorNumberFromResult(result);
                    //Nem mi dobtuk a hib�t
                    if (errorNumber == -1 || errorNumber < 50000)
                    {
                        level = Level.Severe;
                    }
                }
            }

            #region webservice h�v�sok

            private static void FillLoggingEventFromBusinessObject(LoggingEvent loggingEvent, KRT_Log_Login krtLogin)
            {
                try
                {
                    loggingEvent.Properties["Date"] = krtLogin.Typed.Date;
                    loggingEvent.Properties["Status"] = krtLogin.Typed.Status;
                    loggingEvent.Properties["StartDate"] = krtLogin.Typed.StartDate;
                    loggingEvent.Properties["Felhasznalo_Id"] = krtLogin.Typed.Felhasznalo_Id;
                    loggingEvent.Properties["Felhasznalo_Nev"] = krtLogin.Typed.Felhasznalo_Nev;
                    loggingEvent.Properties["CsoportTag_Id"] = krtLogin.Typed.CsoportTag_Id;
                    loggingEvent.Properties["Helyettesito_Id"] = krtLogin.Typed.Helyettesito_Id;
                    loggingEvent.Properties["Helyettesito_Nev"] = krtLogin.Typed.Helyettesito_Nev;
                    loggingEvent.Properties["Helyettesites_Id"] = krtLogin.Typed.Helyettesites_Id;
                    loggingEvent.Properties["Helyettesites_Mod"] = krtLogin.Typed.Helyettesites_Mod;
                    loggingEvent.Properties["LoginType"] = krtLogin.Typed.LoginType;
                    loggingEvent.Properties["Letrehozo_id"] = krtLogin.Base.Typed.Letrehozo_id;
                    loggingEvent.Properties["Tranz_id"] = krtLogin.Base.Typed.Tranz_id;
                    loggingEvent.Properties["HibaKod"] = krtLogin.Typed.HibaKod;
                    loggingEvent.Properties["HibaUzenet"] = krtLogin.Typed.HibaUzenet;
                    loggingEvent.Properties["UserHostAddress"] = krtLogin.Typed.UserHostAddress;
                }
                catch(Exception ex)
                {
                    Result res = ResultException.GetResultFromException(ex);
                    LogError(res);
                }
            }

            private static void FillLoggingEventFromBusinessObject(LoggingEvent loggingEvent, KRT_Log_Page krtPage)
            {
                try
                {
                    loggingEvent.Properties["Date"] = krtPage.Typed.Date;
                    loggingEvent.Properties["Status"] = krtPage.Typed.Status;
                    loggingEvent.Properties["StartDate"] = krtPage.Typed.StartDate;
                    loggingEvent.Properties["Modul_Id"] = krtPage.Typed.Modul_Id;
                    loggingEvent.Properties["Login_Tranz_id"] = krtPage.Typed.Login_Tranz_id;
                    loggingEvent.Properties["Name"] = krtPage.Typed.Name;
                    loggingEvent.Properties["Url"] = krtPage.Typed.Url;
                    loggingEvent.Properties["QueryString"] = krtPage.Typed.QueryString;
                    loggingEvent.Properties["Command"] = krtPage.Typed.Command;
                    loggingEvent.Properties["IsPostBack"] = krtPage.Typed.IsPostBack;
                    loggingEvent.Properties["IsAsync"] = krtPage.Typed.IsAsync;
                    loggingEvent.Properties["Letrehozo_id"] = krtPage.Base.Typed.Letrehozo_id;
                    loggingEvent.Properties["Tranz_id"] = krtPage.Base.Typed.Tranz_id;
                    loggingEvent.Properties["HibaKod"] = krtPage.Typed.HibaKod;
                    loggingEvent.Properties["HibaUzenet"] = krtPage.Typed.HibaUzenet;
                }
                catch(Exception ex)
                {
                    Result res = ResultException.GetResultFromException(ex);
                    LogError(res);
                }
            }

            private static void FillLoggingEventFromBusinessObject(LoggingEvent loggingEvent, KRT_Log_WebService krtWebService)
            {
                try
                {
                    loggingEvent.Properties["Date"] = krtWebService.Typed.Date;
                    loggingEvent.Properties["Status"] = krtWebService.Typed.Status;
                    loggingEvent.Properties["StartDate"] = krtWebService.Typed.StartDate;
                    loggingEvent.Properties["ParentWS_StartDate"] = krtWebService.Typed.ParentWS_StartDate;
                    loggingEvent.Properties["Machine"] = krtWebService.Typed.Machine;
                    loggingEvent.Properties["Url"] = krtWebService.Typed.Url;
                    loggingEvent.Properties["Name"] = krtWebService.Typed.Name;
                    loggingEvent.Properties["Method"] = krtWebService.Typed.Method;
                    loggingEvent.Properties["Letrehozo_id"] = krtWebService.Base.Typed.Letrehozo_id;
                    loggingEvent.Properties["Tranz_id"] = krtWebService.Base.Typed.Tranz_id;
                    loggingEvent.Properties["HibaKod"] = krtWebService.Typed.HibaKod;
                    loggingEvent.Properties["HibaUzenet"] = krtWebService.Typed.HibaUzenet;
                }
                catch (Exception ex)
                {
                    Result res = ResultException.GetResultFromException(ex);
                    LogError(res);
                }
            }

            private static void FillLoggingEventFromBusinessObject(LoggingEvent loggingEvent, KRT_Log_StoredProcedure krtStoredProcedure)
            {
                try
                {
                    loggingEvent.Properties["Date"] = krtStoredProcedure.Typed.Date;
                    loggingEvent.Properties["Status"] = krtStoredProcedure.Typed.Status;
                    loggingEvent.Properties["StartDate"] = krtStoredProcedure.Typed.StartDate;
                    loggingEvent.Properties["WS_StartDate"] = krtStoredProcedure.Typed.WS_StartDate;
                    loggingEvent.Properties["Name"] = krtStoredProcedure.Typed.Name;
                    loggingEvent.Properties["Letrehozo_id"] = krtStoredProcedure.Base.Typed.Letrehozo_id;
                    loggingEvent.Properties["Tranz_id"] = krtStoredProcedure.Base.Typed.Tranz_id;
                    loggingEvent.Properties["HibaKod"] = krtStoredProcedure.Typed.HibaKod;
                    loggingEvent.Properties["HibaUzenet"] = krtStoredProcedure.Typed.HibaUzenet;
                }
                catch (Exception ex)
                {
                    Result res = ResultException.GetResultFromException(ex);
                    LogError(res);
                }
            }

            private static void LogBusinessObject(KRT_Log_Login krtLogin, Level level)
            {
                Logger.Start(level, "AuditLog - Login");
                StringBuilder sb = new StringBuilder();
                try
                {
                    sb.AppendLine(String.Format("Date: {0} - Status: {1} - StartDate :{2}", krtLogin.Date, krtLogin.Status, krtLogin.StartDate));
                    sb.Append(String.Format("Felhaszn�l�: {0},{1}", krtLogin.Felhasznalo_Nev, krtLogin.Felhasznalo_Id));
                    sb.AppendLine(String.Format("; Felhaszn�l� csoporttags�g: {0}", krtLogin.CsoportTag_Id));
                    sb.Append(String.Format("Helyettes�t�: {0},{1}", krtLogin.Helyettesito_Nev, krtLogin.Helyettesito_Id));
                    sb.AppendLine(String.Format("; Helyettes�t�s: {0}, Helyettes�t�si m�d: {1}", krtLogin.Helyettesites_Id, krtLogin.Helyettesites_Mod));
                    sb.AppendLine(String.Format("Bejelentkez�s t�pusa: {0}", krtLogin.LoginType));
                    sb.AppendLine(String.Format("HibaK�d: {0}, Hiba�zenet: {1}", krtLogin.HibaKod, krtLogin.HibaUzenet));
                }
                catch (Exception ex)
                {
                    Result res = ResultException.GetResultFromException(ex);
                    LogError(res);
                }
                Logger.Log(level, sb.ToString());
                Logger.End(level, "AuditLog - Login");
            }

            private static void LogBusinessObject(KRT_Log_Page krtPage, Level level)
            {
                Logger.Start(level, "AuditLog - Page");
                StringBuilder sb = new StringBuilder();
                try
                {
                    sb.AppendLine(String.Format("Date: {0} - Status: {1} - StartDate :{2}", krtPage.Date, krtPage.Status, krtPage.StartDate));
                    sb.AppendLine(String.Format("Oldal neve: {0}, ModulId: {1}", krtPage.Name, krtPage.Modul_Id));
                    sb.Append(String.Format("Oldal Url-je: {0}", krtPage.Url));
                    sb.Append(String.Format("; Query string: {0}", krtPage.QueryString));
                    sb.Append(String.Format("; Command: {0}", krtPage.Command));
                    sb.AppendLine(String.Format("; Postback: {0}, Async: {1}", krtPage.IsPostBack, krtPage.IsAsync));
                    sb.AppendLine(String.Format("HibaK�d: {0}, Hiba�zenet: {1}", krtPage.HibaKod, krtPage.HibaUzenet));
                }
                catch (Exception ex)
                {
                    Result res = ResultException.GetResultFromException(ex);
                    LogError(res);
                }
                Logger.Log(level, sb.ToString());
                Logger.End(level, "AuditLog - Page");
            }

            private static void LogBusinessObject(KRT_Log_WebService krtWebService, Level level)
            {
                Logger.Start(level, "AuditLog - WebService");
                StringBuilder sb = new StringBuilder();
                try
                {
                    sb.AppendLine(String.Format("Date: {0} - Status: {1} - StartDate :{2}", krtWebService.Date, krtWebService.Status, krtWebService.StartDate));
                    sb.AppendLine(String.Format("H�v�: {0}", krtWebService.ParentWS_StartDate));
                    sb.Append(String.Format("Machine: {0}", krtWebService.Machine));
                    sb.AppendLine(String.Format("; Webservice url-je: {0}", krtWebService.Url));
                    sb.Append(String.Format("Webservice neve: {0}", krtWebService.Name));
                    sb.AppendLine(String.Format("; Met�dus neve: {0}", krtWebService.Method));
                    sb.AppendLine(String.Format("HibaK�d: {0}, Hiba�zenet: {1}", krtWebService.HibaKod, krtWebService.HibaUzenet));
                }
                catch (Exception ex)
                {
                    Result res = ResultException.GetResultFromException(ex);
                    LogError(res);
                }
                Logger.Log(level, sb.ToString());
                Logger.End(level, "AuditLog - WebService");
            }

            private static void LogBusinessObject(KRT_Log_StoredProcedure krtStoredProcedure, Level level)
            {
                Logger.Start(level, "AuditLog - StoredProcedure");
                StringBuilder sb = new StringBuilder();
                try
                {
                    sb.AppendLine(String.Format("Date: {0} - Status: {1} - StartDate :{2}", krtStoredProcedure.Date, krtStoredProcedure.Status, krtStoredProcedure.StartDate));
                    sb.Append(String.Format("H�v� webservice: {0}", krtStoredProcedure.WS_StartDate));
                    sb.AppendLine(String.Format("; T�rolt elj�r�s neve: {0}", krtStoredProcedure.Name));
                    sb.AppendLine(String.Format("HibaK�d: {0}, Hiba�zenet: {1}", krtStoredProcedure.HibaKod, krtStoredProcedure.HibaUzenet));
                }
                catch (Exception ex)
                {
                    Result res = ResultException.GetResultFromException(ex);
                    LogError(res);
                }
                Logger.Log(level, sb.ToString());
                Logger.End(level, "AuditLog - StoredProcedure");
            }

            //private static Result WriteOutLog(ExecParam execParam,Page page)
            //{
            //    LogService service = eAdminService.ServiceFactory.GetLogService();
            //    execParam.LogTemp.Page = page.Session[Constants.PageData] as KRT_Log_Page;
            //    Result res = service.WriteOutLog(execParam, execParam.LogTemp);

            //    if (String.IsNullOrEmpty(res.ErrorCode))
            //    {
            //        RemovePageData(page);
            //    }
            //    else
            //    {
            //        LogError(res);
            //    }

            //    return res;
            //}

            //private static Result WriteOutLog(ExecParam execParam)
            //{
            //    LogService service = eAdminService.ServiceFactory.GetLogService();
            //    Result res = service.WriteOutLog(execParam, execParam.LogTemp);

            //    if (String.IsNullOrEmpty(res.ErrorCode))
            //    {
            //        RemoveTempData(execParam);
            //    }
            //    else
            //    {
            //        LogError(res);
            //    }

            //    return res;
            //}

            private static void LogInLogInsert(Page page, KRT_Log_Login krtLogin, Level level)
            {
                ILog log;

                if(krtLogin.Status == Status.Elindult)
                {
                    log = Logger.AuditLog.Log_Login_Start;
                }
                else
                {
                    log = Logger.AuditLog.Log_Login;
                }

                if (log != null)
                {
                    log4net.Core.LoggingEvent loggingEvent = Logger.AuditLog.GetLoggingEvent(log, level);
                    FillLoggingEventFromBusinessObject(loggingEvent, krtLogin);
                    log.Logger.Log(loggingEvent);
                }
            }

            private static void LogInLogUpdate(Page page, KRT_Log_Login krtLogin, Level level)
            {
                string loginStartDate = UI.GetSession(page, Constants.LoginStartDate);
                if (!String.IsNullOrEmpty(loginStartDate))
                {
                    krtLogin.StartDate = loginStartDate;
                }

                string LoginTranzId = UI.GetSession(page, Constants.LoginTranzId);
                if (!String.IsNullOrEmpty(LoginTranzId))
                {
                    krtLogin.Base.Tranz_id = LoginTranzId;
                }

                ILog log = Logger.AuditLog.Log_Login;
                if (log != null)
                {
                    log4net.Core.LoggingEvent loggingEvent = Logger.AuditLog.GetLoggingEvent(log, level);
                    FillLoggingEventFromBusinessObject(loggingEvent, krtLogin);
                    log.Logger.Log(loggingEvent);
                }
            }

            private static void PageLogInsert(Page page, KRT_Log_Page krtPage, Level level)
            {
                ILog log;

                if (krtPage.Status == Status.Elindult)
                {
                    log = Logger.AuditLog.Log_Page_Start;
                }
                else
                {
                    log = Logger.AuditLog.Log_Page;
                }

                if (log != null)
                {
                    log4net.Core.LoggingEvent loggingEvent = Logger.AuditLog.GetLoggingEvent(log, level);
                    FillLoggingEventFromBusinessObject(loggingEvent, krtPage);
                    log.Logger.Log(loggingEvent);
                }
            }

            private static void PageLogUpdate(Page page, KRT_Log_Page krtPage, Level level)
            {
                string pageStartDate = UI.GetSession(page, Constants.PageStartDate);
                if (!String.IsNullOrEmpty(pageStartDate))
                {
                    krtPage.StartDate = pageStartDate;
                }

                string PageTranzId = UI.GetSession(page, Constants.PageTranzId);
                if (!String.IsNullOrEmpty(PageTranzId))
                {
                    krtPage.Base.Tranz_id = PageTranzId;
                }

                ILog log = Logger.AuditLog.Log_Page;
                if (log != null)
                {
                    log4net.Core.LoggingEvent loggingEvent = Logger.AuditLog.GetLoggingEvent(log, level);
                    FillLoggingEventFromBusinessObject(loggingEvent, krtPage);
                    log.Logger.Log(loggingEvent);
                }
            }

            private static void WsLogInsert(ExecParam execParam, KRT_Log_WebService krtWebService, Level level)
            {
                ILog log;

                if (krtWebService.Status == Status.Elindult)
                {
                    log = Logger.AuditLog.Log_WebService_Start;
                }
                else
                {
                    log = Logger.AuditLog.Log_WebService;
                }

                if (log != null)
                {
                    log4net.Core.LoggingEvent loggingEvent = Logger.AuditLog.GetLoggingEvent(log, level);
                    FillLoggingEventFromBusinessObject(loggingEvent, krtWebService);
                    log.Logger.Log(loggingEvent);
                }

            }

            private static void WsLogUpdate(ExecParam execParam, KRT_Log_WebService krtWebService, Level level)
            {
                string webServiceStartDate = GetPrevCallingDateID(execParam.CallingChain);
                if (!String.IsNullOrEmpty(webServiceStartDate))
                {
                    krtWebService.StartDate = webServiceStartDate;
                }

                ILog log = Logger.AuditLog.Log_WebService;
                if (log != null)
                {
                    log4net.Core.LoggingEvent loggingEvent = Logger.AuditLog.GetLoggingEvent(log, level);
                    FillLoggingEventFromBusinessObject(loggingEvent, krtWebService);
                    log.Logger.Log(loggingEvent);
                }
            }

            private static void SpLogInsert(ExecParam execParam, KRT_Log_StoredProcedure krtStoredProcedure, Level level)
            {
                ILog log;

                if (krtStoredProcedure.Status == Status.Elindult)
                {
                    log = Logger.AuditLog.Log_StoredProcedure_Start;
                }
                else
                {
                    log = Logger.AuditLog.Log_StoredProcedure;
                }

                if (log != null)
                {
                    log4net.Core.LoggingEvent loggingEvent = Logger.AuditLog.GetLoggingEvent(log, level);
                    FillLoggingEventFromBusinessObject(loggingEvent, krtStoredProcedure);
                    log.Logger.Log(loggingEvent);
                }
            }

            private static void SpLogUpdate(ExecParam execParam, KRT_Log_StoredProcedure krtStoredProcedure, Level level)
            {
                string storedProcedureStartDate = GetPrevCallingDateID(execParam.CallingChain);
                if (!String.IsNullOrEmpty(storedProcedureStartDate))
                {
                    krtStoredProcedure.StartDate = storedProcedureStartDate;
                }

                ILog log = Logger.AuditLog.Log_StoredProcedure;
                if (log != null)
                {
                    log4net.Core.LoggingEvent loggingEvent = Logger.AuditLog.GetLoggingEvent(log, level);
                    FillLoggingEventFromBusinessObject(loggingEvent, krtStoredProcedure);
                    log.Logger.Log(loggingEvent);
                }
            }

            #endregion

            #region LogIn

            public static KRT_Log_Login GetLoginBusinessObject(Page page, Contentum.eBusinessDocuments.Result result, string status)
            {
                KRT_Log_Login krtLogin = new KRT_Log_Login();
                try
                {
                    krtLogin.Date = GetStaticDateID();

                    if (String.IsNullOrEmpty(status))
                        krtLogin.Status = Status.Fut;
                    else
                        krtLogin.Status = status;

                    krtLogin.Felhasznalo_Nev = UI.GetSession(page, Constants.UserName);
                    krtLogin.Felhasznalo_Id = UI.GetSession(page, Constants.FelhasznaloId);

                    krtLogin.CsoportTag_Id = UI.GetSession(page, Constants.FelhasznaloCsoportTagId);

                    string loginUserId = UI.GetSession(page, Constants.LoginUserId);
                    if (loginUserId != krtLogin.Felhasznalo_Id)
                    {
                        // helyettes�t�s

                        krtLogin.Felhasznalo_Id = UI.GetSession(page, Constants.FelhasznaloId);
                        krtLogin.Felhasznalo_Nev = UI.GetSession(page, Constants.HelyettesitettNev);

                        krtLogin.Helyettesito_Id = loginUserId;
                        krtLogin.Helyettesito_Nev = UI.GetSession(page, Constants.LoginUserNev);

                        krtLogin.Helyettesites_Id = UI.GetSession(page, Constants.HelyettesitesId);

                        krtLogin.Helyettesites_Mod = UI.GetSession(page, Constants.HelyettesitesMod);
                    }

                    krtLogin.LoginType = UI.GetSession(page, Constants.LoginType);

                    krtLogin.UserHostAddress = page.Request.UserHostAddress;

                    if (result != null)
                    {
                        if (!String.IsNullOrEmpty(result.ErrorCode))
                            krtLogin.HibaKod = result.ErrorCode;
                        if (!String.IsNullOrEmpty(result.ErrorMessage))
                            krtLogin.HibaUzenet = result.ErrorMessage;
                    }

                    krtLogin.Base.Letrehozo_id = krtLogin.Felhasznalo_Id;
                    krtLogin.Base.Tranz_id = Guid.NewGuid().ToString();
                }
                catch(Exception e)
                {
                    Result res = ResultException.GetResultFromException(e);
                    LogError(res);
                }

                return krtLogin;
            }

            public static void LogIn(Page page)
            {
                KRT_Log_Login krtLogin = GetLoginBusinessObject(page, null,Status.Elindult);

                if (Logger.AuditLog.Log_Login_Start.IsInfoEnabled)
                {
                    LogInLogInsert(page, krtLogin, Level.Info);

                    #region Log4Net
                    if (Logger.IsInfoEnabled())
                    {
                        LogBusinessObject(krtLogin, Level.Info);
                    }
                    #endregion
                }

                page.Session[Constants.LoginTranzId] = krtLogin.Base.Tranz_id;
                if (!krtLogin.Typed.Date.IsNull)
                    page.Session[Constants.LoginStartDate] = DateTimeToString(krtLogin.Typed.Date.Value);
            }

            public static void LogOut(Page page)
            {
                if (Logger.AuditLog.Log_Login.IsInfoEnabled)
                {
                    KRT_Log_Login krtLogin = GetLoginBusinessObject(page, null, Status.Lefutott);

                    LogInLogUpdate(page, krtLogin, Level.Info);

                    #region Log4Net
                    if (Logger.IsInfoEnabled())
                    {
                        LogBusinessObject(krtLogin, Level.Info);
                    }
                    #endregion
                }

                page.Session.Remove(Constants.LoginTranzId);
                page.Session.Remove(Constants.LoginStartDate);   
            }

            public static void LogInError(Page page, Contentum.eBusinessDocuments.Result result, Level level)
            {
                //az oldal logj�ban is szerepeljen a hiba
                PageError(page,null, result, level);
                SetLevelFromResult(result, ref level);
                if (Logger.AuditLog.Log_Login.Logger.IsEnabledFor(level))
                {
                    KRT_Log_Login krtLogin = GetLoginBusinessObject(page, result, Status.Lefutott);

                    LogInLogInsert(page, krtLogin, level);

                    #region Log4Net
                    if (Logger.IsEnabled(level))
                    {
                        LogBusinessObject(krtLogin, level);
                    }
                    #endregion

                }

            }

            #endregion

            #region Page

            public static KRT_Log_Page GetPageBusinessObject(Page page, HttpRequest request, Contentum.eBusinessDocuments.Result result, string status)
            {
                KRT_Log_Page krtPage = new KRT_Log_Page();

                try
                {
                    krtPage.Date = GetStaticDateID();

                    if (String.IsNullOrEmpty(status))
                        krtPage.Status = Status.Fut;
                    else
                        krtPage.Status = status;

                    string LoginTranzId = UI.GetSession(page, Constants.LoginTranzId);
                    if (!String.IsNullOrEmpty(LoginTranzId))
                    {
                        krtPage.Login_Tranz_id = LoginTranzId;
                    }
                    //else
                    //{
                    //    //nincs id a session-ban: nem biztos, hogy hiba
                    //    Result resTemp = new Result();
                    //    resTemp.ErrorCode = "No LoginTranzId in Session";
                    //    resTemp.ErrorMessage = "No LoginTranzId in Session";
                    //    LogWarning(resTemp);
                    //}

                    if (result != null)
                    {
                        if (!String.IsNullOrEmpty(result.ErrorCode))
                            krtPage.HibaKod = result.ErrorCode;
                        if (!String.IsNullOrEmpty(result.ErrorMessage))
                            krtPage.HibaUzenet = result.ErrorMessage;
                    }

                    krtPage.Base.Letrehozo_id = UI.GetSession(page, Constants.FelhasznaloId);
                    krtPage.Base.Tranz_id = Guid.NewGuid().ToString();

                    if (request == null)
                        request = page.Request;

                    krtPage.Name = Path.GetFileName(request.Path);
                    //krtPage.ModulId t�rolt elj�r�s t�lti ki;
                    krtPage.IsPostBack = page.IsPostBack ? 1.ToString() : 0.ToString();
                    krtPage.Url = request.Url.ToString();
                    krtPage.QueryString = request.Url.Query;
                    krtPage.Command = request.QueryString.Get(QueryStringVars.Command) ?? Constants.BusinessDocument.nullString;

                    //aszinkron postback t�rol�sa
                    if (page.IsPostBack)
                    {
                        if (UI.IsAsyncPostBackRequest(request.Headers))
                        {
                            krtPage.IsAsync = 1.ToString();
                        }
                        else
                        {
                            krtPage.IsAsync = 0.ToString();
                        }
                    }
                    else
                    {
                        krtPage.IsAsync = 0.ToString();
                    }
                }
                catch (Exception ex)
                {
                    Result res = ResultException.GetResultFromException(ex);
                    LogError(res);
                }

                return krtPage;


            }

            public static void PageStart(Page page)
            {
                //Session takar�t�s
                page.Session.Remove(Constants.PageTranzId);
                page.Session.Remove(Constants.PageStartDate);

                if (PerformanceCounters.Enabled)
                {
                    PerformanceCounters.PageStarted();
                }

                KRT_Log_Page krtPage = GetPageBusinessObject(page, null, null, Status.Elindult);

                if (Logger.AuditLog.Log_Page_Start.IsInfoEnabled)
                {
                    PageLogInsert(page, krtPage, Level.Info);

                    #region Log4Net

                    if (Logger.IsInfoEnabled())
                    {
                        LogBusinessObject(krtPage, Level.Info);
                    }

                    #endregion
                }

                if (!krtPage.Typed.Date.IsNull)
                    page.Session[Constants.PageStartDate] = DateTimeToString(krtPage.Typed.Date.Value);
                page.Session[Constants.PageTranzId] = krtPage.Base.Tranz_id;

            }

            public static void PageEnd(Page page, HttpRequest request)
            {
                string pageStartDate = UI.GetSession(page, Constants.PageStartDate);
                string pageEndDate = String.Empty;

                if (Logger.AuditLog.Log_Page.IsInfoEnabled)
                {
                    KRT_Log_Page krtPage = GetPageBusinessObject(page, request, null, Status.Lefutott);
                    if (!krtPage.Typed.Date.IsNull)
                        pageEndDate = DateTimeToString(krtPage.Typed.Date.Value);
                    PageLogUpdate(page, krtPage, Level.Info);

                    #region Log4Net

                    if (Logger.IsInfoEnabled())
                    {
                        LogBusinessObject(krtPage, Level.Info);
                    }

                    #endregion
                }
                else
                {
                    if (PerformanceCounters.Enabled)
                    {
                        pageEndDate = GetStaticDateID();
                    }
                }

                //Session takar�t�sa
                page.Session.Remove(Constants.PageTranzId);
                page.Session.Remove(Constants.PageStartDate);

                if (PerformanceCounters.Enabled)
                {
                    PerformanceCounters.PageEnded(pageStartDate, pageEndDate);
                }
            }

            public static void PageError(Page page,HttpRequest request, Contentum.eBusinessDocuments.Result result, Level level)
            {
                SetLevelFromResult(result, ref level);
                if (Logger.AuditLog.Log_Page.Logger.IsEnabledFor(level))
                {
                    KRT_Log_Page krtPage = GetPageBusinessObject(page, request, result, Status.Fut);
                    PageLogUpdate(page, krtPage, level);
                    #region Log4Net

                    if (Logger.IsEnabled(level))
                    {
                        LogBusinessObject(krtPage, level);
                    }

                    #endregion
                }

                if (PerformanceCounters.Enabled)
                {
                    PerformanceCounters.PageErrored(level);
                }
            }

            public static void PageLog(KRT_Log_Page krtPage, Level level)
            {
                if (Logger.AuditLog.Log_Page.Logger.IsEnabledFor(level))
                {
                    PageLogInsert(null, krtPage, level);

                    #region Log4Net

                    if (Logger.IsEnabled(level))
                    {
                        LogBusinessObject(krtPage, level);
                    }

                    #endregion
                }

                if (PerformanceCounters.Enabled)
                {
                    PerformanceCounters.PageErrored(level);
                }
            }

            #endregion

            #region webservice

            public static void GetWebServiceBusinessObject(ExecParam execParam, KRT_Log_WebService krtWebService, Contentum.eBusinessDocuments.Result result, string status)
            {
                try
                {
                    if (String.IsNullOrEmpty(status))
                        krtWebService.Status = Status.Fut;
                    else
                        krtWebService.Status = status;

                    if (result != null)
                    {
                        if (!String.IsNullOrEmpty(result.ErrorCode))
                            krtWebService.HibaKod = result.ErrorCode;
                        if (!String.IsNullOrEmpty(result.ErrorMessage))
                            krtWebService.HibaUzenet = result.ErrorMessage;
                    }

                    //Ne legyenek azonos id�k
                    if (!krtWebService.Typed.ParentWS_StartDate.IsNull)
                    {
                        if (krtWebService.Typed.StartDate.IsNull)
                        {
                            if (krtWebService.Typed.ParentWS_StartDate == krtWebService.Typed.Date)
                            {
                                krtWebService.Typed.Date = krtWebService.Typed.Date.Value.AddMilliseconds(3);
                            }
                        }
                        else
                        {
                            if (krtWebService.Typed.StartDate > krtWebService.Typed.Date)
                            {
                                krtWebService.Typed.Date = krtWebService.Typed.StartDate;
                            }
                        }
                    }

                    krtWebService.Base.Letrehozo_id = execParam.Felhasznalo_Id;
                    krtWebService.Base.Tranz_id = execParam.Page_Id;
                }
                catch (Exception ex)
                {
                    Result res = ResultException.GetResultFromException(ex);
                    LogError(res);
                }
            }

            public static string WsStart(Contentum.eBusinessDocuments.ExecParam execParam, KRT_Log_WebService krtWebService)
            {
                if (PerformanceCounters.Enabled)
                {
                    PerformanceCounters.WebServiceStarted(HasWebserviceParent(krtWebService));
                }

                if (Logger.AuditLog.Log_WebService_Start.IsInfoEnabled)
                {
                    GetWebServiceBusinessObject(execParam, krtWebService, null, Status.Elindult);

                    WsLogInsert(execParam, krtWebService, Level.Info);

                    #region Log4Net

                    if (Logger.IsInfoEnabled())
                    {
                        LogBusinessObject(krtWebService, Level.Info);
                    }

                    #endregion
                }

                string startDate = String.Empty;
                if (!krtWebService.Typed.Date.IsNull)
                {
                    startDate = DateTimeToString(krtWebService.Typed.Date.Value);
                    execParam.CallingChain.Push(startDate);
                }

                return startDate;
            }

            public static void WsEnd(Contentum.eBusinessDocuments.ExecParam execParam, KRT_Log_WebService krtWebService)
            {
                if (Logger.AuditLog.Log_WebService.IsInfoEnabled)
                {
                    GetWebServiceBusinessObject(execParam, krtWebService, null, Status.Lefutott);
                    //if(krtWebService
                    WsLogUpdate(execParam, krtWebService, Level.Info);

                    #region Log4Net

                    if (Logger.IsInfoEnabled())
                    {
                        LogBusinessObject(krtWebService, Level.Info);
                    }

                    #endregion
                }

                if (PerformanceCounters.Enabled)
                {
                    string startDate = String.Empty;
                    startDate = GetPrevCallingDateID(execParam.CallingChain);
                    string endDate = String.Empty;
                    if (!krtWebService.Typed.Date.IsNull)
                    {
                        endDate = DateTimeToString(krtWebService.Typed.Date.Value);
                    }
                    PerformanceCounters.WebServiceEnded(startDate, endDate, HasWebserviceParent(krtWebService));
                }

                if (execParam.CallingChain.Count > 0)
                {
                    execParam.CallingChain.Pop();
                }
            }

            public static void WsError(Contentum.eBusinessDocuments.ExecParam execParam, KRT_Log_WebService krtWebService, Contentum.eBusinessDocuments.Result result, Level level)
            {
                SetLevelFromResult(result, ref level);
                if (Logger.AuditLog.Log_WebService.Logger.IsEnabledFor(level))
                {
                    GetWebServiceBusinessObject(execParam, krtWebService, result, Status.Fut);

                    WsLogUpdate(execParam, krtWebService, level);

                    #region Log4Net

                    if (Logger.IsEnabled(level))
                    {
                        LogBusinessObject(krtWebService, level);
                    }

                    #endregion
                }

                if (PerformanceCounters.Enabled)
                {
                    PerformanceCounters.WebserviceErrored(level, HasWebserviceParent(krtWebService));
                }
            }

            public static void WsEndError(Contentum.eBusinessDocuments.ExecParam execParam, KRT_Log_WebService krtWebService, Contentum.eBusinessDocuments.Result result, Level level)
            {
                SetLevelFromResult(result, ref level);
                if (Logger.AuditLog.Log_WebService.Logger.IsEnabledFor(level))
                {
                    GetWebServiceBusinessObject(execParam, krtWebService, result, Status.Lefutott);

                    WsLogUpdate(execParam, krtWebService, level);

                    #region Log4Net

                    if (Logger.IsEnabled(level))
                    {
                        LogBusinessObject(krtWebService, level);
                    }

                    #endregion
                }

                if (PerformanceCounters.Enabled)
                {
                    PerformanceCounters.WebserviceErrored(level, HasWebserviceParent(krtWebService));
                    string startDate = String.Empty;
                    startDate = GetPrevCallingDateID(execParam.CallingChain);
                    string endDate = String.Empty;
                    if (!krtWebService.Typed.Date.IsNull)
                    {
                        endDate = DateTimeToString(krtWebService.Typed.Date.Value);
                    }
                    PerformanceCounters.WebServiceEnded(startDate, endDate, HasWebserviceParent(krtWebService));
                }

                if (execParam.CallingChain.Count > 0)
                {
                    execParam.CallingChain.Pop();
                }
            }

            #endregion

            #region storedprocedure

            public static void GetStoredProcedureBusinessObject(ExecParam execParam, KRT_Log_StoredProcedure krtStoredProcedure, Result result, string status)
            {

                try
                {
                    if (String.IsNullOrEmpty(status))
                        krtStoredProcedure.Status = Status.Fut;
                    else
                        krtStoredProcedure.Status = status;

                    if (result != null)
                    {
                        if (!String.IsNullOrEmpty(result.ErrorCode))
                            krtStoredProcedure.HibaKod = result.ErrorCode;
                        if (!String.IsNullOrEmpty(result.ErrorMessage))
                            krtStoredProcedure.HibaUzenet = result.ErrorMessage;
                    }

                    //Ne legyenek azonos id�k
                    if (!krtStoredProcedure.Typed.WS_StartDate.IsNull)
                    {
                        if (krtStoredProcedure.Typed.StartDate.IsNull)
                        {
                            if (krtStoredProcedure.Typed.WS_StartDate == krtStoredProcedure.Typed.Date)
                            {
                                krtStoredProcedure.Typed.Date = krtStoredProcedure.Typed.Date.Value.AddMilliseconds(3);
                            }
                        }
                        else
                        {
                            if (krtStoredProcedure.Typed.StartDate > krtStoredProcedure.Typed.Date)
                            {
                                krtStoredProcedure.Typed.Date = krtStoredProcedure.Typed.StartDate;
                            }
                        }
                    }

                    krtStoredProcedure.Base.Letrehozo_id = execParam.Felhasznalo_Id;
                    krtStoredProcedure.Base.Tranz_id = execParam.Page_Id;
                }
                catch (Exception ex)
                {
                    Result res = ResultException.GetResultFromException(ex);
                    LogError(res);
                }
            }

            public static string SpStart(Contentum.eBusinessDocuments.ExecParam execParam, KRT_Log_StoredProcedure krtStoredProcedure)
            {

                if (Logger.AuditLog.Log_StoredProcedure_Start.IsInfoEnabled)
                {
                    GetStoredProcedureBusinessObject(execParam, krtStoredProcedure, null, Status.Elindult);

                    SpLogInsert(execParam, krtStoredProcedure, Level.Info);

                    #region Log4Net

                    if (Logger.IsInfoEnabled())
                    {
                        LogBusinessObject(krtStoredProcedure, Level.Info);
                    }

                    #endregion
                }

                string startDate = String.Empty;
                if (!krtStoredProcedure.Typed.Date.IsNull)
                {
                    startDate = DateTimeToString(krtStoredProcedure.Typed.Date.Value);
                    execParam.CallingChain.Push(startDate);
                }

                return startDate;

            }

            public static void SpEnd(Contentum.eBusinessDocuments.ExecParam execParam, KRT_Log_StoredProcedure krtStoredProcedure)
            {
                if (Logger.AuditLog.Log_StoredProcedure.IsInfoEnabled)
                {
                    GetStoredProcedureBusinessObject(execParam, krtStoredProcedure, null, Status.Lefutott);

                    SpLogUpdate(execParam, krtStoredProcedure, Level.Info);

                    #region Log4Net

                    if (Logger.IsInfoEnabled())
                    {
                        LogBusinessObject(krtStoredProcedure, Level.Info);
                    }

                    #endregion
                }

                if (execParam.CallingChain.Count > 0)
                {
                    execParam.CallingChain.Pop();
                }

                
            }

            public static void SpError(Contentum.eBusinessDocuments.ExecParam execParam, KRT_Log_StoredProcedure krtStoredProcedure, Contentum.eBusinessDocuments.Result result, Level level)
            {
                SetLevelFromResult(result, ref level);
                if (Logger.AuditLog.Log_StoredProcedure.Logger.IsEnabledFor(level))
                {
                    GetStoredProcedureBusinessObject(execParam, krtStoredProcedure, result, Status.Fut);

                    SpLogUpdate(execParam, krtStoredProcedure, level);

                    #region Log4Net

                    if (Logger.IsEnabled(level))
                    {
                        LogBusinessObject(krtStoredProcedure, level);
                    }

                    #endregion
                }
            }

            public static void SpEndError(Contentum.eBusinessDocuments.ExecParam execParam, KRT_Log_StoredProcedure krtStoredProcedure, Contentum.eBusinessDocuments.Result result, Level level)
            {
                SetLevelFromResult(result, ref level);
                if (Logger.AuditLog.Log_StoredProcedure.Logger.IsEnabledFor(level))
                {
                    GetStoredProcedureBusinessObject(execParam, krtStoredProcedure, result, Status.Lefutott);

                    SpLogUpdate(execParam, krtStoredProcedure, level);

                    #region Log4Net

                    if (Logger.IsEnabled(level))
                    {
                        LogBusinessObject(krtStoredProcedure, level);
                    }

                    #endregion
                }

                if (execParam.CallingChain.Count > 0)
                {
                    execParam.CallingChain.Pop();
                }
            }

            #endregion
        }

        //Error loggol�si f�ggv�nyek
        public static class Error
        {
            #region LogIn

            public static void LogIn(Page page, Contentum.eBusinessDocuments.Result result)
            {
                if (page == null || result == null) return;

                if (IsLogEnabled && IsLoginLogEnabled())
                {
                    if (result.ErrorType == Constants.ErrorType.Warning)
                    {
                        Warning.LogIn(page, result);
                    }
                    else
                    {
                        Add.LogInError(page, result, Level.Error);
                    }
                }
            }

            public static void LogIn(Page page, string ErrorCode, string ErrorMessage)
            {
                if (page == null || ErrorMessage == null || ErrorCode == null) return;

                if (IsLogEnabled && IsLoginLogEnabled())
                {
                    Result result = new Result();
                    result.ErrorCode = ErrorCode;
                    result.ErrorMessage = ErrorMessage;
                    Add.LogInError(page, result, Level.Error);
                }
            }

            public static void LogIn(Page page, string Error)
            {
                if (page == null || Error == null) return;

                if (IsLogEnabled && IsLoginLogEnabled())
                {
                    Result result = new Result();
                    //result.ErrorCode = Error;
                    result.ErrorMessage = Error;
                    Add.LogInError(page, result, Level.Error);
                }
            }

            #endregion

            #region Page

            public static void Page(Page page, Contentum.eBusinessDocuments.Result result)
            {
                if (page == null || result == null) return;

                if (IsLogEnabled && IsPageLogEnabled())
                {
                    if (result.ErrorType == Constants.ErrorType.Warning)
                    {
                        Warning.Page(page, result);
                    }
                    else
                    {
                        Add.PageError(page,null, result, Level.Error);
                    }

                }
            }

            public static void Page(Page page, string ErrorCode, string ErrorMessage)
            {
                if (page == null || ErrorMessage == null || ErrorCode == null) return;

                if (IsLogEnabled && IsPageLogEnabled())
                {
                    Result result = new Result();
                    result.ErrorCode = ErrorCode;
                    result.ErrorMessage = ErrorMessage;
                    Add.PageError(page,null, result, Level.Error);
                }
            }

            public static void Page(Page page, string Error)
            {
                if (page == null || Error == null) return;

                if (IsLogEnabled && IsPageLogEnabled())
                {
                    Result result = new Result();
                    //result.ErrorCode = Error;
                    result.ErrorMessage = Error;
                    Add.PageError(page,null, result, Level.Error);
                }
            }

            #endregion

        }

        //Warning loggol�si f�ggv�nyek
        public static class Warning
        {

            #region LogIn

            public static void LogIn(Page page, Contentum.eBusinessDocuments.Result result)
            {
                if (page == null || result == null) return;

                if (IsLogEnabled && IsLoginLogEnabled())
                {
                    Add.LogInError(page, result, Level.Warn);
                }
            }

            public static void LogIn(Page page, string ErrorCode, string ErrorMessage)
            {
                if (page == null || ErrorMessage == null || ErrorCode == null) return;

                if (IsLogEnabled && IsLoginLogEnabled())
                {
                    Result result = new Result();
                    result.ErrorCode = ErrorCode;
                    result.ErrorMessage = ErrorMessage;
                    Add.LogInError(page, result, Level.Warn);
                }
            }

            public static void LogIn(Page page, string Error)
            {
                if (page == null || Error == null) return;

                if (IsLogEnabled && IsLoginLogEnabled())
                {
                    Result result = new Result();
                    //result.ErrorCode = Error;
                    result.ErrorMessage = Error;
                    Add.LogInError(page, result, Level.Warn);
                }
            }

            #endregion

            #region Page

            public static void Page(Page page, Contentum.eBusinessDocuments.Result result)
            {
                if (page == null || result == null) return;

                if (IsLogEnabled && IsPageLogEnabled())
                {
                    Add.PageError(page,null, result, Level.Warn);
                }
            }

            public static void Page(Page page, string ErrorCode, string ErrorMessage)
            {
                if (page == null || ErrorMessage == null || ErrorCode == null) return;

                if (IsLogEnabled && IsPageLogEnabled())
                {
                    Result result = new Result();
                    result.ErrorCode = ErrorCode;
                    result.ErrorMessage = ErrorMessage;
                    Add.PageError(page,null, result, Level.Warn);
                }
            }

            public static void Page(Page page, string Error)
            {
                if (page == null || Error ==null) return;

                if (IsLogEnabled && IsPageLogEnabled())
                {
                    Result result = new Result();
                    //result.ErrorCode = Error;
                    result.ErrorMessage = Error;
                    Add.PageError(page,null, result, Level.Warn);
                }
            }

            #endregion
        }

        #region LogIn

        public static void LogIn(Page page)
        {
            // Ide jon a bejelentkezett felhasznalo loggolasa az adatbazisba (usernev, logintype, ido, kliens ip, stb...)
            if (page == null) return;

            if (IsLogEnabled && IsLoginLogEnabled())
            {
                Add.LogIn(page);
            }


        }

        public static void LogOut(Page page)
        {
            // Ide jon a bejelentkezett felhasznalo kijelentkezesenek loggolasa az adatbazisba (usernev, logintype, ido, kliens ip, stb...)
            if (page == null) return;

            if (IsLogEnabled && IsLoginLogEnabled())
            {
                Add.LogOut(page);
            }
        }

        #endregion

        #region Page

        public static string GetLogConnectionString()
        {
            ConnectionStringSettings conn = new ConnectionStringSettings();
            conn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["AuditLogConnectionString"];

            if (conn == null)
                return null;

            return conn.ConnectionString;
        }
        
        //page unload esem�nykezel�je
        protected void page_Unload(object sender, EventArgs e)
        {
            PageEnd();
        }
        //a page loggol�si met�dusai nem statikus form�ban
        public void PageStart()
        {
            if (IsLogEnabled && IsPageLogEnabled())
            {
                this.request = page.Request;
                Add.PageStart(page);
            }
        }

        public void PageEnd()
        {
            if (IsLogEnabled && IsPageLogEnabled())
            {
                Add.PageEnd(page, this.request);
            }
        }

        public void PageError(Contentum.eBusinessDocuments.Result result)
        {
            if (IsLogEnabled && IsPageLogEnabled())
            {
                if (result.ErrorType == Constants.ErrorType.Warning)
                {
                    PageWarning(result);
                }
                else
                {
                    Add.PageError(page, this.request, result, Level.Error);
                }

            }
        }

        public void PageWarning(Contentum.eBusinessDocuments.Result result)
        {
            if (IsLogEnabled && IsPageLogEnabled())
            {
                Add.PageError(page, this.request, result, Level.Warn);
            }
        }
        //a page loggol�si met�dusai statikus form�ban
        public static void PageStart(Page page)
        {
            if (page == null) return;

            if (IsLogEnabled && IsPageLogEnabled())
            {
                Add.PageStart(page);
            }
        }

        public static void PageEnd(Page page)
        {
            if (page == null) return;

            if (IsLogEnabled && IsPageLogEnabled())
            {
                Add.PageEnd(page, null);
            }
        }

        //a page log t�bl�j�hoz egy bejegyz�s hozz�ada�sa
        //Javascript hib�k loggol�s�ra
        public static void AddPageLog(KRT_Log_Page krtPage, Level level)
        {
            if (IsLogEnabled && IsPageLogEnabled())
            {
                Add.PageLog(krtPage, level);
            }
        }

        #endregion

        #region webservice

        /// <summary>
        /// A hiv�l�nc konzisztenci�j�nak ellen�rz�se, �s r�szben jav�t�sa
        /// </summary>
        /// <param name="chain"></param>
        /// <returns></returns>
        protected Boolean CheckCallingChain(CallingChain chain)
        {
            //A loggol�s indul�s�nal hiba t�r�nt, �s nem lettek kit�ltve az adatok
            if (currentLevel > -1 && !String.IsNullOrEmpty(currentId))
            {
                //a jelenlegi szint nem egyezik a h�vol�nc hossz�val
                if (currentLevel == chain.Count)
                {
                    //a h�vol�nc utols� �rt�ke megegyezik a jelenlegi id-val, ez a helyes m�k�d�s
                    if (chain.Peek() == currentId)
                    {
                        return true;
                    }
                    //ha nem egyezik meg a h�v�l�nc �rt�ke az id-val, az �rt�k t�rl�se, �s az id hozz�ad�sa
                    else
                    {
                        if (chain.Count > 0)
                            chain.Pop();

                        chain.Push(currentId);
                        return true;
                    }
                }
                else
                {
                    //ha nagyobb a h�v�l�nc hossza, l�nc cs�kkent�se �s a met�dus �jrah�v�sa
                    if (chain.Count > currentLevel)
                    {
                        if (chain.Count > 0)
                            chain.Pop();

                        return CheckCallingChain(chain);
                    }
                    //ha kisebb a h�v�l�nc hossza, szint �t�ll�t�sa
                    else
                    {
                        
                        if (chain.Count > 0 && chain.Peek() == currentId)
                        {
                            currentLevel = chain.Count;
                            return true;
                        }

                        chain.Push(currentId);
                        currentLevel = chain.Count;
                        return true;
                    }
                }
            }
            //�jra id-val kell besz�rni sor-t
            else
            {
                currentLevel = chain.Count + 1;
                return false;
            }

        }

        /// <summary>
        /// A hiv�l�nc utols� id-j�nak elk�r�se
        /// </summary>
        /// <param name="callingChain"></param>
        /// <returns></returns>
        protected static string GetPrevCallingDateID(CallingChain callingChain)
        {
            if (callingChain.Count > 0)
            {
                string prevId = callingChain.Peek();
                return prevId;
            }
            else
            {
                return String.Empty;
            }
        }

        protected static bool HasWebserviceParent(KRT_Log_WebService krtWebservice)
        {
            if (String.IsNullOrEmpty(krtWebservice.ParentWS_StartDate))
            {
                return false;
            }
            return true;
        }

        public void WsError(Contentum.eBusinessDocuments.ExecParam execParam, Contentum.eBusinessDocuments.Result result)
        {
            if (execParam == null || result == null) return;

            if (IsLogEnabled && IsWsLogEnabled())
            {
                if (CheckCallingChain(execParam.CallingChain))
                {
                    if (result.ErrorType == Constants.ErrorType.Warning)
                    {
                        WsWarning(execParam, result);
                    }
                    else
                    {
                        this.wsData.Date = this.GetDateID();
                        Add.WsError(execParam,this.wsData, result, Level.Error);
                    }   
                }
            }
        }

        public void WsWarning(Contentum.eBusinessDocuments.ExecParam execParam, Contentum.eBusinessDocuments.Result result)
        {
            if (execParam == null || result == null) return;

            if (IsLogEnabled && IsWsLogEnabled())
            {
                if (CheckCallingChain(execParam.CallingChain))
                {
                    this.wsData.Date = this.GetDateID();
                    Add.WsError(execParam,this.wsData, result, Level.Warn);
                }
            }
        }

        /// <summary>
        /// Logolja az indul�st �s visszaad egy Log objektumot
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="context"></param>
        /// <param name="serviceType"></param>
        /// <returns>Log</returns>
        public static Log WsStart(Contentum.eBusinessDocuments.ExecParam execParam, HttpContext context, Type serviceType)
        {
            if (execParam == null) return new Log();

            Log log = new Log();
            if (IsLogEnabled && IsWsLogEnabled())
            {
                log.wsData = new KRT_Log_WebService();
                log.wsData.Date = log.GetDateID();
                log.wsData.Url = context.Request.Url.ToString();
                log.wsData.Name = serviceType.Name;
                log.wsData.Method = new System.Diagnostics.StackTrace().GetFrame(1).GetMethod().Name;
                log.wsData.Machine = context.Server.MachineName;
                log.wsData.ParentWS_StartDate = GetPrevCallingDateID(execParam.CallingChain);

                string id = Add.WsStart(execParam, log.wsData);

                if (!String.IsNullOrEmpty(id))
                {
                    log.currentId = id;
                    log.currentLevel = execParam.CallingChain.Count;
                }
            }

            return log;
        }

        /// <summary>
        /// A WsStart ExecParam t�mbre, MultiInvalidate-n�l haszn�land�
        /// </summary>
        /// <param name="execParams"></param>
        /// <param name="context"></param>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public static Log WsStart(Contentum.eBusinessDocuments.ExecParam[] execParams, HttpContext context, Type serviceType)
        {
            if (execParams == null || execParams.Length == 0) return new Log();

            Log log = new Log();

            if (IsLogEnabled && IsWsLogEnabled())
            {
                ExecParam execParam = execParams[0];

                log = WsStart(execParam, context, serviceType);

                if (!String.IsNullOrEmpty(log.currentId))
                {
                    if (execParams.Length > 1)
                    {
                        for (int i = 1; i < execParams.Length; i++)
                        {
                            execParams[i].CallingChain.Push(log.currentId);
                        }
                    }
                }

            }

            return log;

        }

        /// <summary>
        /// Befejez�s �s hiba loggol�sa
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="result"></param>
        public void WsEnd(Contentum.eBusinessDocuments.ExecParam execParam, Contentum.eBusinessDocuments.Result result)
        {
            if (execParam == null || result == null) return;

            if (IsLogEnabled && IsWsLogEnabled())
            {
                if (CheckCallingChain(execParam.CallingChain))
                {
                    this.wsData.Date = this.GetDateID();
                    //nincs hibak�d
                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        Add.WsEnd(execParam, this.wsData);
                    }
                    //van hibak�d
                    else
                    {
                        if (result.ErrorType == Constants.ErrorType.Warning)
                        {
                            Add.WsEndError(execParam, this.wsData ,result, Level.Warn);
                        }
                        else
                        {
                            Add.WsEndError(execParam, this.wsData, result, Level.Error);
                        } 
                    }

                    //loggol�s v�ge ut�n az oszt�ly deinicializ�lt �llapotba ker�l
                    currentId = String.Empty;
                    currentLevel = -1;
                }
            }
        }

        /// <summary>
        /// Hibavizsg�lat n�lk�li WsEnd
        /// </summary>
        /// <param name="execParam"></param>
        public void WsEnd(Contentum.eBusinessDocuments.ExecParam execParam)
        {
            if (execParam == null) return;

            if (IsLogEnabled && IsWsLogEnabled())
            {
                if (CheckCallingChain(execParam.CallingChain))
                {
                    this.wsData.Date = this.GetDateID();
                    Add.WsEnd(execParam, this.wsData);

                    //loggol�s v�ge ut�n az oszt�ly deinicializ�lt �llapotba ker�l
                    currentId = String.Empty;
                    currentLevel = -1;
                }
            }
        }

        /// <summary>
        /// A WsEnd ExecParam t�mbre, MultiInvalidate-n�l haszn�land�
        /// </summary>
        /// <param name="execParams"></param>
        /// <param name="result"></param>
        public void WsEnd(Contentum.eBusinessDocuments.ExecParam[] execParams, Contentum.eBusinessDocuments.Result result)
        {
            if (execParams == null || result == null || execParams.Length == 0) return;

            if (IsLogEnabled && IsWsLogEnabled())
            {
                if (execParams.Length > 0)
                {
                    ExecParam execParam = execParams[0];
                    WsEnd(execParam, result);
                }

                if (execParams.Length > 1)
                {
                    for (int i = 1; i < execParams.Length; i++)
                    {
                        if (CheckCallingChain(execParams[i].CallingChain))
                        {
                            if (execParams[i].CallingChain.Count > 0)
                                execParams[i].CallingChain.Pop();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Hibavizsg�lat n�lk�li WsEnd
        /// </summary>
        /// <param name="execParams"></param>
        public void WsEnd(Contentum.eBusinessDocuments.ExecParam[] execParams)
        {
            if (execParams == null || execParams.Length == 0) return;

            if (IsLogEnabled && IsWsLogEnabled())
            {
                if (execParams.Length > 0)
                {
                    ExecParam execParam = execParams[0];
                    WsEnd(execParam);
                }

                if (execParams.Length > 1)
                {
                    for (int i = 1; i < execParams.Length; i++)
                    {
                        if (CheckCallingChain(execParams[i].CallingChain))
                        {
                            if (execParams[i].CallingChain.Count > 0)
                                execParams[i].CallingChain.Pop();
                        }
                    }
                }
            }
        }

        #endregion

        #region storedprocedure

        public void SpError(Contentum.eBusinessDocuments.ExecParam execParam, Contentum.eBusinessDocuments.Result result)
        {
            if (execParam == null || result == null) return;

            if (IsLogEnabled && IsSpLogEnabled())
            {
                if (CheckCallingChain(execParam.CallingChain))
                {
                    if (result.ErrorType == Constants.ErrorType.Warning)
                    {
                        SpWarning(execParam,result);
                    }
                    else
                    {
                        this.spData.Date = this.GetDateID();
                        Add.SpError(execParam,this.spData, result, Level.Error);
                    }   
                }
            }
        }

        public void SpWarning(Contentum.eBusinessDocuments.ExecParam execParam, Contentum.eBusinessDocuments.Result result)
        {
            if (execParam == null || result == null) return;

            if (IsLogEnabled && IsSpLogEnabled())
            {
                if (CheckCallingChain(execParam.CallingChain))
                {
                    this.spData.Date = this.GetDateID();
                    Add.SpError(execParam, this.spData, result, Level.Warn);
                }
            }
        }

        /// <summary>
        /// Logolja az indul�st �s visszaad egy Log objektumot
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="procedureName"></param>
        /// <returns></returns>
        public static Log SpStart(Contentum.eBusinessDocuments.ExecParam execParam, string procedureName)
        {
            Log log = new Log();

            if (IsLogEnabled && IsSpLogEnabled())
            {
                log.spData = new KRT_Log_StoredProcedure();
                log.spData.Date = log.GetDateID();
                log.spData.Name = procedureName;
                log.spData.WS_StartDate = GetPrevCallingDateID(execParam.CallingChain);
                string id = Add.SpStart(execParam, log.spData);

                if (!String.IsNullOrEmpty(id))
                {
                    log.currentId = id;
                    log.currentLevel = execParam.CallingChain.Count;
                }
            }

            return log;
        }

        public void SpEnd(Contentum.eBusinessDocuments.ExecParam execParam, Contentum.eBusinessDocuments.Result result)
        {
            if (execParam == null || result == null) return;

            if (IsLogEnabled && IsSpLogEnabled())
            {
                if (CheckCallingChain(execParam.CallingChain))
                {
                    this.spData.Date = this.GetDateID();
                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        Add.SpEnd(execParam, this.spData);
                    }
                    //van hibak�d
                    else
                    {
                        if (result.ErrorType == Constants.ErrorType.Warning)
                        {
                            Add.SpEndError(execParam,this.spData, result, Level.Warn);
                        }
                        else
                        {
                            Add.SpEndError(execParam, this.spData, result, Level.Error);
                        } 
                        
                    }

                    //loggol�s v�ge ut�n az oszt�ly deinicializ�lt �llapotba ker�l
                    currentId = String.Empty;
                    currentLevel = -1;
                }
            }
        }

        /// <summary>
        /// Hibavizsg�lat n�lk�li SpEnd
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="result"></param>
        public void SpEnd(Contentum.eBusinessDocuments.ExecParam execParam)
        {
            if (execParam == null) return;

            if (IsLogEnabled && IsSpLogEnabled())
            {
                if (CheckCallingChain(execParam.CallingChain))
                {
                    this.spData.Date = this.GetDateID();
                    Add.SpEnd(execParam, this.spData);

                    //loggol�s v�ge ut�n az oszt�ly deinicializ�lt �llapotba ker�l
                    currentId = String.Empty;
                    currentLevel = -1;
                }
            }
        }

        #endregion

        #region Application

        public static void StartApplication()
        {
            //Alkalmaz�s indul�s�nak napl�z�sa
        }

        public static void EndApplication()
        {
            //Alkalmaz�s le�ll�s�nak napl�z�sa 
        }

        #endregion

        #region Log hib�inak loggol�sa

        protected static void LogError(Contentum.eBusinessDocuments.Result res)
        {
            string stack = new System.Diagnostics.StackTrace().GetFrame(1).ToString();
            //AddFile(String.Format(", Time: {0},ErrorCode: {1}, ErrorMessage: {2}, Stack: {3} ", Time, res.ErrorCode, res.ErrorMessage, stack));
            Logger.ErrorStart();
            Logger.Error("Audit log hiba");
            Logger.Error(String.Format("Hiba id�pontja: {0}", Time));
            Logger.Error(String.Format("Hibak�d: {0}", res.ErrorCode));
            Logger.Error(String.Format("Hiba�zenet: {0}", res.ErrorMessage));
            Logger.Error(String.Format("Stack: {0}", stack));
            Logger.ErrorEnd();
        }

        protected static void LogWarning(Contentum.eBusinessDocuments.Result res)
        {
            string stack = new System.Diagnostics.StackTrace().GetFrame(1).ToString();
            Logger.WarnStart();
            Logger.Error("Audit log hiba");
            Logger.Warn(String.Format("Hiba id�pontja: {0}", Time));
            Logger.Warn(String.Format("Hibak�d: {0}", res.ErrorCode));
            Logger.Warn(String.Format("Hiba�zenet: {0}", res.ErrorMessage));
            Logger.Warn(String.Format("Stack: {0}", stack));
            Logger.WarnEnd();
        }

        public static void AddFile(string message, string fileName)
        {
            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.UTF8))
                    {
                        sw.WriteLine(message);
                    }
                }
            }
            catch
            {
            }

        }

        public static void AddFile(string message)
        {
            AddFile(message,@"c:\temp\log.txt");
        }

        #endregion

    }
}
