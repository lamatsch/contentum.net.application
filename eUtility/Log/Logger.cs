using System;

using log4net;
using log4net.Config;

using Contentum.eBusinessDocuments;
using log4net.Core;

//namespace ...
//{
namespace Contentum.eUtility
{
    //public sealed class Logger
    public class Logger
    {
        public static string LOG_START = ">>>";
        public static string LOG_END = "<<<";
        private static Logger instance = new Logger();
        private static readonly ILog log = LogManager.GetLogger(typeof(Logger));
        private static readonly Type declaringType = typeof(Logger);

        #region AuditLog

        internal static class AuditLog
        {
            private static readonly ILog _log_Login = LogManager.GetLogger("AuditLog.Login");
            private static readonly ILog _log_Login_Start = LogManager.GetLogger("AuditLog.Login.Start");
            private static readonly ILog _log_Page = LogManager.GetLogger("AuditLog.Page");
            private static readonly ILog _log_Page_Start = LogManager.GetLogger("AuditLog.Page.Start");
            private static readonly ILog _log_WebService = LogManager.GetLogger("AuditLog.WebService");
            private static readonly ILog _log_WebService_Start = LogManager.GetLogger("AuditLog.WebService.Start");
            private static readonly ILog _log_StoredProcedure = LogManager.GetLogger("AuditLog.StoredProcedure");
            private static readonly ILog _log_StoredProcedure_Start = LogManager.GetLogger("AuditLog.StoredProcedure.Start");
            private static readonly Type declaringType = typeof(Log);

            public static ILog Log_Login
            {
                get
                {
                    return _log_Login;
                }
            }

            public static ILog Log_Login_Start
            {
                get
                {
                    return _log_Login_Start;
                }
            }

            public static ILog Log_Page
            {
                get
                {
                    return _log_Page;
                }
            }

            public static ILog Log_Page_Start
            {
                get
                {
                    return _log_Page_Start;
                }
            }

            public static ILog Log_WebService
            {
                get
                {
                    return _log_WebService;
                }
            }

            public static ILog Log_WebService_Start
            {
                get
                {
                    return _log_WebService_Start;
                }
            }

            public static ILog Log_StoredProcedure
            {
                get
                {
                    return _log_StoredProcedure;
                }
            }

            public static ILog Log_StoredProcedure_Start
            {
                get
                {
                    return _log_StoredProcedure_Start;
                }
            }

            static AuditLog()
            {
                log4net.Config.XmlConfigurator.Configure();
            }

            public static log4net.Core.LoggingEvent GetLoggingEvent(ILog log, log4net.Core.Level level, object message, Exception exception)
            {
                log4net.Core.LoggingEvent loggingEvent = new log4net.Core.LoggingEvent(declaringType,
                    log.Logger.Repository, log.Logger.Name,level,message, exception);

                loggingEvent.Properties["message"] = message;

                return loggingEvent;
            }

            public static log4net.Core.LoggingEvent GetLoggingEvent(ILog log, log4net.Core.Level level, object message)
            {
                return GetLoggingEvent(log, level, message, null);
            }

            public static log4net.Core.LoggingEvent GetLoggingEvent(ILog log, log4net.Core.Level level)
            {
                return GetLoggingEvent(log, level, null, null);
            }

            public static log4net.Core.LoggingEvent GetLoggingEvent(ILog log)
            {
                return GetLoggingEvent(log, log4net.Core.Level.Info, null, null);
            }

        }

        #endregion

        public static Logger Instance
        {
            get { return instance; }
        }


        protected Logger()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public static ILog GetLogger
        {
            get { return log; }
        }

        private static string Append(string p_msg,
                                     System.Exception p_ex,
                                     Contentum.eBusinessDocuments.ExecParam p_execParam,
                                     Contentum.eBusinessDocuments.Result p_result)
        {
            string felh_Id = (p_execParam == null) ? "NULL" : p_execParam.Felhasznalo_Id;
            string errorCode = (p_result == null) ? "NULL" : p_result.ErrorCode;
            string errorMsg = (p_result == null) ? "NULL" : p_result.ErrorMessage;
            string exMsg = (p_ex == null) ? "NULL" : p_ex.Message;

            return "[" + felh_Id + "] " + p_msg + "(ErrorCode:'" + errorCode + "'; ErrorMessage:'" + errorMsg + "') " + exMsg;
        }

        #region All level

        public static bool IsEnabled(Level level)
        {
            return Logger.log.Logger.IsEnabledFor(level);
        }

        public static void Log(Level level,string p_msg)
        {
            Logger.Log(level,p_msg, null, null, null);
        }

        public static void Log(Level level,string p_msg,
                                 System.Exception p_ex)
        {
            Logger.Log(level,p_msg, p_ex, null, null);
        }

        public static void Log(Level level,string p_msg,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam)
        {
            Logger.Log(level,p_msg, null, p_execParam, null);
        }

        public static void Log(Level level,string p_msg,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam,
                                 Contentum.eBusinessDocuments.Result p_result)
        {
            Logger.Log(level,p_msg, null, p_execParam, p_result);
        }

        public static void Log(Level level,string p_msg,
                                 System.Exception p_ex,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam,
                                 Contentum.eBusinessDocuments.Result p_result)
        {
            if (Logger.IsEnabled(level))
            {
                LogByLevel(level,Append(p_msg, p_ex, p_execParam, p_result));
            }
        }

        public static void Start(Level level)
        {
            Logger.Start(level,"");
        }

        public static void Start(Level level,string p_msg)
        {
            LogByLevel(level,Logger.LOG_START + p_msg);
        }

        public static void End(Level level)
        {
            Logger.End(level,"");
        }

        public static void End(Level level,string p_msg)
        {
            LogByLevel(level,Logger.LOG_END + p_msg);
        }

        private static void LogByLevel(Level level, string message)
        {
            LoggingEvent loggingEvent = new LoggingEvent(declaringType, log.Logger.Repository, log.Logger.Name, level, message, null);
            log.Logger.Log(loggingEvent);
        }

        #endregion

        #region Debug

        public static bool IsDebugEnabled()
        {
            return Logger.log.IsDebugEnabled;
        }

        public static void Debug(string p_msg)
        {
            Logger.Debug(p_msg, null, null, null);
        }

        public static void Debug(string p_msg,
                                 System.Exception p_ex)
        {
            Logger.Debug(p_msg, p_ex, null, null);
        }

        public static void Debug(string p_msg,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam)
        {
            Logger.Debug(p_msg, null, p_execParam, null);
        }

        public static void Debug(string p_msg,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam,
                                 Contentum.eBusinessDocuments.Result p_result)
        {
            Logger.Debug(p_msg, null, p_execParam, p_result);
        }

        public static void Debug(string p_msg,
                                 System.Exception p_ex,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam,
                                 Contentum.eBusinessDocuments.Result p_result)
        {
            if (Logger.IsDebugEnabled())
            {
                Logger.log.Debug(Append(p_msg, p_ex, p_execParam, p_result));
            }
        }

        public static void DebugStart()
        {
            Logger.DebugStart("");
        }

        public static void DebugStart(string p_msg)
        {
            Logger.log.Debug(Logger.LOG_START + p_msg);
        }

        public static void DebugEnd()
        {
            Logger.DebugEnd("");
        }

        public static void DebugEnd(string p_msg)
        {
            Logger.log.Debug(Logger.LOG_END + p_msg);
        }

        #endregion

        #region Info

        public static bool IsInfoEnabled()
        {
            return Logger.log.IsInfoEnabled;
        }

        public static void Info(string p_msg)
        {
            Logger.Info(p_msg, null, null, null);
        }

        public static void Info(string p_msg,
                                 System.Exception p_ex)
        {
            Logger.Info(p_msg, p_ex, null, null);
        }

        public static void Info(string p_msg,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam)
        {
            Logger.Info(p_msg, null, p_execParam, null);
        }

        public static void Info(string p_msg,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam,
                                 Contentum.eBusinessDocuments.Result p_result)
        {
            Logger.Info(p_msg, null, p_execParam, p_result);
        }

        public static void Info(string p_msg,
                                 System.Exception p_ex,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam,
                                 Contentum.eBusinessDocuments.Result p_result)
        {
            if (Logger.IsInfoEnabled())
            {
                Logger.log.Info(Append(p_msg, p_ex, p_execParam, p_result));
            }
        }

        public static void InfoStart()
        {
            Logger.InfoStart("");
        }

        public static void InfoStart(string p_msg)
        {
            Logger.log.Info(Logger.LOG_START + p_msg);
        }

        public static void InfoEnd()
        {
            Logger.InfoEnd("");
        }

        public static void InfoEnd(string p_msg)
        {
            Logger.log.Info(Logger.LOG_END + p_msg);
        }

        #endregion

        #region Warn

        public static bool IsWarnEnabled()
        {
            return Logger.log.IsWarnEnabled;
        }

        public static void Warn(string p_msg)
        {
            Logger.Warn(p_msg, null, null, null);
        }

        public static void Warn(string p_msg,
                                 System.Exception p_ex)
        {
            Logger.Warn(p_msg, p_ex, null, null);
        }

        public static void Warn(string p_msg,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam)
        {
            Logger.Warn(p_msg, null, p_execParam, null);
        }

        public static void Warn(string p_msg,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam,
                                 Contentum.eBusinessDocuments.Result p_result)
        {
            Logger.Warn(p_msg, null, p_execParam, p_result);
        }

        public static void Warn(string p_msg,
                                 System.Exception p_ex,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam,
                                 Contentum.eBusinessDocuments.Result p_result)
        {
            if (Logger.IsWarnEnabled())
            {
                Logger.log.Warn(Append(p_msg, p_ex, p_execParam, p_result));
            }
        }

        public static void WarnStart()
        {
            Logger.WarnStart("");
        }

        public static void WarnStart(string p_msg)
        {
            Logger.log.Warn(Logger.LOG_START + p_msg);
        }

        public static void WarnEnd()
        {
            Logger.WarnEnd("");
        }

        public static void WarnEnd(string p_msg)
        {
            Logger.log.Warn(Logger.LOG_END + p_msg);
        }

        #endregion

        #region Error

        public static bool IsErrorEnabled()
        {
            return Logger.log.IsErrorEnabled;
        }

        public static void Error(string p_msg)
        {
            Logger.Error(p_msg, null, null, null);
        }

        public static void Error(string p_msg,
                                 System.Exception p_ex)
        {
            Logger.Error(p_msg, p_ex, null, null);
        }

        public static void Error(string p_msg,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam)
        {
            Logger.Error(p_msg, null, p_execParam, null);
        }

        public static void Error(string p_msg,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam,
                                 Contentum.eBusinessDocuments.Result p_result)
        {
            Logger.Error(p_msg, null, p_execParam, p_result);
        }

        public static void Error(string p_msg,
                                 System.Exception p_ex,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam,
                                 Contentum.eBusinessDocuments.Result p_result)
        {
            if (Logger.IsErrorEnabled())
            {
                Logger.log.Error(Append(p_msg, p_ex, p_execParam, p_result));
            }
        }

        public static void ErrorStart()
        {
            Logger.ErrorStart("");
        }

        public static void ErrorStart(string p_msg)
        {
            Logger.log.Error(Logger.LOG_START + p_msg);
        }

        public static void ErrorEnd()
        {
            Logger.ErrorEnd("");
        }

        public static void ErrorEnd(string p_msg)
        {
            Logger.log.Error(Logger.LOG_END + p_msg);
        }

        #endregion

        #region Fatal

        public static bool IsFatalEnabled()
        {
            return Logger.log.IsFatalEnabled;
        }

        public static void Fatal(string p_msg)
        {
            Logger.Fatal(p_msg, null, null, null);
        }

        public static void Fatal(string p_msg,
                                 System.Exception p_ex)
        {
            Logger.Fatal(p_msg, p_ex, null, null);
        }

        public static void Fatal(string p_msg,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam)
        {
            Logger.Fatal(p_msg, null, p_execParam, null);
        }

        public static void Fatal(string p_msg,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam,
                                 Contentum.eBusinessDocuments.Result p_result)
        {
            Logger.Fatal(p_msg, null, p_execParam, p_result);
        }

        public static void Fatal(string p_msg,
                                 System.Exception p_ex,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam,
                                 Contentum.eBusinessDocuments.Result p_result)
        {
            if (Logger.IsFatalEnabled())
            {
                Logger.log.Fatal(Append(p_msg, p_ex, p_execParam, p_result));
            }
        }

        public static void FatalStart()
        {
            Logger.FatalStart("");
        }

        public static void FatalStart(string p_msg)
        {
            Logger.log.Fatal(Logger.LOG_START + p_msg);
        }

        public static void FatalEnd()
        {
            Logger.FatalEnd("");
        }

        public static void FatalEnd(string p_msg)
        {
            Logger.log.Fatal(Logger.LOG_END + p_msg);
        }

        #endregion
    }
}
//}
