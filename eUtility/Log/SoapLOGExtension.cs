﻿using Contentum.eUtility;
using System;
using System.IO;
using System.Text;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Web;
using System.Xml;
using Contentum.eBusinessDocuments;

namespace Contentum.eUtility
{
    public class LogHelper
    {
        private const int MAX_CHARS = 100000;
        private const string TRUNCATED_LABEL = "...truncated...";
        public static string TruncateIfNeeded(string input)
        {
            if (input == null || input.Length == 0) { return input; }
            if (input.Length < MAX_CHARS)
            {
                return input;
            }
            return input.Substring(0, MAX_CHARS) + TRUNCATED_LABEL;
        } 
    }

    public class LogSoapData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogSoapData"/> class.
        /// </summary>
        public LogSoapData()
        {
            Request = string.Empty;
            Response = string.Empty;
        }
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string NTUserName { get; set; }
        /// <summary>
        /// Gets or sets the stack trace.
        /// </summary>
        /// <value>
        /// The stack trace.
        /// </value>
        public string StackTrace { get; set; }
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }
        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        public string Url { get; set; }
        /// <summary>
        /// Gets or sets the name of the method.
        /// </summary>
        /// <value>
        /// The name of the method.
        /// </value>
        public string MethodName { get; set; }
        /// <summary>
        /// Gets or sets the request time.
        /// </summary>
        /// <value>
        /// The request time.
        /// </value>
        public DateTime RequestTime { get; set; }
        /// <summary>
        /// Gets or sets the response time.
        /// </summary>
        /// <value>
        /// The response time.
        /// </value>
        public DateTime ResponseTime { get; set; }
        /// <summary>
        /// Gets or sets the request.
        /// </summary>
        /// <value>
        /// The request.
        /// </value>
        public string Request { get; set; }
        /// <summary>
        /// Gets or sets the response.
        /// </summary>
        /// <value>
        /// The response.
        /// </value>
        public string Response { get; set; }

        /// <summary>
        /// Gets or sets the request client ip.
        /// </summary>
        /// <value>
        /// The request client ip.
        /// </value>
        public string RequestClientIP { get; set; }
        /// <summary>
        /// Gets or sets the response client ip.
        /// </summary>
        /// <value>
        /// The response client ip.
        /// </value>
        public string ResponseClientIP { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [requsest is local].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [requsest is local]; otherwise, <c>false</c>.
        /// </value>
        public bool RequestIsLocal { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [response is local].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [response is local]; otherwise, <c>false</c>.
        /// </value>        
        public string ResponseHostIP { get; set; }
        /// <summary>
        /// Gets or sets the request host ip.
        /// </summary>
        /// <value>
        /// The request host ip.
        /// </value>
        public string RequestHostIP { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder().AppendLine();
            sb.AppendFormat("[WINDOWS USERNAME]  :  {0}", NTUserName).AppendLine();
            sb.AppendFormat("[USERNAME]  :  {0}", UserName).AppendLine();
            sb.AppendFormat("[URL]  :  {0}", Url).AppendLine().AppendLine();
            sb.AppendFormat("[METHOD]  :  {0}", MethodName).AppendLine();
            if (!string.IsNullOrEmpty(StackTrace))
                sb.AppendFormat("[EXCEPTION ]  :  {0}", StackTrace).AppendLine();
            sb.AppendLine("----------------------- REQUEST ---------------------");
            sb.AppendFormat("[REQUEST TIME]  :  {0}", RequestTime).AppendLine();
            sb.AppendFormat("[REQUEST CLIENTIP]  :  {0}", RequestClientIP).AppendLine();
            sb.AppendFormat("[REQUEST HOST IP]  :  {0}", RequestHostIP).AppendLine();
            sb.AppendFormat("[REQUEST ISLOCAL]  :  {0}", RequestIsLocal).AppendLine();



            sb.AppendLine(LogHelper.TruncateIfNeeded(Request));
            sb.AppendLine("----------------------- REQUEST ----------------------");
            sb.AppendLine("----------------------- RESPONSE ---------------------");
            sb.AppendFormat("[RESPONSE TIME]  :  {0}", ResponseTime).AppendLine();
            sb.AppendFormat("[RESPONSE CLIENTIP]  :  {0}", ResponseClientIP).AppendLine();
            sb.AppendFormat("[RESPONSE REMOTE HOST]  :  {0}", ResponseHostIP).AppendLine();


            sb.AppendLine(LogHelper.TruncateIfNeeded(Response));
            sb.AppendLine("----------------------- RESPONSE ---------------------");

            return sb.ToString();
        }
        public eBusinessDocuments.KRT_SoapMessageLog ToKRT_SoapMessageLog()
        {
            eBusinessDocuments.KRT_SoapMessageLog obj = new eBusinessDocuments.KRT_SoapMessageLog
            {
                CelRendszer = this.RequestClientIP,
                ForrasRendszer = this.RequestHostIP,
                FuttatoFelhasznalo = (string.IsNullOrEmpty(this.UserName) ? this.NTUserName : this.UserName),
                KeresFogadas = this.RequestTime.ToString(),
                KeresKuldes = this.ResponseTime.ToString(),
                Url = this.Url,
                MetodusNeve = this.MethodName,
                Sikeres = (string.IsNullOrEmpty(this.StackTrace)) ? "1" : "0",
                Hiba = (string.IsNullOrEmpty(this.StackTrace) ? "" : this.StackTrace),
                Local = (this.RequestIsLocal) ? "1" : "0",

            };
            if (Request != null)
            {
                obj.RequestData = LogHelper.TruncateIfNeeded(Request);
            }
            if (Response != null)
            {
                obj.ResponseData = LogHelper.TruncateIfNeeded(Response);
            }
            return obj;
        }
        public void LogObject()
        {
            KRT_SoapMessageLog krt_soapmessagelog = this.ToKRT_SoapMessageLog();
            krt_soapmessagelog.RequestData = LogHelper.TruncateIfNeeded(krt_soapmessagelog.RequestData);
            krt_soapmessagelog.ResponseData = LogHelper.TruncateIfNeeded(krt_soapmessagelog.ResponseData);

            eAdmin.Service.KRT_SoapMessageLogService svc_SoapMessage = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_SoapMessageLogService();
            ExecParam ep = new ExecParam();
            Result res = null;

            if (svc_SoapMessage != null)
                res = svc_SoapMessage.Insert(ep, krt_soapmessagelog);
            if (res != null && res.IsError)
            {
                throw new Exception(res.ErrorMessage);
            }
        }
    }

    public class SoapLOGExtension : SoapExtension
    {
        public SoapLOGExtension() : base()
        {
            _logSoapData = new LogSoapData();
        }
        /// <summary>
        /// Gets or sets the message stream.
        /// </summary>
        /// <value>
        /// The message stream.
        /// </value>
        Stream _messageStream { get; set; }
        MemoryStream _accessStream { get; set; }
        /// <summary>
        /// Gets or sets the log SOAP data.
        /// </summary>
        /// <value>
        /// The log SOAP data.
        /// </value>
        LogSoapData _logSoapData { get; set; }

        /// <summary>
        /// When overridden in a derived class, allows a SOAP extension to initialize data specific to a class implementing an XML Web service at a one time performance cost.
        /// </summary>
        /// <param name="serviceType">The type of the class implementing the XML Web service to which the SOAP extension is applied.</param>
        /// <returns>
        /// The <see cref="T:System.Object" /> that the SOAP extension initializes for caching.
        /// </returns>
        public override object GetInitializer(Type serviceType)
        {
            return null;
        }

        /// <summary>
        /// When overridden in a derived class, allows a SOAP extension to initialize data specific to an XML Web service method using an attribute applied to the XML Web service method at a one time performance cost.
        /// </summary>
        /// <param name="methodInfo">A <see cref="T:System.Web.Services.Protocols.LogicalMethodInfo" /> representing the specific function prototype for the XML Web service method to which the SOAP extension is applied.</param>
        /// <param name="attribute">The <see cref="T:System.Web.Services.Protocols.SoapExtensionAttribute" /> applied to the XML Web service method.</param>
        /// <returns>
        /// The <see cref="T:System.Object" /> that the SOAP extension initializes for caching.
        /// </returns>
        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
        {

            return null;
        }

        /// <summary>
        /// When overridden in a derived class, allows a SOAP extension to initialize itself using the data cached in the <see cref="M:System.Web.Services.Protocols.SoapExtension.GetInitializer(System.Web.Services.Protocols.LogicalMethodInfo,System.Web.Services.Protocols.SoapExtensionAttribute)" /> method.
        /// </summary>
        /// <param name="initializer">The <see cref="T:System.Object" /> returned from <see cref="M:System.Web.Services.Protocols.SoapExtension.GetInitializer(System.Web.Services.Protocols.LogicalMethodInfo,System.Web.Services.Protocols.SoapExtensionAttribute)" /> cached by ASP.NET.</param>
        public override void Initialize(object initializer)
        {

        }

        /// <summary>
        /// When overridden in a derived class, allows a SOAP extension access to the memory buffer containing the SOAP request or response.
        /// </summary>
        /// <param name="stream">A memory buffer containing the SOAP request or response.</param>
        /// <returns>
        /// A <see cref="T:System.IO.Stream" /> representing a new memory buffer that this SOAP extension can modify.
        /// </returns>
        public override System.IO.Stream ChainStream(Stream stream)
        {
            _messageStream = stream;
            _accessStream = new MemoryStream();
            return _accessStream;
        }

        /// <summary>
        /// When overridden in a derived class, allows a SOAP extension to receive a <see cref="T:System.Web.Services.Protocols.SoapMessage" /> to process at each <see cref="T:System.Web.Services.Protocols.SoapMessageStage" />.
        /// </summary>
        /// <param name="message">The <see cref="T:System.Web.Services.Protocols.SoapMessage" /> to process.</param>
        public override void ProcessMessage(SoapMessage message)
        {
            try
            {
                DoProcessMessage(message);
            }
            catch (Exception e)
            {
                Logger.Error("SoapLOGExtension could not log communication.", e);
            }
        }

        private void DoProcessMessage(SoapMessage message)
        {
            // Request esetén
            if (message.Stage == SoapMessageStage.BeforeDeserialize)
            {
                CopyStream(_messageStream, _accessStream);

                _accessStream.Position = 0;
            
                // Log
                _logSoapData.RequestHostIP = HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
                _logSoapData.RequestClientIP = GetClientIp();
                _logSoapData.RequestIsLocal = HttpContext.Current.Request.IsLocal;
                _logSoapData.NTUserName = HttpContext.Current.Request.LogonUserIdentity.Name;
                _logSoapData.Request =  LogMessageToString(_accessStream);
                _logSoapData.RequestTime = DateTime.Now;
                _logSoapData.Url = message.Url;
                _logSoapData.MethodName = message.MethodInfo.Name;
                _logSoapData.UserName = GetSZURUserName();

                _accessStream.Position = 0;
            }
            // Response esetén
            if (message.Stage == SoapMessageStage.AfterSerialize)
            {
                _accessStream.Position = 0;
                                
                // Log
                _logSoapData.ResponseHostIP = HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
                _logSoapData.ResponseClientIP = GetClientIp();
                _logSoapData.NTUserName = HttpContext.Current.Request.LogonUserIdentity.Name;
                _logSoapData.Response = LogMessageToString(_accessStream);
                _logSoapData.ResponseTime = DateTime.Now;
                //_logSoapData.UserName = HttpContext.Current.User.Identity.Name;
                _accessStream.Position = 0;
                if (message.Exception != null)
                {
                    _logSoapData.StackTrace = message.Exception.Message + Environment.NewLine + message.Exception.StackTrace;

                }
                CopyStream(_accessStream, _messageStream);
                _accessStream.Position = 0;
                _logSoapData.LogObject();
            }
        }
        /// <summary>
        /// Logs the message to string.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <returns></returns>
        private string LogMessageToString(Stream s)
        {
            return new StreamReader(s).ReadToEnd();
        }
        /// <summary>
        /// Copies the stream.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        private void CopyStream(Stream from, Stream to)
        {
            TextReader reader = new StreamReader(from);
            TextWriter writer = new StreamWriter(to);
            string line = string.Empty;
            while ((line = reader.ReadLine()) != null)
            {               
                writer.WriteLine(line);
            }
            writer.Flush();
        }
        private static string GetClientIp(string ip = null)
        {
            if (String.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            if (String.IsNullOrEmpty(ip) || ip.Equals("unknown", StringComparison.OrdinalIgnoreCase))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
            }
            return ip;
        }

        string GetSZURUserName()
        {
            string userName = String.Empty;

            var authString = HttpContext.Current.Request.Headers["Authorization"];

            const string basicPrefix = "Basic ";

            // "Basic " stringgel kezdődik az Authorization header értéke, ez után jön a Base64-ben kódolt felhasználónév:jelszó pár
            if (!String.IsNullOrEmpty(authString)
                && authString.StartsWith(basicPrefix))
            {
                string basicAuthContentBase64 = authString.Substring(basicPrefix.Length);

                string basicAuthContent = System.Text.Encoding.GetEncoding("iso-8859-2").GetString(System.Convert.FromBase64String(basicAuthContentBase64));

                // ':' karakterrel elválasztva jön a felhasználónév:jelszó páros:
                int i = basicAuthContent.IndexOf(':');
                if (i > -1)
                {
                    userName = basicAuthContent.Substring(0, i);
                }
            }

            return userName;
        }
    }


}
[AttributeUsage(AttributeTargets.Method)]
public class LogExtensionAttribute : SoapExtensionAttribute
{
    private int priority = 1;
    public override int Priority
    {
        get
        {
            return priority;
        }
        set
        {
            priority = value;
        }
    }
    public override System.Type ExtensionType
    {
        get
        {
            return typeof(SoapLOGExtension);
        }
    }
}