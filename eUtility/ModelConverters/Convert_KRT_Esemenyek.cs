﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility.Models;
using System.Data;

namespace Contentum.eUtility.Convert
{
    public static partial class ModelConverters
    {
        public static Model_KRT_Esemenyek_CheckSum Convert(DataRow row)
        {
            Model_KRT_Esemenyek_CheckSum result = new Model_KRT_Esemenyek_CheckSum();
            if (row.Table.Columns.Contains("Id"))
                result.Id = row["Id"].ToString();
            if (row.Table.Columns.Contains("Obj_Id"))
                result.Obj_Id = row["Obj_Id"].ToString();
            if (row.Table.Columns.Contains("ObjTip_Id"))
                result.ObjTip_Id = row["ObjTip_Id"].ToString();
            if (row.Table.Columns.Contains("Azonositoja"))
                result.Azonositoja = row["Azonositoja"].ToString();
            if (row.Table.Columns.Contains("Felhasznalo_Id_User"))
                result.Felhasznalo_Id_User = row["Felhasznalo_Id_User"].ToString();
            if (row.Table.Columns.Contains("Csoport_Id_FelelosUserSzerveze"))
                result.Csoport_Id_FelelosUserSzerveze = row["Csoport_Id_FelelosUserSzerveze"].ToString();
            if (row.Table.Columns.Contains("Felhasznalo_Id_Login"))
                result.Felhasznalo_Id_Login = row["Felhasznalo_Id_Login"].ToString();
            if (row.Table.Columns.Contains("Csoport_Id_Cel"))
                result.Csoport_Id_Cel = row["Csoport_Id_Cel"].ToString();
            if (row.Table.Columns.Contains("Helyettesites_Id"))
                result.Helyettesites_Id = row["Helyettesites_Id"].ToString();
            if (row.Table.Columns.Contains("Tranzakcio_Id"))
                result.Tranzakcio_Id = row["Tranzakcio_Id"].ToString();
            if (row.Table.Columns.Contains("Funkcio_Id"))
                result.Funkcio_Id = row["Funkcio_Id"].ToString();
            if (row.Table.Columns.Contains("CsoportHierarchia"))
                result.CsoportHierarchia = row["CsoportHierarchia"].ToString();
            if (row.Table.Columns.Contains("KeresesiFeltetel"))
                result.KeresesiFeltetel = row["KeresesiFeltetel"].ToString();
            if (row.Table.Columns.Contains("TalalatokSzama"))
                result.TalalatokSzama = row["TalalatokSzama"].ToString();
            if (row.Table.Columns.Contains("Munkaallomas"))
                result.Munkaallomas = row["Munkaallomas"].ToString();
            if (row.Table.Columns.Contains("MuveletKimenete"))
                result.MuveletKimenete = row["MuveletKimenete"].ToString();
            if (row.Table.Columns.Contains("CheckSum"))
                result.CheckSum = row["CheckSum"].ToString();
            return result;
        }
        public static Model_KRT_Esemenyek_CheckSum Convert(KRT_Esemenyek row)
        {
            Model_KRT_Esemenyek_CheckSum result = new Model_KRT_Esemenyek_CheckSum();
            result.Id = row.Id;

            result.Obj_Id = row.Obj_Id;

            result.ObjTip_Id = row.ObjTip_Id;

            result.Azonositoja = row.Azonositoja;

            result.Felhasznalo_Id_User = row.Felhasznalo_Id_User;

            result.Csoport_Id_FelelosUserSzerveze = row.Csoport_Id_FelelosUserSzerveze;

            result.Felhasznalo_Id_Login = row.Felhasznalo_Id_Login;

            result.Csoport_Id_Cel = row.Csoport_Id_Cel;

            result.Helyettesites_Id = row.Helyettesites_Id;

            result.Tranzakcio_Id = row.Tranzakcio_Id;

            result.Funkcio_Id = row.Funkcio_Id;

            result.CsoportHierarchia = row.CsoportHierarchia;

            result.KeresesiFeltetel = row.KeresesiFeltetel;

            result.TalalatokSzama = row.TalalatokSzama;

            result.Munkaallomas = row.Munkaallomas;

            result.MuveletKimenete = row.MuveletKimenete;

            result.CheckSum = row.CheckSum;
            return result;
        }
    }
}
