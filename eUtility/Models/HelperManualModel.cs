﻿using System.Collections.Generic;

namespace Contentum.eUtility.Models
{
    public class HelperManualModelContainerClass
    {
        public List<HelperManualModelClass> Collection { get; set; }
    }
    public class HelperManualModelClass
    {
        public string Form { get; set; }
        public string Mode { get; set; }
        public string Command { get; set; }
        public string Startup { get; set; }
        public string TopicId { get; set; }
        public HelperManualModelClass()
        {

        }
        public override bool Equals(object obj)
        {
            var @class = obj as HelperManualModelClass;
            return @class != null &&
                  ((string.IsNullOrEmpty(Form) && string.IsNullOrEmpty(@class.Form)) || Form == @class.Form) &&
                  ((string.IsNullOrEmpty(Mode) && string.IsNullOrEmpty(@class.Mode)) || Mode == @class.Mode) &&
                  ((string.IsNullOrEmpty(Command) && string.IsNullOrEmpty(@class.Command)) || Command == @class.Command) &&
                  ((string.IsNullOrEmpty(Startup) && string.IsNullOrEmpty(@class.Startup)) || Startup == @class.Startup)
                  ;
        }

        public override int GetHashCode()
        {
            var hashCode = 1069414829;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Form);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Mode);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Command);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Startup);
            return hashCode;
        }
    }
}
