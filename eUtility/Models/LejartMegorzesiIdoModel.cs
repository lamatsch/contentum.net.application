﻿using System;

namespace Contentum.eUtility.Models
{
    public class LejartMegorzesiIdoModel
    {
        public string Id;
        public string Azonosito;
        public string MegorzesiIdoVege;
        public string Csoport_Id_Felelos;
        public string Csoport_Id_Felelos_Nev;
        public string Csoport_Id_Felelos_Email;
    }
}