﻿using System.Xml.Serialization;

namespace Contentum.eUtility.Models
{
    public class Model_KRT_Esemenyek_CheckSum
    {
        public string Id { get; set; }

        public string Obj_Id { get; set; }

        public string ObjTip_Id { get; set; }

        public string Azonositoja { get; set; }

        public string Felhasznalo_Id_User { get; set; }

        public string Csoport_Id_FelelosUserSzerveze { get; set; }

        public string Felhasznalo_Id_Login { get; set; }

        public string Csoport_Id_Cel { get; set; }

        public string Helyettesites_Id { get; set; }

        public string Tranzakcio_Id { get; set; }

        public string Funkcio_Id { get; set; }

        public string CsoportHierarchia { get; set; }

        public string KeresesiFeltetel { get; set; }

        public string TalalatokSzama { get; set; }

        public string Munkaallomas { get; set; }

        public string MuveletKimenete { get; set; }

        [XmlIgnore]
        public string CheckSum { get; set; }

        [XmlIgnore]
        public string CheckSumAgainOnTheFly { get; set; }

    }
}

