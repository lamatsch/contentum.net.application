﻿using System.Collections.Generic;

namespace Contentum.eUtility.Models
{
    public class NameDescriptionValueModel
    {
        public string Key { get; internal set; }
        public string Name { get; internal set; }
        public string Description { get; internal set; }

        public NameDescriptionValueModel(string key, string name, string description)
        {
            Key = key;
            Name = name;
            Description = description;
        }

        public override bool Equals(object obj)
        {
            var value = obj as NameDescriptionValueModel;
            return value != null &&
                   Key == value.Key;
        }

        public override int GetHashCode()
        {
            return 990326508 + EqualityComparer<string>.Default.GetHashCode(Key);
        }
    }
}
