﻿using System.Collections.Generic;
using System;

namespace Contentum.eUtility.Models
{
    //public class SakkoraModel
    //{
    //    public List<string> tipus { get; set; }
    //    public SakkoraAtmenetUgyFajtaModel[] atmenet { get; set; }

    //    public List<string> eljarasiSzakasz { get; set; }
    //    public List<string> felfuggesztesOka { get; set; }
    //    public List<string> lezarasOka { get; set; }
    //}
    //public class SakkoraAtmenetUgyFajtaModel
    //{
    //    public string ugyfajta { get; set; }
    //    public List<string> atmenet { get; set; }
    //}


    public class SakkoraModel
    {
        public List<string> tipusok { get; set; }
        public List<string> elozmenytipus { get; set; }
        public List<SakkoraAtmenetUgyFajtaModel> atmeneteklista { get; set; }

        public List<string> felfuggesztesOkai { get; set; }
        public List<string> lezarasOkai { get; set; }

        public SakkoraAtmenetUgyFajtaModel GetAtmenetByUgyFajta(string ugyFajta)
        {
            int idx = atmeneteklista.IndexOf(new SakkoraAtmenetUgyFajtaModel() { ugyfajta = ugyFajta });
            if (idx < 0)
                return null;
            return atmeneteklista[idx];
        }
    }
    public class SakkoraAtmenetUgyFajtaModel
    {
        public string ugyfajta { get; set; }
        public List<string> atmenetek { get; set; }

        public override bool Equals(object obj)
        {
            var model = obj as SakkoraAtmenetUgyFajtaModel;
            return model != null &&
                   ugyfajta == model.ugyfajta;
        }

        public override int GetHashCode()
        {
            return 230684302 + EqualityComparer<string>.Default.GetHashCode(ugyfajta);
        }
    }

    public enum SakkoraUgyFajtaTipus
    {
        NH,
        H1,
        H2,
    }
    public enum SakkoraTipus
    {
        NONE,
        ON_PROGRESS,
        PAUSE,
        STOP,
    }
    public enum SakkoraElozmenyTipus
    {
        NONE,
        ON_PROGRESS,
        PAUSE,
        STOP,
    }

    public static class ELJARASI_SZAKASZ_FOK
    {
        public const string KCS = "ELJARASI_SZAKASZ_FOK";
        public const string I = "1";
        public const string II = "2";
    }
    public static class IRAT_HATASA_UGYINTEZESRE_KOD
    {
        public const string NULLA_NONE = "0_NONE";
        public const string NULLA_STOP = "0_STOP";
        public const string NULLA_PAUSE = "0_PAUSE";
        public const string NULLA_ONPROGRESS = "0_ON_PROGRESS";

        public const string Egy = "1";
        public const string Ketto = "2";
        public const string Harom = "3";
        public const string Negy = "4";
        public const string Ot = "5";
        public const string Hat = "6";
        public const string Het = "7";
        public const string Nyolc = "8";
        public const string Kilenc = "9";
        public const string Tiz = "10";
        public const string Tizenegy = "11";
    }

    public class SakkoraModel1
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.ON_PROGRESS.ToString() };
        public List<string> elozmenytipus = new List<string> { };
        public List<SakkoraAtmenetUgyFajtaModel> atmeneteklista =
            new List<SakkoraAtmenetUgyFajtaModel>()
                {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>()
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string>()
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Ketto,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Harom
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string>() }
            };
        public List<string> felfuggesztesOkai = new List<string> { "BIZONYITASI_ELJARASBAN", "ELJARAS_FELFUGGESZTESE", "IRAT_FORDITASA", "HATÁSKORI_VAGY_ILLETEKESSÉGI_VITA", "ADATSZOLGALTATAS", "JOGSEGELYELJARAS", "KEZBESITESI_KOZLESI_IDOTARTAM", "SZAKERTOI_VELEMENY_ELKESZITESE", "SZAKHATOSAGI_ELJARAS" };
        public List<string> lezarasOkai = new List<string> { };
    }
    public class SakkoraModel2
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.PAUSE.ToString() };
        public List<string> elozmenytipus = new List<string> { };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>
                  { IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string>
                 {  IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Egy,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Nyolc,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Kilenc,
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string> { } }
            };

        public List<string> felfuggesztesOkai = new List<string> { "BIZONYITASI_ELJARASBAN", "ELJARAS_FELFUGGESZTESE", "IRAT_FORDITASA", "HATÁSKORI_VAGY_ILLETEKESSÉGI_VITA", "ADATSZOLGALTATAS", "JOGSEGELYELJARAS", "KEZBESITESI_KOZLESI_IDOTARTAM", "SZAKERTOI_VELEMENY_ELKESZITESE", "SZAKHATOSAGI_ELJARAS" };
        public List<string> lezarasOkai = new List<string> { };
    }

    public class SakkoraModel3
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.STOP.ToString() };
        public List<string> elozmenytipus = new List<string> { };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string>
                 {  IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Negy,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Ot
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string> { } }
            };

        public List<string> felfuggesztesOkai = new List<string> { };
        public List<string> lezarasOkai = new List<string> { "ATTETEL", "ELJARAST_MEGSZUNTETO_VEGZES", "VIZSGALAT_NELKULI_ELUTASITO_VEGZES", "ELSO_FOKU_HATAROZAT", "ELSO_FOKU_HATAROZAT_MODOSITASA", "ELSO_FOKU_HATAROZAT_VISSZAVONASA" };
    }

    public class SakkoraModel4
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.ON_PROGRESS.ToString() };
        public List<string> elozmenytipus = new List<string> { };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string>
                 {  IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Ketto,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Harom
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string> { } }
            };

        public List<string> felfuggesztesOkai = new List<string> { };
        public List<string> lezarasOkai = new List<string> { };
    }
    public class SakkoraModel5
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.ON_PROGRESS.ToString() };
        public List<string> elozmenytipus = new List<string> { };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>
                 {  IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS
                 } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string> { } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Hat,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Het
                } }
            };

        public List<string> felfuggesztesOkai = new List<string> { };
        public List<string> lezarasOkai = new List<string> { };
    }
    public class SakkoraModel6
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.PAUSE.ToString() };
        public List<string> elozmenytipus = new List<string> { };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>
                 {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE
                 } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string> { } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string>
                {    IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE,
                     IRAT_HATASA_UGYINTEZESRE_KOD.Hat,
                     IRAT_HATASA_UGYINTEZESRE_KOD.Het,
                     IRAT_HATASA_UGYINTEZESRE_KOD.Tiz,
                     IRAT_HATASA_UGYINTEZESRE_KOD.Tizenegy
                } }
            };

        public List<string> felfuggesztesOkai = new List<string> { "BIZONYITASI_ELJARASBAN", "ELJARAS_FELFUGGESZTESE", "IRAT_FORDITASA", "HATÁSKORI_VAGY_ILLETEKESSÉGI_VITA", "ADATSZOLGALTATAS", "JOGSEGELYELJARAS", "KEZBESITESI_KOZLESI_IDOTARTAM", "SZAKERTOI_VELEMENY_ELKESZITESE", "SZAKHATOSAGI_ELJARAS" };
        public List<string> lezarasOkai = new List<string> { };
    }
    public class SakkoraModel7
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.STOP.ToString() };
        public List<string> elozmenytipus = new List<string> { };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string> { } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Egy } }
            };

        public List<string> felfuggesztesOkai = new List<string> { };
        public List<string> lezarasOkai = new List<string> { "ATTETEL", "VIZSGALAT_NELKULI_ELUTASITO_VEGZES", "MASODFOKU_ELJARAST_MEGSZUNTETO_VEGZES", "MASODFOKU_HATAROZAT", "MASODFOKU_HATAROZAT_MODOSITASA", "ELSO_FOKU_HATAROZAT_VISSZAVONASA" };
    }
    public class SakkoraModel8
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.STOP.ToString() };
        public List<string> elozmenytipus = new List<string> { };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>
                 {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                     IRAT_HATASA_UGYINTEZESRE_KOD.Negy,
                     IRAT_HATASA_UGYINTEZESRE_KOD.Ot
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string>
                {    IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                     IRAT_HATASA_UGYINTEZESRE_KOD.Negy,
                     IRAT_HATASA_UGYINTEZESRE_KOD.Ot
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string> { } }
            };

        public List<string> felfuggesztesOkai = new List<string> { };
        public List<string> lezarasOkai = new List<string> { "ATTETEL", "ELJARAST_MEGSZUNTETO_VEGZES", "VIZSGALAT_NELKULI_ELUTASITO_VEGZES", "ELSO_FOKU_HATAROZAT", "ELSO_FOKU_HATAROZAT_MODOSITASA", "ELSO_FOKU_HATAROZAT_VISSZAVONASA" };
    }

    public class SakkoraModel9
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.PAUSE.ToString() };
        public List<string> elozmenytipus = new List<string> { };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Egy ,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Nyolc,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Kilenc
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Egy ,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Nyolc,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Kilenc
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string> { } }
            };

        public List<string> felfuggesztesOkai = new List<string> { "BIZONYITASI_ELJARASBAN", "ELJARAS_FELFUGGESZTESE", "IRAT_FORDITASA", "HATÁSKORI_VAGY_ILLETEKESSÉGI_VITA", "ADATSZOLGALTATAS", "JOGSEGELYELJARAS", "KEZBESITESI_KOZLESI_IDOTARTAM", "SZAKERTOI_VELEMENY_ELKESZITESE", "SZAKHATOSAGI_ELJARAS" };
        public List<string> lezarasOkai = new List<string> { };
    }

    public class SakkoraModel10
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.STOP.ToString() };
        public List<string> elozmenytipus = new List<string> { };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string> { } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Egy } },
            };

        public List<string> felfuggesztesOkai = new List<string> { };
        public List<string> lezarasOkai = new List<string> { "ATTETEL", "VIZSGALAT_NELKULI_ELUTASITO_VEGZES", "MASODFOKU_ELJARAST_MEGSZUNTETO_VEGZES", "MASODFOKU_HATAROZAT", "MASODFOKU_HATAROZAT_MODOSITASA", "ELSO_FOKU_HATAROZAT_VISSZAVONASA" };
    }

    public class SakkoraModel11
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.PAUSE.ToString() };
        public List<string> elozmenytipus = new List<string> { };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string> { } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Ot,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Tiz,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Tizenegy
                } },
            };

        public List<string> felfuggesztesOkai = new List<string> { "BIZONYITASI_ELJARASBAN", "ELJARAS_FELFUGGESZTESE", "IRAT_FORDITASA", "HATÁSKORI_VAGY_ILLETEKESSÉGI_VITA", "ADATSZOLGALTATAS", "JOGSEGELYELJARAS", "KEZBESITESI_KOZLESI_IDOTARTAM", "SZAKERTOI_VELEMENY_ELKESZITESE", "SZAKHATOSAGI_ELJARAS" };
        public List<string> lezarasOkai = new List<string> { };
    }

    public class SakkoraModel_0_NONE
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.NONE.ToString() };
        public List<string> elozmenytipus = new List<string> { SakkoraElozmenyTipus.NONE.ToString() };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE,
                    IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS,
                    IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE,
                    IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Egy,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Nyolc,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Kilenc
                }},
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string>
                {
                    IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS,
                    IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE,
                    IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string>
                {
                    IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS,
                    IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE,
                    IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                } }
            };

        public List<string> felfuggesztesOkai = new List<string> { };
        public List<string> lezarasOkai = new List<string> { };
    }

    public class SakkoraModel_0_ONPROGRESS
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.NONE.ToString() };
        public List<string> elozmenytipus = new List<string> { SakkoraElozmenyTipus.ON_PROGRESS.ToString() };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string> { } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Ketto,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Harom } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Hat,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Het } }
            };

        public List<string> felfuggesztesOkai = new List<string> { };
        public List<string> lezarasOkai = new List<string> { };
    }
    public class SakkoraModel_0_PAUSE
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.NONE.ToString() };
        public List<string> elozmenytipus = new List<string> { SakkoraElozmenyTipus.PAUSE.ToString() };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Egy,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Nyolc,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Kilenc } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string> { } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Het,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Tiz } }
            };

        public List<string> felfuggesztesOkai = new List<string> { };
        public List<string> lezarasOkai = new List<string> { };
    }
    public class SakkoraModel_0_STOP
    {
        public List<string> tipusok = new List<string> { SakkoraTipus.NONE.ToString() };
        public List<string> elozmenytipus = new List<string> { SakkoraElozmenyTipus.STOP.ToString() };
        public SakkoraAtmenetUgyFajtaModel[] atmeneteklista =
            new SakkoraAtmenetUgyFajtaModel[]
            {
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.NH.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Egy ,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Negy,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Ot} },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H1.ToString(), atmenetek = new List<string> {  } },
                new SakkoraAtmenetUgyFajtaModel() { ugyfajta=SakkoraUgyFajtaTipus.H2.ToString(), atmenetek = new List<string>
                {   IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP,
                    IRAT_HATASA_UGYINTEZESRE_KOD.Egy } }
            };

        public List<string> felfuggesztesOkai = new List<string> { };
        public List<string> lezarasOkai = new List<string> { };
    }

}
/* SAMPLE
{
"tipus": [
"ON_PROGRESS"
],
"atmenet": [
"0",
"1",
"2",
"3",
"8",
"9"
],
"eljarasiSzakasz": [],
"felfuggesztesOka": [],
"lezarasOka": [],
}

*/
