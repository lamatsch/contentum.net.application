﻿using System;

[Serializable]
public class HatosagiStatisztikaTargyszavakTemplate : TargyszavakTemplate
{
    public HatosagiStatisztikaTargyszavakTemplate()
    {
    }

    [TargyszavakId("d3db2868-6922-e911-80c9-00155d020b39")]
    public string Dontest_hozta { get; set; }

    [TargyszavakId("7EF8FE72-C739-E911-80CC-00155D020D03")]
    public string Dontest_hoza_allamigazgatasi { get; set; }

    [TargyszavakId("295de9b7-6922-e911-80c9-00155d020b39")]
    public string Dontes_formaja_onkormanyzati { get; set; }

    [TargyszavakId("cfbdb7da-6922-e911-80c9-00155d020b39")]
    public string Ugyintezes_idotartama { get; set; }

    [TargyszavakId("89fc37fe-6922-e911-80c9-00155d020b39")]
    public string Hatarido_tullepes_napokban { get; set; }

    [TargyszavakId("6c4eb8fd-6a22-e911-80c9-00155d020b39")]
    public string Jogorvoslati_eljaras_tipusa { get; set; }

    [TargyszavakId("38d88f24-6b22-e911-80c9-00155d020b39")]
    public string Jogorv_eljarasban_dontes_tipusa { get; set; }

    [TargyszavakId("365911a6-6b22-e911-80c9-00155d020b39")]
    public string Jogorv_eljarasban_dontest_hozta { get; set; }

    [TargyszavakId("d034dbd1-6b22-e911-80c9-00155d020b39")]
    public string Jogorv_eljarasban_dontes_tartalma { get; set; }

    [TargyszavakId("7e22d83e-6c22-e911-80c9-00155d020b39")]
    public string Jogorv_eljarasban_dontes_valtozasa { get; set; }

    [TargyszavakId("093372a9-6d22-e911-80c9-00155d020b39")]
    public string Hatosagi_ellenorzes_tortent { get; set; }

    [TargyszavakId("127cb717-6e22-e911-80c9-00155d020b39")]
    public string Dontes_munkaorak_szama { get; set; }

    [TargyszavakId("c7d8a03f-6e22-e911-80c9-00155d020b39")]
    public string Megallapitott_eljarasi_koltseg { get; set; }

    [TargyszavakId("3fba6e62-6e22-e911-80c9-00155d020b39")]
    public string Kiszabott_kozigazgatasi_birsag { get; set; }

    [TargyszavakId("04a5a397-6e22-e911-80c9-00155d020b39")]
    public string Sommas_eljarasban_hozott_dontes { get; set; }

    [TargyszavakId("f5afd3be-6e22-e911-80c9-00155d020b39")]
    public string Nyolc_napon_belul_lezart_nem_sommas { get; set; }

    [TargyszavakId("75fb85dd-6e22-e911-80c9-00155d020b39")]
    public string Fuggo_hatalyu_hatarozat { get; set; }

    [TargyszavakId("63da410a-6f22-e911-80c9-00155d020b39")]
    public string Hatarozat_hatalyba_lepese { get; set; }

    [TargyszavakId("f642a92a-6f22-e911-80c9-00155d020b39")]
    public string Fuggo_hatalyu_vegzes { get; set; }

    [TargyszavakId("84893b81-6f22-e911-80c9-00155d020b39")]
    public string Vegzes_hatalyba_lepese { get; set; }

    [TargyszavakId("1b4210ab-6f22-e911-80c9-00155d020b39")]
    public string Hatosag_altal_visszafizetett_osszeg { get; set; }

    [TargyszavakId("887e0bd3-6f22-e911-80c9-00155d020b39")]
    public string Hatosagot_terhelo_eljarasi_koltseg { get; set; }

    [TargyszavakId("d95e10f6-6f22-e911-80c9-00155d020b39")]
    public string Felfuggesztett_hatarozat { get; set; }

    [TargyszavakId("69f8042f-7122-e911-80c9-00155d020b39")]
    public string Dontes_formaja_allamigazgatasi { get; set; }
}