﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

[Serializable]
public abstract class TargyszavakTemplate
{
    public TargyszavakTemplate()
    {
    }

    public PropertyInfo[] GetProperties()
    {
        return GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
    }
}

[AttributeUsage(AttributeTargets.Property)]
public class TargyszavakIdAttribute : Attribute
{
    private readonly string _id;

    public TargyszavakIdAttribute(string id)
    {
        _id = id;
    }

    public string Id
    {
        get { return _id; }
    }
}