﻿namespace Contentum.eUtility.Models
{
    /// <summary>
    /// Model a Targy mezo meghatarozasara
    /// </summary>
    public class eBeadvany_Form_XML_Model
    {
        public int Type { get; set; }

        public string TypeId { get; set; }
        public string FormId { get; set; }
        public string FormTypeName { get; set; }
        public string Text001 { get; set; }
        public string FormUserId { get; set; }
        public string FormUserName { get; set; }
        public string FormCompanyId { get; set; }
        public string FormHirId { get; set; }
        public string FormCompanyName { get; set; }
        public string FormCompanyAddressCity { get; set; }
        public string FormCompanyAddressZip { get; set; }
        public string FormCompanyAddressId { get; set; }

        public string CpfSzervezet { get; set; }
        public string CpfKapcsolattarto { get; set; }
        public string CpfEmailAddress { get; set; }

        public string CpfDrCimJelzes { get; set; }
        public string CpfCsaladiNev { get; set; }
        public string CpfUtonev1 { get; set; }
        public string CpfUtonev2 { get; set; }
        public string CpfBenyujtoLakcimIrsz { get; set; }
        public string CpfBenyujtoLakcimTelepules { get; set; }
        public string CpfBenyujtoLakcim { get; set; }

        public string ElozmenyIktato { get; set; }
        public string ElozmenyInt { get; set; }
        public string ElozmenyNyomt { get; set; }
    }
}

