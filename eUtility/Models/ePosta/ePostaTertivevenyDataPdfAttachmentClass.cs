﻿namespace Contentum.eUtility.Models.ePosta
{
    public class ePostaTertivevenyDataPdfAttachmentClass
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public byte[] Content { get; set; }
    }
}
