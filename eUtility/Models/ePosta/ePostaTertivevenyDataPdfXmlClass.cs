﻿namespace Contentum.eUtility.Models.ePosta
{
    public class ePostaTertivevenyDataPdfXmlClass
    {
        public string Ragszam { get; set; }
        public bool AtvetelSikeres { get; set; }
        public string AtvetelIdopont { get; set; }
        public string AtvevoNeve { get; set; }
        public string AtvetelJogcim { get; set; }
        public string VisszakuldesOka { get; set; }
        public string TertivevenyVisszateresKod { get; set; }
        public string[] ErtesitesIdopontok { get; set; }
        public string TertivisszaDat { get; set; }
        public bool KezbesitesiVelelem { get; set; }
        public string KezbesitesiVelelemDatuma { get; set; }

        public string JegyzekAzonosito { get; set; }
        public string FelvetelDatuma { get; set; }

        public string KezbDatuma { get; set; }
        public string Kezbesitve { get; set; }
        public string KepFile { get; set; }
        public string VisszakezbDatuma { get; set; }
        public string VisszakezbOka { get; set; }
        public string AdatatadasNapja { get; set; }
    }
}
