﻿using Contentum.eUtility.Models.ePosta;
using System.Collections.Generic;

namespace Contentum.eUtility.Models
{
    /// <summary>
    /// Summary description for ePostaTertivevenyDataPdfClass
    /// </summary>
    public class ePostaTertivevenyDataPdfClass
    {
        public ePostaTertivevenyDataPdfClass()
        {
            Igazolas = new ePostaTertivevenyDataPdfXmlClass();
        }

        public string Ragszam { get; set; }
        public string PdfFileName { get; set; }
        public byte[] PdfFileContent { get; set; }
        public List<ePostaTertivevenyDataPdfAttachmentClass> PdfAttachments { get; set; }
        public ePostaTertivevenyDataPdfXmlClass Igazolas { get; set; }

        public bool IsValid()
        {
            if (string.IsNullOrEmpty(Ragszam))
            {
                Logger.Debug("ePostaTertivevenyDataPdfClass.IsValid:Ragszam is null");
                return false;
            }
            if (Igazolas == null)
            {
                Logger.Debug("ePostaTertivevenyDataPdfClass.IsValid:Igazolas is null");
                return false;
            }

           /* if (string.IsNullOrEmpty(Igazolas.TertivevenyVisszateresKod))
            {
                Logger.Debug("ePostaTertivevenyDataPdfClass.IsValid:Igazolas.TertivevenyVisszateresKod is null");
                return false;
            }           
            if (string.IsNullOrEmpty(Igazolas.AtvetelJogcim))
            {
                Logger.Debug("ePostaTertivevenyDataPdfClass.IsValid:Igazolas.AtvetelJogcim is null");
                return false;
            }*/
            return true;
        }

        public override bool Equals(object obj)
        {
            ePostaTertivevenyDataPdfClass @class = obj as ePostaTertivevenyDataPdfClass;
            return @class != null &&
                   Ragszam == @class.Ragszam;
        }

        public override int GetHashCode()
        {
            return 629106950 + EqualityComparer<string>.Default.GetHashCode(Ragszam);
        }
    }
}