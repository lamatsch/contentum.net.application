﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Xml;
using System.Text;
using System.Web.Configuration;

namespace Contentum.eUtility
{
    public class NETLOCKSignatureService
    {
        private string _responseXML;
        private string _errorMessage;
        private string _signedDataBase64;
        private string signatureData;
        private bool raiseError;
        private bool isFileLocal;
        private string filePath;

        public string responseXML
        {
            get { return _responseXML; }
            set { _responseXML = value; }
        }
        public string errorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        public string signedDataBase64
        {
            get { return _signedDataBase64; }
            set { _signedDataBase64 = value; }
        }
        public string SignatureData
        {
            get { return HTMLDecode(signatureData); }
            set { signatureData = value; }
        }
        public bool RaiseError
        {
            get { return raiseError; }
            set { raiseError = value; }
        }
        public bool IsFileLocal
        {
            get { return isFileLocal; }
            set { isFileLocal = value; }
        }
        public string FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }

        static string NlServerSignURL;
        static string NlServerVerifyURL;

        static NETLOCKSignatureService()
        {
            NlServerSignURL = WebConfigurationManager.AppSettings["NlServerSignURL"] ?? "http://80.77.125.50:8081/signpdfsoap";
            NlServerVerifyURL = WebConfigurationManager.AppSettings["NlServerVerifyURL"] ?? "http://80.77.125.50:8081/soapverify2";
        }

        public void SignPDF(string filename, string externalLink, string signXMLname, string signXMLstring)
        {
            SignPDF(filename, externalLink, signXMLname, signXMLstring, String.Empty, String.Empty, String.Empty);
        }

        /// <summary>
        /// Pdf aláírás Netlock-kal.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="buffer"></param>
        /// <param name="signXMLname"></param>
        /// <param name="signXMLstring"></param>
        public void SignPDF(string filename, byte[] buffer, string signXMLname, string signXMLstring)
        {
            try
            {
                string soapXMLMessage = getSignXML(filename, buffer, signXMLname, signXMLstring);
                responseXML = postXMLData(NlServerSignURL, soapXMLMessage);

                if (responseXML.Contains("Fault"))
                {
                    RaiseError = true;
                    errorMessage = extractDataFromXMLNode("SOAP-ENV:Fault", "", responseXML);
                    responseXML = "";
                }
                else
                {
                    RaiseError = false;
                    signedDataBase64 = extractDataFromXMLNode("ns:payload", "", responseXML);
                    responseXML = "";
                }
            }
            catch (Exception ex)
            {
                RaiseError = true;
                errorMessage = ex.Message;
            }
        }


        public void SignPDF(string filename, string externalLink, string signXMLname, string signXMLstring, string spUser, string spPW, string spDomain)
        {
            IsFileLocal = false;
            System.Net.WebRequest request = System.Net.WebRequest.Create(externalLink);

            if (!String.IsNullOrEmpty(spUser) && !String.IsNullOrEmpty(spPW) && !String.IsNullOrEmpty(spDomain))
            { request.Credentials = new System.Net.NetworkCredential(spUser, spPW, spDomain); }
            else
            { request.Credentials = new System.Net.NetworkCredential(); }

            try
            {
                System.Net.WebResponse response = request.GetResponse();
                System.IO.Stream s = response.GetResponseStream();
                MemoryStream repeaterMS = new MemoryStream();
                CopyStream(s, repeaterMS);
                byte[] buf = repeaterMS.ToArray();

                s.Close();

                string soapXMLMessage = getSignXML(filename, buf, signXMLname, signXMLstring);
                responseXML = postXMLData(NlServerSignURL, soapXMLMessage);

                if (responseXML.Contains("Fault"))
                {
                    RaiseError = true;
                    errorMessage = extractDataFromXMLNode("SOAP-ENV:Fault", "", responseXML);
                    responseXML = "";
                }
                else
                {
                    RaiseError = false;
                    signedDataBase64 = extractDataFromXMLNode("ns:payload", "", responseXML);
                    responseXML = "";
                }
            }
            catch (Exception ex)
            {
                RaiseError = true;
                errorMessage = ex.Message;
            }
        }

        public void SignPDF_HDD(string filePath, string fileData, string xmlName, string xmlData)
        {
            IsFileLocal = true;
            try
            {
                string fileName = System.IO.Path.GetFileName(filePath);
                this.filePath = filePath;
                string soapXMLMessage = getSignXML_Raw(fileName, fileData, xmlName, xmlData);
                responseXML = postXMLData(NlServerSignURL, soapXMLMessage);

                if (responseXML.Contains("Fault"))
                {
                    RaiseError = true;
                    errorMessage = extractDataFromXMLNode("SOAP-ENV:Fault", "", responseXML);
                    responseXML = "";
                }
                else
                {
                    RaiseError = false;
                    signedDataBase64 = extractDataFromXMLNode("ns:payload", "", responseXML);
                    responseXML = "";
                }
            }
            catch (Exception ex)
            {
                RaiseError = true;
                errorMessage = ex.Message;
                IsFileLocal = false;
            }
        }

        public void VerifyPDF(string filename, string externalLink)
        {
            VerifyPDF(filename, externalLink, String.Empty, String.Empty, String.Empty);
        }

        public void VerifyPDF(string filename, string externalLink, string spUser, string spPW, string spDomain)
        {
            IsFileLocal = false;
            System.Net.WebRequest request = System.Net.WebRequest.Create(externalLink);

            if (!String.IsNullOrEmpty(spUser) && !String.IsNullOrEmpty(spPW) && !String.IsNullOrEmpty(spDomain))
            { request.Credentials = new System.Net.NetworkCredential(spUser, spPW, spDomain); }
            else
            { request.Credentials = new System.Net.NetworkCredential(); }

            try
            {
                System.Net.WebResponse response = request.GetResponse();
                System.IO.Stream s = response.GetResponseStream();
                MemoryStream repeaterMS = new MemoryStream();
                CopyStream(s, repeaterMS);
                byte[] buf = repeaterMS.ToArray();

                s.Close();

                string soapXMLMessage = getVerifyXML(filename, buf);
                responseXML = postXMLData(NlServerVerifyURL, soapXMLMessage);

                if (responseXML.Contains("Fault"))
                {
                    RaiseError = true;
                    errorMessage = extractDataFromXMLNode("SOAP-ENV:Fault", "", responseXML);
                    responseXML = "";
                }
                else
                {
                    RaiseError = false;
                    SignatureData = System.Text.Encoding.Default.GetString(System.Convert.FromBase64String(extractDataFromXMLNode("ns:payload", "", responseXML)));
                    responseXML = "";
                }
            }
            catch (Exception ex)
            {
                RaiseError = true;
                errorMessage = ex.Message;
            }
        }

        public void VerifyPDF_HDD(string filePath, string fileData)
        {
            IsFileLocal = true;
            try
            {
                string filename = System.IO.Path.GetFileName(filePath);
                this.filePath = filePath;

                string soapXMLMessage = getVerifyXML_Raw(filename, fileData);
                responseXML = postXMLData(NlServerVerifyURL, soapXMLMessage);

                if (responseXML.Contains("Fault"))
                {
                    RaiseError = true;
                    errorMessage = extractDataFromXMLNode("SOAP-ENV:Fault", "", responseXML);
                    responseXML = "";
                }
                else
                {
                    RaiseError = false;
                    SignatureData = System.Text.Encoding.Default.GetString(System.Convert.FromBase64String(extractDataFromXMLNode("ns:payload", "", responseXML)));
                    responseXML = "";
                }
            }
            catch (Exception ex)
            {
                RaiseError = true;
                errorMessage = ex.Message;
                IsFileLocal = false;
            }
        }

        public bool SaveLocalSignedPDF()
        {
            if (IsFileLocal && !String.IsNullOrEmpty(signedDataBase64) && !String.IsNullOrEmpty(filePath))
            {
                try
                {
                    System.IO.File.WriteAllText(filePath, fromBase64String(signedDataBase64), Encoding.Default);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        #region private methods
        private string getSignXML(string fileName, byte[] buffer)
        {
            return getSignXML(fileName, buffer, String.Empty, String.Empty);
        }

        private string getSignXML(string fileName, byte[] buffer, string signXMLName, string signXMLString)
        {
            string xmlString = @"<?xml version=""1.0"" encoding=""UTF-8""?> <SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/""" +
                                    @" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""" +
                                    @" xmlns:ns=""http://www.netlock.hu/WebServices/ynsoap""> <SOAP-ENV:Body> <ns:yaqudjobex> <ns:name>" + fileName + "</ns:name> <ns:payload>";
            string Base64Filestream_String = "";
            try
            {
                Base64Filestream_String = System.Convert.ToBase64String(buffer);
            }
            catch
            {

            }
            xmlString += Base64Filestream_String + @"</ns:payload> <ns:nonce>190000</ns:nonce>";
            if (!String.IsNullOrEmpty(signXMLName) && !String.IsNullOrEmpty(signXMLString))
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(signXMLString);
                string Base64SignXMLString = System.Convert.ToBase64String(bytes);
                xmlString += @"<ns:yattms SOAP-ENC:arrayType=""ns-yaqudAttm[1]""> " +
                    @"<ns:item> <name>" + signXMLName + @"</name> <description>digital signed by yaqud</description> <value>" + Base64SignXMLString +
                    @"</value> </ns:item> </ns:yattms> </ns:yaqudjobex>  </SOAP-ENV:Body> </SOAP-ENV:Envelope>";
            }
            else
            {
                xmlString += @"<ns:yattms></ns:yattms> </ns:yaqudjobex>  </SOAP-ENV:Body> </SOAP-ENV:Envelope>";
            }
            return xmlString;
        }

        private string getSignXML_Raw(string fileName, string fileData, string signXMLName, string signXMLString)
        {
            string Base64Filestream_String = toBase64String(fileData);
            string xmlString = @"<?xml version=""1.0"" encoding=""UTF-8""?> <SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/""" +
                                    @" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""" +
                                    @" xmlns:ns=""http://www.netlock.hu/WebServices/ynsoap""> <SOAP-ENV:Body> <ns:yaqudjobex> <ns:name>" + fileName + "</ns:name> <ns:payload>"
                + Base64Filestream_String + @"</ns:payload> <ns:nonce>190000</ns:nonce>";

            if (!String.IsNullOrEmpty(signXMLName) && !String.IsNullOrEmpty(signXMLString))
            {
                string Base64SignXMLString = toBase64String(signXMLString);
                xmlString += @"<ns:yattms SOAP-ENC:arrayType=""ns-yaqudAttm[1]""> " +
                @"<ns:item> <name>" + signXMLName + @"</name> <description>digital signed by yaqud</description> <value>" + Base64SignXMLString +
                @"</value> </ns:item> </ns:yattms> </ns:yaqudjobex>  </SOAP-ENV:Body> </SOAP-ENV:Envelope>";
            }
            else
            {
                xmlString += @"<ns:yattms></ns:yattms> </ns:yaqudjobex>  </SOAP-ENV:Body> </SOAP-ENV:Envelope>";
            }
            return xmlString;
        }

        private string getVerifyXML(string fileName, byte[] buffer)
        {
            string xmlString = @"<?xml version=""1.0"" encoding=""UTF-8""?> <SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/""" +
                                @" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""" +
                                @" xmlns:ns=""http://www.netlock.hu/WebServices/ynsoap""> <SOAP-ENV:Body> <ns:yaqudjobex> <ns:name>" + fileName + "</ns:name> <ns:payload>";
            string Base64Filestream_String = "";
            try
            {
                Base64Filestream_String = System.Convert.ToBase64String(buffer);
            }
            catch
            {

            }

            // CR 3033  yattms tag kiszedése  -- nekrisz
            //xmlString += Base64Filestream_String + @"</ns:payload> <ns:nonce>190000</ns:nonce> <ns:yattms SOAP-ENC:arrayType=""ns-yaqudAttm[1]""> " +
            //    @"<ns:item> <name></name> <description>digital signed by yaqud</description> <value></value> </ns:item> </ns:yattms> </ns:yaqudjobex>  </SOAP-ENV:Body> </SOAP-ENV:Envelope>";
            xmlString += Base64Filestream_String + @"</ns:payload> <ns:nonce>190000</ns:nonce> </ns:yaqudjobex>  </SOAP-ENV:Body> </SOAP-ENV:Envelope>";
            return xmlString;
        }

        private string getVerifyXML_Raw(string fileName, string fileData)
        {
            string data = toBase64String(fileData);
            // CR 3033  yattms tag kiszedése  -- nekrisz
            //string xmlString = @"<?xml version=""1.0"" encoding=""UTF-8""?> <SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/""" +
            //        @" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""" +
            //        @" xmlns:ns=""http://www.netlock.hu/WebServices/ynsoap""> <SOAP-ENV:Body> <ns:yaqudjobex> <ns:name>" + fileName + "</ns:name> <ns:payload>" +
            //        data + @"</ns:payload> <ns:nonce>190000</ns:nonce> <ns:yattms SOAP-ENC:arrayType=""ns-yaqudAttm[1]""> " +
            //    @"<ns:item> <name></name> <description>digital signed by yaqud</description> <value></value> </ns:item> </ns:yattms> </ns:yaqudjobex>  </SOAP-ENV:Body> </SOAP-ENV:Envelope>";
            string xmlString = @"<?xml version=""1.0"" encoding=""UTF-8""?> <SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/""" +
                    @" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""" +
                    @" xmlns:ns=""http://www.netlock.hu/WebServices/ynsoap""> <SOAP-ENV:Body> <ns:yaqudjobex> <ns:name>" + fileName + "</ns:name> <ns:payload>" +
                    data + @"</ns:payload> <ns:nonce>190000</ns:nonce> </ns:yaqudjobex>  </SOAP-ENV:Body> </SOAP-ENV:Envelope>";
            return xmlString;
        }

        private string postXMLData(string destinationUrl, string requestXml)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }
            return null;
        }

        private string extractDataFromXMLNode(string xPath, string field, string rawXMLString)
        {
            string fieldValue = "";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(rawXMLString);

            if (String.IsNullOrEmpty(field))
            {
                XmlNodeList nodes = xmlDoc.GetElementsByTagName(xPath);
                foreach (XmlNode _item in nodes)
                {
                    fieldValue += _item.InnerText;
                }
            }
            else
            {
                XmlNodeList nodes = xmlDoc.GetElementsByTagName(xPath);
                XmlNode node = xmlDoc.SelectSingleNode("//" + field);
                if (node != null)
                {
                    fieldValue = node.InnerText;
                }
            }

            return fieldValue;
        }

        private static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[32768];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
        }

        private static string toBase64String(string input)
        {
            byte[] bytes = Encoding.Default.GetBytes(input);
            try
            {
                return System.Convert.ToBase64String(bytes);
            }
            catch
            {
                return input;
            }
        }

        private static string fromBase64String(string input)
        {
            try
            {
                return Encoding.Default.GetString(System.Convert.FromBase64String(input));
            }
            catch
            {
                return input;
            }
        }

        public static string HTMLDecode(string input)
        {
            return HttpUtility.UrlDecode(input.Replace("\\", "%"));
        }
        #region XML átalakítása bulletpointos felsorolássá (kivéve, nem kell)
        /*private static string formatSignatureXML(string xml)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(xml);
            List<string> signatureDataList = new List<string>();
            foreach (XmlNode node in xDoc)
            {
                if (node.Name.ToLower() == "xml")
                    continue;
                recursiveNodeSearch(node, signatureDataList);
            }
            return string.Join(Environment.NewLine, signatureDataList.ToArray());
        }

        private static void recursiveNodeSearch(XmlNode node, List<string> list_toload)
        {
            string formatString = new String('\t', getParentNodeCount(node, 0));
            if (node.ChildNodes.Count != 0)
            {
                foreach(XmlNode _item in node.ChildNodes)
                {
                    if (_item.ChildNodes.Count == 1 && _item.ChildNodes[0].Name.ToLower() == "#text")
                    {
                        list_toload.Add(formatString + "• " + _item.Name + ": " + _item.InnerText);
                    }
                    else
                    {
                        list_toload.Add(formatString + "• " + _item.Name);
                        recursiveNodeSearch(_item, list_toload);
                    }
                }
            }
            else
            {
                list_toload.Add(formatString + "• " + node.Name + ": " + node.InnerText);
            }
        }

        private static int getParentNodeCount(XmlNode node, int count)
        {
            int _return = 0;
            if (node.ParentNode.Name != "#document")
            {
                count += 1;
                _return = getParentNodeCount(node.ParentNode, count);
            }
            else
            {
                _return = count;
            }
            return _return;
        }*/
        #endregion

        #endregion
    }
}