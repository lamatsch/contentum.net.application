﻿using System;
using System.Text.RegularExpressions;

namespace Contentum.eUtility.Netlock
{
    public class CertificateData
    {
        public string CommonName { get; set; }
        public string Email { get; set; }
        public string Organization { get; set; }

        public static CertificateData Parse(string input)
        {
            string cn = GetField(input, "CN");
            string emailAddress = GetField(input, "emailAddress");
            string o = GetField(input, "O");

            CertificateData data = new CertificateData();
            data.CommonName = cn;
            data.Email = emailAddress;
            data.Organization = o;
            return data;
        }

        private static string GetField(string input, string fieldName)
        {
            string pattern = String.Format("{0} = ([^=;]*);?", fieldName);
            Match m = Regex.Match(input, pattern);

            if (m.Success)
            {
                return m.Groups[1].Value.TrimStart();
            }
            else
            {
                return String.Empty;
            }
        }

        public override string ToString()
        {
            if (!String.IsNullOrEmpty(Email))
                return String.Format("{0} <{1}>", this.CommonName, this.Email);

            return this.CommonName;
        }
    }
}
