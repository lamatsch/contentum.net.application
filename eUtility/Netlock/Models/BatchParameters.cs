/*
 * SignAssist
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.14.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class BatchParameters : IEquatable<BatchParameters>
    { 
        /// <summary>
        /// Id of user in CloudSign to sign with. If this parameter is specified, cloudSignDomainId MUST NOT be specified. If this parameter is not specified, cloudSignDomainId MUST be specified. 
        /// </summary>
        /// <value>Id of user in CloudSign to sign with. If this parameter is specified, cloudSignDomainId MUST NOT be specified. If this parameter is not specified, cloudSignDomainId MUST be specified. </value>
        [DataMember(Name="cloudSignUserId")]
        public int? CloudSignUserId { get; set; }

        /// <summary>
        /// Id of domain in CloudSign that contains the user to sign with. The user will be identified via authentication. If this parameter is specified, cloudSignUserId MUST NOT be specified. If this parameter is not specified, cloudSignUserId MUST be specified. 
        /// </summary>
        /// <value>Id of domain in CloudSign that contains the user to sign with. The user will be identified via authentication. If this parameter is specified, cloudSignUserId MUST NOT be specified. If this parameter is not specified, cloudSignUserId MUST be specified. </value>
        [DataMember(Name="cloudSignDomainId")]
        public int? CloudSignDomainId { get; set; }

        /// <summary>
        /// Id of certificate in CloudSign. If the user has multiple certificates, this can be used to control the user-selectable certificates. If cloudSignForceCertificate is set, this will be the used certificate, and the user will have no options to select another certificate. If cloudSignForceCertificate is not set, this will be the default selected certificate, but the user will be able to select annother certificate. 
        /// </summary>
        /// <value>Id of certificate in CloudSign. If the user has multiple certificates, this can be used to control the user-selectable certificates. If cloudSignForceCertificate is set, this will be the used certificate, and the user will have no options to select another certificate. If cloudSignForceCertificate is not set, this will be the default selected certificate, but the user will be able to select annother certificate. </value>
        [DataMember(Name="cloudSignCertificateId")]
        public int? CloudSignCertificateId { get; set; }

        /// <summary>
        /// This controls the option for selecting a certificate by the user as described above.
        /// </summary>
        /// <value>This controls the option for selecting a certificate by the user as described above.</value>
        [DataMember(Name="cloudSignForceCertificate")]
        public bool? CloudSignForceCertificate { get; set; }

        /// <summary>
        /// Gets or Sets SignAssistRemoteSignatureParameters
        /// </summary>
        [DataMember(Name="signAssistRemoteSignatureParameters")]
        public SignAssistRemoteSignatureParameters SignAssistRemoteSignatureParameters { get; set; }

        /// <summary>
        /// After signing a document, a validation is automatically performed. To skip this validation, set this to false.
        /// </summary>
        /// <value>After signing a document, a validation is automatically performed. To skip this validation, set this to false.</value>
        [DataMember(Name="signAssistSkipValidate")]
        public bool? SignAssistSkipValidate { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class BatchParameters {\n");
            sb.Append("  CloudSignUserId: ").Append(CloudSignUserId).Append("\n");
            sb.Append("  CloudSignDomainId: ").Append(CloudSignDomainId).Append("\n");
            sb.Append("  CloudSignCertificateId: ").Append(CloudSignCertificateId).Append("\n");
            sb.Append("  CloudSignForceCertificate: ").Append(CloudSignForceCertificate).Append("\n");
            sb.Append("  SignAssistRemoteSignatureParameters: ").Append(SignAssistRemoteSignatureParameters).Append("\n");
            sb.Append("  SignAssistSkipValidate: ").Append(SignAssistSkipValidate).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((BatchParameters)obj);
        }

        /// <summary>
        /// Returns true if BatchParameters instances are equal
        /// </summary>
        /// <param name="other">Instance of BatchParameters to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(BatchParameters other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    CloudSignUserId == other.CloudSignUserId ||
                    CloudSignUserId != null &&
                    CloudSignUserId.Equals(other.CloudSignUserId)
                ) && 
                (
                    CloudSignDomainId == other.CloudSignDomainId ||
                    CloudSignDomainId != null &&
                    CloudSignDomainId.Equals(other.CloudSignDomainId)
                ) && 
                (
                    CloudSignCertificateId == other.CloudSignCertificateId ||
                    CloudSignCertificateId != null &&
                    CloudSignCertificateId.Equals(other.CloudSignCertificateId)
                ) && 
                (
                    CloudSignForceCertificate == other.CloudSignForceCertificate ||
                    CloudSignForceCertificate != null &&
                    CloudSignForceCertificate.Equals(other.CloudSignForceCertificate)
                ) && 
                (
                    SignAssistRemoteSignatureParameters == other.SignAssistRemoteSignatureParameters ||
                    SignAssistRemoteSignatureParameters != null &&
                    SignAssistRemoteSignatureParameters.Equals(other.SignAssistRemoteSignatureParameters)
                ) && 
                (
                    SignAssistSkipValidate == other.SignAssistSkipValidate ||
                    SignAssistSkipValidate != null &&
                    SignAssistSkipValidate.Equals(other.SignAssistSkipValidate)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (CloudSignUserId != null)
                    hashCode = hashCode * 59 + CloudSignUserId.GetHashCode();
                    if (CloudSignDomainId != null)
                    hashCode = hashCode * 59 + CloudSignDomainId.GetHashCode();
                    if (CloudSignCertificateId != null)
                    hashCode = hashCode * 59 + CloudSignCertificateId.GetHashCode();
                    if (CloudSignForceCertificate != null)
                    hashCode = hashCode * 59 + CloudSignForceCertificate.GetHashCode();
                    if (SignAssistRemoteSignatureParameters != null)
                    hashCode = hashCode * 59 + SignAssistRemoteSignatureParameters.GetHashCode();
                    if (SignAssistSkipValidate != null)
                    hashCode = hashCode * 59 + SignAssistSkipValidate.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(BatchParameters left, BatchParameters right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(BatchParameters left, BatchParameters right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
