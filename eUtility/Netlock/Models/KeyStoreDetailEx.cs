﻿using System;
using System.Collections.Generic;

namespace IO.Swagger.Models
{
    public partial class KeyStoreDetail
    {

        public string Password { get; set; }

        public List<string> KeyHolders = new List<string>() ;

        public KeyStoreDetail(string name, string password, string[] keyHolders)
        {
            Name = name;
            Password = password;
            if (keyHolders != null) {
                KeyHolders.AddRange(keyHolders);
            }
        }

        public void CheckKeyHolder(string keyHolderAlias)
        {
            if (!KeyHolders.Contains(keyHolderAlias)) {
                throw new Exception($"Key holder {keyHolderAlias} not found.");
            }
        }
    }
}
