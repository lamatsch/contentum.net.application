﻿using System;

namespace IO.Swagger.Models
{
    public class SignatureSession
    {
        public string SessionId { get; }
        public KeyStoreDetail KeyStore { get; }

        public SignatureSession(KeyStoreDetail keyStore) {
            SessionId = Guid.NewGuid().ToString();
            KeyStore = keyStore;
        }
    }
}
