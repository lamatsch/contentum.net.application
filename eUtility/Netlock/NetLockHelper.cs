﻿using Contentum.eUtility.Netlock;
using IO.Swagger.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using Newtonsoft.Json;

namespace Contentum.eUtility.NetLockSignAssist
{
    public static class NetLockHelper
    {
        public static byte[] DownloadDocument(string externalLink, string spUser, string spPW, string spDomain)
        {
            using (var webClient = new WebClient())
            {
                if (!String.IsNullOrEmpty(spUser) && !String.IsNullOrEmpty(spPW) && !String.IsNullOrEmpty(spDomain))
                {
                    webClient.Credentials = new NetworkCredential(spUser, spPW, spDomain);
                }

                return webClient.DownloadData(externalLink);
            }
        }

        private static DateTime? GetTimeStamp(List<TimestampReport> timestampReports, TimestampReport.TypeEnum type)
        {
            var tsFound = timestampReports.FirstOrDefault(ts => ts.Type == type);
            return tsFound == null ? null : tsFound.ProductionTime;
        }

        public static SignatureData ToSignatureData(this SignatureReport signature)
        {
            return new SignatureData
            {
                TimeStampDate = GetTimeStamp(signature.Timestamps, TimestampReport.TypeEnum.SIGNATURETIMESTAMPEnum),
                CertificateValidityFrom = null,
                CertificateValidityTo = null,
                CertificateSubject = CertificateData.Parse(signature.SigningCertificateSubjectDN),
                CertificateIssuer = CertificateData.Parse(signature.SigningCertificateIssuerDN)
            };
        }

        public static ValidationReport DeserializeValidation(string validationReportJson)
        {
            try
            {
                if (!String.IsNullOrEmpty(validationReportJson))
                {
                    return JsonConvert.DeserializeObject<ValidationReport>(validationReportJson);
                }
            }
            catch (Exception x)
            {
                Logger.Error("ValidationReport Deserialize ERROR", x);
            }
            return null;
        }

        public static void Dump(this ValidationReport validationReport)
        {
            if (validationReport == null)
            {
                Logger.Warn("validationReport is null");
                return;
            }

            if (validationReport.Signatures != null)
            {
                validationReport.Signatures.ForEach(s => s.Dump());
            }
        }

        public static void Dump(this SignatureReport signature)
        {
            if (signature == null)
            {
                Logger.Warn("signatureReport is null");
                return;
            }

            // indication
            Logger.Info("Indication=" + signature.Indication);

            // subindication
            if (signature.SubIndication != null)
            {
                Logger.Warn("SubIndication=" + signature.SubIndication);
            }

            // info
            if (signature.Info != null)
            {
                signature.Info.ForEach(info => Logger.Info(info));
            }

            // warnings
            if (signature.Warnings != null)
            {
                signature.Warnings.ForEach(warning => Logger.Warn(warning));
            }

            // errors
            if (signature.Errors != null)
            {
                signature.Errors.ForEach(error => Logger.Error(error));
            }
        }
    }
}
