﻿using System;
using System.Net;
using Contentum.eBusinessDocuments;
using RestSharp;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Web.Configuration;
using IO.Swagger.Models;

namespace Contentum.eUtility.NetLockSignAssist
{
    public class NetLockSignAssistService
    {
        protected string ParameterToString(object obj)
        {
            if (obj is DateTime)
                // Return a formatted date string - Can be customized with Configuration.DateTimeFormat
                // Defaults to an ISO 8601, using the known as a Round-trip date/time pattern ("o")
                // https://msdn.microsoft.com/en-us/library/az4se3k1(v=vs.110).aspx#Anchor_8
                // For example: 2009-06-15T13:45:30.0000000
                // ISO8601_DATETIME_FORMAT
                return ((DateTime)obj).ToString("o");
            else if (obj is List<string>)
                return String.Join(",", (obj as List<string>).ToArray());
            else
                return System.Convert.ToString(obj);
        }

        protected string Serialize(object obj)
        {
            return obj != null ? JsonConvert.SerializeObject(obj) : null;
        }

        public NetLockSignAssistService()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback += CertificationCallBack;
        }

        ~NetLockSignAssistService()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback -= CertificationCallBack;
        }

        protected bool CertificationCallBack(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public class NetlockSignAssistException : ResultException
        {
            public NetlockSignAssistException(HttpStatusCode errorStatus,
                                             string message,
                                             string content) : base(string.Format(Constants.ERROR_MESSAGE_BASE, (int)errorStatus) + Environment.NewLine + message + Environment.NewLine + content)
            {
            }
        }

        protected static class Settings
        {
            public static string UrlBase { get { return GetSetting("NetLockServerUrl", "https://localhost:44397/api/"); } }
            public static string KeyStoreName { get { return GetSetting("NetLockKeyStoreName", "TestKeyStore1"); } }
            public static string KeyStorePassword { get { return GetSetting("NetLockKeyStorePassword", "pwd1"); } }
            public static string KeyHolderName { get { return GetSetting("NetLockKeyHolderName", "KeyHolder1a"); } }

            private static string GetSetting(string key, string defaultValue)
            {
                return WebConfigurationManager.AppSettings[key] ?? defaultValue;
            }
        }

        protected static class Constants
        {
            public const string USER_AGENT = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)";
            public const string ERROR_MESSAGE_BASE = "Hiba ({0}) a NetLock SignAssist hívása közben.";
        }

        public static class Paths
        {
            //public static string ConstructUrl(string path)
            //{
            //    return Settings.UrlBase + path;
            //}

            public const string INITIALIZE_KEY_STORE = @"keystores/{keyStoreName}/actions/initialize";
            public const string ACTIVATE_KEY_STORE = @"keystores/{keyStoreName}/actions/activate";
            public const string DEACTIVATE_KEY_STORE = @"keystores/{keyStoreName}/actions/deactivate";
            public const string BEGIN_SESSION = @"signature-sessions";
            public const string SET_PARAMETERS = @"signature-sessions/{sessionId}/parameters";
            public const string SIGN = @"signature-sessions/{sessionId}/sign";
            public const string VALIDATE = @"validation/validate";
        }

        public ValidationReport ValidationReport; 

        private void HandleResponse(IRestResponse response, string msg)
        {
            ValidationReport = null;
            if (response.ErrorException != null)
            {
                throw response.ErrorException;
            }

            foreach (var h in response.Headers)
            {
                if (h.Name.Equals("X-VALIDATION-REPORT-PRESENT", StringComparison.InvariantCultureIgnoreCase))
                {
                    try
                    {
                        ValidationReport = JsonConvert.DeserializeObject<ValidationReport>(response.Content);
                    }
                    catch(Exception x)
                    {
                        Logger.Error("validationReport deserialization failed", x);
                    }
                }
            }

            var statusCode = (int)response.StatusCode;
            if (statusCode == 0 || statusCode >= 400)
            {
                throw new NetlockSignAssistException(response.StatusCode, msg, response.Content);
            }
        }

        protected IRestResponse CallRestService(string path,
                                                RestSharp.Method method,
                                                Action<RestRequest> setup)
        {
            var client = new RestClient(Settings.UrlBase);
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            RestRequest request = new RestRequest(path, method);

            if (setup != null)
            {
                setup(request);

                request.AddHeader("content-type", "application/json");

                // request.AddParameter("name", "value"); // adds to POST or URL querystring based on Method
                // request.AddUrlSegment("keyStoreName", "TestKeyStore1"); // replaces matching token in request.Resource
                // easily add HTTP Headers
                // request.AddHeader("header", "value");
                // request.AddJsonBody("pwd1");
                request.RequestFormat = DataFormat.Json;
            }

            // Execute the request
            IRestResponse response = client.Execute(request);

            // var content = response.Content; // raw content as string
            return response;
        }

        protected void InitializeKeyStore(string keyStoreName, string password)
        {
            var response = CallRestService(Paths.INITIALIZE_KEY_STORE, Method.POST, request =>
            {
                request.AddUrlSegment("keyStoreName", keyStoreName);
                request.AddJsonBody(password);
            });

            HandleResponse(response, "Nem sikerült inicializálni a kulcstárat.");
        }

        protected void ActivateKeyStore(string keyStoreName, string password)
        {
            var response = CallRestService(Paths.ACTIVATE_KEY_STORE, Method.PUT, request =>
            {
                request.AddUrlSegment("keyStoreName", keyStoreName);
                request.AddJsonBody(password);
            });

            HandleResponse(response, "Nem sikerült aktiválni a kulcstárat.");
        }

        protected void DeactivateKeyStore(string keyStoreName, string password)
        {
            var response = CallRestService(Paths.DEACTIVATE_KEY_STORE, Method.PUT, request =>
            {
                request.AddUrlSegment("keyStoreName", keyStoreName);
            });

            HandleResponse(response, "Nem sikerült inaktiválni a kulcstárat.");
        }

        protected string BeginSignatureSession(string keyHolderName)
        {
            var response = CallRestService(Paths.BEGIN_SESSION, Method.PUT, request =>
            {
                request.AddJsonBody(keyHolderName);
            });

            HandleResponse(response, "Nem sikerült munkamenetet létrehozni.");

            return response.Content;
        }

        protected void SetRemoteSignatureParameters(string sessionId)
        {
            var response = CallRestService(Paths.SET_PARAMETERS, Method.PUT, request =>
            {
                request.AddUrlSegment("sessionId", sessionId);
                JsonObject parameters = new JsonObject();
                parameters.Add("signatureLevel", "XAdES_BASELINE_T");
                parameters.Add("signaturePackaging", "ENVELOPING");
                request.AddJsonBody(parameters);
            });

            HandleResponse(response, "Nem sikerült beállítani a paramétereket.");
        }

        protected byte[] SignInSession(string sessionId,
                                       string documentName,
                                       byte[] documentContent,
                                       string mimeType,
                                       bool skipValidate,
                                       bool offline)
        {
            var response = CallRestService(Paths.SIGN, Method.POST, request =>
            {
                request.AddUrlSegment("sessionId", sessionId);

                if (!string.IsNullOrEmpty(documentName))
                {
                    request.AddQueryParameter("doc-name", ParameterToString(documentName));
                }

                if (!string.IsNullOrEmpty(mimeType))
                {
                    request.AddQueryParameter("mime-type", ParameterToString(mimeType));
                }

                request.AddQueryParameter("skip-validate", ParameterToString(skipValidate));
                request.AddQueryParameter("offline", ParameterToString(offline));

                request.AddJsonBody(System.Convert.ToBase64String(documentContent));
            });

            HandleResponse(response, "Nem sikerült az aláírás.");

            var signedData = JsonConvert.DeserializeObject<string>(response.Content);
            return System.Convert.FromBase64String(signedData);
        }

        protected T Sign<T>(string keyholderName,
                            Func<string, T> signInSession)
        {
            var result = new Result();
            InitializeKeyStore(Settings.KeyStoreName, Settings.KeyStorePassword);
            ActivateKeyStore(Settings.KeyStoreName, Settings.KeyStorePassword);
            try
            {
                var sessionId = BeginSignatureSession(keyholderName);
                SetRemoteSignatureParameters(sessionId);
                return signInSession(sessionId);
            }
            finally
            {
                DeactivateKeyStore(Settings.KeyStoreName, Settings.KeyStorePassword);
            }
        }

        public byte[] Sign(string keyholderName,
                           string filename,
                           byte[] documentContent)
        {
            if (String.IsNullOrEmpty(keyholderName))
            {
                keyholderName = Settings.KeyHolderName;
            }

            return Sign(keyholderName, sessionId =>
            {
                var signedDocument = SignInSession(sessionId, filename, documentContent, null, false, false);
                // Pass back signed document
                return signedDocument;
            });
        }

        public string Validate(string documentName,
                               byte[] documentContent)
        {
            var response = CallRestService(Paths.VALIDATE, Method.PUT, request =>
            {
                if (!string.IsNullOrEmpty(documentName))
                {
                    request.AddQueryParameter("doc-name", ParameterToString(documentName));
                }

                request.AddQueryParameter("offline", ParameterToString(false));
                request.AddJsonBody(System.Convert.ToBase64String(documentContent));
            });

            HandleResponse(response, "Nem sikerült a validálás.");

            return response.Content;
        }
    }
}