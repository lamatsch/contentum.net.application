﻿using IO.Swagger.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Contentum.eUtility.NetLockSignAssist;

namespace Contentum.eUtility.Netlock
{
    public class SignatureData
    {
        public CertificateData CertificateSubject { get; set; }
        public CertificateData CertificateIssuer { get; set; }
        public DateTime? CertificateValidityFrom { get; set; }
        public DateTime? CertificateValidityTo { get; set; }
        public DateTime? TimeStampDate { get; set; }

        public string ToHTML(Dictionary<string, string> additionalInformation)
        {
            StringBuilder sb = new StringBuilder();
            //HttpUtility.UrlEncode
            sb.AppendFormat("<b>Tanusítvány:</b> {0}", HttpUtility.HtmlEncode(CertificateSubject.ToString()));
            sb.Append("<br/>");
            sb.AppendFormat("<b>Kibocsátó:</b> {0}", HttpUtility.HtmlEncode(CertificateIssuer.ToString()));
            sb.Append("<br/>");
            sb.AppendFormat("<b>Tanusítvány érvényességi ideje:</b> {0}", CertificateValidityTo);
            sb.Append("<br/>");
            sb.AppendFormat("<b>Idõpecsét ideje:</b> {0}", TimeStampDate);

            if (additionalInformation != null)
            {
                foreach (KeyValuePair<string, string> kppair in additionalInformation)
                {

                    if (!string.IsNullOrEmpty(kppair.Key) && !string.IsNullOrEmpty(kppair.Value))
                    {
                        sb.Append("<br/>");
                        string propertyName = getTranslatedName(kppair.Key);
                        string propertyValue = translateYesNo(kppair.Value);
                        sb.AppendFormat("<b>{0}:</b> {1}", propertyName, propertyValue);
                    }
                }
            }

            return sb.ToString();
        }

        //vfyflgtxt: embcrl_req, embcrl_use, TS, p7vfy, tsvfy, SC_exp, purp_smime
        //csub: C = --; ST = --; L = --; O = --; OU = --; CN = NetLock Teszt Aláíró tanúsítvány; emailAddress = lamatsch.andras@axis.hu
        //ciss: C = HU; L = Budapest; O = NetLock Kft.; OU = Tanúsítványkiadók (Certification Services); CN = NetLock Teszt (Class T4) CA; emailAddress = info@netlock.hu
        //cvf: Apr 13 14:20:09 2016 GMT
        //cvt: May 13 14:20:09 2016 GMT
        //tsat: 2016.05.13. 15:05:27
        //tscsub: C = HU; L = Budapest; O = NetLock Kft.; OU = Idõbélyeg Szolgáltatók; CN = NetLock Minõsített Idõbélyeg Szolgáltató 3; emailAddress = info@netlock.hu
        //tsciss: C = HU; L = Budapest; O = NetLock Kft.; OU = Tanúsítványkiadók (Certification Services); CN = NetLock Minõsített Eat. (Class Q Legal) Tanúsítványkiadó
        //tscvf: Oct 20 20:16:29 2015 GMT
        //tscvt: Jul 24 20:16:29 2024 GMT
        /*public static SignatureData Parse(string signature)
        {
            string csub = GetSignatureField(signature, "csub");
            string ciss = GetSignatureField(signature, "ciss");
            string cvf = GetSignatureField(signature, "cvf");
            string cvt = GetSignatureField(signature, "cvt");

            string tsat = GetSignatureField(signature, "tsat");
            string tscsub = GetSignatureField(signature, "tscsub");
            string tsciss = GetSignatureField(signature, "tsciss");
            string tscvf = GetSignatureField(signature, "tscvf");
            string tscvt = GetSignatureField(signature, "tscvf");

            SignatureData data = new SignatureData();

            data.CertificateSubject = CertificateData.Parse(csub);
            data.CetificateIssuer = CertificateData.Parse(ciss);
            data.CertificateValidityFrom = ParseDateTime(cvf, dateFormat1);
            data.CertificateValidityTo = ParseDateTime(cvt, dateFormat1);
            data.TimeStampDate = ParseDateTime(tsat, dateFormat2);

            return data;
        }*/

        public static List<SignatureData> ParseNetLockSignAssistValidationReport(ValidationReport validationReport, SignatureReport.IndicationEnum? indicationToList)
        {
            var res = new List<SignatureData>();
            try
            {
                if (validationReport == null)
                {
                    return res;
                }

                if (validationReport.Signatures != null)
                { 
                    foreach (var signature in validationReport.Signatures)
                    {
                        if (signature == null)
                        {
                            continue;
                        }

                        signature.Dump();

                        // check indication
                        if (indicationToList != null && signature.Indication != indicationToList)
                        {
                            continue;
                        }

                        // convert signature data
                        var signatureData = signature.ToSignatureData();
                        res.Add(signatureData);
                    }
                }
            }
            catch (Exception x)
            {
                Logger.Error("SignatureData.ParseNetLockSignAssistValidationReport ERROR", x);
            }
            return res;
        }

        public static SignatureData ParseFromDict(Dictionary<string, string> signatureDictionary)
        {
            SignatureData data = new SignatureData();
            try
            {
                if (signatureDictionary["signerDN"] != null)
                    data.CertificateSubject = CertificateData.Parse(signatureDictionary["signerDN"].Replace(",", ";").Replace("=", " = "));
                if (signatureDictionary["issuer"] != null)
                    data.CertificateIssuer = CertificateData.Parse(signatureDictionary["issuer"].Replace(",", ";").Replace("=", " = "));
                if (signatureDictionary["issuerValidTo"] != null)
                {
                    data.CertificateValidityTo = ParseDateTime(signatureDictionary["issuerValidTo"], dateFormat2);
                    if (data.CertificateValidityTo == null)
                        data.CertificateValidityTo = ParseDateTime(signatureDictionary["issuerValidTo"], dateFormat3);
                }
                if (signatureDictionary["signerTime"] != null)
                {
                    data.TimeStampDate = ParseDateTime(signatureDictionary["signerTime"], dateFormat2);
                    if (data.TimeStampDate == null)
                        data.TimeStampDate = ParseDateTime(signatureDictionary["signerTime"], dateFormat3);
                }
            }
            catch (Exception) { }
            return data;
        }

        private static string GetSignatureField(string signature, string fieldName)
        {
            string pattern = String.Format("{0}:(.*)", fieldName);
            Match m = Regex.Match(signature, pattern);

            if (m.Success)
            {
                return m.Groups[1].Value.Trim();
            }
            else
            {
                return String.Empty;
            }
        }

        const string dateFormat1 = "MMM dd HH':'mm':'ss yyyy 'GMT'";
        const string dateFormat2 = "yyyy'.'MM'.'dd'.' HH':'mm':'ss";
        const string dateFormat3 = "yyyy'.'MM'.'dd'.' H':'mm':'ss";
        private static DateTime? ParseDateTime(string dateString, string format)
        {
            DateTime dt;

            if (DateTime.TryParseExact(dateString, format, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AllowWhiteSpaces, out dt))
            {
                return dt;
            }

            return null;
        }

        #region BLG 2947 aláírás felülvizsgálat kiegészítõk
        private static Dictionary<string, string> jSonElementDictionary = new Dictionary<string, string>()
        {
            {"AlairasFelulvizsgalatEredmeny", "Aláírás felülvizsgálat eredménye"},
            {"signerDN", "Aláíró adatai"},
            {"timeStampDate","Idõpecsét ideje" },
            {"name", "Név"},
            {"issuer", "Kibocsátó"},
            {"issuerValidFrom", "Tanusítvány érvényességi ideje"},
            {"issuerValidTo", "Tanusítvány érvényességi ideje"},
            {"signerTime", "Aláíró idõpont"},
            {"signerCoversWholeDocument", "Aláírás érvényes az egész dokumentumra"},
            {"revision", "Felülvizsgálat"},
            {"padesType", "Betét típus"},
            {"subFilter", "Másodlagos szûrõ"},
            {"isLTVEnabled", "LTVE engedélyezett"},
            {"qualified", "Minõsített"},
            {"regulation", "Szabályozás"},
            {"compliance", "Teljesítés"},
            {"keyOnQSCD", "QSCD kulcs"},
            {"limitValue", "Limit"},
            {"retentionPeriod", "Megtartási idõszak"},
            {"purpose", "Cél"},
            {"pds", "pds"}
        };

        private static string getTranslatedName(string key)
        {
            string translatedValue = string.Empty;
            if (jSonElementDictionary.TryGetValue(key, out translatedValue))
            {
                return translatedValue;
            }

            return key;
        }

        private static string translateYesNo(string value)
        {
            if ("yes".Equals(value, StringComparison.InvariantCultureIgnoreCase))
            {
                return "Igen";
            }
            else if ("no".Equals(value, StringComparison.InvariantCultureIgnoreCase))
            {
                return "Nem";
            }
            return value;
        }
        #endregion
    }
}
