﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUtility
{
    public class ObjektumTipusok
    {
        private const string UgyiratNev = "Ügyirat";
        private const string IratNev = "Irat";
        private const string IratPeldanyNev = "Iratpédány";
        private const string KuldemenyNev = "Küldemény";
        private const string FeladatNev = "Feljegyzés";
        private const string UgyiratUrl = "UgyUgyiratok";
        private const string KuldemenyUrl = "KuldKuldemenyek";
        private const string IratUrl = "IraIratok";
        private const string IratPeldanyUrl = "PldIratPeldanyok";
        private const string FeladatUrl = "Feladatok";
        private const string IraKezbesitesiTetelekUrl = "IraKezbesitesiTetelek";
        private const string IrattarbaVeteltUrl = "IrattarbaVetel";
        private const string IrattarList = "Irattar";
        private const string UrlExtension = ".aspx";

        private static string _eAdminWebSiteUrl = UI.GetAppSetting("eAdminWebSiteUrl");

        public static string eAdminWebSiteUrl
        {
            get 
            {
                if (String.IsNullOrEmpty(_eAdminWebSiteUrl))
                {
                    throw new Exception("eAdminWebSiteUrl alkalmazásbeállítás nem található!");
                }

                return _eAdminWebSiteUrl;
            }
        }

        private static string _eRecordWebSiteUrl = UI.GetAppSetting("eRecordWebSiteUrl");

        public static string eRecordWebSiteUrl
        {
            get
            {
                if (String.IsNullOrEmpty(_eRecordWebSiteUrl))
                {
                    throw new Exception("eRecordWebSiteUrl alkalmazásbeállítás nem található!");
                }

                return _eRecordWebSiteUrl;
            }
        }

        public static string GetDisplayableNameFromObjectType(string obj_type)
        {
            string name = String.Empty;

            switch (obj_type)
            {
                case Constants.TableNames.EREC_UgyUgyiratok:
                    name = UgyiratNev;
                    break;
                case Constants.TableNames.EREC_KuldKuldemenyek:
                    name = KuldemenyNev;
                    break;
                case Constants.TableNames.EREC_IraIratok:
                    name = IratNev;
                    break;
                case Constants.TableNames.EREC_PldIratPeldanyok:
                    name = IratPeldanyNev;
                    break;
                case Constants.TableNames.EREC_HataridosFeladatok:
                    name = FeladatNev;
                    break;
                default:
                    name = String.Empty;
                    break;
            }

            return name;
        }

        private static string GetPageName(string obj_type)
        {
            string queryString;

            return GetPageName(obj_type, String.Empty,out queryString);
        }

        private static string GetPageName(string obj_type, string funkcioKod, out string queryString)
        {
            string name = String.Empty;
            queryString = String.Empty;
            switch (obj_type)
            {
                case Constants.TableNames.EREC_UgyUgyiratok:
                    name = UgyiratUrl;

                    if (!String.IsNullOrEmpty(funkcioKod))
                    {
                        if (funkcioKod.Contains("AtmenetiIrattarAtvetel"))
                        {
                            name = IrattarbaVeteltUrl;
                            queryString = QueryStringVars.Startup + "=FromAtmenetiIrattar";
                            break;
                        }
                        if (funkcioKod.Contains("KozpontiIrattarAtvetel"))
                        {
                            name = IrattarbaVeteltUrl;
                            queryString = QueryStringVars.Startup + "=FromKozpontiIrattar";
                            break;
                        }
                        if (funkcioKod.Contains("AtmenetiIrattar"))
                        {
                            name = IrattarList;
                            queryString = QueryStringVars.Startup + "=FromAtmenetiIrattar";
                            break;
                        }
                        if (funkcioKod.Contains("KozpontiIrattar") || funkcioKod == "KolcsonzesJovahagyas")
                        {
                            name = IrattarList;
                            queryString = QueryStringVars.Startup + "=FromKozpontiIrattar";
                            break;
                        }
                        if (funkcioKod.Contains("SkontroIrattar"))
                        {
                            name = IrattarList;
                            queryString = QueryStringVars.Startup + "=FromSkontroIrattar";
                            break;
                        }
                    }
                    break;
                case Constants.TableNames.EREC_IrattariKikero:
                    name = UgyiratUrl;
                    break;
                case Constants.TableNames.EREC_KuldKuldemenyek:
                    name = KuldemenyUrl;
                    break;
                case Constants.TableNames.EREC_IraIratok:
                    name = IratUrl;
                    break;
                case Constants.TableNames.EREC_PldIratPeldanyok:
                    name = IratPeldanyUrl;
                    break;
                case Constants.TableNames.EREC_HataridosFeladatok:
                    name = FeladatUrl;
                    break;
                case Constants.TableNames.EREC_IraKezbesitesiTetelek:
                    name = IraKezbesitesiTetelekUrl;
                    if(!String.IsNullOrEmpty(funkcioKod))
                    {
                        if (funkcioKod.Contains("Atvetel"))
                            queryString = QueryStringVars.Mode + "=Atveendok";
                        if (funkcioKod.Contains("Atad"))
                            queryString = QueryStringVars.Mode + "=Atadandok";
                    }
                    break;
                default:
                    int index = obj_type.IndexOf("_");
                    if (index > -1 && index < obj_type.Length)
                    {
                        name = obj_type.Substring(index + 1);
                    }
                    break;
            }

            return name;
        }

        public static string GetBaseUrl(string obj_type)
        {
            string baseUrl = String.Empty;

            if (obj_type.StartsWith("EREC_",StringComparison.CurrentCultureIgnoreCase))
                return eRecordWebSiteUrl;

            if (obj_type.StartsWith("KRT_",StringComparison.CurrentCultureIgnoreCase))
                return eAdminWebSiteUrl;

            return baseUrl;
        } 

        public static string GetFullUrl(string url, string obj_type)
        {
            string baseUrl = GetBaseUrl(obj_type);
            if (!String.IsNullOrEmpty(baseUrl))
            {
                if (!baseUrl.EndsWith("/"))
                {
                    baseUrl += "/";
                }
                url = baseUrl + url;
            }

            return url;
        }

        public static string GetViewUrl(string obj_id, string obj_type)
        {
            string url = String.Empty;
            string pageName = GetPageName(obj_type);
            if (!String.IsNullOrEmpty(pageName))
            {
                url = pageName + "Form" + UrlExtension;
                url += "?" + QueryStringVars.Command + "=" + CommandName.View;
                url += "&" + QueryStringVars.Id + "=" + obj_id;
            }
            return url;
        }

        public static string GetModifyUrl(string obj_id, string obj_type)
        {
            string url = String.Empty;
            string pageName = GetPageName(obj_type);
            if (!String.IsNullOrEmpty(pageName))
            {
                url = pageName + "Form" + UrlExtension;
                url += "?" + QueryStringVars.Command + "=" + CommandName.Modify;
                url += "&" + QueryStringVars.Id + "=" + obj_id;
            }
            return url;
        }

        public static string GetListUrl(string obj_id, string obj_type, string funkcioKod)
        {
            string url = String.Empty;
            string queryString;
            string pageName = GetPageName(obj_type, funkcioKod, out queryString);
            if (!String.IsNullOrEmpty(pageName))
            {
                url = pageName + "List" + UrlExtension;
                if (!String.IsNullOrEmpty(obj_id))
                    url += "?" + QueryStringVars.Id + "=" + obj_id;
            }

            if (!String.IsNullOrEmpty(queryString))
            {
                if (url.Contains("?"))
                    url += "&";
                else
                    url += "?";

                url += queryString;
            }

            return url;
        }

        public static string GetListUrl(string obj_id, string obj_type)
        {
            return GetListUrl(obj_id, obj_type, null);
        }

        public static string GetListUrl(string obj_type)
        {
            return GetListUrl(null, obj_type, null);
        }

        public static string GetFullListUrl(string obj_id, string obj_type, string funkcioKod)
        {
            string url = String.Empty;
            string baseUrl = GetBaseUrl(obj_type);
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string listUrl = GetListUrl(obj_id,obj_type,funkcioKod);
                if (!String.IsNullOrEmpty(listUrl))
                {
                    if (!baseUrl.EndsWith("/"))
                    {
                        baseUrl += "/";
                    }

                    url = baseUrl + listUrl;
                }
            }

            return url;
        }

        public static string GetFullListUrl(string obj_type, string funkcioKod)
        {
            return GetFullListUrl(String.Empty, obj_type, String.Empty);
        }

        public static string GetFullListUrl(string obj_type)
        {
            return GetFullListUrl(obj_type, String.Empty);
        }

        public static string GetViewOnClientClick(string obj_Id, string obj_type)
        {
            if (String.IsNullOrEmpty(obj_Id) || String.IsNullOrEmpty(obj_type))
                return String.Empty;

            string js = String.Empty;

            js = JavaScripts.SetOnCLientClick_NoPostBack(GetViewUrl(obj_Id, obj_type), "", Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);

            return js;
        }
    }
}
