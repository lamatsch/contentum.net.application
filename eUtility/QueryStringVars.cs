﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUtility
{
    public class QueryStringVars
    {
        public const string Id = "Id";
        public const string Command = "Command";

        public const string HiddenFieldId = "HiddenFieldId";
        public const string TextBoxId = "TextBoxId";
        public const string ViewImageButtonId = "ViewImageButtonId";

        //BUG 5197 Kapcsolt partner
        public const string KapcsoltPartnerDropDownId = "KapcsoltPartnerDropDownId";


        //LZS - BLG_335 - Klónozandó mezők Id-jai
        public const string CloneHiddenFieldId = "CloneHiddenFieldId";
        public const string CloneTextBoxId = "CloneTextBoxId";


        public const string IrszTextBoxClientId = "IrszTextBoxClientId";
        public const string AzonositoTextBox = "AzonositoTextBox";
        public const string IrattariTetelTextBox = "IrattariTetelTextBox";
        public const string MegkulJelzesTextBox = "MegkulJelzesTextBox";
        public const string IrattarbaTextBox = "IrattarbaTextBox";
        public const string SkontroVegeTextBox = "SkontroVegeTextBox";
        public const string KodTextBoxClientId = "KodTextBoxClientId";

        public const string SzerepkorId = "SzerepkorId";
        public const string FunkcioId = "FunkcioId";
        public const string FelhasznaloId = "FelhasznaloId";
        public const string CsoportId = "CsoportId";
        public const string CsoportIdJogalany = "CsoportIdJogalany";
        public const string HelyettesitesId = "HelyettesitesId";
        public const string PartnerId = "PartnerId";
        public const string CimId = "CimId";
        public const string TableName = "TableName";
        public const string Filter = "Filter";
        //LZS - BLG_51
        public const string SablonId = "SablonId";

        public const string SoapMessageId = "SoapMessageId";


        public const string Startup = "Startup";

        public const string SzuloMenuId = "SzuloMenuId";
        public const string SzuloCsoportId = "SzuloCsoportId";
        public const string CimTipusKod = "CimTipusKod";
        public const string CsoportTipus = "CsoportTipus";

        public const string MessageHiddenFieldId = "MessageHiddenFieldId";
        public const string PartnerKapcsolatTipus = "PartnerKapcsolatTipus";

        public const string RefreshCallingWindow = "RefreshCallingWindow";
        public const string TryFireChangeEvent = "TryFireChangeEvent";

        public const string SelectedTab = "SelectedTab";
        public const string NewSzemelyPartnerWithMinositoOrKapcsolattarto = "NSZEMELY_W_MIN_OR_KT";

        //LZS - BUG_11549
        public const string CsakKolcsonzesJovahagyasJoguDolgozok = "CsakKolcsonzesJovahagyasJoguDolgozok";
        public const string CsakAtmenetiIrattarKikeresJovahagyasJoguDolgozok = "CsakAtmenetiIrattarKikeresJovahagyasJoguDolgozok";

        public static class CsoportFilter
        {
            //public static string HierarchiabanLefele = "HierarchiabanLefele";
            //public static string KozvetlenulHozzarendelt = "KozvetlenulHozzarendelt";
            //public static string SzervezetId = "SzervezetId";
        }
        
        public const string AgazatiJelId = "AgazatiJelId";
        public const string IratMetadefinicioId = "IratMetadefinicioId";
        public const string TargyszavakId = "TargyszavakId";
        public const string ObjTipId = "ObjTipId";
        public const string ObjektumId = "ObjektumId";

        /************************************************/
        public const string ElosztoivId = "ElosztoivId";
        public const string WithElosztoivek = "WithElosztoivek";
        /************************************************/

        public const string TemplateId = "TemplateId";

        // kiemelve az eRecordból(eMigration használat miatt)
        public const string Mode = "Mode";

        public const string RegiAdatAzonosito = "RegiAdatAzonosito";

        public const string ParentWindowCallbackFunction = "ParentWindowCallbackFunction";

        public const string LoggedFelhasznaloId = "_lgdUserId";
        public const string LoggedHelyettesitesId = "_lgdHelyettesitesId";
        public const string LoggedCsoporttagId = "_lgdCsoporttagId";
        public const string LoggedSzervezetId = "_lgdSzervezetId";
        public const string LoggedLoginType= "_lgdLoginType";

        public static class ElozmenySearchParameters
        {
            public const string Ev = "Ev";
            public const string IktatokonyvValue = "IktatokonyvValue";
            public const string Foszam = "Foszam";
        }

        public const string TipusHiddenFieldId = "TipusHiddenFieldId";
        //public const string FeladatId = "FeladatId";
        public const string WhereByManual = "WhereByManual";

        public const string ObjektumTipusKod = "ObjektumTipusKod";
        public const string ObjektumType = "ObjektumType";
        public const string WindowType = "WindowType";
        public const string RegExpHiddenFieldId = "RegExpHiddenFieldId";
        public const string AlapertelmezettErtekHiddenFieldId = "DefaultHiddenFieldId";
        public const string ToolTipHiddenFieldId = "ToolTipHiddenFieldId";
        public const string ControlTypeSourceHiddenFieldId = "ControlTypeSourceHiddenFieldId";
        public const string ControlTypeDataSourceHiddenFieldId = "ControlTypeDataSourceHiddenFieldId";
        public const string Csoport_Id_Tulaj = "Csoport_Id_Tulaj";

        public const string CloneFieldId = "CloneFieldId";

        /// <summary>
        /// NameValueCollection formában adott QueryString átalakítása az URL-nek megfelelõ string formátummá
        /// </summary>
        /// <param name="qs"></param>
        /// <returns></returns>
        public static string ConvertQueryStringToString(System.Collections.Specialized.NameValueCollection qs)
        {
            if (qs == null) return "";

            StringBuilder sb = new StringBuilder();
            foreach (String name in qs)
            {
                if (sb.Length > 0)
                {
                    sb.Append("&");
                }
                sb.AppendFormat("{0}={1}", name, System.Web.HttpUtility.UrlEncode(qs[name]));

            }

            return sb.ToString();
        }

        public static class ErvenyessegSearchParameters
        {
            public const string ErvenyessegDatum = "ErvenyessegDatum";
            public const string ErvKezd = "ErvKezd";
            public const string ErvVege = "ErvVege";

            /// <summary>
            /// QueryStringbõl annak meghatározása, hogy van-e megadva érvényesség szûrés
            /// </summary>
            /// <param name="qs"></param>
            public static bool IsQSFilteredByErvenyesseg(System.Collections.Specialized.NameValueCollection qs)
            {
                bool isFiltered = false;
                if (qs != null)
                {
                    string qsErvenyessegDatum = qs.Get(ErvenyessegSearchParameters.ErvenyessegDatum);
                    string qsErvKezd = qs.Get(ErvenyessegSearchParameters.ErvKezd);
                    string qsErvVege = qs.Get(ErvenyessegSearchParameters.ErvVege);

                    // akkor is igaznak tekintjük, ha van kulcs, de üres, mert akkor szándékosan lett kivéve az érték
                    if (qsErvenyessegDatum != null || (qsErvKezd != null && qsErvVege != null))
                    {
                        // érvényesség szûrés
                        isFiltered = true;
                    }

                    //if (!String.IsNullOrEmpty(qsErvenyessegDatum) || !String.IsNullOrEmpty(qsErvKezd) || !String.IsNullOrEmpty(qsErvVege))
                    //{
                    //    // érvényesség szûrés
                    //    isFiltered = true;
                    //}

                }

                return isFiltered;
            }

            /// <summary>
            /// Érvényességi dátum beállítása a QueryStringbe, az esetleges korábbi érvényesség szûrések törlése mellett.
            /// Az üres dátumot is beállítjuk, ebbõl látható, hogy volt-e szándékos szûrés letiltás
            /// </summary>
            /// <param name="qs"></param>
            /// <param name="ErvenyessegDatum"></param>
            public static void SetErvenyessegDatumFilterToQs(System.Collections.Specialized.NameValueCollection qs, string ervenyessegDatum)
            {
                if (qs == null) return;
                RemoveErvenyessegFiltersFromQs(qs);
                qs.Add(ErvenyessegSearchParameters.ErvenyessegDatum, System.Web.HttpUtility.UrlEncode(ervenyessegDatum));
            }

            /// <summary>
            /// Érvényességi dátum beállítása QueryString formátumban.
            /// Az üres dátumot is beállítjuk, ebbõl látható, hogy volt-e szándékos szûrés letiltás
            /// </summary>
            /// <param name="ErvenyessegDatum"></param>
            public static string GetErvenyessegDatumFilterAsQsString(string ervenyessegDatum)
            {
                System.Collections.Specialized.NameValueCollection qs = new System.Collections.Specialized.NameValueCollection();
                ErvenyessegSearchParameters.SetErvenyessegDatumFilterToQs(qs, ervenyessegDatum);

                return ConvertQueryStringToString(qs);
            }

            /// <summary>
            /// Érvényességi tartomány beállítása a QueryStringbe, az esetleges korábbi érvényesség szûrések törlése mellett.
            /// Az üres dátumot is beállítjuk, ebbõl látható, hogy volt-e szándékos szûrés letiltás
            /// </summary>
            /// <param name="qs"></param>
            /// <param name="ervKezd"></param>
            /// <param name="ervVege"></param>
            public static void SetErvenyessegTartomanyFilterToQs(System.Collections.Specialized.NameValueCollection qs, string ervKezd, string ervVege)
            {
                if (qs == null) return;
                RemoveErvenyessegFiltersFromQs(qs);
                qs.Add(ErvenyessegSearchParameters.ErvKezd, System.Web.HttpUtility.UrlEncode(ervKezd));
                qs.Add(ErvenyessegSearchParameters.ErvKezd, System.Web.HttpUtility.UrlEncode(ervVege));
            }

            /// <summary>
            /// Érvényességi tartomány beállítása QueryString formátumban.
            /// Az üres dátumot is beállítjuk, ebbõl látható, hogy volt-e szándékos szûrés letiltás
            /// </summary>
            /// <param name="ervKezd"></param>
            /// <param name="ervVege"></param>
            public static string GetErvenyessegTartomanyFilterAsQsString(string ervKezd, string ervVege)
            {
                System.Collections.Specialized.NameValueCollection qs = new System.Collections.Specialized.NameValueCollection();
                ErvenyessegSearchParameters.SetErvenyessegTartomanyFilterToQs(qs, ervKezd, ervVege);

                return qs.ToString();
            }

            /// <summary>
            /// Töröl minden érvényességi szûrést okozó kulcsot a QueryStringbõl
            /// </summary>
            /// <param name="qs"></param>
            public static void RemoveErvenyessegFiltersFromQs(System.Collections.Specialized.NameValueCollection qs)
            {
                if (qs == null) return;

                qs.Remove(ErvenyessegSearchParameters.ErvenyessegDatum);
                qs.Remove(ErvenyessegSearchParameters.ErvKezd);
                qs.Remove(ErvenyessegSearchParameters.ErvVege);
            }

            /// <summary>
            /// Keresési objektum ErvKezd és ErvVege mezõinek beállítása
            /// </summary>
            /// <param name="qs"></param>
            /// <param name="ervKezd"></param>
            /// <param name="ervVege"></param>
            public static void SetSearchObjectFieldsFromQS(System.Collections.Specialized.NameValueCollection qs
                , Contentum.eQuery.BusinessDocuments.Field ervKezd, Contentum.eQuery.BusinessDocuments.Field ervVege)
            {
                if (ervKezd == null || ervVege == null) return;

                if (qs != null)
                {
                    string qsErvenyessegDatum = System.Web.HttpUtility.UrlDecode(qs.Get(ErvenyessegSearchParameters.ErvenyessegDatum));
                    string qsErvKezd = System.Web.HttpUtility.UrlDecode(qs.Get(ErvenyessegSearchParameters.ErvKezd));
                    string qsErvVege = System.Web.HttpUtility.UrlDecode(qs.Get(ErvenyessegSearchParameters.ErvVege));

                    if (!String.IsNullOrEmpty(qsErvenyessegDatum))
                    {
                        // érvényes
                        ervKezd.Value = qsErvenyessegDatum;
                        ervKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
                        ervKezd.Group = "100";
                        ervKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

                        ervVege.Value = qsErvenyessegDatum;
                        ervVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
                        ervVege.Group = "100";
                        ervVege.GroupOperator = Contentum.eQuery.Query.Operators.and;
                    }
                    else if (!string.IsNullOrEmpty(qsErvKezd) || !String.IsNullOrEmpty(qsErvVege))
                    {
                        // érvényességtartomány van kiválasztva:
                        // A feltétel: ervKezd <= this.ErvVege && ervVege >= this.ErvKezd
                        if (!String.IsNullOrEmpty(qsErvVege))
                        {
                            ervKezd.Value = qsErvVege;
                            ervKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
                            ervKezd.Group = "100";
                            ervKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;
                        }
                        else
                        {
                            ervKezd.Clear();
                        }

                        ervVege.Value = qsErvKezd;
                        ervVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
                        ervVege.Group = "100";
                        ervVege.GroupOperator = Contentum.eQuery.Query.Operators.and;
                    }
                    return;
                }

                // összes
                ervKezd.Clear();

                ervVege.Clear();
            }

            /// <summary>
            /// A kapott QueryStringbõl megállapítja, hogy van-e érvényességszûrés, és ha van,
            /// SQL feltétellé alakítja a kapott mezõnevek felhasználásával
            /// </summary>
            /// <param name="qs"></param>
            /// <param name="ErvKezdFieldName"></param>
            /// <param name="ErvVegeFieldName"></param>
            /// <returns></returns>
            public static string GetErvenyessegFilterFromQSAsSQLCondition(System.Collections.Specialized.NameValueCollection qs
                , string fieldNameErvKezd, string fieldNameErvVege)
            {
                if (qs == null || String.IsNullOrEmpty(fieldNameErvKezd) || String.IsNullOrEmpty(fieldNameErvVege)) return "";

                string qsErvenyessegDatum = System.Web.HttpUtility.UrlDecode(qs.Get(ErvenyessegSearchParameters.ErvenyessegDatum));
                if (!String.IsNullOrEmpty(qsErvenyessegDatum))
                {
                    DateTime dtQsErvenyessegDatum;
                    if (DateTime.TryParse(qsErvenyessegDatum, out dtQsErvenyessegDatum))
                    {
                        return String.Format("'{0}' between {1} and {2}", dtQsErvenyessegDatum.ToString("yyyy.MM.dd HH:mm:ss.fff"), fieldNameErvKezd, fieldNameErvVege);
                    }
                }

                string qsErvKezd = System.Web.HttpUtility.UrlDecode(qs.Get(ErvenyessegSearchParameters.ErvKezd));
                string qsErvVege = System.Web.HttpUtility.UrlDecode(qs.Get(ErvenyessegSearchParameters.ErvVege));
                // érvényességtartomány van kiválasztva:
                // A feltétel: ervKezd <= qsErvVege && ervVege >= qsErvKezd 
                if (!String.IsNullOrEmpty(qsErvKezd) || !String.IsNullOrEmpty(qsErvVege))
                {
                    StringBuilder sb = new StringBuilder();
                    if (!String.IsNullOrEmpty(qsErvKezd))
                    {
                        DateTime dtQsErvKezd;
                        if (DateTime.TryParse(qsErvenyessegDatum, out dtQsErvKezd))
                        {
                            sb.AppendFormat("{0}>='{1}'", fieldNameErvVege, dtQsErvKezd.ToString("yyyy.MM.dd HH:mm:ss.fff"));
                        }
                    }

                    if (sb.Length > 0)
                    {
                        sb.Append(" and ");
                    }

                    if (!String.IsNullOrEmpty(qsErvVege))
                    {
                        DateTime dtQsErvVege;
                        if (DateTime.TryParse(qsErvenyessegDatum, out dtQsErvVege))
                        {
                            sb.AppendFormat("{0}<='{1}'", fieldNameErvKezd, dtQsErvVege.ToString("yyyy.MM.dd HH:mm:ss.fff"));
                        }
                    }

                    return sb.ToString();
                }

                return "";
            }
        }
    }
}
