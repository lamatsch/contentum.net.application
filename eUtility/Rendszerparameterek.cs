﻿using System;
using System.Collections.Generic;
using System.Text;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eUtility
{
    public class Rendszerparameterek
    {
        //!!!!!!!!!!!!!ABC SORRENDBEN ÍRJÁTOK BE A PARAMÉTEREKET (VAGY KÉZZEL RENDEZVE VAGY A CODEMAID SORT AL), HOGY NE LEGYEN CONFLICT ÁLLANDÓAN!!!!!!!!!!!!!!!!!!!!!!!!        
        public const string ADATKAPU_PARTNER_SYNC_VIEW_SCHEMA = "ADATKAPU_PARTNER_SYNC_VIEW_SCHEMA";
        public const string ADOSZAM_ELLENORZO_VEKTOR = "ADOSZAM_ELLENORZO_VEKTOR";
        public const string ALAIRAS_VISSZAUTASITAS_KEZELES = "ALAIRAS_VISSZAUTASITAS_KEZELES";
        public const string ALAIROK_CSATOLMANY_NELKUL = "ALAIROK_CSATOLMANY_NELKUL";
        public const string ALKALMAZASGAZDA_EMAIL_CIM = "ALKALMAZASGAZDA_EMAIL_CIM";
        public const string AllMetaCache_RefreshPeriod_Minute = "AllMetaCache_RefreshPeriod_Minute";
        public const string ALSZAM_SZIGNALT_ENABLED = "ALSZAM_SZIGNALT_ENABLED";
        public const string APPLICATION_VERSION = "APPLICATION_VERSION";
        public const string ATMENETI_IRATTAR_KEZELESJOG_TIPUS = "ATMENETI_IRATTAR_KEZELESJOG_TIPUS";
        public const string AUTO_EUZENET_KIKULDES_KIADMANYOZAS_UTAN = "AUTO_EUZENET_KIKULDES_KIADMANYOZAS_UTAN";
        public const string AZONOS_NEVU_CSATOLMANY_ENABLED = "AZONOS_NEVU_CSATOLMANY_ENABLED";
        public const string BEJOVO_IRAT_MUNKAIRATBA_IKTATAS = "BEJOVO_IRAT_MUNKAIRATBA_IKTATAS";
        public const string BEKULDO_PARTNER_CIM_NEM_TORZSBOL_ENABLED = "BEKULDO_PARTNER_CIM_NEM_TORZSBOL_ENABLED";
        public const string BORITO_PRINT = "BORITO_PRINT";
        public const string CIM_SZABAD_SZOVEG_ENABLED = "CIM_SZABAD_SZOVEG_ENABLED";
        public const string CIM_SZEGMENTALTSAG_ELLENORZES = "CIM_SZEGMENTALTSAG_ELLENORZES";
        public const string CSATOLMANY_LATHATOSAG_IRATPELDANYTOL = "CSATOLMANY_LATHATOSAG_IRATPELDANYTOL";
        public const string CSATOLMANY_LETOLTES_MOD = "CSATOLMANY_LETOLTES_MOD";
        public const string CSOPORT_MINDENKI = "CSOPORT_MINDENKI";
        public const string CSOPORT_NEV_WITH_EMAIL_DISABLED = "CSOPORT_NEV_WITH_EMAIL_DISABLED";
        public const string CsoportNevekCache_MaxRowsFromDBToCache = "CsoportNevekCache_MaxRowsFromDBToCache";
        public const string CsoportNevekCache_RefreshPeriod_Minute = "CsoportNevekCache_RefreshPeriod_Minute";
        public const string DEAFULT_INTEZESI_IDO_MASOLAS = "DEAFULT_INTEZESI_IDO_MASOLAS";
        public const string DEFAULT_INTEZESI_IDO = "DEFAULT_INTEZESI_IDO";
        public const string DEFAULT_INTEZESI_IDO_IDOEGYSEG = "DEFAULT_INTEZESI_IDO_IDOEGYSEG";
        public const string DEFAULT_TERTIVEVENY_KERESES = "DEFAULT_TERTIVEVENY_KERESES";
        public const string DEFAULT_TERTIVEVENY_KERESES_ERTEK_RAGSZAM = "2";
        public const string DEFAULT_TERTIVEVENY_KERESES_ERTEK_VONALKOD = "1";
        public const string DOKUMENTUMLISTA_JOGOSULTSAGSZURES_ENABLED = "DOKUMENTUMLISTA_JOGOSULTSAGSZURES_ENABLED";
        public const string ELEKTRONIKUS_POSTAKONYV_AZONOSITO = "ELEKTRONIKUS_POSTAKONYV_AZONOSITO";
        public const string ELINTEZETTE_NYILV_KIADM_FELTETELE = "ELINTEZETTE_NYILV_KIADM_FELTETELE";
        public const string ELINTEZETTE_NYILVANITAS_ENABLED = "ELINTEZETTE_NYILVANITAS_ENABLED";
        public const string ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT = "ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT";
        public const string ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT = "ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT";
        public const string ELSZAMOLTATAS_RELEVANS_UGYIRAT_ALLAPOT = "ELSZAMOLTATAS_RELEVANS_UGYIRAT_ALLAPOT";
        public const string EMAIL_CSATOLMANY_DOKUMENTUMKONYVTAR = "EMAIL_CSATOLMANY_DOKUMENTUMKONYVTAR";
        public const string EMAIL_CSATOLMANY_DOKUMENTUMTARTERULET = "EMAIL_CSATOLMANY_DOKUMENTUMTARTERULET";
        public const string EMAIL_CSATOLMANY_DOKUMENTUMTARTIPUS = "EMAIL_CSATOLMANY_DOKUMENTUMTARTIPUS";
        public const string EMAIL_ERKEZTETES_UZENET = "EMAIL_ERKEZTETES_UZENET";
        public const string EMAIL_KIMENO_TERTIVEVENY = "EMAIL_KIMENO_TERTIVEVENY";
        public const string EMAIL_SERVER_ADDRESS = "EMAIL_SERVER_ADDRESS";
        public const string EMIGRATION_ADATOK_MODOSITHATOSAGA_ENABLED = "EMIGRATION_ADATOK_MODOSITHATOSAGA_ENABLED";
        public const string EMIGRATIONBEN_SELEJTEZES_JEGYZEKRE_HELYEZESSEL = "EMIGRATIONBEN_SELEJTEZES_JEGYZEKRE_HELYEZESSEL";
        public const string ENCRYPT_SAJAT_PRIVAT_KULCS_PASSWORDKI = "ENCRYPT_SAJAT_PRIVAT_KULCS_PASSWORDKI";
        public const string ENCRYPT_SAJAT_PRIVAT_KULCS_PATH = "ENCRYPT_SAJAT_PRIVAT_KULCS_PATH";
        public const string ENCRYPT_SAJAT_PUBLIKUS_KULCS_PATH = "ENCRYPT_SAJAT_PUBLIKUS_KULCS_PATH";
        public const string ERKEZTETOSZAM_FORMATUM = "ERKEZTETOSZAM_FORMATUM";
        public const string ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_LEVELTARIATADAS_EMAIL_SABLON = "ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_LEVELTARIATADAS_EMAIL_SABLON";
        public const string ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_LEVELTARIATADAS_EMAIL_TARGY = "ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_LEVELTARIATADAS_EMAIL_TARGY";
        public const string ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_SELEJTEZES_EMAIL_SABLON = "ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_SELEJTEZES_EMAIL_SABLON";
        public const string ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_SELEJTEZES_EMAIL_TARGY = "ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_SELEJTEZES_EMAIL_TARGY";
        public const string EUZENET_OSSZES = "EUZENET_OSSZES";
        public const string EUZENET_RENDSZERUZENETEK_SZURES = "EUZENET_RENDSZERUZENETEK_SZURES";
        public const string EXPEDIALAS_UJ_IRATPELDANNYAL = "EXPEDIALAS_UJ_IRATPELDANNYAL";
        public const string FELADAT_KEZELES_DISABLED = "FELADAT_KEZELES_DISABLED";
        public const string FELADAT_MANUALIS_PRIORITASOK = "FELADAT_MANUALIS_PRIORITASOK";
        public const string FELADATOSSZESITO_FRISSITES_MIN_PERC = "FELADATOSSZESITO_FRISSITES_MIN_PERC";
        public const string FELADATOSSZESITO_JOB_ADATFRISSITES = "FELADATOSSZESITO_JOB_ADATFRISSITES";
        public const string FELHASZNALO_CSOPORT_TAGSAG_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_SABLON = "FELHASZNALO_CSOPORT_TAGSAG_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_SABLON";
        public const string FELHASZNALO_CSOPORT_TAGSAG_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_TARGY = "FELHASZNALO_CSOPORT_TAGSAG_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_TARGY";
        public const string FELHASZNALO_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_SABLON = "FELHASZNALO_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_SABLON";
        public const string FELHASZNALO_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_TARGY = "FELHASZNALO_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_TARGY";
        public const string FIZIKAI_UGYIRAT_KOLCSONZES_BETEKINTESRE_DISABLED = "FIZIKAI_UGYIRAT_KOLCSONZES_BETEKINTESRE_DISABLED";
        public const string HATOSAGI_ADATOK_BEJOVOHOZ = "HATOSAGI_ADATOK_BEJOVOHOZ";
        public const string HATOSAGI_STATISZTIKA_ONKORMANYZAT = "HATOSAGI_STATISZTIKA_ONKORMANYZAT";
        public const string HIBABEJELENTES_EMAIL_CIM = "HIBABEJELENTES_EMAIL_CIM";
        public const string HIVATKOZAS_KULDES_KEZBESITESITETEL_ENABLED = "HIVATKOZAS_KULDES_KEZBESITESITETEL_ENABLED";
        public const string HIVSZAM_ELLENORZES = "HIVSZAM_ELLENORZES";
        public const string IKTATAS_KOTELEZO_IRATTIPUS = "IKTATAS_KOTELEZO_IRATTIPUS";
        public const string IKTATAS_KOTELEZO_IRATUGYINTEZO = "IKTATAS_KOTELEZO_IRATUGYINTEZO";
        public const string IKTATAS_UGYINTEZO_KOTELEZO_ENABLED = "IKTATAS_UGYINTEZO_KOTELEZO_ENABLED";
        public const string IKTATAS_UGYTIPUS_IRATMETADEFBOL = "IKTATAS_UGYTIPUS_IRATMETADEFBOL";
        public const string IKTATOKONYV_AZONOSITO_HOSSZ = "IKTATOKONYV_AZONOSITO_HOSSZ";
        public const string IKTATOSZAM_FORMATUM = "IKTATOSZAM_FORMATUM";
        public const string IRAT_CSATOLMANY_DOKUMENTUMKONYVTAR = "IRAT_CSATOLMANY_DOKUMENTUMKONYVTAR";
        public const string IRAT_CSATOLMANY_DOKUMENTUMTARTERULET = "IRAT_CSATOLMANY_DOKUMENTUMTARTERULET";
        public const string IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS = "IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS";
        public const string IRAT_ELINT_SAKKORA_FIGYELES = "IRAT_ELINT_SAKKORA_FIGYELES";
        public const string IRAT_INTEZKEDESREATVETEL_ELERHETO = "IRAT_INTEZKEDESREATVETEL_ELERHETO";
        public const string IRAT_INTEZKEDESREATVETEL_SAKKORA_DEFAULT = "IRAT_INTEZKEDESREATVETEL_SAKKORA_DEFAULT";
        public const string IRATTAROZATLAN_UGYIRAT_ALLAPOT = "IRATTAROZATLAN_UGYIRAT_ALLAPOT";
        public const string IRAT_MINOSITES_BIZALMAS = "IRAT_MINOSITES_BIZALMAS";
        public const string IRATTARBA_IKTATAS_ENABLED = "IRATTARBA_IKTATAS_ENABLED";
        public const string IRATTARI_HELY_KOTELEZO = "IRATTARI_HELY_KOTELEZO";
        public const string IRATTAROZAS_ATMENETI_AZONOS_KEZELES = "IRATTAROZAS_ATMENETI_AZONOS_KEZELES";
        public const string IRATTAROZAS_JOVAHAGYAS_ENABLED = "IRATTAROZAS_JOVAHAGYAS_ENABLED";
        public const string IRATTAROZAS_JOVAHAGYO_UGYFELELOS = "IRATTAROZAS_JOVAHAGYO_UGYFELELOS";
        public const string ITSZ_ELORE = "ITSZ_ELORE";
        public const string JEGYZEK_CSAK_SAJATSZERVEZET_LATHATO = "JEGYZEK_CSAK_SAJATSZERVEZET_LATHATO";
        public const string KIADMANYOZNI_KELL_DEFAULT = "KIADMANYOZNI_KELL_DEFAULT";
        public const string KIKERES_ATMENETI_IRATTARBOL_BETEKINTESRE_ENABLED = "KIKERES_ATMENETI_IRATTARBOL_BETEKINTESRE_ENABLED";
        public const string KIKERES_MAS_NEVRE_ENABLED = "KIKERES_MAS_NEVRE_ENABLED";
        public const string KodCsoportokCache_RefreshPeriod_Minute = "KodCsoportokCache_RefreshPeriod_Minute";
        public const string KOLCSONZESFUL_ATMENETI_IRATTAR_ENABLED = "KOLCSONZESFUL_ATMENETI_IRATTAR_ENABLED";
        public const string KOZPONTIIRATTAR_CSOPORTID = "KOZPONTIIRATTAR_CSOPORTID";
        public const string KULDEMENY_CSATOLMANY_DOKUMENTUMKONYVTAR = "KULDEMENY_CSATOLMANY_DOKUMENTUMKONYVTAR";
        public const string KULDEMENY_CSATOLMANY_DOKUMENTUMTARTERULET = "KULDEMENY_CSATOLMANY_DOKUMENTUMTARTERULET";
        public const string KULDEMENY_CSATOLMANY_DOKUMENTUMTARTIPUS = "KULDEMENY_CSATOLMANY_DOKUMENTUMTARTIPUS";
        public const string KULDEMENY_KULDES_MODJA_HELYBEN = "KULDEMENY_KULDES_MODJA_HELYBEN";
        public const string KULDEMENYAZONOSITO_FORMATUM = "KULDEMENYAZONOSITO_FORMATUM";
        public const string LEZARAS_ELINTEZES_NELKUL_ENABLED = "LEZARAS_ELINTEZES_NELKUL_ENABLED";
        public const string LISTA_ELEMSZAM_MAX = "LISTA_ELEMSZAM_MAX";
        public const string LOVLIST_MAX_ROW = "LOVLIST_MAX_ROW";
        public const string MAX_ITEM_NUMBER = "MAX_ITEM_NUMBER";
        public const string MIGRALAS_EVE = "MIGRALAS_EVE";
        public const string MIGRACIOS_FOSZAM_HELY_VALTOZAS_TELJES_LANC = "MIGRACIOS_FOSZAM_HELY_VALTOZAS_TELJES_LANC";
        public const string MUNKANAPLO_IRAT_UGYINTEZO_ENABLED = "MUNKANAPLO_IRAT_UGYINTEZO_ENABLED";
        public const string MUNKAUGYIRATOK_ALLAPOT_SZURES = "MUNKAUGYIRATOK_ALLAPOT_SZURES";
        public const string PAGE_FELADASIIDO_KOTELEZO = "PAGE_FELADASIIDO_KOTELEZO";
        public const string PAGE_VIEW_ENABLED = "PAGE_VIEW_ENABLED";
        public const string PARTNER_LETREHOZAS_JOVAHAGYANDO = "PARTNER_LETREHOZAS_JOVAHAGYANDO";
        public const string PARTNERKAPCSOLATOK_ERKEZTETESKOR_ENABLED = "PARTNERKAPCSOLATOK_ERKEZTETESKOR_ENABLED";
        public const string PIR_INTERFACE_ENABLED = "PIR_INTERFACE_ENABLED";
        public const string PIR_RENDSZER = "PIR_RENDSZER";
        public const string PKI_INTEGRACIO = "PKI_INTEGRACIO";
        public const string PKI_RETRY_CODES = "PKI_RETRY_CODES";
        public const string PKI_RETRY_NUM = "PKI_RETRY_NUM";
        public const string POSTAI_RAGSZAM_ERKEZTETESKOR = "POSTAI_RAGSZAM_ERKEZTETESKOR";
        public const string POSTAI_RAGSZAM_EXPEDIALASKOR = "POSTAI_RAGSZAM_EXPEDIALASKOR";
        public const string POSTAZAS_IRATPELDANNYAL = "POSTAZAS_IRATPELDANNYAL";
        public const string POSTAZAS_TOMEGES_MEZO_LETILT = "POSTAZAS_TOMEGES_MEZO_LETILT";
        public const string REGEXP_PATTERN_EMAIL_ADDRESS = "REGEXP_PATTERN_EMAIL_ADDRESS";
        public const string REGEXP_UGYIRAT_IRATTARIHELY = "REGEXP_UGYIRAT_IRATTARIHELY";
        public const string RENDSZERFELUGYELET_EMAIL_CIM = "RENDSZERFELUGYELET_EMAIL_CIM";
        public const string SCAN_ARCHIVE_PATH = "SCAN_ARCHIVE_PATH";
        public const string SCAN_EHITELES_MASOLAT = "SCAN_EHITELES_MASOLAT";
        public const string SCAN_INPUT_DELAY = "SCAN_INPUT_DELAY";
        public const string SCAN_INPUT_PATH = "SCAN_INPUT_PATH";
        public const string SCAN_OCR_ENABLED = "SCAN_OCR_ENABLED";
        public const string SCAN_OCR_EXPORT_PATH = "SCAN_OCR_EXPORT_PATH";
        public const string SCAN_OCR_IMPORT_DELAY = "SCAN_OCR_IMPORT_DELAY";
        public const string SCAN_OCR_IMPORT_PATH = "SCAN_OCR_IMPORT_PATH";
        public const string SCAN_OCR_IMPORT_TIMEOUT = "SCAN_OCR_IMPORT_TIMEOUT";
        public const string SCAN_USER_NAME = "SCAN_USER_NAME";
        public const string SCROLL_PANEL_HEIGHT = "SCROLL_PANEL_HEIGHT";
        public const string SEARCH_SETUP_LEGUTOBB_KEZELT_ENABLED = "SEARCH_SETUP_LEGUTOBB_KEZELT_ENABLED";
        public const string SHORT_DATE_PATTERN = "SHORT_DATE_PATTERN";
        public const string SIGN_SAJAT_PRIVAT_KULCS_PASSWORD = "SIGN_SAJAT_PRIVAT_KULCS_PASSWORD_PATH";
        public const string SIGN_SAJAT_PRIVAT_KULCS_PATH = "SIGN_SAJAT_PRIVAT_KULCS_PATH";
        public const string SIGN_SAJAT_PUBLIKUS_KULCS_PATH = "SIGN_SAJAT_PUBLIKUS_KULCS_PATH";
        public const string SKONTRO_JOVAHAGYAS_ENABLED = "SKONTRO_JOVAHAGYAS_ENABLED";
        public const string SKONTRO_OKA_VALASZTHATO = "SKONTRO_OKA_VALASZTHATO";
        public const string SZERELT_UGYIRAT_MEGORZESI_IDO_SZAMITASA = "SZERELT_UGYIRAT_MEGORZESI_IDO_SZAMITASA";
        public const string TARGY_MASOLAS_UGYROL = "TARGY_MASOLAS_UGYROL";
        public const string TELJESSEGELLENORZES_IRATTARBAADASELOTT_ENABLED = "TELJESSEGELLENORZES_IRATTARBAADASELOTT_ENABLED";
        public const string TELJESSEGELLENORZES_SKONTROELOTT_ENABLED = "TELJESSEGELLENORZES_SKONTROELOTT_ENABLED";
        public const string TERTIVEVENY_RAGSZAM_KIOSZTAS_MODJA = "TERTIVEVENY_RAGSZAM_KIOSZTAS_MODJA";
        public const string TERTIVONALKOD_KELL = "TERTIVONALKOD_KELL";
        public const string TOBBES_IRATPELDANY_IKTATASKOR = "TOBBES_IRATPELDANY_IKTATASKOR";
        public const string TUK = "TUK";
        public const string UGYFEL_ADATOK_KOTELEZOSEG = "UGYFEL_ADATOK_KOTELEZOSEG";
        public const string UGYINTEZESI_HATARIDO_MASOLAS_UGYROL = "UGYINTEZESI_HATARIDO_MASOLAS_UGYROL";
        public const string UGYINTEZESI_HATARIDO_MINDIG_MEGJELENIK = "UGYINTEZESI_HATARIDO_MINDIG_MEGJELENIK";
        public const string UGYINTEZO_MASOLAS_UGYROL = "UGYINTEZO_MASOLAS_UGYROL";
        public const string UGYIRAT_CSATOLMANY_DOKUMENTUMKONYVTAR = "UGYIRAT_CSATOLMANY_DOKUMENTUMKONYVTAR";
        public const string UGYIRAT_CSATOLMANY_DOKUMENTUMTARTERULET = "UGYIRAT_CSATOLMANY_DOKUMENTUMTARTERULET";
        public const string UGYIRAT_CSATOLMANY_DOKUMENTUMTARTIPUS = "UGYIRAT_CSATOLMANY_DOKUMENTUMTARTIPUS";
        public const string UGYIRAT_HATARIDO_KEZELES = "UGYIRAT_HATARIDO_KEZELES";
        public const string UGYIRAT_TOMEGES_UPDATE_LIMIT = "UGYIRAT_TOMEGES_UPDATE_LIMIT";
        public const string UGYIRATBAN_MARADO_PELDANY_CIMZETTJE = "UGYIRATBAN_MARADO_PELDANY_CIMZETTJE";
        public const string UGYIRATI_PELDANY_SZUKSEGES = "UGYIRATI_PELDANY_SZUKSEGES";
        public const string UGYIRATLEZARAS_POSTAZOTT_IRATPELDANY_ENABLED = "UGYIRATLEZARAS_POSTAZOTT_IRATPELDANY_ENABLED";
        public const string UI_SETUP_HIDE_DISABLED_ICONS = "UI_SETUP_HIDE_DISABLED_ICONS";
        public const string VISSZAKULDES_ENABLED = "VISSZAKULDES_ENABLED";
        public const string VISSZAKULDES_IRATTARBOL_ENABLED = "VISSZAKULDES_IRATTARBOL_ENABLED";
        public const string VONALKOD_BEJOVO_ELEKTRONIKUS_GENERALT = "VONALKOD_BEJOVO_ELEKTRONIKUS_GENERALT";
        public const string VONALKOD_ELLENORZO_VEKTOR = "VONALKOD_ELLENORZO_VEKTOR";
        public const string VONALKOD_FORMATUM = "VONALKOD_FORMATUM";
        public const string VONALKOD_KEZELES = "VONALKOD_KEZELES";
        public const string VONALKOD_MAGYAR_KARAKTEREK_CSEREJE = "VONALKOD_MAGYAR_KARAKTEREK_CSEREJE";
        public const string VONALKOD_PREFIX = "VONALKOD_PREFIX";
        //!!!!!!!!!!!!!ABC SORRENDBEN ÍRJÁTOK BE A PARAMÉTEREKET (VAGY KÉZZEL RENDEZVE VAGY A CODEMAID SORT AL), HOGY NE LEGYEN CONFLICT ÁLLANDÓAN!!!!!!!!!!!!!!!!!!!!!!!!        

        private const String SuperGUID = "00000000-0000-0000-0000-000000000000";
        private static object _sync = new object();
        private static OrgSeperatedDictionary<String, String> ParameterekCache = new OrgSeperatedDictionary<String, String>();
        public static String Get(System.Web.UI.Page ParentPage, String Name)
        {
            return Get(ParentPage, Name, true);
        }

        public static String Get(System.Web.UI.Page ParentPage, String Name, bool UseCheckLogin)
        {
            if (UseCheckLogin)
            {
                Logger.Debug("Rendszerparameterek.Get UseCheckLogin");
                Authentication.CheckLogin(ParentPage);
            }
            return Get(UI.SetExecParamDefault(ParentPage, new ExecParam()), Name);
        }

        public static String Get(ExecParam execparam, String Name)
        {
            Logger.Debug(String.Format("Rendszerparameterek {0} Get start", Name ?? "NULL"));
            String Value = null;

            if (String.IsNullOrEmpty(execparam.Org_Id))
            {
                Logger.Debug("String.IsNullOrEmpty(execparam.Org_Id");
                string org_id = FelhasznaloNevek_Cache.GetOrgByFelhasznalo(execparam.Felhasznalo_Id, System.Web.HttpContext.Current.Cache);
                Logger.Debug(String.Format("Org_Id:{0}", org_id));
                execparam.Org_Id = org_id;
            }

            #region "szuper" (közös) rendszerparaméterek
            Dictionary<string, string> SuperParam = ParameterekCache.Get(SuperGUID);
            if (SuperParam.Count == 0)
            {
                lock (_sync)
                {
                    if (SuperParam.Count == 0)
                    {
                        ExecParam execParam_super = execparam.Clone();
                        execParam_super.Felhasznalo_Id = SuperGUID;
                        execParam_super.Org_Id = SuperGUID;

                        Logger.Info("Rendszerparameterek: Loading super parameters");
                        Result res = Load(execParam_super);
                        if (!String.IsNullOrEmpty(res.ErrorCode))
                        {
                            return null;
                        }
                    }
                }
            }
            if (SuperParam.ContainsKey(Name))
            {
                SuperParam.TryGetValue(Name, out Value);
                Logger.Debug(String.Format("Rendszerparameterek {0} Get end: {1}", Name ?? "NULL", Value ?? "NULL"));
                return Value;
            }
            #endregion "szuper" (közös) rendszerparaméterek

            if (!String.IsNullOrEmpty(execparam.Org_Id))
            {
                Dictionary<string, string> Param = ParameterekCache.Get(execparam.Org_Id);

                if (Param.Count == 0)
                {
                    lock (_sync)
                    {
                        if (Param.Count == 0)
                        {
                            Result res = Load(execparam);
                            if (!String.IsNullOrEmpty(res.ErrorCode))
                            {
                                return null;
                            }
                        }
                    }
                }

                if (Param.ContainsKey(Name))
                {
                    Param.TryGetValue(Name, out Value);
                    Logger.Debug(String.Format("Rendszerparameterek {0} Get end: {1}", Name ?? "NULL", Value ?? "NULL"));
                    return Value;
                }
            }
            else
            {
                Logger.Warn("Org_Id is empty");
            }

            Logger.Error(String.Format("Rendszerpatameterek Get hiba: {0} rendszerparameter not found", Name));
            return null;
        }

        public static bool GetBoolean(System.Web.UI.Page ParentPage, String Name)
        {
            return GetBoolean(ParentPage, Name, false, true);
        }

        public static bool GetBoolean(System.Web.UI.Page ParentPage, String Name, bool DefaultValue)
        {
            return GetBoolean(ParentPage, Name, DefaultValue, true);
        }

        public static bool GetBoolean(System.Web.UI.Page ParentPage, String Name, bool DefaultValue, bool UseCheckLogin)
        {
            if (UseCheckLogin)
            {
                Logger.Debug("Rendszerparameterek.GetBoolean UseCheckLogin");
                Authentication.CheckLogin(ParentPage);
            }
            return GetBoolean(UI.SetExecParamDefault(ParentPage), Name, DefaultValue);
        }

        public static bool GetBoolean(ExecParam execParam, String Name)
        {
            return GetBoolean(execParam, Name, false);
        }

        public static bool GetBoolean(ExecParam execParam, String Name, bool DefaultValue)
        {
            Logger.Debug("GetBoolean DefaultValue:" + DefaultValue);

            bool _ret = DefaultValue;

            string par;
            par = Get(execParam, Name);

            if (!String.IsNullOrEmpty(par))
            {
                Logger.Debug(String.Format("A {0} rendszerparaméter értéke: {1}.", Name, par));
                if (par == "1" || par.ToLower() == "i" || par.ToLower() == "true" || par.ToLower() == "igen")
                {
                    _ret = true;
                }
                else
                {
                    _ret = false;
                }
            }
            else
            {
                Logger.Warn(String.Format("A {0} rendszerparaméter hiényzik vagy üres.", Name));
            }

            return _ret;
        }

        public static int GetInt(System.Web.UI.Page ParentPage, String Name)
        {
            return GetInt(ParentPage, Name, 0);
        }

        public static int GetInt(System.Web.UI.Page ParentPage, String Name, int DefaultValue)
        {
            return GetInt(ParentPage, Name, DefaultValue, true);
        }

        public static int GetInt(System.Web.UI.Page ParentPage, String Name, int DefaultValue, bool UseCheckLogin)
        {
            if (UseCheckLogin)
            {
                Logger.Debug("Rendszerparameterek.GetInt UseCheckLogin");
                Authentication.CheckLogin(ParentPage);
            }
            return GetInt(UI.SetExecParamDefault(ParentPage, new ExecParam()), Name, DefaultValue);
        }

        public static int GetInt(ExecParam execparam, String Name)
        {
            return GetInt(execparam, Name, 0);
        }

        public static int GetInt(ExecParam execparam, String Name, int DefaultValue)
        {
            Logger.Debug(String.Format("GetInt DefaultValue{0}", DefaultValue));

            int valueNumber = DefaultValue;
            string value = Get(execparam, Name);
            if (value != null)
            {
                Int32.TryParse(value, out valueNumber);
            }

            return valueNumber;
        }

        public static bool IsParamExit(System.Web.UI.Page ParentPage, String Name)
        {
            return IsParamExit(UI.SetExecParamDefault(ParentPage), Name);
        }

        public static bool IsParamExit(ExecParam execParam, String Name)
        {
            string value = Get(execParam, Name);

            bool isExist = (value != null);

            Logger.Debug(String.Format("{0} rendszerparameter exist: {1}", Name ?? "NULL", isExist));

            return (value != null);
        }

        public static bool IsTUK(ExecParam execParam)
        {
            return GetBoolean(execParam, TUK, false);
        }

        public static bool IsTUK(System.Web.UI.Page page)
        {
            return GetBoolean(page, TUK, false);
        }

        private static Result Load(System.Web.UI.Page ParentPage)
        {
            return Load(UI.SetExecParamDefault(ParentPage, new ExecParam()));
        }

        private static Result Load(ExecParam execparam)
        {
            Logger.Debug("Rendszerparameterek Load start");
            Contentum.eAdmin.Service.KRT_ParameterekService service = eAdminService.ServiceFactory.GetKRT_ParameterekService();
            Result res = new Result();
            KRT_Parameterek param = new KRT_Parameterek();
            KRT_ParameterekSearch paramsearch = new KRT_ParameterekSearch();
            res = service.GetAll(execparam, paramsearch);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                Logger.Error("Rendszerparameterek GetAll hiba", execparam, res);      
                return res;
            }

            Dictionary<string, string> Param = ParameterekCache.Get(execparam.Org_Id);

            Param.Clear();

            for (int i = 0; i < res.Ds.Tables[0].Rows.Count; i++)
            {
                string nev = res.Ds.Tables[0].Rows[i]["Nev"].ToString();
                string ertek = res.Ds.Tables[0].Rows[i]["Ertek"].ToString();
                if (!Param.ContainsKey(nev))
                {
                    Param.Add(nev, ertek);
                }
                else
                {
                    Logger.Warn(String.Format("Param ContainsKey:{0}, Value:{1}", nev, ertek));
                }
            }

            Logger.Debug("Rendszerparameterek Load end");
            return res;
        }
        #region CR3187 - Nyomtatás header
        public const string NYOMTATAS_HEADER = "NYOMTATAS_HEADER";
        #endregion
        #region UGYINTEZESI_IDO_FELFUGGESZTES
        public const string UGYINTEZESI_IDO_FELFUGGESZTES = "UGYINTEZESI_IDO_FELFUGGESZTES";

        public static Constants.EnumUgyintezesiIdoFelfuggesztes GetUgyintezesiIdoFelfuggesztes(System.Web.UI.Page page)
        {
            string stringValue = Get(page, Rendszerparameterek.UGYINTEZESI_IDO_FELFUGGESZTES);

            if (string.IsNullOrEmpty(stringValue))
                return Constants.EnumUgyintezesiIdoFelfuggesztes.SIMPLE;

            Constants.EnumUgyintezesiIdoFelfuggesztes returnValue =
                (Constants.EnumUgyintezesiIdoFelfuggesztes)Enum.Parse(typeof(Constants.EnumUgyintezesiIdoFelfuggesztes), stringValue);
            return returnValue;
        }
        public static Constants.EnumUgyintezesiIdoFelfuggesztes GetUgyintezesiIdoFelfuggesztesXpm(ExecParam execParam)
        {
            string stringValue = Get(execParam, Rendszerparameterek.UGYINTEZESI_IDO_FELFUGGESZTES);

            if (string.IsNullOrEmpty(stringValue))
                return Constants.EnumUgyintezesiIdoFelfuggesztes.SIMPLE;

            Constants.EnumUgyintezesiIdoFelfuggesztes returnValue =
                (Constants.EnumUgyintezesiIdoFelfuggesztes)Enum.Parse(typeof(Constants.EnumUgyintezesiIdoFelfuggesztes), stringValue);
            return returnValue;
        }

        public static bool UseSakkora(ExecParam param)
        {
            return Rendszerparameterek.GetUgyintezesiIdoFelfuggesztesXpm(param) == Constants.EnumUgyintezesiIdoFelfuggesztes.FANCY; //SAKKORAS UGYFEL
        }
        #endregion        

    }
}
