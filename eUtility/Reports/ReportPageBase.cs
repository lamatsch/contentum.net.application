﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace Contentum.eUtility
{
    public class ReportPageBase : UI.PageBase
    {
        private ReportViewer _reportViewer;

        protected void InitReport(ReportViewer reportViewer, bool autoPdfDownload, string pdfFileName, string kornyezet)
        {
            if (!IsPostBack)
            {
                _reportViewer = reportViewer;

                reportViewer.ServerReport.ReportServerCredentials = SSRSHelper.GetSSRSCredential();
                Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
                reportViewer.ServerReport.ReportServerUrl = ReportServerUrl;
                string dir = ConfigurationManager.AppSettings["SSRS_SERVER_DIR"];
                reportViewer.ServerReport.ReportPath = (string.IsNullOrEmpty(dir) ? "" : "/" + dir) + reportViewer.ServerReport.ReportPath;

                if (kornyezet.Equals("FPH") || kornyezet.Equals("CSEPEL") || kornyezet.Equals("BOPMH")) // refactored from UgyUgyiratokPrintFormSSRSEloadoiIvTomeges.aspx.cs
                {
                    reportViewer.ServerReport.ReportPath += kornyezet;
                }

                ReportParameterInfoCollection rpis = reportViewer.ServerReport.GetParameters();

                if (rpis.Count > 0)
                {
                    ReportParameter[] ReportParameters = GetReportParameters(rpis);

                    reportViewer.ServerReport.SetParameters(ReportParameters);
                }

                reportViewer.ShowParameterPrompts = false;
                reportViewer.ReportError += ReportViewer1_ReportError;
                reportViewer.ServerReport.Refresh();

                // BLG 6295: SSRS riportok automatikus letöltése PDF-ben
                if (autoPdfDownload)
                {
                    RenderFileToDownload("PDF", pdfFileName);
                }
            }
        }

        private void ReportViewer1_ReportError(object sender, ReportErrorEventArgs e)
        {
            if (e.Exception != null)
            {
                Debug.WriteLine(e.Exception.ToString());
                throw e.Exception;
            }
        }

        protected void RenderFileToDownload(string format, string fileName)
        {
            if (String.IsNullOrEmpty(format) || String.IsNullOrEmpty(fileName))
            {
                return;
            }

            string mimeType, encoding, extension;
            Warning[] warnings;
            string[] streamIds;
            byte[] data = _reportViewer.ServerReport.Render(format, null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            if (!fileName.EndsWith("." + extension, StringComparison.InvariantCultureIgnoreCase))
            {
                fileName += "." + extension;
            }

            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("Content-Disposition", "attachment; filename=" + Uri.EscapeDataString(fileName));
            Response.BinaryWrite(data);
            Response.End();
        }

        protected void Print()
        {
            string mimeType; //, encoding, extension;
            int w, h;
            bool isLandscape;
            //Warning[] warnings;
            //string[] streamIds;
            //var exts = _reportViewer.ServerReport.ListRenderingExtensions();

            //byte[] data = _reportViewer.ServerReport.Render("IMAGE", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            //PrintImages(new string[] { System.Convert.ToBase64String(data) }, mimeType);

            var base64images = RenderPagesToBase64(out mimeType, out w, out h, out isLandscape);
            PrintImages(base64images, mimeType, w, h, isLandscape);
        }

        private static string ReadBase64FromStream(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return System.Convert.ToBase64String(ms.ToArray());
            }
        }

        private List<string> RenderPagesToBase64(out string mimeType, out int w, out int h, out bool isLandscape)
        {
            string extension;

            var base64images = new List<string>();
            var m_reportStreams = new List<Stream>();
            try
            {
                var pageSettings = _reportViewer.ServerReport.GetDefaultPageSettings();
                w = (int)Math.Round(pageSettings.PaperSize.Width / 3.937);
                h = (int)Math.Round(pageSettings.PaperSize.Height / 3.937);
                isLandscape = pageSettings.IsLandscape;
                string deviceInfo = String.Format(
      "<DeviceInfo>" +
      "  <OutputFormat>PNG</OutputFormat>" +
      "  <PageWidth>{0}mm</PageWidth>" +
      "  <PageHeight>{1}mm</PageHeight>" +
      "  <MarginTop>0mm</MarginTop>" +
      "  <MarginLeft>0mm</MarginLeft>" +
      "  <MarginRight>0mm</MarginRight>" +
      "  <MarginBottom>0mm</MarginBottom>" +
      "</DeviceInfo>", w, h);

                // Tell SSRS to store one stream per page on server
                NameValueCollection urlAccessParameters = new NameValueCollection();
                urlAccessParameters.Add("rs:PersistStreams", "True");

                // Render first page
                Stream s = _reportViewer.ServerReport.Render("IMAGE", deviceInfo, urlAccessParameters, out mimeType, out extension);
                m_reportStreams.Add(s);

                // Loop to get other streams
                urlAccessParameters.Remove("rs:PersistStreams");
                urlAccessParameters.Add("rs:GetNextStream", "True");
                do
                {
                    s = _reportViewer.ServerReport.Render("IMAGE", deviceInfo, urlAccessParameters, out mimeType, out extension);
                    if (s.Length != 0) m_reportStreams.Add(s);
                }
                while (s.Length > 0);

                // Now there's one stream per page - do stuff with it
                foreach (var stream in m_reportStreams)
                {
                    if (stream != null)
                    {
                        base64images.Add(ReadBase64FromStream(stream));
                    }
                }
            }
            finally
            {
                foreach (Stream s in m_reportStreams)
                {
                    s.Close();
                    s.Dispose();
                }
                m_reportStreams = null;
            }
            return base64images;
        }

        private void PrintImages(IEnumerable<string> base64images, string mimeType, int w, int h, bool isLandscape)
        {
            // change the mimeType to render tiff images correctly
            //if (mimeType == "image/TIF")
            //{
            //    mimeType = "image/tiff";
            //}

            Response.Clear();
            Response.ContentType = "text/html";
            Response.Write(@"<html>
<head>
    <style type='text/css' media='print'>
        @page {
            size: " + w.ToString() + "mm " + h.ToString() + "mm " + (isLandscape ? "landscape" : "") + @";
            margin: 0;
        }" + (isLandscape ? 
        @"body { 
            writing-mode: tb-rl;
        }" : "") +
    @"</style>
</head>
<body onload='javascript:window.print();'>");

            var i = 0;
            foreach (var data in base64images)
            {
                if (i > 0)
                {
                    // page break
                    Response.Write("<div style=page-break-before:always align='center'><span style='visibility: hidden;'>-</span></div>");
                }
                // base64 image
                Response.Write(String.Format("<img src='data:{0};base64,{1}' border='0'>", mimeType, data));
                i++;
            }
            Response.Write("</body></html>");
            Response.End();
        }

        protected string GetReportFileName(string suffix)
        {
            var fn = Request.QueryString.Get("reportFileName");
            if (String.IsNullOrEmpty(fn))
            {
                fn = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            }
            return fn + (suffix == null ? "" : " " + suffix.Trim());
        }

        protected virtual ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
        {
            return new ReportParameter[0];
        }
    }
}
