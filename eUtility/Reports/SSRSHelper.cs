﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace Contentum.eUtility
{
    public static class SSRSHelper
    {
        public static ReportViewerCredentials GetSSRSCredential()
        {
            // BLG_1643
            //return new ReportViewerCredentials("INFORMATIKA\\" + System.Environment.MachineName + "$", "");
            return new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);

        }
        /// <summary>
        /// Visszaadja a SSRS riport pdf fájlt
        /// </summary>
        /// <param name="reportServerUrl">Szerver címe</param>
        /// <param name="reportPath">Riport útvonala</param>
        /// <param name="reportParameters">Riport paraméterei</param>
        /// http://ax-vfphtst02/reportserver/EDOKTesztRS/Iktatas/AlszamraIktatottIratokListaja
        /// <returns></returns>
        public static byte[] GetSSRSRiportPdf(Uri reportServerUrl, string reportPath, Dictionary<string, string> inReportParameters)
        {
            byte[] reportContent = null;
            ReportViewer reportViewer = null;
            ReportParameter[] reportParameter = null;
            try
            {
                ReportViewerCredentials rvc = GetSSRSCredential();

                #region REPORT VIEWER
                reportViewer = new ReportViewer();
                reportViewer.ServerReport.ReportServerCredentials = rvc;
                reportViewer.ServerReport.ReportServerUrl = reportServerUrl;
                reportViewer.ServerReport.ReportPath = reportPath;

                reportParameter = GetReportParameters(inReportParameters);
                reportViewer.ServerReport.SetParameters(reportParameter);
                reportViewer.ServerReport.Refresh();
                #endregion REPORT VIEWER

                #region SSRS EXPORT
                Warning[] warnings;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = string.Empty;
                reportContent = reportViewer.ServerReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                #endregion
            }
            catch (Exception exc)
            {
                Logger.Error("Contentum.eUtility.SSRSHelper ", exc);
                throw;
            }
            return reportContent;
        }
        /// <summary>
        /// GetReportParameters
        /// </summary>
        /// <param name="inParams"></param>
        /// <returns></returns>
        private static ReportParameter[] GetReportParameters(Dictionary<string, string> inParams)
        {
            List<ReportParameter> result = new List<ReportParameter>();
            foreach (KeyValuePair<string, string> item in inParams)
            {
                result.Add(new ReportParameter(item.Key, item.Value));
            }
            return result.ToArray();
        }
    }
}
