using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUtility
{
    public static class Resources
    {
        private static System.Globalization.CultureInfo _CultureInfo = new System.Globalization.CultureInfo("hu-HU");
        private static System.Resources.ResourceManager _Error = null;
        private static System.Resources.ResourceManager _Form = null;
        private static System.Resources.ResourceManager _List = null;
        private static System.Resources.ResourceManager _LovList = null;
        private static System.Resources.ResourceManager _Question = null;
        private static System.Resources.ResourceManager _Search = null;

        static Resources() 
        {
            LoadAssemblies();
        }

        private static void LoadAssemblies()
        {          
            System.Reflection.Assembly asmError = System.Reflection.Assembly.Load("Contentum.eResources.Error");
            System.Reflection.Assembly assemblyForErrorResources = null;
            try
            {
                assemblyForErrorResources = asmError.GetSatelliteAssembly(_CultureInfo);
            }
            catch 
            {
                 assemblyForErrorResources = asmError;
            }
            _Error = new System.Resources.ResourceManager("Error", assemblyForErrorResources);

            System.Reflection.Assembly asmForm = System.Reflection.Assembly.Load("Contentum.eResources.Form");
            System.Reflection.Assembly assemblyForFormResources = null;
            try
            {
               assemblyForFormResources = asmForm.GetSatelliteAssembly(_CultureInfo);
            }
            catch
            {
                assemblyForFormResources = asmForm;
            }
            _Form = new System.Resources.ResourceManager("Form", assemblyForFormResources);

            System.Reflection.Assembly asmList = System.Reflection.Assembly.Load("Contentum.eResources.List");
            System.Reflection.Assembly assemblyForListResources = null;
            try
            {
                assemblyForListResources = asmList.GetSatelliteAssembly(_CultureInfo);
            }
            catch
            {
                assemblyForListResources = asmList; 
            }
            _List = new System.Resources.ResourceManager("List", assemblyForListResources);

            System.Reflection.Assembly asmLovList = System.Reflection.Assembly.Load("Contentum.eResources.LovList");
            System.Reflection.Assembly assemblyForLovListResources = null;
            try
            {
                assemblyForLovListResources = asmLovList.GetSatelliteAssembly(_CultureInfo);
            }
            catch
            {
                assemblyForLovListResources = asmLovList;
            }
            _LovList = new System.Resources.ResourceManager("LovList", assemblyForLovListResources);

            System.Reflection.Assembly asmQuestion = System.Reflection.Assembly.Load("Contentum.eResources.Question");
            System.Reflection.Assembly assemblyForQuestionResources = null;
            try
            {
                assemblyForQuestionResources = asmQuestion.GetSatelliteAssembly(_CultureInfo);
            }
            catch
            {
                assemblyForQuestionResources = asmQuestion;
            }
            _Question = new System.Resources.ResourceManager("Question", assemblyForQuestionResources);

            System.Reflection.Assembly asmSearch = System.Reflection.Assembly.Load("Contentum.eResources.Search");
            System.Reflection.Assembly assemblyForSearchResources = null;
            try
            {
                assemblyForSearchResources = asmSearch.GetSatelliteAssembly(_CultureInfo);
            }
            catch
            {
                assemblyForSearchResources = asmSearch;
            }
            _Search = new System.Resources.ResourceManager("Search", assemblyForSearchResources);        
        }

        public static System.Globalization.CultureInfo CultureInfo
        {
            get { return _CultureInfo;  }
            set
            {
                LoadAssemblies();
                _CultureInfo = value; 
            }
        }

        public static System.Resources.ResourceManager Error
        {
            get {
                return _Error;
            }
        }
        public static System.Resources.ResourceManager Form
        {
            get
            {
                return _Form;
            }
        }
        public static System.Resources.ResourceManager List
        {
            get
            {
                return _List;
            }
        }
        public static System.Resources.ResourceManager LovList
        {
            get
            {
                return _LovList;
            }
        }
        public static System.Resources.ResourceManager Question
        {
            get
            {
                return _Question;
            }
        }
        public static System.Resources.ResourceManager Search
        {
            get
            {
                return _Search;
            }
        }

    }
}
