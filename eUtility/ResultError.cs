﻿using Contentum.eBusinessDocuments;
using Contentum.eUIControls;
using System;
using System.Web.UI;

namespace Contentum.eUtility
{
    public class ResultError
    {
        public ResultError()
        { }
        /// <summary>
        /// Visszaadja a megadott hibakódhoz tartozó hibaüzenetet
        /// </summary>
        /// <param name="errorCode">Hibakód</param>
        /// <returns></returns>
        public static string GetErrorMessageByErrorCode(int errorCode)
        {
            string resourceName = "ErrorCode_" + errorCode.ToString();
            try
            {
                string errorMessage = Resources.Error.GetString(resourceName);
                if (errorMessage != null)
                {
                    return errorMessage;
                }
                else
                {
                    // Nincs hibaüzenet

                    // Ha a hibakód alapján az SQL szerverről jött a hiba, más hibaüzenet:
                    if (errorCode < 50000 && errorCode > 0)
                    {
                        return Resources.Error.GetString("ErrorFromSqlServer")
                            + " " + Resources.Error.GetString("ErrorCode") + ": [" + errorCode.ToString() + "]";

                    }
                    else
                    {
                        return Resources.Error.GetString("NotErrorCode") + " "
                            + Resources.Error.GetString("ErrorCode") + ": [" + errorCode.ToString() + "]";
                    }
                }
            }
            catch (System.Resources.MissingManifestResourceException)
            {
                return Resources.Error.GetString("NotErrorCode");
            }
        }

        /// <summary>
        /// result.ErrorMessage-ben megadott hibakódhoz tartozó hibaüzenetet adja vissza
        /// result.ErrorMessage formátuma: '[ERRORCODE] ... '
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static string GetErrorMessageFromResultObject(Result result)
        {
            string resultErrorMsg = result.ErrorMessage;
            if (resultErrorMsg.StartsWith("[") == false)
            {
                return result.ErrorMessage;
            }
            else
            {
                try
                {
                    int index = resultErrorMsg.IndexOf("]");
                    int errorCode = Int32.Parse(resultErrorMsg.Substring(1, index - 1));
                    return ResultError.GetErrorMessageByErrorCode(errorCode);
                }
                catch (Exception)
                {
                    return result.ErrorMessage;
                }
            }
        }

        /// <summary>
        /// ErrorMessage-ben megadott hibakódhoz tartozó hibaüzenetet adja vissza
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static string ErrorMessageCodeToDetailedText(string errorMessage)
        {
            if (string.IsNullOrEmpty(errorMessage))
                return null;
            if (errorMessage.Contains("[") && errorMessage.Contains("]"))
            {
                try
                {
                    int startindex = errorMessage.IndexOf("[");
                    int endindex = errorMessage.IndexOf("]");
                    if (startindex < 0)
                        return errorMessage;

                    int errorCode = int.Parse(errorMessage.Substring(startindex + 1, endindex - startindex - 1));
                    return GetErrorMessageByErrorCode(errorCode);
                }
                catch (Exception)
                {
                    return errorMessage;
                }
            }
            else
                return errorMessage;
        }
        /// <summary>
        /// A Result objektum ErrorMessage mezőjéből kinyeri a hiba kódját. 
        /// Az ErrorMessage mezőben a hibakódoknak '[xxxxx]' formában kell szerepelniük.
        /// Ha nem sikerül megállapítani a hibakódot, a visszatérési érték -1.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static int GetErrorNumberFromResult(Result result)
        {
            string resultErrorMsg = result.ErrorMessage;
            if (resultErrorMsg.StartsWith("[") == false)
            {
                return -1;
            }
            else
            {
                try
                {
                    int index = resultErrorMsg.IndexOf("]");
                    int errorNumber = Int32.Parse(resultErrorMsg.Substring(1, index - 1));
                    return errorNumber;
                }
                catch (Exception)
                {
                    return -1;
                }
            }
        }


        /// <summary>
        /// Megjeleníti az esetleges hibaüzenetet a megadott ErrorPanelon
        /// </summary>
        /// <param name="errorPanel"></param>
        /// <param name="result"></param>
        public static void DisplayResultErrorOnErrorPanel(eErrorPanel errorPanel, Result result)
        {
            DisplayResultErrorOnErrorPanel(errorPanel, result, null);
        }

        public static void DisplayResultErrorOnErrorPanel(
            eErrorPanel errorPanel, Result result, string customErrorHeader)
        {
            errorPanel.ErrorCode = result.ErrorCode;
            errorPanel.ErrorMessage = result.ErrorMessage;
            if (result.ErrorType == Constants.ErrorType.Warning) errorPanel.IsWarning = true;
            if (errorPanel.Page != null) errorPanel.CurrentLogEntry = (string)(errorPanel.Page.Session[Constants.PageStartDate] ?? String.Empty);
            ResultError resultError = new ResultError(result);
            if (resultError.IsError)
            {
                //A hiba loggolása
                Page page = errorPanel.Page;
                if (page != null)
                {
                    Log.Error.Page(page, result);
                }

                if (string.IsNullOrEmpty(customErrorHeader))
                {
                    errorPanel.Header = resultError.ErrorHeader;
                }
                else
                {
                    errorPanel.Header = customErrorHeader;
                }
                errorPanel.Body = resultError.ErrorMessage;
                errorPanel.Enabled = true;
                errorPanel.Visible = true;
                if (errorPanel.Parent.Parent.GetType() == typeof(UpdatePanel))
                {
                    UpdatePanel up = (UpdatePanel)errorPanel.Parent.Parent;
                    up.Update();
                }

                #region ErrorDetail információk megjelenítése

                if (result.ErrorDetail != null && !result.ErrorDetail.IsEmpty)
                {
                    string errorDetailMsg = GetErrorDetailInfoMsg(result.ErrorDetail, false, false, errorPanel.Page);

                    if (!String.IsNullOrEmpty(errorDetailMsg))
                    {
                        // link (window.open) a problémás objektumra:
                        string htmlLink = GetHtmlLinkFromErrorDetail(result.ErrorDetail);
                        if (!String.IsNullOrEmpty(htmlLink))
                        {
                            htmlLink = "<br/>" + htmlLink;
                        }

                        errorPanel.Body += " <br/>(" + errorDetailMsg + ")" + htmlLink;
                    }
                }

                #endregion
            }
        }


        public static void DisplayErrorOnErrorPanel(eErrorPanel errorPanel,
            String errorHeader, String errorMessage)
        {
            //A hiba loggolása
            Page page = errorPanel.Page;
            if (page != null)
            {
                Result result = new Result();
                result.ErrorCode = String.Empty;
                result.ErrorMessage = errorMessage;
                if (errorPanel.IsWarning) result.ErrorType = Constants.ErrorType.Warning;
                errorPanel.CurrentLogEntry = (string)(page.Session[Constants.PageStartDate] ?? String.Empty);
                Log.Error.Page(page, result);
            }
            errorPanel.Header = errorHeader;
            errorPanel.Body = errorMessage;
            errorPanel.Visible = true;
        }

        public static void DisplayWarningOnErrorPanel(eErrorPanel errorPanel,
            String errorHeader, String errorMessage)
        {
            errorPanel.IsWarning = true;
            DisplayErrorOnErrorPanel(errorPanel, errorHeader, errorMessage);
        }

        public static void DisplayNoIdParamError(eErrorPanel errorPanel)
        {
            if (errorPanel == null) return;
            errorPanel.Visible = true;
            errorPanel.Header = Resources.Error.GetString("DefaultErrorHeader");
            errorPanel.Body = Resources.Error.GetString("UINoIdParam");
            if (errorPanel.Page != null) errorPanel.CurrentLogEntry = (string)(errorPanel.Page.Session[Constants.PageStartDate] ?? String.Empty);
            //A hiba loggolása
            Page page = errorPanel.Page;
            if (page != null)
            {
                Result result = new Result();
                result.ErrorCode = String.Empty;
                result.ErrorMessage = errorPanel.Body;
                Log.Error.Page(page, result);
            }
        }

        #region BLG_2219
        /// <summary>
        /// Hibakezeléshez készült függvény, amely kiírja az esetleges DB insert, delete hibákat. A partnerkapcsaolatokForm.aspx képernyőre.
        /// </summary>
        /// <param name="errorPanel"></param>
        /// <param name="result"></param>
        public static void DisplayErrorMessage(eErrorPanel errorPanel, Result result)
        {
            if (errorPanel == null) return;
            errorPanel.Visible = true;
            errorPanel.Header = Resources.Error.GetString("DefaultErrorHeader") + " - " + result.ErrorCode;

            string errorString = result.ErrorMessage.Trim(new char[2] { '[', ']' });
            errorPanel.Body = Resources.Error.GetString("ErrorCode_" + errorString);

            if (errorPanel.Page != null) errorPanel.CurrentLogEntry = (string)(errorPanel.Page.Session[Constants.PageStartDate] ?? String.Empty);
            //A hiba loggolása
            Page page = errorPanel.Page;
            if (page != null)
            {
                result = new Result();
                result.ErrorCode = String.Empty;
                result.ErrorMessage = errorPanel.Body;
                Log.Error.Page(page, result);
            }
        }
        #endregion

        public static void DisplayFormatExceptionOnErrorPanel(eErrorPanel errorPanel,
            FormatException fe)
        {
            DisplayErrorOnErrorPanel(errorPanel, Resources.Error.GetString("UIFormatErrorHeader"), fe.Message);
            if (errorPanel.Parent.Parent.GetType() == typeof(UpdatePanel))
            {
                UpdatePanel up = (UpdatePanel)errorPanel.Parent.Parent;
                up.Update();
            }
        }

        /// <summary>
        /// Létrehoz egy új Result objektumot a megadott hibakóddal
        /// </summary>
        /// <param name="ErrorCode"></param>
        /// <returns></returns>
        public static Result CreateNewResultWithErrorCode(int ErrorCode)
        {
            return CreateNewResultWithErrorCode(ErrorCode, null);
        }

        public static Result CreateNewResultWithErrorCode(int ErrorCode, ErrorDetails errorDetail)
        {
            Result newResult = new Result();
            String ErrorCode_str = ErrorCode.ToString();
            String ErrorMsg = GetErrorMessage(ErrorCode);
            newResult.ErrorCode = ErrorCode_str;
            newResult.ErrorMessage = ErrorMsg;
            newResult.ErrorDetail = errorDetail;
            return newResult;
        }

        public static string GetErrorMessage(int ErrorCode)
        {
            return "[" + ErrorCode.ToString() + "]";
        }

        public static void SetResultByErrorCode(Result result, int ErrorCode)
        {
            String ErrorCode_str = ErrorCode.ToString();
            String ErrorMsg = GetErrorMessage(ErrorCode);
            result.ErrorCode = ErrorCode_str;
            result.ErrorMessage = ErrorMsg;
        }


        private bool isError;
        /// <summary>
        /// true: van hiba; false: nincs hiba
        /// </summary>
        public bool IsError
        {
            get { return isError; }
        }

        private string errorMessage;

        public string ErrorMessage
        {
            get { return errorMessage; }
        }

        private string errorHeader;

        public string ErrorHeader
        {
            get { return errorHeader; }
        }

        // Az eredeti result obj. ErrorMessage mezője
        private string result_errorMessage;

        public int Error_Number
        {
            get
            {
                try
                {
                    int index = result_errorMessage.IndexOf("]");
                    int errorCode = Int32.Parse(result_errorMessage.Substring(1, index - 1));

                    return errorCode;
                }
                catch
                {
                    return 0;
                }
            }
        }

        public ResultError(Result result)
        {
            if (String.IsNullOrEmpty(result.ErrorMessage))
            {
                // nincs hiba
                isError = false;
                errorMessage = "";
                errorHeader = "";
            }
            else
            {
                isError = true;

                // lementjük az eredeti hibaüzenetet
                result_errorMessage = result.ErrorMessage;

                errorHeader = Resources.Error.GetString("DefaultErrorHeader");

                errorMessage = ResultError.GetErrorMessageFromResultObject(result);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_isError">true: van hiba; false: nincs hiba</param>
        public ResultError(bool _isError)
        {
            if (_isError == true)
            {
                isError = true;
                errorMessage = "[General error]";
                errorHeader = "Error";
            }
            else
            {
                isError = false;
                errorMessage = "";
                errorHeader = "";
            }
        }

        /// <summary>
        /// Megvizgsálja, hogy a lekérdezés eredményének rekordszámossága nem nulla-e
        /// Ha nulla beállítja a ErrorCode-ot és ErrorMessage-t 50101-re
        /// </summary>
        /// <param name="result">a lekérés ereménye</param>
        public static void CreateNoRowResultError(Result result)
        {
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Ds == null || result.Ds.Tables.Count == 0 || result.Ds.Tables[0] == null || result.Ds.Tables[0].Rows.Count == 0)
                {
                    //record lekérése az adatbázisból sikertelen
                    result.ErrorCode = result.ErrorMessage = "[50101]";
                }
            }
        }

        public static string GetErrorDetailInfoMsg(ErrorDetails errorDetail, Page page)
        {
            return GetErrorDetailInfoMsg(errorDetail, false, false, page);
        }

        public static string GetErrorDetailInfoMsg(ErrorDetails errorDetail, ExecParam execParam, System.Web.Caching.Cache cache)
        {
            return GetErrorDetailInfoMsg(errorDetail, false, false, execParam, cache);
        }

        public static string GetErrorDetailMsgForAlert(ErrorDetails errorDetail, Page page)
        {
            return GetErrorDetailInfoMsg(errorDetail, true, true, page);
        }

        private static string GetErrorDetailInfoMsg(ErrorDetails errorDetail, bool needNewLineChar, bool kellZarojel, Page page)
        {
            ExecParam execParam = UI.SetExecParamDefault(page);
            var cache = page.Cache;
            return GetErrorDetailInfoMsg(errorDetail, needNewLineChar, kellZarojel, execParam, cache);
        }

        private static string GetErrorDetailInfoMsg(ErrorDetails errorDetail, bool needNewLineChar, bool kellZarojel, ExecParam execParam, System.Web.Caching.Cache cache)
        {
            // TODO: Szövegeket Resource file-ba áttenni

            // csak ha van adat az errorDetailben:
            if (errorDetail != null && !errorDetail.IsEmpty)
            {
                string msg = "";
                if (needNewLineChar)
                {
                    msg += "\\n";
                }

                string errorMessage = "";
                if (errorDetail.Code > 0)
                {
                    // TODO: errorMessage megállapítása a hibakódból
                    errorMessage = GetErrorMessageByErrorCode(errorDetail.Code);
                }
                else
                {
                    errorMessage = errorDetail.Message;
                }


                // Oszlop értékének(szövegének) megállapítása:
                string columnMsg = "";
                if (!String.IsNullOrEmpty(errorDetail.ObjectType)
                    && !String.IsNullOrEmpty(errorDetail.ColumnType)
                    && !String.IsNullOrEmpty(errorDetail.ColumnValue))
                {
                    switch (errorDetail.ObjectType)
                    {
                        case Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok:
                            {
                                #region Ügyiratoknál lehetséges oszloptípusok vizsgálata:
                                switch (errorDetail.ColumnType)
                                {
                                    case Constants.ErrorDetails.ColumnTypes.Allapot:
                                        {
                                            string allapot = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                KodTarak.UGYIRAT_ALLAPOT.KodcsoportKod, errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "Az ügyirat állapota: " + allapot;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.SakkoraAllapot:
                                        {
                                            string allapot = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                KodTarak.IRAT_HATASA_UGYINTEZESRE.KCS, errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "Az ügyirat sakkóra állapota: " + allapot;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Csoport_Id_Felelos:
                                        {
                                            string felelosNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            // (Felelős == Kezelő)
                                            columnMsg = "Az ügyirat kezelője: " + felelosNev;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.FelhasznaloCsoport_Id_Orzo:
                                        {
                                            string orzoNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "Az ügyirat őrzője: " + orzoNev;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.TovabbitasAlattAllapot:
                                        {
                                            string tovabbitasAlattAllapot = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                KodTarak.UGYIRAT_ALLAPOT.KodcsoportKod, errorDetail.ColumnValue, execParam, cache);
                                            string allapotNev_tovabbitasAlatt = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                KodTarak.UGYIRAT_ALLAPOT.KodcsoportKod, KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt, execParam, cache);

                                            columnMsg = "Az ügyirat állapota: " + allapotNev_tovabbitasAlatt + "(" + tovabbitasAlattAllapot + ")";
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.SkontrobaHelyezesJovahagyo:
                                        {
                                            string jovahagyoNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "Az skontróba helyezés jóváhagyója: " + jovahagyoNev;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.LezarasDat:
                                        {
                                            columnMsg = "Ügyirat lezárás időpontja: " + errorDetail.ColumnValue;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.UgyUgyirat_Id_Szulo:
                                        {
                                            if (String.IsNullOrEmpty(errorDetail.ColumnValue))
                                            {
                                                columnMsg = "Nincs megadva szülő ügyirat.";
                                            }
                                            else
                                            {
                                                columnMsg = "Nem megfelelő szülő ügyirat: " + errorDetail.ColumnValue;
                                            }
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Jelleg:
                                        {
                                            string jelleg = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                KodTarak.UGYIRAT_JELLEG.KodcsoportKod, errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "Az ügyirat fajtája: " + jelleg;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.IrattarozasJovahagyo:
                                        {
                                            string jovahagyoNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "Az irattárba küldés jóváhagyója: " + jovahagyoNev;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.ElintezesJovahagyo:
                                        {
                                            string jovahagyoNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "Az elintézetté nyilvánítás jóváhagyója: " + jovahagyoNev;
                                        }
                                        break;
                                }
                                #endregion
                            }
                            break;
                        case Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratdarabok:
                            {
                                #region Ügyiratdaraboknál lehetséges oszloptípusok vizsgálata:
                                switch (errorDetail.ColumnType)
                                {
                                    case Constants.ErrorDetails.ColumnTypes.Allapot:
                                        {
                                            string allapot = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                "UGYIRATDARAB_ALLAPOT", errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "Az ügyiratdarab állapota: " + allapot;
                                        }
                                        break;
                                }
                                #endregion
                            }
                            break;
                        case Constants.ErrorDetails.ObjectTypes.EREC_IraIratok:
                            {
                                #region Iratoknál lehetséges oszloptípusok vizsgálata:
                                switch (errorDetail.ColumnType)
                                {
                                    case Constants.ErrorDetails.ColumnTypes.Allapot:
                                        {
                                            string allapot = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                "IRAT_ALLAPOT", errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "Az irat állapota: " + allapot;
                                        }
                                        break;
                                }
                                #endregion
                            }
                            break;
                        case Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok:
                            {
                                #region Iratpéldányoknál lehetséges oszloptípusok vizsgálata:
                                switch (errorDetail.ColumnType)
                                {
                                    case Constants.ErrorDetails.ColumnTypes.Allapot:
                                        {
                                            string allapot = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                "IRATPELDANY_ALLAPOT", errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "Az iratpéldány állapota: " + allapot;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.FelhasznaloCsoport_Id_Orzo:
                                        {
                                            string orzoNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "Az iratpéldány őrzője: " + orzoNev;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Csoport_Id_Felelos:
                                        {
                                            string felelosNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            // (Felelős == Kezelő)
                                            columnMsg = "Az iratpéldány kezelője: " + felelosNev;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Sorszam:
                                        {
                                            columnMsg = "Az iratpéldány sorszáma: " + errorDetail.ColumnValue;
                                        }
                                        break;
                                }
                                #endregion
                            }
                            break;
                        case Constants.ErrorDetails.ObjectTypes.EREC_KuldKuldemenyek:
                            {
                                #region Küldeményeknél lehetséges oszloptípusok vizsgálata:
                                switch (errorDetail.ColumnType)
                                {
                                    case Constants.ErrorDetails.ColumnTypes.Allapot:
                                        {
                                            string allapot = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                "KULDEMENY_ALLAPOT", errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A küldemény állapota: " + allapot;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.IktatniKell:
                                        {
                                            string iktatniKell = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                "IKTATASI_KOTELEZETTSEG", errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A küldemény iktatási kötelezettsége: " + iktatniKell;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.FelhasznaloCsoport_Id_Orzo:
                                        {
                                            string orzoNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A küldemény őrzője: " + orzoNev;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Csoport_Id_Felelos:
                                        {
                                            string kezeloNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A küldemény kezelője: " + kezeloNev;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.PIRAllapot:
                                        {
                                            // TODO. kódtár
                                            string pirallapot = null;
                                            switch (errorDetail.ColumnValue)
                                            {
                                                case "F":
                                                    pirallapot = "Fogadott";
                                                    break;
                                                case "E":
                                                    pirallapot = "Ellenőrzött";
                                                    break;
                                                case "V":
                                                    pirallapot = "Véglegesített";
                                                    break;
                                                case "R":
                                                    pirallapot = "Rontott";
                                                    break;
                                                default:
                                                    if (String.IsNullOrEmpty(errorDetail.ColumnValue))
                                                    {
                                                        pirallapot = "(üres)";
                                                    }
                                                    else
                                                    {
                                                        pirallapot = errorDetail.ColumnValue;
                                                    }
                                                    break;

                                            }
                                            columnMsg = "A küldemény PIR-beli állapota: " + pirallapot;
                                        }
                                        break;
                                }
                                #endregion
                            }
                            break;
                        case Constants.ErrorDetails.ObjectTypes.EREC_IrattariKikero:
                            {
                                #region Irattári kikérőknél lehetséges oszloptípusok vizsgálata:
                                switch (errorDetail.ColumnType)
                                {
                                    case Constants.ErrorDetails.ColumnTypes.Allapot:
                                        {
                                            string allapot = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                "IRATTARIKIKEROALLAPOT", errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "Az irattári kikérő állapota: " + allapot;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Kikero_Id:
                                        {
                                            string kikeroNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A kikérő: " + kikeroNev;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Jovahagyo_Id:
                                        {
                                            string jovahagyoNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A jóváhagyó: " + jovahagyoNev;
                                        }
                                        break;

                                }
                                #endregion
                            }
                            break;
                        case Constants.ErrorDetails.ObjectTypes.EREC_HataridosFeladatok:
                            {
                                #region Határidős feladatoknál lehetséges oszloptípusok vizsgálata:

                                switch (errorDetail.ColumnType)
                                {
                                    case Constants.ErrorDetails.ColumnTypes.Allapot:
                                        {
                                            string allapot = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                KodTarak.FELADAT_ALLAPOT.kcsNev, errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A határidős feladat állapota: " + allapot;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Felhasznalo_Id_kiado:
                                        {
                                            string kiadoNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A feladat kiadója: " + kiadoNev;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Csoport_Id_FelelosFelhasznalo:
                                        {
                                            string felelosNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A feladat felelőse: " + felelosNev;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Letrehozo_Id:
                                        {
                                            string letrehozoNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A feljegyzés létrehozója: " + letrehozoNev;
                                        }
                                        break;
                                }

                                #endregion
                            }
                            break;
                        case Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek:
                            {
                                #region Iktatókönyveknél lehetséges oszloptípusok vizsgálata:
                                switch (errorDetail.ColumnType)
                                {
                                    case Constants.ErrorDetails.ColumnTypes.Ev:
                                        {
                                            columnMsg = "Érvényesség éve: " + errorDetail.ColumnValue;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.LezarasDatuma:
                                        {
                                            columnMsg = "Lezárás időpontja: " + errorDetail.ColumnValue;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Zarolo_id:
                                        {
                                            columnMsg = "Zároló: " + FelhasznaloNevek_Cache.GetFelhasznaloNevFromCache(errorDetail.ColumnValue, execParam, cache);
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.ErvVege:
                                        {
                                            columnMsg = "Érvényesség vége: " + errorDetail.ColumnValue;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.ErvKezd:
                                        {
                                            columnMsg = "Érvényesség kezdete: " + errorDetail.ColumnValue;
                                        }
                                        break;
                                }
                                #endregion
                            }
                            break;
                        case Constants.ErrorDetails.ObjectTypes.EREC_Szamlak:
                            {
                                #region Számlák esetén lehetséges oszloptípusok vizsgálata:
                                switch (errorDetail.ColumnType)
                                {
                                    case Constants.ErrorDetails.ColumnTypes.PIRAllapot:
                                        {
                                            string allapot = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                    "PIR_ALLAPOT", errorDetail.ColumnValue, execParam, cache);
                                            columnMsg = allapot;
                                        }
                                        break;
                                }
                                #endregion
                            }
                            break;
                        case Constants.ErrorDetails.ObjectTypes.EREC_IraKezbesitesiTetelek:
                            {
                                #region Kézbesítési tételeknél lehetséges oszloptípusok vizsgálata:
                                switch (errorDetail.ColumnType)
                                {
                                    case Constants.ErrorDetails.ColumnTypes.Allapot:
                                        {
                                            string allapot = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(
                                                KodTarak.KEZBESITESITETEL_ALLAPOT.KodcsoportKod, errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A kézbesítési tétel állapota: " + allapot;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Felhasznalo_Id_Atado_USER:
                                        {
                                            string csoportNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A kézbesítési tétel átadója: " + csoportNev;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Csoport_Id_Cel:
                                        {
                                            string csoportNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A kézbesítési tétel címzettje: " + csoportNev;
                                        }
                                        break;

                                }
                                #endregion
                            }
                            break;
                        case Constants.ErrorDetails.ObjectTypes.KRT_Mappak:
                            {
                                #region Dossziéknál lehetséges oszloptípusok vizsgálata:
                                switch (errorDetail.ColumnType)
                                {
                                    case Constants.ErrorDetails.ColumnTypes.Felhasznalo_Id_Atado_USER:
                                        {
                                            string csoportNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A dosszié átadója: " + csoportNev;
                                        }
                                        break;
                                    case Constants.ErrorDetails.ColumnTypes.Csoport_Id_Cel:
                                        {
                                            string csoportNev = CsoportNevek_Cache.GetCsoportNevFromCache(errorDetail.ColumnValue, execParam, cache);

                                            columnMsg = "A dosszié címzettje: " + csoportNev;
                                        }
                                        break;

                                }
                                #endregion
                            }
                            break;
                    }
                }

                if (!String.IsNullOrEmpty(columnMsg))
                    errorMessage += " " + columnMsg;

                if (kellZarojel)
                {
                    msg += "(" + errorMessage + ")";
                }
                else
                {
                    msg += errorMessage;
                }
                return msg;
            }
            else
            {
                return "";
            }
        }

        public static string GetErrorDetailInfoMsgSzur(ErrorDetails errorDetail, ExecParam execParam, System.Web.Caching.Cache cache)
        {
            return GetErrorDetailInfoMsgSzur(errorDetail, false, false, execParam, cache);
        }
        private static string GetErrorDetailInfoMsgSzur(ErrorDetails errorDetail, bool needNewLineChar, bool kellZarojel, ExecParam execParam, System.Web.Caching.Cache cache)
        {
            if (errorDetail == null || errorDetail.IsEmpty)
            {
                return string.Empty;
            }

            if (errorDetail.Code > 0 && !string.IsNullOrEmpty(errorDetail.Message))
            {
                return string.Format("[ {0} ] {1}", errorDetail.Message, GetErrorDetailInfoMsg(errorDetail, needNewLineChar, kellZarojel, execParam, cache));
            }
            else
            {
                return GetErrorDetailInfoMsg(errorDetail, needNewLineChar, kellZarojel, execParam, cache);
            }
        }

        /// <summary>
        /// window.open -es html kódrészlet a problémás objektum url-jére
        /// </summary>
        /// <param name="errorDetail"></param>
        /// <returns></returns>
        private static string GetHtmlLinkFromErrorDetail(ErrorDetails errorDetail)
        {
            if (errorDetail == null || errorDetail.IsEmpty) { return String.Empty; }

            string linkText = String.Empty;
            string url = String.Empty;
            string pageName = String.Empty;

            switch (errorDetail.ObjectType)
            {
                case Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok:
                    linkText = "Ugrás az ügyiratra";
                    pageName = "UgyUgyiratokForm.aspx";
                    break;
                case Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratdarabok:
                    linkText = "Ugrás az ügyiratdarabra";
                    pageName = "UgyUgyiratdarabokForm.aspx";
                    break;
                case Constants.ErrorDetails.ObjectTypes.EREC_IraIratok:
                    linkText = "Ugrás az iratra";
                    pageName = "IraIratokForm.aspx";
                    break;
                case Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok:
                    linkText = "Ugrás az iratpéldányra";
                    pageName = "PldIratPeldanyokForm.aspx";
                    break;
                case Constants.ErrorDetails.ObjectTypes.EREC_KuldKuldemenyek:
                    linkText = "Ugrás a küldeményre";
                    pageName = "KuldKuldemenyekForm.aspx";
                    break;
                default:
                    return String.Empty;
            }

            url = pageName + "?" + QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + errorDetail.ObjectId;

            return "<a href=\"javascript:void(0)\" style=\"text-decoration:underline\" onclick=\"window.open('" + url + "');\">" + linkText + "</a>";
        }

        public static void CheckOrg(string Org_Id)
        {
            Logger.Debug(String.Format("CheckOrg, Org_Id: {0}", Org_Id ?? "NULL"));

            if (String.IsNullOrEmpty(Org_Id))
            {
                Result result = new Result();
                //'Org' értéke nem meghatározható
                result.ErrorCode = result.ErrorMessage = "[50202]";
                throw new ResultException(result);
            }
        }
        /// <summary>
        /// ResetErrorPanel
        /// </summary>
        /// <param name="errorPanel"></param>
        public static void ResetErrorPanel(eErrorPanel errorPanel)
        {
            errorPanel.Header = string.Empty;
            errorPanel.Body = string.Empty;
            errorPanel.Enabled = true;
            errorPanel.Visible = false;
            if (errorPanel.Parent.Parent.GetType() == typeof(UpdatePanel))
            {
                UpdatePanel up = (UpdatePanel)errorPanel.Parent.Parent;
                up.Update();
            }
        }
    }

}
