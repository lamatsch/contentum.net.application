using Contentum.eBusinessDocuments;
using System;

namespace Contentum.eUtility
{
    public class ResultException:ApplicationException
    {

        private Result result;

        public string ErrorCode
        {
            get { return result.ErrorCode; }
        }

        public string ErrorMessage
        {
            get { return result.ErrorMessage; }
        }

        public string ErrorType
        {
            get { return result.ErrorType; }
        }
		
        public ResultException(Result result)
        {
            this.result = result;
        }
        public ResultException(Result result, ErrorDetails errorDetail)
        {
            this.result = result;
            this.result.ErrorDetail = errorDetail;
        }
        public ResultException(string ErrorCode, string ErrorMessage):base(ErrorMessage)
        {
            Result result = new Result();
            result.ErrorCode = ErrorCode;
            result.ErrorMessage = ErrorMessage;

            this.result = result;
        }

        public ResultException(string ErrorMessage):base(ErrorMessage)
        {
            Result result = new Result();
            result.ErrorCode = ErrorMessage;
            result.ErrorMessage = ErrorMessage;

            this.result = result;
        }

        public ResultException(string ErrorMessage, string ErrorCode, string ErrorType):base(ErrorMessage)
        {
            Result result = new Result();
            result.ErrorCode = ErrorCode;
            result.ErrorMessage = ErrorMessage;
            result.ErrorType = ErrorType;

            this.result = result;
        }

        public ResultException(int ErrorCode)
        {
            Result result = ResultError.CreateNewResultWithErrorCode(ErrorCode);
            this.result = result;
        }

        public ResultException(int ErrorCode, ErrorDetails errorDetail)
        {
            Result result = ResultError.CreateNewResultWithErrorCode(ErrorCode, errorDetail);
            this.result = result;
        }

        public Result GetResult()
        {
            return this.result;
        }

        #region statikus met�dusok

        public const string defaultErrorCode = "[66666]";

        public static string GetMessageFromException(Exception e)
        {
            string message = string.Empty;

            while (e != null)
            {
                message += e.Message;

                if (e.InnerException != null)
                    message += ";";

                e = e.InnerException;
            }

            return message;
        }

        public static Result GetResultFromException(Exception e, string ErrorCode)
        {
            if (e is ResultException)
            {
                return ((ResultException)e).GetResult();
            }
            else
            {
                Result res = new Result();
                res.ErrorCode = ErrorCode;
                if (ErrorCode == defaultErrorCode)
                    res.ErrorMessage = GetMessageFromException(e);
                else
                    res.ErrorMessage = ErrorCode + GetMessageFromException(e);
                return res;

            }
        }

        public static Result GetResultFromException(Exception e)
        {
            return GetResultFromException(e, defaultErrorCode);
        }

        #endregion
    }

    public static class ResultExtension
    {
        /// <summary>
        /// Result ellen�rz�se, �s ResultException dob�sa hiba eset�n
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static void CheckError(this Result result)
        {
            if (result.IsError)
            {
                throw new ResultException(result);
            }
        }

        public static void CheckError(this Result result, Action errorAction)
        {
            if (result.IsError)
            {
                if (errorAction != null)
                {
                    errorAction.Invoke();
                }
                throw new ResultException(result);
            }
        }
    }
}
