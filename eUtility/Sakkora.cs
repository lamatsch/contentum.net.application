﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Contentum.eUtility
{
    /// <summary>
    /// Sakkóra utility
    /// </summary>
    public class Sakkora
    {
        #region CONSTANTS

        public enum EnumIratHatasaUgyintezesre
        {
            NOTHING,
            START,
            PAUSE,
            STOP,
            ON_PROGRESS,
            NONE
        }

        public enum NormalVagyAlszamraIktatas { NULL, NormalIktatas, AlszamraIktatas }

        public static readonly string CONST_ELTELT_IDO_DEFAULT = CONST_ELTELT_IDO_DEFAULT_INT.ToString();
        public static readonly int CONST_ELTELT_IDO_DEFAULT_INT = 0;

        #endregion CONSTANTS

        /// <summary>
        /// Sakkora Modosithato
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name=""></param>
        /// <param name="irat"></param>
        /// <param name="ugyirat"></param>
        /// <returns></returns>
        public static bool SakkoraModosithato(ExecParam execParam, EREC_IraIratok irat, EREC_UgyUgyiratok ugyirat)
        {
            if (ugyirat == null || irat == null)
            {
                return true;
            }

            if (IsUgyiratLezart(ugyirat) || ugyirat.ElteltidoAllapot == KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.STOP)
            {
                return false;
            }

            return irat.Allapot != KodTarak.IRAT_ALLAPOT.Kiadmanyozott;
        }

        /// <summary>
        /// Eltelt ido leállítása postázás során
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="iratId"></param>
        public static void StopElteltIdoPostazasSoran(ExecParam execParam, string iratId)
        {
            try
            {
                if (string.IsNullOrEmpty(iratId))
                {
                    return;
                }

                EREC_IraIratok irat = GetIrat(execParam, iratId);
                if (irat == null)
                {
                    return;
                }

                EREC_UgyUgyiratok ugy = GetUgyirat(execParam, irat.Ugyirat_Id);
                if (ugy == null)
                {
                    return;
                }

                ugy.ElteltidoAllapot = KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.STOP;
                ugy.Updated.ElteltidoAllapot = true;

                UpdateUgyirat(execParam, ugy);
            }
            catch (Exception exc)
            {
                Logger.Error("sakkora.StopElteltIdoPostazasSoran ", exc);
            }
        }

        public static void StopElteltIdoPostazasSoranByIratPlds(ExecParam execParam, List<string> iratPldIds)
        {
            try
            {
                if (iratPldIds == null || iratPldIds.Count < 1)
                {
                    return;
                }

                Result result = GetIraPeldanyok(execParam, iratPldIds.ToArray());
                if (result == null || result.Ds.Tables[0].Rows.Count < 1)
                {
                    return;
                }

                Dictionary<string, string> iratIds = new Dictionary<string, string>();
                foreach (DataRow item in result.Ds.Tables[0].Rows)
                {
                    if (!iratIds.ContainsKey(item["IraIrat_Id"].ToString()))
                    {
                        iratIds.Add(item["IraIrat_Id"].ToString(), item["IraIrat_Id"].ToString());
                    }
                }

                if (iratIds.Count < 1)
                {
                    return;
                }

                foreach (KeyValuePair<string, string> iratId in iratIds)
                {
                    StopElteltIdoPostazasSoran(execParam, iratId.Key);
                }
            }
            catch (Exception exc)
            {
                Logger.Error("sakkora.StopElteltIdoPostazasSoranByIratPld ", exc);
            }
        }

        /// <summary>
        /// Sakkora kezelése
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="irat"></param>
        /// param name="ugy"></param> // BUG_3435
        public static void SakkoraKezeles(ExecParam execParam, EREC_IraIratok irat, EREC_UgyUgyiratok ugy, out bool kellElteltIdoUjraszamolas)
        {
            kellElteltIdoUjraszamolas = false;
            if (string.IsNullOrEmpty(irat.IratHatasaUgyintezesre) || ugy == null)
            {
                return;
            }

            if (IsUgySakkoraAllapotaModosithato(irat, ugy))
            {
                #region UGY SAKKORA ALLAPOT BEALLITASA

                SetUgySakkoraAllapotaFromIrat(irat.IratHatasaUgyintezesre, ref ugy);
                execParam.Record_Id = ugy.Id;
                UpdateUgyirat(execParam, ugy);

                #endregion UGY SAKKORA ALLAPOT BEALLITASA
            }

            if (ugy.ElteltidoAllapot == KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.STOP)
            {
                return;
            }

            if (string.IsNullOrEmpty(ugy.ElteltidoAllapot) || ugy.ElteltidoAllapot == KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.NEW)
            {
                switch (irat.IratHatasaUgyintezesre)
                {
                    case KodTarak.IRAT_HATASA_UGYINTEZESRE.A_hatosagi_ugyintezesi_idore_nincs_hatassal_NONE:
                    case KodTarak.IRAT_HATASA_UGYINTEZESRE.A_hatosagi_ugyintezesi_idore_nincs_hatassal_ON_PROGRESS:
                    case KodTarak.IRAT_HATASA_UGYINTEZESRE.A_hatosagi_ugyintezesi_idore_nincs_hatassal_PAUSE:
                    case KodTarak.IRAT_HATASA_UGYINTEZESRE.A_hatosagi_ugyintezesi_idore_nincs_hatassal_STOP:
                    case KodTarak.IRAT_HATASA_UGYINTEZESRE.A_hatosagi_ugyintezesi_ido_indul_folytatodik:
                    case KodTarak.IRAT_HATASA_UGYINTEZESRE.Az_elsofok_fellebbezesi_kerelem_alapjan_modosít_visszavon:
                    case KodTarak.IRAT_HATASA_UGYINTEZESRE.A_hatosagi_ugyintezesi_ido_indul_ujraindul_es_megall:
                    case KodTarak.IRAT_HATASA_UGYINTEZESRE.Hatosagi_ugyintezesi_ido_indul_ujraindul_es_vege:
                    case KodTarak.IRAT_HATASA_UGYINTEZESRE.Masodfoku_hatosagi_ugyintezesi_ido_indul_folytatodik:
                    case KodTarak.IRAT_HATASA_UGYINTEZESRE.Masodfoku_hatosagi_ugyintezesi_ido_indul_ujraindul_es_megall:
                    case KodTarak.IRAT_HATASA_UGYINTEZESRE.Masodfoku_hatosagi_ugyintezesi_ido_indul_ujraindul_es_vege:

                        ugy = GetUgyirat(execParam, irat.Ugyirat_Id);

                        ugy.ElteltIdo = CONST_ELTELT_IDO_DEFAULT;
                        ugy.Updated.ElteltIdo = true;

                        string idoEgyseg = KodTarak.IDOEGYSEG.Munkanap;
                        try
                        {
                            idoEgyseg = GetUgyiratIdoEgyseg(execParam, irat.Ugyirat_Id);
                        }
                        catch (Exception exc) { Logger.Error("SakkoraKezeles", exc); }
                        ugy.ElteltIdoIdoEgyseg = idoEgyseg;
                        ugy.Updated.ElteltIdoIdoEgyseg = true;

                        ugy.ElteltidoAllapot = KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.START;
                        ugy.Updated.ElteltidoAllapot = true;

                        ugy.ElteltIdoUtolsoModositas = Sakkora.GetTodayLastMinuteAsString();
                        ugy.Updated.ElteltIdoUtolsoModositas = true;

                        UpdateUgyirat(execParam, ugy);
                        kellElteltIdoUjraszamolas = true;
                        break;

                    default:
                        break;
                }
            }
        }

        #region IRAT HATAS

        /// <summary>
        /// Irat hatása állapotátmenetek
        /// </summary>
        /// <param name="kodtarKod"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        private static void GetIratHatasaStates(ExecParam execparam, string kodtarKod, out List<EnumIratHatasaUgyintezesre> from, out EnumIratHatasaUgyintezesre to)
        {
            from = new List<EnumIratHatasaUgyintezesre>();
            to = new EnumIratHatasaUgyintezesre();
            if (string.IsNullOrEmpty(kodtarKod))
            {
                return;
            }
            KRT_KodTarak iratHatasKodTar = GetIratHatasaItem(execparam, kodtarKod);
            if (iratHatasKodTar == null || string.IsNullOrEmpty(iratHatasKodTar.Egyeb))
            {
                return;
            }
            ExtractIratHatasStates(iratHatasKodTar.Egyeb, ref from, ref to);
        }

        /// <summary>
        /// Irat hatása felfüggesztés?
        /// </summary>
        /// <returns></returns>
        public static bool IsIratHatasaPause(ExecParam execparam, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }
            List<EnumIratHatasaUgyintezesre> from;
            EnumIratHatasaUgyintezesre to;
            GetIratHatasaStates(execparam, value, out from, out to);

            if (to == EnumIratHatasaUgyintezesre.PAUSE)
            {
                return true;
            }
            else if (to == EnumIratHatasaUgyintezesre.NONE && value == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Irat hatása felfüggesztés?
        /// </summary>
        /// <param name="execparam"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsIratHatasaPauseStrict(ExecParam execparam, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }
            List<EnumIratHatasaUgyintezesre> from = new List<EnumIratHatasaUgyintezesre>();
            EnumIratHatasaUgyintezesre to = new EnumIratHatasaUgyintezesre();
            GetIratHatasaStates(execparam, value, out from, out to);

            if (to == EnumIratHatasaUgyintezesre.PAUSE)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Irat hatása indítás?
        /// </summary>
        /// <returns></returns>
        public static bool IsIratHatasaStart(ExecParam execparam, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            List<EnumIratHatasaUgyintezesre> from;
            EnumIratHatasaUgyintezesre to;
            GetIratHatasaStates(execparam, value, out from, out to);

            if (to == EnumIratHatasaUgyintezesre.START || to == EnumIratHatasaUgyintezesre.ON_PROGRESS)
            {
                return true;
            }
            else if (to == EnumIratHatasaUgyintezesre.NONE && value == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// IsIratHatasaStartStrict
        /// </summary>
        /// <param name="execparam"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsIratHatasaStartStrict(ExecParam execparam, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            List<EnumIratHatasaUgyintezesre> from;
            EnumIratHatasaUgyintezesre to;
            GetIratHatasaStates(execparam, value, out from, out to);

            if (to == EnumIratHatasaUgyintezesre.START || to == EnumIratHatasaUgyintezesre.ON_PROGRESS)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Irat hatása lezárt?
        /// </summary>
        /// <returns></returns>
        public static bool IsIratHatasaStop(ExecParam execparam, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }
            List<EnumIratHatasaUgyintezesre> from;
            EnumIratHatasaUgyintezesre to;
            GetIratHatasaStates(execparam, value, out from, out to);

            if (to == EnumIratHatasaUgyintezesre.STOP)
            {
                return true;
            }
            else if (to == EnumIratHatasaUgyintezesre.NONE && value == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// IsIratHatasaStopStrict
        /// </summary>
        /// <param name="execparam"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsIratHatasaStopStrict(ExecParam execparam, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }
            List<EnumIratHatasaUgyintezesre> from;
            EnumIratHatasaUgyintezesre to;
            GetIratHatasaStates(execparam, value, out from, out to);

            return to == EnumIratHatasaUgyintezesre.STOP;
        }

        /// <summary>
        /// Visszaadja a lehetséges sakkóra állapotokat
        /// </summary>
        /// <param name="execparam"></param>
        /// <param name="ugy"></param>
        /// <returns></returns>
        public static List<string> SakkoraAllapotok(ExecParam execparam, EREC_UgyUgyiratok ugy, string eljarasiSzakaszKod, bool onMagatIs, out Sakkora.EnumIratHatasaUgyintezesre actualType)
        {
            if (ugy == null)
            {
                return SakkoraLathatoAllapotok(execparam, string.Empty, string.Empty, eljarasiSzakaszKod, out actualType, onMagatIs);
            }
            else
            {
                return SakkoraLathatoAllapotok(execparam, ugy.SakkoraAllapot, ugy.Ugy_Fajtaja, eljarasiSzakaszKod, out actualType, onMagatIs);
            }
        }

        /// <summary>
        /// SakkoraLathatoAllapotok
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="jelenlegiSakkoraAllapota"></param>
        /// <param name="ugyFajta"></param>
        /// <param name="eljarasiSzakaszFok"></param>
        /// <param name="actualType"></param>
        /// <param name="onMagatIs"></param>
        /// <returns></returns>
        public static List<string> SakkoraLathatoAllapotok(ExecParam execParam, string jelenlegiSakkoraAllapota, string ugyFajta, string eljarasiSzakaszFok, out Sakkora.EnumIratHatasaUgyintezesre actualType, bool onMagatIs)
        {
            actualType = EnumIratHatasaUgyintezesre.NOTHING;

            #region VARIABLES

            List<string> toStatesResult = new List<string>();
            SakkoraModel model = null;
            KRT_KodTarak kodtar = null;

            #endregion VARIABLES

            KodTarak.SakkoraUgyFajtaTipus ugyFajtaTipus = GetSakkoraUgyFajtaTipus(execParam, ugyFajta, eljarasiSzakaszFok);
            if (string.IsNullOrEmpty(jelenlegiSakkoraAllapota) || (jelenlegiSakkoraAllapota == "0"))
            {
                kodtar = GetIratHatasaItem(execParam, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE);
            }
            else
            {
                kodtar = GetIratHatasaItem(execParam, jelenlegiSakkoraAllapota);
            }

            if (kodtar == null)
            {
                return toStatesResult;
            }

            model = JSONUtilityFunctions.DeSerializeSakkoraModelObj(kodtar.Egyeb);
            if (model == null)
            {
                return toStatesResult;
            }

            try
            {
                actualType = (EnumIratHatasaUgyintezesre)Enum.Parse(typeof(EnumIratHatasaUgyintezesre), model.tipusok[0]);
            }
            catch (Exception exc) { Logger.Error("SakkoraLathatoAllapotok", exc); }

            SakkoraAtmenetUgyFajtaModel atmenet = model.GetAtmenetByUgyFajta(ugyFajtaTipus.ToString());
            if (atmenet.atmenetek.Count < 1)
            {
                return toStatesResult;
            }

            toStatesResult.AddRange(atmenet.atmenetek);

            if (onMagatIs && !toStatesResult.Contains(jelenlegiSakkoraAllapota))
            {
                toStatesResult.Add(jelenlegiSakkoraAllapota);
            }

            return toStatesResult;
        }

        /// <summary>
        /// Az irat hatása állapotátmeneteket kiveszi a mezõbõl
        /// </summary>
        /// <param name="iratHatasKodTar"></param>
        /// <param name="from"></param>
        /// <param name="actualtype"></param>
        /// <returns></returns>
        public static Sakkora.EnumIratHatasaUgyintezesre ExtractIratHatasStates(string iratHatasKodTar, ref List<Sakkora.EnumIratHatasaUgyintezesre> from, ref Sakkora.EnumIratHatasaUgyintezesre actualtype)
        {
            from = null;

            if (string.IsNullOrEmpty(iratHatasKodTar))
            {
                return EnumIratHatasaUgyintezesre.NOTHING;
            }

            SakkoraModel model = JSONUtilityFunctions.DeSerializeSakkoraModelObj(iratHatasKodTar);
            if (model == null || model.tipusok.Count < 1)
            {
                return EnumIratHatasaUgyintezesre.NOTHING;
            }

            try
            {
                actualtype = (Sakkora.EnumIratHatasaUgyintezesre)Enum.Parse(typeof(Sakkora.EnumIratHatasaUgyintezesre), model.tipusok[0]);
            }
            catch (Exception)
            {
                return EnumIratHatasaUgyintezesre.NOTHING;
            }
            return actualtype;
        }

        #endregion IRAT HATAS

        /// <summary>
        /// IRAT_HATASA_UGYINTEZESRE
        /// </summary>
        public static string IRAT_HATASA_UGYINTEZESRE
        {
            get { return KodTarak.KodCsoportokKodok.IRAT_HATASA_UGYINTEZESRE; }
        }

        /// <summary>
        /// UGYIRAT_FELFUGGESZTES_OKA
        /// </summary>
        public static string UGYIRAT_FELFUGGESZTES_OKA
        {
            get { return KodTarak.KodCsoportokKodok.UGYIRAT_FELFUGGESZTES_OKA; }
        }

        #region UGYIRAT ALLAPOT

        /// <summary>
        /// Az ügyirat ügyintézés alatt állapotú?
        /// </summary>
        /// <returns></returns>
        public static bool IsUgyiratUgyintezesAlatt(EREC_UgyUgyiratok ugyirat)
        {
            return (ugyirat != null) && ugyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
        }

        /// <summary>
        /// Az ügyirat felfüggesztett állapotú?
        /// </summary>
        /// <returns></returns>
        public static bool IsUgyiratFelfuggesztett(EREC_UgyUgyiratok ugyirat)
        {
            return (ugyirat != null) && ugyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Felfuggesztett;
        }

        /// <summary>
        /// Az ügyirat lezárt állapotú?
        /// </summary>
        /// <returns></returns>
        public static bool IsUgyiratLezart(EREC_UgyUgyiratok ugyirat)
        {
            return (ugyirat != null) && ugyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo;
        }

        /// <summary>
        /// IsUgyiratIktatott
        /// </summary>
        /// <param name="ugyirat"></param>
        /// <returns></returns>
        public static bool IsUgyiratIktatott(EREC_UgyUgyiratok ugyirat)
        {
            return (ugyirat != null) && ugyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Iktatott;
        }

        #endregion UGYIRAT ALLAPOT

        #region HELPER

        /// <summary>
        /// Ugyirat idoegysege
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ugyiratId"></param>
        private static string GetUgyiratIdoEgyseg(ExecParam execParam, string ugyiratId)
        {
            EREC_UgyUgyiratok ugyiratObj = GetUgyirat(execParam.Clone(), ugyiratId);

            string idoEgyseg = null;

            if (!string.IsNullOrEmpty(ugyiratObj.IratMetadefinicio_Id))
            {
                EREC_IratMetaDefinicioService serviceIratMetaDef = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();

                ExecParam execParamGetIratMetaDef = execParam.Clone();
                execParamGetIratMetaDef.Record_Id = ugyiratObj.IratMetadefinicio_Id;

                Result resultIratMetaDef = serviceIratMetaDef.Get(execParamGetIratMetaDef);
                if (resultIratMetaDef.IsError)
                {
                    throw new ResultException(resultIratMetaDef);
                }

                EREC_IratMetaDefinicio iratMetaDef = (EREC_IratMetaDefinicio)resultIratMetaDef.Record;
                idoEgyseg = iratMetaDef.Idoegyseg;
            }
            if (string.IsNullOrEmpty(idoEgyseg))
            {
                idoEgyseg = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.DEFAULT_INTEZESI_IDO_IDOEGYSEG);
            }
            return idoEgyseg;
        }

        /// <summary>
        /// GetSakkoraUgyFajtaTipus
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ugyFajta"></param>
        /// <param name="eljarasiSzakaszFok"></param>
        /// <returns></returns>
        public static KodTarak.SakkoraUgyFajtaTipus GetSakkoraUgyFajtaTipus(ExecParam execParam, string ugyFajta, string eljarasiSzakaszFok)
        {
            if (IsUgyFajtaHatosagi(execParam, ugyFajta))
            {
                if (eljarasiSzakaszFok.Equals(KodTarak.ELJARASI_SZAKASZ_FOK.I))
                {
                    return KodTarak.SakkoraUgyFajtaTipus.H1;
                }
                else if (eljarasiSzakaszFok.Equals(KodTarak.ELJARASI_SZAKASZ_FOK.II))
                {
                    return KodTarak.SakkoraUgyFajtaTipus.H2;
                }
                else
                {
                    return KodTarak.SakkoraUgyFajtaTipus.H1;
                }
            }
            else
            {
                return KodTarak.SakkoraUgyFajtaTipus.NH;
            }
        }

        /// <summary>
        /// Hatosagi ugy ?
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ugyFajta"></param>
        /// <returns></returns>
        public static bool IsUgyFajtaHatosagi(ExecParam execParam, string ugyFajta)
        {
            if (string.IsNullOrEmpty(ugyFajta))
            {
                return false;
            }

            #region KCS

            KRT_KodCsoportokService svcKcs = eAdminService.ServiceFactory.GetKRT_KodCsoportokService();
            KRT_KodCsoportokSearch srcKcs = new KRT_KodCsoportokSearch();
            srcKcs.Kod.Value = KodTarak.UGY_JELLEG_FAJTAJA_HATOSAGI.KCS_UGY_FAJTAJA;
            srcKcs.Kod.Operator = Query.Operators.equals;
            Result resultKcs = svcKcs.GetAll(execParam.Clone(), srcKcs);
            if (!string.IsNullOrEmpty(resultKcs.ErrorCode) || resultKcs.Ds.Tables[0].Rows.Count < 1)
            {
                return false;
            }

            string kodcsoportKod = resultKcs.Ds.Tables[0].Rows[0]["Kod"].ToString();

            #endregion KCS

            #region KT

            KRT_KodTarak ktUgyFajta = GetKodtarFromCache(execParam, kodcsoportKod, ugyFajta);

            #endregion KT

            if (ktUgyFajta == null)
            {
                return false;
            }

            string egyebMezo = ktUgyFajta.Egyeb;
            return egyebMezo.Equals(KodTarak.UGY_JELLEG_FAJTAJA_HATOSAGI.HATOSAGI);
        }

        /// <summary>
        /// SakkoraLezarasOkai
        /// </summary>
        /// <param name="execparam"></param>
        /// <param name="jelenlegiSakkoraAllapota"></param>
        /// <returns></returns>
        public static string[] SakkoraLezarasOkai(ExecParam execparam, string jelenlegiSakkoraAllapota)
        {
            if (string.IsNullOrEmpty(jelenlegiSakkoraAllapota))
            {
                return new string[] { };
            }

            KRT_KodTarak kodtar = GetIratHatasaItem(execparam, jelenlegiSakkoraAllapota);
            if (kodtar == null)
            {
                return new string[] { };
            }

            SakkoraModel model = JSONUtilityFunctions.DeSerializeSakkoraModelObj(kodtar.Egyeb);
            return (model == null || model.lezarasOkai.Count < 1) ? null : model.lezarasOkai.ToArray();
        }

        /// <summary>
        /// Sakkora Felfuggesztes Okai
        /// </summary>
        /// <param name="execparam"></param>
        /// <param name="jelenlegiSakkoraAllapota"></param>
        /// <returns></returns>
        public static string[] SakkoraFelfuggesztesOkai(ExecParam execparam, string jelenlegiSakkoraAllapota)
        {
            if (string.IsNullOrEmpty(jelenlegiSakkoraAllapota))
            {
                return new string[] { };
            }

            KRT_KodTarak kodtar = GetIratHatasaItem(execparam, jelenlegiSakkoraAllapota);
            if (kodtar == null)
            {
                return new string[] { };
            }

            SakkoraModel model = JSONUtilityFunctions.DeSerializeSakkoraModelObj(kodtar.Egyeb);
            return (model == null || model.felfuggesztesOkai.Count < 1) ? null : model.felfuggesztesOkai.ToArray();
        }

        /// <summary>
        /// SakkoraFelfuggesztesOkaiKodtarak
        /// </summary>
        /// <param name="execparam"></param>
        /// <param name="jelenlegiSakkoraAllapota"></param>
        /// <returns></returns>
        public static List<KodTar_Cache.KodTarElem> SakkoraFelfuggesztesOkaiKodtarak(ExecParam execparam, string jelenlegiSakkoraAllapota)
        {
            List<KodTar_Cache.KodTarElem> result = new List<KodTar_Cache.KodTarElem>();
            if (string.IsNullOrEmpty(jelenlegiSakkoraAllapota))
            {
                return new List<KodTar_Cache.KodTarElem>();
            }

            KRT_KodTarak kodtar = GetIratHatasaItem(execparam, jelenlegiSakkoraAllapota);
            if (kodtar == null)
            {
                return new List<KodTar_Cache.KodTarElem>();
            }

            SakkoraModel model = JSONUtilityFunctions.DeSerializeSakkoraModelObj(kodtar.Egyeb);
            if (model == null || model.felfuggesztesOkai.Count < 1)
            {
                return new List<KodTar_Cache.KodTarElem>();
            }

            List<KodTar_Cache.KodTarElem> felfuggesztesOkok = KodTar_Cache.GetKodtarakByKodCsoportList(KodTarak.UGYIRAT_FELFUGGESZTES_OKA.KCS, execparam, System.Web.HttpContext.Current.Cache, null);
            if (felfuggesztesOkok == null || felfuggesztesOkok.Count < 1)
            {
                return new List<KodTar_Cache.KodTarElem>();
            }

            KodTar_Cache.KodTarElem tmp;
            foreach (string item in model.felfuggesztesOkai)
            {
                tmp = felfuggesztesOkok.FirstOrDefault(f => f.Kod.Equals(item));
                if (tmp != null)
                {
                    result.Add(tmp);
                }
            }

            return result;
        }

        /// <summary>
        /// SakkoraLezarasOkaiKodtarak
        /// </summary>
        /// <param name="execparam"></param>
        /// <param name="jelenlegiSakkoraAllapota"></param>
        /// <returns></returns>
        public static List<KodTar_Cache.KodTarElem> SakkoraLezarasOkaiKodtarak(ExecParam execparam, string jelenlegiSakkoraAllapota)
        {
            List<KodTar_Cache.KodTarElem> result = new List<KodTar_Cache.KodTarElem>();
            if (string.IsNullOrEmpty(jelenlegiSakkoraAllapota))
            {
                return new List<KodTar_Cache.KodTarElem>();
            }

            KRT_KodTarak kodtar = GetIratHatasaItem(execparam, jelenlegiSakkoraAllapota);
            if (kodtar == null)
            {
                return new List<KodTar_Cache.KodTarElem>();
            }

            SakkoraModel model = JSONUtilityFunctions.DeSerializeSakkoraModelObj(kodtar.Egyeb);
            if (model == null || model.lezarasOkai.Count < 1)
            {
                return new List<KodTar_Cache.KodTarElem>();
            }

            List<KodTar_Cache.KodTarElem> lezarasOkok = KodTar_Cache.GetKodtarakByKodCsoportList(KodTarak.UGYIRAT_LEZARAS_OKA.KCS, execparam, System.Web.HttpContext.Current.Cache, null);
            if (lezarasOkok == null || lezarasOkok.Count < 1)
            {
                return new List<KodTar_Cache.KodTarElem>();
            }

            KodTar_Cache.KodTarElem tmp;
            foreach (string item in model.lezarasOkai)
            {
                tmp = lezarasOkok.FirstOrDefault(f => f.Kod.Equals(item));
                if (tmp != null)
                {
                    result.Add(tmp);
                }
            }

            return result;
        }

        /// <summary>
        /// CheckLehetsegesFelfuggesztesOk
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="filteredFelfuggesztesOkok"></param>
        /// <param name="ok"></param>
        /// <returns></returns>
        public static bool CheckLehetsegesFelfuggesztesOk(ExecParam execParam, List<KodTar_Cache.KodTarElem> filteredFelfuggesztesOkok, string ok)
        {
            if (filteredFelfuggesztesOkok.Any(a => a.Kod.Equals(ok, StringComparison.CurrentCultureIgnoreCase)))
            {
                return true;
            }
            if (filteredFelfuggesztesOkok.Any(a => a.Nev.Equals(ok, StringComparison.CurrentCultureIgnoreCase)))
            {
                return true;
            }

            throw new Exception(string.Format("A megadott '{0}' érték nem adható meg a megadott Sakkóra státusznál!", ok));
        }

        /// <summary>
        /// CheckLehetsegesLezarasOk
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="filteredLezarasOkok"></param>
        /// <param name="ok"></param>
        /// <returns></returns>
        public static bool CheckLehetsegesLezarasOk(ExecParam execParam, List<KodTar_Cache.KodTarElem> filteredLezarasOkok, string ok)
        {
            if (filteredLezarasOkok.Any(a => a.Kod.Equals(ok, StringComparison.CurrentCultureIgnoreCase)))
            {
                return true;
            }
            if (filteredLezarasOkok.Any(a => a.Nev.Equals(ok, StringComparison.CurrentCultureIgnoreCase)))
            {
                return true;
            }

            throw new Exception(string.Format("A megadott '{0}' érték nem adható meg a megadott Sakkóra státusznál!", ok));
        }

        #endregion HELPER

        #region SERVICE

        /// <summary>
        /// Get ugyirat
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ugyiratId"></param>
        /// <returns></returns>
        public static EREC_UgyUgyiratok GetUgyirat(ExecParam execParam, string ugyiratId)
        {
            #region GET UGY

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            execParam.Record_Id = ugyiratId;
            Result resultGet = service.Get(execParam);

            if (!string.IsNullOrEmpty(resultGet.ErrorCode))
            { return null; }

            EREC_UgyUgyiratok ugy = (EREC_UgyUgyiratok)resultGet.Record;
            ugy.Updated.SetValueAll(false);
            ugy.Base.Updated.SetValueAll(false);
            ugy.Base.Updated.Ver = true;

            return ugy;

            #endregion GET UGY
        }

        /// <summary>
        /// GetIrat
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static EREC_IraIratok GetIrat(ExecParam execParam, string id)
        {
            #region GET IRAT

            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();

            execParam.Record_Id = id;
            Result resultGet = service.Get(execParam);

            if (!string.IsNullOrEmpty(resultGet.ErrorCode))
            { return null; }

            EREC_IraIratok irat = (EREC_IraIratok)resultGet.Record;
            irat.Updated.SetValueAll(false);
            irat.Base.Updated.SetValueAll(false);
            irat.Base.Updated.Ver = true;

            return irat;

            #endregion GET IRAT
        }

        /// <summary>
        /// UpdateUgyirat
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ugyirat"></param>
        /// <returns></returns>
        private static bool UpdateUgyirat(ExecParam execParam, EREC_UgyUgyiratok ugyirat)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            execParam.Record_Id = ugyirat.Id;
            Result result = service.Update(execParam, ugyirat);

            return string.IsNullOrEmpty(result.ErrorCode);
        }

        /// <summary>
        /// SetUgyiratAllapot
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ugyiratId"></param>
        /// <param name="allapot"></param>
        /// <returns></returns>
        private static bool SetUgyiratAllapot(ExecParam execParam, string ugyiratId, string allapot)
        {
            EREC_UgyUgyiratok ugy = GetUgyirat(execParam.Clone(), ugyiratId);
            if (ugy == null)
            {
                return false;
            }

            ugy.Allapot = allapot;
            ugy.Updated.Allapot = true;

            return UpdateUgyirat(execParam.Clone(), ugy);
        }

        /// <summary>
        /// Visszaadja a kódtár elemet kód alapján
        /// </summary>
        /// <param name="kodtariKod"></param>
        /// <returns></returns>
        public static KRT_KodTarak GetIratHatasaItem(ExecParam execParam, string kodtariKod)
        {
            KRT_KodTarak kt = null;
            try
            {
                kt = GetKodtarFromCache(execParam, KodTarak.IRAT_HATASA_UGYINTEZESRE.KCS, kodtariKod);
            }
            catch (Exception exc)
            {
                Logger.Debug("GetIratHatasaItem", exc);
            }
            return kt;
        }

        /// <summary>
        /// GetIraPeldanyok
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="iratPldIds"></param>
        /// <returns></returns>
        private static Result GetIraPeldanyok(ExecParam execParam, string[] iratPldIds)
        {
            if (iratPldIds == null || iratPldIds.Length < 1)
            {
                return null;
            }

            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            EREC_PldIratPeldanyokSearch src = new EREC_PldIratPeldanyokSearch();
            src.Id.Value = Search.GetSqlInnerString(iratPldIds);
            src.Id.Operator = Query.Operators.inner;
            Result result = service.GetAll(execParam, src);

            return !string.IsNullOrEmpty(result.ErrorCode) ? null : result;
        }

        /// <summary>
        /// Ügy sakkora állapota beállítható ?
        /// </summary>
        /// <param name="irat"></param>
        /// <param name="ugy"></param>
        /// <returns></returns>
        public static bool IsUgySakkoraAllapotaModosithato(EREC_IraIratok irat, EREC_UgyUgyiratok ugy)
        {
            return ugy == null || irat == null || IsUgySakkoraAllapotaModosithato(ugy.SakkoraAllapot, irat.IratHatasaUgyintezesre);
        }

        /// <summary>
        /// IsUgySakkoraAllapotaModosithato
        /// </summary>
        /// <param name="ugyiratSakkora"></param>
        /// <param name="iratSakkora"></param>
        /// <returns></returns>
        public static bool IsUgySakkoraAllapotaModosithato(string ugyiratSakkora, string iratSakkora)
        {
            return string.IsNullOrEmpty(ugyiratSakkora) || string.IsNullOrEmpty(iratSakkora)
                ? true
                :
                iratSakkora != KodTarak.IRAT_HATASA_UGYINTEZESRE.A_hatosagi_ugyintezesi_idore_nincs_hatassal_NONE
                &&
                iratSakkora != KodTarak.IRAT_HATASA_UGYINTEZESRE.A_hatosagi_ugyintezesi_idore_nincs_hatassal_PAUSE
                &&
                iratSakkora != KodTarak.IRAT_HATASA_UGYINTEZESRE.A_hatosagi_ugyintezesi_idore_nincs_hatassal_ON_PROGRESS
                &&
                iratSakkora != KodTarak.IRAT_HATASA_UGYINTEZESRE.A_hatosagi_ugyintezesi_idore_nincs_hatassal_STOP;
        }

        #endregion SERVICE

        #region DESIGN

        /// <summary>
        /// GetIratHatasaTipusIkon
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetIratHatasaTipusIkon(object type)
        {
            if (type.ToString() == "START" || type.ToString() == "ON_PROGRESS" || type.ToString() == "0_START")
            {
                return "&#9654;";
            }
            else if (type.ToString() == IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS.ToString() || type.ToString() == IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP.ToString() || type.ToString() == IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE.ToString() || type.ToString() == IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString())
            {
                return "&#9673;";
            }
            else if (type.ToString() == "STOP")
            {
                return "&#9632;";
            }
            else if (type.ToString() == "PAUSE")
            {
                return "&#9167;";
            }
            else if (type.ToString() == "NONE")
            {
                return "&#9673;";
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// GetIratHatasaTipusColor
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static System.Drawing.Color GetIratHatasaTipusColor(object type)
        {
            if (type.ToString() == "START" || type.ToString() == "ON_PROGRESS")
            {
                return System.Drawing.Color.Green;
            }
            else if (type.ToString() == IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS.ToString() || type.ToString() == IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP.ToString() || type.ToString() == IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE.ToString() || type.ToString() == IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString())
            {
                return System.Drawing.Color.Gray;
            }
            else if (type.ToString() == "STOP")
            {
                return System.Drawing.Color.Black;
            }
            else if (type.ToString() == "PAUSE")
            {
                return System.Drawing.Color.Orange;
            }
            else if (type.ToString() == "NONE")
            {
                return System.Drawing.Color.Gray;
            }
            else
            {
                return System.Drawing.Color.Black;
            }
        }

        #endregion DESIGN

        /// <summary>
        /// GetSakkoraStatus - 0-as eset eseten
        /// </summary>
        /// <param name="sakkoraStatus"></param>
        /// <returns></returns>
        public static string ChangeSakkoraStatus(string sakkoraStatus)
        {
            return sakkoraStatus == "0" ? KodTarak.IRAT_HATASA_UGYINTEZESRE.A_hatosagi_ugyintezesi_idore_nincs_hatassal_NONE : sakkoraStatus;
        }

        /// <summary>
        /// Forras rendszereknek visszaadjuk az egyseges 0-s erteket.
        /// Contentumban 4 nullas ertek van, visszafele csak egy 0 lehet.
        /// </summary>
        /// <param name="sakkoraStatus"></param>
        /// <returns></returns>
        public static string ChangebBackSakkoraStatusIfNull(string sakkoraStatus)
        {
            if (string.IsNullOrEmpty(sakkoraStatus))
            {
                return sakkoraStatus;
            }

            return
                sakkoraStatus == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE
                ||
                sakkoraStatus == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS
                ||
                sakkoraStatus == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE
                ||
                sakkoraStatus == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP
                ? "0"
                : sakkoraStatus;
        }

        /// <summary>
        /// SakkoraNullas
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool SakkoraNullas(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            switch (value)
            {
                case IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE:
                case IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS:
                case IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE:
                case IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP:
                    return true;

                default:
                    return false;
            }
        }

        /// <summary>
        /// hatralveo napok lathatosag ellenorzes
        /// </summary>
        /// <param name="ugyAllapota"></param>
        /// <param name="sakkoraAllapota"></param>
        /// <param name="ugyFajtaja"></param>
        /// <returns></returns>
        public static bool HatralevoNapokLathatoak(ExecParam xpm, string ugyAllapota, string sakkoraAllapotaTipus, string ugyFajtaja)
        {
            bool lathato = true;
            switch (ugyAllapota)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                    lathato = true;
                    break;

                default:
                    return false;
            }
            if (sakkoraAllapotaTipus.Equals(EnumIratHatasaUgyintezesre.STOP.ToString()) && IsUgyFajtaHatosagi(xpm, ugyFajtaja))
            {
                return false;
            }
            return lathato;
        }

        /// <summary>
        /// Sakkora atmenet lehetseges-e?
        /// </summary>
        /// <param name="xpm"></param>
        /// <param name="ugyJelenlegiSakkoraAllapota"></param>
        /// <param name="ugyFajta"></param>
        /// <param name="ugyEljarasiSzakaszFok"></param>
        /// <param name="kovetkezoSakkoraAllapota"></param>
        /// <param name="toStates">A jelenlegi állapotból elérhető sakkóra állapotok</param>
        /// <returns></returns>
        public static bool CheckSakkoraAtmenet(ExecParam xpm, string ugyJelenlegiSakkoraAllapota, string ugyFajta, string ugyEljarasiSzakaszFok, string kovetkezoSakkoraAllapota
                    , out List<string> toStates)
        {
            EnumIratHatasaUgyintezesre actualType;
            toStates = SakkoraLathatoAllapotok(xpm, ugyJelenlegiSakkoraAllapota, ugyFajta, ugyEljarasiSzakaszFok, out actualType, false);

            return toStates == null || toStates.Count < 1 ? false : toStates.Contains(kovetkezoSakkoraAllapota);
        }

        #region FELFUGGESZTES OKA FORMATUM

        /// <summary>
        /// Felfüggesztés oka formátum a mentéshez
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="oka"></param>
        /// <returns></returns>
        public string TurnFelfuggesztesOka(ExecParam execParam, string oka)
        {
            if (string.IsNullOrEmpty(oka))
            {
                return null;
            }

            if (oka.Contains('|'))
            {
                return oka;
            }

            List<KodTar_Cache.KodTarElem> felfuggesztesOkok = KodTar_Cache.GetKodtarakByKodCsoportList(KodTarak.UGYIRAT_FELFUGGESZTES_OKA.KCS, execParam, System.Web.HttpContext.Current.Cache, null);
            if (felfuggesztesOkok == null || felfuggesztesOkok.Count < 1)
            {
                return null;
            }

            KodTar_Cache.KodTarElem tmpKodtar = felfuggesztesOkok.FirstOrDefault(a => a.Kod == oka);
            if (tmpKodtar == null)
            {
                tmpKodtar = felfuggesztesOkok.FirstOrDefault(a => a.Nev == oka);
            }

            return
                tmpKodtar == null
                ? string.Format("{0}|{1}|{2}", KodTarak.UGYIRAT_FELFUGGESZTES_OKA.EGYEB_OK, "Egyeb", oka)
                : string.Format("{0}|{1}|{2}", tmpKodtar.Kod, tmpKodtar.Nev, oka);
        }

        #endregion FELFUGGESZTES OKA FORMATUM

        /// <summary>
        /// A megadott sakkóra ok ha nem kódként, hanem megjelenítendő névként van megadva, akkor a kódját adja vissza.
        /// (Ha nem találunk ilyen kódú vagy nevű elemet, akkor nincs konverzió, visszaadjuk a megadott értéket.
        /// </summary>
        /// <param name="sakkoraOk"></param>
        /// <returns></returns>
        public static string ConvertSakkoraOkToKodtarKod(string sakkoraOk, ExecParam execParamCaller)
        {
            // Ha nincs mit konvertálni, kilépünk:
            if (String.IsNullOrEmpty(sakkoraOk)) { return sakkoraOk; }

            // Két kódcsoportból kell összeszedni:
            List<KodTar_Cache.KodTarElem> sakkoraOkok = KodTar_Cache.GetKodtarakByKodCsoportList(KodTarak.UGYIRAT_FELFUGGESZTES_OKA.KCS, execParamCaller, System.Web.HttpContext.Current.Cache, null);
            sakkoraOkok.AddRange(KodTar_Cache.GetKodtarakByKodCsoportList(KodTarak.UGYIRAT_LEZARAS_OKA.KCS, execParamCaller, System.Web.HttpContext.Current.Cache, null));

            // Ha van ilyen kód, akkor azt adjuk vissza, egyébként a Nev mezőre keresünk:
            if (sakkoraOkok.Any(e => e.Kod == sakkoraOk))
            {
                return sakkoraOk;
            }
            else
            {
                KodTar_Cache.KodTarElem sakkoraOkItemByNev = sakkoraOkok.FirstOrDefault(e => e.Nev.Equals(sakkoraOk, StringComparison.CurrentCultureIgnoreCase));
                if (sakkoraOkItemByNev != null)
                {
                    return sakkoraOkItemByNev.Kod;
                }
                else
                {
                    // Nincs találat, visszaadjuk a megadott értéket:
                    return sakkoraOk;
                }
            }
        }

        /// <summary>
        /// GetKodtarFromCache
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="kodcsoport"></param>
        /// <param name="kodtariErtek"></param>
        /// <returns></returns>
        public static KRT_KodTarak GetKodtarFromCache(ExecParam execParam, string kodcsoport, string kodtariErtek)
        {
            List<KodTar_Cache.KodTarElem> cache = KodTar_Cache.GetKodtarakByKodCsoportList(kodcsoport, execParam, System.Web.HttpContext.Current.Cache, null);
            if (cache == null || cache.Count < 1)
            {
                #region FROM DB

                using (KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService())
                {
                    Result resultKcs = service.GetAllByKodcsoportKod(execParam, kodcsoport);
                    if (resultKcs.IsError || resultKcs.HasData())
                    {
                        return null;
                    }
                    var kodCsoportok = resultKcs.GetData<KRT_KodTarak>();
                    foreach (var kodCsoport in kodCsoportok)
                    {
                        if (kodCsoport.Kod == kodtariErtek)
                        {
                            return kodCsoport;
                        }
                    }
                    Logger.Error(string.Format("Nem található kódcsoport {0} as következő kódtári értékkel {1}", kodcsoport, kodtariErtek));

                    return null;
                }

                #endregion FROM DB
            }

            KodTar_Cache.KodTarElem item = cache.FirstOrDefault(s => s.Kod.Equals(kodtariErtek));
            if (item == null)
            {
                return null;
            }

            KRT_KodTarak result = new KRT_KodTarak
            {
                Id = item.Id,
                Egyeb = item.Egyeb,
                Kod = item.Kod,
                Nev = item.Nev,
                RovidNev = item.RovidNev
            };
            if (item.Sorrend != null)
            {
                result.Sorrend = item.Sorrend;
            }

            return result;
        }

        /// <summary>
        /// GetTodayLastMinute
        /// </summary>
        /// <returns></returns>

        /// <summary>
        /// SetUgySakkoraAllapotaFirstly
        /// </summary>
        /// <param name="irat"></param>
        /// <param name="ugy"></param>
        public static void SetUgySakkoraAllapotaFromIrat(string iratHatasaUgyintezesre, ref EREC_UgyUgyiratok ugy)
        {
            if (string.IsNullOrEmpty(ugy.SakkoraAllapot))
            {
                ugy.SakkoraAllapot =
                    string.IsNullOrEmpty(iratHatasaUgyintezesre) || iratHatasaUgyintezesre.StartsWith("0")
                    ? KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE
                    : iratHatasaUgyintezesre;
            }
            else if (IsUgySakkoraAllapotaModosithato(ugy.SakkoraAllapot, iratHatasaUgyintezesre))
            {
                ugy.SakkoraAllapot = iratHatasaUgyintezesre;
            }
            ugy.Updated.SakkoraAllapot = true;
        }

        /// <summary>
        /// SetUgySakkoraAllapotFromIrat
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ugy"></param>
        /// <param name="irat"></param>
        public static void SetUgySakkoraAllapotFromIrat(ExecParam execParam, ref EREC_UgyUgyiratok ugy, EREC_IraIratok irat)
        {
            if (ugy == null || irat == null)
            { return; }

            if (!string.IsNullOrEmpty(ugy.SakkoraAllapot))
            { return; }

            if (Rendszerparameterek.UseSakkora(execParam)) //SAKKORAS UGYFEL
            {
                ugy.SakkoraAllapot = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE;
                ugy.Updated.SakkoraAllapot = true;
            }
        }

        #region UGYINTEZES KEZEDET | HATARIDO

        /// <summary>
        /// Ugy UgyintezesKezdete meghatarozasa
        /// Sakkora es intezesi ido figyelessel.
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ugyirat"></param>
        public static void SetUgyUgyintezesKezdeteGlobal(ExecParam execParam, ref EREC_UgyUgyiratok ugyirat)
        {
            if (ugyirat == null)
            {
                return;
            }

            DateTime result = SetUgyUgyintezesKezdeteGlobal(execParam, ugyirat.IntezesiIdo, ugyirat.Hatarido, ugyirat.UgyintezesKezdete);
            ugyirat.UgyintezesKezdete = result.ToString();
            ugyirat.Updated.UgyintezesKezdete = true;
        }

        /// <summary>
        /// SetUgyUgyintezesKezdeteGlobal
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="intezesiIdo"></param>
        /// <param name="hatarido"></param>
        /// <param name="ugyintezesKezdete"></param>
        /// <returns></returns>
        public static DateTime SetUgyUgyintezesKezdeteGlobal(ExecParam execParam, string intezesiIdo, string hatarido, string ugyintezesKezdete)
        {
            DateTime result = GetDateTimeNow();
            if (Rendszerparameterek.UseSakkora(execParam))
            {
                if (!string.IsNullOrEmpty(intezesiIdo)) //MEG VAN ADVA AZ INTEZESI IDO
                {
                    int tmpIntezesiIdo;
                    if (int.TryParse(intezesiIdo, out tmpIntezesiIdo))
                    {
                        if (tmpIntezesiIdo == 1)
                        {
                            return GetDateTimeNextHour();
                        }
                        else
                        {
                            return GetDateTimeNextDay();
                        }
                    }
                    else // HIBAS INTEZESI IDO
                    {
                        return GetDateTimeNow();
                    }
                }
                else if (!string.IsNullOrEmpty(hatarido)) // CSAK HATARIDO VAN MEGADVA
                {
                    DateTime tmpHatarido;
                    if (DateTime.TryParse(hatarido, out tmpHatarido))
                    {
                        if (tmpHatarido <= DateTime.Now.AddDays(1))
                        {
                            return GetDateTimeNextHour();
                        }
                        else
                        {
                            return GetDateTimeNextDay();
                        }
                    }
                    else // HIBAS HATARIDO
                    {
                        return GetDateTimeNow();
                    }
                }
                else // INTEZESI IDO ES HATARIDO IS URES -> MOST
                {
                    return GetDateTimeNow();
                }
            }
            else if (string.IsNullOrEmpty(ugyintezesKezdete))// NEM SAKKORAS UGYFELEKNEL -> MOST
            {
                return GetDateTimeNow();
            }
            return result;
        }

        /// <summary>
        /// SetUgyUgyintezesKezdeteGlobal
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="datumTol"></param>
        /// <param name="intezesiIdo"></param>
        /// <returns></returns>
        public static DateTime SetUgyUgyintezesKezdeteGlobal(ExecParam execParam, DateTime datumTol, string intezesiIdo)
        {
            if (Rendszerparameterek.UseSakkora(execParam))
            {
                if (!string.IsNullOrEmpty(intezesiIdo)) //MEG VAN ADVA AZ INTEZESI IDO
                {
                    int tmpIntezesiIdo;
                    if (int.TryParse(intezesiIdo, out tmpIntezesiIdo))
                    {
                        if (tmpIntezesiIdo == 1)
                        {
                            return GetDateTimeNextHour(datumTol);
                        }
                        else
                        {
                            return GetDateTimeNextDay(datumTol);
                        }
                    }
                    else // HIBAS INTEZESI IDO
                    {
                        return GetDateTimeNow();
                    }
                }
                else
                {
                    return GetDateTimeNow();
                }
            }
            return GetDateTimeNow();
        }

        /// <summary>
        ///GetUgyUgyintezesKezdeteDefaultGlobal
        /// </summary>
        /// <param name="execParam"></param>
        /// <returns></returns>
        public static string GetUgyUgyintezesKezdeteDefaultGlobal(ExecParam execParam)
        {
            return GetUgyUgyintezesKezdeteDefaultDateTimeGlobal(execParam).ToString();
        }

        /// <summary>
        /// GetUgyUgyintezesKezdeteDefaultDateTimeGlobal
        /// </summary>
        /// <param name="execParam"></param>
        /// <returns></returns>
        public static DateTime GetUgyUgyintezesKezdeteDefaultDateTimeGlobal(ExecParam execParam)
        {
            if (Rendszerparameterek.UseSakkora(execParam))
            {
                return GetDateTimeNextDay();
            }
            else // NEM SAKKORAS UGYFELEKNEL -> MOST
            {
                return GetDateTimeNow();
            }
        }

        /// <summary>
        /// GetUgyUgyintezesHataridoGlobal
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="DatumTol"></param>
        /// <param name="strIntezesiIdo"></param>
        /// <param name="strIdoegyseg"></param>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        public static DateTime GetUgyUgyintezesHataridoGlobal(ExecParam execParam, DateTime DatumTol, string strIntezesiIdo, string strIdoegyseg, out string ErrorMessage)
        {
            #region VARAIABLES

            DateTime resultHatarido = DateTime.Now.AddDays(30);
            ErrorMessage = string.Empty;

            #endregion VARAIABLES

            if (string.IsNullOrEmpty(strIdoegyseg) || string.IsNullOrEmpty(strIntezesiIdo))
            {
                return DatumTol == null ? DateTime.Now.AddDays(30) : DatumTol.AddDays(30);
            }
            int intIdoegyseg;
            int intIntezesiIdo;
            string outIntezesiIdoParseError;
            if (!GetUgyUgyintezesIntezesiIdoGlobal(execParam, strIdoegyseg, strIntezesiIdo, out intIdoegyseg, out intIntezesiIdo, out outIntezesiIdoParseError))
            {
                ErrorMessage = outIntezesiIdoParseError;
                return resultHatarido;
            }

            if (Rendszerparameterek.UseSakkora(execParam)) //SAKKORAS UGYFEL
            {
                if (intIdoegyseg.ToString() == KodTarak.IDOEGYSEG.Honap.ToString())
                {
                    resultHatarido = DatumTol.AddMonths(intIntezesiIdo);
                }
                else if (intIdoegyseg.ToString() == KodTarak.IDOEGYSEG.Munkanap.ToString())
                {
                    KRT_Extra_NapokService service_extra_napok = eAdminService.ServiceFactory.GetKRT_Extra_NapokService();
                    Result result_extra_napok = service_extra_napok.KovetkezoMunkanap(execParam, DatumTol, intIntezesiIdo);
                    if (result_extra_napok.IsError)
                    {
                        ErrorMessage = ResultError.GetErrorMessageFromResultObject(result_extra_napok);
                    }
                    else
                    {
                        try
                        {
                            resultHatarido = (DateTime)result_extra_napok.Record;
                        }
                        catch
                        {
                            resultHatarido = DateTime.MinValue;
                        }
                    }
                    resultHatarido += new TimeSpan(DatumTol.Hour, DatumTol.Minute, 0);
                }
                else if (intIdoegyseg.ToString() == KodTarak.IDOEGYSEG.Nap.ToString())
                {
                    if (intIntezesiIdo == 42) // NMHH 42 NAP = 1 NAP ??
                    {
                        resultHatarido = DatumTol.AddHours(1);
                    }
                    else if (intIntezesiIdo == 1)
                    {
                        if (DatumTol < DateTime.Now)
                        {
                            DatumTol = SetUgyUgyintezesKezdeteGlobal(execParam, strIntezesiIdo, string.Empty, string.Empty);
                        }
                        resultHatarido = DatumTol.AddDays(1);
                    }
                    else
                    {
                        resultHatarido = (DatumTol < DateTime.Now && DatumTol.Hour != 0) ? DatumTol.AddDays(intIntezesiIdo) : DatumTol.AddDays(intIntezesiIdo - 1);
                        resultHatarido += new TimeSpan(23, 59, 59);
                    }
                }
                else if (intIdoegyseg.ToString() == KodTarak.IDOEGYSEG.Ora.ToString())
                {
                    resultHatarido = DatumTol.AddHours(intIntezesiIdo);
                }
                else if (intIdoegyseg.ToString() == KodTarak.IDOEGYSEG.Perc.ToString())
                {
                    resultHatarido = DatumTol.AddMinutes(intIntezesiIdo);
                }
                else
                {
                    resultHatarido = DatumTol.AddDays(intIntezesiIdo);
                    resultHatarido += new TimeSpan(DatumTol.Hour, DatumTol.Minute, 0);
                }
            }
            else // NEM SAKKORAS UGYFELEKNEL
            {
                if (intIdoegyseg.ToString() == KodTarak.IDOEGYSEG.Honap.ToString())
                {
                    resultHatarido = DatumTol.AddMonths(intIntezesiIdo);
                }
                else if (intIdoegyseg.ToString() == KodTarak.IDOEGYSEG.Munkanap.ToString())
                {
                    KRT_Extra_NapokService service_extra_napok = eAdminService.ServiceFactory.GetKRT_Extra_NapokService();
                    Result result_extra_napok = service_extra_napok.KovetkezoMunkanap(execParam, DatumTol, intIntezesiIdo);
                    if (result_extra_napok.IsError)
                    {
                        ErrorMessage = ResultError.GetErrorMessageFromResultObject(result_extra_napok);
                    }
                    else
                    {
                        try
                        {
                            resultHatarido = (DateTime)result_extra_napok.Record;
                        }
                        catch
                        {
                            resultHatarido = DateTime.Now.AddDays(30);
                        }
                    }
                }
                else if (intIdoegyseg.ToString() == KodTarak.IDOEGYSEG.Nap.ToString())
                {
                    resultHatarido = DatumTol.AddDays(intIntezesiIdo);
                }
                else if (intIdoegyseg.ToString() == KodTarak.IDOEGYSEG.Ora.ToString())
                {
                    resultHatarido = DatumTol.AddHours(intIntezesiIdo);
                }
                else if (intIdoegyseg.ToString() == KodTarak.IDOEGYSEG.Perc.ToString())
                {
                    resultHatarido = DatumTol.AddMinutes(intIntezesiIdo);
                }
                else
                {
                    resultHatarido = DatumTol.AddDays(intIntezesiIdo);
                }
                resultHatarido += new TimeSpan(DatumTol.Hour, DatumTol.Minute, 0);
            }
            return resultHatarido;
        }

        /// <summary>
        /// GetIratintezesHataridoGlobal
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="strKezdet"></param>
        /// <param name="strIntezesiIdo"></param>
        /// <param name="strIdoegyseg"></param>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        public static DateTime GetIratintezesHataridoGlobal(ExecParam execParam, string strKezdet, string strIntezesiIdo, string strIdoegyseg, out string ErrorMessage)
        {
            DateTime kezdet;
            if (string.IsNullOrEmpty(strKezdet) || !DateTime.TryParse(strKezdet, out kezdet))
            {
                kezdet = DateTime.Now;
            }

            return GetUgyUgyintezesHataridoGlobal(execParam, kezdet, strIntezesiIdo, strIdoegyseg, out ErrorMessage);
        }

        /// <summary>
        /// GetUgyUgyintezesIntezesiIdoGlobal
        /// </summary>
        /// <param name="ExecParam"></param>
        /// <param name="strIdoegyseg"></param>
        /// <param name="strIntezesiIdo"></param>
        /// <param name="outintIdoegyseg"></param>
        /// <param name="outintIntezesiIdo"></param>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        public static bool GetUgyUgyintezesIntezesiIdoGlobal(ExecParam ExecParam, string strIdoegyseg, string strIntezesiIdo, out int outintIdoegyseg, out int outintIntezesiIdo, out string ErrorMessage)
        {
            ErrorMessage = string.Empty;
            outintIdoegyseg = 0;
            outintIntezesiIdo = 0;

            if (string.IsNullOrEmpty(strIdoegyseg) || strIdoegyseg == "0" || string.IsNullOrEmpty(strIntezesiIdo) || strIntezesiIdo == "0")
            {
                // default érték használata
                int nDefaultIntezesiIdo = Rendszerparameterek.GetInt(ExecParam, Rendszerparameterek.DEFAULT_INTEZESI_IDO);
                int nDefaultIdoegyseg = Rendszerparameterek.GetInt(ExecParam, Rendszerparameterek.DEFAULT_INTEZESI_IDO_IDOEGYSEG);

                if (nDefaultIdoegyseg == 0)
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65101); //"Az alapértelmezett időegység nem határozható meg!"
                    return false;
                }

                if (nDefaultIntezesiIdo == 0)
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65102); //"Az alapértelmezett intézési idő nem határozható meg!"
                    return false;
                }

                outintIdoegyseg = nDefaultIdoegyseg;
                outintIntezesiIdo = nDefaultIntezesiIdo;
            }
            else
            {
                int nIdoegyseg;
                if (!int.TryParse(strIdoegyseg, out nIdoegyseg))
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65103); //"Az időegység nem határozható meg."
                    return false;
                }

                int nIntezesiIdo;
                if (!int.TryParse(strIntezesiIdo, out nIntezesiIdo))
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65104); //"Az intézési idő nem határozható meg!"
                    return false;
                }

                outintIdoegyseg = nIdoegyseg;
                outintIntezesiIdo = nIntezesiIdo;
            }
            return true;
        }

        #endregion UGYINTEZES KEZEDET | HATARIDO

        #region DATE FUNCTIONS

        /// <summary>
        /// GetTodayLastMinute
        /// </summary>
        /// <returns></returns>
        public static DateTime GetTodayLastMinute()
        {
            return DateTime.Today + new TimeSpan(23, 59, 59);
        }

        /// <summary>
        /// GetTodayLastMinuteAsString
        /// </summary>
        /// <returns></returns>
        public static string GetTodayLastMinuteAsString()
        {
            return GetTodayLastMinute().ToString();
        }

        /// <summary>
        /// Kovetkezo ora nulla perc
        /// </summary>
        /// <returns></returns>
        public static DateTime GetDateTimeNextHour()
        {
            DateTime nextHourDate = DateTime.Now.AddHours(1);
            return new DateTime(nextHourDate.Year, nextHourDate.Month, nextHourDate.Day, nextHourDate.Hour, 0, 0);
        }

        /// <summary>
        /// GetDateTimeNextHour
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public static DateTime GetDateTimeNextHour(DateTime from)
        {
            DateTime nextHourDate = from.AddHours(1);
            return new DateTime(nextHourDate.Year, nextHourDate.Month, nextHourDate.Day, nextHourDate.Hour, 0, 0);
        }

        /// <summary>
        /// Kovetkezo nap nulla ora
        /// </summary>
        /// <returns></returns>
        public static DateTime GetDateTimeNextDay()
        {
            return DateTime.Today.AddDays(1) + new TimeSpan(0, 0, 0);
        }

        /// <summary>
        /// GetDateTimeNextDay
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public static DateTime GetDateTimeNextDay(DateTime from)
        {
            return new DateTime(from.Year, from.Month, from.Day).AddDays(1) + new TimeSpan(0, 0, 0);
        }

        /// <summary>
        /// Aktualis datum
        /// </summary>
        /// <returns></returns>
        public static DateTime GetDateTimeNow()
        {
            return DateTime.Now;
        }

        #endregion DATE FUNCTIONS

        /// <summary>
        /// Set irat eljarasi szakasz to 1, if ugyirat hatosagi.
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="irat"></param>
        /// <param name="ugy"></param>
        /// <returns></returns>
        public static void SetIratEljarasiSzakaszDefault(ExecParam execParam, ref EREC_IraIratok irat, string ugyFajta)
        {
            if (irat != null && string.IsNullOrEmpty(irat.EljarasiSzakasz) && !string.IsNullOrEmpty(ugyFajta) && Sakkora.IsUgyFajtaHatosagi(execParam.Clone(), ugyFajta))
            {
                irat.EljarasiSzakasz = KodTarak.ELJARASI_SZAKASZ_FOK.I;
                irat.Updated.EljarasiSzakasz = true;
            }
        }
    }
}