using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace Contentum.eUtility
{
    public class Search
    {
        public class SortCondition
        {
            private string _SortExpression = String.Empty;

            public string SortExpression
            {
                get { return _SortExpression; }
                set { _SortExpression = value; }
            }

            private SortDirection _SortDirection = SortDirection.Ascending;

            public SortDirection SortDirection
            {
                get { return _SortDirection; }
                set { _SortDirection = value; }
            }

            public SortCondition()
            {
            }

            public SortCondition(string SortConditionString)
            {
                if(String.IsNullOrEmpty(SortConditionString.Trim())) return;

                string[] parts = SortConditionString.Split(' ');
                this._SortExpression = parts[0].Trim();

                if (parts.Length > 1)
                {
                    if (String.Compare(parts[1].Trim(), "DESC", true) == 0)
                    {
                        this._SortDirection = SortDirection.Descending;
                    }
                }
            }

            public bool Contains(string SortExpression)
            {
                if (String.Compare(this._SortExpression, SortExpression, true) == 0)
                {
                    return true;
                }

                return false;
            }

        }

        public class SortConditionList:IEnumerable<SortCondition>
        {
            private List<SortCondition> _list = new List<SortCondition>();

            public SortConditionList()
            {
            }

            public SortConditionList(string SortConditionListString)
            {
                if (String.IsNullOrEmpty(SortConditionListString.Trim())) return;

                if (SortConditionListString.StartsWith(FullSortExpression))
                {
                    // [FullSortExpression] string lev�g�sa:
                    SortConditionListString = SortConditionListString.Remove(0, FullSortExpression.Length);
                }

                string[] sortConditionStringArray = SortConditionListString.Split(',');

                foreach (string sortConditionString in sortConditionStringArray)
                {
                    _list.Add(new SortCondition(sortConditionString.Trim()));
                }
            }

            public bool Contains(string SortExpression)
            {
                foreach (SortCondition sortCondition in _list)
                {
                    if (sortCondition.Contains(SortExpression))
                        return true;
                }
                return false;
            }

            public int Count
            {
                get
                {
                    return _list.Count;
                }
            }

            public SortDirection GetSortDirection(string SortExpression)
            {
                foreach (SortCondition sortCondition in _list)
                {
                    if (sortCondition.Contains(SortExpression))
                        return sortCondition.SortDirection;
                }

                return SortDirection.Ascending;
            }

            public int GetSortNumber(string SortExpression)
            {
                int i = 0;

                foreach (SortCondition sortCondition in _list)
                {
                    i++;
                    if (sortCondition.Contains(SortExpression))
                        return i;
                }

                return -1;
            }

            #region IEnumerable<SortCondition> Members

            public IEnumerator<SortCondition> GetEnumerator()
            {
                return _list.GetEnumerator();
            }

            #endregion

            #region IEnumerable Members

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return _list.GetEnumerator();
            }

            #endregion
        }

        public static void SetIdsToSearchObject(Page ParentPage, String QueryStringName, object SearchObject,string CustomSessionName)
        {
            if (ParentPage == null || QueryStringName == null || SearchObject == null) return;
            if (ParentPage.IsPostBack)
            {
                return;
            }

            if (ParentPage.Request.QueryString[QueryStringName] != null && !String.IsNullOrEmpty(ParentPage.Request.QueryString.Get(QueryStringName).ToString()))
            {
                string _ids = ParentPage.Request.QueryString.Get(QueryStringName).ToString();

                char[] separator = { ';' };
                string[] _idsArray = _ids.Split(separator, StringSplitOptions.RemoveEmptyEntries);

                _ids = String.Join("','", _idsArray);
                if (!String.IsNullOrEmpty(_ids))
                {
                    _ids = "'" + _ids + "'";
                }

                try
                {
                    System.Reflection.PropertyInfo IdField = SearchObject.GetType().GetProperty("Id");
                    object IdObject = IdField.GetValue(SearchObject, null);
                    System.Reflection.PropertyInfo Operator = IdObject.GetType().GetProperty("Operator");
                    System.Reflection.PropertyInfo Value = IdObject.GetType().GetProperty("Value");
                    if (!String.IsNullOrEmpty(_ids))
                    {
                        Operator.SetValue(IdObject, Query.Operators.inner, null);
                        Value.SetValue(IdObject, _ids, null);
                    }
                    else
                    {
                        // ha nem adtunk �t Id-t, soha nem teljes�l� felt�telt gener�lunk
                        Operator.SetValue(IdObject, Query.Operators.isnull, null);
                        Value.SetValue(IdObject,"", null);
                        Logger.Info("Nincs megadva Id!");
                    }

                    
                    //        http://localhost:100/eAdmin/SzerepkorokList.aspx?Id=8a378737-3460-41e3-bbcf-f9c461f2a844;0c63de14-8060-4a16-8a22-333c6e7a9882
                    Search.SetSearchObject_CustomSessionName(ParentPage, SearchObject,CustomSessionName);
                }
                catch { }
            }
        }

        public static void SetIdsToSearchObject(Page ParentPage, String QueryStringName, object SearchObject)
        {
            if (ParentPage == null || QueryStringName == null || SearchObject == null) return;
            
            string typename = SearchObject.GetType().Name;
            SetIdsToSearchObject(ParentPage, QueryStringName, SearchObject, typename);
        }

        public static bool GetBool(String Value)
        {
            return Value == "1";
        }

        public static object GetSearchObject(Page ParentPage, object SearchObject)
        {
            string typename = SearchObject.GetType().Name;
            return GetSearchObject_CustomSessionName(ParentPage, SearchObject, typename);
        }
        
        public static object GetSearchObject_CustomSessionName(Page ParentPage, object SearchObject, String CustomSessionName)
        {
            if (ParentPage == null || SearchObject == null) return null;

            return ParentPage.Session[CustomSessionName] ?? SearchObject;
        }

        public static void SetSearchObject(Page ParentPage, object SearchObject)
        {
            string typename = SearchObject.GetType().Name;
            SetSearchObject_CustomSessionName(ParentPage, SearchObject, typename);
        }

        public static void SetSearchObject_CustomSessionName(Page ParentPage, object SearchObject, String CustomSessionName)
        {
            if (ParentPage == null || SearchObject == null) return;

            ParentPage.Session[CustomSessionName] = SearchObject;
        }


        /// <summary>
        /// Elt�vol�tja a session-b�l a megadott t�pus� keres�si objektumot
        /// </summary>
        public static void RemoveSearchObjectFromSession(Page parentPage, Type searchObjectType)
        {
            if (parentPage == null || searchObjectType == null) return;

            RemoveSearchObjectFromSession_CustomSessionName(parentPage, searchObjectType.Name);
        }

        /// <summary>
        /// Elt�vol�tja a session-b�l a megadott t�pus� keres�si objektumot
        /// </summary>
        public static void RemoveSearchObjectFromSession_CustomSessionName(Page parentPage, String CustomSessionName)
        {
            if (parentPage == null) return;

            if (parentPage.Session[CustomSessionName] != null)
            {
                parentPage.Session.Remove(CustomSessionName);
            }
        }

        public static bool IsSearchObjectInSession(Page parentPage, Type searchObjectType)
        {
            if (parentPage == null || searchObjectType == null) return false;

            return IsSearchObjectInSession_CustomSessionName(parentPage, searchObjectType.Name);
        }

        public static bool IsSearchObjectInSession_CustomSessionName(Page parentPage, String CustomSessionName)
        {
            return (parentPage != null && parentPage.Session[CustomSessionName] != null);
        }

        /// <summary>
        /// �sszehasonl�t k�t keres�si objektumot
        /// </summary>
        /// <param name="searchObject1"></param>
        /// <param name="searchObject2"></param>
        /// <returns>True: ha a k�t keres�si objektum �ltal el��ll�tott Where felt�tel megegyezik</returns>
        public static bool IsIdentical(object searchObject1, object searchObject2)
        {
            if (searchObject1 == null && searchObject2 == null)
            {
                return true;
            }
            else if (searchObject1 == null || searchObject2 == null)
            {
                return false;
            }

            return IsIdentical(searchObject1, searchObject2, "", "");
        }

        /// <summary>
        /// �sszehasonl�t k�t keres�si objektumot
        /// </summary>
        /// <param name="searchObject1">1-es keres�si obj.</param>
        /// <param name="searchObject2">2-es keres�si obj.</param>
        /// <param name="whereByManual1">1-es keres�si obj. manu�lis Where felt�tele</param>
        /// <param name="whereByManual2">2-es keres�si obj. manu�lis Where felt�tele</param>
        /// <returns>True: ha a k�t keres�si objektum �ltal el��ll�tott Where felt�tel megegyezik</returns>
        public static bool IsIdentical(object searchObject1, object searchObject2
            , string whereByManual1, string whereByManual2)
        {
            if (searchObject1 == null && searchObject2 == null && String.IsNullOrEmpty(whereByManual1) && String.IsNullOrEmpty(whereByManual2))
            {
                return true;
            }
            else if (searchObject1 == null || searchObject2 == null || whereByManual1 == null || whereByManual2 == null)
            {
                return false;
            }

            Query query1 = new Query();
            query1.BuildFromBusinessDocument(searchObject1,true);
            query1.Where += whereByManual1;

            Query query2 = new Query();
            query2.BuildFromBusinessDocument(searchObject2,true);
            query2.Where += whereByManual2;

            if (String.Equals(query1.Where, query2.Where))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public const string FullSortExpression = "[FullSortExpression]";

        public static string GetOrderBy(String GridViewName, StateBag ViewState, String SortExpression, SortDirection SortDirection)
        {
            string _ret = "";
            if (GridViewName == null || ViewState == null || SortExpression == null) return _ret;

            if (SortExpression != "")
            {
                _ret = SortExpression;

                // ha [FullSortExpression] stringgel kezd�dik a kifejez�s, akkor ez m�r tartalmazza a rendez�si ir�nyokat is,
                // nem kell azt hozz�csapni (mivel lehet hogy t�bb oszlopot is megadtunk, mindegyikhez k�l�n rendez�si ir�nnyal)
                if (_ret.StartsWith(FullSortExpression))
                {
                    // [FullSortExpression] string lev�g�sa:
                    _ret = _ret.Remove(0, FullSortExpression.Length);
                }
                else
                {
                    switch (SortDirection)
                    {
                        case SortDirection.Ascending:
                            _ret = String.Format(_ret, "ASC");  // ha t�bb mez�b�l �ll a rendez�s, megadhat� {0} is az egyes mez�kh�z
                            _ret += " ASC";
                            break;
                        case SortDirection.Descending:
                            _ret = String.Format(_ret, "DESC");  // ha t�bb mez�b�l �ll a rendez�s, megadhat� {0} is az egyes mez�kh�z
                            _ret += " DESC";
                            break;
                    }
                }
            }
            ViewState[GridViewName + "SortExpression"] = SortExpression;
            ViewState[GridViewName + "SortDirection"] = SortDirection;

            return _ret;
        }

        /// <summary>
        /// Visszaadja a ViewState-ben let�rolt, a GridView-hoz tartoz� SortExpression-t, ha az l�tezik.
        /// Ha nincs a ViewState-ben, a megadott default �rt�kkel t�r vissza.
        /// </summary>
        public static string GetSortExpressionFromViewState(String GridViewName, StateBag ViewState, String DefaultSortExpression)
        {
            if (GridViewName == null || ViewState == null || DefaultSortExpression == null) return "";

            string key = GridViewName + "SortExpression";
            if (ViewState[key] != null)
            {
                return (String)ViewState[key];
            }
            else
            {
                return DefaultSortExpression;
            }
        }

        /// <summary>
        /// Visszaadja a ViewState-ben let�rolt, a GridView-hoz tartoz� SortDirection-t, ha az l�tezik.
        /// Default �rt�k a SortDirection.Ascending, ha nem lenne benne a ViewState-ben
        /// </summary>
        public static SortDirection GetSortDirectionFromViewState(String GridViewName, StateBag ViewState, SortDirection DefaultSortDirection)
        {
            if (GridViewName == null || ViewState == null) return SortDirection.Ascending;

            string key = GridViewName + "SortDirection";
            if (ViewState[key] != null)
            {
                if (ViewState[key].ToString() == SortDirection.Ascending.ToString())
                {
                    return SortDirection.Ascending;
                }
                else
                {
                    return SortDirection.Descending;
                }
            }
            else
            {
                return DefaultSortDirection;
            }
        }

        public static SortDirection GetSortDirectionFromViewState(String GridViewName, StateBag ViewState)
        {
            if (GridViewName == null || ViewState == null) return SortDirection.Ascending;

            string key = GridViewName + "SortDirection";
            if (ViewState[key] != null)
            {
                if (ViewState[key].ToString() == SortDirection.Ascending.ToString())
                {
                    return SortDirection.Ascending;
                }
                else
                {
                    return SortDirection.Descending;
                }
            }
            else
            {
                return SortDirection.Ascending;
            }
        }

        public static string GetOperatorByLikeCharater(String Text)
        {
            if (Text == null) return "";

            if (Text.Trim() != "")
            {
                if (Text.Contains("*"))
                {
                    return Query.Operators.like;
                }
                else
                {
                    return Query.Operators.equals;
                }
            }

            return "";
        }



        #region where kiirat�sa

        public const string whereDelimeter = " | ";

        public static string GetReadableWhere(Control control)
        {
            List<Control> eFormPanels = new List<Control>();
            if (control is Contentum.eUIControls.eFormPanel)
            {
                eFormPanels.Add(control);
            }
            else
            {
                if (control.Page.Master == null)
                {
                    foreach (Control c in control.Controls)
                    {
                        if (c is Contentum.eUIControls.eFormPanel)
                        {
                            eFormPanels.Add(c);
                        }
                    }
                }
                else
                {
                    foreach (Control placeHolder in UI.FindControls<ContentPlaceHolder>(control.Controls))
                    {
                        foreach (Control c in placeHolder.Controls)
                        {
                            if (c is Contentum.eUIControls.eFormPanel)
                            {
                                eFormPanels.Add(c);
                            }
                        }
                    }
                }
            }


            //// minden toplevel eFormPanel lek�r�se (akkor is, ha pl. UpdatePanelen bel�l van)
            //// (de sajnos �gy a header is beleker�l (pl. template-ek))
            //Queue<Control> queueControls = new Queue<Control>();
            //queueControls.Enqueue(control);

            //while (queueControls.Count > 0)
            //{
            //    Control ctrl = queueControls.Dequeue();
            //    if (ctrl is Contentum.eUIControls.eFormPanel)
            //    {
            //        eFormPanels.Add(ctrl);
            //    }
            //    else if (ctrl.HasControls())
            //    {
            //        foreach (Control c in ctrl.Controls)
            //        {
            //            queueControls.Enqueue(c);
            //        }
            //    }
            //}

            string where = String.Empty;

            foreach (Control eFormPanel in eFormPanels)
            {
                where += GetReadableWhereOnEFormPanel(eFormPanel);
            }

            return where;
        }

        public static string GetReadableWhereOnPanel(Control control)
        {
            return GetReadableWhereOnEFormPanel(control);
        }

        private static string GetReadableWhereOnEFormPanel(Control control)
        {
            string text = String.Empty;
            string labelText = String.Empty;

            foreach (Control c in control.Controls)
            {
                SetTextByControl(c, ref labelText, ref text);
            }

            return text;
        }

        private static void SetTextByControl(Control control, ref string labelText, ref string text)
        {

            if(!control.Visible) return;

            if (control is Label)
            {
                labelText = (control as Label).Text;
                return;
            }

            if (control is TextBox)
            {
                string cText = (control as TextBox).Text;
                text += GetQueryText(labelText, cText);
                labelText = String.Empty;
                return;
            }

            if (control is DropDownList)
            {
                DropDownList dlist = control as DropDownList;
                if (dlist.SelectedIndex > -1 && !String.IsNullOrEmpty(dlist.SelectedValue))
                {
                    string cText = dlist.SelectedItem.Text;
                    text += GetQueryText(labelText, cText);
                }
                labelText = String.Empty;
                return;
            }

            if (control is CheckBox)
            {
                CheckBox cb = control as CheckBox;
                if (!String.IsNullOrEmpty(cb.Text))
                {
                    if (cb.Checked)
                    {
                        text += cb.Text + whereDelimeter;
                    }
                }
                else if(!String.IsNullOrEmpty(labelText))
                {
                    if (cb.Checked)
                    {
                        string cText = cb.Checked ? "Igen" : "Nem";
                        text += GetQueryText(labelText, cText);
                    }
                }
                labelText = String.Empty;
                return;
            }

            if (control is RadioButtonList)
            {
                RadioButtonList rbList = control as RadioButtonList;
                if (rbList.SelectedIndex > -1 && !String.IsNullOrEmpty(rbList.SelectedValue))
                {
                    string cText = rbList.SelectedItem.Text;
                    text += GetQueryText(labelText, cText);
                }
                labelText = String.Empty;
                return;
            }

            if (control is CheckBoxList)
            {
                var cbList = control as CheckBoxList;
                var t = labelText + " ";
                int n = 0;
                foreach (ListItem listItem in cbList.Items)
                {
                    if (listItem.Selected)
                    {
                        if (n > 0) { t += ", "; }
                        t += listItem.Text;
                        n++;
                    }
                }
                if (n > 0) { text += t + whereDelimeter; }
                labelText = String.Empty;
                return;
            }

            if (control is Contentum.eUIControls.DynamicValueControl)
            {
                if (!String.IsNullOrEmpty((control as Contentum.eUIControls.DynamicValueControl).Value))
                {
                    string cText = (control as Contentum.eUIControls.DynamicValueControl).Text;
                    text += GetQueryText(labelText, cText);
                }
                labelText = String.Empty;
                return;
            }

            if (control is ISearchComponent)
            {
                string cText = (control as ISearchComponent).GetSearchText();
                text += GetQueryText(labelText, cText);
                labelText = String.Empty;
                return;
            }

            if (control is HtmlTableRow || control is Panel || control is Contentum.eUIControls.eFormPanel || control is HtmlTable || control is UpdatePanel)
            {
                text += GetReadableWhereOnPanel(control);
                return;
            }

            if (control is HtmlTableCell)
            {
                foreach (Control c in control.Controls)
                {
                    SetTextByControl(c, ref labelText, ref text);
                }
                return;
            }
        }

        public static string GetQueryText(string searchCondition, string searchValue)
        {
            string text = String.Empty;
            if (!String.IsNullOrEmpty(searchValue) && !String.IsNullOrEmpty(searchCondition))
            {
                foreach (string operatorName in Query.OperatorNames.Values)
                {
                    if (searchValue.StartsWith(operatorName))
                    {
                        if (searchCondition.EndsWith(":"))
                        {
                            searchCondition = searchCondition.Substring(0, searchCondition.Length - 1);
                        }
                        break;
                    }
                }

                text = searchCondition + " " + searchValue + whereDelimeter;
            }
            else
            {
                if (!String.IsNullOrEmpty(searchValue))
                {
                    text = searchValue + whereDelimeter;
                }
            }
            return text;
        }

        #endregion

        public static string GetSqlInnerString(string[] array)
        {
            if (array == null || array.Length == 0)
                return String.Empty;

            return String.Format("'{0}'", String.Join("','", array));
        }

        public static string ConcatWhereExpressions(string where1, string where2)
        {
            return ConcatWhereExpressions(where1, where2, Query.Operators.and);
        }

        public static string ConcatWhereExpressions(string where1, string where2, string concatOperator)
        {
            if (where1 == null) where1 = String.Empty;

            if (!String.IsNullOrEmpty(where2))
            {
                if (!String.IsNullOrEmpty(where1))
                {
                    where1 += " " + concatOperator + " ";
                }

                where1 += where2;
            }

            return where1;
        }

    }


}
