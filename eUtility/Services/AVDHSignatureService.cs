﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Xml;
using System.Text;
using System.Web.Configuration;

namespace Contentum.eUtility
{
    public class AVDHSignatureService
    {
        private string _responseXML;
        private string _errorMessage;
        private string _signedDataBase64;
        private string signatureData;
        private bool raiseError;
        private bool isFileLocal;
        private string filePath;
        private string _uploadResponseXML;

        public string responseXML
        {
            get { return _responseXML; }
            set { _responseXML = value; }
        }
        public string errorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        public string signedDataBase64
        {
            get { return _signedDataBase64; }
            set { _signedDataBase64 = value; }
        }
        public string SignatureData
        {
            get { return HTMLDecode(signatureData); }
            set { signatureData = value; }
        }
        public bool RaiseError
        {
            get { return raiseError; }
            set { raiseError = value; }
        }
        public bool IsFileLocal
        {
            get { return isFileLocal; }
            set { isFileLocal = value; }
        }
        public string FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }

        public string UploadResponseXML
        {
            get { return _uploadResponseXML; }
            set { _uploadResponseXML = value; }
        }

        public static string AVDH_MODE;
        static string AVDH_DHSZ_SSL_URL;
        static string AVDH_TKASZ_SSL_URL;
        static string AVDH_PAdES;
        static string AVDH_XAdES;
        static string AVDH_ASiC;
        static string AVDH_TKASZ;
        static string AVDH_DOWNLOAD;
        static string AVDH_DELETE;
        static string AVDH_LOGOUT;

        public AVDHSignatureService()
        {
            AVDH_MODE = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AVDH_MODE");

            AVDH_DHSZ_SSL_URL = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AVDH_DHSZ_SSL_URL");
            AVDH_TKASZ_SSL_URL = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AVDH_TKASZ_SSL_URL");
            AVDH_PAdES = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AVDH_PAdES");
            AVDH_XAdES = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AVDH_XAdES");
            AVDH_ASiC = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AVDH_ASiC");
            AVDH_TKASZ = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AVDH_TKASZ");
            AVDH_DOWNLOAD = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AVDH_DOWNLOAD");
            AVDH_DELETE = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AVDH_DELETE");
            AVDH_LOGOUT = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AVDH_LOGOUT");
        }

        public void UploadPDF(string filename, string formatum, string externalLink)
        {
            UploadPDF(filename, formatum, externalLink, String.Empty, String.Empty, String.Empty);
        }

        /// <summary>
        /// Pdf aláírás AVDH-val.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="buffer"></param>
        /// <param name="signXMLname"></param>
        /// <param name="signXMLstring"></param>
        public void UploadPDF(string filename, string formatum, byte[] buffer)
        {
            try
            {
                string soapXMLMessage = "";
                string AVDH_URL = AVDH_DHSZ_SSL_URL;
                
                if (formatum == "pdf")
                {
                    if (buffer.Length> 20971520)
                    {
                        RaiseError = true;
                        errorMessage = "A kért fájlok AVDH-s aláírása nem lehetséges, a fájl méret korlátozás (max. 20 MB) átlépése miatt";
                        responseXML = "";
                        return;
                    }

                    soapXMLMessage = getUploadXML(filename, buffer, "padesUpload");
                    AVDH_URL = AVDH_URL + AVDH_PAdES;

                }
                else if (formatum == "xml")
                {
                    if (buffer.Length > 209715200)
                    {
                        RaiseError = true;
                        errorMessage = "A kért fájlok AVDH-s aláírása nem lehetséges, a fájl méret korlátozás (max. 200 MB) átlépése miatt";
                        responseXML = "";
                        return;
                    }

                    soapXMLMessage = getUploadXML(filename, buffer, "xadesUpload");
                    AVDH_URL = AVDH_URL + AVDH_XAdES;
                }
                else if (formatum == "asice")
                {
                    if (buffer.Length > 209715200)
                    {
                        RaiseError = true;
                        errorMessage = "A kért fájlok AVDH-s aláírása nem lehetséges, a fájl méret korlátozás (max. 200 MB) átlépése miatt";
                        responseXML = "";
                        return;
                    }

                    soapXMLMessage = getUploadXML(filename, buffer, "asicUpload");
                    AVDH_URL = AVDH_URL + AVDH_ASiC;
                }
                else
                {
                    return;
                }

                responseXML = postXMLData(AVDH_URL, soapXMLMessage);

                if (responseXML.Contains("Fault"))
                {
                    RaiseError = true;
                    errorMessage = extractDataFromXMLNode("SOAP-ENV:Fault", "", responseXML);
                    responseXML = "";
                }
                else
                {
                    RaiseError = false;

                    UploadResponseXML = responseXML;// extractDataFromXMLNode("response", "", responseXML);
                    //responseXML = "";

                }
            }
            catch (Exception ex)
            {
                RaiseError = true;
                errorMessage = ex.Message;
            }
        }


        public void UploadPDF(string filename, string formatum, string externalLink, string spUser, string spPW, string spDomain)
        {
            IsFileLocal = false;
            System.Net.WebRequest request = System.Net.WebRequest.Create(externalLink);

            if (!String.IsNullOrEmpty(spUser) && !String.IsNullOrEmpty(spPW) && !String.IsNullOrEmpty(spDomain))
            { request.Credentials = new System.Net.NetworkCredential(spUser, spPW, spDomain); }
            else
            { request.Credentials = new System.Net.NetworkCredential(); }

            try
            {
                System.Net.WebResponse response = request.GetResponse();
                System.IO.Stream s = response.GetResponseStream();
                MemoryStream repeaterMS = new MemoryStream();
                CopyStream(s, repeaterMS);
                byte[] buf = repeaterMS.ToArray();

                s.Close();

                UploadPDF(filename, formatum, buf);
            }
            catch (Exception ex)
            {
                RaiseError = true;
                errorMessage = ex.Message;
            }
        }

        public void DownloadSignedPDF(string url)
        {
            try
            {
                responseXML = postXMLData(url, "");

                if (responseXML.Contains("Fault"))
                {
                    RaiseError = true;
                    errorMessage = extractDataFromXMLNode("SOAP-ENV:Fault", "", responseXML);
                    responseXML = "";
                }
                else
                {
                    RaiseError = false;
                    UploadResponseXML = extractDataFromXMLNode("return", "", responseXML);
                    responseXML = "";
                }

            }
            catch (Exception ex)
            {
                RaiseError = true;
                errorMessage = ex.Message;
            }
        }

        #region private methods

        private string getUploadXML(string fileName, byte[] buffer, string uploadType)
        {
            string Base64Filestream_String = "";

            try
            {
                Base64Filestream_String = System.Convert.ToBase64String(buffer);
            }
            catch
            {

            }
            string xmlString = @"<?xml version=""1.0"" encoding=""UTF-8""?>" +
                               @"<S:Envelope xmlns:S=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                               @"<S:Body>" +
                               @"<ns2:" + uploadType + @"xmlns:ns2=""http://service.web.avdh.gca.microsec.hu/"" >" +
                               @"<documents>" +
                               @"<docBytes>" + Base64Filestream_String + "</docBytes>" +
                               @"<fileName>" + fileName + "</fileName>" +
                               @"</documents>+
                               @</ns2:" + uploadType + @"></S:Body></S:Envelope>";


            return xmlString;

        }



        private string postXMLData(string destinationUrl, string requestXml)
        {
            string responseStr = @"<?xml version=""1.0"" encoding=""UTF-8""?>" +
                                 @"<S:Envelope xmlns:S=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                                 @"<S:Header/><S:Body>" +
                                 @"<ns2:padesUploadResponse xmlns:ns2 = ""http://service.web.avdh.gca.microsec.hu/"">" +
                                 @"<response>" +
                                 @"<avdhCertURL>https://avdh-cert.gca.intra.microsec.hu/avdh/login?target=avdh2-dc71afa4-ec1c-4c69-bff5-d04a9d59a2c8</avdhCertURL>" +
                                 @"<avdhDownloadURL>https://avdh.gca.intra.microsec.hu/avdh/download?documentId=avdh2-dc71afa4-ec1c-4c69-bff5-d04a9d59a2c8</avdhDownloadURL>" +
                                 @"<avdhKauLoginRequestUrl>https://avdh.gca.intra.microsec.hu/avdh/kau-login-request?target=avdh2-dc71afa4-ec1c-4c69-bff5-d04a9d59a2c8</avdhKauLoginRequestUrl>" +
                                 @"<avdhKauLogoutRequestUrl>https://avdh.gca.intra.microsec.hu/avdh/kau-logoutrequest?target=szakrendszer1</avdhKauLogoutRequestUrl>" +
                                 @"<avdhUrl>https://avdh.gca.intra.microsec.hu/avdh/login?target=avdh2-dc71afa4-ec1c-4c69-bff5-d04a9d59a2c8</avdhUrl>" +
                                 @"<target>avdh2-dc71afa4-ec1c-4c69-bff5-d04a9d59a2c8</target>" +
                                 @"<ugyfelkapuLoginURL>https://teszt.gate.gov.hu/sso/ap/ApServlet?partnerid=microsecavdh&amp;target=avdh2-dc71afa4-ec1c-4c69-bff5-d04a9d59a2c8</ugyfelkapuLoginURL>" +
                                 @"<ugyfelkapuLogoutURL>https://teszt.gate.gov.hu/sso/ap/logout?partnerid=microsecavdh&amp;target=szakrendszer1</ugyfelkapuLogoutURL>" +
                                 @"</response>" +
                                 @"</ns2:padesUploadResponse>" +
                                 @"</S:Body>" +
                                 @"</S:Envelope>";
            return responseStr;

            //System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            //if (!String.IsNullOrEmpty(requestXml))
            //{
            //    byte[] bytes;
            //    bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
            //    request.ContentType = "text/xml; encoding='utf-8'";
            //    request.ContentLength = bytes.Length;
            //    request.Method = "POST";
            //    Stream requestStream = request.GetRequestStream();
            //    requestStream.Write(bytes, 0, bytes.Length);
            //    requestStream.Close();
            //}

            //HttpWebResponse response;
            //response = (HttpWebResponse)request.GetResponse();
            //if (response.StatusCode == HttpStatusCode.OK)
            //{
            //    Stream responseStream = response.GetResponseStream();
            //    string responseStr = new StreamReader(responseStream).ReadToEnd();
            //    return responseStr;
            //}
            //return null;
        }



        public string extractDataFromXMLNode(string xPath, string field, string rawXMLString)
        {
            string fieldValue = "";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(rawXMLString);

            if (String.IsNullOrEmpty(field))
            {
                XmlNodeList nodes = xmlDoc.GetElementsByTagName(xPath);
                foreach (XmlNode _item in nodes)
                {
                    fieldValue += _item.InnerText;
                }
            }
            else
            {
                XmlNodeList nodes = xmlDoc.GetElementsByTagName(xPath);
                XmlNode node = xmlDoc.SelectSingleNode("//" + field);
                if (node != null)
                {
                    fieldValue = node.InnerText;
                }
            }

            return fieldValue;
        }

        private static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[32768];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
        }

        private static string toBase64String(string input)
        {
            byte[] bytes = Encoding.Default.GetBytes(input);
            try
            {
                return System.Convert.ToBase64String(bytes);
            }
            catch
            {
                return input;
            }
        }

        private static string fromBase64String(string input)
        {
            try
            {
                return Encoding.Default.GetString(System.Convert.FromBase64String(input));
            }
            catch
            {
                return input;
            }
        }

        public static string HTMLDecode(string input)
        {
            return HttpUtility.UrlDecode(input.Replace("\\", "%"));
        }
        #region XML átalakítása bulletpointos felsorolássá (kivéve, nem kell)
        /*private static string formatSignatureXML(string xml)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(xml);
            List<string> signatureDataList = new List<string>();
            foreach (XmlNode node in xDoc)
            {
                if (node.Name.ToLower() == "xml")
                    continue;
                recursiveNodeSearch(node, signatureDataList);
            }
            return string.Join(Environment.NewLine, signatureDataList.ToArray());
        }

        private static void recursiveNodeSearch(XmlNode node, List<string> list_toload)
        {
            string formatString = new String('\t', getParentNodeCount(node, 0));
            if (node.ChildNodes.Count != 0)
            {
                foreach(XmlNode _item in node.ChildNodes)
                {
                    if (_item.ChildNodes.Count == 1 && _item.ChildNodes[0].Name.ToLower() == "#text")
                    {
                        list_toload.Add(formatString + "• " + _item.Name + ": " + _item.InnerText);
                    }
                    else
                    {
                        list_toload.Add(formatString + "• " + _item.Name);
                        recursiveNodeSearch(_item, list_toload);
                    }
                }
            }
            else
            {
                list_toload.Add(formatString + "• " + node.Name + ": " + node.InnerText);
            }
        }

        private static int getParentNodeCount(XmlNode node, int count)
        {
            int _return = 0;
            if (node.ParentNode.Name != "#document")
            {
                count += 1;
                _return = getParentNodeCount(node.ParentNode, count);
            }
            else
            {
                _return = count;
            }
            return _return;
        }*/
        #endregion

        #endregion
    }
}