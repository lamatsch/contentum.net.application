﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUtility
{
    public class BusinessService
    {
        private string _Type = "";

        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        private string _Url = "";

        public string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }
        private string _Authentication = "";

        public string Authentication
        {
            get { return _Authentication; }
            set { _Authentication = value; }
        }
        private string _UserDomain = "";

        public string UserDomain
        {
            get { return _UserDomain; }
            set { _UserDomain = value; }
        }
        private string _UserName = "";

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        private string _Password = "";

        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

    }
}
