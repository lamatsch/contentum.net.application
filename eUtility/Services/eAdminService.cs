using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Data;

namespace Contentum.eUtility
{
    public class eAdminService
    {
        //public class BusinessService
        //{
        //    private string _Type = "";

        //    public string Type
        //    {
        //        get { return _Type; }
        //        set { _Type = value; }
        //    }
        //    private string _Url = "";

        //    public string Url
        //    {
        //        get { return _Url; }
        //        set { _Url = value; }
        //    }
        //    private string _Authentication = "";

        //    public string Authentication
        //    {
        //        get { return _Authentication; }
        //        set { _Authentication = value; }
        //    }
        //    private string _UserDomain = "";

        //    public string UserDomain
        //    {
        //        get { return _UserDomain; }
        //        set { _UserDomain = value; }
        //    }
        //    private string _UserName = "";

        //    public string UserName
        //    {
        //        get { return _UserName; }
        //        set { _UserName = value; }
        //    }
        //    private string _Password = "";

        //    public string Password
        //    {
        //        get { return _Password; }
        //        set { _Password = value; }
        //    }

        //}

        public static Contentum.eAdmin.Service.ServiceFactory GetServiceFactory(BusinessService BS)
        {
            return GeteAdminServiceFactory(BS);
        }

        public static Contentum.eAdmin.Service.ServiceFactory ServiceFactory
        {
            get { return GeteAdminServiceFactory(); }
        }

        private static Contentum.eAdmin.Service.ServiceFactory GeteAdminServiceFactory()
        {
            BusinessService BS = new BusinessService();
            BS.Type = UI.GetAppSetting("eAdminBusinessServiceType");
            BS.Url = UI.GetAppSetting("eAdminBusinessServiceUrl");
            BS.Authentication = UI.GetAppSetting("eAdminBusinessServiceAuthentication");
            BS.UserDomain = UI.GetAppSetting("eAdminBusinessServiceUserDomain");
            BS.UserName = UI.GetAppSetting("eAdminBusinessServiceUserName");
            BS.Password = UI.GetAppSetting("eAdminBusinessServicePassword");
            return GeteAdminServiceFactory(BS);
        }

        private static Contentum.eAdmin.Service.ServiceFactory GeteAdminServiceFactory(BusinessService BS)
        {
            Contentum.eAdmin.Service.ServiceFactory sc = new Contentum.eAdmin.Service.ServiceFactory();
            sc.BusinessServiceType = BS.Type;
            sc.BusinessServiceUrl = BS.Url;
            sc.BusinessServiceAuthentication = BS.Authentication;
            sc.BusinessServiceUserDomain = BS.UserDomain;
            sc.BusinessServiceUserName = BS.UserName;
            sc.BusinessServicePassword = BS.Password;
            return sc;
        }

        public static string GetFelhasznaloNevById(string Felhasznalo_Id, Page page)
        {
            return FelhasznaloNevek_Cache.GetFelhasznaloNevFromCache(Felhasznalo_Id, page);
        }
    }
}
