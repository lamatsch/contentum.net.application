﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;

namespace Contentum.eUtility
{
    public class eDocumentService
    {
        //public class BusinessService
        //{
        //    private string _Type = "";

        //    public string Type
        //    {
        //        get { return _Type; }
        //        set { _Type = value; }
        //    }
        //    private string _Url = "";

        //    public string Url
        //    {
        //        get { return _Url; }
        //        set { _Url = value; }
        //    }
        //    private string _Authentication = "";

        //    public string Authentication
        //    {
        //        get { return _Authentication; }
        //        set { _Authentication = value; }
        //    }
        //    private string _UserDomain = "";

        //    public string UserDomain
        //    {
        //        get { return _UserDomain; }
        //        set { _UserDomain = value; }
        //    }
        //    private string _UserName = "";

        //    public string UserName
        //    {
        //        get { return _UserName; }
        //        set { _UserName = value; }
        //    }
        //    private string _Password = "";

        //    public string Password
        //    {
        //        get { return _Password; }
        //        set { _Password = value; }
        //    }

        //}

        public static Contentum.eDocument.Service.ServiceFactory GetServiceFactory(BusinessService BS)
        {
            return GeteDocumentServiceFactory(BS);
        }

        public static Contentum.eDocument.Service.ServiceFactory ServiceFactory
        {
            get { return GeteDocumentServiceFactory(); }
        }

        private static Contentum.eDocument.Service.ServiceFactory GeteDocumentServiceFactory()
        {
            BusinessService BS = new BusinessService();
            BS.Type = UI.GetAppSetting("eDocumentBusinessServiceType");
            BS.Url = UI.GetAppSetting("eDocumentBusinessServiceUrl");
            BS.Authentication = UI.GetAppSetting("eDocumentBusinessServiceAuthentication");
            BS.UserDomain = UI.GetAppSetting("eDocumentBusinessServiceUserDomain");
            BS.UserName = UI.GetAppSetting("eDocumentBusinessServiceUserName");
            BS.Password = UI.GetAppSetting("eDocumentBusinessServicePassword");
            return GeteDocumentServiceFactory(BS);
        }

        private static Contentum.eDocument.Service.ServiceFactory GeteDocumentServiceFactory(BusinessService BS)
        {
            Contentum.eDocument.Service.ServiceFactory sc = new Contentum.eDocument.Service.ServiceFactory();
            sc.BusinessServiceType = BS.Type;
            sc.BusinessServiceUrl = BS.Url;
            sc.BusinessServiceAuthentication = BS.Authentication;
            sc.BusinessServiceUserDomain = BS.UserDomain;
            sc.BusinessServiceUserName = BS.UserName;
            sc.BusinessServicePassword = BS.Password;
            return sc;
        }

        /// <summary>
        /// Egy megadott ekezetes stringet konvertal at ekezettelenne.
        /// A space helyere _ (sutty) jelet rak.
        /// 
        /// Ezt a targyszavak SPS kompatibilesse tetelehez hasznaljuk.
        /// 
        /// A megvalositas eleg fapados, de hirtelen ez jutott eszembe.
        /// </summary>
        /// <param name="forras">Az atkonveralando string.</param>
        /// <returns>String visszateresi ertek.</returns>
        ///
        public static string ConvertToIdentifier(string forras)
        {
            char[] mit = { (char)' ', (char)'á', (char)'é', (char)'ű', (char)'û', (char)'ő', (char)'ô', (char)'ú', (char)'ö', (char)'ü', (char)'ó', (char)'í',
                                      (char)'Á', (char)'É', (char)'Ű', (char)'Û', (char)'Ő', (char)'Ô', (char)'Ú', (char)'Ö', (char)'Ü', (char)'Ó', (char)'Í' };
            char[] mire = { (char)'_', (char)'a', (char)'e', (char)'u', (char)'u', (char)'o', (char)'o', (char)'u', (char)'o', (char)'u', (char)'o', (char)'i',
                                       (char)'A', (char)'E', (char)'U', (char)'U', (char)'O', (char)'O', (char)'U', (char)'O', (char)'U', (char)'O', (char)'I' };

            string eredmeny = "";

            foreach (char item in forras.ToCharArray())
            {

                int idx = -1;
                int i = 0;
                while (idx == -1 && i < mit.Length)
                {
                    if (mit[i].Equals(item))
                        idx = i;

                    i++;
                }

                char b = (idx == -1) ? item : mire[idx];

                eredmeny += System.Convert.ToString(b);
            }

            return eredmeny;
        }

        public static string getDataFromExternalInfo(string External_Info, int part)
        {
            string[] parts = External_Info.Split(new string[1] { ";" }, StringSplitOptions.None);
            string _partReturn = "";
            if (parts.Length != 5)
            {
                throw new Exception("Hibás formátumú External_Info mező");
            }
            if (part < parts.Length)
            {
                _partReturn = parts[part];
            }
            /*
            spsMachineName = parts[0];
            rootSiteCollectionUrl = parts[1];
            doktarSitePath = parts[2];
            doktarDocLibPath = parts[3];
            doktarFolderPath = parts[4];
            fileName = fileName;
             */
            return _partReturn;
        }

        public static Result UploadSignedFile(ExecParam execParam, string iratId, string recordId, string documentSite, string documentStore, string documentFolder, string filename, byte[] documentData)
        {
            //Contentum.eDocument.Service.DocumentService spService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();

            //Result spUploadResult = spService.DirectUploadWithCheckin(execParam.Clone(), "SharePoint", documentSite, documentStore, documentFolder, filename, documentData, "2");

            ExecParam execParamUpload = execParam.Clone();
            EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            execParamUpload.Record_Id = iratId;

            EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
            erec_Csatolmanyok.Updated.SetValueAll(false);
            erec_Csatolmanyok.Base.Updated.SetValueAll(false);

            erec_Csatolmanyok.IraIrat_Id = iratId;
            erec_Csatolmanyok.Updated.IraIrat_Id = true;

            Csatolmany csatolmany = new Csatolmany();
            csatolmany.Tartalom = documentData;
            csatolmany.Nev = filename;

            Result result = erec_IraIratokService.CsatolmanyUpload(execParamUpload, erec_Csatolmanyok, csatolmany);


            if (result.IsError)
            {
                Logger.Error(String.Format("spUpload hiba: " + result.ErrorMessage));
                throw new Contentum.eUtility.ResultException(String.Format("spUpload hiba: " + result.ErrorMessage));
            }
            else
            {
                // CR 3033 Aláírás felülvizsgálat bejegyzés - nekrisz
                Contentum.eDocument.Service.KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                ExecParam docExecParam = execParam.Clone();
                docExecParam.Record_Id = recordId;
                Result dokResult = dokService.Get(docExecParam);
                if (!dokResult.IsError)
                {
                    KRT_Dokumentumok docRecord = (KRT_Dokumentumok)dokResult.Record;
                    docRecord.Updated.SetValueAll(false);
                    docRecord.Base.Updated.SetValueAll(false);
                    docRecord.Base.Updated.Ver = true;

                    docRecord.AlairasFelulvizsgalat = System.DateTime.Now.ToString();
                    docRecord.Updated.AlairasFelulvizsgalat = true;

                    docRecord.ElektronikusAlairas = KodTarak.DOKUMENTUM_ALAIRAS_PKI.Ervenyes_alairas;
                    docRecord.Updated.ElektronikusAlairas = true;

                    Result result_krt_doc_alairas = dokService.Update(docExecParam, docRecord);
                    if (!String.IsNullOrEmpty(result_krt_doc_alairas.ErrorCode))
                    {
                        Logger.Error(String.Format("KRT_Dokumentumok update hiba: " + result_krt_doc_alairas.ErrorMessage));
                        throw new Contentum.eUtility.ResultException(String.Format("KRT_Dokumentumok update hiba: " + result_krt_doc_alairas.ErrorMessage));
                    }                    
                }
            }

            return result;
        }

        public static Result SetFolyamatTetelStatus(ExecParam execParam, string id, string status, string hiba)
        {
            if (String.IsNullOrEmpty(id))
                return null;

            Result res = new Result();
            try
            {
                EREC_Alairas_Folyamat_TetelekService service = eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
                execParam.Record_Id = id;

                res = service.Get(execParam);

                if (!res.IsError)
                {
                    EREC_Alairas_Folyamat_Tetelek record = res.Record as EREC_Alairas_Folyamat_Tetelek;

                    record.Updated.SetValueAll(false);
                    record.Base.Updated.SetValueAll(false);
                    record.Base.Updated.Ver = true;

                    record.AlairasStatus = status;
                    record.Updated.AlairasStatus = true;

                    record.Hiba = hiba;
                    record.Updated.Hiba = true;

                    res = service.Update(execParam, record);

                }
            }
            catch (Exception ex)
            {
                res = Contentum.eUtility.ResultException.GetResultFromException(ex);
            }

            return res;
        }

        public static Result SetFolyamatStatus(ExecParam execParam, string id, string status)
        {
            if (String.IsNullOrEmpty(id))
                return null;

            Result res = new Result();
            try
            {
                EREC_Alairas_FolyamatokService service = eRecordService.ServiceFactory.GetEREC_Alairas_FolyamatokService();
                execParam.Record_Id = id;

                res = service.Get(execParam);

                if (!res.IsError)
                {
                    EREC_Alairas_Folyamatok record = res.Record as EREC_Alairas_Folyamatok;

                    record.Updated.SetValueAll(false);
                    record.Base.Updated.SetValueAll(false);
                    record.Base.Updated.Ver = true;

                    record.AlairasStatus = status;
                    record.Updated.AlairasStatus = true;

                    record.AlairasStopped = DateTime.Now.ToString();
                    record.Updated.AlairasStopped = true;

                    res = service.Update(execParam, record);

                }
            }
            catch (Exception ex)
            {
                res = Contentum.eUtility.ResultException.GetResultFromException(ex);
            }

            return res;
        }

        public static Result SetFolyamatTetelStatus(ExecParam execParam, string procId, string iratId, string csatolmanyId, string status, string hiba)
        {
            if (String.IsNullOrEmpty(procId) || String.IsNullOrEmpty(iratId) || String.IsNullOrEmpty(csatolmanyId))
                return null;

            Result res = new Result();
            try
            {
                EREC_Alairas_Folyamat_TetelekService service = eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
                EREC_Alairas_Folyamat_TetelekSearch search = new EREC_Alairas_Folyamat_TetelekSearch();
                search.AlairasFolyamat_Id.Value = procId;
                search.AlairasFolyamat_Id.Operator = Query.Operators.equals;
                search.IraIrat_Id.Value = iratId;
                search.IraIrat_Id.Operator = Query.Operators.equals;
                search.Csatolmany_Id.Value = csatolmanyId;
                search.Csatolmany_Id.Operator = Query.Operators.equals;

                res = service.GetAll(execParam, search);

                if (!res.IsError)
                {
                    foreach (DataRow row in res.Ds.Tables[0].Rows)
                    {
                        EREC_Alairas_Folyamat_Tetelek record = new EREC_Alairas_Folyamat_Tetelek();

                        record.Updated.SetValueAll(false);
                        record.Base.Updated.SetValueAll(false);

                        record.Base.Ver = row["Ver"].ToString();
                        record.Base.Updated.Ver = true;

                        record.AlairasStatus = status;
                        record.Updated.AlairasStatus = true;

                        record.Hiba = hiba;
                        record.Updated.Hiba = true;

                        execParam.Record_Id = row["Id"].ToString();


                        res = service.Update(execParam, record);
                    }

                }
            }
            catch (Exception ex)
            {
                res = Contentum.eUtility.ResultException.GetResultFromException(ex);
            }

            return res;
        }

        private static string getFelulvizsgalatEredmenyString(string eredmenycode)
        {
            string result = KodTarak.TOMEGES_ALAIRAS_FELULVIZSGALAT_EREDMENY.sikertelen_felulvizsgalat;
            if (!string.IsNullOrEmpty(eredmenycode))
            {
                if (eredmenycode.Equals(KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikeres_ellenorzes))
                {
                    result = KodTarak.TOMEGES_ALAIRAS_FELULVIZSGALAT_EREDMENY.sikeres_felulvizsgalat;
                }
            }
            return result;
        }

        // BLG_1639
        public static Result SetFolyamatTetelNote(ExecParam execParam, string id, string note)
        {
            if (String.IsNullOrEmpty(id))
                return null;

            Result res = new Result();
            try
            {
                EREC_Alairas_Folyamat_TetelekService service = eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
                execParam.Record_Id = id;

                res = service.Get(execParam);

                if (!res.IsError)
                {
                    EREC_Alairas_Folyamat_Tetelek record = res.Record as EREC_Alairas_Folyamat_Tetelek;

                    record.Updated.SetValueAll(false);
                    record.Base.Updated.SetValueAll(false);
                    record.Base.Updated.Ver = true;

                    record.Base.Note = note;
                    record.Base.Updated.Note = true;

                    res = service.Update(execParam, record);

                }
            }
            catch (Exception ex)
            {
                res = Contentum.eUtility.ResultException.GetResultFromException(ex);
            }

            return res;
        }

        #region BLG 2947
        public static Result SetAlairasFelulvizsgalatEredmenyFromXML(ExecParam execParam, string docId, XmlDocument doc)
        {
            string eredmeny = string.Empty;
            try
            {
                eredmeny = JsonConvert.SerializeXmlNode(doc);
            }
            catch (Exception) { }
            if (string.IsNullOrEmpty(eredmeny))
            {
                return SetAlairasFelulvizsgalatEredmenyFromStatusCode(execParam, docId, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes);
            }
            return SetAlairasFelulvizsgalatEredmeny(execParam, docId, eredmeny);
        }

        public static Result SetAlairasFelulvizsgalatEredmenyFromStatusCode(ExecParam execParam, string docId, string statusCode)
        {
            string eredmeny = string.Empty;
            // JSON összeállítása
            StringBuilder sbVerify = new StringBuilder();
            StringWriter swVerify = new StringWriter(sbVerify);
            using (JsonWriter writerVerify = new JsonTextWriter(swVerify))
            {
                writerVerify.Formatting = Newtonsoft.Json.Formatting.Indented;
                writerVerify.WriteStartObject();
                writerVerify.WritePropertyName("AlairasFelulvizsgalatEredmeny");
                writerVerify.WriteValue(getFelulvizsgalatEredmenyString(eredmeny));
                writerVerify.WriteEndObject();
            }

            eredmeny = sbVerify.ToString();

            return SetAlairasFelulvizsgalatEredmeny(execParam, docId, eredmeny);
        }

        public static Result SetAlairasFelulvizsgalatEredmeny(ExecParam execParam, string docId, string eredmeny)
        {
            Result res = new Result();
            try
            {
                Contentum.eDocument.Service.KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                ExecParam docExecParam = execParam.Clone();
                docExecParam.Record_Id = docId;
                Result dokResult = dokService.Get(docExecParam);
                if (!dokResult.IsError)
                {
                    KRT_Dokumentumok docRecord = (KRT_Dokumentumok)dokResult.Record;
                    docRecord.Updated.SetValueAll(false);
                    docRecord.Base.Updated.SetValueAll(false);
                    docRecord.Base.Updated.Ver = true;

                    docRecord.AlairasFelulvizsgalat = System.DateTime.Now.ToString();
                    docRecord.Updated.AlairasFelulvizsgalat = true;

                    docRecord.AlairasFelulvizsgalatEredmeny = eredmeny;
                    docRecord.Updated.AlairasFelulvizsgalatEredmeny = true;

                    Result result_krt_doc_alairas = dokService.Update(docExecParam, docRecord);
                    if (!String.IsNullOrEmpty(result_krt_doc_alairas.ErrorCode))
                    {
                        Logger.Error(String.Format("KRT_Dokumentumok update hiba: " + result_krt_doc_alairas.ErrorMessage));
                        return result_krt_doc_alairas;
                    }
                }
                
            }
            catch (Exception ex)
            {
                res = Contentum.eUtility.ResultException.GetResultFromException(ex);
            }
            return res;
        }
        #endregion

        #region BUG 6889 - alairas utan státusz állítás
        public static Result SetStatusAfterServerSideSign(ExecParam execParam, string iratId, string recordId, string megjegyzes)
        {
            ExecParam execParamUpload = execParam.Clone();
            EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            execParamUpload.Record_Id = iratId;

            Result resultIrat = erec_IraIratokService.Get(execParamUpload);
            if (resultIrat == null || resultIrat.IsError)
            {
                if (resultIrat != null)
                {
                    Logger.Error(String.Format("Server oldali aláírás, iratkeresés hiba: " + resultIrat.ErrorMessage));
                }
                else
                {
                    Logger.Error(String.Format("Server oldali aláírás, iratkeresés hiba: resultIrat meghatározása sikertelen"));
                }
                return resultIrat;
            }
            else
            {
                Contentum.eDocument.Service.KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                ExecParam docExecParam = execParam.Clone();
                docExecParam.Record_Id = recordId;
                Result dokResult = dokService.Get(docExecParam);
                if (dokResult == null || dokResult.IsError)
                {
                    if (dokResult != null)
                    {
                        Logger.Error(String.Format("Server oldali aláírás, dokumentum keresési hiba: " + dokResult.ErrorMessage));
                    }
                    else
                    {
                        Logger.Error(String.Format("Server oldali aláírás, dokumentum keresési hiba: dokResult meghatározása sikertelen."));
                    }
                    return dokResult;
                }
                else
                {
                    KRT_Dokumentumok docRecord = (KRT_Dokumentumok)dokResult.Record;
                    docRecord.Updated.SetValueAll(false);
                    docRecord.Base.Updated.SetValueAll(false);
                    docRecord.Base.Updated.Ver = true;

                    docRecord.AlairasFelulvizsgalat = System.DateTime.Now.ToString();
                    docRecord.Updated.AlairasFelulvizsgalat = true;

                    docRecord.ElektronikusAlairas = KodTarak.DOKUMENTUM_ALAIRAS_PKI.Ervenyes_alairas;
                    docRecord.Updated.ElektronikusAlairas = true;

                    Result result_krt_doc_alairas = dokService.Update(docExecParam, docRecord);
                    if (result_krt_doc_alairas == null || result_krt_doc_alairas.IsError)
                    {
                        if (result_krt_doc_alairas != null)
                        {
                            Logger.Error(String.Format("KRT_Dokumentumok update hiba: " + result_krt_doc_alairas.ErrorMessage));
                        }
                        else
                        {
                            Logger.Error(String.Format("KRT_Dokumentumok update hiba: result_krt_doc_alairas meghatározása sikertelen."));
                        }
                        return result_krt_doc_alairas;
                    }
                }
            }

            #region irat_Alairok
            EREC_IratAlairokService service_IratAlairok = eRecordService.ServiceFactory.GetEREC_IratAlairokService();

            EREC_IratAlairokSearch search_iratAlairok = new EREC_IratAlairokSearch();

            // Keresés: az adott irathoz a felhasznalo az aláíró, és állapot aláírandó

            search_iratAlairok.Obj_Id.Value = iratId;
            search_iratAlairok.Obj_Id.Operator = Query.Operators.equals;

            search_iratAlairok.FelhasznaloCsoport_Id_Alairo.Value = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            search_iratAlairok.FelhasznaloCsoport_Id_Alairo.Operator = Query.Operators.equals;
            search_iratAlairok.FelhasznaloCsoport_Id_Alairo.Group = "alairo";
            search_iratAlairok.FelhasznaloCsoport_Id_Alairo.GroupOperator = Query.Operators.or;

            search_iratAlairok.FelhaszCsop_Id_HelyettesAlairo.Value = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            search_iratAlairok.FelhaszCsop_Id_HelyettesAlairo.Operator = Query.Operators.equals;
            search_iratAlairok.FelhaszCsop_Id_HelyettesAlairo.Group = "alairo";
            search_iratAlairok.FelhaszCsop_Id_HelyettesAlairo.GroupOperator = Query.Operators.or;

            search_iratAlairok.Allapot.Value = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
            search_iratAlairok.Allapot.Operator = Query.Operators.equals;

            Result result_IratAlairokGetAll = service_IratAlairok.GetAll(execParam.Clone(), search_iratAlairok);
            if (result_IratAlairokGetAll == null || result_IratAlairokGetAll.IsError)
            {
                if (result_IratAlairokGetAll != null)
                {
                    Logger.Error(String.Format("IratAlairok keresés hiba: " + result_IratAlairokGetAll.ErrorMessage));
                }
                else
                {
                    Logger.Error(String.Format("IratAlairok Keresés hiba: result_IratAlairokGetAll meghatározása sikertelen."));
                }
                return result_IratAlairokGetAll;
            }

            if (result_IratAlairokGetAll.Ds.Tables[0].Rows.Count > 0)
            {
                DataRow row_IratAlairok = result_IratAlairokGetAll.Ds.Tables[0].Rows[0];

                string iratAlairok_Id = row_IratAlairok["Id"].ToString();
                string iratAlairok_Ver = row_IratAlairok["Ver"].ToString();

                EREC_IratAlairok iratAlairok = new EREC_IratAlairok();
                iratAlairok.Updated.SetValueAll(false);
                iratAlairok.Base.Updated.SetValueAll(false);
                
                // Állapot állítása aláírt-ra:
                iratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
                iratAlairok.Updated.Allapot = true;

                //iratAlairok.FelhasznaloCsoport_Id_Alairo = execParam.Felhasznalo_Id;
                //iratAlairok.Updated.FelhasznaloCsoport_Id_Alairo = true;

                iratAlairok.FelhaszCsoport_Id_Helyettesito = execParam.LoginUser_Id;
                iratAlairok.Updated.FelhaszCsoport_Id_Helyettesito = true;

                iratAlairok.AlairasDatuma = DateTime.Now.ToString();
                iratAlairok.Updated.AlairasDatuma = true;

                // Megjegyzés
                iratAlairok.Leiras = megjegyzes;
                iratAlairok.Updated.Leiras = true;

                // Verzió megadása:
                iratAlairok.Base.Ver = iratAlairok_Ver;
                iratAlairok.Base.Updated.Ver = true;

                ExecParam execParam_iratAlairokUpdate = execParam.Clone();
                execParam_iratAlairokUpdate.Record_Id = iratAlairok_Id;

                Result result_iratAlairokUpdate = service_IratAlairok.Update(execParam_iratAlairokUpdate, iratAlairok);
                if (result_iratAlairokUpdate.IsError)
                {
                    if (result_iratAlairokUpdate != null)
                    {
                        Logger.Error(String.Format("IratAlairok update hiba: " + result_iratAlairokUpdate.ErrorMessage));
                    }
                    else
                    {
                        Logger.Error(String.Format("IratAlairok update hiba: result_iratAlairokUpdate meghatározása sikertelen."));
                    }
                    return result_iratAlairokUpdate;
                }
            }
            #endregion

            return resultIrat;
        }
        #endregion

    }
}
