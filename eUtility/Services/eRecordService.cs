using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUtility
{
    public class eRecordService
    {
        //public class BusinessService
        //{
        //    private string _Type = "";

        //    public string Type
        //    {
        //        get { return _Type; }
        //        set { _Type = value; }
        //    }
        //    private string _Url = "";

        //    public string Url
        //    {
        //        get { return _Url; }
        //        set { _Url = value; }
        //    }
        //    private string _Authentication = "";

        //    public string Authentication
        //    {
        //        get { return _Authentication; }
        //        set { _Authentication = value; }
        //    }
        //    private string _UserDomain = "";

        //    public string UserDomain
        //    {
        //        get { return _UserDomain; }
        //        set { _UserDomain = value; }
        //    }
        //    private string _UserName = "";

        //    public string UserName
        //    {
        //        get { return _UserName; }
        //        set { _UserName = value; }
        //    }
        //    private string _Password = "";

        //    public string Password
        //    {
        //        get { return _Password; }
        //        set { _Password = value; }
        //    }

        //}

        public static Contentum.eRecord.Service.ServiceFactory GetServiceFactory(BusinessService BS)
        {
            return GeteRecordServiceFactory(BS);
        }

        public static Contentum.eRecord.Service.ServiceFactory ServiceFactory
        {
            get { return GeteRecordServiceFactory(); }
        }

        private static Contentum.eRecord.Service.ServiceFactory GeteRecordServiceFactory()
        {
            BusinessService BS = new BusinessService();
            BS.Type = UI.GetAppSetting("eRecordBusinessServiceType");
            BS.Url = UI.GetAppSetting("eRecordBusinessServiceUrl");
            BS.Authentication = UI.GetAppSetting("eRecordBusinessServiceAuthentication");
            BS.UserDomain = UI.GetAppSetting("eRecordBusinessServiceUserDomain");
            BS.UserName = UI.GetAppSetting("eRecordBusinessServiceUserName");
            BS.Password = UI.GetAppSetting("eRecordBusinessServicePassword");
            return GeteRecordServiceFactory(BS);
        }

        private static Contentum.eRecord.Service.ServiceFactory GeteRecordServiceFactory(BusinessService BS)
        {
            Contentum.eRecord.Service.ServiceFactory sc = new Contentum.eRecord.Service.ServiceFactory();
            sc.BusinessServiceType = BS.Type;
            sc.BusinessServiceUrl = BS.Url;
            sc.BusinessServiceAuthentication = BS.Authentication;
            sc.BusinessServiceUserDomain = BS.UserDomain;
            sc.BusinessServiceUserName = BS.UserName;
            sc.BusinessServicePassword = BS.Password;
            return sc;
        }

        //public static Contentum.eRecord.Service.ServiceFactory ServiceFactory
        //{
        //    get { return GeteRecordServiceFactory(); }
        //}

        //private static Contentum.eRecord.Service.ServiceFactory GeteRecordServiceFactory()
        //{
        //    Contentum.eRecord.Service.ServiceFactory sc = new Contentum.eRecord.Service.ServiceFactory();
        //    sc.BusinessServiceType = UI.GetAppSetting("eRecordBusinessServiceType");
        //    sc.BusinessServiceUrl = UI.GetAppSetting("eRecordBusinessServiceUrl");
        //    sc.BusinessServiceAuthentication = UI.GetAppSetting("eRecordBusinessServiceAuthentication");
        //    sc.BusinessServiceUserDomain = UI.GetAppSetting("eRecordBusinessServiceUserDomain");
        //    sc.BusinessServiceUserName = UI.GetAppSetting("eRecordBusinessServiceUserName");
        //    sc.BusinessServicePassword = UI.GetAppSetting("eRecordBusinessServicePassword");
        //    return sc;
        //}
    }
}
