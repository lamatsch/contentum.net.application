using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eUtility
{
    public class eTemplateManagerService
    {
        public class BusinessService
        {
            private string _Type = "";

            public string Type
            {
                get { return _Type; }
                set { _Type = value; }
            }
            private string _Url = "";

            public string Url
            {
                get { return _Url; }
                set { _Url = value; }
            }
            private string _Authentication = "";

            public string Authentication
            {
                get { return _Authentication; }
                set { _Authentication = value; }
            }
            private string _UserDomain = "";

            public string UserDomain
            {
                get { return _UserDomain; }
                set { _UserDomain = value; }
            }
            private string _UserName = "";

            public string UserName
            {
                get { return _UserName; }
                set { _UserName = value; }
            }
            private string _Password = "";

            public string Password
            {
                get { return _Password; }
                set { _Password = value; }
            }

        }

        public static Contentum.eTemplateManager.Service.ServiceFactory GetServiceFactory(BusinessService BS)
        {
            return GeteTemplateManagerServiceFactory(BS);
        }

        public static Contentum.eTemplateManager.Service.ServiceFactory ServiceFactory
        {
            get { return GeteTemplateManagerServiceFactory(); }
        }

        private static Contentum.eTemplateManager.Service.ServiceFactory GeteTemplateManagerServiceFactory()
        {
            BusinessService BS = new BusinessService();
            BS.Type = UI.GetAppSetting("eTemplateManagerBusinessServiceType");
            BS.Url = UI.GetAppSetting("eTemplateManagerBusinessServiceUrl");
            BS.Authentication = UI.GetAppSetting("eTemplateManagerBusinessServiceAuthentication");
            BS.UserDomain = UI.GetAppSetting("eTemplateManagerBusinessServiceUserDomain");
            BS.UserName = UI.GetAppSetting("eTemplateManagerBusinessServiceUserName");
            BS.Password = UI.GetAppSetting("eTemplateManagerBusinessServicePassword");
            return GeteTemplateManagerServiceFactory(BS);
        }

        private static Contentum.eTemplateManager.Service.ServiceFactory GeteTemplateManagerServiceFactory(BusinessService BS)
        {
            Contentum.eTemplateManager.Service.ServiceFactory sc = new Contentum.eTemplateManager.Service.ServiceFactory();
            sc.BusinessServiceType = BS.Type;
            sc.BusinessServiceUrl = BS.Url;
            sc.BusinessServiceAuthentication = BS.Authentication;
            sc.BusinessServiceUserDomain = BS.UserDomain;
            sc.BusinessServiceUserName = BS.UserName;
            sc.BusinessServicePassword = BS.Password;
            return sc;
        }
    
    }
}
