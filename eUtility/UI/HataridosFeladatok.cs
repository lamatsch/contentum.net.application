﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace Contentum.eUtility
{
    public class HataridosFeladatok
    {
        public class Forras
        {
        }

        public static bool IsDisabled(Page page)
        {
            bool isDisabled = Rendszerparameterek.GetBoolean(page, Rendszerparameterek.FELADAT_KEZELES_DISABLED, false);
            return isDisabled;
        }

        public static bool IsFeladatJelzoIkonEnabledOnList(Page page)
        {
            return !IsDisabled(page);
        }

        public static bool IsFeladatJelzoIkonEnabledOnForm(Page page)
        {
            return !IsDisabled(page);
        }

        public static bool IsFeladatJelzoIkonEnabledOnTerkep(Page page)
        {
            return !IsDisabled(page);
        }

        public static bool IsFeladatFunctionKeyEnabled(Page page)
        {
            return !IsDisabled(page);
        }
    }
}
