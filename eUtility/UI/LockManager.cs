using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contentum.eUtility
{
    public class LockManager
    {
        /// <summary>
        /// Z�rolja a megadott GridView kijel�lt sorait;
        /// Jogosults�got ellen�rzi: vagy nincs hozz� jogosults�ga, vagy pedig jogosults�gt�l f�gg�en
        /// Lock vagy ForceLock v�grehajt�sa
        /// </summary>
        /// <param name="gridview"></param>
        /// <param name="tableName"></param>
        /// <param name="funkcioNev_Lock"></param>
        /// <param name="funkcioNev_ForceLock"></param>
        /// <param name="page"></param>
        /// <param name="errorPanel"></param>
        /// <param name="errorUpdatePanel"></param>
        public static void LockSelectedGridViewRecords(GridView gridview, string tableName
            , string funkcioNev_Lock, string funkcioNev_ForceLock
            , Page page, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            UI ui = new UI();
            List<string> selectedItemsList = ui.GetGridViewSelectedRows(gridview, errorPanel, errorUpdatePanel);

            LockSelectedRecords(selectedItemsList, tableName, funkcioNev_Lock, funkcioNev_ForceLock
                , page, errorPanel, errorUpdatePanel);
        }


        public static void LockSelectedRecords(List<string> selectedItemsList, string tableName
            , string funkcioNev_Lock, string funkcioNev_ForceLock
            , Page page, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (FunctionRights.GetFunkcioJog(page, funkcioNev_Lock))
            {
                //UI ui = new UI();
                //List<string> selectedItemsList = ui.GetGridViewSelectedRows(gridview, errorPanel, errorUpdatePanel);
                Contentum.eAdmin.Service.LockManagerService service = eAdminService.ServiceFactory.GetLockManagerService();
                Contentum.eBusinessDocuments.ExecParam[] execParams = new Contentum.eBusinessDocuments.ExecParam[selectedItemsList.Count];
                for (int i = 0; i < selectedItemsList.Count; i++)
                {
                    execParams[i] = UI.SetExecParamDefault(page, new Contentum.eBusinessDocuments.ExecParam());
                    execParams[i].Record_Id = selectedItemsList[i];
                }
                Contentum.eBusinessDocuments.Result result = null;
                if (FunctionRights.GetFunkcioJog(page, funkcioNev_ForceLock))
                {
                    // ha van 'ForceLock' funkci�jogosults�ga -> ForceLock, egy�bk�nt sima Lock
                    result = service.ForceMultiLockRecord(execParams, tableName);
                }
                else
                {
                    result = service.MultiLockRecord(execParams, tableName);
                }

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_LockRecord"));
                    errorUpdatePanel.Update();
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(page);
            }
        }

        /// <summary>
        /// Z�rolja a megadott Id-t;
        /// Jogosults�got ellen�rzi: vagy nincs hozz� jogosults�ga, vagy pedig jogosults�gt�l f�gg�en
        /// Lock vagy ForceLock v�grehajt�sa
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="tableName"></param>
        /// <param name="funkcioNev_Lock"></param>
        /// <param name="funkcioNev_ForceLock"></param>
        /// <param name="page"></param>
        /// <param name="errorPanel"></param>
        /// <param name="errorUpdatePanel"></param>
        public static void LockSelectedGridViewRecords(String Id, string tableName
            , string funkcioNev_Lock, string funkcioNev_ForceLock
            , Page page, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (FunctionRights.GetFunkcioJog(page, funkcioNev_Lock))
            {
                Contentum.eAdmin.Service.LockManagerService service = eAdminService.ServiceFactory.GetLockManagerService();
                Contentum.eBusinessDocuments.ExecParam execParam = UI.SetExecParamDefault(page, new Contentum.eBusinessDocuments.ExecParam());
                execParam.Record_Id = Id;

                Contentum.eBusinessDocuments.Result result = null;
                if (FunctionRights.GetFunkcioJog(page, funkcioNev_ForceLock))
                {
                    // ha van 'ForceLock' funkci�jogosults�ga -> ForceLock, egy�bk�nt sima Lock
                    result = service.ForceLockRecord(execParam, tableName);
                }
                else
                {
                    result = service.LockRecord(execParam, tableName);
                }

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_LockRecord"));
                    errorUpdatePanel.Update();
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(page);
            }
        }


        public static void UnlockSelectedGridViewRecords(GridView gridview, string tableName
            , string funkcioNev_Lock, string funkcioNev_ForceLock
            , Page page, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            UI ui = new UI();
            List<string> selectedItemsList = ui.GetGridViewSelectedRows(gridview, errorPanel, errorUpdatePanel);

            UnlockSelectedRecords(selectedItemsList, tableName, funkcioNev_Lock, funkcioNev_ForceLock
                , page, errorPanel, errorUpdatePanel);
        }

        public static void UnlockSelectedRecords(List<string> selectedItemsList, string tableName
            , string funkcioNev_Lock, string funkcioNev_ForceLock
            , Page page, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (FunctionRights.GetFunkcioJog(page, funkcioNev_Lock))
            {
                //UI ui = new UI();
                //List<string> selectedItemsList = ui.GetGridViewSelectedRows(gridview, errorPanel, errorUpdatePanel);
                Contentum.eAdmin.Service.LockManagerService service = eAdminService.ServiceFactory.GetLockManagerService();
                Contentum.eBusinessDocuments.ExecParam[] execParams = new Contentum.eBusinessDocuments.ExecParam[selectedItemsList.Count];
                for (int i = 0; i < selectedItemsList.Count; i++)
                {
                    execParams[i] = UI.SetExecParamDefault(page, new Contentum.eBusinessDocuments.ExecParam());
                    execParams[i].Record_Id = selectedItemsList[i];
                }
                Contentum.eBusinessDocuments.Result result = null;
                if (FunctionRights.GetFunkcioJog(page, funkcioNev_ForceLock))
                {
                    // ha van 'ForceLock' funkci�jogosults�ga -> ForceUnlock, egy�bk�nt sima Unlock
                    result = service.ForceMultiUnlockRecord(execParams, tableName);
                }
                else
                {
                    result = service.MultiUnlockRecord(execParams, tableName);
                }

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_UnlockRecord"));
                    errorUpdatePanel.Update();
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(page);
            }
        }

        public static void UnlockSelectedGridViewRecords(String Id, string tableName
            , string funkcioNev_Lock, string funkcioNev_ForceLock
            , Page page, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (FunctionRights.GetFunkcioJog(page, funkcioNev_Lock))
            {
                Contentum.eAdmin.Service.LockManagerService service = eAdminService.ServiceFactory.GetLockManagerService();
                Contentum.eBusinessDocuments.ExecParam execParam = UI.SetExecParamDefault(page, new Contentum.eBusinessDocuments.ExecParam());
                execParam.Record_Id = Id;
                Contentum.eBusinessDocuments.Result result = null;
                if (FunctionRights.GetFunkcioJog(page, funkcioNev_ForceLock))
                {
                    // ha van 'ForceLock' funkci�jogosults�ga -> ForceUnlock, egy�bk�nt sima Unlock
                    result = service.ForceUnlockRecord(execParam, tableName);
                }
                else
                {
                    result = service.UnlockRecord(execParam, tableName);
                }

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_UnlockRecord"));
                    errorUpdatePanel.Update();
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(page);
            }
        }
    
    }
}
