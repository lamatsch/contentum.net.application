using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Contentum.eUtility
{
    public class PageView
    {
        // Ezzel le lehet tiltani a n�zetek haszn�lat�t:
        // TODO: Rendszerparam�terb�l kellene tudni ki/bekapcsolni
        private bool PageViewEnabled
        {
            get
            {
                return Rendszerparameterek.GetBoolean(_ParentPage, Rendszerparameterek.PAGE_VIEW_ENABLED, false);
            }
        }

        private string krt_Parameterek_PAGE_VIEW_CUSTOM_TAB_ORDER = "0";
        protected StateBag _ViewState = null;
        protected Page _ParentPage = null;
        private string Command = null;
        private IComponentSelectControl compSelector = null;

        public IComponentSelectControl CompSelector
        {
            get { return compSelector; }
            set { compSelector = value; }
        }

        public enum Status
        {
            ReadOnly = 1,
            Disabled = 2,
            Invisible = 3
        }

        private bool enabled = true;

        public bool Enabled
        {
            get
            {
                return enabled && PageViewEnabled;
            }
            set
            {
                enabled = value;
            }
        }

        public PageView(Page ParentPage, StateBag ViewState)
        {
            _ViewState = ViewState;
            _ParentPage = ParentPage;
        }

        public PageView(Page ParentPage, StateBag ViewState, bool addComponentSelector)
            : this(ParentPage, ViewState)
        {
            if (addComponentSelector)
            {
                Command = ParentPage.Request.QueryString.Get(CommandName.Command) ?? String.Empty;

                ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());

                krt_Parameterek_PAGE_VIEW_CUSTOM_TAB_ORDER = Rendszerparameterek.Get(execParam, "PAGE_VIEW_CUSTOM_TAB_ORDER");

                if (Command == CommandName.DesignView)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(ParentPage, CommandName.DesignView);
                    ParentPage.Load += new EventHandler(ParentPage_Load);
                    LoadCompSelector();
                }
                else
                {
                    ParentPage.PreRender += new EventHandler(ParentPage_PreRender);
                }
            }
        }

        private void LoadCompSelector()
        {
            Control c = _ParentPage.LoadControl("~/Component/ComponentSelectControl.ascx");
            this.compSelector = c as IComponentSelectControl;
            _ParentPage.Form.Controls.Add(c);

        }


        void ParentPage_PreRender(object sender, EventArgs e)
        {
            if (Command != CommandName.DesignView)
            {
                this.SetViewOnPage(Command);
            }
        }

        void ParentPage_Load(object sender, EventArgs e)
        {
            if (Command == CommandName.DesignView)
            {
                if (_ParentPage is UI.PageBase)
                {
                    UI.PageBase _baseParentPage = _ParentPage as UI.PageBase;
                    _baseParentPage.Load_ComponentSelectModul();

                }
            }
        }

        public Boolean GetUpdatedByView(Control WebControl)
        {
            String _ReadOnlyControls = null;
            String _DisabledControls = null;
            string _InvisibleControls = null;
            if (_ViewState["ReadOnlyControls"] != null)
            {
                _ReadOnlyControls = _ViewState["ReadOnlyControls"].ToString();
            }
            if (_ViewState["DisabledControls"] != null)
            {
                _DisabledControls = _ViewState["DisabledControls"].ToString();
            }
            if (_ViewState["InvisibleControls"] != null)
            {
                _InvisibleControls = _ViewState["InvisibleControls"].ToString();
            }

            if (_ReadOnlyControls != null && _ReadOnlyControls.Contains(WebControl.UniqueID))
                return false;

            if (_DisabledControls != null && _DisabledControls.Contains(WebControl.UniqueID))
                return false;

            if (_InvisibleControls != null && _InvisibleControls.Contains(WebControl.UniqueID))
                return false;

            return true;
        }

        public System.Web.UI.Control FindControlIterative(System.Web.UI.Control root, string id)
        {
            System.Web.UI.Control ctl = root;
            var ctls = new LinkedList<System.Web.UI.Control>();

            while (ctl != null)
            {
                if (ctl.ClientID == id)
                    return ctl;
                foreach (System.Web.UI.Control child in ctl.Controls)
                {
                    if (child.ClientID == id)
                        return child;
                    if (child.HasControls())
                        ctls.AddLast(child);
                }
                if (ctls.First != null)
                {
                    ctl = ctls.First.Value;
                    ctls.Remove(ctl);
                }
                else return null;
            }
            return null;
        }

        public void SetTabIndex(string tabIndexes)
        {
            if (string.IsNullOrEmpty(tabIndexes)) return;

            try
            {
                string[] temp1 = tabIndexes.Split(',');

                foreach (var key in temp1)
                {
                    string[] temp2 = key.Split('|');

                    //var control = _ParentPage.FindControl(temp2[0]);
                    var control = FindControlIterative(_ParentPage, temp2[0]);
                    
                    if (control != null)
                    {
                        var webControl = control as WebControl;
                        if (webControl != null)
                        {
                            webControl.TabIndex = short.Parse(temp2[1]);
                        }
                    }
                }
            }
            catch
            {

            }
        }

        public void SetViewOnPage(string MuveletKod)
        {
            // TODO: Rendszerparam�terb�l kellene tudni ki/bekapcsolni a n�zetek haszn�lat�t
            if (!Enabled)
            {
                return;
            }

            KRT_Nezetek nezet = NezetekCache.GetNezet(_ParentPage, MuveletKod);

            if (nezet != null)
            {
                if (krt_Parameterek_PAGE_VIEW_CUSTOM_TAB_ORDER == "1")
                {
                    SetTabIndex(nezet.TabIndexControls);
                }

                SetControlsByStatus(nezet.DisableControls, Status.Disabled);
                SetControlsByStatus(nezet.ReadOnlyControls, Status.ReadOnly);
                SetControlsByStatus(nezet.InvisibleControls, Status.Invisible);

                _ViewState["DisabledControls"] = nezet.DisableControls;
                _ViewState["ReadOnlyControls"] = nezet.ReadOnlyControls;
                _ViewState["InvisibleControls"] = nezet.InvisibleControls;
            }

        }

        private void SetControlsByStatus(String Controls, Status status)
        {
            // TODO: Rendszerparam�terb�l kellene tudni ki/bekapcsolni a n�zetek haszn�lat�t
            if (!Enabled)
            {
                return;
            }

            //        String loadcomp = Nev.TextBox.UniqueID + "," + Orszag_TextBox.UniqueID + "," + ErvenyessegCalendarControl1.UniqueID + "," + Tipus_DropDownList.UniqueID + "," + Divizio_CheckBox.UniqueID;
            String[] loadcomparray = Controls.Split(',');

            foreach (string comp in loadcomparray)
            {
                Control c = _ParentPage.FindControl(comp);
                if (c != null)
                {
                    //string type = c.GetType().BaseType.BaseType.ToString();
                    if (status == Status.Invisible)
                    {
                        c.Visible = false;
                        continue;
                    }
                    System.Reflection.PropertyInfo p = null;
                    if (c is ISelectableUserComponent)
                    {
                        ISelectableUserComponent selectableUserComponent = c as ISelectableUserComponent;
                        if (status == Status.ReadOnly)
                        {
                            //p = c.GetType().GetProperty("ViewEnabled");
                            //if (p != null)
                            //{
                            //    p.SetValue(c, false, null);
                            //}
                            selectableUserComponent.ViewEnabled = false;
                        }
                        if (status == Status.Disabled)
                        {
                            //p = c.GetType().GetProperty("ViewVisible");
                            //if (p != null)
                            //{
                            //    p.SetValue(c, false, null);
                            //}
                            selectableUserComponent.ViewVisible = false;
                        }
                    }
                    else if (c is WebControl)
                    {
                        WebControl wc = c as WebControl;
                        if (status == Status.Disabled)
                        {
                            wc.Enabled = false;
                            wc.CssClass += " ViewDisabledWebControl";

                        }
                        if (status == Status.ReadOnly)
                        {
                            p = c.GetType().GetProperty("ReadOnly");
                            if (p != null)
                            {
                                p.SetValue(c, true, null);
                            }
                            else
                            {
                                wc.Enabled = false;
                            }
                            wc.CssClass += " ViewReadOnlyWebControl";
                        }

                        if (wc is Image ||
                            wc is ImageButton)
                        {
                            UI.AddCssClass(wc, "disableditem");
                        }
                    }
                }
            }
        }

        public static IEnumerable<Control> GetSelectableChildComponents(ControlCollection controls)
        {
            return GetSelectableChildComponents(controls, null);
        }

        public static IEnumerable<Control> GetSelectableChildComponents(ControlCollection controls, List<Control> excludedControls)
        {
            if (controls != null && controls.Count > 0)
            {
                foreach (Control control in controls)
                {
                    if (control.ID != null && !IsControlExcluded(control, excludedControls))
                    {

                        if (control is ISelectableUserComponent
                            || control is ISelectableUserComponentContainer)
                        {
                            yield return control;
                            continue;
                        }

                        if (control is WebControl)
                        {
                            yield return control;
                        }

                        if (control is HtmlControl)
                        {
                            yield return control;
                        }

                    }

                    foreach (Control childControl in GetSelectableChildComponents(control.Controls, excludedControls))
                    {
                        yield return childControl;
                    }

                }
            }
        }

        private static bool IsControlExcluded(Control control, List<Control> excludedControls)
        {
            if (excludedControls == null)
                return false;

            return excludedControls.Contains(control);
        }

        public class NezetekCache
        {
            private static readonly object _syncPage = new Object();
            private static readonly object _syncMuvelet = new Object();

            private const int _cacheExpirationHours = 8;
            private const string _nezetekCacheName = "Nezetek";

            private static string GetPageName(Page page)
            {
                return Path.GetFileName(page.Request.Path);
            }

            private static Dictionary<string, Dictionary<string, KRT_Nezetek>> GetCache(Page page)
            {
                Dictionary<string, Dictionary<string, KRT_Nezetek>> ret = null;
                ExecParam execParam = UI.SetExecParamDefault(page);

                try
                {
                    if (String.IsNullOrEmpty(execParam.Org_Id))
                    {
                        Logger.Debug("String.IsNullOrEmpty(execParam.Org_Id)");
                        string orgId = FelhasznaloNevek_Cache.GetOrgByFelhasznalo(execParam.Felhasznalo_Id, page.Cache);
                        Logger.Debug("execParam.Org_Id=" + orgId ?? "NULL");
                        execParam.Org_Id = orgId;
                    }


                    OrgSeperatedDictionary<string, Dictionary<string, KRT_Nezetek>> _nezetekCache = (OrgSeperatedDictionary<string, Dictionary<string, KRT_Nezetek>>)page.Cache.Add
                                    (_nezetekCacheName, new OrgSeperatedDictionary<string, Dictionary<string, KRT_Nezetek>>(), null
                                    , DateTime.Now.AddHours(_cacheExpirationHours), System.Web.Caching.Cache.NoSlidingExpiration
                                    , System.Web.Caching.CacheItemPriority.NotRemovable, null);

                    if (_nezetekCache == null)
                    {
                        _nezetekCache = (OrgSeperatedDictionary<string, Dictionary<string, KRT_Nezetek>>)page.Cache[_nezetekCacheName];
                    }

                    ret = _nezetekCache.Get(execParam.Org_Id);
                }
                catch (Exception ex)
                {
                    Logger.Error(String.Format("NezetekCache.GetCache({0}) hiba", execParam.Org_Id ?? "NULL"),
                        execParam, ResultException.GetResultFromException(ex));
                }

                return ret;

            }

            private static Dictionary<string, KRT_Nezetek> GetNezetek(Page page)
            {
                Dictionary<string, KRT_Nezetek> ret = null;
                string pageName = GetPageName(page);

                try
                {
                    Dictionary<string, Dictionary<string, KRT_Nezetek>> _nezetekCache = GetCache(page);

                    if (_nezetekCache != null)
                    {
                        if (!_nezetekCache.ContainsKey(pageName))
                        {
                            lock (_syncPage)
                            {
                                if (!_nezetekCache.ContainsKey(pageName))
                                {
                                    _nezetekCache.Add(pageName, new Dictionary<string, KRT_Nezetek>());
                                }
                            }
                        }

                        ret = _nezetekCache[pageName];
                    }
                    else
                    {
                        Logger.Error("NezetekCache.GetNezetek: _nezetekCache == null");
                    }
                }
                catch (Exception ex)
                {
                    ExecParam execParam = UI.SetExecParamDefault(page);
                    Logger.Error(String.Format("NezetekCache.GetNezetek({0}) hiba", pageName),
                        execParam, ResultException.GetResultFromException(ex));
                }

                return ret;

            }

            public static bool RemoveNezetek(Page page)
            {
                bool ret = false;
                string pageName = GetPageName(page);

                try
                {
                    Dictionary<string, Dictionary<string, KRT_Nezetek>> _nezetekCache = GetCache(page);

                    if (_nezetekCache != null)
                    {
                        if (_nezetekCache.ContainsKey(pageName))
                        {
                            lock (_syncPage)
                            {
                                if (_nezetekCache.ContainsKey(pageName))
                                {
                                    ret = _nezetekCache.Remove(pageName);
                                }
                            }
                        }
                    }
                    else
                    {
                        Logger.Error("NezetekCache.RemoveNezetek: _nezetekCache == null");
                    }
                }
                catch (Exception ex)
                {
                    ExecParam execParam = UI.SetExecParamDefault(page);
                    Logger.Error(String.Format("NezetekCache.RemoveNezetek({0}) hiba", pageName),
                        execParam, ResultException.GetResultFromException(ex));
                }

                return ret;
            }

            public static KRT_Nezetek GetNezet(Page page, string MuveletKod)
            {
                KRT_Nezetek ret = null;
                if (String.IsNullOrEmpty(MuveletKod)) MuveletKod = "Empty";

                try
                {

                    Dictionary<string, KRT_Nezetek> _nezetek = GetNezetek(page);

                    if (_nezetek != null)
                    {

                        if (!_nezetek.ContainsKey(MuveletKod))
                        {
                            lock (_syncMuvelet)
                            {
                                if (!_nezetek.ContainsKey(MuveletKod))
                                {
                                    KRT_Nezetek nezet = GetNezetFromDatabase(page, MuveletKod);

                                    _nezetek.Add(MuveletKod, nezet);
                                }
                            }
                        }

                        ret = _nezetek[MuveletKod];
                    }
                    else
                    {
                        Logger.Error("NezetekCache.GetNezet: _nezetek == null");
                    }
                }
                catch (Exception ex)
                {
                    ExecParam execParam = UI.SetExecParamDefault(page);
                    string pageName = GetPageName(page);
                    Logger.Error(String.Format("NezetekCache.RemoveNezetek({0}, {1}) hiba", pageName, MuveletKod),
                        execParam, ResultException.GetResultFromException(ex));
                }

                return ret;
            }

            private static KRT_Nezetek GetNezetFromDatabase(Page page, string MuveletKod)
            {

                Contentum.eBusinessDocuments.ExecParam ExecParam = UI.SetExecParamDefault(page);

                string pageName = GetPageName(page);

                Contentum.eAdmin.Service.KRT_NezetekService nezetekservice = eAdminService.ServiceFactory.GetKRT_NezetekService();
                Contentum.eQuery.BusinessDocuments.KRT_NezetekSearch nezeteksearch = new Contentum.eQuery.BusinessDocuments.KRT_NezetekSearch(true);

                nezeteksearch.Extended_KRT_ModulokSearch.Kod.Value = pageName;
                nezeteksearch.Extended_KRT_ModulokSearch.Kod.Operator = Contentum.eQuery.Query.Operators.equals;

                if (MuveletKod != "Empty")
                {
                    nezeteksearch.Extended_KRT_MuveletekSearch.Kod.Value = MuveletKod;
                    nezeteksearch.Extended_KRT_MuveletekSearch.Kod.Operator = Contentum.eQuery.Query.Operators.isnullorequals;
                }
                else
                {
                    nezeteksearch.Extended_KRT_MuveletekSearch.Kod.Operator = Contentum.eQuery.Query.Operators.isnull;
                }

                nezeteksearch.OrderBy = "Prioritas DESC";
                nezeteksearch.TopRow = 1;

                Result _res = nezetekservice.GetAllWithFK(ExecParam, nezeteksearch);

                if (_res.IsError)
                {
                    throw new ResultException(_res);
                }

                //nincs n�zet
                if (_res.Ds.Tables[0].Rows.Count == 0)
                {
                    return null;
                }

                DataRow rowNezet = _res.Ds.Tables[0].Rows[0];

                KRT_Nezetek nezet = new KRT_Nezetek();
                nezet.DisableControls = rowNezet["DisableControls"].ToString();
                nezet.ReadOnlyControls = rowNezet["ReadOnlyControls"].ToString();
                nezet.InvisibleControls = rowNezet["InvisibleControls"].ToString();
                nezet.TabIndexControls = rowNezet["TabIndexControls"].ToString();
                return nezet;
            }
        }
    }
}
