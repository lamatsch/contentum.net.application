﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace Contentum.eUtility
{
    public static class Test
    {
        /// <summary>
        /// Teszt oldalak ebből származnak
        /// </summary>
        public class BaseTestPage : System.Web.UI.Page
        {
            protected override void OnPreInit(EventArgs e)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "TestPage", "window.testpage = 'TestPage';", true);
                base.OnPreInit(e);
            }
        }

        /// <summary>
        /// Tesztelési funkciót tartalmazó komponensek
        /// </summary>
        public interface ITestComponent
        {
            /// <summary>
            /// if (Page is Contentum.Utility.Test.BaseTestPage)
            ///     return true;
            /// return false;
            /// </summary>
            bool IsOnTestPage
            {
                get;
            }
        }
    }
}
