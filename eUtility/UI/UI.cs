﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Reflection;
using Contentum.eBusinessDocuments;
using System.Collections.Specialized;
using System.Threading;
using System.Globalization;
using System.Security.Permissions;
using System.Collections;
using System.Web.Script.Serialization;
using Contentum.eRecord.Service;
using System.IO;
using System.Configuration;

[assembly: SecurityPermission(SecurityAction.RequestMinimum, ControlThread = true)]
namespace Contentum.eUtility
{

    public class UI
    {
        private const string DisabledImageUrlPostfix = "_dis";

        public static void SwapImageToDisabled(System.Web.UI.WebControls.ImageButton imagecontrol)
        {
            if (imagecontrol == null) return;
            if (imagecontrol.Page.IsPostBack) return;
            if (!imagecontrol.Enabled)
            {
                int pont = imagecontrol.ImageUrl.LastIndexOf('.');
                imagecontrol.ImageUrl = imagecontrol.ImageUrl.Substring(0, pont)
                    + DisabledImageUrlPostfix + imagecontrol.ImageUrl.Substring(pont);
            }
        }

        public static void SetImageButtonStyleToDisabled(System.Web.UI.WebControls.ImageButton imageButton)
        {
            if (imageButton != null)
            {
                AddCssClass(imageButton, "disableditem");
            }
        }

        public static void SetImageButtonStyleToEnabled(System.Web.UI.WebControls.ImageButton imageButton)
        {
            if (imageButton != null)
            {
                RemoveCssClass(imageButton, "disableditem");
            }
        }

        /// <summary>
        /// Javascripttel tiltja le a komponens láthatóságát (display: none )
        /// </summary>
        public static void SetComponentToJSNonVisible(System.Web.UI.WebControls.WebControl webcontrol)
        {
            webcontrol.Style["display"] = "none";
        }

        public static string GetAppSetting(string Name)
        {
            string _ret;
            _ret = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(Name);
            if (_ret == null)
                _ret = "";
            return _ret;
        }

        public static bool GetAppSetting(string Name, bool defaultValue)
        {
            string value = GetAppSetting(Name);
            if (!String.IsNullOrEmpty(value))
            {
                bool ret;
                if (Boolean.TryParse(value, out ret))
                {
                    return ret;
                }
            }

            return defaultValue;
        }

        public static string GetSession(Page ParentPage, string Name)
        {
            string _ret = "";
            if (ParentPage == null || String.IsNullOrEmpty(Name)) return _ret;

            return GetSession(ParentPage.Session, Name);
        }

        public static string GetSession(System.Web.SessionState.HttpSessionState Session, string Name)
        {
            string _ret = "";
            if (Session == null || String.IsNullOrEmpty(Name)) return _ret;

            if (Session[Name] != null)
            {
                _ret = Session[Name].ToString();
            }

            return _ret;
        }

        public void SetClientScriptToGridViewSelectDeSelectButton(GridView ParentGridView)
        {
            if (ParentGridView == null) return;
            if (ParentGridView.Rows.Count > 0)
            {
                //(ParentGridView.HeaderRow.FindControl("SelectingRowsImageButton") as ImageButton).Attributes["onClick"] = "__doPostBack('" + (ParentGridView.HeaderRow.FindControl("SelectingRowsImageButton") as ImageButton).UniqueID + "','');return false;";

                //(ParentGridView.HeaderRow.FindControl("DeSelectingRowsImageButton") as ImageButton).Attributes["onClick"] = "__doPostBack('" + (ParentGridView.HeaderRow.FindControl("DeSelectingRowsImageButton") as ImageButton).UniqueID + "','');return false;";

                ImageButton sib = (ParentGridView.HeaderRow.FindControl("SelectingRowsImageButton") as ImageButton);
                if (sib != null)
                    sib.Attributes["onClick"] = "SelectAllCheckBox('" + ParentGridView.ClientID + "','check');return false";
                else
                {
                    Logger.Error("Sorkiválasztó gomb nem talalhato !!!");
                    //throw new Exception("Sorkiválasztó gomb nem talalhato !!!");
                }

                ImageButton desib = (ParentGridView.HeaderRow.FindControl("DeSelectingRowsImageButton") as ImageButton);
                if (desib != null)
                    desib.Attributes["onClick"] = "DeSelectAllCheckBox('" + ParentGridView.ClientID + "','check');return false;";
                else
                {
                    Logger.Error("Sorkiválasztást törlõ gomb nem talalhato !!!");
                    // throw new Exception("Sorkiválasztást törlõ gomb nem talalhato !!!");
                }
            }
        }

        public void GridViewSelectAllCheckBox(GridView gridView)
        {
            if (gridView == null) return;
            for (int i = 0; i < gridView.Rows.Count; i++)
            {
                CheckBox x = (CheckBox)gridView.Rows[i].FindControl("check");
                x.Checked = true;
            }
        }

        public void GridViewDeSelectAllCheckBox(GridView gridView)
        {
            if (gridView == null) return;
            for (int i = 0; i < gridView.Rows.Count; i++)
            {
                CheckBox x = (CheckBox)gridView.Rows[i].FindControl("check");
                x.Checked = false;
            }
        }

        /// <summary>
        /// A GridView-ban a kijelölt sorhoz tartozó CheckBoxot Checked-re állítja, az összes többit UnChecked-re
        /// </summary>
        /// <param name="gridView"></param>
        /// <param name="selectedRowNumber">A kijelölt sor száma (0 - ...)</param>
        /// <param name="checkBox_Id"></param>
        public static void SetGridViewCheckBoxesToSelectedRow(GridView gridView, int selectedRowNumber, String checkBox_Id)
        {
            GridViewRow selectedRow = gridView.Rows[selectedRowNumber];
            if (selectedRow != null)
            {
                // Összes CheckBoxot unchecked-re állítjuk:
                foreach (GridViewRow row in gridView.Rows)
                {
                    CheckBox ch = (CheckBox)row.FindControl(checkBox_Id);
                    if (ch != null)
                    {
                        ch.Checked = false;
                    }
                }
                // kijelölt sor checkboxát checked-re állítjuk:
                CheckBox selected_ch = (CheckBox)selectedRow.FindControl(checkBox_Id);
                if (selected_ch != null && selected_ch.Enabled)
                {
                    selected_ch.Checked = true;
                }
            }
        }


        /// <summary>
        /// A vezérlõt tartalmazó GridView megkeresése
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public static GridView FindParentGridView(Control control)
        {
            Control parent = control;
            while (parent.GetType() != typeof(GridView))
            {
                parent = parent.Parent;
                if ((parent == null) || (parent.GetType() == typeof(Page)))
                    return null;
            }
            return (GridView)parent;
        }

        public static int GetTopRow(Page ParentPage)
        {
            if (ParentPage == null) return 0;
            if (ParentPage.Session[Constants.TopRow] != null)
            {
                if (!String.IsNullOrEmpty(ParentPage.Session[Constants.TopRow].ToString()))
                    return Int32.Parse(ParentPage.Session[Constants.TopRow].ToString());
                else
                    return 0;
            }

            return 0;
        }

        public static void GridViewSetScrollable(bool Scrollable, AjaxControlToolkit.CollapsiblePanelExtender CPE)
        {
            if (CPE == null) return;

            CPE.ScrollContents = Scrollable;
            if (Scrollable)
            {
                int height = Rendszerparameterek.GetInt(CPE.Page, Rendszerparameterek.SCROLL_PANEL_HEIGHT);
                if (height <= 0)
                {
                    height = 200;
                }
                CPE.ExpandedSize = height; // grid magassaga, ha scrollable
            }
            else
            {
                CPE.ExpandedSize = 0; // grid magassaga, ha nem scrollable
            }
        }

        public static void SetLovListGridViewScrollable(GridView gridView, AjaxControlToolkit.CollapsiblePanelExtender CPE)
        {
            SetLovListGridViewScrollable(gridView, CPE, 15);
        }

        public static void SetLovListGridViewScrollable(GridView gridView, AjaxControlToolkit.CollapsiblePanelExtender CPE, int rowCount)
        {
            if (CPE == null) return;
            else
            {
                if (gridView.Rows.Count > rowCount)
                {
                    CPE.ScrollContents = true;
                    CPE.ExpandedSize = 280 - ((15 - rowCount) * (280 / 15));
                }
                else
                {
                    CPE.ScrollContents = false;
                    CPE.ExpandedSize = 0;
                }
            }
        }

        public static SortDirection GetSortToGridView(String GridViewName, StateBag ViewState, String NewSortExpression)
        {
            SortDirection sort = SortDirection.Ascending;

            if (GridViewName == null || ViewState == null || NewSortExpression == null) return sort;

            if (ViewState[GridViewName + "SortExpression"] != null)
            {
                if (ViewState[GridViewName + "SortExpression"].ToString() == NewSortExpression)
                {
                    if (ViewState[GridViewName + "SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        sort = SortDirection.Descending;
                    else
                        sort = SortDirection.Ascending;
                }
            }
            return sort;
        }


        public static void GridView_RowDataBound_SetLockingInfo(GridViewRowEventArgs e, Page page)
        {
            GridView_RowDataBound_SetLockingInfo(e, page, "Zarolo_id", "ZarolasIdo");
        }

        public static void GridView_RowDataBound_SetLockingInfo(GridViewRowEventArgs e, Page page
            , string columnName_Zarolo_id, string columnName_ZarolasIdo)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

                if (drv != null)
                {
                    DataRow row = drv.Row;
                    Image lockImage = (Image)e.Row.FindControl("LockedImage");
                    SetLockingInfo(row, page, lockImage, columnName_Zarolo_id, columnName_ZarolasIdo);
                }

            }
        }

        public static void SetLockingInfo(DataRow row, Page page, Image lockImage)
        {
            SetLockingInfo(row, page, lockImage, "Zarolo_id", "ZarolasIdo");
        }

        public static void SetLockingInfo(DataRow row, Page page
            , Image lockImage, string columnName_Zarolo_id, string columnName_ZarolasIdo)
        {
            if (row == null || page == null || lockImage == null) return;

            string lockingUserId = String.Empty;

            if (row.Table.Columns.Contains(columnName_Zarolo_id))
            {
                lockingUserId = row[columnName_Zarolo_id].ToString();
            }
            else lockingUserId = String.Empty;

            if (!String.IsNullOrEmpty(lockingUserId))
            {

                string lockingUserName = FelhasznaloNevek_Cache.GetFelhasznaloNevFromCache(lockingUserId, page);
                lockImage.AlternateText = Contentum.eUtility.Resources.List.GetString("UI_RecordIsLockedBy") + " " + lockingUserName;

                string zarolasIdo = row[columnName_ZarolasIdo].ToString();
                lockImage.AlternateText += "\n" + Contentum.eUtility.Resources.List.GetString("UI_LockingTime") + " " + zarolasIdo;

                lockImage.ToolTip = lockImage.AlternateText;

                string userId = FelhasznaloProfil.FelhasznaloId(page);
                if (userId == lockingUserId)
                {
                    lockImage.ImageUrl = "~/images/hu/egyeb/locked_green.gif";
                }
                else
                {
                    lockImage.ImageUrl = "~/images/hu/egyeb/locked_red.gif";
                }
                lockImage.Visible = true;
            }
            else
            {
                lockImage.Visible = false;
            }
        }


        #region GridViewUtils
        public static int GetGridViewColumnIndex(GridView gridView, string dataFieldName)
        {
            for (int i = 0; i < gridView.Columns.Count; i++)
            {
                DataControlField field = gridView.Columns[i];

                BoundField boundfield = field as BoundField;

                if (boundfield != null && boundfield.DataField == dataFieldName)
                {
                    return i;
                }
            }
            return -1;
        }

        public static string GetGridViewColumnText(GridViewRow row, string dataFieldName)
        {
            GridView gridView = row.NamingContainer as GridView;
            if (gridView != null)
            {
                int index = GetGridViewColumnIndex(gridView, dataFieldName);
                if (index != -1)
                    return row.Cells[index].Text;
            }
            return "";
        }


        public static bool isGridViewCheckBoxChecked(GridView gridView, string checkboxId, string id)
        {
            CheckBox cb = UI.GetGridViewCheckBox(gridView, checkboxId, id);

            if (cb != null)
            {
                return cb.Checked;
            }
            else
            {
                return false;
            }
        }

        public static bool isGridViewCheckBoxChecked(GridViewRow gridViewRow, string checkboxId)
        {
            CheckBox cb = (CheckBox)gridViewRow.FindControl(checkboxId);

            if (cb != null)
            {
                return cb.Checked;
            }
            else
            {
                return false;
            }
        }
        #endregion GridViewUtils


        public static Contentum.eBusinessDocuments.ExecParam SetExecParamDefault(Page ParentPage)
        {
            if (ParentPage == null) return null;

            return SetExecParamDefault(ParentPage, new ExecParam());
        }

        public static Contentum.eBusinessDocuments.ExecParam SetExecParamDefault(System.Web.SessionState.HttpSessionState Session)
        {
            if (Session == null) return null;

            return SetExecParamDefault(Session, new ExecParam());
        }

        public static Contentum.eBusinessDocuments.ExecParam SetExecParamDefault(Page ParentPage, Contentum.eBusinessDocuments.ExecParam ExecParam)
        {
            if (ParentPage == null || ExecParam == null) return null;

            ExecParam xpm = SetExecParamDefault(ParentPage.Session, ExecParam);
            if (ParentPage is PageBase) xpm.FunkcioKod = (ParentPage as PageBase).FunkcioKod;
            return xpm;
        }

        public static Contentum.eBusinessDocuments.ExecParam SetExecParamDefault(System.Web.SessionState.HttpSessionState Session, Contentum.eBusinessDocuments.ExecParam ExecParam)
        {
            if (Session == null || ExecParam == null) return null;

            ExecParam.Felhasznalo_Id = FelhasznaloProfil.FelhasznaloId(Session);
            ExecParam.LoginUser_Id = FelhasznaloProfil.LoginUserId(Session);
            ExecParam.FelhasznaloSzervezet_Id = FelhasznaloProfil.FelhasznaloSzerverzetId(Session);
            ExecParam.CsoportTag_Id = FelhasznaloProfil.FelhasznaloCsoportTagId(Session);

            if (!String.IsNullOrEmpty(FelhasznaloProfil.HelyettesitesId(Session)))
            {
                ExecParam.Helyettesites_Id = FelhasznaloProfil.HelyettesitesId(Session);
            }

            if (!String.IsNullOrEmpty(FelhasznaloProfil.ApplicationId(Session)))
            {
                ExecParam.Alkalmazas_Id = FelhasznaloProfil.ApplicationId(Session);
            }

            if (Session[Constants.AccessId] != null)
            {
                ExecParam.UIAccessLog_Id = Session[Constants.AccessId].ToString();
            }

            string pageTranzId = GetSession(Session, Constants.PageTranzId);
            if (!String.IsNullOrEmpty(pageTranzId))
            {
                ExecParam.Page_Id = pageTranzId;
            }
            else
            {
                //nincs pageId a session-ban
            }

            string OrgId = FelhasznaloProfil.OrgId(Session);

            if (!String.IsNullOrEmpty(OrgId))
            {
                ExecParam.Org_Id = OrgId;
            }

            //UserHostAddress
            ExecParam.UserHostAddress = GetSession(Session, Constants.Munkaallomas);
            //}

            return ExecParam;
        }


        public List<string> GetGridViewSelectedRows(GridView ParentGridView, eUIControls.eErrorPanel ParenteErrorPanel, UpdatePanel ParentErrorUpdatePanel)
        {
            if (ParentGridView == null || ParenteErrorPanel == null)
            {
                return null;
            }
            // törölhetõ elemek listája
            List<string> deletableItemsList = new List<string>();

            // van-e definiálva DataKey:
            if (ParentGridView.Rows.Count != ParentGridView.DataKeys.Count)
            {
                // nincs DataKey, hiba:

                ParenteErrorPanel.Header = Contentum.eUtility.Resources.Error.GetString("ErrorLabel") + ": " +
                    Contentum.eUtility.Resources.Error.GetString("UIDeleteError");
                ParenteErrorPanel.Body = Contentum.eUtility.Resources.Error.GetString("UIDeleteError_Text");
                ParenteErrorPanel.Visible = true;
                if (ParentErrorUpdatePanel != null)
                {
                    ParentErrorUpdatePanel.Update();
                }
            }
            else
            {
                for (int i = 0; i < ParentGridView.Rows.Count; i++)
                {
                    CheckBox checkB = (CheckBox)ParentGridView.Rows[i].FindControl("check");
                    if (checkB.Checked)
                    {

                        string itemId = ParentGridView.DataKeys[i].Value.ToString();
                        // felvesszük a listába az id-t:
                        deletableItemsList.Add(itemId);
                    }
                }
                return deletableItemsList;
            }
            return null;
        }


        /// <summary>
        /// A kiválasztott sorokban található Label-ek értékeit adja vissza
        /// </summary>
        /// <param name="gridView"></param>
        /// <param name="labelId">Label szerver oldali Id-ja</param>
        /// <returns></returns>
        public static List<string> GetGridViewColumnValuesOfSelectedRows(GridView gridView, string labelId)
        {
            if (gridView == null || string.IsNullOrEmpty(labelId))
            {
                return null;
            }

            List<string> itemsList = new List<string>();

            for (int i = 0; i < gridView.Rows.Count; i++)
            {
                CheckBox checkB = (CheckBox)gridView.Rows[i].FindControl("check");
                if (checkB.Checked)
                {
                    // Label megkeresése:
                    Label label = (Label)gridView.Rows[i].FindControl(labelId);

                    if (label != null)
                    {
                        // felvesszük a listába a Label szövegét:
                        itemsList.Add(label.Text);
                    }
                }
            }

            return itemsList;
        }


        /// <summary>
        /// gridView.SelectedRow -ban megkeresi a megadott Label-t, és visszaadja az értékét
        /// </summary>
        /// <param name="gridView"></param>
        /// <param name="labelId"></param>
        /// <returns></returns>
        public static string GetGridViewColumnValueOfSelectedRow(GridView gridView, string labelId)
        {
            if (gridView == null || string.IsNullOrEmpty(labelId) || gridView.SelectedRow == null)
            {
                return null;
            }

            // Label megkeresése:
            Label label = (Label)gridView.SelectedRow.FindControl(labelId);

            if (label != null)
            {
                return label.Text;
            }
            else
            {
                return null;
            }
        }


        public static int GetGridViewSelectedCheckBoxesCount(GridView ParentGridView, String checkBoxServerId)
        {
            int count = 0;

            if (ParentGridView == null)
            {
                return 0;
            }

            for (int i = 0; i < ParentGridView.Rows.Count; i++)
            {
                CheckBox checkB = (CheckBox)ParentGridView.Rows[i].FindControl(checkBoxServerId);
                if (checkB.Checked)
                {
                    count++;
                }
            }

            return count;
        }


        //a sorindexeketis hozza
        public List<string[]> GetGridViewSelectedRowsWithIndex(GridView ParentGridView, eUIControls.eErrorPanel ParenteErrorPanel, UpdatePanel ParentErrorUpdatePanel)
        {
            if (ParentGridView == null || ParenteErrorPanel == null || ParentErrorUpdatePanel == null)
            {
                return null;
            }
            // törölhetõ elemek listája
            List<string[]> deletableItemsList = new List<string[]>();

            // van-e definiálva DataKey:
            if (ParentGridView.Rows.Count != ParentGridView.DataKeys.Count)
            {
                // nincs DataKey, hiba:

                ParenteErrorPanel.Header = Contentum.eUtility.Resources.Error.GetString("ErrorLabel") + ": " +
                    Contentum.eUtility.Resources.Error.GetString("UIDeleteError");
                ParenteErrorPanel.Body = Contentum.eUtility.Resources.Error.GetString("UIDeleteError_Text");
                ParenteErrorPanel.Visible = true;
                ParentErrorUpdatePanel.Update();
            }
            else
            {
                for (int i = 0; i < ParentGridView.Rows.Count; i++)
                {
                    CheckBox checkB = (CheckBox)ParentGridView.Rows[i].FindControl("check");
                    if (checkB.Checked)
                    {
                        string[] item = new string[2];
                        string itemId = ParentGridView.DataKeys[i].Value.ToString();
                        item[0] = itemId;
                        item[1] = i.ToString();
                        // felvesszük a listába az id-t:
                        deletableItemsList.Add(item);
                    }
                }
                return deletableItemsList;
            }
            return null;
        }


        /// <summary>
        /// Megszünteti a sor kijelölést a megadott GridView-n
        /// </summary>
        /// <param name="gridView"></param>
        public static void ClearGridViewRowSelection(GridView gridView)
        {
            if (gridView == null) return;
            gridView.SelectedIndex = -1;
        }

        public static string GetGridViewSelectedRecordId(GridView gridView)
        {
            string id = "";

            if (gridView == null) return id;

            if (gridView.SelectedValue != null)
            {
                try
                {
                    Guid rowGuid = (Guid)gridView.SelectedValue;
                    if (rowGuid != null)
                    {
                        id = rowGuid.ToString();
                    }
                }
                catch
                {
                    // ha nem sikerült castolni, üres stringet adunk vissza
                }
            }
            return id;
        }

        public static string GetGridViewSelectedRecord_ColumnValue(GridView gridView, int columnIndex)
        {
            String columnValue = "";
            try
            {
                columnValue = gridView.SelectedRow.Cells[columnIndex].Text;
            }
            catch (Exception)
            {
                columnValue = "";
            }

            // ha üres a mezõ, akkor &nbsp; -t ad vissza, ezért:
            columnValue = System.Web.HttpUtility.HtmlDecode(columnValue).Trim();

            return columnValue;
        }

        /// <summary>
        /// GridView feltöltése a megadott Result objektumból, és az esetleges hibaüzenet megjelenítése
        /// </summary>
        /// <param name="ParentGridView"></param>
        /// <param name="result"></param>
        /// <param name="errorPanel"></param>
        public void GridViewFill(GridView ParentGridView, Contentum.eBusinessDocuments.Result result, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
        {
            GridViewFill(ParentGridView, result, "", errorPanel, parentErrorUpdatePanel);
        }

        public void GridViewFill(GridView ParentGridView, Contentum.eBusinessDocuments.Result result, String RowCount, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
        {
            ResultError resultError = new ResultError(result);
            if (resultError.IsError)
            {
                // Hiba kezeles
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillGridView"));
                if (parentErrorUpdatePanel != null)
                {
                    parentErrorUpdatePanel.Update();
                }
            }
            else
            {
                DataSet ds = result.Ds;
                ParentGridView.DataSource = ds;
                bool isEmpty = true;
                if (ds.Tables[0].Rows.Count > 0) isEmpty = false;

                if (isEmpty)
                {
                    DataRow x = ds.Tables[0].NewRow();
                    ds.Tables[0].Rows.Add(x);
                }
                if (!String.IsNullOrEmpty(RowCount) && RowCount != "0")
                {
                    ParentGridView.PageSize = Int32.Parse(RowCount);
                }
                else
                {
                    ParentGridView.PageSize = ds.Tables[0].Rows.Count; // alapertek, mondjuk rendszer parameterekbol veve!!!
                }

                ParentGridView.DataBind();

                if (isEmpty)
                {
                    ParentGridView.Rows[0].Visible = false;
                }
            }
        }

        public void GridViewFill(GridView ParentGridView, DataTable table, String RowCount, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
        {
            ParentGridView.DataSource = table;
            bool isEmpty = true;
            if (table.Rows.Count > 0) isEmpty = false;

            if (isEmpty)
            {
                DataRow x = table.NewRow();
                table.Rows.Add(x);
            }
            if (!String.IsNullOrEmpty(RowCount) && RowCount != "0")
            {
                ParentGridView.PageSize = Int32.Parse(RowCount);
            }
            else
            {
                ParentGridView.PageSize = table.Rows.Count; // alapertek, mondjuk rendszer parameterekbol veve!!!
            }

            ParentGridView.DataBind();

            if (isEmpty)
            {
                ParentGridView.Rows[0].Visible = false;
            }
        }

        static public void GridViewFill(GridView ParentGridView, Contentum.eBusinessDocuments.Result result, UserControl ListHeader, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
        {
            string RowCount = String.Empty;
            string selectedRecordId = String.Empty;
            PropertyInfo piSelectedRecorID = null;
            PropertyInfo piPageIndex = null;
            PropertyInfo piRecordNumber = null;
            if (ListHeader != null)
            {
                if (ListHeader.GetType().Name.ToLower() == "component_sublistheader_ascx"
                    || ListHeader.GetType().Name.ToLower() == "component_listheader_ascx"
                    || ListHeader.GetType().Name.ToLower() == "erecordcomponent_jogosultaksublistheader_ascx")
                {
                    PropertyInfo piRowCount = ListHeader.GetType().GetProperty("RowCount");
                    if (piRowCount != null)
                        RowCount = piRowCount.GetValue(ListHeader, null).ToString();
                    else
                        System.Diagnostics.Debug.WriteLine("RowCount property not found.");

                    piSelectedRecorID = ListHeader.GetType().GetProperty("SelectedRecordId");
                    if (piSelectedRecorID != null)
                        selectedRecordId = piSelectedRecorID.GetValue(ListHeader, null).ToString();
                    else
                        System.Diagnostics.Debug.WriteLine("selectedRecordId property not found.");

                    piPageIndex = ListHeader.GetType().GetProperty("PageIndex");
                    piRecordNumber = ListHeader.GetType().GetProperty("RecordNumber");
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("ListHeader argument bad type.");
                }
            }

            Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);
            if (resultError.IsError)
            {
                // Hiba kezeles
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillGridView"));
                if (parentErrorUpdatePanel != null)
                {
                    parentErrorUpdatePanel.Update();
                }
                return;
            }

            DataSet ds = result.Ds;
            ParentGridView.DataSource = ds;
            bool isEmpty = true;
            bool isSingleRow = false; // true, ha csak egy találat van

            if (ds.Tables[0].Rows.Count > 0) isEmpty = false;

            // ha csak egy találat van: (a lapozós módszernél majd fejbevágjuk, de a régifajta lekérésnél itt kell eldönteni)
            if (ds.Tables[0].Rows.Count == 1) { isSingleRow = true; }

            if (isEmpty)
            {
                DataRow x = ds.Tables[0].NewRow();
                ds.Tables[0].Rows.Add(x);
            }

            if (!String.IsNullOrEmpty(RowCount) && RowCount != "0")
            {
                ParentGridView.PageSize = Int32.Parse(RowCount);
            }
            else
            {
                ParentGridView.PageSize = ds.Tables[0].Rows.Count; // alapertek, mondjuk rendszer parameterekbol veve!!!
            }


            // Van-e lapozás (tárolt eljárás szinten) (ha két eredmény tábla van)
            bool isPaging = false;
            if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count == 1)
            {
                // Van lapozás: Második selectben jön vissza a találatok száma:

                isPaging = true;

                // találatok száma:
                int recordNumberFromPaging = Int32.Parse(ds.Tables[1].Rows[0]["RecordNumber"].ToString());
                piRecordNumber.SetValue(ListHeader, recordNumberFromPaging, null);

                // ha csak egy találat van:
                if (recordNumberFromPaging == 1)
                {
                    isSingleRow = true;
                }
                else
                {
                    isSingleRow = false;
                }

                // oldalszám:
                int pageIndex = Int32.Parse(ds.Tables[1].Rows[0]["PageNumber"].ToString());
                piPageIndex.SetValue(ListHeader, pageIndex, null);

                //a hiddenfield-ben beállított id-hoz tartozó sor kiválasztása
                //ha nincs beállítva id a kiválasztás törlése
                if (!String.IsNullOrEmpty(selectedRecordId) && selectedRecordId != Guid.Empty.ToString())
                {
                    string id = selectedRecordId;
                    int selectedIndex = -1;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i]["Id"].ToString() == id)
                        {
                            selectedIndex = i;
                            break;
                        }
                    }
                    if (selectedIndex > -1)
                    {
                        int rowCount = Int32.Parse(RowCount);

                        if (rowCount > 0)
                            selectedIndex = selectedIndex % rowCount;

                        ParentGridView.SelectedIndex = selectedIndex;
                    }
                    else
                    {
                        ParentGridView.SelectedIndex = -1;
                        piSelectedRecorID.SetValue(ListHeader, String.Empty, null);
                    }
                }
                else
                {
                    ParentGridView.SelectedIndex = -1;
                }
            }


            if (!isPaging)
            {

                //a hiddenfield-ben beállított id-hoz tartozó sor kiválasztása
                //ha nincs beállítva id a kiválasztás törlése
                if (!String.IsNullOrEmpty(selectedRecordId) && selectedRecordId != Guid.Empty.ToString())
                {
                    string id = selectedRecordId;
                    int selectedIndex = -1;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i]["Id"].ToString() == id)
                        {
                            selectedIndex = i;
                            break;
                        }
                    }
                    if (selectedIndex > -1)
                    {
                        int rowCount = ParentGridView.PageSize;
                        int pageNumber = 0;
                        if (rowCount > 0)
                        {
                            pageNumber = selectedIndex / rowCount;
                            selectedIndex = selectedIndex % rowCount;
                        }
                        ParentGridView.PageIndex = pageNumber;
                        ParentGridView.SelectedIndex = selectedIndex;
                        if (piPageIndex != null)
                        {
                            piPageIndex.SetValue(ListHeader, pageNumber, null);
                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine("PageIndex property not found.");
                        }
                    }
                    else
                    {
                        ParentGridView.SelectedIndex = -1;
                        piSelectedRecorID.SetValue(ListHeader, String.Empty, null);
                    }
                }
                else
                {
                    ParentGridView.SelectedIndex = -1;
                }

                if (piRecordNumber != null)
                {
                    if (!isEmpty)
                        piRecordNumber.SetValue(ListHeader, ds.Tables[0].Rows.Count, null);
                    else
                        piRecordNumber.SetValue(ListHeader, 0, null);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("RecorNumber property not found");
                }
            }

            //// ha egy találat volt, sor kijelölése (csak PostBack-nél, mert a listákon a gombokat csak PostBacknél állítjuk)
            //if (isSingleRow && ParentGridView.Page.IsPostBack)
            if (isSingleRow)
            {
                ParentGridView.SelectedIndex = 0;
                if (piSelectedRecorID != null)
                {
                    piSelectedRecorID.SetValue(ListHeader, result.Ds.Tables[0].Rows[0]["Id"].ToString(), null);
                }
            }

            ParentGridView.DataBind();

            if (isEmpty)
            {
                ParentGridView.Rows[0].Visible = false;
            }

            if (ParentGridView.SelectedIndex > -1)
            {
                UI.SetGridViewCheckBoxesToSelectedRow(ParentGridView, ParentGridView.SelectedIndex, "check");
            }

        }


        private static string SetTabHeaderRowCountText(Contentum.eBusinessDocuments.Result result, string TabHeaderText)
        {
            try
            {
                int rowCount = result.Ds.Tables[0].Rows.Count;

                if (result.Ds.Tables.Count > 1 && result.Ds.Tables[1].Rows.Count == 1)
                {
                    Int32.TryParse(result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString(), out rowCount);
                }
                else
                {
                    // 1 üres rekord a DS-ben kiszûrése
                    if (result.Ds.Tables[0].Rows.Count == 1)
                    {
                        rowCount = 0;

                        for (int i = 0; i < result.Ds.Tables[0].Rows[0].ItemArray.Length; i++)
                        {
                            if (!string.IsNullOrEmpty(result.Ds.Tables[0].Rows[0][i].ToString()))
                            {
                                rowCount = 1;
                                break;
                            }
                        }
                    }
                }

                if (TabHeaderText.IndexOf(" (") > 0)
                    TabHeaderText = TabHeaderText.Remove(TabHeaderText.IndexOf(" ("));
                TabHeaderText += " (" + rowCount.ToString() + ")";

                return TabHeaderText;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Setting TabHeaderText failed." + e.Message);
                return TabHeaderText;
            }
        }

        public static void SetTabHeaderRowCountText(Contentum.eBusinessDocuments.Result result, Label TabHeader)
        {
            TabHeader.Text = UI.SetTabHeaderRowCountText(result, TabHeader.Text);
        }

        public static void SetTabHeaderRowCountText(Contentum.eBusinessDocuments.Result result, AjaxControlToolkit.TabPanel TabPanel)
        {
            TabPanel.HeaderText = UI.SetTabHeaderRowCountText(result, TabPanel.HeaderText);
        }


        private static string ClearTabHeaderRowCountText(string TabHeaderText)
        {
            try
            {
                if (TabHeaderText.IndexOf(" (") > 0)
                    TabHeaderText = TabHeaderText.Remove(TabHeaderText.IndexOf(" ("));

                return TabHeaderText;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Clearing TabHeaderText failed." + e.Message);
                return TabHeaderText;
            }
        }

        public static void ClearTabHeaderRowCountText(Label TabHeader)
        {
            TabHeader.Text = UI.ClearTabHeaderRowCountText(TabHeader.Text);
        }

        public static void ClearTabHeaderRowCountText(AjaxControlToolkit.TabPanel TabPanel)
        {
            TabPanel.HeaderText = UI.ClearTabHeaderRowCountText(TabPanel.HeaderText);
        }

        public static UserControl FindParentListHeader(Control control)
        {
            foreach (UserControl userControl in FindControls<UserControl>(control.Page.Form.Controls))
            {
                if (userControl.GetType().Name.ToLower() == "component_sublistheader_ascx" || userControl.GetType().Name.ToLower() == "component_listheader_ascx")
                {
                    PropertyInfo piAttachedGridViewId = userControl.GetType().GetProperty("AttachedGridViewId");
                    if (piAttachedGridViewId != null)
                    {
                        string gridViewId = (string)piAttachedGridViewId.GetValue(userControl, null);
                        if (!String.IsNullOrEmpty(gridViewId) && gridViewId == control.ID)
                        {
                            return userControl;
                        }

                    }
                }
            }

            return null;
        }


        /// <summary>
        /// Az execParam-ban beállítja a lapozáshoz szükséges paramétereket
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ListHeader"></param>
        public static void SetPaging(ExecParam execParam, UserControl ListHeader)
        {
            if (execParam == null || ListHeader == null) { return; }

            // Ablakméret (sorok száma egy oldalon)
            PropertyInfo piRowCount = ListHeader.GetType().GetProperty("RowCount");
            if (piRowCount != null)
            {
                execParam.Paging.PageSize = Int32.Parse(piRowCount.GetValue(ListHeader, null).ToString());
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("RowCount property not found.");
            }

            // Oldal sorszáma:
            PropertyInfo piPageIndex = ListHeader.GetType().GetProperty("PageIndex");
            if (piPageIndex != null)
            {
                execParam.Paging.PageNumber = Int32.Parse(piPageIndex.GetValue(ListHeader, null).ToString());
                //if (execParam.Paging.PageNumber == 0)
                //{
                //    execParam.Paging.PageNumber = 1;
                //}
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("PageIndex property not found.");
            }

            // Kiválasztott rekord id
            PropertyInfo piSelectedRecordID = ListHeader.GetType().GetProperty("SelectedRecordId");
            if (piSelectedRecordID != null)
            {
                execParam.Paging.SelectedRowId = piSelectedRecordID.GetValue(ListHeader, null).ToString();
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("selectedRecordId property not found.");
            }
        }


        /// <summary>
        /// GridView tartalmát törli
        /// </summary>
        /// <param name="gridView"></param>
        public void GridViewClear(GridView gridView)
        {
            if (gridView == null) return;

            gridView.DataSource = null;

            //A sorok számának 0-ra állítása
            UserControl ListHeader = FindParentListHeader(gridView);
            if (ListHeader != null)
            {
                PropertyInfo piRecordNumber = ListHeader.GetType().GetProperty("RecordNumber");
                if (piRecordNumber != null)
                {
                    piRecordNumber.SetValue(ListHeader, 0, null);
                }
            }

            gridView.DataBind();
        }

        public static string GetGridViewPagerLabel(GridView gridView)
        {
            if (gridView == null) return null;
            return (gridView.PageIndex + 1).ToString() + "/" + gridView.PageCount.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gridView"></param>
        /// <param name="checkBoxId"></param>
        /// <param name="Id">(eleme a GridView.DataKeys-nek)</param>
        /// <returns></returns>
        public static CheckBox GetGridViewCheckBox(GridView gridView, String checkBoxId, String Id)
        {
            GridViewRow containerRow = null;
            foreach (GridViewRow row in gridView.Rows)
            {
                if ((row.RowType == DataControlRowType.DataRow) && (gridView.DataKeys[row.RowIndex].Value.ToString() == Id))
                {
                    containerRow = row;
                    break;
                }
            }
            if (containerRow == null)
            {
                return null;
            }
            else
            {
                CheckBox checkBox = (CheckBox)containerRow.FindControl(checkBoxId);
                return checkBox;
            }
        }

        //LZS - BUG_3146
        public static void ListBoxFill(ListControl listBox, Contentum.eBusinessDocuments.Result result, String columnName
            , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel, string PreName)
        {
            ListBoxFill(listBox, result, "Id", columnName, errorPanel, parentErrorUpdatePanel, PreName);
        }

        public static void ListBoxFill(ListControl listBox, Contentum.eBusinessDocuments.Result result, String columnName
           , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
        {
            ListBoxFill(listBox, result, "Id", columnName, errorPanel, parentErrorUpdatePanel);
        }

        /// <summary>
        /// ListBox feltöltése a megadott Result objektumból, és az esetleges hibaüzenet megjelenítése
        /// </summary>
        /// <param name="listBox"></param>
        /// <param name="result"></param>
        /// <param name="columnName"></param>
        /// <param name="errorPanel"></param>
        /// <param name="parentErrorUpdatePanel"></param>
        public static void ListBoxFill(ListControl listBox, Contentum.eBusinessDocuments.Result result, string idColumnName, String columnName
            , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
        {
            if (listBox == null || columnName == null) return;

            ResultError resultError = new ResultError(result);

            if (resultError.IsError)
            {
                // Hiba kezeles
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
                if (parentErrorUpdatePanel != null)
                {
                    parentErrorUpdatePanel.Update();
                }
            }
            else
            {
                try
                {
                    listBox.Items.Clear();
                    DataSet ds = result.Ds;

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        String text = (String)row[columnName];
                        String id = ((Guid)row[idColumnName]).ToString();

                        listBox.Items.Add(new ListItem(text, id));
                    }
                }
                catch (Exception)
                {
                    ResultError.DisplayErrorOnErrorPanel(errorPanel, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"),
                        "");
                }
            }
        }

        //LZS - BUG_3146
        public static void ListBoxFill(ListControl listBox, Contentum.eBusinessDocuments.Result result, string idColumnName, String columnName
            , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel, string preName)
        {
            if (listBox == null || columnName == null) return;

            ResultError resultError = new ResultError(result);

            if (resultError.IsError)
            {
                // Hiba kezeles
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
                if (parentErrorUpdatePanel != null)
                {
                    parentErrorUpdatePanel.Update();
                }
            }
            else
            {
                try
                {
                    listBox.Items.Clear();
                    DataSet ds = result.Ds;

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        String text = (String)row[columnName];

                        if (text.Trim().ToLower().StartsWith(preName))
                        {
                            text = text.Substring(preName.Length + 1) + text.Substring(0, preName.Length);
                        }

                        String id = ((Guid)row[idColumnName]).ToString();

                        listBox.Items.Add(new ListItem(text, id));
                    }

                    listBox = SortListBox(listBox, false);

                    //Visszaírjuk az előtagot
                    foreach (ListItem item in listBox.Items)
                    {
                        if (item.Text.Trim().ToLower().EndsWith(preName))
                        {
                            item.Text = item.Text.Substring(item.Text.Length - preName.Length) + " " + item.Text.Substring(0, item.Text.Length - preName.Length);
                        }
                    }
                }
                catch
                {
                    ResultError.DisplayErrorOnErrorPanel(errorPanel, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"),
                        "");
                }
            }
        }

        //LZS - BUG_3146
        public static ListControl SortListBox(ListControl pList, bool pByValue)
        {
            SortedList lListItems = new SortedList();

            try
            {
                //add listbox items to SortedList 
                foreach (ListItem lItem in pList.Items)
                {

                    if (pByValue)
                    {
                        lListItems.Add(lItem.Value, lItem);
                    }
                    else
                    {
                        if (!lListItems.Contains(lItem.Text))
                        {
                            lListItems.Add(lItem.Text, lItem);
                        }
                    }
                }
            }
            catch
            {
            };

            //clear list box
            pList.Items.Clear();

            //add sorted items to listbox
            for (int i = 0; i < lListItems.Count; i++)
            {
                pList.Items.Add((ListItem)lListItems[lListItems.GetKey(i)]);
            }

            return pList;
        }

        /// <summary>
        /// Checkbox oszlopok beállítása ('I' vagy '1' esetén .Checked = true; 'N' vagy '0' esetén false;
        /// egyébként CheckBox.Visible = false;)
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <param name="checkBoxId">CheckBox szerver oldali id-ja a GridView-n belül</param>
        /// <param name="columnName">Oszlop neve, ami alapján a CheckBox be lesz állítva</param>
        public static void GridView_RowDataBound_SetCheckBoxCheckedValue(
            GridViewRowEventArgs e, Page page, String checkBoxId, String columnName)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drw = (DataRowView)e.Row.DataItem;
                CheckBox checkBox = (CheckBox)e.Row.FindControl(checkBoxId);
                if (checkBox != null)
                {
                    if (drw[columnName] != null)
                    {
                        String columnValue = drw[columnName].ToString();
                        switch (columnValue)
                        {
                            case "I":
                            case "1":
                                checkBox.Checked = true;
                                break;
                            case "N":
                            case "0":
                                checkBox.Checked = false;
                                break;
                            default:
                                checkBox.Visible = false;
                                break;
                        }
                    }
                    else
                    {
                        checkBox.Visible = false;
                    }
                }
            }
        }


        /// <summary>
        /// Beállítja a GridView adott sorában megtalálható Image Visible értékét a DataRow megadott mezõjének értéke alapján. 
        /// Ha a mezõ értéke '1', 'I', 'i', akkor image.Visible = true; egyébként false
        /// </summary>
        /// <param name="e"></param>
        /// <param name="imageId"></param>
        /// <param name="columnName"></param>
        public static void GridView_RowDataBound_SetImageVisibility(GridViewRowEventArgs e, string imageId, string columnName)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
                string columnValue = String.Empty;

                if (drw[columnName] != null)
                {
                    columnValue = drw[columnName].ToString();
                }

                if (!String.IsNullOrEmpty(columnValue))
                {
                    Image image = (Image)e.Row.FindControl(imageId);
                    if (image != null)
                    {
                        switch (columnValue)
                        {
                            case "1":
                            case "I":
                            case "i":
                                image.Visible = true;
                                break;
                            default:
                                image.Visible = false;
                                break;
                        }
                    }
                }
                else
                {
                    Image image = (Image)e.Row.FindControl(imageId);
                    if (image != null)
                    {
                        image.Visible = false;
                    }
                }
            }
        }



        /// <summary>
        /// Megjelenít egy "Nincs megfelelõ jogosultsága a mûvelet végrehajtásához" üzenetet a megadott hibapanelra
        /// </summary>
        /// <param name="errorPanel"></param>
        /// <param name="updatePanel"></param>
        public static void DisplayDontHaveFunctionRights(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (errorPanel == null) return;
            else
            {
                errorPanel.Visible = true;
                errorPanel.Header = Contentum.eUtility.Resources.Error.GetString("UIFunctionRightsError");
                errorPanel.Body = Contentum.eUtility.Resources.Error.GetString("UIDontHaveFunctionRights");
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }
        }



        public static void RedirectToAccessDeniedErrorPage(Page page)
        {
            if (page == null) return;
            page.Response.Redirect("Error.aspx?errorcode=UIAccessDeniedFunction", true);
        }

        public static void RedirectToErrorPageByErrorCode(Page page, String ErrorCode)
        {
            if (page == null) return;
            page.Response.Redirect("Error.aspx?errorcode=" + ErrorCode, true);
        }

        public static void RedirectToErrorPageByErrorMsg(Page page, String ErrorMsg)
        {
            if (page == null) return;
            page.Response.Redirect("Error.aspx?ErrorMsg=" + ErrorMsg, true);
        }

        public static List<String> StringToListBySeparator(String StringList, Char Separator)
        {
            String[] stringlist = StringList.Split(Separator);
            //List<string> _List = new List<string>();

            //foreach (String item in stringlist)
            //{
            //    if (!String.IsNullOrEmpty(item))
            //    {
            //        _List.Add(item);
            //    }
            //}
            List<string> _List = new List<string>(stringlist);
            return _List;
        }

        public static void popupRedirect(Page page, string Url)
        {
            if (page == null || String.IsNullOrEmpty(Url)) return;

            Url = page.ResolveUrl(Url);

            string js = "var a = document.createElement('a');a.href = '" + Url + "';"
                        + "if(a.click){document.forms[0].appendChild(a);a.click();} else {window.location = a.href;}";

            ScriptManager.RegisterStartupScript(page, page.GetType(), "popupRedirect", js, true);
        }

        //FONTOS LOGOLASHOZ
        /// <summary>
        /// PageBase osztály, minden oldal ebbõl származik, a beépített Page osztály helyett
        /// Így az ítt lévõ függvéynek minden oldalon végrehajtódnak, mint loggolás
        /// </summary>
        public class PageBase : System.Web.UI.Page
        {
            private bool IsInitializedCustomCulture = false;
            private void InitalizeCustomCulture()
            {
                if (CultureInfo.CurrentCulture.Name == "hu-HU")
                {
                    //ExecParam xpm = UI.SetExecParamDefault(this);
                    //if (!String.IsNullOrEmpty(xpm.Org_Id))
                    //{
                    string ShortDatePattern = Rendszerparameterek.Get(Page, Rendszerparameterek.SHORT_DATE_PATTERN, false);
                    if (!String.IsNullOrEmpty(ShortDatePattern))
                    {
                        CultureInfo hun = new CultureInfo("hu-Hu", false);
                        hun.DateTimeFormat.ShortDatePattern = ShortDatePattern;
                        Thread.CurrentThread.CurrentCulture = hun;
                        Thread.CurrentThread.CurrentUICulture = hun;
                    }
                    IsInitializedCustomCulture = true;
                    //}
                    //else
                    //{
                    //    Logger.Debug("InitalizeCustomCulture: Org_Id null");
                    //}
                }
            }

            protected override void InitializeCulture()
            {
                InitalizeCustomCulture();
                base.InitializeCulture();
            }

            private Log _log;

            private string _FunkcioKod;

            public string FunkcioKod
            {
                get { return _FunkcioKod; }
                set { _FunkcioKod = value; }
            }

            protected override void OnPreInit(EventArgs e)
            {
                _log = new Log(Page);

                //az új oldal elmentése a historyba
                if (!Page.IsPostBack)
                    AddPageToHistory(Page);

                base.OnPreInit(e);
            }

            protected override void OnInit(EventArgs e)
            {
                base.OnInit(e);
            }

            protected override void OnError(EventArgs e)
            {
                Exception lastException = Server.GetLastError();
                if (lastException != null)
                {
                    // hiba logolása:

                    // log4Net:
                    Logger.Error("PAGE_ERROR LOG  \n " + lastException.ToString());

                    // Audit log:
                    if (_log != null)
                    {
                        Result res = new Result();
                        res.ErrorCode = res.ErrorMessage = lastException.ToString();
                        _log.PageError(res);
                    }
                }

                base.OnError(e);
            }

            protected override void OnPreRenderComplete(EventArgs e)
            {
                base.OnPreRenderComplete(e);
                if (IsInitializedCustomCulture)
                {
                    ExecParam execParam = UI.SetExecParamDefault(this);
                    if (!String.IsNullOrEmpty(execParam.Org_Id))
                    {
                        ScriptManager sm = ScriptManager.GetCurrent(this);
                        if (sm != null)
                        {
                            if (!sm.IsInAsyncPostBack && sm.EnableScriptGlobalization)
                            {
                                string script = ClientCultureInfo.GetClientCultureScriptBlock(CultureInfo.CurrentCulture, execParam.Org_Id);
                                if (script != null)
                                {
                                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "CultureInfoAllDateTimePatterns", script, true);
                                }
                            }

                            // BUG 2891, tooltip fix (use image 'alt' attribute value to 'title' if not specified)
                            const string fixTooltipsName = "FixTooltips";
                            if (!sm.IsInAsyncPostBack && ClientScript != null && !ClientScript.IsStartupScriptRegistered(fixTooltipsName))
                            {
                                //string fixTooltipsScript = @"$('input[type=""image""][alt]:not([title]),img[alt]:not([title])').attr(""title"", function() { return $(this).attr(""alt""); });";
                                string fixTooltipsScript = @"Array.prototype.slice.call(document.querySelectorAll('input[type=""image""][alt]:not([title]),img[alt]:not([title])')).forEach(function (el) { el.setAttribute(""title"", el.getAttribute(""alt"")); });";
                                ClientScript.RegisterStartupScript(Page.GetType(), fixTooltipsName, fixTooltipsScript, true);
                            }
                        }
                    }
                }
            }

            protected override object LoadPageStateFromPersistenceMedium()
            {
                bool enableViewStateCompression = UI.GetAppSetting("enableViewStateCompression", false);
                if (enableViewStateCompression)
                {
                    string viewState = Request.Form["__VSTATE"];
                    byte[] bytes = System.Convert.FromBase64String(viewState);
                    bytes = Compressor.Decompress(bytes);
                    LosFormatter formatter = new LosFormatter();
                    return formatter.Deserialize(System.Convert.ToBase64String(bytes));
                }
                else
                {
                    return base.LoadPageStateFromPersistenceMedium();
                }
            }

            protected override void SavePageStateToPersistenceMedium(object viewState)
            {
                bool enableViewStateCompression = UI.GetAppSetting("enableViewStateCompression", false);
                if (enableViewStateCompression)
                {
                    LosFormatter formatter = new LosFormatter();
                    StringWriter writer = new StringWriter();
                    formatter.Serialize(writer, viewState);
                    string viewStateString = writer.ToString();
                    byte[] bytes = System.Convert.FromBase64String(viewStateString);
                    bytes = Compressor.Compress(bytes);
                    ScriptManager.RegisterHiddenField(this, "__VSTATE", System.Convert.ToBase64String(bytes));
                }
                else
                {
                    base.SavePageStateToPersistenceMedium(viewState);
                }
            }

            public virtual void Load_ComponentSelectModul()
            {
            }
        }


        /// <summary>
        /// Általános listák SSRS riportjainak absztrakt õsosztálya.
        /// Kezeli az adatlekérést, a standard GetAll jellegû riport paramétereket névkonvenció alapján
        /// (tárolt eljárás bemenõ paranéterének neve azonos a riportparaméter nevével),
        /// az oszlop láthatósági paramétereket, a keresõobjektumból kiolvasható (pl. ReadableWhere)
        /// paramétereket.
        /// </summary>
        public abstract class ListSSRSPageBase : Contentum.eUtility.UI.PageBase
        {

            protected string vis = String.Empty;

            protected abstract Microsoft.Reporting.WebForms.ReportViewer ReportViewer { get; }
            protected abstract String DefaultOrderBy { get; }
            protected abstract Contentum.eUIControls.eErrorPanel ErrorPanel { get; }

            protected abstract Result GetData();
            protected abstract Contentum.eQuery.BusinessDocuments.BaseSearchObject GetSearchObject();

            protected abstract Dictionary<string, int> CreateVisibilityColumnsInfoDictionary();
            protected abstract void SetSpecificReportParameter(Microsoft.Reporting.WebForms.ReportParameter rp);

            protected virtual void HandleError(Exception ex)
            {
                if (this.ErrorPanel == null)
                {
                    this.ClientScript.RegisterStartupScript(this.GetType()
                        , "HandleSSRSPageError", String.Format("alert('{0}');window.close();", ex.Message), true);
                }
                else
                {
                    ResultError.DisplayErrorOnErrorPanel(this.ErrorPanel
                        , Contentum.eUtility.Resources.Error.GetString("DefaultErrorHeader"), ex.Message);
                }
            }

            protected virtual String DefaultPageSize
            {
                get { return "10000"; }
            }

            protected virtual String VisibilityParameterTrueValue
            {
                get { return "true"; }
            }

            protected virtual String VisibilityParameterFalseValue
            {
                get { return "false"; }
            }

            protected virtual bool IsGetAllParameter(String pname)
            {
                switch (pname)
                {
                    case "Where":
                    case "TopRow":
                    case "ExecutorUserId":
                    case "Jogosultak":
                    case "OrderBy":
                    case "pageNumber":
                    case "pageSize":
                    case "SelectedRowId":
                        return true;
                    default:
                        return false;
                }
            }

            protected virtual bool IsSearchObjectParameter(String pname)
            {
                switch (pname)
                {
                    case "ReadableWhere":
                        return true;
                    default:
                        return false;
                }
            }

            protected virtual bool IsVisibilityParameter(String pname)
            {
                return (VisibilityParameters != null && pname != null && VisibilityParameters.ContainsKey(pname));
            }

            private Dictionary<string, int> visibilityParameters = null;
            public Dictionary<string, int> VisibilityParameters
            {
                get
                {
                    if (visibilityParameters == null)
                    {
                        visibilityParameters = CreateVisibilityColumnsInfoDictionary();
                    }

                    return visibilityParameters;
                }
            }

            private Result resultOfDataQuery = null;
            protected Result ResultOfDataQuery
            {
                get
                {
                    if (resultOfDataQuery == null)
                    {
                        resultOfDataQuery = GetData();

                        if (resultOfDataQuery.IsError)
                        {
                            throw new Exception(Contentum.eUtility.ResultError.GetErrorMessageFromResultObject(resultOfDataQuery));
                        }
                    }
                    return resultOfDataQuery;
                }
            }

            private Contentum.eQuery.BusinessDocuments.BaseSearchObject searchObject = null;
            protected Contentum.eQuery.BusinessDocuments.BaseSearchObject SearchObject
            {
                get
                {
                    if (searchObject == null)
                    {
                        searchObject = GetSearchObject();
                    }
                    return searchObject;
                }
            }

            protected virtual ReportViewerCredentials GetReportViewCredentials()
            {
                string app_server_name = System.Environment.MachineName;
                ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
                return rvc;
            }

            protected virtual void SetVisibilityArray()
            {
                vis = Request.QueryString.Get(QueryStringVars.Filter);
            }

            protected virtual void SetReportViewer()
            {
                ReportViewerCredentials rvc = GetReportViewCredentials();

                if (ReportViewer != null && !IsPostBack)
                {
                    System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
                    ReportViewer.ServerReport.ReportServerUrl = ReportServerUrl;
                    ReportViewer.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer.ServerReport.ReportPath;

                    ReportViewer.ServerReport.ReportServerCredentials = rvc;
                    ReportViewer.ShowRefreshButton = false;
                    ReportViewer.ShowParameterPrompts = false;

                    Microsoft.Reporting.WebForms.ReportParameterInfoCollection rpis = ReportViewer.ServerReport.GetParameters();

                    if (rpis.Count > 0)
                    {
                        Microsoft.Reporting.WebForms.ReportParameter[] ReportParameters = GetReportParameters(rpis);

                        ReportViewer.ServerReport.SetParameters(ReportParameters);
                    }

                    ReportViewer.ServerReport.Refresh();
                }
            }

            protected virtual String GetGetAllParameterDefaultValue(String rpName)
            {
                switch (rpName)
                {
                    case "OrderBy":
                        return DefaultOrderBy;
                    case "pageSize":
                        return DefaultPageSize;
                    case "pageNumber":
                        return "0";
                    default:
                        return null;
                }
            }

            protected virtual bool SetGetAllReportParameter(Microsoft.Reporting.WebForms.ReportParameter rp)
            {
                bool isGetAllParam = false;
                if (rp != null && !String.IsNullOrEmpty(rp.Name))
                {
                    isGetAllParam = IsGetAllParameter(rp.Name);
                    if (isGetAllParam)
                    {
                        String spParamName = String.Concat("@", rp.Name);
                        String rpParamValue = ResultOfDataQuery.SqlCommand.GetParamValue(spParamName);

                        if (String.IsNullOrEmpty(rpParamValue))
                        {
                            String defaultValue = GetGetAllParameterDefaultValue(rp.Name);
                            if (defaultValue != null)
                            {
                                rp.Values.Add(defaultValue);
                            }
                        }
                        else
                        {
                            rp.Values.Add(rpParamValue);
                        }
                    }
                }
                return isGetAllParam;
            }


            protected virtual void SetVisibilityParameterValue(Microsoft.Reporting.WebForms.ReportParameter rp, int visindex)
            {
                if (rp != null && vis != null)
                {
                    String visp = vis.Substring(visindex, 1);
                    rp.Values.Add(visp.Equals("1") ? VisibilityParameterTrueValue : VisibilityParameterFalseValue);
                }
            }

            protected bool SetVisibilityReportParameter(Microsoft.Reporting.WebForms.ReportParameter rp)
            {
                if(vis == null) { return false; }
                bool isVisibilityParam = false;
                if (rp != null && !String.IsNullOrEmpty(rp.Name))
                {
                    isVisibilityParam = IsVisibilityParameter(rp.Name);
                    if (isVisibilityParam)
                    {
                        SetVisibilityParameterValue(rp, VisibilityParameters[rp.Name]);
                    }
                }
                return isVisibilityParam;
            }

            protected virtual void SetSearchObjectParameterValue(Microsoft.Reporting.WebForms.ReportParameter rp)
            {
                if (rp != null && SearchObject != null)
                {
                    switch (rp.Name)
                    {
                        case "ReadableWhere":
                            rp.Values.Add(SearchObject.ReadableWhere);
                            break;
                    }
                }
            }

            protected bool SetSearchObjectParameter(Microsoft.Reporting.WebForms.ReportParameter rp)
            {
                bool isSearcObjectParam = false;
                if (rp != null && !String.IsNullOrEmpty(rp.Name))
                {
                    isSearcObjectParam = IsSearchObjectParameter(rp.Name);
                    if (isSearcObjectParam)
                    {
                        SetSearchObjectParameterValue(rp);
                    }
                }
                return isSearcObjectParam;
            }

            protected virtual void SetReportParameter(Microsoft.Reporting.WebForms.ReportParameter rp)
            {
                if (rp != null)
                {
                    if (!SetGetAllReportParameter(rp)
                        && !SetVisibilityReportParameter(rp)
                        && !SetSearchObjectParameter(rp))
                    {
                        SetSpecificReportParameter(rp);
                    }
                }
            }

            protected Microsoft.Reporting.WebForms.ReportParameter[] GetReportParameters(Microsoft.Reporting.WebForms.ReportParameterInfoCollection rpis)
            {
                Microsoft.Reporting.WebForms.ReportParameter[] ReportParameters = null;
                if (rpis != null)
                {
                    if (rpis.Count > 0)
                    {
                        ReportParameters = new Microsoft.Reporting.WebForms.ReportParameter[rpis.Count];

                        for (int i = 0; i < rpis.Count; i++)
                        {
                            ReportParameters[i] = new Microsoft.Reporting.WebForms.ReportParameter(rpis[i].Name);

                            SetReportParameter(ReportParameters[i]);
                        }
                    }
                }
                return ReportParameters;
            }

            #region Page
            protected virtual void Page_Init(object sender, EventArgs e)
            {
                SetVisibilityArray();
            }

            protected virtual void Page_Load(object sender, EventArgs e)
            {
                try
                {
                    SetReportViewer();
                }
                catch (Exception ex)
                {
                    HandleError(ex);
                }
            }

            #endregion Page
        }

        internal class ClientCultureInfo
        {

            private static Hashtable cultureScriptBlockCache = Hashtable.Synchronized(new Hashtable());

            public string[] alldateTimePatterns;

            private ClientCultureInfo(CultureInfo cultureInfo)
            {
                alldateTimePatterns = cultureInfo.DateTimeFormat.GetAllDateTimePatterns();
            }

            internal class CultureInfoWithOrg
            {
                public CultureInfo CultureInfo;
                public string Org_Id;

                public override bool Equals(object obj)
                {
                    if (obj is CultureInfoWithOrg)
                    {
                        CultureInfoWithOrg cultureInfoWithOrg = obj as CultureInfoWithOrg;
                        return (this.CultureInfo == cultureInfoWithOrg.CultureInfo && this.Org_Id == cultureInfoWithOrg.Org_Id);
                    }
                    return false;
                }

                public override int GetHashCode()
                {
                    return this.CultureInfo.GetHashCode() ^ this.Org_Id.GetHashCode();
                }
            }

            internal static string GetClientCultureScriptBlock(CultureInfo cultureInfo, string Org_Id)
            {
                if ((cultureInfo == null) ||
                    cultureInfo.Equals(CultureInfo.GetCultureInfo(0x409)))
                {
                    return null;
                }

                CultureInfoWithOrg cultureInfoWithOrg = new CultureInfoWithOrg();
                cultureInfoWithOrg.CultureInfo = cultureInfo;
                cultureInfoWithOrg.Org_Id = Org_Id;

                object cached = cultureScriptBlockCache[cultureInfoWithOrg];
                if (cached == null)
                {
                    ClientCultureInfo clientCultureInfo = new ClientCultureInfo(cultureInfo);
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string rawJsonString = serializer.Serialize(clientCultureInfo.alldateTimePatterns);
                    if (rawJsonString.Length > 0)
                    {
                        cached = "Sys.CultureInfo.CurrentCulture._dateTimeFormats = " + rawJsonString + ";";
                    }

                    cultureScriptBlockCache[cultureInfoWithOrg] = cached;
                }
                return (string)cached;
            }
        }

        public static bool IsLovListMaxRowWarning(Result res, int MaxRow)
        {
            if (res != null && res.Ds != null)
            {
                if (res.Ds.Tables[0].Rows.Count >= MaxRow)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// ListBox feltöltése több oszlopból vett értékek összevonásával
        /// </summary>
        /// <param name="listBox"></param>
        /// <param name="result"></param>
        /// <param name="columnsName"></param>
        /// <param name="seperator"></param>
        /// <param name="errorPanel"></param>
        /// <param name="parentErrorUpdatePanel"></param>
        public static void ListBoxFill(ListControl listBox, Contentum.eBusinessDocuments.Result result, List<string> columnsName, string beginSeperator, string endSeperator
            , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
        {
            if (listBox == null || columnsName == null || columnsName.Count == 0) return;

            ResultError resultError = new ResultError(result);

            if (resultError.IsError)
            {
                // Hiba kezeles
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
                if (parentErrorUpdatePanel != null)
                {
                    parentErrorUpdatePanel.Update();
                }
            }
            else
            {
                try
                {
                    listBox.Items.Clear();
                    DataSet ds = result.Ds;

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        String id = ((Guid)row["Id"]).ToString();
                        String text = String.Empty;

                        for (int i = 0; i < columnsName.Count; i++)
                        {
                            string columnName = columnsName[i];
                            text += (String)row[columnName];

                            if (i < columnsName.Count - 1)
                            {
                                text += beginSeperator;
                            }

                            if (i > 0)
                            {
                                text += endSeperator;
                            }

                        }

                        listBox.Items.Add(new ListItem(text, id));
                    }
                }
                catch (Exception)
                {
                    ResultError.DisplayErrorOnErrorPanel(errorPanel, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"),
                        "");
                }
            }
        }

        /// <summary>
        /// T tipusú elemeket adja vissza egy megadottvezérlõ fában
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="controls"></param>
        /// <returns></returns>
        public static IEnumerable<T> FindControls<T>(ControlCollection controls) where T : Control
        {
            if (controls != null && controls.Count > 0)
            {
                foreach (Control control in controls)
                {
                    if (control.GetType() == typeof(T) || control.GetType().IsSubclassOf(typeof(T)))
                    {
                        yield return control as T;
                    }

                    foreach (T childControl in FindControls<T>(control.Controls))
                    {
                        yield return childControl;
                    }

                }
            }
        }

        public static T FindParentControl<T>(Control control) where T : Control
        {
            if (control == null) return null;

            Control parent = control;

            while (parent != null)
            {
                if (parent is T)
                {
                    return parent as T;
                }

                parent = parent.Parent;
            }

            return null;
        }


        public static void SetGridViewDateTime(GridViewRowEventArgs e, string labelDateId)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label labelDate = (Label)e.Row.FindControl(labelDateId);
                if (labelDate != null)
                {
                    string sDate = labelDate.Text.Trim();
                    labelDate.Text = GetShortDateTimeString(sDate);
                }
            }
        }

        public static string GetShortDateTimeString(string dateString, bool always)
        {
            if (!String.IsNullOrEmpty(dateString))
            {
                DateTime date;
                if (DateTime.TryParse(dateString, out date))
                {
                    if (always)
                    {
                        return date.ToShortDateString();
                    }
                    else
                    {
                        if (date.Hour > 0 || date.Minute > 0)
                        {
                            return date.ToString("g");
                        }
                        else
                        {
                            return date.ToShortDateString();
                        }
                    }
                }
            }
            return dateString;
        }

        public static string GetShortDateTimeString(string dateString)
        {
            return GetShortDateTimeString(dateString, true);
        }

        public static class JegyzekTipus
        {
            public static readonly ListItem SelejtezesiJegyzek = new ListItem("Selejtezési jegyzék", "S");
            public static readonly ListItem LeveltariAtadasJegyzek = new ListItem("Levéltári átadás jegyzék", "L");
            public static readonly ListItem EgyebSzervezetAtadasJegyzek = new ListItem("Egyéb szervezetnek átadási jegyzék", "E");

            static ListItem CloneListItem(ListItem li)
            {
                return new ListItem(li.Text, li.Value);
            }

            public static void FillDropDownList(DropDownList list)
            {
                list.Items.Clear();
                list.Items.Add(CloneListItem(SelejtezesiJegyzek));
                list.Items.Add(CloneListItem(LeveltariAtadasJegyzek));
                list.Items.Add(CloneListItem(EgyebSzervezetAtadasJegyzek));
            }

            public static void FillDropDownListWithEmptyValue(DropDownList list)
            {
                list.Items.Clear();
                list.Items.Add(new ListItem("[Nincs kiválasztva]", ""));
                list.Items.Add(CloneListItem(SelejtezesiJegyzek));
                list.Items.Add(CloneListItem(LeveltariAtadasJegyzek));
                list.Items.Add(CloneListItem(EgyebSzervezetAtadasJegyzek));
            }

            public static void SetGridViewRow(GridViewRowEventArgs e, string labelJegyzekTipusId)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label labelJegyzekTipus = (Label)e.Row.FindControl(labelJegyzekTipusId);
                    if (labelJegyzekTipus != null)
                    {
                        if (labelJegyzekTipus.Text == SelejtezesiJegyzek.Value)
                        {
                            labelJegyzekTipus.Text = SelejtezesiJegyzek.Text;
                        }
                        else if (labelJegyzekTipus.Text == LeveltariAtadasJegyzek.Value)
                        {
                            labelJegyzekTipus.Text = LeveltariAtadasJegyzek.Text;
                        }
                        else if (labelJegyzekTipus.Text == EgyebSzervezetAtadasJegyzek.Value)
                        {
                            labelJegyzekTipus.Text = EgyebSzervezetAtadasJegyzek.Text;
                        }
                    }
                }
            }
        }

        #region Dátum ellenõrzés

        public static string GetInvalidTimeFormatMessage(string fieldName)
        {
            return String.Format(Contentum.eUtility.Resources.Error.GetString("UISpecDateTimeFormatError"), fieldName);
        }

        public static string GetRequeredTimeMessage(string fieldName)
        {
            return String.Format(Contentum.eUtility.Resources.Error.GetString("UISpecRequiredTimeError"), fieldName);
        }

        public static string GetTimeIsGraterThenNowMessage(string fieldName)
        {
            return String.Format(Contentum.eUtility.Resources.Error.GetString("UISpecMaUtaniDatum"), fieldName);
        }

        public static string GetTimeIsLesserThenNowMessage(string fieldName)
        {
            return String.Format(Contentum.eUtility.Resources.Error.GetString("UISpecMaElottiDatum"), fieldName);
        }

        public static DateTime GetDateTimeFromString(string date, string fieldName)
        {
            string sDate = date.Trim();
            DateTime dtDate;

            if (DateTime.TryParse(sDate, out dtDate))
            {
                return dtDate;
            }
            else
            {
                throw new FormatException(GetInvalidTimeFormatMessage(fieldName));
            }
        }

        public static void CheckDateTimeIsFutureDate(DateTime date, string fieldName)
        {
            DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            if (date < now)
            {
                throw new FormatException(GetTimeIsLesserThenNowMessage(fieldName));
            }
        }

        public static void CheckDateTimeIsPastDate(DateTime date, string fieldName)
        {
            if (date > DateTime.Now)
            {
                throw new FormatException(GetTimeIsGraterThenNowMessage(fieldName));
            }
        }

        #endregion

        //A kód a microsoft ajax extension-ból lett kimásolva, ezért jó, hogy nyílt forráskódú
        //Aszinkron postback kiderítése az oldal PreInit fázisában, amikor a scriptmanager még nincs inicializálva
        public static bool IsAsyncPostBackRequest(NameValueCollection headers)
        {
            string[] headerValues = headers.GetValues("X-MicrosoftAjax");
            if (headerValues != null)
            {
                for (int i = 0; i < headerValues.Length; i++)
                {
                    string[] headerContents = headerValues[i].Split(',');
                    for (int j = 0; j < headerContents.Length; j++)
                    {
                        if (headerContents[j].Trim() == "Delta=true")
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        //Aszinkron postback esetén javascriptes átírányítás a hiba elkerülése végett
        public static void RedirectPage(Page page, string Url)
        {
            //if (IsAsyncPostBackRequest(page.Request.Headers))
            //{
            //    RedirectPageWithJavascript(page, Url);
            //}
            //else
            //{
            page.Response.Redirect(Url, true);
            //}
        }

        //Oldal átírányítása kliens oldali javascript kóddal
        public static void RedirectPageWithJavascript(Page page, string Url)
        {
            if (page == null || String.IsNullOrEmpty(Url)) return;

            Url = page.ResolveUrl(Url);

            string js = JavaScripts.GetRedirectPageScript(Url);

            ScriptManager.RegisterStartupScript(page, page.GetType(), "JavascriptRedirect", js, true);
        }

        private static CustomStack<string> GetPageHistory(Page page)
        {
            if (page.Session[Constants.SessionNames.PageHistory] == null || !(page.Session[Constants.SessionNames.PageHistory] is CustomStack<string>))
            {
                page.Session[Constants.SessionNames.PageHistory] = new CustomStack<string>(Constants.HistoryLength);
            }

            return page.Session[Constants.SessionNames.PageHistory] as CustomStack<string>;
        }


        public static void AddPageToHistory(Page page)
        {
            //Csak az új oldalt adjuk a history-hoz
            CustomStack<string> history = GetPageHistory(page);
            string url = page.Request.Url.AbsoluteUri;
            if (history.Length > 0)
            {
                string lastUrl = history.Peek();

                if (lastUrl != url)
                {
                    history.Push(url);
                }
            }
            else
            {
                history.Push(url);
            }
        }

        public static string GetPageFromHistory(Page page, int backStep)
        {
            CustomStack<string> history = GetPageHistory(page);

            if (history.Length == 0)
                return null;

            if (backStep < 0)
                backStep = 0;

            if (backStep > history.Length - 1)
                return null;

            string url = null;

            while (backStep > 0)
            {
                if (history.Length == 1)
                {
                    url = history.Pop();
                    return url;
                }

                history.Pop();
                backStep--;
            }

            if (history.Length > 0)
                url = history.Pop();

            return url;
        }

        public static int MarkFailedRecordsInGridView(GridView gridView, Result result)
        {
            if (result.Ds == null || result.Ds.Tables.Count == 0 || result.Ds.Tables[0].Rows.Count == 0)
                return 0;

            int recordNumber = 0;

            foreach (GridViewRow r in gridView.Rows)
            {
                CheckBox checkBox = (CheckBox)r.FindControl("check");
                if (!checkBox.Enabled)
                    continue;

                for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                {
                    if (gridView.DataKeys[r.RowIndex].Value.ToString() == result.Ds.Tables[0].Rows[i]["Id"].ToString())
                    {
                        r.BackColor = System.Drawing.Color.Red;
                        checkBox.Checked = false;
                        recordNumber++;
                        break;
                    }
                    else
                        r.BackColor = System.Drawing.Color.White;
                }
            }

            return recordNumber;
        }

        /// <summary>
        /// A Resultban szereplõ rekordoknak megfelelõ GridView sorokban a CheckBoxAndBoolCheckIfFailedPairs párokban
        /// név szerint átadott checkboxokat a pár második tagjában megadottak szerint jelöli vagy nem jelöli, ha a sor
        /// megtalálható a hibásak között
        /// </summary>
        /// <param name="gridView">A checkboxokat, illetve a resultban visszavárható rekordokat tartalmazó gridview</param>
        /// <param name="CheckBoxBoolPairs">Egy gridviewbeli checkbox megnevezésébõl és egy bool értékbõl álló párokból képzett tömb.
        /// Ha a bool érték false, és a checkbox engedélyezett, a hibás rekordban a checkbopx nem lesz megjelölve, ha true, ki lesz pipálva</param>
        /// <param name="result">A hibás sorokat tartalmazó Result objektum.</param>
        /// <returns></returns>
        public static int MarkFailedRecordsInGridView(GridView gridView, Pair[] CheckBoxNamesAndBoolCheckIfFailedPairs, Result result)
        {
            if (result.Ds == null || result.Ds.Tables.Count == 0 || result.Ds.Tables[0].Rows.Count == 0)
                return 0;

            int recordNumber = 0;

            foreach (GridViewRow r in gridView.Rows)
            {
                bool bFound = false;
                foreach (Pair pair in CheckBoxNamesAndBoolCheckIfFailedPairs)
                {
                    try
                    {
                        string checkboxName = pair.First.ToString();
                        bool bCheckIfFailed = (bool)pair.Second;

                        CheckBox checkBox = (CheckBox)r.FindControl(checkboxName);
                        if (checkBox != null)
                        {
                            if (!checkBox.Enabled)
                                continue;

                            for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                            {
                                if (gridView.DataKeys[r.RowIndex].Value.ToString() == result.Ds.Tables[0].Rows[i]["Id"].ToString())
                                {
                                    r.BackColor = System.Drawing.Color.Red;
                                    checkBox.Checked = bCheckIfFailed;
                                    bFound = true;
                                    break;
                                }
                                else
                                    r.BackColor = System.Drawing.Color.White;
                            }
                        }
                    }
                    catch
                    {
                        // do nothing just continue   
                    }
                }

                if (bFound)
                {
                    recordNumber++;
                }
            }

            return recordNumber;
        }


        public static string GetLoginDataQueryString(Page page)
        {
            string query = FelhasznaloProfil.ConvertToQueryString(page);
            return query;
        }


        public static void AddCssClass(WebControl wc, string className)
        {
            if (!wc.CssClass.Contains(className))
            {
                if (!String.IsNullOrEmpty(wc.CssClass))
                {
                    wc.CssClass += " ";
                }

                wc.CssClass += className;
            }
        }

        public static void AddCssClass(IAttributeAccessor control, string className)
        {
            string cssClass = control.GetAttribute("class");

            if (String.IsNullOrEmpty(cssClass))
            {
                cssClass = className;
                control.SetAttribute("class", cssClass);
            }
            else
            {
                if (!cssClass.Contains(className))
                {
                    cssClass += " " + className;
                    control.SetAttribute("class", cssClass);
                }
            }
        }

        public static void RemoveCssClass(WebControl wc, string className)
        {
            if (wc.CssClass.Contains(className))
            {
                string spClassName = " " + className;
                if (wc.CssClass.Contains(spClassName))
                {
                    wc.CssClass = wc.CssClass.Replace(spClassName, "");
                }
                else
                {
                    wc.CssClass = wc.CssClass.Replace(className, "");
                }

                wc.CssClass = wc.CssClass.Trim();
            }
        }

        public static void RemoveCssClass(IAttributeAccessor control, string className)
        {
            string cssClass = control.GetAttribute("class");

            if (!String.IsNullOrEmpty(cssClass))
            {
                if (cssClass.Contains(className))
                {
                    string spClassName = " " + className;
                    if (cssClass.Contains(spClassName))
                    {
                        cssClass = cssClass.Replace(spClassName, "");
                    }
                    else
                    {
                        cssClass = cssClass.Replace(className, "");
                    }

                    cssClass = cssClass.Trim();

                    control.SetAttribute("class", className);
                }
            }
        }

        #region Menühöz adatstruktúra

        public static class Menu
        {
            public class MainMenuStruct
            {
                public string name;
                public string id;
                public string imageUrl;
                private List<SubMenuStruct> _subMenus;
                public List<SubMenuStruct> subMenus
                {
                    get { return _subMenus; }
                }

                public void AddSubMenu(SubMenuStruct subMenuStruct)
                {
                    if (_subMenus == null)
                    {
                        _subMenus = new List<SubMenuStruct>();
                    }
                    _subMenus.Add(subMenuStruct);
                }
            }

            public class SubMenuStruct
            {
                public string name;
                public string id;
                //public CheckBox checkBox; // admin módban használatos
                public string url;
                private List<SubMenuStruct> _subMenus;
                public List<SubMenuStruct> subMenus
                {
                    get { return _subMenus; }
                }

                public void AddSubMenu(SubMenuStruct subMenuStruct)
                {
                    if (_subMenus == null)
                    {
                        _subMenus = new List<SubMenuStruct>();
                    }
                    _subMenus.Add(subMenuStruct);
                }
            }
        }
        #endregion

        public List<string> GetGridViewSelectedAndExecutableRows(GridView ParentGridView, string executableCheckBoxID, eUIControls.eErrorPanel ParenteErrorPanel, UpdatePanel ParentErrorUpdatePanel)
        {
            if (ParentGridView == null || ParenteErrorPanel == null)
            {
                return null;
            }
            // törölhetõ elemek listája
            List<string> selectedItemsList = new List<string>();

            // van-e definiálva DataKey:
            if (ParentGridView.Rows.Count != ParentGridView.DataKeys.Count)
            {
                // nincs DataKey, hiba:

                ParenteErrorPanel.Header = Contentum.eUtility.Resources.Error.GetString("ErrorLabel") + ": " +
                    Contentum.eUtility.Resources.Error.GetString("UIDeleteError");
                ParenteErrorPanel.Body = Contentum.eUtility.Resources.Error.GetString("UIDeleteError_Text");
                ParenteErrorPanel.Visible = true;
                if (ParentErrorUpdatePanel != null)
                {
                    ParentErrorUpdatePanel.Update();
                }
            }
            else
            {
                for (int i = 0; i < ParentGridView.Rows.Count; i++)
                {
                    CheckBox checkB = (CheckBox)ParentGridView.Rows[i].FindControl("check");
                    CheckBox cbExecutable = (CheckBox)ParentGridView.Rows[i].FindControl(executableCheckBoxID);
                    bool executable = true;
                    if (cbExecutable != null && !cbExecutable.Checked)
                    {
                        executable = false;
                    }
                    if (checkB.Checked && executable)
                    {

                        string itemId = ParentGridView.DataKeys[i].Value.ToString();
                        // felvesszük a listába az id-t:
                        selectedItemsList.Add(itemId);
                    }
                }
                return selectedItemsList;
            }
            return null;
        }

        /// <summary>
        /// Az adatbázisban email-címmel együtt tárolt csoportnevekbõl törli az email-címet
        /// </summary>
        /// <param name="csoportnev"></param>
        /// <returns></returns>
        public static string RemoveEmailAddressFromCsoportNev(string csoportnev)
        {
            if (String.IsNullOrEmpty(csoportnev))
            {
                return csoportnev;
            }
            else
            {
                return System.Text.RegularExpressions.Regex.Replace(csoportnev, @" ?\(.*@.*\)", "");
            }
        }

        public interface ILovListTextBox
        {
            string Text { set; get; }
            string Id_HiddenField { set; get; }
            bool Validate { get; set; }
            string ValidationGroup { get; set; }
            bool Enabled { get; set; }
            bool ReadOnly { get; set; }
            bool SearchMode { get; set; }

            void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel);
        }

        public static void SetFeladatInfo(GridViewRowEventArgs e, string ObjektumTipus)
        {
            SetFeladatInfo(e, ObjektumTipus, String.Empty);
        }

        public static void SetFeladatInfo(GridViewRowEventArgs e, string ObjektumTipus, string ObjektumId)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;

                ImageButton FeladatImage = (ImageButton)e.Row.FindControl("FeladatImage");

                if (FeladatImage != null && drw != null)
                {
                    SetFeladatInfo(drw.Row, FeladatImage, ObjektumTipus, ObjektumId);
                }
            }
        }

        public static void SetFeladatInfo(DataRow row, ImageButton FeladatImage, string ObjektumTipus)
        {
            SetFeladatInfo(row, FeladatImage, ObjektumTipus, String.Empty);
        }

        public static void SetFeladatInfo(Page page, ImageButton FeladatImage, string ObjektumTipus, string ObjektumId)
        {
            if (page == null || FeladatImage == null || String.IsNullOrEmpty(ObjektumTipus) && String.IsNullOrEmpty(ObjektumId)) return;

            EREC_HataridosFeladatokService svc = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
            ExecParam xpm = UI.SetExecParamDefault(page);
            Result res = svc.GetCount(xpm, ObjektumId);
            if (!res.IsError)
            {
                DataRow row = res.Ds.Tables[0].Rows[0];
                SetFeladatInfo(row, FeladatImage, ObjektumTipus, ObjektumId);
            }
        }

        public static void SetFeladatInfo(DataRow row, ImageButton FeladatImage, string ObjektumTipus, string ObjektumId)
        {
            if (row != null && FeladatImage != null)
            {
                FeladatImage.OnClientClick = "";
                FeladatImage.ImageUrl = "~/images/hu/ikon/feladat.gif";
                FeladatImage.CssClass = "highlightit";
                FeladatImage.Style.Add(HtmlTextWriterStyle.Height, "20px");
                FeladatImage.Style.Add(HtmlTextWriterStyle.Width, "20px");
                FeladatImage.Style.Add(HtmlTextWriterStyle.BorderColor, "red");
                FeladatImage.Style.Add(HtmlTextWriterStyle.BorderStyle, "solid");
                FeladatImage.Style.Add(HtmlTextWriterStyle.BorderWidth, "0px");
                FeladatImage.Visible = false;

                string feladatCountAndMaxPriority = String.Empty;

                if (row.Table.Columns.Contains("FeladatCount") && row["FeladatCount"] != null)
                {
                    feladatCountAndMaxPriority = row["FeladatCount"].ToString();
                }

                if (!String.IsNullOrEmpty(feladatCountAndMaxPriority))
                {
                    string[] feladatCountsAndMaxPriorityArray = feladatCountAndMaxPriority.Split(',');
                    string feladatCount = feladatCountsAndMaxPriorityArray[0];
                    string liveFeladatCount = String.Empty;
                    if (feladatCountsAndMaxPriorityArray.Length > 1)
                    {
                        liveFeladatCount = feladatCountsAndMaxPriorityArray[1];
                    }
                    string maxPriorityString = String.Empty;
                    if (feladatCountsAndMaxPriorityArray.Length > 2)
                    {
                        maxPriorityString = feladatCountsAndMaxPriorityArray[2];
                    }
                    int count = 0;
                    Int32.TryParse(feladatCount, out count);
                    if (String.IsNullOrEmpty(liveFeladatCount))
                    {
                        liveFeladatCount = feladatCount;
                    }
                    if (count > 0)
                    {
                        FeladatImage.Visible = true;
                        FeladatImage.OnClientClick = "return false;";
                        FeladatImage.AlternateText = String.Format(Resources.Form.GetString("UI_FeladatImage_ToolTip_WithCount"), liveFeladatCount, feladatCount);
                        FeladatImage.ToolTip = String.Format(Resources.Form.GetString("UI_FeladatImage_ToolTip_WithCount"), liveFeladatCount, feladatCount);

                        // set "link"
                        string id = String.Empty;

                        if (String.IsNullOrEmpty(ObjektumId))
                        {
                            if (row["Id"] != null)
                            {
                                id = row["Id"].ToString();
                            }
                        }
                        else
                        {
                            id = ObjektumId;
                        }

                        if (!String.IsNullOrEmpty(id) && !String.IsNullOrEmpty(ObjektumTipus))
                        {

                            FeladatImage.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("Feladatok.aspx?Command=Modify&"
                                + QueryStringVars.ObjektumId + "=" + id + "&" + QueryStringVars.ObjektumType + "=" + ObjektumTipus
                                + "&" + QueryStringVars.Startup + "=" + "FeladatImage",
                                "", Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);
                        }

                        if (!String.IsNullOrEmpty(maxPriorityString))
                        {
                            int maxPriority = 0;
                            Int32.TryParse(maxPriorityString, out maxPriority);
                            if (maxPriority > 0)
                            {
                                double borderWidth = 0;
                                borderWidth = (double)maxPriority / 10;
                                borderWidth--;
                                borderWidth = Math.Max(borderWidth, 0);
                                FeladatImage.Style[HtmlTextWriterStyle.BorderWidth] = (int)Math.Round(borderWidth) + "px";
                            }
                        }
                    }
                    else
                    {
                        FeladatImage.Visible = false;
                    }
                }
            }
        }

        public static string RenderControl(Control control)
        {
            if (control.Visible)
            {
                StringWriter stringWriter = new StringWriter();
                Html32TextWriter textWriter = new Html32TextWriter(stringWriter);
                control.RenderControl(textWriter);
                return stringWriter.ToString();
            }

            return String.Empty;
        }

        /// <summary>
        /// GetCheckedCheckBoxesInGridView
        /// </summary>
        /// <param name="gridView"></param>
        /// <returns></returns>
        public static string[] GetCheckedCheckBoxIdsInGridView(GridView gridView)
        {
            if (gridView == null || gridView.Rows.Count < 1)
                return null;

            List<string> result = new List<string>();
            foreach (GridViewRow r in gridView.Rows)
            {
                CheckBox checkBox = (CheckBox)r.FindControl("check");
                if (checkBox != null)
                {
                    if (!checkBox.Enabled)
                        continue;

                    if (checkBox.Checked)
                        result.Add(checkBox.Text);
                }
            }
            return result.ToArray();
        }

        /// <summary>
        /// HiddenField mezõ értékét növeli 1-gyel (ha az értéke szám egyáltalán)
        /// </summary>
        /// <param name="hiddenfield"></param> 
        public static void IncrementHiddenFieldValue(HiddenField hiddenfield)
        {
            try
            {
                int value = Int32.Parse(hiddenfield.Value);
                value++;
                hiddenfield.Value = value.ToString();
            }
            catch
            {
                // ha hiba van, marad a régi érték
            }
        }
    }
}