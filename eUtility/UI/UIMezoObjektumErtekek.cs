using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Contentum.eUIControls;
using Contentum.eBusinessDocuments;
using System.Web.UI;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data;
using System.Xml;

namespace Contentum.eUtility
{
    public class UIMezoObjektumErtekek
    {
        /// <summary>
        /// Lek�ri a megadott id-j� objektumot, �s m�g m�dos�tja az utols� haszn�lat idej�t is
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <param name="Felhasznalo_Id"></param>
        /// <returns></returns>
        public static Contentum.eBusinessDocuments.KRT_UIMezoObjektumErtekek Get(ExecParam execParam, eErrorPanel ErrorPanel)
        {            
            Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService _KRT_UIMezoObjektumErtekekService = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();
            Contentum.eBusinessDocuments.KRT_UIMezoObjektumErtekek _KRT_UIMezoObjektumErtekek = null;

            Contentum.eBusinessDocuments.Result result = new Contentum.eBusinessDocuments.Result();
            result = _KRT_UIMezoObjektumErtekekService.Get(execParam);

            if (result.Record != null)
            {
                _KRT_UIMezoObjektumErtekek = (Contentum.eBusinessDocuments.KRT_UIMezoObjektumErtekek)result.Record;

                // m�dos�tjuk az utols� haszn�lat idej�t
                int CurrentVersion = Int32.Parse(_KRT_UIMezoObjektumErtekek.Base.Ver);
                ResultError resUpdate = UpdateUtolsoHasznalatIdo(execParam, CurrentVersion);

                if (!resUpdate.IsError)
                {
                    try
                    {
                        int newVer = CurrentVersion + 1;
                        _KRT_UIMezoObjektumErtekek.Base.Ver = newVer.ToString();

                        return _KRT_UIMezoObjektumErtekek;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
                //return XmlFunction.XmlToObject(_KRT_UIMezoObjektumErtekek.TemplateXML, type);
            }
            else if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel, result);
            }
            return null;
        }

        public static void FillDropDownList(ExecParam execParam, DropDownList ddl, Type type, Contentum.eUIControls.eErrorPanel errorPanel)
        {
            if (ddl == null || type == null || execParam == null || errorPanel == null) return;

            Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService _KRT_UIMezoObjektumErtekekService = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();

            Contentum.eQuery.BusinessDocuments.KRT_UIMezoObjektumErtekekSearch searchObj = new Contentum.eQuery.BusinessDocuments.KRT_UIMezoObjektumErtekekSearch();
            searchObj.Felhasznalo_Id.Value = execParam.Felhasznalo_Id;
            searchObj.Felhasznalo_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            searchObj.Felhasznalo_Id.GroupOperator = Contentum.eQuery.Query.Operators.or;
           
            if (type == null)
            {
                return;
            }
            else
            {
                searchObj.TemplateTipusNev.Value = type.Name;
                searchObj.TemplateTipusNev.Operator = Contentum.eQuery.Query.Operators.equals;
            }


            Contentum.eBusinessDocuments.Result result = _KRT_UIMezoObjektumErtekekService.GetAll(execParam, searchObj);

            if (result.Ds != null)
            {
                ddl.Items.Clear();
                for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                {
                    ddl.Items.Add(new ListItem(result.Ds.Tables[0].Rows[i]["Nev"].ToString(), result.Ds.Tables[0].Rows[i]["Id"].ToString()));
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillTemplateList"));
            }
        }

        /// <summary> 
        /// </summary>
        /// <returns>A besz�rt rekord id-ja, ha a m�velet sikeres; "", ha a m�velet sikertelen</returns>
        public static string Insert(ExecParam execParam, Object objektum, string templateTipusNev, string nev, string alapertelmezett, string publikus, string szervezetId, eErrorPanel ErrorPanel)
        {
            if (objektum == null || templateTipusNev == null || nev == null || execParam == null) return "";

            Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService _KRT_UIMezoObjektumErtekekService = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();

            Contentum.eBusinessDocuments.KRT_UIMezoObjektumErtekek _KRT_UIMezoObjektumErtekek = new Contentum.eBusinessDocuments.KRT_UIMezoObjektumErtekek();
            _KRT_UIMezoObjektumErtekek.Nev = nev;
            _KRT_UIMezoObjektumErtekek.TemplateTipusNev = templateTipusNev;
            _KRT_UIMezoObjektumErtekek.TemplateXML = XmlFunction.ObjectToXml(objektum, false);
            _KRT_UIMezoObjektumErtekek.Felhasznalo_Id = execParam.Felhasznalo_Id;

            if (!string.IsNullOrEmpty(szervezetId))
                _KRT_UIMezoObjektumErtekek.Szervezet_Id = szervezetId;

            if (!string.IsNullOrEmpty(alapertelmezett))
            {
                _KRT_UIMezoObjektumErtekek.Alapertelmezett = alapertelmezett;
                _KRT_UIMezoObjektumErtekek.UtolsoHasznIdo = DateTime.Now.ToString();
            }

            if (!string.IsNullOrEmpty(publikus))
            {
                _KRT_UIMezoObjektumErtekek.Publikus = publikus;
                _KRT_UIMezoObjektumErtekek.UtolsoHasznIdo = DateTime.Now.ToString();
            }

            Contentum.eBusinessDocuments.Result result = new Contentum.eBusinessDocuments.Result();

            result = _KRT_UIMezoObjektumErtekekService.Insert(execParam, _KRT_UIMezoObjektumErtekek);


            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                return result.Uid;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(ErrorPanel, result);
                return "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objektum">Elmentend� objektum</param>
        /// <param name="type">Objektum t�pusa</param>
        /// <param name="id">rekord id</param>
        /// <param name="Ver">M�dos�tand� rekord verzi�sz�ma</param>
        /// <returns></returns>
        public static ResultError Update(ExecParam execParam, Object objektum, int Ver, string Alapertelmezett)
        {
            Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService _KRT_UIMezoObjektumErtekekService = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();

            Contentum.eBusinessDocuments.KRT_UIMezoObjektumErtekek _KRT_UIMezoObjektumErtekek = new Contentum.eBusinessDocuments.KRT_UIMezoObjektumErtekek();

            // csak a sz�ks�ges mez�kre menjen az update:
            _KRT_UIMezoObjektumErtekek.Updated.SetValueAll(false);
            _KRT_UIMezoObjektumErtekek.Base.Updated.SetValueAll(false);

            _KRT_UIMezoObjektumErtekek.TemplateXML = XmlFunction.ObjectToXml(objektum, false);
            _KRT_UIMezoObjektumErtekek.Updated.TemplateXML = true;

            _KRT_UIMezoObjektumErtekek.UtolsoHasznIdo = DateTime.Now.ToString();
            _KRT_UIMezoObjektumErtekek.Updated.UtolsoHasznIdo = true;

            if (!String.IsNullOrEmpty(Alapertelmezett))
            {
                _KRT_UIMezoObjektumErtekek.Alapertelmezett = Alapertelmezett;
                _KRT_UIMezoObjektumErtekek.Updated.Alapertelmezett = true;
            }

            _KRT_UIMezoObjektumErtekek.Base.Ver = Ver.ToString();
            _KRT_UIMezoObjektumErtekek.Base.Updated.Ver = true;

            Contentum.eBusinessDocuments.Result result_update = new Contentum.eBusinessDocuments.Result();
            result_update = _KRT_UIMezoObjektumErtekekService.Update(execParam, _KRT_UIMezoObjektumErtekek);
            return new ResultError(result_update);

        }
        #region CR3194 - Kisherceg megoszt�s
        public static Result Share(ExecParam execParam, KRT_UIMezoObjektumErtekek rekord)
        {
            Contentum.eBusinessDocuments.Result result_Share = new Contentum.eBusinessDocuments.Result();
            using (Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService _KRT_UIMezoObjektumErtekekService = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService())
            {
                result_Share = _KRT_UIMezoObjektumErtekekService.MegosztasWithSzemelyOrSzervezet(execParam, rekord);
            }
            return result_Share;
        }
        #endregion CR3194 - Kisherceg megoszt�s
        public static ResultError UpdateUtolsoHasznalatIdo(ExecParam execParam, int CurrentVersion)
        {
            Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService service = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();

            Contentum.eBusinessDocuments.KRT_UIMezoObjektumErtekek objektumErtekek = new Contentum.eBusinessDocuments.KRT_UIMezoObjektumErtekek();

            // csak a sz�ks�ges mez�kre menjen az update:
            objektumErtekek.Updated.SetValueAll(false);
            objektumErtekek.Base.Updated.SetValueAll(false);

            objektumErtekek.UtolsoHasznIdo = DateTime.Now.ToString();
            objektumErtekek.Updated.UtolsoHasznIdo = true;

            objektumErtekek.Base.Ver = CurrentVersion.ToString();
            objektumErtekek.Base.Updated.Ver = true;

            Contentum.eBusinessDocuments.Result res = new Contentum.eBusinessDocuments.Result();
            res = service.Update(execParam, objektumErtekek);

            return new ResultError(res);
        }

        public static ResultError UpdateAlapertelmezett(ExecParam execParam,string Alapertelmezett)
        {
            Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService service = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();

            Contentum.eBusinessDocuments.KRT_UIMezoObjektumErtekek objektumErtekek = new Contentum.eBusinessDocuments.KRT_UIMezoObjektumErtekek();

            Result resGet = service.Get(execParam);

            if (!String.IsNullOrEmpty(resGet.ErrorCode))
            {
                return new ResultError(resGet);
            }

            string Version = (resGet.Record as KRT_UIMezoObjektumErtekek).Base.Ver;

            // csak a sz�ks�ges mez�kre menjen az update:
            objektumErtekek.Updated.SetValueAll(false);
            objektumErtekek.Base.Updated.SetValueAll(false);

            if (Alapertelmezett != Constants.Database.Yes)
            {
                Alapertelmezett = Constants.Database.No;
            }

            objektumErtekek.Alapertelmezett = Alapertelmezett;
            objektumErtekek.Updated.Alapertelmezett = true;

            objektumErtekek.Base.Ver = Version;
            objektumErtekek.Base.Updated.Ver = true;

            Contentum.eBusinessDocuments.Result res = new Contentum.eBusinessDocuments.Result();
            res = service.Update(execParam, objektumErtekek);

            return new ResultError(res);
        }

        public static ResultError Invalidate(ExecParam execParam)
        {
            //ServiceFactory sf = new ServiceFactory();

            Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService _KRT_UIMezoObjektumErtekekService = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();
            Contentum.eBusinessDocuments.Result result = _KRT_UIMezoObjektumErtekekService.Invalidate(execParam);

            return new ResultError(result);
        }

        public static object GetDeafultSearchObject(Page page, string sessionName, Type searchObjectType)
        {
            Logger.DebugStart();
            Logger.Debug("UIMezoObjektumErtekek osztaly GetDeafultSearchObject metodus start");
            Logger.Debug("--Param�terek--");
            Logger.Debug("page: " + page.Request.Url.ToString());
            Logger.Debug("sessionName: " + sessionName);
            Logger.Debug("searchObjectType: " + searchObjectType.FullName);

            List<string> defaultTemplatesList;
            KRT_UIMezoObjektumErtekekService service = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();
            ExecParam xpm = UI.SetExecParamDefault(page, new ExecParam());
            Logger.Debug("A felhasznalo id: " + xpm.Felhasznalo_Id);

            if (page.Session[Constants.SessionNames.DefaultTemplatesList] != null && page.Session[Constants.SessionNames.DefaultTemplatesList] is List<string>)
            {
                Logger.Debug("Session tartalmazza a default template listat");
                defaultTemplatesList = page.Session[Constants.SessionNames.DefaultTemplatesList] as List<string>;

                if (defaultTemplatesList.Contains(sessionName))
                {
                    Logger.Debug(String.Format("A keres�si objektumhoz tartozik default template: {0}", sessionName));

                    KRT_UIMezoObjektumErtekekSearch search = new KRT_UIMezoObjektumErtekekSearch();

                    search.Felhasznalo_Id.Value = xpm.Felhasznalo_Id;
                    search.Felhasznalo_Id.Operator = Query.Operators.equals;

                    search.Alapertelmezett.Value = Constants.Database.Yes;
                    search.Alapertelmezett.Operator = Query.Operators.equals;

                    search.TemplateTipusNev.Value = sessionName;
                    search.TemplateTipusNev.Operator = Query.Operators.equals;

                    Logger.Debug("KRT_UIMezoObjektumErtekekService GetAll start");
                    Result res = service.GetAll(xpm, search);

                    if (!String.IsNullOrEmpty(res.ErrorCode))
                    {
                        Logger.Error(String.Format("KRT_UIMezoObjektumErtekekService GetAll hiba: {0},{1}", res.ErrorCode, res.ErrorMessage));
                    }

                    Logger.Debug("KRT_UIMezoObjektumErtekekService GetAll visszatert sorok szama: " + res.Ds.Tables[0].Rows.Count);
                    Logger.Debug("KRT_UIMezoObjektumErtekekService GetAll end");

                    if (res.Ds.Tables[0].Rows.Count > 0)
                    {
                        string templateXml = res.Ds.Tables[0].Rows[0]["TemplateXML"].ToString();
                        object SearchObject = XmlFunction.XmlToObject(templateXml, searchObjectType);
                        Logger.Debug("UIMezoObjektumErtekek osztaly GetDeafultSearchObject metodus end");
                        return SearchObject;
                    }
                    else
                    {
                        Logger.Error("KRT_UIMezoObjektumErtekekService GetAll nulla rekorddal t�rt vissza");
                        return null;
                    }


                }
            }
            else
            {
                Logger.Debug("Session nem tartalmazza a default template listat");
                defaultTemplatesList = new List<string>();

                KRT_UIMezoObjektumErtekekSearch search = new KRT_UIMezoObjektumErtekekSearch();

                search.Felhasznalo_Id.Value = xpm.Felhasznalo_Id;
                search.Felhasznalo_Id.Operator = Query.Operators.equals;

                search.Alapertelmezett.Value = Constants.Database.Yes;
                search.Alapertelmezett.Operator = Query.Operators.equals;

                Logger.Debug("KRT_UIMezoObjektumErtekekService GetAll start");
                Result res = service.GetAll(xpm, search);

                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    Logger.Error(String.Format("KRT_UIMezoObjektumErtekekService GetAll hiba: {0},{1}", res.ErrorCode, res.ErrorMessage));
                    return null;
                }

                Logger.Debug("KRT_UIMezoObjektumErtekekService GetAll visszatert sorok szama: " + res.Ds.Tables[0].Rows.Count);
                Logger.Debug("KRT_UIMezoObjektumErtekekService GetAll end");

                string templateXml = null;
                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    defaultTemplatesList.Add(row["TemplateTipusNev"].ToString());
                    if (sessionName == row["TemplateTipusNev"].ToString())
                    {
                        Logger.Debug(String.Format("A keres�si objektumhoz tartozik default template: {0}", sessionName));
                        templateXml = row["TemplateXML"].ToString();
                    }
                }

                //lista elment�se sessionba
                page.Session[Constants.SessionNames.DefaultTemplatesList] = defaultTemplatesList;

                #region Oszlop-l�that�s�g template-ek bet�lt�se Session-be (ha m�g nincs bent a Session-ben)

                if (page.Session[Constants.SessionNames.DefaultColumnsVisibilityDictionary] == null)
                {
                    page.Session[Constants.SessionNames.DefaultColumnsVisibilityDictionary] = CreateColumnsVisibilityDictionary(res.Ds);
                }

                #endregion

                // BUG_8359
                #region Rendezetts�g template-ek bet�lt�se Session-be (ha m�g nincs bent a Session-ben)

                if (page.Session[Constants.SessionNames.DefaultOrderByDictionary] == null)
                {
                    page.Session[Constants.SessionNames.DefaultOrderByDictionary] = CreateOrderByDictionary(res.Ds);
                }

                #endregion

                if (!String.IsNullOrEmpty(templateXml))
                {
                    object SearchObject = XmlFunction.XmlToObject(templateXml, searchObjectType);
                    Logger.Debug("UIMezoObjektumErtekek osztaly GetDeafultSearchObject metodus start");
                    return SearchObject;
                }
            }

            Logger.Debug("A keres�si objektumhoz nem tartozik default template: " + sessionName);
            Logger.Debug("UIMezoObjektumErtekek osztaly GetDeafultSearchObject metodus end");
            return null;
        }


        #region ColumnsVisibility

        public class GridViewColumnsVisibility
        {
            public struct ColumnProperties
            {
                public bool visible;
                public string headerText;
                public string sortExpression;

                public ColumnProperties(bool _visible, string _headerText, string _sortExpression)
                {
                    this.visible = _visible;
                    this.headerText = _headerText;
                    this.sortExpression = _sortExpression;
                }
            }

            private string uiMezoObjektumErtekek_Id = String.Empty;
            public string UIMezoObjektumErtekek_Id
            {
                get { return uiMezoObjektumErtekek_Id; }
            }

            private Dictionary<int, ColumnProperties> values = new Dictionary<int, ColumnProperties>();
            public Dictionary<int, ColumnProperties> Values
            {
                get { return values; }
            }

            /// <summary>
            /// Konstruktor, meg kell adni a KRT_UIMezoObjektumErtekek rekord Id-j�t, �s az xml-t, amiben az adatok vannak t�rolva, megfelel� form�tumban
            /// </summary>
            /// <param name="templateId"></param>
            /// <param name="templateXml"></param>
            public GridViewColumnsVisibility(string templateId, string templateXml)
            {
                uiMezoObjektumErtekek_Id = templateId;

                #region templateXml feldolgoz�sa

                /// Form�tum pl.:
                /// <GridView>
                ///     <Columns>
                ///         <Column index="6" visible="true" headerText="T�rgy" sortExpression="EREC_UgyUgyiratok.Targy" />
                ///         <Column index="8" visible="false" headerText="Vonalk�d" sortExpression="EREC_UgyUgyiratok.BARCODE" />
                ///     </Columns>
                /// </GridView>

                if (!string.IsNullOrEmpty(templateXml))
                {
                    try
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(templateXml);

                        if (xmlDoc != null)
                        {
                            XmlNodeList columnNodeList = xmlDoc.SelectNodes("./GridView/Columns/Column");
                            foreach (XmlNode columnNode in columnNodeList)
                            {
                                string indexAttr = columnNode.Attributes["index"].Value;
                                string visibleAttr = columnNode.Attributes["visible"].Value;
                                string headerText = String.Empty;
                                XmlAttribute headerTextXmlAttr = columnNode.Attributes["headerText"];
                                if (headerTextXmlAttr != null)
                                {
                                    headerText = headerTextXmlAttr.Value;
                                }
                                string sortExpression = String.Empty;
                                XmlAttribute sortExpressionXmlAttr = columnNode.Attributes["sortExpression"];
                                if (sortExpressionXmlAttr != null)
                                {
                                    sortExpression = sortExpressionXmlAttr.Value;
                                }

                                int index = Int32.Parse(indexAttr);
                                bool visible;
                                if (visibleAttr == "true")
                                {
                                    visible = true;
                                }
                                else if (visibleAttr == "false")
                                {
                                    visible = false;
                                }
                                else
                                {
                                    continue;
                                }

                                if (!this.values.ContainsKey(index))
                                {
                                    this.values.Add(index, new ColumnProperties(visible, headerText, sortExpression));
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // hiba:
                        Logger.Error("Hiba a templateXml feldolgoz�sa sor�n. TemplateXml: " + templateXml, e);
                    }
                }

                #endregion
            }
        }

        public const string ColumnsVisibilityTag = "ColumnsVisibility_";


        /// <summary>
        /// Ha van elmentett default oszlopl�that�s�g-�ll�t�s az adott list�ra, be�ll�tja az oszlopok l�that�s�g�t
        /// A visszaadott �rt�k ilyenkor true, egy�b esetben false
        /// </summary>
        /// <param name="page"></param>
        /// <param name="attachedGridView"></param>
        /// <param name="searchObjectType"></param>
        /// <param name="customSearchObjectSessionName"></param>
        /// <returns></returns>
        public static bool SetDefaultColumnsVisibilityIfExist(Page page, GridView attachedGridView, Type searchObjectType, string customSearchObjectSessionName)
        {
            if (attachedGridView == null || page == null || (searchObjectType == null && string.IsNullOrEmpty(customSearchObjectSessionName)))
            {
                return false;
            }


            string templateTipusNev;
            if (!string.IsNullOrEmpty(customSearchObjectSessionName))
            {
                templateTipusNev = ColumnsVisibilityTag + customSearchObjectSessionName;
            }
            else
            {
                templateTipusNev = ColumnsVisibilityTag + searchObjectType.Name;
            }

            Dictionary<string, GridViewColumnsVisibility> columnsVisibilityDict;

            if (page.Session[Constants.SessionNames.DefaultColumnsVisibilityDictionary] == null)
            {
                #region Elmentett oszlopl�that�s�gos adatok bet�lt�se

                KRT_UIMezoObjektumErtekekService service = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();

                KRT_UIMezoObjektumErtekekSearch search = new KRT_UIMezoObjektumErtekekSearch();

                search.Felhasznalo_Id.Value = FelhasznaloProfil.FelhasznaloId(page);
                search.Felhasznalo_Id.Operator = Query.Operators.equals;

                search.Alapertelmezett.Value = Constants.Database.Yes;
                search.Alapertelmezett.Operator = Query.Operators.equals;

                ExecParam execParam = UI.SetExecParamDefault(page);

                Result result = service.GetAll(execParam, search);
                if (result.IsError)
                {
                    // hiba:
                    Logger.Error("Hiba a default KRT_UIMezoObjektumErtekek �rt�kek lek�r�sekor (ColumnsVisibility �rt�kekhez)", execParam, result);
                    return false;
                }

                columnsVisibilityDict = CreateColumnsVisibilityDictionary(result.Ds);

                #endregion
            }
            else
            {
                columnsVisibilityDict = (Dictionary<string, GridViewColumnsVisibility>)page.Session[Constants.SessionNames.DefaultColumnsVisibilityDictionary];
            }

            if (columnsVisibilityDict != null)
            {
                if (columnsVisibilityDict.ContainsKey(templateTipusNev))
                {
                    SetGridViewColumnsVisibility(attachedGridView, columnsVisibilityDict[templateTipusNev]);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }            
        }


        private static Dictionary<string, GridViewColumnsVisibility> CreateColumnsVisibilityDictionary(DataSet ds)
        {
            if (ds == null || ds.Tables.Count == 0)
            {
                return null;
            }

            Dictionary<string, GridViewColumnsVisibility> columnsVisibilityDict = new Dictionary<string, GridViewColumnsVisibility>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string row_TemplateTipusNev = row["TemplateTipusNev"].ToString();

                // azok a template-ek az oszlopl�that�s�g-�ll�t�s template-ek, amik 'ColumnsVisibility_'-vel kezd�dnek
                if (row_TemplateTipusNev.StartsWith(ColumnsVisibilityTag))
                {
                    string row_Id = row["Id"].ToString();
                    string row_TemplateXML = row["TemplateXML"].ToString();

                    GridViewColumnsVisibility columnsVisibilityObject = new GridViewColumnsVisibility(row_Id, row_TemplateXML);

                    // felv�tel a Dictionary-be:
                    if (!columnsVisibilityDict.ContainsKey(row_TemplateTipusNev))
                    {
                        columnsVisibilityDict.Add(row_TemplateTipusNev, columnsVisibilityObject);
                    }
                }
                else
                {
                    continue;
                }
            }

            return columnsVisibilityDict;
        }

        private static void SetGridViewColumnsVisibility(GridView attachedGridView, GridViewColumnsVisibility gridViewColumnsVisibility)
        {
            if (attachedGridView == null || gridViewColumnsVisibility == null)
            {
                return;
            }

            foreach (KeyValuePair<int, GridViewColumnsVisibility.ColumnProperties> kvp in gridViewColumnsVisibility.Values)
            {
                int index = kvp.Key;
                GridViewColumnsVisibility.ColumnProperties columnProperties = kvp.Value;

                bool visible = columnProperties.visible;
                string headerText = columnProperties.headerText;
                string sortExpression = columnProperties.sortExpression;

                /// Ha az index-edik oszlopn�l megegyezik a headerText vagy a sortExpression, akkor megvan az oszlop --> visible be�ll�t�sa
                /// Ha nem egyezik meg, v�gigmegy�nk a GridView oszlopain, �s megkeress�k azt az oszlopot,
                /// ahol a headerText vagy a SortExpression megegyezik

                DataControlField gridViewColumn = null;

                if (index >= attachedGridView.Columns.Count)
                {
                    // nem j� az index, megkeress�k headerText, sortExpression alapj�n:
                    gridViewColumn = SearchGridViewColumn(attachedGridView, headerText, sortExpression);
                }
                else
                {
                    /// ha vagy a headerText, vagy a sortExpression megegyezik a megadott index-� oszlopn�l --> ok
                    /// ha nincs megadva sem headerText, sem sortExpression --> index-edik oszlop
                    // egy�bk�nt megpr�b�ljuk megkeresni
                    if ((attachedGridView.Columns[index].HeaderText == headerText && !string.IsNullOrEmpty(headerText))
                        || (attachedGridView.Columns[index].SortExpression == sortExpression && !string.IsNullOrEmpty(sortExpression))
                        || (string.IsNullOrEmpty(headerText) && string.IsNullOrEmpty(sortExpression)))
                    {
                        gridViewColumn = attachedGridView.Columns[index];
                    }                    
                    else
                    {
                        gridViewColumn = SearchGridViewColumn(attachedGridView, headerText, sortExpression);
                    }
                }

                if (gridViewColumn != null)
                {
                    gridViewColumn.Visible = visible;
                }
            }
        }

        /// <summary>
        /// Azt az oszlopot adja vissza, ahol megegyezik vagy a headerText, vagy a sortExpression
        /// </summary>
        private static DataControlField SearchGridViewColumn(GridView gridView, string headerText, string sortExpression)
        {
            // headerText nem lehet �res, sortExpression lehet
            if (gridView == null || String.IsNullOrEmpty(headerText)) { return null; }

            foreach (DataControlField column in gridView.Columns)
            {
                if (column.HeaderText == headerText
                    || (column.SortExpression == sortExpression && !string.IsNullOrEmpty(sortExpression)))
                {
                    return column;
                }
            }

            return null;
        }

        #endregion

        #region OrderBy

        public class GridViewOrderBy
        {
            public struct ColumnProperties
            {
                public string columnName;
                public string sortExpression;
                public string sortDirection;


                public ColumnProperties(string _columnName, string _sortExpression, string _sortDirection)
                {
                    this.columnName = _columnName;
                    this.sortExpression = _sortExpression;
                    this.sortDirection = _sortDirection;
                }
            }

            private string uiMezoObjektumErtekek_Id = String.Empty;
            public string UIMezoObjektumErtekek_Id
            {
                get { return uiMezoObjektumErtekek_Id; }
            }

            private Dictionary<int, ColumnProperties> values = new Dictionary<int, ColumnProperties>();
            public Dictionary<int, ColumnProperties> Values
            {
                get { return values; }
            }

            /// <summary>
            /// Konstruktor, meg kell adni a KRT_UIMezoObjektumErtekek rekord Id-j�t, �s az xml-t, amiben az adatok vannak t�rolva, megfelel� form�tumban
            /// </summary>
            /// <param name="templateId"></param>
            /// <param name="templateXml"></param>
            public GridViewOrderBy(string templateId, string templateXml)
            {
                uiMezoObjektumErtekek_Id = templateId;

                #region templateXml feldolgoz�sa

                /// Form�tum pl.:
                /// <OrderBy>
                ///     <Columns>
                ///         <Column index="1" name="�v" sortExpression="EREC_UgyUgyiratok.Ev" sortDirection="DESC" />
                ///         <Column index="2" name="Iktat�sz�m" sortExpression="EREC_UgyUgyiratok.Iktatohely, EREC_UgyUgyiratok.Azonosito" sortDirection="ASC" />
                ///     </Columns>
                /// </OrderBy>

                if (!string.IsNullOrEmpty(templateXml))
                {
                    try
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(templateXml);

                        if (xmlDoc != null)
                        {
                            XmlNodeList columnNodeList = xmlDoc.SelectNodes("./OrderBy/Columns/Column");
                            foreach (XmlNode columnNode in columnNodeList)
                            {
                                string indexAttr = columnNode.Attributes["index"].Value;
                                int index = Int32.Parse(indexAttr);
                                // string visibleAttr = columnNode.Attributes["visible"].Value;
                                string columnName = String.Empty;
                                XmlAttribute columnNameXmlAttr = columnNode.Attributes["name"];
                                if (columnNameXmlAttr != null)
                                {
                                    columnName = columnNameXmlAttr.Value;
                                }
                                string sortExpression = String.Empty;
                                XmlAttribute sortExpressionXmlAttr = columnNode.Attributes["sortExpression"];
                                if (sortExpressionXmlAttr != null)
                                {
                                    sortExpression = sortExpressionXmlAttr.Value;
                                }
                                string sortDirection = String.Empty;
                                XmlAttribute sortDirectionXmlAttr = columnNode.Attributes["sortDirection"];
                                if (sortExpressionXmlAttr != null)
                                {
                                    sortDirection = sortDirectionXmlAttr.Value;
                                }

                                if (!this.values.ContainsKey(index))
                                {
                                    this.values.Add(index, new ColumnProperties(columnName, sortExpression, sortDirection));
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // hiba:
                        Logger.Error("Hiba a templateXml feldolgoz�sa sor�n. TemplateXml: " + templateXml, e);
                    }
                }

                #endregion
            }
        }

        public const string OrderByTag = "OrderBy_";


        public static Dictionary<string, GridViewOrderBy> CreateOrderByDictionary(DataSet ds)
        {
            if (ds == null || ds.Tables.Count == 0)
            {
                return null;
            }

            Dictionary<string, GridViewOrderBy> columnsOrderByDict = new Dictionary<string, GridViewOrderBy>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string row_TemplateTipusNev = row["TemplateTipusNev"].ToString();

                if (row_TemplateTipusNev.StartsWith(OrderByTag))
                {
                    string row_Id = row["Id"].ToString();
                    string row_TemplateXML = row["TemplateXML"].ToString();

                    GridViewOrderBy columnsOrderByObject = new GridViewOrderBy(row_Id, row_TemplateXML);

                    // felv�tel a Dictionary-be:
                    if (!columnsOrderByDict.ContainsKey(row_TemplateTipusNev))
                    {
                        columnsOrderByDict.Add(row_TemplateTipusNev, columnsOrderByObject);
                    }
                }
                else
                {
                    continue;
                }
            }

            return columnsOrderByDict;
        }

        #endregion


    }
}
