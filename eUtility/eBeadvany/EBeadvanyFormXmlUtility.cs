﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility.EKF;
using Contentum.eUtility.Models;
using System;
using System.Collections.Generic;

namespace Contentum.eUtility.eBeadvany
{
    public class eBeadvanyFormXmlUtility
    {
        #region XPATH
        public readonly string XML_TAG_NAME_TYPE_ID = EBeadvanyFormXmlHelper.XML_TAG_NAME_TYPE_ID;

        public readonly string XML_TAG_NAME_IRAT_TARGYA = EBeadvanyFormXmlHelper.XML_TAG_NAME_IRAT_TARGYA;

        public readonly string XML_TAG_NAME_FORM_ID = EBeadvanyFormXmlHelper.XML_TAG_NAME_FORM_ID;
        public readonly string XML_TAG_NAME_FORM_TYPE_NAME = EBeadvanyFormXmlHelper.XML_TAG_NAME_FORM_TYPE_NAME;
        public readonly string XML_TAG_NAME_TEXT001_INPUT = EBeadvanyFormXmlHelper.XML_TAG_NAME_TEXT001_INPUT;
        public readonly string XML_TAG_NAME_FORM_USERID = EBeadvanyFormXmlHelper.XML_TAG_NAME_FORM_USERID;
        public readonly string XML_TAG_NAME_FORM_USERNAME = EBeadvanyFormXmlHelper.XML_TAG_NAME_FORM_USERNAME;
        public readonly string XML_TAG_NAME_FORM_COMPANY_ID = EBeadvanyFormXmlHelper.XML_TAG_NAME_FORM_COMPANY_ID;
        public readonly string XML_TAG_NAME_FORM_HIR_ID = EBeadvanyFormXmlHelper.XML_TAG_NAME_FORM_HIR_ID;
        public readonly string XML_TAG_NAME_FORM_COMPANY_NAME = EBeadvanyFormXmlHelper.XML_TAG_NAME_FORM_COMPANY_NAME;
        public readonly string XML_TAG_NAME_FORM_COMPANY_ADDRESS_CITY = EBeadvanyFormXmlHelper.XML_TAG_NAME_FORM_COMPANY_ADDRESS_CITY;
        public readonly string XML_TAG_NAME_FORM_COMPANY_ADDRESS_ZIP = EBeadvanyFormXmlHelper.XML_TAG_NAME_FORM_COMPANY_ADDRESS_ZIP;
        public readonly string XML_TAG_NAME_FORM_COMPANY_ADDRESS_ID = EBeadvanyFormXmlHelper.XML_TAG_NAME_FORM_COMPANY_ADDRESS_ID;

        public readonly string XML_TAG_NAME_CPF = EBeadvanyFormXmlHelper.XML_TAG_NAME_CPF;

        public readonly string XML_TAG_NAME_CPF_SZERVEZET = EBeadvanyFormXmlHelper.XML_TAG_NAME_CPF_SZERVEZET;
        public readonly string XML_TAG_NAME_CPF_KAPCSOLATTARTO = EBeadvanyFormXmlHelper.XML_TAG_NAME_CPF_KAPCSOLATTARTO;
        public readonly string XML_TAG_NAME_CPF_EMAILADDRESS = EBeadvanyFormXmlHelper.XML_TAG_NAME_CPF_EMAILADDRESS;

        public readonly string XML_TAG_NAME_CPF_DRCIMJELZES = EBeadvanyFormXmlHelper.XML_TAG_NAME_CPF_DRCIMJELZES;
        public readonly string XML_TAG_NAME_CPF_CSALADINEV = EBeadvanyFormXmlHelper.XML_TAG_NAME_CPF_CSALADINEV;
        public readonly string XML_TAG_NAME_CPF_UTONEV1 = EBeadvanyFormXmlHelper.XML_TAG_NAME_CPF_UTONEV1;
        public readonly string XML_TAG_NAME_CPF_UTONEV2 = EBeadvanyFormXmlHelper.XML_TAG_NAME_CPF_UTONEV2;
        public readonly string XML_TAG_NAME_CPF_BENYUJTO_LAKCIM_IRSZ = EBeadvanyFormXmlHelper.XML_TAG_NAME_CPF_BENYUJTO_LAKCIM_IRSZ;
        public readonly string XML_TAG_NAME_CPF_BENYUJTO_LAKCIM_TELEPULES = EBeadvanyFormXmlHelper.XML_TAG_NAME_CPF_BENYUJTO_LAKCIM_TELEPULES;
        public readonly string XML_TAG_NAME_CPF_BENYUJTO_LAKCIM = EBeadvanyFormXmlHelper.XML_TAG_NAME_CPF_BENYUJTO_LAKCIM;

        public readonly string XML_TAG_VALUE_TYPE_ID = EBeadvanyFormXmlHelper.XML_TAG_VALUE_TYPE_ID;

        public const string XML_TAG_NAME_ElozmenyIktato = "//*/*[name()='variables']/*[name()='elozmeny:iktato']";
        public const string XML_TAG_NAME_ElozmenyInt = "//*/*[name()='variables']/*[name()='elozmenyint']";
        public const string XML_TAG_NAME_ElozmenyNyomt = "//*/*[name()='variables']/*[name()='elozmenynyomt']";
        #endregion

        /// <summary>
        /// ExtractDataFromEBeadvanyFormXml
        /// </summary>
        /// <param name="id"></param>
        /// <param name="myExecParam"></param>
        /// <returns></returns>
        public eBeadvany_Form_XML_Model ExtractDataFromEBeadvanyFormXml(Guid id, ExecParam myExecParam)
        {
            List<byte[]> byteContents = EBeadvanyFormXmlHelper.GetEBeadvanyXmlDocuments(id, myExecParam);
            if (byteContents == null || byteContents.Count < 1)
            {
                return null;
            }

            eBeadvany_Form_XML_Model modelFormXml = new eBeadvany_Form_XML_Model();

            for (int i = 0; i < 1; i++)   //for (int i = 0; i < byteContents.Count; i++)
            {
                modelFormXml.TypeId = GetContentFromXml(byteContents[i], XML_TAG_NAME_TYPE_ID);
                if (!string.IsNullOrEmpty(modelFormXml.TypeId))
                {
                    modelFormXml.Type = 1;

                    modelFormXml.FormId = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_ID);
                    modelFormXml.FormTypeName = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_TYPE_NAME);
                    modelFormXml.Text001 = GetContentFromXml(byteContents[i], XML_TAG_NAME_TEXT001_INPUT);
                    modelFormXml.FormUserId = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_USERID);
                    modelFormXml.FormUserName = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_USERNAME);
                    modelFormXml.FormCompanyId = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_COMPANY_ID);
                    modelFormXml.FormHirId = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_HIR_ID);
                    modelFormXml.FormCompanyName = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_COMPANY_NAME);
                    modelFormXml.FormCompanyAddressCity = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_COMPANY_ADDRESS_CITY);
                    modelFormXml.FormCompanyAddressZip = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_COMPANY_ADDRESS_ZIP);
                    modelFormXml.FormCompanyAddressId = GetContentFromXml(byteContents[i], XML_TAG_NAME_FORM_COMPANY_ADDRESS_ID);
                }
                else
                {
                    modelFormXml.CpfSzervezet = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_SZERVEZET);
                    if (!string.IsNullOrEmpty(modelFormXml.CpfSzervezet))
                    {
                        modelFormXml.Type = 2;

                        modelFormXml.CpfKapcsolattarto = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_KAPCSOLATTARTO);
                        modelFormXml.FormCompanyAddressZip = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_EMAILADDRESS);
                    }
                    else
                    {
                        modelFormXml.Type = 3;

                        modelFormXml.CpfDrCimJelzes = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_DRCIMJELZES);
                        modelFormXml.CpfCsaladiNev = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_CSALADINEV);
                        modelFormXml.CpfUtonev1 = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_UTONEV1);
                        modelFormXml.CpfUtonev2 = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_UTONEV2);
                        modelFormXml.CpfBenyujtoLakcimIrsz = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_BENYUJTO_LAKCIM_IRSZ);
                        modelFormXml.CpfBenyujtoLakcimTelepules = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_BENYUJTO_LAKCIM_TELEPULES);
                        modelFormXml.CpfBenyujtoLakcim = GetContentFromXml(byteContents[i], XML_TAG_NAME_CPF_BENYUJTO_LAKCIM);
                    }
                }
            }
            return modelFormXml;
        }

        /// <summary>
        /// Extract PR_HivatkozasiSzam From eBeadvany FormXml
        /// </summary>
        /// <param name="eBeadvany"></param>
        /// <param name="myExecParam"></param>
        /// <returns></returns>
        public string GetPRHivatkozasiSzamFromFormXml(EREC_eBeadvanyok eBeadvany, ExecParam myExecParam)
        {
            try
            {
                List<byte[]> byteContents = null;
                byteContents = GetXmlDocuments(myExecParam, eBeadvany.Id, EKFConstants.CONST_XML_FILE_FORMXML , Query.Operators.equals);
                if (byteContents == null || byteContents.Count < 1)
                {
                    if (string.IsNullOrEmpty(eBeadvany.UzenetTipusa))
                    {
                        return null;
                    }

                    byteContents = GetXmlDocuments(myExecParam, eBeadvany.Id, eBeadvany.UzenetTipusa + EKFConstants.CONST_XML_FILE_EXTENSION, Query.Operators.equals);
                    if (byteContents == null || byteContents.Count < 1)
                    {
                        return null;
                    }
                }

                return string.Format("{0}{1}{2}",
                    GetContentFromXml(byteContents[0], XML_TAG_NAME_ElozmenyIktato) ?? string.Empty,
                    GetContentFromXml(byteContents[0], XML_TAG_NAME_ElozmenyInt) ?? string.Empty,
                    GetContentFromXml(byteContents[0], XML_TAG_NAME_ElozmenyNyomt) ?? string.Empty);
            }
            catch (Exception exc)
            {
                Logger.Error("GetPRHivatkozasiSzamFromFormXml error:", exc);
                return null;
            }
        }

        #region HELPER

        /// <summary>
        /// GetEBeadvany
        /// </summary>
        /// <param name="id"></param>
        /// <param name="myExecParam"></param>
        /// <returns></returns>
        private Result GetEBeadvany(Guid id, ExecParam myExecParam)
        {
            return EBeadvanyFormXmlHelper.GetEBeadvany(id, myExecParam);
        }

        /// <summary>
        /// GetXmlDocuments
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="eBeadvanyId"></param>
        /// <param name="searchFajlNev"></param>
        /// <param name="searchFajlNevOperator"></param>
        /// <returns></returns>
        private List<byte[]> GetXmlDocuments(ExecParam execParam, string eBeadvanyId, string searchFajlNev, string searchFajlNevOperator)
        {
            return EBeadvanyFormXmlHelper.GetXmlDocuments(execParam, eBeadvanyId, searchFajlNev, searchFajlNevOperator);
        }

        /// <summary>
        /// GetContentFromXml
        /// </summary>
        /// <param name="docContent"></param>
        /// <returns></returns>
        private string GetContentFromXml(byte[] docContent, string xPath)
        {
            return EBeadvanyFormXmlHelper.GetContentFromXml(docContent, xPath);
        }
        /// <summary>
        /// GetDocContent
        /// </summary>
        /// <param name="externalLink"></param>
        /// <returns></returns>
        private byte[] GetDocContent(string externalLink)
        {
            return EBeadvanyFormXmlHelper.GetDocContent(externalLink);
        }

        #endregion
    }
}
