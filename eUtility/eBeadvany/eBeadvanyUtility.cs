﻿using System;

namespace Contentum.eUtility.eBeadvany
{
    public static class eBeadvanyUtility
    {
        /// <summary>
        /// ErkezesModjaMeghatarozasa
        /// </summary>
        /// <param name="kuldoRendszer"></param>
        /// <param name="partnerKapcsolatiKod"></param>
        /// <returns>KULD_KEZB_MODJA kodertek</returns>
        public static string ErkezesModjaMeghatarozasa(string kuldoRendszer, string partnerKapcsolatiKod)
        {
            if (string.IsNullOrEmpty(kuldoRendszer) && string.IsNullOrEmpty(partnerKapcsolatiKod))
            {
                return KodTarak.KULD_KEZB_MODJA.elektronikus_uton;
            }

            if (Constants.eBeadvanyKuldoRendszer.Adatkapu.Equals(kuldoRendszer, StringComparison.CurrentCultureIgnoreCase))
            {
                return KodTarak.KULD_KEZB_MODJA.elektronikus_uton_Adatkapu;
            }
            else if (Constants.eBeadvanyKuldoRendszer.e_nmhh_Captcha.Equals(kuldoRendszer, StringComparison.CurrentCultureIgnoreCase))
            {
                return KodTarak.KULD_KEZB_MODJA.elektronikus_uton_e_nmhh_nem_azonositott_felhasznalo;
            }
            else if (Constants.eBeadvanyKuldoRendszer.e_nmhh_VIAZ.Equals(kuldoRendszer, StringComparison.CurrentCultureIgnoreCase))
            {
                return KodTarak.KULD_KEZB_MODJA.elektronikus_uton_e_nmhh_azonositott_felhasznalo;
            }
            else if (partnerKapcsolatiKod.Length == 8)
            {
                return KodTarak.KULD_KEZB_MODJA.elektronikus_uton_cegkapu;
            }
            else if (partnerKapcsolatiKod.Length == 9)
            {
                return KodTarak.KULD_KEZB_MODJA.elektronikus_uton_HKP;
            }
            else if (partnerKapcsolatiKod.Length == 32)
            {
                return KodTarak.KULD_KEZB_MODJA.elektronikus_uton_Ugyfelkapu;
            }
            else
            {
                return KodTarak.KULD_KEZB_MODJA.elektronikus_uton;
            }
        }
        /// <summary>
        /// FeladoTipusMeghatarozasa
        /// </summary>
        /// <param name="partnerKapcsolatiKod"></param>
        /// <returns></returns>
        public static string FeladoTipusMeghatarozasa(string partnerKapcsolatiKod)
        {
            if (string.IsNullOrEmpty(partnerKapcsolatiKod))
            {
                return null;
            }

            if (partnerKapcsolatiKod.Length == 8)
            {
                return KodTarak.EBEADVANY_FELADO_TIPUS.Elektronikus_Uton_Cegkapu;
            }
            else if (partnerKapcsolatiKod.Length == 9)
            {
                return KodTarak.EBEADVANY_FELADO_TIPUS.Elektronikus_Uton_HivataliKapu;
            }
            else if (partnerKapcsolatiKod.Length == 32)
            {
                return KodTarak.EBEADVANY_FELADO_TIPUS.Elektronikus_Uton_Ugyfelkapu;
            }
            else
            {
                return null;
            }
        }
    }
}
