﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;

namespace Contentum.eUtility.EFeladoJegyzek
{
    // TODO: mezők és validálási szabályok kiolvasása XSD-ből

    public static class Constants
    {
        public enum XmlType { Adatok, UgyfelAdatok };
        public enum XmlFormattingType { None, OnlyLineBreaks, Indented }

        public static XmlFormattingType DefaultXmlFormattingType = XmlFormattingType.Indented;
        public static string DefaultXmlSettingsIndentChars = "\t";

        //public const string tomeges_adatok = "tomeges_adatok";
        public const string kozonseges_tetelek = "kozonseges_tetelek";
        public const string nyilvantartott_tetelek = "nyilvantartott_tetelek";
        public const string kozonseges_tetel = "kozonseges_tetel";
        public const string nyilvantartott_tetel = "nyilvantartott_tetel";
        public const string ugyfel_adatok = "ugyfel_adatok";
        // BLG_1938
        public const string kozonseges_azon_tetelek = "kozonseges_azon_tetelek";
        public const string nemz_kozonseges_tetelek = "nemz_kozonseges_tetelek";
        public const string nemz_kozonseges_azon_tetelek = "nemz_kozonseges_azon_tetelek";
        public const string nemz_nyilvantartott_tetelek = "nemz_nyilvantartott_tetelek";
        public const string kozonseges_azon_tetel = "kozonseges_azon_tetel";
        public const string nemz_kozonseges_tetel = "nemz_kozonseges_tetel";
        public const string nemz_kozonseges_azon_tetel = "nemz_kozonseges_azon_tetel";
        public const string nemz_nyilvantartott_tetel = "nemz_nyilvantartott_tetel";
 

        public const string DefaultXmlDirPath = @"C:\temp\ContentumNet\EFeladoJegyzek";


        public static readonly System.Text.Encoding DefaultEncoding = null;
        static Constants()
        {
            DefaultEncoding = System.Text.Encoding.GetEncoding(1250);
        }

        #region Paths to XSD/XML files
        public const string DirectoryPathToXsdEFeladojegyzek = "App_Data";
        public const string FileNameXsdEFeladoJegyzek = "ADAT.xsd";
        public const string FileNameXsdEFeladoJegyzekUgyfelAdatok = "UGYFEL.xsd";

        public static string PathToAdatokXsd
        {
            get
            {
                return System.IO.Path.Combine(
                         System.IO.Path.Combine(
                             System.Web.HttpContext.Current.Server.MapPath(".")
                             , DirectoryPathToXsdEFeladojegyzek
                         )
                         , FileNameXsdEFeladoJegyzek
                      );
            }
        }

        public const string DirectoryPathToXsdEFeladoJegyzekUgyfelAdatok = "App_Data";
        public const string FileNameXmlEFeladoJegyzekUgyfelAdatok = "UGYFEL.xml";

        public const string FileNameXmlEFeladoJegyzek = "ADAT.xml";
        #endregion Paths to XSD/XML files
    }

    /// <summary>
    /// Summary description for EFeladoJegyzek.Validation
    /// </summary>
    public class Validation
    {
        public Validation()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private static Dictionary<string, string> dictErrorCategoryHeader = null;
        public static Dictionary<string, string> DictErrorCategoryHeader
        {
            get
            {
                if (dictErrorCategoryHeader == null)
                {
                    dictErrorCategoryHeader = new Dictionary<string, string>();
                    dictErrorCategoryHeader.Add("baseType","Adattípus nem megfelelő");
                    dictErrorCategoryHeader.Add("mandatory", "Kötelező érték hiányzik");
                    dictErrorCategoryHeader.Add("enumeration","Felsorolási érték nem megengedett");
                    dictErrorCategoryHeader.Add("fractionDigits","Tizedesjegyek száma eltér az előírttól");
                    dictErrorCategoryHeader.Add("length","Hossz eltér az előírttól");
                    dictErrorCategoryHeader.Add("maxExclusive", "Felső értékhatárnál (exkl.) nagyobb érték");
                    dictErrorCategoryHeader.Add("maxInclusive", "Felső értékhatár (inkl.) nagyobb érték");
                    dictErrorCategoryHeader.Add("maxLength", "Max. hosszat meghaladó adat");
                    dictErrorCategoryHeader.Add("minExclusive", "Alsó értékhatárnál (exkl.) kisebb érték");
                    dictErrorCategoryHeader.Add("minInclusive", "Alsó értékhatárnál (inkl.) kisebb érték");
                    dictErrorCategoryHeader.Add("minLength", "Min. hossznál rövidebb adat");
                    dictErrorCategoryHeader.Add("pattern","Formátum (reg.kif. szerint) nem megfelelő");
                    dictErrorCategoryHeader.Add("totalDigits","Számjegyek száma eltér az előírttól");
                    dictErrorCategoryHeader.Add("komplex", "Összetett szabály megsértése");
                }
                return dictErrorCategoryHeader;
            }
        }

        // TODO: SimpleTypeRestriction alapján, vagy XSD-ből
        private static Dictionary<string, List<string>> fieldNames = null;
        public static Dictionary<string, List<string>> FieldNames
        {
            get
            {
                if (fieldNames == null)
                {
                    fieldNames = GetFieldNamesDictionary();
                }
                return fieldNames;
            }
        }

        // TODO: SimpleTypeRestriction alapján, vagy XSD-ből
        private static Dictionary<string, List<string>> mandatoryFields = null;
        public static Dictionary<string, List<string>> MandatoryFields
        {
            get
            {
                if (mandatoryFields == null)
                {
                    mandatoryFields = GetMandatoryFieldsDictionary();
                }
                return mandatoryFields;
            }
        }

        private static Dictionary<string, Dictionary<string, SimpleTypeRestriction>> simpleTypeRestrictions = null;
        public static Dictionary<string, Dictionary<string, SimpleTypeRestriction>> SimpleTypeRestrictions
        {
            get
            {
                if (simpleTypeRestrictions == null)
                {
                    simpleTypeRestrictions = GetSimpleTypeRestrictionDictionary();
                }
                return simpleTypeRestrictions;
            }
        }

        private static Dictionary<string, List<ComplexValidationRule>> dataRowRules = null;
        public static Dictionary<string, List<ComplexValidationRule>> DataRowRules
        {
            get
            {
                if (dataRowRules == null)
                {
                    dataRowRules = new Dictionary<string, List<ComplexValidationRule>>();

                    dataRowRules[EFeladoJegyzek.Constants.kozonseges_tetel] = GetDataRowRules(EFeladoJegyzek.Constants.kozonseges_tetel);

                    dataRowRules[EFeladoJegyzek.Constants.nyilvantartott_tetel] = GetDataRowRules(EFeladoJegyzek.Constants.nyilvantartott_tetel);

                    // BLG_1938

                    dataRowRules[EFeladoJegyzek.Constants.nemz_kozonseges_tetel] = GetDataRowRules(EFeladoJegyzek.Constants.nemz_kozonseges_tetel);

                    dataRowRules[EFeladoJegyzek.Constants.kozonseges_azon_tetel] = GetDataRowRules(EFeladoJegyzek.Constants.kozonseges_azon_tetel);

                    dataRowRules[EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel] = GetDataRowRules(EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel);

                    dataRowRules[EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel] = GetDataRowRules(EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel);



                    //dataRowRules[ugyfel_adatok] = GetDataRowRules(ugyfel_adatok);
                }
                return dataRowRules;
            }
        }

        #region Utility
        // TODO: XSD-ből beolvasni
        private static Dictionary<string, List<string>> GetFieldNamesDictionary()
        {
            Dictionary<string, List<string>> fieldNames = new Dictionary<string, List<string>>();
            fieldNames[EFeladoJegyzek.Constants.kozonseges_tetel] = new List<string>();
            // BLG_1938
            //fieldNames[EFeladoJegyzek.Constants.kozonseges_tetel].AddRange(new string[] { "sorszam", "alapszolg", "viszonylat", "orszagkod", "suly", "darab", "kulonszolgok", "vakok_irasa", "meret", "dij", "gepre_alkalmassag" });
            fieldNames[EFeladoJegyzek.Constants.kozonseges_tetel].AddRange(new string[] { "sorszam", "alapszolg", "cimzett_irsz", "suly", "darab", "kulonszolgok", "vakok_irasa", "meret", "dij", "gepre_alkalmassag" });

            fieldNames[EFeladoJegyzek.Constants.nyilvantartott_tetel] = new List<string>();
            // BLG_1938
            //fieldNames[EFeladoJegyzek.Constants.nyilvantartott_tetel].AddRange(new string[] { "sorszam", "azonosito", "alapszolg", "orszagkod", "szall_mod", "cimzett_irsz", "suly", "kulonszolgok", "vakok_irasa", "uv_osszeg", "uv_lapid", "eny_osszeg", "kezbesites_ideje", "kezelesi_mod", "meret", "alapdij", "tobbletdij", "arufizdij", "cimzett_nev", "cimzett_hely", "cimzett_ertcsat", "cimzett_ertcim", "felado_ertcsat", "felado_ertcim", "potlapszam", "orz_ido", "dij", "cimzett_kozelebbi_cim", "megjegyzes", "gepre_alkalmassag" });
            fieldNames[EFeladoJegyzek.Constants.nyilvantartott_tetel].AddRange(new string[] { "sorszam", "azonosito", "alapszolg", "suly", "kulonszolgok", "vakok_irasa", "uv_osszeg", "uv_lapid", "eny_osszeg", "kezelesi_mod", "meret", "kezb_mod", "cimzett_nev", "cimzett_irsz", "cimzett_hely", "cimzett_kozelebbi_cim", "cimzett_kozterulet_nev", "cimzett_kozterulet_jelleg", "cimzett_hazszam", "cimzett_epulet", "cimzett_lepcsohaz", "cimzett_emelet", "cimzett_ajto", "cimzett_postafiok", "cimzett_cim_id", "cimzett_email", "cimzett_telefon", "dij", "sajat_azonosito", "gepre_alkalmassag" });

            fieldNames[EFeladoJegyzek.Constants.ugyfel_adatok] = new List<string>();
            fieldNames[EFeladoJegyzek.Constants.ugyfel_adatok].AddRange(new string[] { "azonosito", "nev", "iranyitoszam", "helyseg", "utca", "megallapodas", "kezdoallas", "zaroallas", "engedelyszam", "adatfajlnev" });
            // BLG_1938
            fieldNames[EFeladoJegyzek.Constants.nemz_kozonseges_tetel] = new List<string>();
            fieldNames[EFeladoJegyzek.Constants.nemz_kozonseges_tetel].AddRange(new string[] { "sorszam", "alapszolg", "viszonylat", "orszagkod", "suly", "darab", "kulonszolgok", "vakok_irasa", "meret", "dij", "potlapszam" });
            fieldNames[EFeladoJegyzek.Constants.kozonseges_azon_tetel] = new List<string>();
            fieldNames[EFeladoJegyzek.Constants.kozonseges_azon_tetel].AddRange(new string[] { "sorszam", "alapszolg", "azonosito", "cimzett_nev", "cimzett_irsz", "cimzett_hely", "cimzett_kozelebbi_cim", "cimzett_kozterulet_nev", "cimzett_kozterulet_jelleg", "cimzett_hazszam", "cimzett_epulet", "cimzett_lepcsohaz","cimzett_emelet","cimzett_ajto","cimzett_postafiok","cimzett_cim_id", "suly", "kulonszolgok", "vakok_irasa", "meret", "dij", "gepre_alkalmassag" });
            fieldNames[EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel] = new List<string>();
            fieldNames[EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel].AddRange(new string[] { "sorszam", "alapszolg", "azonosito", "orszagkod", "cimzett_nev", "cimzett_irsz", "cimzett_hely", "cimzett_kozelebbi_cim", "suly", "kulonszolgok", "vakok_irasa", "meret", "dij", "vam_ertek", "export_eng", "valutanem", "aru_tartalom", "aru_db", "aru_nev", "aru_ertek", "vam_tarifakod", "aru_orszag", "aru_suly", "engedely_szama", "dok_nev"  });  // vámadatokat nem kezelünk
            fieldNames[EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel] = new List<string>();
            fieldNames[EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel].AddRange(new string[] { "sorszam", "azonosito", "alapszolg", "orszagkod", "suly", "kulonszolgok", "vakok_irasa", "uv_osszeg", "uvdevizanem",  "eny_osszeg", "kezelesi_mod", "meret", "cimzett_nev", "cimzett_irsz", "cimzett_hely", "cimzett_kozelebbi_cim", "cimzett_email", "cimzett_telefon", "dij", "potlapszam", "sajat_azonosito", "vam_ertek", "export_eng", "valutanem", "aru_tartalom", "aru_db", "aru_nev", "aru_ertek", "vam_tarifakod", "aru_orszag", "aru_suly", "engedely_szama", "dok_nev" }); //vámadatokat nem kezelünk

            return fieldNames;
        }

        // TODO: XSD-ből beolvasni
        private static Dictionary<string, List<string>> GetMandatoryFieldsDictionary()
        {
            Dictionary<string, List<string>> mandatoryFields = new Dictionary<string, List<string>>();
            mandatoryFields[EFeladoJegyzek.Constants.kozonseges_tetel] = new List<string>();
            // BLG_1938
            //mandatoryFields[EFeladoJegyzek.Constants.kozonseges_tetel].AddRange(new string[] { "sorszam", "alapszolg", "suly", "darab", "dij" });
            mandatoryFields[EFeladoJegyzek.Constants.kozonseges_tetel].AddRange(new string[] { "sorszam", "alapszolg", "suly", "darab" });
  
            mandatoryFields[EFeladoJegyzek.Constants.nyilvantartott_tetel] = new List<string>();
            // BLG_1938
            //mandatoryFields[EFeladoJegyzek.Constants.nyilvantartott_tetel].AddRange(new string[] { "alapszolg", "suly", "dij", "sorszam", "azonosito", "cimzett_nev", "cimzett_hely" });
            mandatoryFields[EFeladoJegyzek.Constants.nyilvantartott_tetel].AddRange(new string[] { "sorszam", "azonosito", "alapszolg", "suly", "cimzett_nev", "cimzett_hely","cimzett_irsz" });

            mandatoryFields[EFeladoJegyzek.Constants.ugyfel_adatok] = new List<string>();
            mandatoryFields[EFeladoJegyzek.Constants.ugyfel_adatok].AddRange(new string[] { "azonosito", "nev", "iranyitoszam", "helyseg", "utca", "megallapodas" });
            // BLG_1938
            mandatoryFields[EFeladoJegyzek.Constants.kozonseges_azon_tetel] = new List<string>();
            mandatoryFields[EFeladoJegyzek.Constants.kozonseges_azon_tetel].AddRange(new string[] { "sorszam", "alapszolg","azonosito", "cimzett_nev", "cimzett_hely" });
            mandatoryFields[EFeladoJegyzek.Constants.nemz_kozonseges_tetel] = new List<string>();
            mandatoryFields[EFeladoJegyzek.Constants.nemz_kozonseges_tetel].AddRange(new string[] { "sorszam", "alapszolg", "suly", "darab" });
            mandatoryFields[EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel] = new List<string>();
            mandatoryFields[EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel].AddRange(new string[] { "sorszam", "alapszolg", "cimzett_nev", "cimzett_hely" });
            mandatoryFields[EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel] = new List<string>();
            mandatoryFields[EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel].AddRange(new string[] { "sorszam", "azonosito", "alapszolg", "suly", "cimzett_nev", "cimzett_hely" });

            mandatoryFields["vam_adatok"] = new List<string>();
            mandatoryFields["documents"] = new List<string>();

            return mandatoryFields;
        }


        // TODO: XSD-ből beolvasni
        private static Dictionary<string, Dictionary<string, SimpleTypeRestriction>> GetSimpleTypeRestrictionDictionary()
        {
            Dictionary<string, Dictionary<string, SimpleTypeRestriction>> simpleTypeRestrictions = new Dictionary<string, Dictionary<string, SimpleTypeRestriction>>();
            simpleTypeRestrictions[EFeladoJegyzek.Constants.kozonseges_tetel] = new Dictionary<string, SimpleTypeRestriction>();

            foreach (string key in FieldNames[EFeladoJegyzek.Constants.kozonseges_tetel])
            {
                simpleTypeRestrictions[EFeladoJegyzek.Constants.kozonseges_tetel].Add(key, GetSimpleTypeRestrictionForField(key, EFeladoJegyzek.Constants.kozonseges_tetel));
            }

            simpleTypeRestrictions[EFeladoJegyzek.Constants.nyilvantartott_tetel] = new Dictionary<string, SimpleTypeRestriction>();

            foreach (string key in FieldNames[EFeladoJegyzek.Constants.nyilvantartott_tetel])
            {
                simpleTypeRestrictions[EFeladoJegyzek.Constants.nyilvantartott_tetel].Add(key, GetSimpleTypeRestrictionForField(key, EFeladoJegyzek.Constants.nyilvantartott_tetel));
            }

            simpleTypeRestrictions[EFeladoJegyzek.Constants.ugyfel_adatok] = new Dictionary<string, SimpleTypeRestriction>();

            foreach (string key in FieldNames[EFeladoJegyzek.Constants.ugyfel_adatok])
            {
                simpleTypeRestrictions[EFeladoJegyzek.Constants.ugyfel_adatok].Add(key, GetSimpleTypeRestrictionForField(key, EFeladoJegyzek.Constants.ugyfel_adatok));
            }

            // BLG_1938
            simpleTypeRestrictions[EFeladoJegyzek.Constants.kozonseges_azon_tetel] = new Dictionary<string, SimpleTypeRestriction>();

            foreach (string key in FieldNames[EFeladoJegyzek.Constants.kozonseges_azon_tetel])
            {
                simpleTypeRestrictions[EFeladoJegyzek.Constants.kozonseges_azon_tetel].Add(key, GetSimpleTypeRestrictionForField(key, EFeladoJegyzek.Constants.kozonseges_azon_tetel));
            }

            simpleTypeRestrictions[EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel] = new Dictionary<string, SimpleTypeRestriction>();

            foreach (string key in FieldNames[EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel])
            {
                simpleTypeRestrictions[EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel].Add(key, GetSimpleTypeRestrictionForField(key, EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel));
            }

            simpleTypeRestrictions[EFeladoJegyzek.Constants.nemz_kozonseges_tetel] = new Dictionary<string, SimpleTypeRestriction>();

            foreach (string key in FieldNames[EFeladoJegyzek.Constants.nemz_kozonseges_tetel])
            {
                simpleTypeRestrictions[EFeladoJegyzek.Constants.nemz_kozonseges_tetel].Add(key, GetSimpleTypeRestrictionForField(key, EFeladoJegyzek.Constants.nemz_kozonseges_tetel));
            }

            simpleTypeRestrictions[EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel] = new Dictionary<string, SimpleTypeRestriction>();

            foreach (string key in FieldNames[EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel])
            {
                simpleTypeRestrictions[EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel].Add(key, GetSimpleTypeRestrictionForField(key, EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel));
            }

            return simpleTypeRestrictions;
        }

        private static SimpleTypeRestriction GetSimpleTypeRestrictionForField(string field, string parenttable)
        {
            SimpleTypeRestriction sr = new SimpleTypeRestriction();
            // BLG_1938
            //if (parenttable == EFeladoJegyzek.Constants.nyilvantartott_tetel || parenttable == EFeladoJegyzek.Constants.kozonseges_tetel)
            // {
            switch (parenttable)
            {
                case EFeladoJegyzek.Constants.nyilvantartott_tetel:
                case EFeladoJegyzek.Constants.kozonseges_azon_tetel:
                case EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel:
                case EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel:
                case EFeladoJegyzek.Constants.kozonseges_tetel:
                case EFeladoJegyzek.Constants.nemz_kozonseges_tetel:
                    {
                        #region kozonseges_adatok és nyilvantartott_adatok
                        switch (field)
                        {
                            case "sorszam":
                                sr.Mandatory = true;
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "integer";
                                sr.MinInclusive = 1;
                                switch (parenttable)
                                {
                                    case EFeladoJegyzek.Constants.kozonseges_tetel:
                                    // BLG_1938
                                    case EFeladoJegyzek.Constants.nemz_kozonseges_tetel:
                                        //sr.MaxInclusive = 999;
                                        sr.MaxInclusive = 99999;

                                        break;
                                    case EFeladoJegyzek.Constants.nyilvantartott_tetel:
                                    // BLG_1938
                                    case EFeladoJegyzek.Constants.kozonseges_azon_tetel:
                                    case EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel:
                                    case EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel:
                                        sr.MaxInclusive = 9999999999;
                                        break;
                                }
                                break;
                            case "alapszolg":
                                sr.Mandatory = true;
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MinLength = 7;
                                sr.MaxLength = 9;

                                switch (parenttable)
                                {
                                    case EFeladoJegyzek.Constants.kozonseges_tetel:
                                    case EFeladoJegyzek.Constants.nyilvantartott_tetel:
                                    case EFeladoJegyzek.Constants.kozonseges_azon_tetel:
                                        sr.Enumeration = new List<string>(new string[] {
                                                "A_111_LEV", "A_15_HIV", "A_111_LEV", "A_161_LEV", "A_114_KCR", "A_117_ULV", "A_114_UDM", "A_117_CKL",
                                                "A_111_LER", "A_114_KRK", "A_117_ULR", "A_114_UDR", "A_119_PRI" });

                                        break;
                                    case EFeladoJegyzek.Constants.nemz_kozonseges_tetel:
                                    case EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel:
                                    case EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel:
                                        sr.Enumeration = new List<string>(new string[] {
                                                 "A_111_LEV", "A_111_LLP", "A_112_NYT", "A_111_LEV", "A_118_NDL"});
                                        break;
                                }

                                break;
                            case "viszonylat":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MinLength = 1;
                                sr.MaxLength = 2;
                                sr.Enumeration = new List<string>(new string[] { "K1", "K2" });
                                break;
                            case "orszagkod":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MinLength = 2;
                                sr.MaxLength = 2;
                                sr.Pattern = @"^[A-Z]{2}$";
                                break;
                            case "suly":
                                sr.Mandatory = true;
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MinLength = 1;
                                sr.MaxLength = 5;
                                sr.Pattern = @"^[0-9]{1,5}$";
                                break;
                            case "darab":
                                sr.SimpleType = "integer";
                                sr.Mandatory = true;
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.MinInclusive = 1;
                                sr.MaxInclusive = 99999;
                                break;
                            case "kulonszolgok":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.Pattern = @"^(K_AJN|K_TEV|K_ENY|K_UVT|K_FEL|K_ETV|K_AMF|K_PRI|K_LEZ|K_SKZ|K_REG|K_VNY|K_POT|K_VER|K_PSZ|K_BAJ)?(,(K_AJN|K_TEV|K_ENY|K_UVT|K_FEL|K_ETV|K_AMF|K_PRI|K_LEZ|K_SKZ|K_REG|K_VNY|K_POT|K_VER|K_PSZ|K_BAJ))*$";
                                break;
                            case "vakok_irasa":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.Length = 1;
                                sr.Enumeration = new List<string>(new string[] { "0", "1" });
                                break;
                            case "meret":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MinLength = 1;
                                sr.MaxLength = 2;
                                sr.Enumeration = new List<string>(new string[] { "0", "1", "L" });
                                break;
                            case "dij":
                                sr.SimpleType = "integer";
                                sr.Mandatory = true;
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.MinInclusive = 1;
                                sr.MaxInclusive = 999999999;
                                break;
                            case "gepre_alkalmassag":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.Length = 1;
                                sr.Enumeration = new List<string>(new string[] { "G", "S", "" });
                                break;
                            case "azonosito":
                                /*       st_belf_kuldemeny_azonosito XSD-ben ezeket kell nézni
	                                        ct_kozonseges_azon_tetel
	                                        ct_kozonseges_azon_tetelBase
	                                        ct_nyilvantartott_tetelBase
	                                        ct_nyilvantartott_tetel
	                                        [0-9a-zA-Z]{16}
                                        st_nemz_egyedi_kuldemeny_azonosito
	                                        ct_nemz_nyilvantartott_tetelBase	
	                                        [0-9a-zA-Z]{13}
                                        st_nemz_kuldemeny_azonosito
	                                        ct_nemz_kozonseges_azon_tetelBase
	                                        ct_nemz_kozonseges_azon_tetel
	                                        [0-9a-zA-Z]{13}*/
                                switch (parenttable)
                                {
                                    case EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel:
                                        sr.Mandatory = true;
                                        sr.MinLength = 13;
                                        sr.MaxLength = 13;
                                        sr.Pattern = @"[0-9a-zA-Z]{13}";
                                        break;
                                    case EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel:
                                        sr.Mandatory = true;
                                        sr.MinLength = 13;
                                        sr.MaxLength = 13;
                                        sr.Pattern = @"[0-9a-zA-Z]{13}";
                                        break;
                                    case EFeladoJegyzek.Constants.kozonseges_azon_tetel:                                   
                                    case EFeladoJegyzek.Constants.nyilvantartott_tetel:
                                         sr.Mandatory = true;
                                        sr.MinLength = 16;
                                        sr.MaxLength = 16;
                                        sr.Pattern = @"[0-9a-zA-Z]{16}";                                        
                                        break;                                
                                    default:
                                        sr.Mandatory = false;
                                        sr.MinLength = 13;
                                        sr.MaxLength = 16;
                                        sr.Pattern = @"[0-9a-zA-Z]{16}";
                                        break;
                                }
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";                               
                                break;
                            // BLG_1938
                            //case "szall_mod":
                            //    sr.Mandatory = false;
                            //    sr.MinOccurs = 0;
                            //    sr.MaxOccurs = 1;
                            //    sr.SimpleType = "string";
                            //    break;
                            case "cimzett_irsz":
                                sr.Mandatory = false;
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                // BLG_1938
                                //sr.MaxLength = 4;
                                //sr.Pattern = @"^([1-9][0-9]{3})?$";
                                switch (parenttable)
                                {
                                    case EFeladoJegyzek.Constants.kozonseges_tetel:
                                    case EFeladoJegyzek.Constants.nyilvantartott_tetel:                                       
                                    case EFeladoJegyzek.Constants.kozonseges_azon_tetel:
                                        sr.MaxLength = 4;
                                        sr.Pattern = @"^([1-9][0-9]{3})?$";
                                        break;
                                    case EFeladoJegyzek.Constants.nemz_kozonseges_tetel:
                                    case EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel:
                                    case EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel:
                                        sr.MaxLength = 10;
                                        break;
                                }
                                switch(parenttable)
                                {
                                    case EFeladoJegyzek.Constants.kozonseges_azon_tetel:
                                    case EFeladoJegyzek.Constants.nyilvantartott_tetel:
                                        sr.Mandatory = true;
                                        break;
                                }
                                break;
                            case "uv_osszeg":
                                sr.SimpleType = "integer";
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.MinInclusive = 1;
                                sr.MaxInclusive = 999999999;
                                break;
                            case "uv_lapid":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.Length = 13;
                                sr.Pattern = @"^[0-9]{13}$";
                                break;
                            case "eny_osszeg":
                                sr.SimpleType = "integer";
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.MinInclusive = 1;
                                sr.MaxInclusive = 999999999;
                                break;
                            // BLG_1938
                            //case "kezbesites_ideje":
                            //    sr.SimpleType = "string";
                            //    sr.Mandatory = false;
                            //    sr.MinOccurs = 0;
                            //    sr.MaxOccurs = 1;
                            //    break;
                            case "kezelesi_mod":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.Length = 1;
                                sr.Enumeration = new List<string>(new string[] { "2", "3" });
                                break;
                            case "alapdij":
                            case "tobbletdij":
                            case "arufizdij":
                                sr.SimpleType = "integer";
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.MinInclusive = 1;
                                sr.MaxInclusive = 999999999;
                                break;
                            case "cimzett_nev":
                                sr.Mandatory = true;
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MinLength = 1;
                                sr.MaxLength = 150;
                                break;
                            case "cimzett_hely":
                                sr.Mandatory = true;
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MinLength = 2;
                                sr.MaxLength = 35;
                                break;
                            /*case "cimzett_ertcsat":
                            case "felado_ertcsat":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.Length = 1;
                                sr.Enumeration = new List<string>(new string[] { "E", "T" });
                                break;
                            case "cimzett_ertcim":
                            case "felado_ertcim":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MinLength = 6;
                                sr.MaxLength = 60;
                                sr.Pattern = @"^(([a-z0-9]+)([._-]([a-z0-9]+))*[@]([a-z0-9]+)([._-]([a-z0-9]+))*[.]([a-z0-9]){2}([a-z0-9])?)$|^(\+36[1-9]((-[0-9]{7})|([0-9]-[0-9]{6,7})))$";
                                break;*/
                            case "potlapszam":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MinLength = 1;
                                sr.MaxLength = 2;
                                sr.Pattern = @"^[0-9]{1,2}$";
                                break;
                            case "orz_ido":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                break;
                            case "cimzett_kozelebbi_cim":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MaxLength = 60;
                                break;
                            // BLG_1938
                            case "cimzett_kozterulet_nev":
                            case "cimzett_kozterulet_jelleg":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MaxLength = 80;
                                break;
                            case "cimzett_hazszam":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MaxLength = 30;
                                break;
                            case "cimzett_epulet":
                            case "cimzett_lepcsohaz":
                            case "cimzett_emelet":
                            case "cimzett_ajto":
                            case "cimzett_postafiok":
                            case "cimzett_cim_id":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MaxLength = 10;
                                break;
                            // case "megjegyzes":
                            case "sajat_azonosito":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MaxLength = 50;
                                break;
                        }
                        #endregion kozonseges_adatok és nyilvantartott_adatok
                        break;
                    }
                //}
                //else if (parenttable == EFeladoJegyzek.Constants.ugyfel_adatok)
                //{
                case EFeladoJegyzek.Constants.ugyfel_adatok:
                    {
                        #region ugyfel_adatok
                        switch (field)
                        {
                            case "azonosito":
                                sr.Mandatory = true;
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.Pattern = @"[0-9]+";
                                break;
                            case "nev":
                                sr.Mandatory = true;
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MinLength = 2;
                                sr.MaxLength = 35;
                                break;
                            case "iranyitoszam":
                                sr.Mandatory = true;
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "integer";
                                sr.MinInclusive = 1000;
                                sr.MaxInclusive = 9999;
                                break;
                            case "helyseg":
                            case "utca":
                                sr.Mandatory = true;
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MinLength = 2;
                                sr.MaxLength = 35;
                                break;
                            case "megallapodas":
                                sr.Mandatory = true;
                                sr.MinOccurs = 1;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.Length = 8;
                                break;
                            case "kezdoallas":
                            case "zaroallas":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "integer";
                                sr.MinInclusive = 1;
                                sr.MaxInclusive = 9999999999;
                                break;
                            case "adatfajlnev":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MaxLength = 20;
                                sr.Pattern = @"[a-zA-Z0-9.\-_]+";
                                break;
                            case "engedelyszam":
                                sr.Mandatory = false;
                                sr.MinOccurs = 0;
                                sr.MaxOccurs = 1;
                                sr.SimpleType = "string";
                                sr.MaxLength = 100;
                                sr.Pattern = @"[0-9]+(,[0-9]+)*";
                                break;
                        }
                        #endregion ugyfel_adatok
                        break;
                    }
             
            }

            return sr;
        }

        /// <summary>
        /// Összetett szabályok a mezők egymás közötti viszonyának leírására ill. komplex ellenőrzésekre (pl.CDV)
        /// TODO: bővítés az elektronikus feladójegyzék specifikációnak megfelelően
        /// </summary>
        /// <param name="parenttable"></param>
        /// <returns></returns>
        private static List<ComplexValidationRule> GetDataRowRules(string parenttable)
        {
            List<ComplexValidationRule> rules = new List<ComplexValidationRule>();
            ComplexValidationRule cvr = null;

            // BLG_1938
            //if (parenttable == EFeladoJegyzek.Constants.kozonseges_tetel || parenttable == EFeladoJegyzek.Constants.nyilvantartott_tetel)
            {
                //BLG_1938
                //Nemzetközi küldemények külön kerültek
                #region rule 1
                //cvr = new ComplexValidationRule(delegate (DataRow row, out List<string> errors)
                //{
                //    bool isValid = true;
                //    errors = new List<string>();

                //    if (row.Table.Columns.Contains("alapszolg") && row.Table.Columns.Contains("viszonylat") && row.Table.Columns.Contains("orszagkod"))
                //    {
                //        if (row["alapszolg"].ToString() == "A_115_NCR")
                //        {
                //            if (String.IsNullOrEmpty(row["orszagkod"].ToString()))
                //            {
                //                isValid = false;
                //                errors.Add("Nemzetközi reklámküldeménynél az országkód megadása kötelező!");
                //            }

                //            if (!String.IsNullOrEmpty(row["viszonylat"].ToString()))
                //            {
                //                isValid = false;
                //                errors.Add("Nemzetközi reklámküldeménynél a viszonylat nem szerepelhet, az országkód megadása kötelező!");
                //            }
                //        }
                //    }

                //    return isValid;
                //}
                //); // rule 1

                //rules.Add(cvr);
                #endregion rule 1

                #region rule 2
                cvr = new ComplexValidationRule(delegate(DataRow row, out List<string> errors)
                {
                    bool isValid = true;
                    errors = new List<string>();

                    if (row.Table.Columns.Contains("alapszolg") && row.Table.Columns.Contains("meret"))
                    {
                        string alapszolg = row["alapszolg"].ToString();
                        if (!String.IsNullOrEmpty(row["meret"].ToString()))
                        {
                            bool isFieldAllowed = false;
                            // BLG_1938
                            //string[] alapszolgokFieldAllowed = new string[] { "A_111_LEV", "A_112_NYT", "A_114_CRK", "A_115_NCR", "A_116_FLX" };
                            string[] alapszolgokFieldAllowed = new string[] { "A_111_LEV", "A_112_NYT"};

                            foreach (string item in alapszolgokFieldAllowed)
                            {
                                if (alapszolg == item)
                                {
                                    isFieldAllowed = true;
                                    break;
                                }
                            }

                            if (!isFieldAllowed)
                            {
                                // KDM, Levél (alapszolg: A_111_LEV), nyomtatvány (alapszolg: A_112_NYT),
                                // belföldi címzett reklámküldemény – DM (alapszolg: A_114_CRK),
                                // nemzetközi címzett reklámküldemény – IDM (alapszolg: A_115_NCR),
                                // valamint Flexi Üzleti levél(alapszolg: A_116_FLX)
                                // küldeményfajtáknál szerepelhet az adatállományban.
                                errors.Add(String.Format("A méret mező csak az {0} alapszolgáltatások mellett megengedett. Az aktuális alapszolgáltatás: {1}", String.Join(", ", alapszolgokFieldAllowed), alapszolg));
                            }
                            else
                            {
                                // ...

                            }
                        }
                    }

                    return isValid;
                }
                ); // rule 2
                rules.Add(cvr);
                #endregion rule 2

                #region rule 3
                cvr = new ComplexValidationRule(delegate(DataRow row, out List<string> errors)
                {
                    bool isValid = true;
                    errors = new List<string>();

                    if (row.Table.Columns.Contains("alapszolg") && row.Table.Columns.Contains("kulonszolgok") && row.Table.Columns.Contains("gepre_alkalmassag"))
                    {
                        string alapszolg = row["alapszolg"].ToString();
                        List<string> kulonszolgok = new List<string>((row["kulonszolgok"].ToString() ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
                        string feldolg = row["gepre_alkalmassag"].ToString();
                        if (kulonszolgok.Contains("K_FEL") && String.IsNullOrEmpty(feldolg))
                        {
                            // Ha a különszolgáltatások között szerepel a K_FEL,
                            // de a „feldolg” mező nincs megadva, akkor a rendszer a küldeményt hibásnak jelöli. 
                            errors.Add("A K_FEL különszolgáltatás mellett a feldolgozottság mértékének megadása kötelező!");
                        }
                    }

                    return isValid;
                }
                ); // rule 3
                rules.Add(cvr);
                #endregion rule 3

                // BLG_1938
                //if (parenttable == EFeladoJegyzek.Constants.nyilvantartott_tetel)
                if ((parenttable == EFeladoJegyzek.Constants.nyilvantartott_tetel) | (parenttable == EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel))

                {
                    #region rule 4
                    cvr = new ComplexValidationRule(delegate(DataRow row, out List<string> errors)
                    {
                        bool isValid = true;
                        errors = new List<string>();

                        if (row.Table.Columns.Contains("alapszolg") && row.Table.Columns.Contains("kulonszolgok"))
                        {
                            // Az értesítési csatorna lehetséges kódjai (4.2.8 melléklet):
                            // „E” – Elektronikus levelező rendszer (elektronikus levél küldése e-mail címre az interneten keresztül),
                            // „T” – Telefonszám (SMS), rövid szöveges üzenetküldő szolgáltatás (SMS üzenet küldése telefonszámra).

                            // hossz: e-mailcím: 6 … 60 kar. Tel.szám: 9 v.10 karakt.
                            // e-előrejelzés (K_EFC) különleges szolgáltatás esetén kötelezők: Cimzett_ertcsat, Cimzett_ertcim
                            // e-előrejelzés (K_EFF) különleges szolgáltatás esetén kötelezők: Felado_ertcsat, Felado_ertcim


                            string alapszolg = row["alapszolg"].ToString();
                            List<string> kulonszolgok = new List<string>((row["kulonszolgok"].ToString() ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));

                            //if (kulonszolgok.Contains("K_EFC"))
                            //{
                            //    string ertcsat = row["cimzett_ertcsat"].ToString();
                            //    string ertcim = row["cimzett_ertcim"].ToString();

                            //    if (String.IsNullOrEmpty(ertcsat))
                            //    {
                            //        errors.Add("A K_EFC különszolgáltatás mellett a címzett értesítési csatorna megadása kötelező!");
                            //    }

                            //    if (String.IsNullOrEmpty(ertcim))
                            //    {
                            //        errors.Add("A K_EFC különszolgáltatás mellett a címzett értesítési cím megadása kötelező!");
                            //    }
                            //    else if (ertcsat == "E")
                            //    {
                            //        if (ertcim.Length < 6 || ertcim.Length > 60)
                            //        {
                            //            errors.Add("A címzett értesítési email-cím hossza csak 6 és 60 karakter között lehet!");
                            //        }
                            //    }
                            //    else if (ertcsat == "T")
                            //    {
                            //        if (ertcim.Length < 9 || ertcim.Length > 10)
                            //        {
                            //            errors.Add("A címzett értesítési telefonszám hossza csak 9 vagy 10 karakter lehet!");
                            //        }
                            //    }
                            //}

                            //if (kulonszolgok.Contains("K_EFF"))
                            //{
                            //    string ertcsat = row["felado_ertcsat"].ToString();
                            //    string ertcim = row["felado_ertcim"].ToString();

                            //    if (String.IsNullOrEmpty(ertcsat))
                            //    {
                            //        errors.Add("A K_EFF különszolgáltatás mellett a feladó értesítési csatorna megadása kötelező!");
                            //    }

                            //    if (String.IsNullOrEmpty(ertcim))
                            //    {
                            //        errors.Add("A K_EFF különszolgáltatás mellett a feladó értesítési cím megadása kötelező!");
                            //    }
                            //    else if (ertcsat == "E")
                            //    {
                            //        if (ertcim.Length < 6 || ertcim.Length > 60)
                            //        {
                            //            errors.Add("A feladó értesítési email-cím hossza csak 6 és 60 karakter között lehet!");
                            //        }
                            //    }
                            //    else if (ertcsat == "T")
                            //    {
                            //        if (ertcim.Length < 9 || ertcim.Length > 10)
                            //        {
                            //            errors.Add("A feladó értesítési telefonszám hossza csak 9 vagy 10 karakter lehet!");
                            //        }
                            //    }
                            //}
                        }

                        return isValid;
                    }
                    ); // rule 4
                    rules.Add(cvr);
                    #endregion rule 4

                    #region rule 5
                    cvr = new ComplexValidationRule(delegate(DataRow row, out List<string> errors)
                    {
                        bool isValid = true;
                        errors = new List<string>();

                        if (row.Table.Columns.Contains("azonosito"))
                        {
                            // CDV ellenőrzések                           
                            string azonosito = row["azonosito"].ToString();

                            if (!String.IsNullOrEmpty(azonosito))
                            {
                                string orszagkod = null;
                                string viszonylat = null;
                                if (row.Table.Columns.Contains("orszagkod"))
                                {
                                    orszagkod = row["orszagkod"].ToString();
                                }
                                if (row.Table.Columns.Contains("viszonylat"))
                                {
                                    viszonylat = row["viszonylat"].ToString();
                                }

                                // Belföld
                                // BLG_1938
                                if (parenttable == EFeladoJegyzek.Constants.nyilvantartott_tetel)
                                //  if (String.IsNullOrEmpty(viszonylat) && (String.IsNullOrEmpty(orszagkod) || orszagkod == "HU"))
                                {
                                    #region Specification
                                    // Belföldi azonosító
                                    //Szerkezet:
                                    //1-2.	Küldeményfajta azonosító	Karakter (2)
                                    //3-6.	Felvevő posta irányítószáma	Szám (4)
                                    //7-15.	Egyedi sorszám	            Szám (9)
                                    //16.	CDV ellenőrzőszám	        Szám (1)

                                    // A CDV ellenőrzőszámot kizárólag az azonosító „egyedi sorszám” részének (7-15. pozíció) 9 numerikus karakterére kell képezni az alábbi algoritmus szerint:
                                    // -	a páratlan helyiértékű számokat balról jobbra haladva 3-mal kell megszorozni (1. lépés),
                                    // -	a páros helyiértékű számokat balról jobbra haladva 1-gyel kell megszorozni (1. lépés), 
                                    // -	a szorzatokat össze kell adni (2. lépés),
                                    // -	az eredménnyel el kell végezni a MOD10-es műveletet (3. lépés).

                                    //Ajánlott: RL
                                    //Értéknyilvánított küldemény: EL
                                    //Ajánlott válaszküldemény: KR
                                    //Értéknyilvánított válaszküldemény: RB

                                    // Alapszolgáltatás:
                                    // Válaszlevél	A_161_LEV
                                    // Válasz levelezőlap	A_161_LLP
                                    // Különszolgáltatás
                                    // Ajánlott	K_AJN
                                    // Értéknyilvánítás 	K_ENY
                                    // Értéknyilvánított válaszküldemény K_BAJ
                                    #endregion Specification

                                    List<string> allowedCodes = new List<string>(new string[] { "RL", "EL", "KR", "RB" });
                                    // a formátumokat külön lépésben, SimpleTypeRestriction segítségével validáljuk,
                                    // CDV-t ezért csak a megfelelő formátumnál számolunk
                                    if (azonosito.Length == 16)
                                    {
                                        string code = azonosito.Substring(0, 2);
                                        if (allowedCodes.Contains(code))
                                        {
                                            if (Array.TrueForAll(azonosito.Substring(2).ToCharArray(), delegate(char c) { return Char.IsDigit(c); }))
                                            {
                                                int check = Int32.Parse(azonosito.Substring(15, 1));
                                                int sum = 0;
                                                for (int i = 6; i < 15; i++)
                                                {
                                                    sum += Int32.Parse(azonosito.Substring(i, 1)) * (i % 2 == 0 ? 3 : 1);
                                                }

                                                int calculatedcheck = sum % 10;
                                                if (calculatedcheck != check)
                                                {
                                                    isValid = false;
                                                    errors.Add(String.Format("A belföldi küldeményazonosító ellenőrző összege hibás! Ellenőrző jegy: {0} - Számított: {1}", check, calculatedcheck));
                                                }
                                            }
                                        }
                                    }
                                }
                                // Külföld
                                else
                                {
                                    #region Specification
                                    // Külföldi azonosító
                                    // Szerkezet: ajánlott, értéklevél
                                    // 1-2.	    Küldeményfajta azonosító (RR)	    Karakteres (2)
                                    // 3-10.	Egyedi sorszám	                    Szám (8)
                                    // 11.	    CDV ellenőrzőszám	                Szám (1)
                                    // 12-13.	Származási ország ISO-2 kódja  (HU)	Karakteres (2)

                                    // Az egyedi sorszám (3-10.) CDV ellenőrzőszám képzésének algoritmusa (8 karak-terre):
                                    // -	az ellenőrizendő számot rendre meg kell szorozni a 8, 6, 4, 2, 3, 5, 9, 7 súlyté-nyezőkkel,
                                    // -	a szorzatokat össze kell adni,
                                    // -	az eredménnyel el kell végezni a MOD 11-es műveletet,
                                    // -	a maradékot ki kell vonni 11-ből, az így kapott szám lesz az ellenőrzőszám, ki-véve
                                    // -	ha a maradék = 0, akkor a CDV = 5
                                    // -	ha a maradék = 1, akkor a CDV = 0

                                    // Külföldi ajánlott levél: RR
                                    // Értéklevél: VV
                                    #endregion Specification

                                    List<string> allowedCodes = new List<string>(new string[] { "RR", "VV" });
                                    // a formátumokat külön lépésben, SimpleTypeRestriction segítségével validáljuk,
                                    // CDV-t ezért csak a megfelelő formátumnál számolunk
                                    if (azonosito.Length == 13)
                                    {
                                        if (allowedCodes.Contains(azonosito.Substring(0, 2)))
                                        {
                                            if (Array.TrueForAll(azonosito.Substring(2, 9).ToCharArray(), delegate(char c) { return Char.IsDigit(c); }))
                                            {
                                                int check = Int32.Parse(azonosito.Substring(10, 1));
                                                int[] vector = new int[] { 8, 6, 4, 2, 3, 5, 9, 7 };
                                                int sum = 0;
                                                for (int i = 2; i < 10; i++)
                                                {
                                                    sum += Int32.Parse(azonosito.Substring(i, 1)) * vector[i - 2];
                                                }

                                                int modresult = sum % 11;
                                                int calculatedcheck = -1;
                                                switch (modresult)
                                                {
                                                    case 0:
                                                        calculatedcheck = 5;
                                                        break;
                                                    case 1:
                                                        calculatedcheck = 0;
                                                        break;
                                                    default:
                                                        calculatedcheck = 11 - modresult;
                                                        break;
                                                }

                                                if (calculatedcheck != check)
                                                {
                                                    isValid = false;
                                                    errors.Add(String.Format("A nemzetközi küldeményazonosító ellenőrző összege hibás! Ellenőrző jegy: {0} - Számított: {1}", check, calculatedcheck));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        return isValid;
                    }
                    ); // rule 5
                    rules.Add(cvr);
                    #endregion rule 5

                    #region rule 6
                    cvr = new ComplexValidationRule(delegate(DataRow row, out List<string> errors)
                    {
                        bool isValid = true;
                        errors = new List<string>();

                        // irányítószám belföldre kötelező
                        string orszagkod = null;
                        string viszonylat = null;
                        if (row.Table.Columns.Contains("orszagkod"))
                        {
                            orszagkod = row["orszagkod"].ToString();
                        }
                        if (row.Table.Columns.Contains("viszonylat"))
                        {
                            viszonylat = row["viszonylat"].ToString();
                        }

                        // Belföld
                        //if (String.IsNullOrEmpty(viszonylat) && (String.IsNullOrEmpty(orszagkod) || orszagkod == "HU"))
                        if (parenttable == EFeladoJegyzek.Constants.nyilvantartott_tetel)
                        {
                            if (row.Table.Columns.Contains("cimzett_irsz") == false || String.IsNullOrEmpty(row["cimzett_irsz"].ToString()))
                            {
                                isValid = false;
                                errors.Add("A belföldi küldemény irányítószáma nincs megadva!");
                            }
                        }
                        // Külföld
                        else
                        {
                            // külföldre nem kell kitölteni az irányítószámot
                        }

                        return isValid;
                    }
                    ); // rule 6
                    rules.Add(cvr);
                    #endregion rule 6
                }
            }
            return rules;
        }
        #endregion Utility

        /// <summary>
        /// XSD Restrictions/Facets for Datatypes
        /// -----------------------------------------------------------------------------------------------------
        /// Constraint          Description 
        /// -----------------------------------------------------------------------------------------------------
        /// enumeration         Defines a list of acceptable values 
        /// fractionDigits      Specifies the maximum number of decimal places allowed. Must be equal to or greater than zero 
        /// length              Specifies the exact number of characters or list items allowed. Must be equal to or greater than zero 
        /// maxExclusive        Specifies the upper bounds for numeric values (the value must be less than this value) 
        /// maxInclusive        Specifies the upper bounds for numeric values (the value must be less than or equal to this value) 
        /// maxLength           Specifies the maximum number of characters or list items allowed. Must be equal to or greater than zero 
        /// minExclusive        Specifies the lower bounds for numeric values (the value must be greater than this value) 
        /// minInclusive        Specifies the lower bounds for numeric values (the value must be greater than or equal to this value) 
        /// minLength           Specifies the minimum number of characters or list items allowed. Must be equal to or greater than zero 
        /// pattern             Defines the exact sequence of characters that are acceptable  
        /// totalDigits         Specifies the exact number of digits allowed. Must be greater than zero 
        /// whiteSpace          Specifies how white space (line feeds, tabs, spaces, and carriage returns) is handled 
        /// </summary>
        public class SimpleTypeRestriction
        {
            #region Properties
            private bool mandatory = false; // true if element or element's any parent has the setting minOccurs == 0; false otherwise
            public bool Mandatory
            {
                get { return mandatory; }
                set { mandatory = value; }
            }

            private string simpleType = null; // xs:string, xs:integer
            public string SimpleType
            {
                get { return simpleType; }
                set { simpleType = value; }
            }

            private Type baseType = null; // typeof(string), typeof(integer)
            public Type BaseType
            {
                get { return baseType; }
                set { baseType = value; }
            }

            private UInt64? minOccurs = null; // not defined
            public UInt64? MinOccurs
            {
                get { return minOccurs; }
                set { minOccurs = value; }
            }

            private UInt64? maxOccurs = null; // -> unbound
            public UInt64? MaxOccurs
            {
                get { return maxOccurs; }
                set { maxOccurs = value; }
            }

            private List<string> enumeration = null;
            public List<string> Enumeration
            {
                get { return enumeration; }
                set { enumeration = value; }
            }

            private UInt64? fractionDigits = null; // not defined
            public UInt64? FractionDigits
            {
                get { return fractionDigits; }
                set { fractionDigits = value; }
            }

            private UInt64? length = null; // not defined
            public UInt64? Length
            {
                get { return length; }
                set { length = value; }
            }

            private UInt64? maxExclusive = null; // not defined
            public UInt64? MaxExclusive
            {
                get { return maxExclusive; }
                set { maxExclusive = value; }
            }

            private UInt64? maxInclusive = null; // not defined
            public UInt64? MaxInclusive
            {
                get { return maxInclusive; }
                set { maxInclusive = value; }
            }

            private UInt64? maxLength = null; // not defined
            public UInt64? MaxLength
            {
                get { return maxLength; }
                set { maxLength = value; }
            }

            private UInt64? minExclusive = null; // not defined
            public UInt64? MinExclusive
            {
                get { return minExclusive; }
                set { minExclusive = value; }
            }

            private UInt64? minInclusive = null; // not defined
            public UInt64? MinInclusive
            {
                get { return minInclusive; }
                set { minInclusive = value; }
            }

            private UInt64? minLength = null; // not defined
            public UInt64? MinLength
            {
                get { return minLength; }
                set { minLength = value; }
            }

            private string pattern = null;
            public string Pattern
            {
                get { return pattern; }
                set { pattern = value; }
            }

            private UInt64? totalDigits = null; // not defined
            public UInt64? TotalDigits
            {
                get { return totalDigits; }
                set { totalDigits = value; }
            }

            private string whiteSpace = null;
            public string WhiteSpace
            {
                get { return whiteSpace; }
                set { whiteSpace = value; }
            }
            #endregion Properties

            #region Private Utility
            private const int LengthLimit = 50;

            private static string CropValueToLengthLimit(string value, bool addEllipsis)
            {
                if (!String.IsNullOrEmpty(value) && value.Length > LengthLimit)
                {
                    if (addEllipsis)
                    {
                        return String.Concat(value.Substring(0, LengthLimit - 3), "...");
                    }
                    else
                    {
                        return (value.Substring(0, LengthLimit));
                    }
                }

                return value;
            }

            private static List<string> AddToErrorsList(List<string> errors, string facetname)
            {
                if (errors != null)
                {
                    if (!String.IsNullOrEmpty(facetname) && !errors.Contains(facetname))
                    {
                        errors.Add(facetname);
                    }
                }
                return errors;
            }

            private static List<string> AddToErrorsList(List<string> errors, string facetname, string value)
            {
                if (errors != null)
                {
                    if (!String.IsNullOrEmpty(facetname) && !errors.Contains(facetname))
                    {
                        errors.Add(String.Join(";", new string[] { facetname, CropValueToLengthLimit(value, true) ?? "" }));
                    }
                }
                return errors;
            }
            #endregion Private Utility

            #region Validation
            public bool Validate(object value, out List<string> errors)
            {
                bool isValid = true;

                errors = new List<string>();

                if (this.Mandatory && (value == null || value is DBNull))
                {
                    AddToErrorsList(errors, "mandatory");
                }
                else
                {
                    if (value != null && !(value is DBNull))
                    {
                        if (this.SimpleType == "integer")
                        {
                            UInt64 uintValue;
                            if (UInt64.TryParse(value.ToString(), out uintValue))
                            {
                                if (this.MinInclusive.HasValue && uintValue < this.MinInclusive)
                                {
                                    AddToErrorsList(errors, "minInclusive", this.MinInclusive.ToString());
                                }
                                if (this.MaxInclusive.HasValue && uintValue > this.MaxInclusive)
                                {
                                    AddToErrorsList(errors, "maxInclusive", this.MaxInclusive.ToString());
                                }

                                if (this.MinExclusive.HasValue && uintValue <= this.MinExclusive)
                                {
                                    AddToErrorsList(errors, "minExclusive", this.MinExclusive.ToString());
                                }
                                if (this.MaxExclusive.HasValue && uintValue >= this.MaxExclusive)
                                {
                                    AddToErrorsList(errors, "maxExclusive", this.MaxExclusive.ToString());
                                }
                            }
                            else
                            {
                                AddToErrorsList(errors, "baseType", "integer");
                            }
                        }
                        else if (this.SimpleType == "string")
                        {
                            String strValue = value as String;
                            if (strValue != null)
                            {
                                if (this.Length.HasValue && (UInt64)strValue.Length != this.Length)
                                {
                                    AddToErrorsList(errors, "length", this.Length.ToString());
                                }
                                if (this.MaxLength.HasValue && (UInt64)strValue.Length > this.MaxLength)
                                {
                                    AddToErrorsList(errors, "maxLength", this.MaxLength.ToString());
                                }
                                if (this.MinLength.HasValue && (UInt64)strValue.Length < this.MinLength)
                                {
                                    AddToErrorsList(errors, "minLength", this.MinLength.ToString());
                                }

                                if (!String.IsNullOrEmpty(this.Pattern))
                                {
                                    System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(this.Pattern);

                                    if (!re.IsMatch(strValue))
                                    {
                                        AddToErrorsList(errors, "pattern", this.Pattern);
                                    }
                                }

                                if (this.Enumeration != null && this.Enumeration.Count > 0)
                                {
                                    if (!this.Enumeration.Contains(strValue))
                                    {
                                        string enumValues = String.Join("|", this.Enumeration.ToArray());
                                        AddToErrorsList(errors, "enumeration", enumValues);
                                    }
                                }
                            }
                            else
                            {
                                AddToErrorsList(errors, "baseType", "string");
                            }
                        }
                    }
                }
                if (errors.Count > 0)
                {
                    isValid = false;
                }
                return isValid;
            }

            #endregion Validation

        } // class


        public delegate bool DataRowRule(DataRow row, out List<string> errors);
        public class ComplexValidationRule
        {
            #region Properties
            private DataRowRule rule = null;

            public DataRowRule Rule
            {
                get { return rule; }
                set { rule = value; }
            }

            #endregion Properties

            #region Private Utility
            #endregion Private Utility

            #region Constructors
            public ComplexValidationRule(DataRowRule rule)
            {
                this.rule = rule;
            }
            #endregion Constructors

            #region Validation
            public virtual bool Validate(object value, out List<string> errors)
            {
                bool isValid = true;
                errors = null;
                if (this.rule != null)
                {
                    if (value is DataRow)
                    {
                        DataRow row = value as DataRow;

                        isValid = this.rule(row, out errors);
                    }
                }
 
                return isValid;
            }

            public virtual bool Validate(System.Data.DataRow row, string colname, out List<string> errors)
            {
                bool isValid = true;

                object value = null;
                if (row != null && !String.IsNullOrEmpty(colname))
                {
                    if (row.Table.Columns.Contains(colname))
                    {
                        value = row[colname];
                    }
                }

                isValid = Validate(value, out errors);
                return isValid;
            }

            #endregion Validation

        } // class

        #region private validation methods
        private static bool ValidateRestrictions(System.Data.DataRow row, string sourcetable, out Dictionary<string, List<string>> errors)
        {
            bool isValid = true;
            errors = new Dictionary<string, List<string>>();
            Dictionary<string, SimpleTypeRestriction> restrictions = null;
            if (!String.IsNullOrEmpty(sourcetable) && SimpleTypeRestrictions.ContainsKey(sourcetable))
            {
                restrictions = SimpleTypeRestrictions[sourcetable];
            }

            if (restrictions != null || row != null)
            {
                foreach (string dictKey in restrictions.Keys)
                {
                    string[] keyElements = dictKey.Split(new char[] { '/' });
                    string colname = keyElements[0];

                    if (!String.IsNullOrEmpty(colname) && row.Table.Columns.Contains(colname))
                    {
                        SimpleTypeRestriction restriction = restrictions[dictKey];
                        if (restriction != null)
                        {
                            object value = null;
                            value = row[colname];

                            List<string> lstErrors;
                            isValid &= restriction.Validate(value, out lstErrors);
                            if (lstErrors != null)
                            {
                                foreach (string facetname_with_value in lstErrors)
                                {
                                    string[] facetname_with_value_items = facetname_with_value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                                    string facetname = facetname_with_value_items[0];
                                    string facetValue = (facetname_with_value_items.Length < 2 ? null : String.Format("{0}={1}", facetname, facetname_with_value_items[1]));

                                    // áttesszük a facet előírt értékét az oszlop neve mellé
                                    AddToErrorsDictionary(errors, String.Join(";", new string[] { colname, facetValue }), facetname);
                                }
                            }
                        }
                    }
                }
            }

            return isValid;
        }

        private static bool ValidateComplexRules(DataRow row, string sourcetable, out Dictionary<string, List<string>> errors)
        {
            bool isValid = true;
            errors = new Dictionary<string, List<string>>();

            List<ComplexValidationRule> rules = null;
            if (!String.IsNullOrEmpty(sourcetable) && DataRowRules.ContainsKey(sourcetable))
            {
                rules = DataRowRules[sourcetable];
            }

            if (rules != null || row != null)
            {
                foreach (ComplexValidationRule rule in rules)
                {
                    if (rule != null)
                    {
                        List<string> lstErrors;
                        isValid &= rule.Validate(row, out lstErrors);
                        if (lstErrors != null)
                        {
                            foreach (string msg in lstErrors)
                            {
                                AddToErrorsDictionary(errors, msg, "komplex");
                            }
                        }
                    }
                }
            }

            return isValid;
        }

        #endregion private validation methods

        protected static Dictionary<string, List<string>> AddToErrorsDictionary(Dictionary<string, List<string>> errors, string colname, string facetname)
        {
            if (errors != null)
            {
                if (!String.IsNullOrEmpty(colname) && !String.IsNullOrEmpty(facetname))
                {
                    if (!errors.ContainsKey(facetname))
                    {
                        errors.Add(facetname, new List<string>());
                    }

                    if (!errors[facetname].Contains(colname))
                    {
                        errors[facetname].Add(colname);
                    }
                }
            }
            return errors;
        }

        public static bool Validate(System.Data.DataRow row, string sourcetable, out Dictionary<string, List<string>> errors)
        {
            bool isValid = true;

            errors = null;

            Dictionary<string, List<string>> errorDictionary = new Dictionary<string, List<string>>();

            isValid &= ValidateRestrictions(row, sourcetable, out errorDictionary);

            if (errorDictionary != null)
            {
                errors = new Dictionary<string, List<string>>(errorDictionary);
            }

            isValid &= ValidateComplexRules(row, sourcetable, out errors);

            if (errorDictionary != null)
            {
                if (errors == null)
                {
                    errors = new Dictionary<string, List<string>>(errorDictionary);
                }
                else
                {
                    foreach (string key in errorDictionary.Keys)
                    {
                        if (!errors.ContainsKey(key))
                        {
                            errors.Add(key, new List<string>());
                        }
                        foreach (string error in errorDictionary[key])
                        {
                            if (!errors[key].Contains(error))
                            {
                                errors[key].Add(error);
                            }
                        }
                    }
                }
            }

            return isValid;
        }

        public class StatisticsResult
        {
            private const string defaultSeparator = ";";

            private string fieldname;
            public string Fieldname
            {
                get { return fieldname; }
                //set { fieldname = value; }
            }

            private int count = 0;
            public int Count
            {
                get { return count; }
                //set { count = value; }
            }

            private List<string> ids = new List<string>();
            public List<string> Ids
            {
                get { return ids; }
                //set { ids = value; }
            }

            public string GetIds(string separator)
            {
                return String.Join(separator ?? defaultSeparator, Ids.ToArray());
            }

            public string GetIds()
            {
                return GetIds(defaultSeparator);
            }

            public StatisticsResult(string fieldname)
            {
                this.fieldname = fieldname;
            }

            public void AddNewItem(string ids)
            {
                if (!String.IsNullOrEmpty(ids))
                {
                    string[] arrayIds = ids.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string id in arrayIds)
                    {
                        if (Ids.Contains(id) == false)
                        {
                            Ids.Add(id);
                        }
                    }
                }

                count++;
            }
        }

        public static DataSet ConvertStatisticsToDataSet(string detailkey, Dictionary<string, Dictionary<string, StatisticsResult>> statistics)
        {
            DataSet ds = new DataSet();

            if (statistics != null)
            {
                foreach (string facetName in statistics.Keys)
                {
                    DataTable dt = new DataTable(String.Format("{0}_{1}", detailkey, facetName));
                    dt.Columns.Add("FacetName", typeof(string));
                    dt.Columns.Add("DetailKey", typeof(string));
                    dt.Columns.Add("FieldName", typeof(string));
                    dt.Columns.Add("Count", typeof(int));
                    dt.Columns.Add("Ids", typeof(string));
                    foreach (string fieldName in statistics[facetName].Keys)
                    {
                        DataRow row = dt.NewRow();
                        row["DetailKey"] = detailkey;
                        row["FacetName"] = facetName;

                        row["FieldName"] = fieldName;
                        row["Count"] = statistics[facetName][fieldName].Count;
                        row["Ids"] = statistics[facetName][fieldName].GetIds();
                        dt.Rows.Add(row);
                    }

                    ds.Tables.Add(dt);
                }
            }

            return ds;
        }

        public static Dictionary<string, Dictionary<string, StatisticsResult>> GetValidationStatistics(System.Data.DataTable datatable)
        {
            // facet ((fieldname, cnt, ids), (fieldname, cnt),...), facet ((fieldname, cnt, ids), (fieldname, cnt, ids),...), ...
            Dictionary<string, Dictionary<string, StatisticsResult>> statisticsDictionary = new Dictionary<string, Dictionary<string, StatisticsResult>>();
            string sourcetable = datatable.TableName;

            Dictionary<string, List<string>> errors;

            foreach (DataRow row in datatable.Rows)
            {
                ValidateRestrictions(row, sourcetable, out errors);

                if (errors != null)
                {
                    string ids = null;
                    if (row.Table.Columns.Contains("ids"))
                    {
                        ids = row["ids"].ToString();
                    }


                    foreach (string key in errors.Keys)
                    {
                        if (!statisticsDictionary.ContainsKey(key))
                        {
                            statisticsDictionary.Add(key, new Dictionary<string, StatisticsResult>());
                        }
                        foreach (string fieldname_with_facetvalue in errors[key])
                        {
                            string fieldname = fieldname_with_facetvalue.Split(new char[] {';'})[0];
                            if (!statisticsDictionary[key].ContainsKey(fieldname))
                            {
                                statisticsDictionary[key].Add(fieldname, new StatisticsResult(fieldname));
                            }
                            statisticsDictionary[key][fieldname].AddNewItem(ids);
                        }
                    }
                }
            }

            return statisticsDictionary;
        }

        public static string GetErrorCategoryHeader(string errorCategory)
        {
            if (!String.IsNullOrEmpty(errorCategory))
            {
                if (DictErrorCategoryHeader.ContainsKey(errorCategory))
                {
                    return DictErrorCategoryHeader[errorCategory];
                }
            }

            return errorCategory;
        }

    } // class

    public static class Xml
    {

        public static string GetFormattedStringFromXmlDocument(System.Xml.XmlDocument xmlDocument)
        {
            return GetFormattedStringFromXmlDocument(xmlDocument, EFeladoJegyzek.Constants.DefaultXmlFormattingType);
        }

        public static string GetFormattedStringFromXmlDocument(System.Xml.XmlDocument xmlDocument, EFeladoJegyzek.Constants.XmlFormattingType xmlFormattingType)
        {
            System.Xml.XmlWriter writer = null;
            System.IO.MemoryStream ms = null;
            string xmlString = null;

            try
            {
                System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(1250);
                ms = new System.IO.MemoryStream();

                System.Xml.XmlWriterSettings settings = new System.Xml.XmlWriterSettings();
                settings.Encoding = encoding;
                settings.ConformanceLevel = System.Xml.ConformanceLevel.Document;
                settings.Indent = (xmlFormattingType != EFeladoJegyzek.Constants.XmlFormattingType.None);
                if (xmlFormattingType == EFeladoJegyzek.Constants.XmlFormattingType.Indented)
                {
                    settings.IndentChars = EFeladoJegyzek.Constants.DefaultXmlSettingsIndentChars;
                }
                else
                {
                    settings.IndentChars = ""; //"\t";
                }

                writer = System.Xml.XmlTextWriter.Create(ms, settings);
                xmlDocument.Save(writer);
                //writer.Flush();

                xmlString = encoding.GetString(ms.ToArray());
            }
            finally
            {
                if (writer != null)
                    writer.Close();

                if (ms != null)
                    ms.Close();
            }

            return xmlString;
        }

        // delete old exported files
        private static void deleteOldFiles(string path)
        {
            System.IO.DirectoryInfo myDirectoryInfo = new System.IO.DirectoryInfo(path);
            foreach (System.IO.FileInfo myFileInfo in myDirectoryInfo.GetFiles())
            {
                if (myFileInfo.LastWriteTime < DateTime.Today)
                {
                    myFileInfo.Delete();
                }
            }
        }

        public static string WriteFormattedStringFromXmlDocument(System.Xml.XmlDocument xmlDocument, EFeladoJegyzek.Constants.XmlType xmlType)
        {
            return WriteFormattedStringFromXmlDocument(xmlDocument, EFeladoJegyzek.Constants.DefaultXmlDirPath, xmlType);
        }

        public static string WriteFormattedStringFromXmlDocument(System.Xml.XmlDocument xmlDocument, string dirPath, EFeladoJegyzek.Constants.XmlType xmlType)
        {
            string fileName = null;
            string xmlString = GetFormattedStringFromXmlDocument(xmlDocument);

            if (String.IsNullOrEmpty(dirPath))
            {
                throw new Exception("Nincs megadva a célkönyvtár!");
            }

            if (!String.IsNullOrEmpty(xmlString) && !String.IsNullOrEmpty(dirPath))
            {
                System.IO.StreamWriter writer = null;

                try
                {
                    //string dirPath = @"C:\temp\ContentumNet\EFeladoJegyzek";
                    System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(dirPath);

                    if (di.Exists == false)
                    {
                        di.Create();
                    }
                    else
                    {
                        try
                        {
                            // possible Exception types
                            // System.IO.IOException
                            // System.Security.SecurityException
                            // System.UnauthorizedAccessException
                            deleteOldFiles(dirPath);  // clean up in the directory
                        }
                        catch (Exception ex)
                        {
                            Logger.Warn(String.Format("Path: {0}<br />Operation: File Delete", dirPath), ex);
                        }
                    }

                    switch (xmlType)
                    {
                        case EFeladoJegyzek.Constants.XmlType.Adatok:
                            fileName = String.Format("ADAT_{0}.xml", DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                            break;
                        case EFeladoJegyzek.Constants.XmlType.UgyfelAdatok:
                            fileName = String.Format("UGYFEL_{0}.xml", DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                            break;
                        default:
                            fileName = String.Format("EFELADOJEGYZEK_{0}.xml", DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                            break;
                    }

                    string filePath = System.IO.Path.Combine(dirPath, fileName);
                    System.Text.Encoding encoding = EFeladoJegyzek.Constants.DefaultEncoding; //System.Text.Encoding.GetEncoding(1250);
                    writer = new System.IO.StreamWriter(filePath, false, encoding);

                    //byte[] bytes = encoding.GetBytes(xmlString);

                    writer.Write(xmlString);
                    writer.Flush();
                }
                finally
                {
                    if (writer != null)
                        writer.Close();
                }
            }
            return fileName;
        }
    } // class

} // namespace
