﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility.Models;
using System;

namespace Contentum.eUtility.ePosta.Tertiveveny
{
    /// <summary>
    /// Summary description for ePostaiTertivevenyErkeztet
    /// </summary>
    public static partial class ePostaiTertivevenyErkeztet
    {

        /// <summary>
        /// Erkeztet
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="ragSzam"></param>
        /// <param name="atvetelIdopont"></param>
        /// <param name="tertivevenyVisszateresKod"></param>
        public static void Erkeztet(ExecParam execParam, ePostaTertivevenyDataPdfClass wrapper)
        {
            EREC_KuldTertivevenyek terti = ServiceFindTertiveveny(execParam, wrapper.Ragszam);

            if (terti == null)
            {
                throw new ResultException("Tertiveveny nem található !");
            }

            EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
            DateTime atvetelDatum = DateTime.Today;
            DateTime.TryParse(wrapper.Igazolas.AtvetelIdopont, out atvetelDatum);
            
            Result ResultErkeztet = service.ETertivevenyErkeztetes(execParam,
                  wrapper.Ragszam,
                  wrapper.Igazolas.TertivevenyVisszateresKod,
                  wrapper.Igazolas.KezbesitesiVelelemDatuma,
                  atvetelDatum,
                  wrapper.Igazolas.AtvevoNeve,
                  wrapper.Igazolas.AtvetelJogcim,
                  string.Empty
                  );
            ResultErkeztet.CheckError();
        }

        public static EREC_KuldTertivevenyek ServiceFindTertiveveny(ExecParam execParam, string ragSzam)
        {
            EREC_KuldTertivevenyek erec_KuldTertiveveny = new EREC_KuldTertivevenyek();
            EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();

            EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();

            search.Ragszam.Value = ragSzam;
            search.Ragszam.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, search);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                return null;
            }
            if (result.Ds.Tables.Count < 1 || result.Ds.Tables[0].Rows.Count < 1)
            {
                return null;
            }

            erec_KuldTertiveveny.Id = result.Ds.Tables[0].Rows[0]["Id"].ToString();
            erec_KuldTertiveveny.Kuldemeny_Id = result.Ds.Tables[0].Rows[0]["Kuldemeny_Id"].ToString();
            erec_KuldTertiveveny.Ragszam = result.Ds.Tables[0].Rows[0]["Ragszam"].ToString();
            erec_KuldTertiveveny.TertivisszaKod = result.Ds.Tables[0].Rows[0]["TertivisszaKod"].ToString();
            erec_KuldTertiveveny.TertivisszaDat = result.Ds.Tables[0].Rows[0]["TertivisszaDat"].ToString();
            erec_KuldTertiveveny.AtvevoSzemely = result.Ds.Tables[0].Rows[0]["AtvevoSzemely"].ToString();
            erec_KuldTertiveveny.AtvetelDat = result.Ds.Tables[0].Rows[0]["AtvetelDat"].ToString();
            erec_KuldTertiveveny.BarCode = result.Ds.Tables[0].Rows[0]["BarCode"].ToString();
            erec_KuldTertiveveny.Allapot = result.Ds.Tables[0].Rows[0]["Allapot"].ToString();
            erec_KuldTertiveveny.ErvKezd = result.Ds.Tables[0].Rows[0]["ErvKezd"].ToString();
            erec_KuldTertiveveny.ErvVege = result.Ds.Tables[0].Rows[0]["ErvVege"].ToString();
            erec_KuldTertiveveny.Base.Ver = result.Ds.Tables[0].Rows[0]["Ver"].ToString();
            erec_KuldTertiveveny.Base.LetrehozasIdo = result.Ds.Tables[0].Rows[0]["LetrehozasIdo"].ToString();
            erec_KuldTertiveveny.Base.Letrehozo_id = result.Ds.Tables[0].Rows[0]["Letrehozo_id"].ToString();
            erec_KuldTertiveveny.Base.ModositasIdo = result.Ds.Tables[0].Rows[0]["ModositasIdo"].ToString();
            erec_KuldTertiveveny.Base.Modosito_id = result.Ds.Tables[0].Rows[0]["Modosito_id"].ToString();
            erec_KuldTertiveveny.Base.Note = result.Ds.Tables[0].Rows[0]["Note"].ToString();
            erec_KuldTertiveveny.Base.Stat_id = result.Ds.Tables[0].Rows[0]["Stat_id"].ToString();
            erec_KuldTertiveveny.Base.Tranz_id = result.Ds.Tables[0].Rows[0]["Tranz_id"].ToString();
            erec_KuldTertiveveny.Base.UIAccessLog_id = result.Ds.Tables[0].Rows[0]["UIAccessLog_id"].ToString();
            erec_KuldTertiveveny.Base.ZarolasIdo = result.Ds.Tables[0].Rows[0]["ZarolasIdo"].ToString();
            erec_KuldTertiveveny.Base.Zarolo_id = result.Ds.Tables[0].Rows[0]["Zarolo_id"].ToString();

            return erec_KuldTertiveveny;
        }

        /// <summary>
        /// Update item in EREC_KuldTertivevenyekService
        /// </summary>
        /// <param name="item"></param>
        public static Result ServiceUpdateTertiveveny(ExecParam execParam, EREC_KuldTertivevenyek item)
        {
            EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
            execParam.Record_Id = item.Id;
            Result result = service.Update(execParam, item);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            return result;
        }
        /// <summary>
        /// GetKuldemeny
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static EREC_KuldKuldemenyek GetKuldemeny(ExecParam execParam, string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return null;
            }
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            execParam.Record_Id = id;
            Result result = service.Get(execParam);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            return result.Record as EREC_KuldKuldemenyek;
        }

        /// <summary>
        /// Load document by scan service
        /// </summary>
        /// <param name="content"></param>
        /// <param name="fileName"></param>
        /// <param name="ragszam"></param>
        public static bool ServiceUploadPDF(ExecParam execParam, byte[] content, string fileName, string ragszam)
        {

            eScanWebService.ScanService service = eRecordService.ServiceFactory.GetScanService();
            KRT_Felhasznalok currentUser = GetUser(execParam);
            string userName = currentUser.UserNev;
            string result = service.LoadDocumentDS(userName, ragszam, "-1", "ePostaiTertivevenyErkeztet", fileName, content, string.Empty, null);

            if (!string.IsNullOrEmpty(result) & result != "OK")
            {
                string msg = string.Format("ScanService.LoadDoument Ragszam:{0}. Hiba:{1}", ragszam, result);
                Logger.Error(msg);

                return false;
            }

            return true;
        }

        /// <summary>
        /// Return current user
        /// </summary>
        /// <returns></returns>
        private static KRT_Felhasznalok GetUser(ExecParam execParam)
        {
            eAdmin.Service.KRT_FelhasznalokService krt_FelhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            execParam.Record_Id = execParam.Felhasznalo_Id;
            Result result = krt_FelhasznalokService.Get(execParam);
            return (KRT_Felhasznalok)result.Record;
        }
    }
}