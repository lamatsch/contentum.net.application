﻿using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eUtility.ePosta
{
    public class ePostaUtility
    {
        public static bool IsHivatalosKuldemeny(ExecParam execParam, string kimenoKuldemenyFajta)
        {
            bool nincsItem;
            var hivatalosFuggo = Contentum.eUtility.KodtarFuggoseg.GetFuggoKodtarakKodList(execParam, KodTarak.KIMENO_KULDEMENY_FAJTA.kcsNev, KodTarak.HIVATALOS_LEVEL_KODCSOPORT.KODCSOPORT_KOD, kimenoKuldemenyFajta, out nincsItem);
            if (hivatalosFuggo == null || hivatalosFuggo.Count == 0 || nincsItem)
            {
                Logger.Error(string.Format("ePostaUtility.IsHivatalosKuldemeny: Nincs meghatározva, hogy hivatalos-e a küldemény típus {0}. FüggőKódtár összerendelés szükséges", kimenoKuldemenyFajta));
                return false;
            }
            var result = hivatalosFuggo.First();
            if (result == KodTarak.HIVATALOS_LEVEL_KODCSOPORT.IGEN)
            {
                return true;
            }
            return false;
        }
    }
}
